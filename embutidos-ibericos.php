<?php include_once('includes/parametros.php'); ?>
<?php
	$header_title = 'Jamones Extremadura';
	$header_description = 'Jamones Extremadura';
?>
<?php 
	$isHome = true;
?>
<?php include('includes/head.php'); ?>
	<!-- Slider Revolution -->
	<link rel="stylesheet" href="plugins/revolution-slider/settings.css">
	<!-- Owl Carousel -->
	<link rel="stylesheet" href="plugins/owl-carousel/owl.carousel.css">
	<link rel="stylesheet" href="plugins/owl-carousel/owl.transitions.css">
	<link rel="stylesheet" href="plugins/owl-carousel/owl-multi.css">
<?php include('includes/header.php'); ?>
<?php include('includes/nav.php'); ?>
	<main role="main">
		<!-- Main slider. Begin -->

		<div class="re-cien box-reset shoes-genre-2 bg-contain-bottom" >
            <div align="center">
            <h2 style="padding-top:150px" class="title-jamon text-center">NUESTROS EMBUTIDOS IBÉRICOS</h2>
            <h4>Obtenido de las extremidades posteriores del cerdo ibérico.</h4>
			<i class="icon-line font-30 font-fourth"></i> 
			</div>
            

        <section id="section-marcas" class="re-central box-reset">
			<div class="row padding-b-100" style="background-color: white;">
			
				<div class="clearfix"></div>

				<div class="brands-home-wrapper">
					<div class="row">
						<div class="col-sm-6 col-lg-4 brand-item brand-enginia">
						<a href="#"><img src="images-product/img-chorizo.jpg" class="brand-item-image-gray"></a>
							<a href="#"><img src="images-product/img-chorizo-hover.jpg" class="brand-item-image-color"></a>
							<figcaption class="brand-item-caption-wrapper">
								<div class="brand-item-caption">
									<h4>Chorizo Ibérico</h4>
									<p>El Chorizo Ibérico es elaborado a partir de una selección de carnes del cerdo ibérico, troceadas artesanalmente a cuchillo y aderezadas a base de pimentón, 
                                    ajo, sal y embuchados en tripa natural y secados al aire del pie de la Sierra Extremeña.</p>
									<ul class="brand-item-features">
										<li>Por pieza: Caña de 1 kg.</li>
										<li>Paquete rebanado de 100 grs.</li>
									</ul>
								</div>
							</figcaption>
						</div>
						<div class="col-sm-6 col-lg-4 brand-item brand-sikla">
							<a href="#"><img src="images-product/img-lomo.jpg" class="brand-item-image-gray"></a>
							<a href="#"><img src="images-product/img-lomo-hover.jpg" class="brand-item-image-color"></a>
							<figcaption class="brand-item-caption-wrapper">
								<div class="brand-item-caption">
									<h4>Lomo Ibérico</h4>
									<p>Se trata de un embutido hecho a partir de una selección de carne de lomo de cerdo ibérico y aliñado según la receta original de la casa con sal, pimentón y ajo.</p>
                                    <p>Una exquisitez entre los embutidos, es consistente y presenta vetas de grasa entre las fibras musculares que aportan un aroma sutil y un delicioso sabor.</p>
									<ul class="brand-item-features">
										<li>Por pieza: Caña de 1.3 kg</li>
										<li>Paquete rebanado de 100 grs. al alto vacío</li>
									</ul>
								</div>
							</figcaption>
						</div>
						<div class="col-sm-6 col-lg-4 brand-item brand-sikla">
							<a href="#"><img src="images-product/img-lomito.jpg" class="brand-item-image-gray"></a>
							<a href="#"><img src="images-product/img-lomito-hover.jpg" class="brand-item-image-color"></a>
							<figcaption class="brand-item-caption-wrapper">
								<div class="brand-item-caption">
									<h4>Lomito Ibérico</h4>
									<p>El Lomito Ibérico de Bellota es un embutido elaborado a partir de la presa ubicada junto a la paletilla del cerdo ibérico.</p>
                                    <p>Aliñado con pimentón, ajo y sal, sigue un proceso de curado lento y artesanal.</p>
									<ul class="brand-item-features">
										<li>Pieza 400 grs.</li>
									</ul>
								</div>
							</figcaption>
						</div>
						<div class="col-sm-6 col-lg-4 brand-item brand-zylentek">
							<a href="#"><img src="images-product/images_catalogo-04.jpg" class="brand-item-image-gray"></a>
							<a href="#"><img src="images-product/images_catalogo-04-hover.jpg" class="brand-item-image-color"></a>
							<figcaption class="brand-item-caption-wrapper">
								<div class="brand-item-caption">
									<h4>Paleta Ibérica de Bellota</h4>
									<p>Paleta Ibérica de Bellota con peso de 5 kg. y 36 meses de maduración.</p>
									<ul class="brand-item-features">
										<li>Con Base Jamonero $5,900</li>
										<li>Sin Base Jamonero $5,100</li>
									</ul>
								</div>
							</figcaption>
						</div>
						<div class="col-sm-6 col-lg-4 brand-item brand-sikla">
							<a href="#"><img src="images-product/img-salchichon.jpg" class="brand-item-image-gray"></a>
							<a href="#"><img src="images-product/img-salchichon-hover.jpg" class="brand-item-image-color"></a>
							<figcaption class="brand-item-caption-wrapper">
								<div class="brand-item-caption">
									<h4>Salchichón Ibérico</h4>
									<p>El Salchichón Ibérico es elaborado a partir de una selección de magro de cerdo ibérico, condimentado con sal y especias naturales, tales 
                                    como pimienta en grano y nuez moscada. Delicioso.</p>
									<ul class="brand-item-features">
										<li>Por pieza: Caña de 1 kg.</li>
										<li>Paquete rebanado de 100 grs.</li>
									</ul>
								</div>
							</figcaption>
						</div>
						<div class="col-sm-6 col-lg-4 brand-item brand-gripple">
							<a href="#"><img src="images-product/img-fuet.jpg" class="brand-item-image-gray"></a>
							<a href="#"><img src="images-product/img-fuet-hover.jpg" class="brand-item-image-color"></a>
							<figcaption class="brand-item-caption-wrapper">
								<div class="brand-item-caption">
									<h4>Fuet Ibérico</h4>
									<p>Es elaborado a base de carne magra de cerdo ibérico y panceta picada y saborizada con ajo y una variedad de especias.</p>
                                    <p>Se embute en tripa natural de tamaño no superior a un dedo y se deja curar en nuestros secaderos naturales durante 30 días aproximadamente. Se suele utilizar como tapa o aperitivo.</p>
									<ul class="brand-item-features">
										<li>Piezas de 200 gr.</li>
									</ul>
								</div>
							</figcaption>
						</div>
                        
                        <div class="col-sm-6 col-lg-4 brand-item brand-enginia">
						<a href="#"><img src="images-product/img-morcilla.jpg" class="brand-item-image-gray"></a>
							<a href="#"><img src="images-product/img-morcilla-hover.jpg" class="brand-item-image-color"></a>
							<figcaption class="brand-item-caption-wrapper">
								<div class="brand-item-caption">
									<h4>Morcilla Ibérica curada artesanal</h4>
									<p>Elaboradas de forma tradicional con magro de cerdo ibérico de  bellota, sangre, pimentón y grasa de cerdo Ibérico.</p>
                                    <p>Todo amasado, embutido en tripa natural de curada.</p>
                                    <p>Envasado al vacío.</p>
									<ul class="brand-item-features">
										<li> Piezas de 360 grs. aprox.</li>
									</ul>
								</div>
							</figcaption>
						</div>
                        
						<div class="col-sm-6 col-lg-4 brand-item brand-gripple">
							<a href="#"><img src="images-product/img-morcon.jpg" class="brand-item-image-gray"></a>
							<a href="#"><img src="images-product/img-morcon-hover.jpg" class="brand-item-image-color"></a>
							<figcaption class="brand-item-caption-wrapper">
								<div class="brand-item-caption">
									<h4>Morcón Ibérico</h4>
									<p>El Morcón Ibérico se elabora a partir de carne magra de cerdo ibérico, condimentado con pimentón natural, presenta un color rojo brillante, marmóreo,
                                     espléndido sabor, gran aroma y textura compacta.</p>
									<ul class="brand-item-features">
										<li>Por pieza: Caña de 1 kg.</li>
                                        <li>Paquete rebanado de 100 grs.</li>
									</ul>
								</div>
							</figcaption>
						</div>
                        <div class="col-sm-6 col-lg-4 brand-item brand-gripple">
							<a href="#"><img src="images-product/img-sobrasada.jpg" class="brand-item-image-gray"></a>
							<a href="#"><img src="images-product/img-sobrasada-hover.jpg" class="brand-item-image-color"></a>
							<figcaption class="brand-item-caption-wrapper">
								<div class="brand-item-caption">
									<h4>Sobrasada de Ibérico</h4>
									<p>Elaborada con magro y cabecero de lomo de cerdos ibéricos de bellota. Se pican finamente y se marinan con una mezcla de pimentón dulce y picante, 
                                    sal y ajo durante 24 horas y se cura en tripa natural durante 3 meses.</p>
                                    <p>Las carnes usadas para su elaboración proceden de animales que han adquirido sus últimos kilos en régimen de montanera a base de bellotas y hierbas de la dehesa.</p>
									<ul class="brand-item-features">
										<li>Piezas de: 400 grs. / Envasado al vacío.</li>
									</ul>
								</div>
							</figcaption>
						</div>
					</div>
				</div>
			</div>
		</section>
				</div>
			</div>
		</div>
		<!-- Banner Board. End -->
	</main>
<?php $scripts = '
	<!-- Slider Revolution -->
	<script src="plugins/revolution-slider/jquery.themepunch.tools.min.js"></script>
	<script src="plugins/revolution-slider/jquery.themepunch.revolution.min.js"></script>

	<!-- Owl Carousel -->
	<script src="plugins/owl-carousel/owl.carousel.js"></script>

	<script src="css-js/slider-main.js"></script>
'; ?>
<?php include('includes/footer.php'); ?>