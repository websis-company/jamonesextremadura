<?php include_once('includes/parametros.php'); ?>
<?php
	$header_title = 'Jamones Extremadura';
	$header_description = 'Jamones Extremadura';
?>
<?php 
	$isHome = true;
?>
<?php include('includes/head.php'); ?>
	<!-- Slider Revolution -->
	<link rel="stylesheet" href="plugins/revolution-slider/settings.css">
	<!-- Owl Carousel -->
	<link rel="stylesheet" href="plugins/owl-carousel/owl.carousel.css">
	<link rel="stylesheet" href="plugins/owl-carousel/owl.transitions.css">
	<link rel="stylesheet" href="plugins/owl-carousel/owl-multi.css">
<?php include('includes/header.php'); ?>
<?php include('includes/nav.php'); ?>
	<main role="main">
		<!-- Main slider. Begin -->

		<div class="re-cien box-reset shoes-genre-2 bg-contain-bottom" style="background-image: url(images/images_bg-origen1.jpg);">
            <div align="center">
            <h2 style="padding-top:150px" class="title-jamon text-center">DENOMINACIÓN DE ORIGEN (D.O.)</h2>
            <h4>Origen y Tradición.</h4>
			<i class="icon-line font-30 font-fourth"></i> 
			</div>
            
            <div class="row"> 
				<div class="col-hd-12 selectos-contain padding-v-50">
					<div class="col-hd-6 col-sm-6">
						<div class="selectos-info">
							<img src="images/selectos-02.png">
							<small>El Origen del Cerdo Ibérico y su Tradición.</small>
							<i class="icon-line"></i>
                            
                            <div align="left" style="padding:15px; color:#FFF">
                            <p>El término  dehesa procede del castellano " defensa ", que hace referencia al terreno acotado al libre pastoreo de los ganados 
                            trashumantes mesteños que recorrían el suroeste español, y que data de épocas remotas.</p>
                            <p>El cerdo ibérico ha formado parte del paisaje de Extremadura desde la más remota antigüedad. Los romanos eran expertos ganaderos de la dehesa, 
                            así como elaboradores de perniles conservados en sal.</p>
                            <p>Esta tradición se ha mantenido a lo largo de la historia, conservando y mejorando una raza que constituye un auténtico tesoro genético, un animal 
                            perfectamente adaptado al ecosistema de la Dehesa, que obra el milagro, gracias a su particular metabolismo, de transformar los pastos y las bellotas 
                            de los que se alimenta en uno de los productos naturales más sanos y exquisitos que puedan apreciarse: el jamón ibérico de bellota, que aparte de ser 
                            un alimento sano y un manjar gastronómico, es uno de los máximos exponentes del saber hacer, de la tradición y la alegría de vivir de todo un pueblo.</p>
			 				</div>
                            
						</div>
					</div>
					<div class="col-hd-6 col-sm-6">
						<div class="selectos-info">
							<img src="images/selectos-01.png">
							<small>La tradición unida al culto del Jamón Ibérico.</small>
							<i class="icon-line"></i>
                            
                            <div align="left" style="padding:15px; color:#FFF">
                            <p>El Consejo Regulador de la Denominación de Origen "Dehesa de Extremadura", comienza su andadura la primavera de 1990 (D.O.E. 30/5/90), siendo 
                            ratificado por el Ministerio de Agricultura posteriormente (B.O.E. 2/7/90).</p>
                            <p>La Unión Europea reconoció en Junio de 1996 a Dehesa de Extremadura como Denominación de Origen Protegida (DOP) avalando de esta forma a nivel 
                            comunitario el prestigio y la calidad de los jamones y paletas ibéricas acogidas por nuestro Consejo Regulador.</p>
                            <p>Nuestra Denominación de Origen ha desarrollado una labor constante de mejora y control de los productos ibéricos acogidos a la misma, para 
                            garantizar al consumidor que cuando adquiere un jamón o paleta con nuestra etiqueta, esta adquiriendo un producto natural con toda garantía.</p></br>
                            <p></p>
			 				</div>
						</div>
					</div>
						
					</div>
				</div>
			</div>
		</div>
		<!-- Banner Board. End -->
	</main>
<?php $scripts = '
	<!-- Slider Revolution -->
	<script src="plugins/revolution-slider/jquery.themepunch.tools.min.js"></script>
	<script src="plugins/revolution-slider/jquery.themepunch.revolution.min.js"></script>

	<!-- Owl Carousel -->
	<script src="plugins/owl-carousel/owl.carousel.js"></script>

	<script src="css-js/slider-main.js"></script>
'; ?>
<?php include('includes/footer.php'); ?>