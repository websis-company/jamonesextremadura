
	<!-- <div class="images" data-toggle="modal" data-target="#productSingleModal">
			Prueba
	</div> -->
	<!-- Modal - Imágenes del producto. Begin -->
	<div id="productSingleModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog product-modal">
			<div class="modal-content">
				<div class="modal-header">
					<h2 class="modal-title">Nombre del producto</h2>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>

				<div class="modal-body">
					<div class="product-modal-image">
						<img id="productSingleModalImage" src="images-product/01-detail-01.jpg">

						<div class="product-modal-thumbnails">
							<img src="images-product/01-detail-01.jpg">
							<img src="images-product/01-detail-02.jpg">
							<img src="images-product/01-detail-03.jpg">
							<img src="images-product/01-detail-04.jpg">
						</div>
					</div>

					<div class="product-modal-info">
						<h3 class="product-modal-title">Nombre del producto</h3>

						<div class="product-modal-price">
							<span class="price-new">$ 398.00</span>
						</div>

						<span class="button-wrapper margin-b-20">
							<a href="productos-detalle" class="button-flat secondary">COMPRAR</a>
						</span>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia ipsum recusandae provident laboriosam, dolorum commodi incidunt rem, assumenda sed doloremque magnam delectus obcaecati libero praesentium similique unde explicabo, rerum pariatur.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Modal - Imágenes del producto. End -->