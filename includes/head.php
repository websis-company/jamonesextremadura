<!DOCTYPE html>
<html lang="es" xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<head>
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js";></script>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->
	<meta charset="UTF-8">
	<? $header_title = $header_title ? $header_title : 'Mandara Residencial';?>
	<title><?=$header_title;?></title>
	<meta name="description" content="<?= $header_description ?>">
	<meta name="robots" content="index,follow">
	<meta name="robots" content="noodp,noydir">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.5">
	<meta name="format-detection" content="telephone=no">
	<meta name="skype_toolbar" content="skype_toolbar_parser_compatible">
	<!-- <meta name="theme-color" content="#2c3137"> -->

	<!-- Rutas -->
	<base href="<?=$base_url?>">
	<link rel="canonical" href="<?=$base_url?>">
	<link rel="shortcut icon" href="favicon.ico">

	<!-- CSS Generales -->
	<link rel="stylesheet" href="bootstrap/bootstrap.css">
	<link rel="stylesheet" href="css-js/styles.css">
	<link rel="stylesheet" href="plugins/dlmenumovil/dlmenumovil.css">
	<link rel="stylesheet" href="https://file.myfontastic.com/UKbWzGnSfCqNGQq32qomn7/icons.css">

	<!-- CSS de Plugins -->
	<link rel="stylesheet" href="plugins/font-awesome/font-awesome.css">
	<!--[if lt IE 9]>
		<script src="css-js/respond.js"></script>
	<![endif]-->