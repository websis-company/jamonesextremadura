
	<footer>
		<div style="background-color: white;">
			<section class="bg-parallax-top" style="background-image: url(images/images_bg-contacto.jpg);">
					<div class="re-central">
						<h2 class="font-secondary text-center">CONTACTO</h2>
						<div class="row padding-v-100 contacto-container">
							<div class="col-md-4" data-height="siblings">
								<p class="text-center font-primario">Llene el siguiente formulario para recibir más información</p>
								<i class="icon-line font-15"></i>
								<!-- Formulario de contacto. Begin -->
								<form action="" method="post" id="formContacto" name="formContacto" enctype="multipart/form-data" class="form-general">
									<!-- <legend class="font-secundario">Contacto</legend> -->
									<div>
										<label for="Nombre" class="sr-only">Nombre <span>*</span></label>
										<input type="text" id="Nombre" name="Centity[Nombre]" value="" placeholder="Nombre *">
									</div>
									<div class="">
										<label for="Telefono" class="sr-only">Teléfono</label>
										<input type="tel" id="Telefono" name="Centity[Telefono]" value="" placeholder="Teléfono">
									</div>
									<div class="">
										<label for="Email" class="sr-only">Email <span>*</span></label>
										<input type="email" id="Email" name="Centity[Email]" value="" placeholder="Email *">
									</div>
									<div>
										<label for="Comentario" class="sr-only">Comentarios <span>*</span></label>
										<textarea id="Comentario" name="Centity[Comentarios]" placeholder="Comentarios *"></textarea>
									</div>
									<div class="send text-center">
										<a type="submit" id="EnviarContact" name="Enviar" value="Enviar" class="button-line">ENVIAR</a>
									</div>
								</form>
								<!-- Formulario de contacto. End -->
							</div>
							
                            <div class="col-md-4 contacto-container">
								<p>Surtimos pedidos a toda la República.</p>
								<p>¿Tienes un evento y requieres un servicio original? <br>Ponte en contacto con nosotros.</p>
								<address style="text-align:left">
									<span>Llámanos:</span>
									<a href="tel:+52 222 273 8310" class="llamar">Oficina: 01 (222) 583 2071 </a>
									<a href="tel:+52 222 273 8324" class="llamar">Movil-1: (045) 22 21 70 72 94</a>
									<a href="tel:+52 222 273 8300" class="llamar">Movil-2: (045) 22 22 03 11 64</a>
									<span>Visítanos:</span>
									<p class="">
										<a >SHOWROOM. Calle Turquesa No. 3935 Col. <br>Villa Posadas. C.P. 72060.Puebla, Pue.</a>
									</p>
									<span>Síguenos: </span>
									<a href=""><i class="icon-facebook"></i></a>
								</address>
							</div>
                            
                            <div class="col-md-4 ">
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3770.8730860710293!2d-98.22396398466857!3d19.069316857136894!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85cfc6ccae50cdd7%3A0x4c265ccef9b0747f!2sAv+Turquesa+3935%2C+Villa+Posadas%2C+72060+Puebla%2C+Pue.!5e0!3m2!1ses-419!2smx!4v1497377299429" 
                                width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
							</div>
                            
						</div>
					</div>
				</section>
		</div>
		<div class="principal-footer" style="background-color: black;">
			<div class="re-cien">
				<div class="row">
					<div class="col-bs-12">
						<div class="footer-nav">
							<div class="col-bs-9">
								<a href="#" class="footer-nav-link">Jamones Extremadura</a>
								<a href="#" class="footer-nav-link">Denominación de Origen</a>
								<a href="#" class="footer-nav-link">Jamones</a>
								<a href="#" class="footer-nav-link">Paletas</a>
								<a href="#" class="footer-nav-link">Embutidos</a>
								<a href="#" class="footer-nav-link">Eventos</a>
							</div>
							<div class="col-bs-3">
								<a href="#" class="footer-nav-link button-line secondary">Catálogo</a>
							</div>
						</div>	
					</div>
				</div>
			</div>
		</div>

		<div class="post-footer">
			<div class="re-central">
				<div class="row">
					<div class="col-md-6 legal">
						<span>Copyright © 2017 Jamones Extremadura. Todos los derechos reservados</span>
					</div>
					<div class="col-md-6">
						<span class="developed-by">
							<a href="http://vicom.mx" target="_blank">
								<img src="images/layout/vicom.png" alt="VICOM">
							</a>
						</span>
					</div>
				</div>
			</div>
		</div>
	</footer>

	<!-- Go to top. Begin -->
	<div class="go-to-top">
		<i class="fa fa-angle-up"></i>
	</div>
	<!-- Go to top. End -->

	<!-- JS Generales -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="css-js/jquery.min.js"><\/script>')</script>
	<script src="bootstrap/bootstrap.min.js"></script>
	<script src="css-js/modernizr.js"></script>
	<script src="css-js/jquery.easing.min.js"></script>
	<script src="plugins/dlmenumovil/jquery.dlmenumovil.js"></script>
	<script>
		$(function() {
			 $('#dl-menu').dlmenu({
				animationClasses : {classin : 'dl-animate-in-2', classout : 'dl-animate-out-2'}
			 });
		});
	</script>
	
	<!-- JS Core -->
	<script src="css-js/re-app.js"></script>
	<script>
		$(document).on('ready', function(){
			Reapp.init();
		});
		$(window).on('load', function(){
			Reapp.init();
		});	
	</script>
	<?php 
		/*--------------------------------------------------
		 Llamar JS que sólo se usa en la página actual
		--------------------------------------------------*/
		function scriptsofpage($scripts){
			if (isset($GLOBALS[$scripts])){
				echo 
				"<!-- JS de página. Begin -->".
				$GLOBALS[$scripts].
				"\t"."<!-- JS de página. End -->"."\n";
			}
		}
		scriptsofpage("scripts");
	?>
</body>
</html>