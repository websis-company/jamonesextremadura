
	<!-- Minicart - Trigger & Wrapper. Begin -->
	<div id="toolbar" class="toolbar-right-trigger toolbar-minicart-trigger">
		<a href="javascript:;" title="Carrito de Compras">
			<span id="cantidadRight">1</span><br>
			<i class="icon-shopping-cart"></i>
		</a>
	</div>

	<div id="cartright" class="toolbar-right-wrapper toolbar-minicart-wrapper" style="display: block;">
		<div id="divscroll" class="minicart">
			<table class="shopping-cart-table">
				<tbody class="shopping-cart-tbody">
					<tr>
						<td class="prod-act"><a href="#" class="icon-delete"></a></td>
						<td class="prod-img"><a href="#"><img src="images-product/01.jpg" alt="Nombre del producto con descripción corta"></a></td>
						<td class="prod-des">Nombre del producto con descripción corta</td>
						<td class="prod-sku">55832AFC90</td>
						<td class="prod-qua">5</td>
						<td class="prod-pri">$ 1,540.00 MXN</td>
					</tr>
					<tr>
						<td class="prod-act"><a href="#" class="icon-delete"></a></td>
						<td class="prod-img"><a href="#"><img src="images-product/01.jpg" alt="Nombre del producto con descripción corta"></a></td>
						<td class="prod-des">Nombre del producto con descripción corta</td>
						<td class="prod-sku">55832AFC90</td>
						<td class="prod-qua">5</td>
						<td class="prod-pri">$ 1,540.00 MXN</td>
					</tr>
				</tbody>

				<tfoot class="shopping-cart-tfoot">
					<tr>
						<td colspan="5"><small>El Subtotal no incluye el envío. El costo del envío será confirmado una vez que continúes con la compra.</small></td>
						<td colspan="2" class="shopping-cart-subtotal">
							<span>Subtotal</span> <strong>$ 100.00 MXN</strong>
						</td>
					</tr>

					<tr>
						<td colspan="5"></td>
						<td colspan="2" class="shopping-cart-shipping">
							<span>Envío y manejo </span> <strong>$ 150.00 MXN</strong>
						</td>
					</tr>
					<tr>
						<td colspan="5"></td>
						<td colspan="2" class="shopping-cart-total">
							<span>Total</span> <strong>$ 250.00 MXN</strong>
						</td>
					</tr>

					<tr class="shopping-cart-actions">
						<td colspan="7">
							<a href="index.php?anchor=section-ecommerce" class="button-flat secondary">Seguir comprando</a>
							<a href="cart.php" class="button-flat primary">Comprar</a>
						</td>
					</tr>
				</tfoot>
			</table>
		</div>

		<div class="button-wrapper">
			<a href="shopping-cart" title="Carrito de Compras" class="button-flat secondary">Ir al carrito de compras</a>
		</div>
	</div>
	<!-- Minicart - Trigger & Wrapper. End -->
