
</head>
<body>

<?php 
	if(isset($isHome) && $isHome == true){
		echo '<header class="site-header">';
	}
	else{
		echo '<header class="site-header site-header--internal-page">';
	}
?>
		

		<!-- Top Bar. Begin -->
		<div class="top-bar">
			<div class="re-central">
				<div class="row">
					<div class="col-bs-4 col-xxl-4">
					</div>
					<div class="col-bs-4 col-xxl-4">
					<a href="index" class="brand"></a>
					</div>
					<div class="col-bs-4 col-xxl-4 text-right">
						<button class="dl-trigger"><span></span></button>
						<ul class="re-ul-principal dl-menu">
							<li>
								"Una Joya al Paladar"
							</li>
							<li>
								<a href="#"><i class="icon-facebook icon-facebook"></i></a>
							</li>
							<li>
								<a href="#"><i class="icon-twitter icon-twitter"></i></a>
							</li>
							<li>
								<a href="#"><i class="icon-instagram icon-instagram font-7"></i></a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- Top Bar. End -->
