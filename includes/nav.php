		<!-- Nav. Begin -->
		<nav class="site-nav menu-onepage" data-onepage-animation-speed="750">
			<div class="re-central re-nav-color">
				<div id="dl-menu" class="dl-menuwrapper col-bs-9">
					<button class="dl-trigger"><span></span></button>
					<ul class="re-ul-principal dl-menu">
						<li class="mega-menu">
							<a href="empresa.php">Empresa</a>
					  </li>
						<li class="mega-menu">
							<a href="denominacion.php">Denominación de Origen</a>
							<ul class="dl-submenu tours-categories">
								<li class="tours-category">
									<div class="">
										<a href="denominacion.php">Naturaleza</a> 
									</div>
								</li>
								<li class="tours-category">
									<div class="">
										<a href="denominacion-origen.php">Origen & Tradición</a> 
									</div>
								</li>
							</ul>
						</li>
						<li class="mega-menu primero">
							<a href="#">Jamones</a>
						</li>
						<li>
							<a href="#">Paletas</a>
						</li>
						<li class="">
							<a href="#">Embutidos</a>
						</li>
						<li class="mega-menu segundo">
							<a href="#">Eventos</a>
							<ul class="dl-submenu tours-categories">
								<li class="tours-category">
									<div class="">
										<a href="">Paquetes</a> 
									</div>
								</li>
							</ul>
						</li>
					</ul>
				</div>
				<div id="dl-menu" class="dl-menuwrapper col-bs-3">
					<button class="dl-trigger"><span></span></button>
					<ul class="re-ul-principal dl-menu">
						<li class="">
							<a href="#" class="font-white">Catálogo</a>
						</li>
                        
                        <li class="mega-menu segundo">
							<a href="#" class="font-white">Venta Mayoreo</a>
                            <ul class="dl-submenu" style="background-color: rgba(11, 11, 11, 0.7); font-size: 1rem; font-weight: 200; padding-bottom: 1em; padding-top: 1em; position: absolute; text-align: center; top: 92%; width: 55%;">
								<li class="tours-category">
									<div class="">
										<small style="color:#FFF">LLÁMANOS para más información. </small> 
									</div>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- Nav. End -->
	</header>

