<?php include_once('includes/parametros.php'); ?>
<?php
	$header_title = 'Jamones Extremadura';
	$header_description = 'Jamones Extremadura';
?>
<?php 
	$isHome = true;
?>
<?php include('includes/head.php'); ?>
	<!-- Slider Revolution -->
	<link rel="stylesheet" href="plugins/revolution-slider/settings.css">
	<!-- Owl Carousel -->
	<link rel="stylesheet" href="plugins/owl-carousel/owl.carousel.css">
	<link rel="stylesheet" href="plugins/owl-carousel/owl.transitions.css">
	<link rel="stylesheet" href="plugins/owl-carousel/owl-multi.css">
<?php include('includes/header.php'); ?>
<?php include('includes/nav.php'); ?>
	<main role="main">
		<!-- Main slider. Begin -->
		
		
		<div class="re-cien box-reset shoes-genre-2 bg-contain-100" style="background-image: url(images/images_bg-empresa.jpg);">
			<div class="row"> 
				<div class="col-sm-12 margin-v-150">
					<div class="row slogan-end">
						<img src="images/empresa-ico.png">
						<small>Somos una empresa mexicana </small>
						<span>con sede en la ciudad de Puebla</span>
					</div>
				</div>
			</div>
		</div>
		<section id="section-marcas" class="re-central box-reset">
			<div class="row padding-b-100" style="background-color: white;">
				<div class="col-md-12 text-center padding-v-50">
					<h2 class="title-jamon text-center">JAMONES <br>EXTREMADURA</h2>
					<i class="icon-line font-30 font-fourth"></i>
                    
                    
                    <div class="row">
						<div class="col-md-5 ">
                        <img src="images/img-empresa.png" width="70%">
                        </div>
                        
                        <div class="col-md-6" style="padding:45px" align="left">
                        <h6>Somos una empresa mexicana con sede en la ciudad de Puebla, dedicados a la importación, 
                        comercialización y distribución de empaquetados al alto vacío, embutidos, paletas, y piernas de 
                        Jamón Ibérico de la más alta calidad, 100% de origen español y 100% Puro de Bellota.</h6></br></br>
                        
                        <p>
                        Nuestra empresa nace con la finalidad de ofrecer al mercado mexicano el máximo sabor y la calidad de nuestros 
                        productos a precios justos, ya que debido a la naturaleza gourmet de este tipo de jamón; son muy pocos los sitios 
                        en donde puede ser adquirido y por ende, su costo es muy elevado.</p>
                        <p>
                        Por ello, le invitamos a comprobar el sabor y la calidad que a lo largo de generaciones le han dado la fama internacional 
                        al jamón de cerdo ibérico de pata negra.
                        </p>
                        
                        </div>
                        
                        
                   </div>     
                    
				</div>

				<div class="clearfix"></div>

				
			</div>
		</section>
		
		<!-- Banner Board. End -->
	</main>
<?php $scripts = '
	<!-- Slider Revolution -->
	<script src="plugins/revolution-slider/jquery.themepunch.tools.min.js"></script>
	<script src="plugins/revolution-slider/jquery.themepunch.revolution.min.js"></script>

	<!-- Owl Carousel -->
	<script src="plugins/owl-carousel/owl.carousel.js"></script>

	<script src="css-js/slider-main.js"></script>
'; ?>
<?php include('includes/footer.php'); ?>