<?php include_once('includes/parametros.php'); ?>
<?php
	$header_title = 'Jamones Extremadura';
	$header_description = 'Jamones Extremadura';
?>
<?php 
	$isHome = true;
?>
<?php include('includes/head.php'); ?>
	<!-- Slider Revolution -->
	<link rel="stylesheet" href="plugins/revolution-slider/settings.css">
	<!-- Owl Carousel -->
	<link rel="stylesheet" href="plugins/owl-carousel/owl.carousel.css">
	<link rel="stylesheet" href="plugins/owl-carousel/owl.transitions.css">
	<link rel="stylesheet" href="plugins/owl-carousel/owl-multi.css">
<?php include('includes/header.php'); ?>
<?php include('includes/nav.php'); ?>
	<main role="main">
		<!-- Main slider. Begin -->

		<div class="re-cien box-reset shoes-genre-2 bg-contain-bottom" style="background-image: url(images/images_bg-origen1.jpg);">
            <div align="center">
            <h2 style="padding-top:150px" class="title-jamon text-center">JAMÓN IBÉRICO PATA NEGRA, 100% PURO DE BELLOTA</h2>
            <h4>Obtenido de las extremidades posteriores del cerdo ibérico.</h4>
			<i class="icon-line font-30 font-fourth"></i> 
			</div>
            
            <section id="section-marcas" class="re-central box-reset">
			<div class="row padding-b-0" style="background-color: white;">
				<div class="col-md-12 text-center padding-v-50">
					                    
                    <div class="row">
						<div class="col-md-5 ">
                        <img src="images/img-jamon-iberico.jpg" width="80%">
                        </div>
                        
                        <div class="col-md-7" style="padding:45px" align="left">
                        <h6>El Cerdo Ibérico es la última raza porcina de pastoreo en Europa; De origen africano y que ha habitado el mediterráneo desde hace miles de años. 
                        Destaca por su capacidad de acumular grasa bajo su piel y de infiltrarla en sus músculos, lo que hace que los productos 
                        elaborados con su carne sean tan exclusivos.</h6></br></br>
                        
                        <p>
                        Éstos cerdos son alimentados exclusivamente con frutos silvestres, hierbas aromáticas y bellotas durante el periodo de caída de bellota conocida como la "montanera", 
                        campeando por la dehesa. Provocando que debido al ejercicio que realiza en la búsqueda de su alimento, se retarde su engorde y su grasa se infiltre aún más entre sus 
                        masas musculares. Dando como resultado jamones de una jugosidad, aroma y sabor incomparables, que junto con un cuidadoso y elaborado proceso de preparación dan a 
                        nuestros productos la máxima calidad.</p>
                        <p>
                        <p>Para que un jamón o paleta se considere ibérica es necesario que como mínimo un 75% de su pureza sea originaria de la Raza Negra. Las características de los jamones 
                        ibéricos tienen que ver directamente con la pureza de la raza de los cerdos de los que provienen, y siempre se trata de animales cuya cría se realiza en régimen extensivo 
                        de libertad en dehesas arboladas en las que se pueden mover ampliamente realizando ejercicio físico.</p>
                        
                        <p>La alimentación de los cerdos en el periodo de montanera, también diferenciará el resto las cualidades del jamón ibérico, así como la curación del jamón que se desarrollará 
                        durante un periodo que oscila entre los 24 y los 40 meses, en función del tamaño de la pieza y de la cantidad de bellota que haya ingerido el cerdo antes de su sacrificio.</p>

                        </p>
                        
                        </div>
                        
                        
                   </div>     
                    
				</div>

				<div class="clearfix"></div>

				
			</div>
		</section>
            
            
            <div class="row"> 
				<div class="col-hd-12 selectos-contain padding-v-50">
					<div class="col-hd-6 col-sm-6">
						<div class="selectos-info">
							<img src="images/jamon02.png">
							<small>Jamones con Denominación de Origen<br> "Dehesa de Extremadura"</small>
							<i class="icon-line"></i>
                            
                            <div align="left" style="padding:15px; color:#FFF">
                            <p>Algunas regiones con tradición de elaboración de jamones crearon, junto con el Ministerio de Medio Ambiente y Medio Rural y Marino, las Denominaciones de Origen, que exigen y 
                            controlan que los jamones ibéricos cumplan determinadas características para poder llevar su sello de calidad.</p>
                             <p></p><br> <p></p><br><br>
			 				</div>
                            
						</div>
					</div>
					<div class="col-hd-6 col-sm-6">
						<div class="selectos-info">
							<img src="images/jamon01.png">
							<small>Las denominaciones de origen reconocidas del cerdo ibérico son:</small>
							<i class="icon-line"></i>
                            
                            <div align="left" style="padding:15px; color:#FFF">
                            <p>Jamón Ibérico D.O. Jamón de Guijuelo, Jamón Ibérico D.O. Jamón de Huelva, Jamón Ibérico D.O.P. Los Pedroches y Jamón Ibérico D.O. Dehesa de Extremadura.</p>
                            <p>Las denominaciones de origen están protegidas legalmente por el Reglamento Europeo (CE) nº 510/2006 del Consejo de la Unión Europea.</p>
                            <p>Nuestra marca cuenta desde hace muchos años con esta distinción de D.O. y nos llena de orgullo y placer poder ofrecer al mercado mexicano un producto con el sello de valor y calidad certificada</p>
                            </p>
                            <p></p>
			 				</div>
						</div>
					</div>
						
					</div>
				</div>
			</div>
		</div>
		<!-- Banner Board. End -->
	</main>
<?php $scripts = '
	<!-- Slider Revolution -->
	<script src="plugins/revolution-slider/jquery.themepunch.tools.min.js"></script>
	<script src="plugins/revolution-slider/jquery.themepunch.revolution.min.js"></script>

	<!-- Owl Carousel -->
	<script src="plugins/owl-carousel/owl.carousel.js"></script>

	<script src="css-js/slider-main.js"></script>
'; ?>
<?php include('includes/footer.php'); ?>