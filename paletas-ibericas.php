<?php include_once('includes/parametros.php'); ?>
<?php
	$header_title = 'Jamones Extremadura';
	$header_description = 'Jamones Extremadura';
?>
<?php 
	$isHome = true;
?>
<?php include('includes/head.php'); ?>
	<!-- Slider Revolution -->
	<link rel="stylesheet" href="plugins/revolution-slider/settings.css">
	<!-- Owl Carousel -->
	<link rel="stylesheet" href="plugins/owl-carousel/owl.carousel.css">
	<link rel="stylesheet" href="plugins/owl-carousel/owl.transitions.css">
	<link rel="stylesheet" href="plugins/owl-carousel/owl-multi.css">
<?php include('includes/header.php'); ?>
<?php include('includes/nav.php'); ?>
	<main role="main">
		<!-- Main slider. Begin -->
		
		
		<div class="re-cien box-reset shoes-genre-2 bg-contain-100" style="background-image: url(images/img-superior-paleta.jpg);">
			<div class="row"> 
				<div class="col-sm-12 margin-v-150">
					<div class="row slogan-end">
						<img src="images/paleta-ico.png">
						<small>Jamón 100% Puro de Bellota</small>
						<span>Puro de Bellota</span>
					</div>
				</div>
			</div>
		</div>
		<section id="section-marcas" class="re-central box-reset">
			<div class="row padding-b-100" style="background-color: white;">
				<div class="col-md-12 text-center padding-v-50">
					<h2 class="title-jamon text-center">NUESTRAS PALETAS IBÉRICAS</h2>
                    <h4>Jamón 100% Puro de Bellota</h4>
					<i class="icon-line font-30 font-fourth"></i>
                    
                    
                    <div class="row">
						<div class="col-md-5 ">
                        <img src="images/img-paleta.jpg" width="70%">
                        </div>
                        
                        <div class="col-md-6" style="padding:45px" align="left">
                        <h6>Se obtienen de las extremidades delanteras del Cerdo Ibérico.</h6></br></br>
                        
                        <p>Proceden de cerdos criados específicamente en la dehesa, se alimenta comúnmente de hierbas y piensos la mayor parte del año; y durante el periodo conocido 
                        como la montanera, que se da entre los meses de noviembre y marzo, se alimenta exclusivamente de bellotas frescas de la dehesa y pastos, alcanzando con ello 
                        el peso ideal para que el animal pueda ser sacrificado. </p>
                        
                        </div>
                        
                        
                   </div>     
                    
				</div>

				<div class="clearfix"></div>

				
			</div>
		</section>
		
		<!-- Banner Board. End -->
	</main>
<?php $scripts = '
	<!-- Slider Revolution -->
	<script src="plugins/revolution-slider/jquery.themepunch.tools.min.js"></script>
	<script src="plugins/revolution-slider/jquery.themepunch.revolution.min.js"></script>

	<!-- Owl Carousel -->
	<script src="plugins/owl-carousel/owl.carousel.js"></script>

	<script src="css-js/slider-main.js"></script>
'; ?>
<?php include('includes/footer.php'); ?>