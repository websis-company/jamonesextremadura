<?php include_once('includes/parametros.php'); ?>
<?php
	$header_title = 'Jamones Extremadura';
	$header_description = 'Jamones Extremadura';
?>
<?php 
	$isHome = true;
?>
<?php include('includes/head.php'); ?>
	<!-- Slider Revolution -->
	<link rel="stylesheet" href="plugins/revolution-slider/settings.css">
	<!-- Owl Carousel -->
	<link rel="stylesheet" href="plugins/owl-carousel/owl.carousel.css">
	<link rel="stylesheet" href="plugins/owl-carousel/owl.transitions.css">
	<link rel="stylesheet" href="plugins/owl-carousel/owl-multi.css">
<?php include('includes/header.php'); ?>
<?php include('includes/nav.php'); ?>
	<main role="main">
		<!-- Main slider. Begin -->
		
		
		<div class="re-cien box-reset shoes-genre-2 bg-contain-100" style="background-image: url(images/images_bg-empresa.jpg);">
			<div class="row"> 
				<div class="col-sm-12 margin-v-150">
					<div class="row slogan-end">
						<img src="images/empresa-ico.png">
						<small>DENOMINACIÓN DE ORIGEN (D.O.)</small>
						<span>Calidad Certificada</span>
					</div>
				</div>
			</div>
		</div>
		<section id="section-marcas" class="re-central box-reset">
			<div class="row padding-b-50" style="background-color: white;">
				<div class="col-md-12 text-center padding-v-50">
					<h2 class="title-jamon text-center">DENOMINACIÓN DE ORIGEN (D.O.</h2>
					<i class="icon-line font-30 font-fourth"></i>
                    
                    
                    <div class="row">
						<div class="col-md-5 ">
                        <img src="images/img-denominacion.jpg" width="70%">
                        </div>
                        
                        <div class="col-md-6" style="padding:45px" align="left">
                        <h6>La certificación de un producto implica un trabajo integral desde el principio hasta el final de nuestros procesos: 
                        desde la supervisión del cerdo en las dehesas, pasando por las auditorías en matadero, secadero y bodega, así como antes 
                        de su salida al mercado, rubricado con la colocación de la contraetiqueta del Consejo.</h6></br></br>
                        
                        <p>
                        Nuestra empresa nace con la finalidad de ofrecer al mercado mexicano el máximo sabor y la calidad de nuestros 
                        productos a precios justos, ya que debido a la naturaleza gourmet de este tipo de jamón; son muy pocos los sitios 
                        en donde puede ser adquirido y por ende, su costo es muy elevado.</p>
                        <p>
                        Por ello, le invitamos a comprobar el sabor y la calidad que a lo largo de generaciones le han dado la fama internacional 
                        al jamón de cerdo ibérico de pata negra.
                        </p>
                        
                        </div>
                        
                        
                   </div>     
                    
				</div>

				<div class="clearfix"></div>

				
			</div>
		</section>
        
        
        <section id="section-marcas" class="re-central box-reset">
			<div class="row padding-b-80" style="background-color: white;">
				<div class="col-md-12 text-center padding-v-50">
					<h2 class="title-jamon text-center">CALIDAD <br>CERTIFICADA</h2>
					<i class="icon-line font-30 font-fourth"></i>
				</div>

				<div class="clearfix"></div>

				<div class="brands-home-wrapper">
					<div class="row">
						<div class="col-sm-6 col-lg-4 brand-item brand-enginia">
						<a href="#"><img src="images/images-proceso1.jpg" class="brand-item-image-gray"></a>
							<a href="#"><img src="images/images-proceso-hover1.jpg" class="brand-item-image-color"></a>
							<figcaption class="brand-item-caption-wrapper">
								<div class="brand-item-caption">
									
                                    <h4>Control de Campo</h4>
                                    
									<p>Los técnicos inspectores, ante la solicitud del ganadero, y tras la comprobación de los registros y documentos legales exigidos 
                                    por la administración, verifican la raza, edad y peso de los cerdos, así como el número máximo de animales que puede engordar la 
                                    explotación en régimen de montanera.</p>
                                    
                                    <p>Se identifican cada uno de los animales aptos mediante un crotal metálico numerado.</p>
                                    <p>A partir de este momento, los servicios técnicos realizan tantas visitas a la explotación como consideren oportunas.</p>
                                    <p>Perderán el amparo del Consejo regulador, aquellos animales que no cumplan con las exigencias del mismo, llegando a 
                                    matadero sólo aquellos que finalicen su fase de engorde correctamente.</p>
 									
								</div>
							</figcaption>
						</div>
						<div class="col-sm-6 col-lg-4 brand-item brand-sikla">
							<a href="#"><img src="images/images-proceso2.jpg" class="brand-item-image-gray"></a>
							<a href="#"><img src="images/images-proceso-hover2.jpg" class="brand-item-image-color"></a>
							<figcaption class="brand-item-caption-wrapper">
								<div class="brand-item-caption">
									<h4>Control del Proceso</h4>
									<p>Una vez verificado en el matadero la procedencia, numeración de crotales y reposición de la partida de cerdos, se colocará a cada jamón 
                                    y paleta un precinto plástico numerado.</p>
                                    
                                    <p>Se auditará por parte de los servicios técnicos del Consejo tanto la fase de matadero como cada una de las fases de perfilado, salado, 
                                    asentamiento, secado, maduración y envejecimiento en bodega.</p>
                                    
                                    <p>Tanto en el matadero, como en cualquiera de las fases posteriores, se podrán descalificar las piezas que no cumplan los requisitos de calidad 
                                    estipulados, retirándole el precinto que las identifica.</p>
									
								</div>
							</figcaption>
						</div>
						<div class="col-sm-6 col-lg-4 brand-item brand-sikla">
							<a href="#"><img src="images/images-proceso3.jpg" class="brand-item-image-gray"></a>
							<a href="#"><img src="images/images-proceso-hover3.jpg" class="brand-item-image-color"></a>
							<figcaption class="brand-item-caption-wrapper">
								<div class="brand-item-caption">
									
                                    <h4>Control del Producto Final</h4>
                                    
									<p>Pasado el tiempo de maduración de la pieza, el industrial solicita al Consejo la auditoría que avale la salida de cada pieza al mercado.</p>
                                    <p>Los técnicos inspectores se desplazan a la bodega, realizando la auditoría del 100 % de las piezas, comprobando la numeración del precinto, 
                                    así como la edad y estado de maduración de la pieza, y procediendo a calar cada jamón y paleta seleccionados por el industrial como aptos en su 
                                    sistema de trazabilidad, y siendo esta la única forma de detectar el aroma.</p>
                                    <p>Tras el proceso de cala, se coloca la contraetiqueta numerada (ver imagen) a aquellas piezas que mantengan la calidad que exige Dehesa de 
                                    Extremadura, y desechando las no aptas.Así pues, Para que un jamón o paleta salga al mercado con la garantía y el aval de Dehesa de Extremadura, 
                                    debe ir con el precinto colocado en matadero y la contraetiqueta colocada al final del proceso de maduración.</p>
                                    
								</div>
							</figcaption>
						</div>
					
						
					</div>
				</div>
			</div>
		</section>
		
		<!-- Banner Board. End -->
	</main>
<?php $scripts = '
	<!-- Slider Revolution -->
	<script src="plugins/revolution-slider/jquery.themepunch.tools.min.js"></script>
	<script src="plugins/revolution-slider/jquery.themepunch.revolution.min.js"></script>

	<!-- Owl Carousel -->
	<script src="plugins/owl-carousel/owl.carousel.js"></script>

	<script src="css-js/slider-main.js"></script>
'; ?>
<?php include('includes/footer.php'); ?>