<?php include_once('includes/parametros.php'); ?>
<?php
	$header_title = 'Jamones Extremadura';
	$header_description = 'Jamones Extremadura';
?>
<?php 
	$isHome = true;
?>
<?php include('includes/head.php'); ?>
	<!-- Slider Revolution -->
	<link rel="stylesheet" href="plugins/revolution-slider/settings.css">
	<!-- Owl Carousel -->
	<link rel="stylesheet" href="plugins/owl-carousel/owl.carousel.css">
	<link rel="stylesheet" href="plugins/owl-carousel/owl.transitions.css">
	<link rel="stylesheet" href="plugins/owl-carousel/owl-multi.css">
<?php include('includes/header.php'); ?>
<?php include('includes/nav.php'); ?>
	<main role="main">
		<!-- Main slider. Begin -->
		<section class="main-slider-fullscreen">
			<div class="container-slider-mobile-portrait">
				<div class="slider-mobile-portrait">
					<ul>
						<li data-transition="slideleft" data-slotamount="1" data-masterspeed="200">
							<img src="images-slider/slider-01-portrait.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
						</li>
					</ul>
				</div>
			</div>
			<div class="container-slider-mobile-landscape">
				<div class="slider-mobile-landscape">
					<ul>
						<li data-transition="slideleft" data-slotamount="1" data-masterspeed="200">
							<img src="images-slider/slider-01.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
							<div>
								<div class="conten-text-slider">
									<!--<div class="info">
										<h2 class="text">
										<small>UNA</small> JOYA <br><small>AL</small> PALADAR
										</h2>
										<i class="icon-line icon-line"></i>
									</div>-->
								</div>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</section>
		<div class="">
			<div class="re-cien box-reset services-genre">
			<div class="row">
				
                
				<div class="col-md-4 services-genre-item">
					<div class="services-genre-description">
						<span class="tag-subtitle">
							<img src="images/servicios-02.png" alt="Empaque">
							<strong>Empaquetados</strong>
							<small>de 100 grs.</small>
						</span>
						<p>
							En sobres y charolas listas para disfrutar
						</p>
						<a href="#" title="Ver más" class="button-line">Ver más</a>
					</div>
					<img src="images/home-empaques.jpg" alt="Empaque" class="img-cien">
				</div>
                
                
				<div class="col-md-4 services-genre-item">
					<div class="services-genre-description">
						<span class="tag-subtitle">
							<img src="images/servicios-03.png" alt="Corte">
							<strong>¿Cómo cortar </strong>
							<small>un jamón?</small>
						</span>
						<p>
							Porque saberlo cortar es saberlo disfrutar
						</p>
						<a href="#" title="Ver más" class="button-line">Ver más</a>
					</div>
					<img src="images/home-corte.jpg" alt="Corte" class="img-cien">
				</div>	
                
                
				<div class="col-md-4 services-genre-item">
					<div class="services-genre-description">
						<span class="tag-subtitle">
							<img src="images/servicios-01.png" alt="Eventos">
							<strong>Servicio para</strong>
							<small>Eventos</small>
						</span>
						<p>
							Paquetes para Sociales y Empresariales
						</p>
						<a href="#" title="Ver más" class="button-line">Ver más</a>
					</div>
					<img src="images/home-eventos.jpg" alt="Eventos" class="img-cien">
				</div>
                
                
                
			</div>
			</div>
		</div>
		<div class="re-cien box-reset shoes-genre-2 bg-contain-100" style="background-image: url(images/images_bg-copy-01.jpg);">
			<div class="row"> 
				<div class="col-sm-12 margin-v-150">
					<div class="row slogan-end">
						<img src="images/home-pig.png">
						<small>UN REGALO DE SELECCIÓN</small>
						<span>Porque sólo lo bueno se comparte</span>
					</div>
				</div>
			</div>
		</div>
		<section id="section-marcas" class="re-central box-reset">
			<div class="row padding-b-100" style="background-color: white;">
				<div class="col-md-12 text-center padding-v-50">
					<h2 class="title-jamon text-center">PRODUCTOS <br>DESTACADOS</h2>
					<i class="icon-line font-30 font-fourth"></i>
				</div>

				<div class="clearfix"></div>

				<div class="brands-home-wrapper">
					<div class="row">
						<div class="col-sm-6 col-lg-4 brand-item brand-enginia">
						<a href="#"><img src="images-product/images_catalogo-01.jpg" class="brand-item-image-gray"></a>
							<a href="#"><img src="images-product/images_catalogo-01-hover.jpg" class="brand-item-image-color"></a>
							<figcaption class="brand-item-caption-wrapper">
								<div class="brand-item-caption">
									<h4>Paleta Ibérica de Bellota</h4>
									<p>Paleta Ibérica de Bellota con peso de 5 kg. y 36 meses de maduración.</p>
									<ul class="brand-item-features">
										<li>Con Base Jamonero $5,900</li>
										<li>Sin Base Jamonero $5,100</li>
									</ul>
								</div>
							</figcaption>
						</div>
						<div class="col-sm-6 col-lg-4 brand-item brand-sikla">
							<a href="#"><img src="images-product/images_catalogo-02.jpg" class="brand-item-image-gray"></a>
							<a href="#"><img src="images-product/images_catalogo-02-hover.jpg" class="brand-item-image-color"></a>
							<figcaption class="brand-item-caption-wrapper">
								<div class="brand-item-caption">
									<h4>Paleta Ibérica de Bellota</h4>
									<p>Paleta Ibérica de Bellota con peso de 5 kg. y 36 meses de maduración.</p>
									<ul class="brand-item-features">
										<li>Con Base Jamonero $5,900</li>
										<li>Sin Base Jamonero $5,100</li>
									</ul>
								</div>
							</figcaption>
						</div>
						<div class="col-sm-6 col-lg-4 brand-item brand-sikla">
							<a href="#"><img src="images-product/images_catalogo-03.jpg" class="brand-item-image-gray"></a>
							<a href="#"><img src="images-product/images_catalogo-03-hover.jpg" class="brand-item-image-color"></a>
							<figcaption class="brand-item-caption-wrapper">
								<div class="brand-item-caption">
									<h4>Paleta Ibérica de Bellota</h4>
									<p>Paleta Ibérica de Bellota con peso de 5 kg. y 36 meses de maduración.</p>
									<ul class="brand-item-features">
										<li>Con Base Jamonero $5,900</li>
										<li>Sin Base Jamonero $5,100</li>
									</ul>
								</div>
							</figcaption>
						</div>
						<div class="col-sm-6 col-lg-4 brand-item brand-zylentek">
							<a href="#"><img src="images-product/images_catalogo-04.jpg" class="brand-item-image-gray"></a>
							<a href="#"><img src="images-product/images_catalogo-04-hover.jpg" class="brand-item-image-color"></a>
							<figcaption class="brand-item-caption-wrapper">
								<div class="brand-item-caption">
									<h4>Paleta Ibérica de Bellota</h4>
									<p>Paleta Ibérica de Bellota con peso de 5 kg. y 36 meses de maduración.</p>
									<ul class="brand-item-features">
										<li>Con Base Jamonero $5,900</li>
										<li>Sin Base Jamonero $5,100</li>
									</ul>
								</div>
							</figcaption>
						</div>
						<div class="col-sm-6 col-lg-4 brand-item brand-sikla">
							<a href="#"><img src="images-product/images_catalogo-05.jpg" class="brand-item-image-gray"></a>
							<a href="#"><img src="images-product/images_catalogo-05-hover.jpg" class="brand-item-image-color"></a>
							<figcaption class="brand-item-caption-wrapper">
								<div class="brand-item-caption">
									<h4>Paleta Ibérica de Bellota</h4>
									<p>Paleta Ibérica de Bellota con peso de 5 kg. y 36 meses de maduración.</p>
									<ul class="brand-item-features">
										<li>Con Base Jamonero $5,900</li>
										<li>Sin Base Jamonero $5,100</li>
									</ul>
								</div>
							</figcaption>
						</div>
						<div class="col-sm-6 col-lg-4 brand-item brand-gripple">
							<a href="#"><img src="images-product/images_catalogo-06.jpg" class="brand-item-image-gray"></a>
							<a href="#"><img src="images-product/images_catalogo-06-hover.jpg" class="brand-item-image-color"></a>
							<figcaption class="brand-item-caption-wrapper">
								<div class="brand-item-caption">
									<h4>Paleta Ibérica de Bellota</h4>
									<p>Paleta Ibérica de Bellota con peso de 5 kg. y 36 meses de maduración.</p>
									<ul class="brand-item-features">
										<li>Con Base Jamonero $5,900</li>
										<li>Sin Base Jamonero $5,100</li>
									</ul>
								</div>
							</figcaption>
						</div>
					</div>
				</div>
			</div>
		</section>
		<div class="re-cien bg-parallax-top padding-b-100 padding-t-100 text-center" style="background-image: url(images/home-grass.png);">
			</div>
		<div class="re-cien box-reset shoes-genre-2 bg-contain-bottom" style="background-image: url(images/images_bg-copy-02.jpg);">
			<div class="row"> 
				<div class="col-hd-12 selectos-contain padding-v-50">
					<div class="col-hd-6 col-sm-6">
						<div class="selectos-info">
							<img src="images/selectos-02.png">
							<small>Productos Ibéricos Selectos</small>
							<span>Embutidos Loncheados & Cañas</span>
							<a href="#" title="Ver más" class="button-line">Ver más</a>
							<i class="icon-line"></i>
						</div>
					</div>
					<div class="col-hd-6 col-sm-6">
						<div class="selectos-info">
							<img src="images/selectos-01.png">
							<small>Jamón Ibérico 36 Meses</small>
							<span>¡Delicioso, una joya al paladar!</span>
							<a href="#" title="Ver más" class="button-line">Ver más</a>
							<i class="icon-line"></i>
						</div>
					</div>
						
					</div>
				</div>
			</div>
		</div>
		<div class="re-central">
			<div class="row slogan-end padding-v-100">
				<span>PRODUCTOS IBÉRICOS SELECTOS</span>
				<i class="icon-line"></i>
			</div>
		</div>
		<!-- Banner Board. End -->
	</main>
<?php $scripts = '
	<!-- Slider Revolution -->
	<script src="plugins/revolution-slider/jquery.themepunch.tools.min.js"></script>
	<script src="plugins/revolution-slider/jquery.themepunch.revolution.min.js"></script>

	<!-- Owl Carousel -->
	<script src="plugins/owl-carousel/owl.carousel.js"></script>

	<script src="css-js/slider-main.js"></script>
'; ?>
<?php include('includes/footer.php'); ?>