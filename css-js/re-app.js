/*!
  * General Script - Iván Luna [RE]
  * ivanluna.re@outlook.com
*/

/*--------------------------------------------------------------------------------*/
// Igualar el alto entre elementos del mismo tipo
/*--------------------------------------------------------------------------------*/
var sameHeight = function(){
	$('[data-height="parent"]').each(function () {
		var parent = $(this);
		var childs = $('[data-height="siblings"]', parent);
		var alturaMayor = 0;

		// Only for products
		if(childs.hasClass('product-item')) {
			// Images
			var productImage = $('[data-height="siblings"] div.contenedor-img', parent)
			productImage.each(function()
			{
				var contenedor = $(this);
				var contenedorAncho = parseInt(contenedor.css('width'));
				var contenedorAlto = contenedorAncho * 0.80;
				contenedor.css({'height' : contenedorAlto})

				// Asignar el alto máximo de la imagen
				setTimeout(function() {
					var image = $('.img-centrada' , contenedor);
					image.each(function()
					{
						// console.log('in')
						var image = $(this);
						$(this).css({
							'max-height' : (contenedorAlto - 5),
						})

						// Calcular y asginar el top de la imagen
						setTimeout(function(){
							var imgAltura = parseInt(image.innerHeight())
							var imgPosicion = Math.round((contenedorAlto - imgAltura) / 2)
							image.css({
								'position': 'relative',
								'top' : imgPosicion,
							})

							// Hacer visible la imagen
							setTimeout(function(){
								image.css({
									'visibility' : 'visible',
									'opacity' : 1,
								})
							}, 300)
						}, 200)
					})
				}, 50);
			})

			// Captions
			var productCaption = $('[data-height="siblings"] .product-caption', parent)
			var productCaptionAlturaMayor = 0;
			productCaption.each(function(){
				if($(this).outerHeight() > productCaptionAlturaMayor){
					$(productCaption).removeAttr('style');
					productCaptionAlturaMayor = $(this).outerHeight();
				}
			})
			productCaption.css({
				minHeight: productCaptionAlturaMayor
			})

			// Prices
			var productPrice = $('[data-height="siblings"] .product-price', parent)
			var productPriceAlturaMayor = 0;
			productPrice.each(function(){
				if($(this).outerHeight() > productPriceAlturaMayor){
					$(productPrice).removeAttr('style');
					productPriceAlturaMayor = $(this).outerHeight();
				}
			})
			productPrice.css({
				minHeight: productPriceAlturaMayor
			})

			// Item container
			childs.each(function(){
				if($(this).outerHeight() > alturaMayor){
					$(childs).removeAttr('style');
					alturaMayor = $(this).outerHeight() + 1;
				}
			})
			childs.css({
				minHeight: alturaMayor
			})
		}

		// For generic items
		else if(childs.hasClass('item-generic')) {
			// Images
			var itemImage = $('[data-height="siblings"] .contenedor-img', parent)
			itemImage.each(function(){
				var contenedor = $(this);
				var contenedorAncho = parseInt(contenedor.css('width'));
				var contenedorAlto = contenedorAncho * 0.66;
				contenedor.css('height' , contenedorAlto);

				var image = $('.img-centrada' , contenedor);
				image.each(function() {
					var image = $(this);
					$(this).css('max-height', (contenedorAlto - 10) + 'px');

					var imgAltura = parseInt(image.css('height'));
					var imgPosicion = Math.round((contenedorAlto - imgAltura) / 2);
					image.css({
						'position': 'relative',
						'top': imgPosicion,
					})

					$(window).load(function() {
						image.css('visibility', 'visible').animate({opacity: 1}, 200);
					})
				})
			})

			// Titles
			var itemTitle = $('[data-height="siblings"] .item-generic-title', parent)
			var itemTitleAlturaMayor = 0;
			itemTitle.each(function(){
				if($(this).outerHeight() > itemTitleAlturaMayor){
					$(itemTitle).removeAttr('style');
					itemTitleAlturaMayor = $(this).outerHeight();
				}
			})
			itemTitle.css({
				minHeight: itemTitleAlturaMayor
			})

			// Captions
			var itemCaption = $('[data-height="siblings"] .item-generic-caption', parent)
			var itemCaptionAlturaMayor = 0;
			itemCaption.each(function(){
				if($(this).outerHeight() > itemCaptionAlturaMayor){
					$(itemCaption).removeAttr('style');
					itemCaptionAlturaMayor = $(this).outerHeight();
				}
			})
			itemCaption.css({
				minHeight: itemCaptionAlturaMayor
			})
		}

		// Only for news
		else if(childs.hasClass('news-item')) {
			// Images
			var newsImage = $('[data-height="siblings"] .contenedor-img', parent)
			newsImage.each(function(){
				var contenedor = $(this);
				var contenedorAncho = parseInt(contenedor.css('width'));
				var contenedorAlto = contenedorAncho * 0.66;
				contenedor.css('height' , contenedorAlto);

				var image = $('.img-centrada' , contenedor);
				image.each(function() {
					var image = $(this);
					$(this).css('max-height', (contenedorAlto - 10) + 'px');

					var imgAltura = parseInt(image.css('height'));
					var imgPosicion = Math.round((contenedorAlto - imgAltura) / 2);
					image.css({
						'position': 'relative',
						'top': imgPosicion,
					})

					$(window).load(function() {
						image.css('visibility', 'visible').animate({opacity: 1}, 200);
					})
				})
			})

			// Captions
			var newsCaption = $('[data-height="siblings"] .news-caption', parent)
			var newsCaptionAlturaMayor = 0;
			newsCaption.each(function(){
				if($(this).outerHeight() > newsCaptionAlturaMayor){
					$(newsCaption).removeAttr('style');
					newsCaptionAlturaMayor = $(this).outerHeight();
				}
			})
			newsCaption.css({
				minHeight: newsCaptionAlturaMayor
			})
		}

		// For containers
		else{
			childs.each(function(){
				if($(this).outerHeight() > alturaMayor){
					$(childs).removeAttr('style');
					alturaMayor = $(this).outerHeight() + 1;
				}
			})
			childs.css({
				minHeight: alturaMayor
			})
		}
	})
}

/*--------------------------------------------------------------------------------*/
// Asignar el alto de un elemento igual a su ancho
/*--------------------------------------------------------------------------------*/
var heightWidth = function(){
	$('[data-height="height-width"]').each(function (){
		var height = $(this).css('width');

		$(this).css('height', height);
	})
}

/*--------------------------------------------------------------------------------*/
// Asignar el alto en proporción 3:2
/*--------------------------------------------------------------------------------*/
var heightPhoto32 = function(){
	$('[data-height="photo-3-2"]').each(function (){
		var height = parseInt($(this).css('width'));
		var height32 = Math.floor(height*0.66) + 1;

		$(this).css('height', height32);
	})
}

var sameHeightPhoto32 = function(){
	$('[data-height="gallery-3-2"]').each(function () {
		var parent = $(this);
		var childs = $('[data-height="gallery-siblings"]', parent);
		var anchoMayor = 0;
		var alturaMayor = 0;

		childs.each(function(){
			if($(this).outerWidth() > anchoMayor){
				$(childs).removeAttr('style');
				anchoMayor = $(this).outerWidth();
			}
		})
		childs.css({
			minHeight: Math.floor(anchoMayor*0.66)
		})
	})
}

/*--------------------------------------------------------------------------------*/
// Asignar el alto igual al ViewPort
/*--------------------------------------------------------------------------------*/
var heightViewport = function(){
	$('[data-height="viewport"]').each(function () {
		var height = Reapp.getViewPort().height - $('.site-header').outerHeight();
		$(this).css('min-height', height);
	})
}

/*--------------------------------------------------------------------------------*/
// Asignar el alto igual al 50% del ViewPort
/*--------------------------------------------------------------------------------*/
var heightViewportHalf = function(){
	$('[data-height="viewport-half"]').each(function () {
		var height = (Reapp.getViewPort().height - $('.site-header').outerHeight()) / 1.75;
		$(this).css('min-height', height);
	})
}

/*--------------------------------------------------------------------------------*/
// Galería de imágenes para detalle de producto
/*--------------------------------------------------------------------------------*/
var productGallery = function(){
	// Contenedor e imagen inicial
	$('[data-image="product-gallery"]').each(function (){
		var containerImage = $(this);
		var containerImageAncho = parseInt(containerImage.css('width'));
		containerImage.css('height' , containerImageAncho);

		var image = $('.img-centrada' , containerImage);
		image.each(function() {
			var image = $(this);
			$(this).css('max-height', (containerImageAncho - 30) + 'px');

			var imgAltura = parseInt(image.css('height'));
			var imgPosicion = Math.round((containerImageAncho - imgAltura) / 2);
			image.css({
				'position': 'relative',
				'top': imgPosicion,
			})

			$(window).load(function() {
				image.css('visibility', 'visible');
			})
		})
	})

	// Miniaturas y centrado de imagen nueva
	var miniaturas = $('ul.miniaturas');
	var imgActual;
	miniaturas.on('click','li img', function(){
		var imgActual = $(this).attr('src');

		$('.product-detail-image img.img-centrada').fadeOut(100, function(){
			$(this)
			.attr('src',imgActual)
			.fadeIn(100, function(){

				$('[data-image="product-gallery"]').each(function (){
					var containerImage = $(this);
					var containerImageAncho = parseInt(containerImage.css('width'));
					containerImage.css('height' , containerImageAncho);

					var image = $('.img-centrada' , containerImage);
					image.each(function() {
						var image = $(this);
						$(this).css('max-height', (containerImageAncho - 30) + 'px');

						var imgAltura = parseInt(image.css('height'));
						var imgPosicion = Math.round((containerImageAncho - imgAltura) / 2);
						image.css({
							'position': 'relative',
							'top': imgPosicion,
						})
						image.css('visibility', 'visible');
					})
				})
			})
			.css('visibility','hidden');
		})
	})
}

/*--------------------------------------------------------------------------------*/
// Galería de imágenes 3:2
/*--------------------------------------------------------------------------------*/
var photoGallery = function(){
	// Contenedor e imagen inicial
	$('[data-image="photo-gallery"]').each(function (){
		var containerImage = $(this);
		var containerImageAncho = parseInt(containerImage.css('width'));
		var containerImageAlto = containerImageAncho * 0.66;
		containerImage.css('height' , containerImageAlto);

		var image = $('.img-centrada' , containerImage);
		image.each(function() {
			var image = $(this);
			$(this).css('max-height', (containerImageAncho - 20) + 'px');

			var imgAltura = parseInt(image.css('height'));
			var imgPosicion = Math.round((containerImageAlto - imgAltura) / 2);
			image.css({
				'position': 'relative',
				'top': imgPosicion,
			})

			$(window).load(function() {
				image.css('visibility', 'visible');
			})
		})
	})

	// Miniaturas y centrado de imagen nueva
	var miniaturas = $('ul.miniaturas');
	var imgActual;
	miniaturas.on('click','li img', function(){
		var imgActual = $(this).attr('src');

		$('.news-single-image img.img-centrada').fadeOut(100, function(){
			$(this)
			.attr('src',imgActual)
			.fadeIn(100, function(){

				$('[data-image="photo-gallery"]').each(function (){
					var containerImage = $(this);
					var containerImageAncho = parseInt(containerImage.css('width'));
					var containerImageAlto = containerImageAncho * 0.66;
					containerImage.css('height' , containerImageAlto);

					var image = $('.img-centrada' , containerImage);
					image.each(function() {
						var image = $(this);
						$(this).css('max-height', (containerImageAlto - 20) + 'px');

						var imgAltura = parseInt(image.css('height'));
						var imgPosicion = Math.round((containerImageAlto - imgAltura) / 2);
						image.css({
							'position': 'relative',
							'top': imgPosicion,
						})
						image.css('visibility', 'visible');
					})
				})
			})
			.css('visibility','hidden');
		})
	})
}

/*--------------------------------------------------------------------------------*/
// Page scroll active
/*--------------------------------------------------------------------------------*/
var pageScrollActive = function () {
	var offset = parseInt($('.site-header').attr('data-minimize-offset') > 0 ? parseInt($('.site-header').attr('data-minimize-offset')) : 0);
	var _handleHeaderOnScroll = function () {
		if ($(window).scrollTop() > offset) {
			$("body").addClass("page-scroll-on");
		} else {
			$("body").removeClass("page-scroll-on");
		}
	}

	return {
		//main function to initiate the module
		init: function () {
			_handleHeaderOnScroll();

			$(window).scroll(function () {
				_handleHeaderOnScroll();
			});
		}
	};
}();

/*--------------------------------------------------------------------------------*/
// One page nav
/*--------------------------------------------------------------------------------*/
var onePageNav = function () {

	var handle = function () {
		var offset;
		var scrollspy;
		var speed;
		var nav;

		$('body').addClass('page-scroll-on');
		offset = $('.site-header').outerHeight() - $('.top-bar').outerHeight();
		$('body').removeClass('page-scroll-on');

		if ($('.menu-onepage-dots').size() > 0) {
			if ($('.menu-onepage-link-dots-wrapper').size() > 0) {
				$('.menu-onepage-link-dots-wrapper').css('margin-top', -($('.menu-onepage-link-dots-wrapper').outerHeight() / 2));
			}
			scrollspy = $('body').scrollspy({
				target: '.menu-onepage-dots',
				offset: offset
			})
			speed = parseInt($('.menu-onepage-dots').attr('data-onepage-animation-speed'));
		} 
		else {
			scrollspy = $('body').scrollspy({
				target: '.menu-onepage',
				offset: offset
			})
			speed = parseInt($('.menu-onepage').attr('data-onepage-animation-speed'));
		}

		// El elemento necesita la clase nav de Bootstrap
		scrollspy.on('activate.bs.scrollspy', function () {
			$(this).find('.menu-onepage-link.active-anchor').removeClass('active-anchor');
			$(this).find('.menu-onepage-link.active').addClass('active-anchor');
		})

		// Desplazamiento
		$('.brand-onepage, .menu-onepage-link > a').on('click', function (e) {
			// var section = $(this).attr('href');
			var section = $(this).data('anchor');
			var href = $(this).attr('href');
			var top = 0;
			var location = window.location.pathname;

			// if (location == '/santoinfierno/' || location == '/santoinfierno/index') {
			if ( $(section).length) {
				// console.log(section)
				// console.log('existe')
				// return false;
				if (section !== "#section-home") {
					top = $(section).offset().top - offset;
				}
			}
			else {
				// console.log(section)
				// console.log('no existe')
				// return false;
				var index = '/santoinfierno/';
				window.location.href = href
			}

			$('html, body').stop().animate({
				scrollTop: top,
			}, speed, 'easeInExpo');

			e.preventDefault();

			if (Reapp.getViewPort().width < Reapp.getBreakpoint('md')) {
				$('.c-hor-nav-toggler').click();
			}
		})

		// Agregar | Eliminar .active-anchor
		$(window).scroll(function(){
			var scrollPos = $(document).scrollTop();
			$('.menu-onepage a').each(function () {
				var currLink = $(this);
				// var refElement = $(currLink.attr('href'));
				var refElement = $(currLink.data('anchor'));

				if(refElement.length) {
					var		windowViewTop = $(window).scrollTop() + offset,
							windowViewBottom = $(window).scrollTop() + offset + $(refElement).height(),
						 	sectionTop = $(refElement).offset().top,
							sectionBottom = sectionTop + ($(refElement).innerHeight());

					// console.log("offset " + offset);
					// console.log("windowViewTop " + windowViewTop);
					// console.log("windowViewBottom " + windowViewBottom);
					// console.log("sectionTop " + sectionTop);
					// console.log("sectionBottom " + sectionBottom);

					if ((windowViewTop + 1 >= sectionTop)) {
						$('.menu-onepage ul li a').removeClass('active-anchor');
						currLink.addClass('active-anchor');
					}
					else {
						currLink.removeClass('active-anchor');
					}
				}
			})
		})
	};

	return {

		//main function to initiate the module
		init: function () {
			handle(); // call headerFix() when the page was loaded
		}

	};
}();

/*--------------------------------------------------------------------------------*/
// Go to top
/*--------------------------------------------------------------------------------*/
var goToTop = function () {

	var handle = function () {
		var currentWindowPosition = $(window).scrollTop(); // current vertical position
		if (currentWindowPosition > 300) {
			$('.go-to-top').fadeIn();
		} else {
			$('.go-to-top').fadeOut();
		}
	};

	return {

		//main function to initiate the module
		init: function () {
			handle(); // call headerFix() when the page was loaded

			if (navigator.userAgent.match(/iPhone|iPad|iPod/i)) {
				$(window).bind("touchend touchcancel touchleave", function (e) {
					handle();
				})
			} else {
				$(window).scroll(function () {
					handle();
				})
			}

			$('.go-to-top').on('click', function (e) {
				e.preventDefault();
				$('html, body').animate({
					scrollTop: 0
				}, 600);
			})
		}

	};
}();



/*--------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------*/
// Función que llama a todas las funciones independientes
// Se llama con document y window
/*--------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------*/
var Reapp = function() {
	var resizeHandlers = [];

	// runs callback functions set by Jango.addResponsiveHandler().
	var _runResizeHandlers = function() {
		// reinitialize other subscribed elements
		for (var i = 0; i < resizeHandlers.length; i++) {
			var each = resizeHandlers[i];
			each.call();
		}
	};

	 // handle the layout reinitialization on window resize
	var handleOnResize = function() {
		var resize;
		$(window).resize(function() {
			if (resize) {
				clearTimeout(resize);
			}
			resize = setTimeout(function() {
				_runResizeHandlers();
			}, 50); // wait 50ms until window resize finishes.
		})
	};

	// Inicializa las variables / funciones
	return {
		init: function () {
			//Core handlers
			sameHeight();
			heightWidth();
			heightPhoto32();
			sameHeightPhoto32();
			heightViewport();
			heightViewportHalf();
			productGallery();
			photoGallery();
			// Handle auto calculating height on window resize
			this.addResizeHandler(sameHeight); 
			this.addResizeHandler(heightWidth);
			this.addResizeHandler(heightPhoto32);
			this.addResizeHandler(sameHeightPhoto32);
			this.addResizeHandler(heightViewport);
			this.addResizeHandler(heightViewportHalf);
			this.addResizeHandler(productGallery);
			this.addResizeHandler(photoGallery);

			handleOnResize(); // set and handle responsive
		},

		//public function to add callback a function which will be called on window resize
		addResizeHandler: function(func) {
			resizeHandlers.push(func);
		},

		//public functon to call _runresizeHandlers
		runResizeHandlers: function() {
			_runResizeHandlers();
		},

		 // To get the correct viewport width based on  http://andylangton.co.uk/articles/javascript/get-viewport-size-javascript/
		getViewPort: function() {
			var e = window,
				a = 'inner';
			if (!('innerWidth' in window)) {
				a = 'client';
				e = document.documentElement || document.body;
			}

			return {
				width: e[a + 'Width'],
				height: e[a + 'Height']
			};
		},

		// responsive breakpoints
		getBreakpoint: function(size) {
			// bootstrap responsive breakpoints
			var sizes = {
				'xs': 480, // extra small
				'sm': 768, // small
				'md': 992, // medium
				'lg': 1200 // large
			};

			return sizes[size] ? sizes[size] : 0;
		}
	};
}();

// Main initialization
$(document).ready(function () {
	pageScrollActive.init();
	onePageNav.init();
	goToTop.init();
})

// To classify
/*--------------------------------------------------------------------------------*/
// Evitar llamadas desde dispositivos !Smartphones
/*--------------------------------------------------------------------------------*/
$('a.llamar').click(function(){
	return navigator.userAgent.match(/Android|iPhone|iPad|iPod|Mobile/i)!=null;
})

/*--------------------------------------------------------------------------------*/
// Eliminar video del slider para visitas desde Safari de Windows
/*--------------------------------------------------------------------------------*/
var ua = navigator.userAgent.toLowerCase();
if (ua.indexOf('safari/') !== -1 &&  // It says it's Safari
	ua.indexOf('windows') !== -1 &&  // It says it's on Windows
	ua.indexOf('chrom')   === -1     // It DOESN'T say it's Chrome/Chromium
	) {
	$('.slider-single-landscape ul li:first-child').remove();
}

/*--------------------------------------------------------------------------------*/
// Ajustar validación con required de HTML5
/*--------------------------------------------------------------------------------*/
$(':required').one('blur keydown', function() {
	$(this).addClass('touched');
})