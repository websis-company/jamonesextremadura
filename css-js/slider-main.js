// RevoSlider
$(document).ready(function(){
	var cont = $(".container-slider-mobile-portrait");
	var slider = $(".slider-mobile-portrait");
	var api = slider.show().revolution({
		delay: 7500,
		startwidth: 768,
		startheight: Reapp.getViewPort().height,
		
		hideTimerBar: "on",
		hideNavDelayOnMobile: 1500,
		hideThumbsOnMobile: "on",
		hideBulletsOnMobile: "on",
		hideArrowsOnMobile: "off",
		hideThumbsUnderResolution: 0,
		
		navigationType: "hide",
		navigationArrows: "solo",
		navigationStyle: "circle",
		navigationHAlign: "center",
		navigationVAlign: "bottom",
		
		fullWidth: "off", //*
		fullScreen: "on", //*
		fullScreenAlignForce: "on",
		fullScreenOffsetContainer: "",
		forceFullWidth: "off",
	
		spinner: "spinner2",
		touchenabled: "on",
		onHoverStop: "on",
		keyboardNavigation: "off",
		shadow: 0,
	});
});
$(document).ready(function(){
	var cont = $(".container-slider-mobile-landscape");
	var slider = $(".slider-mobile-landscape");
	var api = slider.show().revolution({
		delay: 7500,
		startwidth: 1920,
		startheight: Reapp.getViewPort().height,
		
		hideTimerBar: "on",
		hideNavDelayOnMobile: 1500,
		hideThumbsOnMobile: "on",
		hideBulletsOnMobile: "on",
		hideArrowsOnMobile: "on",
		hideThumbsUnderResolution: 0,
		
		navigationType: "hide",
		navigationArrows: "solo",
		navigationStyle: "circle",
		navigationHAlign: "center",
		navigationVAlign: "bottom",
		
		fullWidth: "off", //*
		fullScreen: "on", //*
		fullScreenAlignForce: "on",
		fullScreenOffsetContainer: "",
		forceFullWidth: "off",
	
		spinner: "spinner2",
		touchenabled: "on",
		onHoverStop: "on",
		keyboardNavigation: "off",
		shadow: 0,
	});
});

// Owl Carousel
if ($('#owl-featured-tours').length){
	$("#owl-featured-tours").owlCarousel({
		items : 3,
		itemsDesktop : [1440,3],
		itemsDesktopSmall : [1280,2],
		itemsTablet : [768, 2],
		itemsTabletSmall : [480, 1],
		itemsMobile : [350, 1],
		navigation : true,
		pagination : false,
		autoPlay: 5000,
		theme: "owl-multi"
	})
}

// Owl Carousel
if ($('#owl-testimonial').length){
	$("#owl-testimonial").owlCarousel({
		items : 1,
		itemsDesktop : [1440,1],
		itemsDesktopSmall : [1280,1],
		itemsTablet : [768, 1],
		itemsTabletSmall : [480, 1],
		itemsMobile : [350, 1],
		navigation : false,
		pagination : true,
		autoPlay: 5000,
		theme: "owl-multi"
	})
}

