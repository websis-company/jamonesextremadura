<form name="formCart" action="purchase.php" method="get" id="formCart" >
	<table class="shopping-cart-table table-hover">
		<!-- <caption>Mi compra</caption> -->
		<thead class="shopping-cart-thead">
			<tr>
				<?php
				if($showLinkToDelte){?>
					<th class="prod-act"></th>
				<?php
				}//end if
				?>
				<th class="prod-img">Imagen</th>
				<th class="prod-sku"><?=$code[$lang]?></th>
				<th class="prod-des"><?=$product_name[$lang]?></th>
				<th class="prod-des">Caracter&iacute;stica</th>
				<th class="prod-qua"><?=$qty[$lang]?></th>
				<th class="prod-pri"><?=$unit_price[$lang]?></th>
				<th class="prod-sub"><?=$price_disc[$lang]?></th>
				<th class="prod-sub">Subtotal</th>
			</tr>
		</thead>
		
		<tbody class="shopping-cart-tbody">
			<?php $totalTmp = 0;
			foreach($OUTPUT["array_product"] as $product){
				$id              = $product['id'];
				$hrefBorrar      =  "cart.php?cmd=remove&productId=$id";
				$src             = $product['image_url'];
				$codigo          = $product['codigo'];
				$nombre          = $product['name'];
				$version_codigo  = $product['version_sku'];
				$version_nombre  = $product['version_name'];
				$caracteristicas = str_replace(",","<br/>",$product['caracteristicas']);
				$peso            = $product['str_peso'];
				// $color         = $product['color'];
				// $colores         = eCommerce_SDO_Color::LoadColor($color);
				// $color           = $colores->getNombre();
				// $talla           = $product['talla'];
				// $tallas          = eCommerce_SDO_Talla::LoadTalla($talla);
				// $talla           = $tallas->getNombre();
				$quantity        = $product['quantity'];
				$pzsEstima       = $uvc == 'Peso' ? floor($quantity / $product['peso']) : 0;
				$lblEstima       = ucfirst(strtolower($uv))."s ". ($uv=='PIEZA'?str_replace('o','a',$estimated[$lang]):$estimated[$lang])."s";
				$unidad          = $uvc=='Pieza'?" $piece[$lang]".($product['quantity']>1?'s':''):($uvc=='Peso'?" $gram[$lang]".($product['quantity']>1?'s<br /><span style="font-weight:normal">'.$lblEstima.': '.$pzsEstima.'</span>':''):'');
				$unidad          = "";
				$precio          = $product['str_price'];
				$descuento       = $product['str_price_with_discount'];
				$totalTmp        +=$product['quantity']*$product['version_price'];
				$subtotal        = $product['str_total_price'];
			?>
			<tr>
				<?php
				if($showLinkToDelte){?>
					<td class="prod-act">
						<a href="<?=$hrefBorrar?>" class="fa fa-trash-o"></a>
					</td>
				<?php
				}//end if
				?>
				<td class="prod-img"><img src="<?php echo $src?>" alt="<?php echo $nombre; ?>" /></td>
				<td class="prod-sku"><?php echo $version_codigo?></td>
				<td class="prod-des"><?php echo $version_nombre?></td>
				<td class="prod-des"><?php echo $caracteristicas?></td>
				<td class="prod-qua">
					<?php 
					if($editProductQuantity){
					?>
						<input type="number" name="quantity_<?=$id?>" id="quantity_<?=$id?>" value="<?=$quantity?>" min="1" size="4"/><strong><?php echo $unidad?></strong>
						<br />
						<a href="javascript:updateQuantity('<?=$id?>')" style='text-decoration:none;' class="negro12a"><b><?=$update[$lang] ?></b></a>
					<?php }else{ ?>
						<strong><?=$quantity?></strong>	
					<?php 
					}
					?>
				</td>
				<td class="prod-pri"><?=$precio?></td>
				<td class="prod-sub"><?=$descuento?></td>
				<td class="prod-sub"><?=$subtotal?></td>
			</tr>
			<?php 
			}
			?>
		</tbody>

		<tfoot class="shopping-cart-tfoot" >
			<tr>
				<td colspan="5"><!-- <small>El Subtotal no incluye el envío. El costo del envío será confirmado una vez que continúes con la compra.</small> --></td>
				<td colspan="4" class="shopping-cart-subtotal">
					<span>Subtotal</span> <strong><?=$OUTPUT["str_subtotal"];?></strong>
				</td>
			</tr>
			<?php 
			if( $showExtrasAndTotal ){
				foreach($OUTPUT["extras"] as $extra){ ?>
				<tr>
					<td colspan="5"></td>
					<td colspan="4" class="shopping-cart-shipping">
						<span><?=$extra["description"];?></span> <strong><?=$extra["str_amount"];?></strong>
					</td>
				</tr>
			<?php
				}//end foreach
			}//end if
			?>
			<tr>
				<td colspan="5"></td>
				<td colspan="4" class="shopping-cart-total">
					<span>Total</span> <strong><?=$OUTPUT["str_total"];?></strong>
				</td>
			</tr>
			<tr class="shopping-cart-actions">
				<td colspan="9">
					<a href="<?=ABS_HTTP_URL?>cuadros-decorativos/" class="button-flat secondary">Seguir comprando</a>
					<!-- <a href="#" class="button-flat primary">Comprar</a> -->
					<input type="submit" class="button-flat primary" value="Comprar" id="purchase" style="border: none;">
				</td>
			</tr>
		</tfoot>
	</table>
</form>


<!--<form name="formCart" action="purchase.php" method="get" id="formCart" >
	<table id="table-titles" class="table" border="0" align="right" cellpadding="0" cellspacing="0">
		<tr>
			<td valign="top">
			<table class="table" border="0" cellspacing="0" cellpadding="0">
				<tr style="color: #FFF; height: 35px;">
					<?if($showLinkToDelte){?><td  bgcolor="#000"><img src="images/trans.gif" style="width:80%"></td><?}?>
					<td  bgcolor="#000"><img src="images/trans.gif" style="80%"></td>
					<td  align="center" bgcolor="#000" class="blanco12 tdCode"><strong><?=$code[$lang]?></strong></td>
					<td  align="center" bgcolor="#000" class="blanco12 tdName"><strong><?=$product_name[$lang]?></strong></td>
					<!--<td  align="center" bgcolor="#000" class="blanco12 tdAttr"><strong>Atributo</strong></td> ->							
				    <td  align="center" bgcolor="#000" class="blanco12 tdCar"><strong>Caracter&iacute;stica</strong></td>
					<?if($editProductQuantity){?>
						<td  height="25" align="center" valign="middle" bgcolor="#000" class="blanco12"><strong><?=$qty[$lang]?></strong></td>
					<?}else{?>
						<td  align="center" bgcolor="#000" class="blanco12 tdQty"><strong><?=$qty[$lang]?></strong></td>
					<?}?>
					<td  align="center" bgcolor="#000" class="blanco12 tdPrice"><strong><?=$unit_price[$lang]?></strong></td>
					<td  align="center" bgcolor="#000" class="blanco12 tdPrice"><strong><?=$price_disc[$lang]?></strong></td>
					<td  align="right" bgcolor="#000" class="blanco12 tdSub"><strong>Subtotal</strong></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td valign="top" height="1" bgcolor="#00BEFF">&nbsp;
				<!-- <img src="<?=ABS_HTTP_URL?>images/trans.gif" width="10" height="1" style="display:block;" /> ->
			</td>
		</tr>
		<?php /*$totalTmp = 0;
		foreach($OUTPUT["array_product"] as $product){
			$id              = $product['id'];
			$hrefBorrar      =  "cart.php?cmd=remove&productId=$id";
			$src             = $product['image_url'];
			$codigo          = $product['codigo'];
			$nombre          = $product['name'];
			$version_codigo  = $product['version_sku'];
			$version_nombre  = $product['version_name'];
			$caracteristicas = str_replace(",","<br/>",$product['caracteristicas']);
			$peso            = $product['str_peso'];
			// $color         = $product['color'];
			// $colores         = eCommerce_SDO_Color::LoadColor($color);
			// $color           = $colores->getNombre();
			// $talla           = $product['talla'];
			// $tallas          = eCommerce_SDO_Talla::LoadTalla($talla);
			// $talla           = $tallas->getNombre();
			$quantity        = $product['quantity'];
			$pzsEstima       = $uvc == 'Peso' ? floor($quantity / $product['peso']) : 0;
			$lblEstima       = ucfirst(strtolower($uv))."s ". ($uv=='PIEZA'?str_replace('o','a',$estimated[$lang]):$estimated[$lang])."s";
			$unidad          = $uvc=='Pieza'?" $piece[$lang]".($product['quantity']>1?'s':''):($uvc=='Peso'?" $gram[$lang]".($product['quantity']>1?'s<br /><span style="font-weight:normal">'.$lblEstima.': '.$pzsEstima.'</span>':''):'');
			$unidad          = "";
			$precio          = $product['str_price'];
			$descuento       = $product['str_price_with_discount'];
			$totalTmp        +=$product['quantity']*$product['version_price'];
			$subtotal        = $product['str_total_price'];*/
			?>
			<tr style="border-bottom: 1px solid #000;">
				<td valign="top" style="background-color:#FFFFFF;">
				<table class="products-wrapper table" border="0" cellspacing="0" cellpadding="0" style="font-size: 12px; width:90%">
					<tr>
						<?if($showLinkToDelte){?><td ><div align="center" class="rojo12"><strong><a href="<?=$hrefBorrar?>" class="rojo12">
						<img src="img/close.png" width="80%" alt="<?=$remove[$lang]?>" border="0"></a></strong></div></td><?}?>
						<td  align="center" ><img src="<?=$src?>" style="width:50%" /></td>
						<td  align="center"><strong class="negro12a tdCode"><?=$version_codigo?></strong></td>
						<td  class="negro12a tdName" align="center"><?=$version_nombre?></td>
						<!--<td  align="center" class="negro12a tdAttr" align="center"><?=utf8_encode($color)?></td> ->
						<td  class="negro12a tdCar" align="center"><?=$caracteristicas?></td>									
						<?if($editProductQuantity){?><td  align="center" class="negro12a tdQty" >
							<input type="number" name="quantity_<?=$id?>" id="quantity_<?=$id?>" value="<?=$quantity?>" min="1" size="4" style="width:50px; text-align:center;" /><strong><?=$unidad?></strong>
							<br />
							<a href="javascript:updateQuantity('<?=$id?>')" style='text-decoration:none;' class="negro12a"><b><?=$update[$lang] ?></b></a>
						</td><?}else{?>
						<td  align="center" class="negro12a tdQty" >
							<strong><?=$quantity?></strong>										
						</td>
						<?}?>
						<td  align="center" class="negro12a tdPrice"><?=$precio?></td>
						<td  align="center" class="negro12a"><?=$descuento?></td>
						<td align="right" class="negro12a tdSub" ><strong><?=$subtotal?></strong></td>
					</tr>
				</table>
				</td>
			</tr>
		<?/*}*/?>
		<tr>
			<td valign="top" height="1" bgcolor="#00BEFF">
				<img src="<?=ABS_HTTP_URL?>images/trans.gif" width="10" height="1" style="display:block;" />
			</td>
		</tr>
		<tr>
			<td valign="top" style="background-color:#FFFFFF;">
			<table border="0" cellspacing="0" cellpadding="0" class="shop-cart-table-total table" style="font-size: 13px;">
				<tr>
					<td align="right" class="negro12a"><strong class="rosa12">Subtotal</strong></td>
					<td  align="right" class="grishome"><strong class="negro12a" ><?=$OUTPUT["str_subtotal"];?></strong></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
		<td valign="top">&nbsp;
		</td>
		</tr>
		<?php if( $showExtrasAndTotal ){
			foreach($OUTPUT["extras"] as $extra){ ?>
			<tr>
			<td valign="top">
			<table border="0" cellspacing="0" cellpadding="0" class="shop-cart-table-total table" style="font-size: 13px;">
				<tr>
					<td align="right" class="negro12a" ><strong class="negro12a"><?=$extra["description"];?></strong></td>
                	<td  align="right" class="negro12a"><?=$extra["str_amount"];?></td>
				</tr>
			</table>
			</td>
			</tr>
			<?}?>
			<tr>
			<td valign="top">
			<table border="0" cellspacing="0" cellpadding="0" class="shop-cart-table-total table" style="font-size: 13px;">
				<tr>
					<td align="right" class="negro12a" >&nbsp;</td>
					<td  align="right" class="negro12a"><hr></td>
				</tr>
			</table>
			</td>
			</tr>
			<tr>
			<td valign="top">
			<table border="0" cellspacing="0" cellpadding="0" class="shop-cart-table-total table" style="font-size: 13px;">
				<tr>
					<td  align="right" class="negro12a" ><p><span class="negro12a" style="font-weight: bold;">Total </span></td>
                	<td  align="right" class="negro12a"><strong id="strTotal"><?=$OUTPUT["str_total"];?></strong></td>
				</tr>
				</table>
			</td>
			</tr>
		<?}?>
	</table>
</form>-->