<?php
function subText($txt) {
	return Util_String::subText($txt, 150);
}

function showImage($image) {
	$image = explode(',', $image);
	if(!empty($image[0])) {
		$file = eCommerce_SDO_Core_Application_FileManagement::LoadById($image[0]);
		$imgSrc = '../file.php?id='.$image[0].'&type=image';
		return  '<a class="thickbox" href="'.$imgSrc.'" title="'.$file->getDescription().'">'.
						'<img src="../file.php?id=' . $image[0] . '&type=image&img_size=predefined&width=50&height=50" border="0" />'.
						'</a>';
	}
	return '';
}

function parseImage($image_id){
	if ( empty( $image_id ) ){
		return '';
	}
	else {
		return '<a class="thickbox" href="../file.php?id=' . $image_id . '&type=image" target="_blank"><img border="0" src="../file.php?id=' . $image_id . '&type=image&img_size=predefined&height=50&width=51" /></a> ';
	}
}

$listPanel = new GUI_ListPanel_ListPanel( 'category' );
$listPanel->setData( $this->data );


$hiddenColumns = array( 'parent_category_id','category_id','banner_id');
foreach( $hiddenColumns as $col ){
	$listPanel->addHiddenColumn( $col );
}

$columnNamesOverride = array('category_id'=>'ID', 'name'=>$this->trans('name'), 'description'=>$this->trans('description'), 'image_id'=>$this->trans('image'),'banner_id'=>"Banner");
foreach($columnNamesOverride as $col => $name) {
	$listPanel->addColumnNameOverride( $col, $name );
}

$listPanel->addCallBack('image_id', 'showImage');
$listPanel->addCallBack('banner_id', 'showImage');

$listPanel->addColumnAlignment( 'name', 'left');
$listPanel->addColumnAlignment( 'description', 'left');

// Add "Order By" capabilities
$listPanel = new GUI_ListPanel_Decorator_Sorter( $listPanel );
$listPanel->setOrderBy( $this->options->getOrderBy() );

// Add actions
$listPanel = new GUI_ListPanel_Decorator_RowActions( $listPanel );
$listPanel->setHeaderLabel( $this->trans('catalog') );

// Modify SubCategories
//$actModifySubcategories = new GUI_ListPanel_Action( '?cmd=list&category_id={$category_id}', 'img/chart_organisation.png' );
$actModifySubcategories = new GUI_ListPanel_Action( '?cmd=list&category_id={$category_id}', 'fa fa-sitemap' );
$actModifySubcategories->setImageTitle( 'Sub-categorias' );
$listPanel->addAction( $actModifySubcategories );

// Show Products
/** Validar si se muestra */
//$actShowProducts = new GUI_ListPanel_Action( 'productcategory.php?cmd=listProducts&category_id={$category_id}', 'img/application_cascade.png' );
/*$actShowProducts = new GUI_ListPanel_Action( 'productcategory.php?cmd=listProducts&category_id={$category_id}', 'fa fa-shopping-cart' );
$actShowProducts->setImageTitle($this->trans('product').'s' );
$listPanel->addAction( $actShowProducts );*/

// Add actions
$listPanel = new GUI_ListPanel_Decorator_RowActions( $listPanel );
$listPanel->setHeaderLabel( $this->trans('actions') );

// Modify
/*$actModify = new GUI_ListPanel_Action( '?cmd=edit&id={$category_id}&parent={$category_id}', 'img/write.png' );
$actModify->setImageTitle( $this->trans('edit').' '.$this->trans('category') );
$listPanel->addAction( $actModify );*/
//$actModify = new GUI_ListPanel_Action('javascript:;', 'img/write.png' );
$actModify = new GUI_ListPanel_Action('javascript:;', 'fa fa-pencil-square-o' );
$actModify->setImageTitle(  $this->trans('edit').' '.$this->trans('category') );
$actModify->setOnClickEvent( 'editForm("edit","{$category_id}")' );
$listPanel->addAction( $actModify );


// Delete
//$actDelete = new GUI_ListPanel_Action( '?cmd=delete&id={$category_id}&category_id=' . $this->category->getCategoryId(), 'img/delete.png' );
$actDelete = new GUI_ListPanel_Action( '?cmd=delete&id={$category_id}&category_id=' . $this->category->getCategoryId(), 'fa fa-trash-o' );
$actDelete->setImageTitle( $this->trans('delete').' '.$this->trans('category') );
$actDelete->setOnClickEvent( 'return confirm("'.$this->trans('?_confirm_delete').' '.strtolower($this->trans('the(f)')).' '.strtolower($this->trans('category')).' \"{$name}\"?");' );
$listPanel->addAction( $actDelete );


// Add "Search" capabilities
$listPanel = new GUI_ListPanel_Decorator_Searcher( $listPanel );
$listPanel->setFieldLabel( '<img src="img/search2_small.png" style="vertical-align:middle">&nbsp;' );
$listPanel->setButtonLabel($this->trans('search'));
$listPanel->setClearButtonLabel($this->trans('clear'));
$listPanel->setSearchString( $this->options->getKeywords() );

// Add paging capabilities
$listPanel = new GUI_ListPanel_Decorator_Pager( $listPanel );
$listPanel->setCurrentPage( $this->options->getPage() );
$listPanel->setPagingLabel($this->trans('page'));
$listPanel->setPreviousLabel('&#9668; '.$this->trans('previous'));
$listPanel->setNextLabel($this->trans('next').' &#9658;');
$listPanel->setFirstPageLabel($this->trans('first(f)').' '.$this->trans('page'));
$listPanel->setLastPageLabel($this->trans('last(f)').' '.$this->trans('page'));
$listPanel->setOffsetLabel($this->trans('results_per').' p&aacute;gina');
$listPanel->setTotalPages( 1 ); //TODO:
$listPanel->setOffset( $this->options->getResultsPerPage() );


include_once ( "includes/header.php" );
?>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1> <?php if( $this->category->getCategoryId() != 0 ) echo 'Sub-'; echo $this->trans('categories'); ?></h1>
		<ol class="breadcrumb">
			<li><a href="index.php"><i class="fa fa-home"></i> Home</a></li>
			<li><a href="category.php"><?php if( $this->category->getCategoryId() != 0 ) echo 'Sub-'; echo $this->trans('categories'); ?></a></li>
		</ol>
	</section>

    <div id="divEditForm" class="col-xs-12 col-md-8 col-md-offset-2" style="display: none;"></div>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        	<div class="box">
        		<div class="box-header">
                    <div class="cleafix"></div>
                    <?php 
						if( $this->category->getCategoryId() != 0 ) {
							$this->display( 'category/info.php' );
						}//end if
						?>
					<?php 
					if( $this->category->getCategoryId() != 0 ) {
					?>
					<div class="box-title pull-left">
						<a href="?cmd=list&category_id=<?php echo $this->category->getParentCategoryId(); ?>" class="btn btn-default">
							<!-- <img src="img/rewind.png" border="0" hspace="5" style="vertical-align:text-bottom"> -->
                            <i class="fa fa-chevron-circle-left" aria-hidden="true"></i>
                            <?=$this->trans('go_back_to_parent_category')?>
						</a>
					</div>
					<?php 
					}//end if
					?>
        			<div class="box-title pull-right">
                        <div class="loading"></div>
                        <a id="btnEditForm" class="btn btn-default" onclick="editForm('new','<?php echo $this->category->getCategoryId();?>');" style="cursor: pointer;">
                            <!-- <img src="img/wand.png" border="0" hspace="2" align="absmiddle"> -->
                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                            <?php echo $this->trans('add').' '.$this->trans('new(f)').' '; if( $this->category->getCategoryId() != 0 ) echo 'Sub-'; echo $this->trans('category') ?>
                        </a>
                    </div>
        		</div><!-- /.box-header -->
        		<div class="box-body" style="overflow-x: auto;">
        		<?php
        			$listPanel->setTableWidth( '100%' );
        			$listPanel->display();
        		?>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div><!-- /.col -->
      </div><!-- /.row -->
    </section><!-- /.content -->	
</div><!-- /.content-wrapper -->


<?php
include_once ( "includes/footer.php" );
?>
<script type="text/javascript">
    $(function () {
        $("#table").DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },
            "order": [[ 3, "desc" ]]
        });
    });

    function editForm(type,id){
        if(type == 'new'){
            cmd = 'cmd=add&parent_category_id='+id;
        }else if(type == 'edit'){
            cmd = 'cmd=edit&id='+id+'&parent='+id;
        }


        var Url = "<?php echo ABS_HTTP_URL?>";
        $.ajax({
            'type'    :"GET",
            'url'     : Url+'bo/backoffice/category.php',
            'data'    : cmd,
            'dataType': "html",
            beforeSend: function(data){
                $("#btnEditForm").attr('disabled',true);
                $(".loading").html('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');
            },
            success:  function (data){
                $("#btnEditForm").hide();
                $(".loading").hide();
                $("#divEditForm").html(data);
                $("#divEditForm").slideDown();
                $('html, body').stop().animate({scrollTop:0},500,'swing',function(){});
            }
        });       
    }

</script>