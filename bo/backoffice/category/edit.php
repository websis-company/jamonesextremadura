<?php
//$treeCategories = '';
//include_once ( "header.php" );
function dataToInput( $value ){
	return htmlentities( $value, ENT_QUOTES, CHARSET_PROJECT );
}

$errors    = $this->errors;
$category  = $this->category;
$ArrStatus = $this->ArrStatus;
?>

<div class="row">
	<div class="col-md-12">
		<div class="box box-danger">
			<div class="box-header with-border">
				<h3><? echo $this->strSubtitles; ?></h3>
			</div>
			<form name="frmCategory" id="frmCategory" method="post" action="category.php?category_id=<?php echo $category->getParentCategoryId();?>" enctype="multipart/form-data">
				<div class="box-body">
					<div class="form-group">
						<h4 class="box-title">
							<?=$this->trans('information')?>
						</h4>
					</div>
					<div class="form-group">
						<div class="col-xs-12 col-md-6">
							<label for="parent_category_id"><?=$this->trans('parent')?> :</label>
							<select class="form-control" name="entity[parent_category_id]" id="parent_category_id">
								<? echo $this->treeCategories; ?>
							</select>
						</div>
						<div class="col-xs-12 col-md-6">
							<label for="name"><?=$this->trans('name')?>:</label>
							<input type="text" size="30" maxlength="100" class="form-control" name="entity[name]" id="name" value="<?php echo dataToInput( $category->getNameInDefaultLanguage());?>" required="required">
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12 col-md-12">
							<label for="description"><?=$this->trans('description')?> :</label>
							<textarea rows="4" class="form-control" name="entity[description]" id="description" required="required"><?php echo dataToInput( $category->getDescriptionInDefaultLanguage());?></textarea>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12 col-md-6">
							<label for="name">Orden :</label>
							<input type="number" size="30" min="0" maxlength="100" class="form-control" name="entity[orden]" id="orden" value="<?php echo dataToInput( $category->getOrden()  ) ?>">
						</div>
						<div class="col-xs-12 col-md-6">
							<label for="status"><?=$this->trans('status')?> :</label>
							<?php
								echo '<select id="status" name="entity[status]" class="form-control" required="required">';
								foreach( $ArrStatus as $Status){
									echo "<option value='{$Status}'";
									if( $category->getStatus() == $Status ){
										echo " selected='selected'";
									}
									echo ">".$this->trans(strtolower($Status))."</option>";
								}
								echo "</select>";
							?>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12 col-md-12">
							<label for="image_id"><?=$this->trans('image')?>  <br/><span style="font-size: 12px;">(Tamaño de Imagen <strong>700x487pixeles</strong>)</span> :</label>
							<?php 
							$hideDes = is_null($element['hide_description']) ? false : $element['hide_description'];
							$imageHandler = new eCommerce_SDO_Core_Application_ImageHandler("image_id", 1 );
							$imageHandler->setArrImages( $category->getImageId() );
							$html = $imageHandler->createBOImages(null,null,$hideDes);
							echo $html;
							?>
						</div>
					</div>
					<br><br>
					<div class="form-group">
						<div class="col-xs-12 col-md-12">
							<label for="image_id"><?=$this->trans('Banner')?> <br/><span style="font-size: 12px;"><strong>Tamaño de Imagen 1920x300pixeles</strong></span> :</label>
							<?php 
							$hideDes = is_null($element['hide_description']) ? false : $element['hide_description'];
							$imageHandler = new eCommerce_SDO_Core_Application_ImageHandler("banner_id", 1 );
							$imageHandler->setArrImages( $category->getBannerId() );
							$html = $imageHandler->createBOImages(null,null,$hideDes);
							echo $html;
							?>
						</div>
					</div>
				</div>
				<div class="box-footer">
					<input type="hidden" name="entity[category_id]" value="<?php echo dataToInput( $category->getCategoryId() ) ?>">
					<input type="hidden" name="cmd" value="save">
					<button type="submit" class="btn btn-primary pull-right" style="margin-left: 15px;">Aceptar</button>
					<button type="button" class="btn btn-default pull-right" onClick="closeForm();">Cancelar</button>
				</div>
			</form>
		</div>
	</div>
</div>
<?php
//include_once ( "footer.php" );
?>

<script type="text/javascript">
	function closeForm(){
		$("#divEditForm").slideUp();
		$("#frmCategory")[0].reset();
		$("#btnEditForm").show(function(e){
			$("#btnEditForm").attr('disabled',false);
			$(".loading").html('');
			$(".loading").show('');
		});
	}// end function

	$(document).ready(function(e){
		$("#frmCategory").validate({
			debug: false,
			/** Pava validar campos de imagen obligatorio **/
			/*rules:{
				file_image_id_0:{required:true},
			}*/
		});
		//Date range picker with time picker
        //$('#rangeTime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
    });

</script>