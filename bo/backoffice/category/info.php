<?php
function getCategoryPath( $categoryId, &$tpl ){
	$category = eCommerce_SDO_Catalog::LoadCategory( $categoryId );
	if ( $category->getCategoryId() == 0 ){
		$category->setName( '<i>'.$tpl->trans('root').'</i>');
	}
	$categories = array();
	$categories[] = $category;
	if ( $category->getCategoryId() > 0 ){
		$categories = array_merge_recursive( getCategoryPath( $category->getParentCategoryId(), $tpl ), $categories ); 
	}
	return $categories;
}

$path = getCategoryPath( $this->category->getParentCategoryId(), $this );
?>

<div class="box box-widget widget-user-2">
	<!-- Add the bg color to the header using any of the bg-* classes -->
	<div class="widget-user-header bg-red">
		<h3 class="widget-user-username"><?=$this->trans('category')?>: <?php echo $this->category->getName()?></h3>
	</div>
	<div class="box-footer">
		<div class="row">
			<div class="col-xs-12 col-sm-2 border-right">
				<div class="description-block">
					<h5 class="description-header">ID</h5>
					<span class="description-text"><?php echo $this->category->getCategoryId(); ?></span>
				</div>
				<!-- /.description-block -->
			</div>
			<!-- /.col -->
			<div class="col-xs-12 col-sm-3 border-right">
				<div class="description-block">
					<h5 class="description-header"><?=$this->trans('parent_category')?></h5>
					<span class="description-text"><?php 
					foreach( $path as $category ){
						echo $category->getName() . ' &raquo; ';
					}
					?></span>
				</div>
				<!-- /.description-block -->
			</div>
			<!-- /.col -->
			<div class="col-xs-12 col-sm-2 border-right">
				<div class="description-block">
					<h5 class="description-header">
						<!-- <a href="productcategory.php?cmd=listProducts&category_id=<?php echo $this->category->getCategoryId() ?>" title="<?=$this->trans('manage').' '.$this->trans('product').'s '.$this->trans('in').' '.$this->trans('this(f)').' '.$this->trans('category')?>">
							<img src="img/search2_small.png" border="0" align="right">
						</a> -->
						<?=$this->trans('product').'s'?>:
					</h5>
					<span class="description-text">
						<?php 
							$products = eCommerce_SDO_Catalog::GetProductsByCategoryId( $this->category->getCategoryId() );
							echo number_format( count($products), 0, '.', ',' );
						?>
					</span>
				</div>
				<!-- /.description-block -->
			</div>
			<!-- /.col -->
			<div class="col-xs-12 col-sm-2 border-right">
				<div class="description-block">
					<h5 class="description-header">
						<!-- <a href="category.php?cmd=list&category_id=<?php echo $this->category->getCategoryId() ?>" title="<?=$this->trans('manage').' Sub'.strtolower($this->trans('categories'))?>"><img src="img/search2_small.png" border="0" align="right"></a> -->Subcategorias:
					</h5>
					<span class="description-text">
						<?php
						$search = new eCommerce_Entity_Search();
						$search->setResultsPerPage( -1 ); 
						$rs = eCommerce_SDO_Catalog::SearchCategoriesByParentCategory( $search, $this->category->getCategoryId() );
						$categories = count( $rs->getResults() );
						echo number_format( $categories, 0, '.', ',' );
						?>
					</span>
				</div>
				<!-- /.description-block -->
			</div>
			<!-- /.col -->
			<div class="col-xs-12 col-sm-3 border-right">
				<div class="description-block">
					<h5 class="description-header">
						<?=$this->trans('description')?>:
					</h5>
					<span class="description-text"><?php echo $this->category->getDescription(); ?></span>
				</div>
			</div>
		</div>
		<!-- /.row -->
	</div>
</div>

