<?php
function getCategoryPath( $galerycategoryId, &$tpl ){
	$category = eCommerce_SDO_Galerycategory::LoadGalerycategory( $galerycategoryId );
	if ( $category->getGalerycategoryId() == 0 ){
		$category->setName( '<i>'.$tpl->trans('root').'</i>');
	}
	$categories = array();
	$categories[] = $category;
	if ( $category->getGalerycategoryId() > 0 ){
		$categories = array_merge_recursive( getCategoryPath( $category->getParentCategoryId(), $tpl ), $categories ); 
	}
	return $categories;
}

$path = getCategoryPath( $this->category->getParentCategoryId(), $this );
?>

<table border="0" width="100%" cellpadding="0" cellspacing="0" 
       style="border-top: 3px double #c0c0c0; border-bottom: 3px double #c0c0c0;margin:12px 0">
	<tr bgcolor="#e0e0e0">
		<td colspan="3" style="font-size: 1.5em; font-weight: bold;">
			<?=$this->trans('category')?>: <?php echo $this->category->getName()?>
		</td>
		<td>
	</tr>
	<tr>
		<td width="30%" valign="top">
			<dl class="form">
				<dt>ID: </dt>
				<dd><div class="dd_content">
					<?php echo $this->category->getGalerycategoryId(); ?>
				</div></dd>
				<dt><?=$this->trans('parent_category')?></dt>
				<dd><div class="dd_content">
					<?php 
					foreach( $path as $category ){
						echo $category->getName() . ' &raquo; ';
					}
					?>
				</div></dd>
			</dl>
		</td>
		<td width="30%" valign="top">
			<dl class="form">
				<dt>
					<a href="productcategory.php?cmd=listProducts&galerycategory_id=<?php echo $this->category->getGalerycategoryId() ?>" title="<?=$this->trans('manage').' '.$this->trans('product').'s '.$this->trans('in').' '.$this->trans('this(f)').' '.$this->trans('category')?>"><img src="img/search2_small.png" border="0" align="right"></a>
					<?=$this->trans('product').'s'?>:
				</dt>
				<dd><div class="dd_content" style="padding-left:10px">
					<?php 
					//$products = eCommerce_SDO_Catalog::GetProductsByCategoryId( $this->category->getCategoryId() );
					//echo number_format( count($products), 0, '.', ',' );
					?>
				</div></dd>
				<dt>
					<a href="category.php?cmd=list&galerycategory_id=<?php echo $this->category->getGalerycategoryId() ?>" title="<?=$this->trans('manage').' Sub'.strtolower($this->trans('categories'))?>"><img src="img/search2_small.png" border="0" align="right"></a>
					Subcategorias:
				</dt>
				<dd><div class="dd_content" style="padding-left:10px">
					<?php
					$search = new eCommerce_Entity_Search();
					$search->setResultsPerPage( -1 ); 
					$rs = eCommerce_SDO_Galerycategory::SearchCategoriesByParentCategory( $search, $this->category->getGalerycategoryId() );
					$categories = count( $rs->getResults() );
					echo number_format( $categories, 0, '.', ',' );
					?>
				</div></dd>
			</dl>
		</td>
		<td width="40%" valign="top">
			<dl class="form">
				<dt><?=$this->trans('description')?>:</dt>
				<dd><div class="dd_content"><?php echo $this->category->getDescription(); ?></div></dd>
			</dl>
		</td>
	</tr>
	<tr height="6px">
		<td colspan="3"></td>
	</tr>
</table>
