<?php
include_once ( "header.php" );

$form = $this->form;
$viewConfig = $this->viewConfig;
$errors = $this->errors;

$ArrayImages = $this->ArrayImages;


?>

 
  
<h2 align="left" style="margin:0px;"><? echo $viewConfig['title']; ?></h2>

<form name="frmUser" id="frmUser" method="post" action="" enctype="multipart/form-data">
<div class="success"><?php if ( isset( $this->strSuccess ) ) echo $this->strSuccess; ?></div>
  <div class="error">
		<?php echo $errors->getDescription(); ?>
  </div><br />
  <?php $errors->getHtmlError("generalError"); ?>
	
	<fieldset>
		<legend>Information</legend>
		
		<dl class="form">
		<?php
			eCommerce_SDO_Core_Application_Form::displayForm( $form, $errors,'frmUser' );
		
		?>

		</dl>
		<?php
		/*
			<dt>Archivo: *</dt>
				<dd>	
					$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('AdminToolImages');
					
					$imageHandler->setArrImages( $ArrayImages);
					$imageHandler->setMaximum( 1 );
					echo ($imageHandler->createBOImages('Archivo'));
				</dd>
			<?php //$errors->getHtmlError("file_AdminToolImages"); ?>
		*/
		?>
	</fieldset>
	
	<input type="hidden" name="cmd" value="save">
	
	<input type="submit" value="Save" class="frmButton"> <input type="button" value="Cancel" class="frmButton" onClick="document.location.href='AdminTool.php'">
</form>

<?php
include_once ( "footer.php" );
?>