<?php
$products = $this->options->getResults();
$data = array();
foreach( $products as $product ){
	$data[] = get_object_vars( $product );
}
		

function renameStatus( $status ){
	return eCommerce_SDO_Order::GetStatusById( $status );
}

function renamePaymentMethod( $payment ){
	return eCommerce_SDO_Order::GetPaymentMethodById( $payment );
}

function renameProfileId( $profileId ){
	$profile = eCommerce_SDO_User::LoadUserProfile( $profileId );
	return "<a href='user.php?cmd=edit&id=". $profile->getProfileId(). "'>" . $profile->getEmail() . "</a>";
	
}

$listPanel = new GUI_ListPanel_ListPanel( "Order" );
$listPanel->setData( $data );

$listPanel->addCallBack( "status","renameStatus" );
$listPanel->addCallBack( "payment_method","renamePaymentMethod" );
$listPanel->addCallBack( "profile_id","renameProfileId" );

$hiddenColumns = array( "comments", "modification_date" ,"requiere_factura", "folio", "tipo_cargo", "tipo_tarjeta", "language" );



foreach( $hiddenColumns as $col ){
	$listPanel->addHiddenColumn( $col );
}

$viewConfig["columNamesOverride"]["payment_method"] = "M&eacute;todo de pago";
$viewConfig["columNamesOverride"]["creation_date"] = "Fecha de creación";
$viewConfig["columNamesOverride"]["currency"] = "Moneda";

foreach( $viewConfig['columNamesOverride'] as $colum => $rename){
	$listPanel->addColumnNameOverride($colum,$rename);
}


$listPanel = new GUI_ListPanel_Decorator_Sorter( $listPanel );
$listPanel->setOrderBy( $this->options->getOrderBy() );

$listPanel = new GUI_ListPanel_Decorator_RowActions( $listPanel );

// Modify
$actModify = new GUI_ListPanel_Action( '?cmd=edit&id={$order_id}', 'img/write.png' );
$actModify->setImageTitle( 'Edit Order' );
$listPanel->addAction( $actModify );


// Delete
$actDelete = new GUI_ListPanel_Action( '?cmd=delete&id={$order_id}', 'img/delete.png' );
$actDelete->setImageTitle( 'Delete Order' );
$actDelete->setOnClickEvent( 'return confirm("Are you sure you want to delete the order \"{$order_id}\"?");' );
$listPanel->addAction( $actDelete );

// Add "Search" capabilities
$listPanel = new GUI_ListPanel_Decorator_Searcher( $listPanel );
$listPanel->setFieldLabel( '<img src="img/search2_small.png">' );
$listPanel->setButtonLabel( 'Search' );
$listPanel->setClearButtonLabel( " Clear " );
$listPanel->setSearchString( $this->options->getKeywords() );

// Add paging capabilities
$listPanel = new GUI_ListPanel_Decorator_Pager( $listPanel );
$listPanel->setCurrentPage( $this->options->getPage() );

$listPanel->setTotalPages( $this->options->getTotalPages() );
$listPanel->setOffset( $this->options->getResultsPerPage() );

include_once ( "header.php" );

?>
<div class="container" style="width: 100%; text-align: left;">
	<div class="subtitle"><?=$this->trans("orders");?></div>
	<div class="success">
	<?php if ( isset( $this->strSuccess ) ) echo $this->strSuccess; ?>
</div>

<div class="error">
	<h2><?php if ( isset( $this->strError ) ) echo $this->strError;	?></h2>
</div>


	<?php
	$listPanel->display();
	?>
	<!-- <a href="?toExcel=1&<?php echo str_replace( "cmd=edit&",'',$_SERVER['QUERY_STRING']) ?>"><img src="img/excel.png" align="absmiddle" border="0" hspace="5">Export To Excel</a> -->
</div>
<?php
include_once ( "footer.php" );
?>