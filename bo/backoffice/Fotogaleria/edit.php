<?php
include_once ( "header.php" );
$form          = $this->form;
$viewConfig    = $this->viewConfig;
$errors        = $this->errors;

$ArrayImages   = $this->ArrayImages;

$cancelParams  = empty($this->cancelParams) ? '' : $this->cancelParams;
$cancelConfirm = empty($this->cancelConfirm) ? '' : $this->cancelConfirm;


function dataToInput( $value ){
	return htmlentities( $value, ENT_QUOTES, CHARSET_PROJECT );
}

$fotogaleria      = $this->fotogaleria;
$categorias       = $this->categorias;
$_SESSION['tipo'] = 'Fotogaleria';
?>

<!-- calendar stylesheet -->
<link rel="stylesheet" type="text/css" media="all" href="calendar/css/jscal2.css" title="jscal2" />
<!-- main calendar program -->
<script type="text/javascript" src="calendar/js/jscal2.js"></script>
<!-- language for the calendar -->
<script type="text/javascript" src="calendar/js/lang/<?=eCommerce_SDO_LanguageManager::LanguageToZendLang()?>.js"></script>


<!-- CSS GALERIA 2016 -->
<link rel="stylesheet" href="<?=ABS_HTTP_URL?>css/blueimp/style.css">
<link rel="stylesheet" href="<?=ABS_HTTP_URL?>css/blueimp/blueimp-gallery.min.css">
<link rel="stylesheet" href="<?=ABS_HTTP_URL?>css/blueimp/jquery.fileupload.css">
<link rel="stylesheet" href="<?=ABS_HTTP_URL?>css/blueimp/jquery.fileupload-ui.css">
<noscript><link rel="stylesheet" href="<?=ABS_HTTP_URL?>css/blueimp/jquery.fileupload-noscript.css"></noscript>
<noscript><link rel="stylesheet" href="<?=ABS_HTTP_URL?>css/blueimp/jquery.fileupload-ui-noscript.css"></noscript>
<!-- CSS GALERIA 2016 -->

<h2 align="left" style="margin:0px;"><? echo $viewConfig['title']; ?></h2>
<!-- <form name="frmUser" id="frmUser" method="post" action="" enctype="multipart/form-data">
	<div class="error"><?php echo $errors->getDescription(); ?></div><br />
	<?php $errors->getHtmlError("generalError"); ?>
	<?php eCommerce_SDO_Core_Application_Form::displayForm( $form, $errors,'frmUser' );?>
	<p align='left'>
		<input type="hidden" name="cmd" value="save">
		<input type="submit" value="<?=$this->trans('save')?>" class="frmButton">
		<input type="button" value="<?=$this->trans('cancel')?>" class="frmButton" onClick="<?php echo $cancelConfirm; ?>document.location.href='Fotogaleria.php<?php echo $cancelParams; ?>'">
	</p>
</form> -->

<form name="FotogaleriaForm" id="FotogaleriaForm" method="post" action="Fotogaleria.php?fotogaleria_id=<?php echo $fotogaleria->getFotogaleriaId();?>" onsubmit="" enctype="multipart/form-data">
	<div class="error" align="left"><?php echo $errors->getDescription(); ?></div>
	<fieldset>
		<legend><?=$this->trans('information')?></legend>
		<dl class="form">
			<dt><?=$this->trans('category')?>: </dt>
			<dd>
				<select class="frmInput" name="entity[galerycategory_id]" id="galerycategory_id">
					<?php echo $categorias; ?>
				</select>
				<a href="_formCategory.php" name="addcategory" id="addcategory" onClick="selectCategoria();" title="Agregar Categoría" class="render btn btn-primary">+</a>
			</dd>
			<?php $errors->getHtmlError( 'galerycategory_id' ); ?>

			<dt><?=$this->trans('Nombre')?>: *</dt>
			<dd>
				<input type="text" size="30" maxlength="100" class="frmInput" name="entity[titulo]" id="titulo" value="<?php echo dataToInput( $fotogaleria->getTitulo()  ); ?>">
			</dd>
			<?php $errors->getHtmlError( 'titulo'); ?>

			<!-- <dt><?=$this->trans('description')?>: *</dt>
			<dd>
				<textarea rows="10" cols="50" class="frmInput" name="entity[descripcion]" id="descripcion"
				><?php echo dataToInput( $fotogaleria->getDescripcion() ) ?></textarea>
			</dd> -->
			<?php // $errors->getHtmlError( 'descripcion'); ?>

			<!-- <dt><?//=$this->trans('image')?>: *</dt>
			<dd>
				<input type="file" class="frmInput" name="imagenes" size="30" />
				<input type="hidden" name="entity[imagenes]" value="<?php //echo dataToInput( $fotogaleria->getImagenes() ) ?>" />
				<?php
					/*if( $fotogaleria->getImagenes() != 0){
						echo '<a href="../file.php?id=' . $fotogaleria->getImagenes() . '&type=image" class="thickbox">'.$this->trans('show').' '.$this->trans('current_image').'</a>'.
						'<br /><label><input type="checkbox" name="entity[image_delete]" style="vertical-align:middle;margin-bottom:6px" />&nbsp;'.$this->trans('delete').' '.$this->trans('current_image').'</label>';
					}*/
				?>
			</dd> -->
			<?php // $errors->getHtmlError( 'imagenes'); ?>
		</dl>
	</fieldset>
	<input type="hidden" id="fotogaleria_id" name="entity[fotogaleria_id]" value="<?php echo dataToInput( $fotogaleria->getFotogaleriaId() ) ?>">	
	<input type="hidden" name="cmd" value="save">
</form>

<fieldset>
<?php 
// Galeria 2016
$formImages = array();
$formImages['name'] = 'FotogaleriaForm';
$formImages['elements'] = array();
$formImages['displayFieldset'] = false;
$formImages['elements'][] = array( 'title' => "Im&aacute;genes Secundarias", 'type'=>'array_images', 'options'=>array(), 'name'=>'entity[imagenes]', 'id'=>'imagenes');
eCommerce_SDO_Core_Application_Form::displayForm( $formImages, $errors,'frmUserX');
?>
</fieldset>

<input type="button" value="<?=$this->trans('save')?>" class="frmButton" id="submitBtn">
<input type="button" value="<?=$this->trans('cancel')?>" class="frmButton" onclick="window.location.href='Fotogaleria.php'">



<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="<?=ABS_HTTP_URL?>js/vendor/jquery.ui.widget.js"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="<?=ABS_HTTP_URL?>js/blueimp/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="<?=ABS_HTTP_URL?>js/blueimp/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="<?=ABS_HTTP_URL?>js/blueimp/canvas-to-blob.min.js"></script>
<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
<script src="<?=ABS_HTTP_URL?>js/blueimp/bootstrap.min.js"></script>
<!-- blueimp Gallery script -->
<script src="<?=ABS_HTTP_URL?>js/blueimp/jquery.blueimp-gallery.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="<?=ABS_HTTP_URL?>js/blueimp/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="<?=ABS_HTTP_URL?>js/blueimp/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="<?=ABS_HTTP_URL?>js/blueimp/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="<?=ABS_HTTP_URL?>js/blueimp/jquery.fileupload-image.js"></script>
<!-- The File Upload audio preview plugin -->
<script src="<?=ABS_HTTP_URL?>js/blueimp/jquery.fileupload-audio.js"></script>
<!-- The File Upload video preview plugin -->
<script src="<?=ABS_HTTP_URL?>js/blueimp/jquery.fileupload-video.js"></script>
<!-- The File Upload validation plugin -->
<script src="<?=ABS_HTTP_URL?>js/blueimp/jquery.fileupload-validate.js"></script>
<!-- The File Upload user interface plugin -->
<script src="<?=ABS_HTTP_URL?>js/blueimp/jquery.fileupload-ui.js"></script>
<!-- The main application script -->
<script src="<?=ABS_HTTP_URL?>js/blueimp/main.js"></script>
<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
<!--[if (gte IE 8)&(lt IE 10)]>
<script src="js/cors/jquery.xdr-transport.js"></script>
<![endif]-->
<script type="text/javascript">
$("#submitBtn").click(function(){
	$("#FotogaleriaForm").submit();
});
</script>

<script type="text/javascript">
$(document).ready(function(){  
    $(".render").fancybox({
        padding : 0,
        width : '400px',
        height  : '314px',
        autoSize : false,
        closeBtn : true,
        helpers : { 'overlay' :{'closeClick': false,},
                    'css':{'background-color' : '#77BC1F'},
                  },
        type : "iframe",
        scrolling: 'no',        
    });  
});  
</script>


<!-- <script type="text/javascript">
	function selectCategoria(){
		var idCategoria = jQuery("#galerycategory_id").val();
		alert("categoria seleccionnada"+idCategoria);
	}
</script> -->

<?php
include_once ( "footer.php" );
?>