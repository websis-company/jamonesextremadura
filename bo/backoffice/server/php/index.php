<?php
/*
 * jQuery File Upload Plugin PHP Example
* https://github.com/blueimp/jQuery-File-Upload
*
* Copyright 2010, Sebastian Tschan
* https://blueimp.net
*
* Licensed under the MIT license:
* http://www.opensource.org/licenses/MIT
*

error_reporting(E_ALL | E_STRICT);
require('UploadHandler.php');
$upload_handler = new UploadHandler();
*/
include_once "../../../common.php";
$config = Config::getGlobalConfiguration();
$options = array(
	'delete_type' => $config["delete_type"],
	'db_host' => $config["db_host"],
	'db_user' => $config["db_user"],
	'db_pass' => $config["db_pass"],
	'db_name' => $config["db_name"],
	'db_table' => $config["db_table"]
);


//error_reporting(E_ALL | E_STRICT);
require('UploadHandler.php');
class CustomUploadHandler extends UploadHandler {
	protected function initialize() { 
		$this->db = new mysqli(
				$this->options['db_host'],
				$this->options['db_user'],
				$this->options['db_pass'],
				$this->options['db_name']
		);

		#-- Validación para galerias en el FRONT de inmuebles
		/*if(isset($_GET["tipo"]) && $_GET["tipo"] == "front"){
			$_SESSION['tipo'] = $_GET["tipo"];
			$tot_images       = (int) $_GET["images"];
		}//end if*/

		switch ($_GET['tipo']){
			case 'Fotogaleria': 
				$this->options['max_number_of_files'] = null;
				$this->options['upload_dir']          = dirname($_SERVER['SCRIPT_FILENAME']).'/files/fotogalerias/';
				$this->options['upload_url']          = ABS_HTTP_URL.'bo/backoffice/server/php/files/fotogalerias/';
				$this->options['image_versions']      = array(
															''          => array('auto_orient' => true),
															'small'     => array('max_width' => 135,'max_height' => 95),
															'medium'    => array('max_width' => 270,'max_height' => 190),
															'large'     => array('max_width' => 900,'max_height' => 800),
															'thumbnail' => array( 'max_width' => 80, 'max_height' => 80	)
														);
			break;
			case 'Noticia': 
				$this->options['max_number_of_files'] = null;
				$this->options['upload_dir']          = dirname($_SERVER['SCRIPT_FILENAME']).'/files/noticias/';
				$this->options['upload_url']          = ABS_HTTP_URL.'bo/backoffice/server/php/files/noticias/';
				$this->options['image_versions']      =  array(
															''          => array('auto_orient' => true),
															'small'     => array('max_width' => 135,'max_height' => 95),
															'medium'    => array('max_width' => 270,'max_height' => 190),
															'large'     => array('max_width' => 900,'max_height' => 800),
															'thumbnail' => array( 'max_width' => 80, 'max_height' => 80	)
														);
			break;
			case 'Inmueble': 
				$this->options['max_number_of_files'] = null;
				$this->options['upload_dir']          = dirname($_SERVER['SCRIPT_FILENAME']).'/files/inmuebles/';
				$this->options['upload_url']          = ABS_HTTP_URL.'bo/backoffice/server/php/files/inmuebles/';
				$this->options['image_versions']      =  array(
																''          => array('auto_orient' => true),
																'small'     => array('max_width' => 150,'max_height' => 108),
																'medium'    => array('max_width' => 403,'max_height' => 403),
																'large'     => array('max_width' => 900,'max_height' => 800),
																'jumbo'     => array('max_width' => 1500,'max_height' => 1500),
																'thumbnail' => array( 'max_width' => 80, 'max_height' => 80	)
															);

			break;
			case 'Front':
				$this->options['max_number_of_files'] = 1;
				$this->options['upload_dir']          = dirname($_SERVER['SCRIPT_FILENAME']).'/files/';
				$this->options['upload_url']          = ABS_HTTP_URL.'bo/backoffice/server/php/files/';
				$this->options['image_versions']      =  array(
																''          => array('auto_orient' => true),
																'small'     => array('max_width' => 150,'max_height' => 108),
																'medium'    => array('max_width' => 403,'max_height' => 403),
																'large'     => array('max_width' => 980,'max_height' => 419),
																'jumbo'     => array('max_width' => 1500,'max_height' => 1500),
																'thumbnail' => array( 'max_width' => 80, 'max_height' => 80	)
															);
			break;
		}			
		parent::initialize();		
		$this->db->close();		
	}
	protected function handle_form_data($file, $index) {
		$file->title = @$_REQUEST['title'][$index];
		$file->description = @$_REQUEST['description'][$index];
	}

	protected function handle_file_upload($uploaded_file, $name, $size, $type, $error,$index = null, $content_range = null) {
		$file = parent::handle_file_upload($uploaded_file, $name, $size, $type, $error, $index, $content_range);

		if (empty($file->error)) {
			#-- Validación para saber si esta creada una galeria en backoffice
			/*if(!isset($_GET["galleryId"]) || $_GET["galleryId"] == ""){
				$galeria_id = "-1";
			}else{
				
			}//end if*/

			$galeria_id = $_GET["galleryId"];

			#-- Validación para galerias en el FRONT de inmuebles
			if($_GET["tipo"] == "Front"){
				$sql = 'INSERT INTO `'.$this->options['db_table'].'` (`name`, `size`, `type`, `title`, `description`, `galeria_id`, `tipo`)'.' VALUES (?, ?, ?, ?, ?, ?, ?)';
				$query = $this->db->prepare($sql);
				$query->bind_param(
						'sisssss',
						$file->name,
						$file->size,
						$file->type,
						$file->title,
						$file->description,
						$galeria_id,
						$_GET["tipo"]
				);
			}/*else{ //Galeria de imagenes backoffice
				$sql = 'INSERT INTO `'.$this->options['db_table'].'` (`name`, `size`, `type`, `title`, `description`, `galeria_id`)'.' VALUES (?, ?, ?, ?, ?, ?)';
				$query = $this->db->prepare($sql);
				$query->bind_param(
						'sisssi',
						$file->name,
						$file->size,
						$file->type,
						$file->title,
						$file->description,
						$galeria_id
				);
			}//end if*/

			$query->execute();
			$file->id = $this->db->insert_id;
		}
		return $file;
	}

	protected function set_additional_file_properties($file) { 
		parent::set_additional_file_properties($file);
		
		if ($_SERVER['REQUEST_METHOD'] === 'GET') {		
			$sql = 'SELECT `files_id`, `type`, `title`, `description` FROM `'
					.$this->options['db_table'].'` WHERE `name`=?';
			$query = $this->db->prepare($sql);
			
			$query->bind_param('s', $file->name);			
			$query->execute();
			$query->bind_result(
					$id,
					$type,
					$title,
					$description
			);
			while ($query->fetch()) {
				$file->id = $id;
				$file->type = $type;
				$file->title = $title;
				$file->description = $description;
			}
			
		}
	}

	public function delete($print_response = true) {
		$response = parent::delete(false);
		foreach ($response as $name => $deleted) {
			if ($deleted) {
				$sql = 'DELETE FROM `'
						.$this->options['db_table'].'` WHERE `name`=?';
				$query = $this->db->prepare($sql);
				$query->bind_param('s', $name);
				$query->execute();
			}
		}
		return $this->generate_response($response, $print_response);
	}

}

$upload_handler = new CustomUploadHandler($options);
?>

