<?php

$listPanel = new GUI_ListPanel_ListPanel( 'artist' );
$listPanel->setData( $this->data );


$hiddenColumns = array('biography_sp','biography_en','image_id');
foreach( $hiddenColumns as $col ){
	$listPanel->addHiddenColumn( $col );
}
$listPanel->addColumnNameOverride( 'artist_id', 'ID' );




$listPanel->addColumnAlignment( 'name', 'left');
$listPanel->addColumnAlignment( 'description', 'left');


// Add "Order By" capabilities
$listPanel = new GUI_ListPanel_Decorator_Sorter( $listPanel );
$listPanel->setOrderBy( $this->options->getOrderBy() );

// Add actions
$listPanel = new GUI_ListPanel_Decorator_RowActions( $listPanel );
$listPanel->setHeaderLabel( 'Sculptures' );

// Show Products
$actShowProducts = new GUI_ListPanel_Action( 'productartist.php?cmd=listProducts&artist_id={$artist_id}', 'img/application_cascade.png' );
$actShowProducts->setImageTitle( 'Manage Sculptures' );
$listPanel->addAction( $actShowProducts );

// Add actions
$listPanel = new GUI_ListPanel_Decorator_RowActions( $listPanel );
$listPanel->setHeaderLabel( 'Actions' );

// Modify

$actModify = new GUI_ListPanel_Action( '?cmd=edit&id={$artist_id}', 'img/write.png' );
$actModify->setImageTitle( 'Edit Artist' );
$listPanel->addAction( $actModify );

// Delete
$actDelete = new GUI_ListPanel_Action( '?cmd=delete&id={$artist_id}', 'img/delete.png' );
$actDelete->setImageTitle( 'Delete Artist' );
$actDelete->setOnClickEvent( 'return confirm("Are you sure you want to delete the artist \"{$name}\"?");' );
$listPanel->addAction( $actDelete );


// Add "Search" capabilities
$listPanel = new GUI_ListPanel_Decorator_Searcher( $listPanel );
$listPanel->setFieldLabel( '<img src="img/search2_small.png">' );
$listPanel->setButtonLabel( 'Search' );
$listPanel->setClearButtonLabel( " Clear " );
$listPanel->setSearchString( $this->options->getKeywords() );

// Add paging capabilities
$listPanel = new GUI_ListPanel_Decorator_Pager( $listPanel );
$listPanel->setCurrentPage( $this->options->getPage() );
$listPanel->setTotalPages( 1 ); //TODO:
$listPanel->setOffset( $this->options->getResultsPerPage() );

include_once ( "header.php" );
?>
	
	<div class="container" style="width: 100%; text-align: left;">
	
	<?php 
		//	$this->display( 'category/info.php' );
			?>
	<div class="subtitle">Artists</div>
	<div class="success">
		<?php if ( isset( $this->strSuccess ) ) echo $this->strSuccess; ?>
	</div>
	<div class="error">
		<h2><?php if ( isset( $this->strError ) ) echo $this->strError;	?></h2>
	</div>
	
	<a href="?cmd=add"><img src="img/wand.png" border="0" hspace="2" align="absmiddle">Add New Artist</a> <br>
	<br>
  <?php
		$listPanel->setTableWidth( '100%' );
		$listPanel->display();
	?>
	<a href="?toExcel=1&<?php echo str_replace( "cmd=edit&",'',$_SERVER['QUERY_STRING']) ?>"><img src="img/excel.png" align="absmiddle" border="0" hspace="5">Export To Excel</a>
</div>
<?php
include_once ( "footer.php" );
?>