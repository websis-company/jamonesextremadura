<?php

include_once ( "header.php" );
function dataToInput( $value ){
	return htmlentities( $value, ENT_QUOTES, 'UTF-8' );
}

$errors = $this->errors;
$artist = $this->artist;

$artistsTypes = array("sculptor"=>'sculptor','designer'=>'designer');

?>

<h2 align="left" style="margin:0px;"><? echo $this->strSubtitles; ?></h2>

<form name="frmCategory" id="frmCategory" method="post" action="artist.php?" onsubmit="" enctype="multipart/form-data">
  <div class="error" align="left"><?php echo $errors->getDescription(); ?></div>	
	<fieldset>
		<legend>Description</legend>
		<dl class="form">
			<dt>Name: *</dt>
			<dd>
				<input type="text" size="30" maxlength="100" class="frmInput"
				       name="entity[name]" id="name"
				       value="<?php echo dataToInput( $artist->getName()  ) ?>">
			</dd>
			<?php $errors->getHtmlError( 'name'); ?>

			<dt>Type: *</dt>
			<dd>
			<select name="entity[type]" id="type">
			<?php
			
			foreach($artistsTypes as $key => $artistType){
				echo "<option value='".$key."'";
				if($key == $artist->getType())
					echo "selected='selected'";
				echo ">".$artistType."</option>";
			}
			?>
			</select>
			</dd>
			<?php $errors->getHtmlError( 'type'); ?>
			
			<dt>Email: *</dt>
			<dd>
				<input type="text" size="30" maxlength="100" class="frmInput"
				       name="entity[email]" id="email"
				       value="<?php echo dataToInput( $artist->getEmail()  ) ?>">
			</dd>
			<?php $errors->getHtmlError( 'email'); ?>
			
			<dt>Url: *</dt>
			<dd>
				<input type="text" size="30" maxlength="100" class="frmInput"
				       name="entity[url]" id="url"
				       value="<?php echo dataToInput( $artist->getUrl()  ) ?>">
			</dd>
			<?php $errors->getHtmlError( 'url'); ?>
			
			<dt>Imagen:</dt>
			<dd>
			<?php
			$html .= '<input type="file" class="frmInput" name="image_id" id="image_id" >';
			$html .= '<input type="hidden" name="entity[image_id]" value="'.$artist->getImageId().'">';
			$html .= ( $artist->getImageId() > 0 ) ? " <a href='../file.php?id=".$artist->getImageId()."' target='_blank'>Ver imagen</a>" : '';
			echo $html;
		?>
			</dd>
			
			<dt>Biography (SP): *</dt>
			<dd>
				<textarea class="frmInput" name="entity[biography_sp]" id="biography_sp" cols='50' rows='5'><?php echo dataToInput( $artist->getBiographySp()  ); ?></textarea>
			</dd>
			<?php $errors->getHtmlError( 'biography_sp'); ?>
			
			<dt>Biography (EN): *</dt>
			<dd>
				<textarea class="frmInput" name="entity[biography_en]" id="biography_en"  cols='50' rows='5'><?php echo dataToInput( $artist->getBiographyEn()  ); ?></textarea>
			</dd>
			<?php $errors->getHtmlError( 'biography_en'); ?>
			
		</dl>
	</fieldset>	
	<input type="hidden" name="entity[artist_id]" value="<?php echo dataToInput( $artist->getArtistId() ) ?>">	
	<input type="hidden" name="cmd" value="save">
	<input type="submit" value="Save" class="frmButton">
</form>

<?php
include_once ( "footer.php" );
?>