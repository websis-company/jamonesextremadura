<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<?php
define ("BO_ADMIN", true);
require_once( "../common.php" );

$lang = eCommerce_SDO_Core_Application_LanguageManager::GetActualLanguage();
$config = Config::getGlobalConfiguration();

// secciones del BackOffice
$sections = array();
$sections["index"]          = array("include" => 1, "name" => $this->trans('home'));
$sections["user"]           = array("include" => 1, "name" => $this->trans('user').'s');
$sections["Banner"]           = array("include" => 1, "name" => $this->trans('Banner').'s Home');
$sections["Boletin"]           = array("include" => 1, "name" => "Newsletter");
$sections["Clientes"]           = array("include" => 1, "name" => "Testimoniales");
$sections["cupondescuento"]           = array("include" => 1, "name" => "Cupones de Descuento");
$sections["Galerycategory"] = array("include" => 1, "name" => 'Categorías de Gelería');
$sections["Galeria"]        = array("include" => 1, "name" => 'Gelería');
//$sections["Fotogaleria"]  = array("include" => 1, "name" => 'Fotogalería');
//$sections["Banner"]       = array("include" => 1, "name" => $this->trans('Banner').'s');
//$sections["artist"]       = array("include" => 0, "name" => $this->trans('artist').'s');
$sections["category"]       = array("include" => 1, "name" => "Categor&iacute;as");
$sections["product"]        = array("include" => 1, "name" => 'Productos');
//$sections["paquetes"]     = array("include" => 1, "name" => 'Paquetes');


//$sections["Brand"]          = array("include" => 1, "name" => $this->trans('brand').'s');
$sections["Talla"]          = array("include" => 1, "name" => 'Medidas');
// $sections["Color"]       = array("include" => 1, "name" => 'Caracter&iacute;sticas');
/*$sections["order"]        = array("include" => 1, "name" => "&Oacute;rdenes");*/

/*$sections["Promociones"]  = array("include" => 1, "name" => 'Promociones');*/
/*$sections["Banner"]       = array("include" => 1, "name" =>'Banner');*/
//$sections["Fotogaleria"]  = array("include" => 1, "name" => 'Fotogaleria');
//$sections["Videogaleria"] = array("include" => 1, "name" => 'Videogaleria');
//$sections["Alianzas"]     = array("include" => 1, "name" => 'Alianzas');
$sections["AdminTool"]      = array("include" => 1, "name" => $this->trans('system_configuration'));
$sections["order"]      = array("include" => 1, "name" => 'Ordenes');

?>

<title><?=$config["project_name"]?></title>
<link rel="stylesheet" href="js/thickbox/thickbox.css" type="text/css" media="screen">
<!--<script type="text/javascript" src="tiny_mce/tiny_mce.js"></script>-->
<!-- <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.css"> -->
<style type="text/css">
@import url('css/estilo.css');
@import url('css/form.css');
@import url('css/listPanel.css');
</style>


<!-- <link rel="stylesheet" type="text/css" href="../../js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" /> -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script> -->
<script src="http://code.jquery.com/jquery-1.8.1.min.js"></script>
<script type="text/javascript" src="js/thickbox/thickbox.js"></script>
<!-- <script type="text/javascript" src="../../js/fancybox/jquery.fancybox.js?v=2.1.5"></script> -->
</head>
<body bgcolor="#fefefe">
<div align="center" <?=(Util_Server::getScript() == 'index.php'  && $show)?'style="margin-top:200px"':''?>>
<table border="0" id="table1" cellspacing="0" width="779" align="center" <?=(Util_Server::getScript() == 'index.php')?'style="background-image:url('.ABS_HTTP_URL.'bo/backoffice/img/bglogin.png);"':'bgcolor="#EFEFEF"'?>>
  <tr>
    <td valign="top" <?=(Util_Server::getScript() == 'index.php'  && $show)?'':'bgcolor="#ffffff"'?>>
    <table border="0" width="100%" id="table2" cellspacing="0" cellpadding="0" <?=(Util_Server::getScript() == 'index.php')?'':'style="border: 1px solid #fff"'?> >
      <tr>
        <td <?=(Util_Server::getScript() == 'index.php'  && $show)?'':'bgcolor="#ffffff"'?>>
          <table width="100%" cellpadding="0" cellspacing="0">
          <?php  if(Util_Server::getScript() == 'index.php'  && $show){ ?>
           <tr>
            	<td colspan="2" >
            		<img src="<?=ABS_HTTP_URL?>bo/backoffice/ima/1px.gif" border="0" height="141">
            	</td>
            </tr>
            <?php } else{?>
             <tr>
                           <td colspan="2" bgcolor="<?=$config["bakground_logo"]?>" style="height: 120px;">
                             <a href="<?=ABS_HTTP_URL?>bo/backoffice/"><img src="<?=$config["logo_project"]?>" border="0" ></a> 
                           </td>
                         </tr> 
            <?php }?>
            <?php        	
							if( eCommerce_FrontEnd_BO_Authentification::verifyAuthentification(false) ){
								$userProfile = eCommerce_FrontEnd_Util_UserSession::GetIdentity();
						?>
            <tr bgcolor="#eeeeee">
            	<td align="left" valign="middle" width="30%" style="padding: 5px;">
            		<b><?php echo "Bienvenido " .$userProfile->getFirstName(); ?></b> | 
                	<a href="index.php?cmd=logout">Salir</a>
                &nbsp;&nbsp;&nbsp;
            	</td>
              <td align="right" valign="middle">
              <?php
              	foreach($sections as $key => $section) {
              		if($section["include"]) {
              ?>
              	<a href='<?=$key?>.php'><?=$section["name"]?></a> |
              <?php
              		}
              	}
              ?>
      				</td>
            </tr>
           <?php 
							} else{
		           	/*echo '
		           		<tr bgcolor="#eeeeee">
		            	<td align="left" valign="bottom" width="30%" style="padding: 5px;" colspan="2">&nbsp;</td>
		            	</tr>';	*/
		          }
           ?>
          </table>
        </td>
      </tr>
      <tr>
        <td bgcolor="#EFEFEF"><img border="0" src="ima/1px.gif" width="1" height="1"></td>
      </tr>
      <?  if(Util_Server::getScript() != 'index.php'){?>
      <tr>
        <td align="center" style="padding: 20px">
        	<h1 align="center" style="margin:0px;color: #19a7da; border-bottom: 1px solid #eee">Panel de administraci&oacute;n</h1>
        	</td>
      </tr>      
      <?php }?>
      <tr>
      	<td align="center" style="padding: 20px"><?  if(Util_Server::getScript() != 'index.php'){?>
      		<table width="100%">
      			<tr>
      				<td width="1" valign="top"><? include_once 'menu.php';?></td>
      				<td valign="top" style="padding-left:15px;"><? }?>
          