<?php
include_once ( "header.php" );

$form = $this->form;
$viewConfig = $this->viewConfig;
$errors = $this->errors;

$ArrayImages = $this->ArrayImages;

$cancelParams = empty($this->cancelParams) ? '' : $this->cancelParams;
$cancelConfirm= empty($this->cancelConfirm) ? '' : $this->cancelConfirm;
?>

  <!-- calendar stylesheet -->
  <link rel="stylesheet" type="text/css" media="all" href="calendar/calendar-win2k-cold-1.css" title="win2k-cold-1" />
  <!-- main calendar program -->
  <script type="text/javascript" src="calendar/calendar.js"></script>
  <!-- language for the calendar -->
  <script type="text/javascript" src="calendar/lang/calendar-es.js"></script>
  <!-- the following script defines the Calendar.setup helper function, which makes
       adding a calendar a matter of 1 or 2 lines of code. -->
  <script type="text/javascript" src="calendar/calendar-setup.js"></script>
  
<h2 align="left" style="margin:0px;"><? echo $viewConfig['title']; ?></h2>

<form name="frmUser" id="frmUser" method="post" action="" enctype="multipart/form-data">

  <div class="error">
		<?php echo $errors->getDescription(); ?>
  </div><br />
  <?php $errors->getHtmlError("generalError"); ?>
	

		<?php
			eCommerce_SDO_Core_Application_Form::displayForm( $form, $errors,'frmUser' );
		
		?>

<p align='left'>
	<input type="hidden" name="cmd" value="save">
	
	<input type="submit" value="Guardar" class="frmButton"> <input type="button" value="Cancelar" class="frmButton" onClick="<?php echo $cancelConfirm; ?>document.location.href='Alianzas.php<?php echo $cancelParams; ?>'">
</p>
</form>

<?php
include_once ( "footer.php" );
?>