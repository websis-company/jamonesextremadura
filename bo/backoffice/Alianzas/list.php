<?php

$users = $this->options->getResults();

$viewConfig = $this->viewConfig;

//params of the variable viewConfig
$paramsUsed = "name,title,id";
$paramsArrUsed = "columNamesOverride,hiddenColums,hiddenFields";

$paramsUsed = explode(",",$paramsUsed);

foreach( $paramsUsed as $param){
	$viewConfig[ $param ] = empty( $viewConfig[ $param ] ) ? '' : $viewConfig[ $param ];
}


$paramsArrUsed = explode(",",$paramsArrUsed);

foreach( $paramsArrUsed as $param){
	$viewConfig[ $param ] = empty( $viewConfig[ $param ] ) ? array() : $viewConfig[ $param ];
}


$data = array();
foreach($users as $user){
	$data[] = get_object_vars( $user );
}
		

$listPanel = new GUI_ListPanel_ListPanel( $viewConfig['name'] );
$listPanel->setData( $data );

foreach( $viewConfig['columNamesOverride'] as $colum => $rename){
	$listPanel->addColumnNameOverride($colum,$rename);
}


foreach( $viewConfig['hiddenColums'] as $col ){
	$listPanel->addHiddenColumn( $col );
}

foreach( $viewConfig['hiddenFields'] as $name=> $value ){
	$listPanel->addHiddenField($name, $value );
}

// Add "Order By" capabilities
$listPanel = new GUI_ListPanel_Decorator_Sorter( $listPanel );
$listPanel->setOrderBy( $this->options->getOrderBy() );


// Add actions
$listPanel = new GUI_ListPanel_Decorator_RowActions( $listPanel );
$listPanel->setHeaderLabel( 'Acciones' );

// Modify
$actModify = new GUI_ListPanel_Action( '?cmd=edit&id=' . $viewConfig['id'] . '', 'img/write.png' );
$actModify->setImageTitle( 'Editar '. $viewConfig['name'] );
$listPanel->addAction( $actModify );

// Delete
$actDelete = new GUI_ListPanel_Action( '?cmd=delete&id=' . $viewConfig['id'] . '', 'img/delete.png' );
$actDelete->setImageTitle( 'Eliminar '. $viewConfig['name'] );
$actDelete->setOnClickEvent( 'return confirm("Esta seguro que desea eliminar el registro ' . $viewConfig['id'] . ' ?");' );
$listPanel->addAction( $actDelete );

// Add "Search" capabilities
$listPanel = new GUI_ListPanel_Decorator_Searcher( $listPanel );
$listPanel->setFieldLabel( '<img src="img/search2_small.png">' );
$listPanel->setButtonLabel( 'Buscar' );
$listPanel->setClearButtonLabel( " Limpiar " );
$listPanel->setSearchString( $this->options->getKeywords() );

// Add paging capabilities
$listPanel = new GUI_ListPanel_Decorator_Pager( $listPanel );
$listPanel->setCurrentPage( $this->options->getPage() );

//echo $this->totalPages;
$listPanel->setTotalPages( $this->options->getTotalPages() ); //TODO:

//$listPanel->setOffsetIncrement( $this->options->getResultsPerPage() );
$listPanel->setOffset( $this->options->getResultsPerPage() );

include_once ( "header.php" );
?>

<div class="container" style="width: 100%; text-align: left;">
<div class="subtitle"><?php echo $viewConfig['title']; ?></div>
<div class="success">
	<?php if ( isset( $this->strSuccess ) ) echo $this->strSuccess; ?>
</div>
<div class="error">
	<h2><?php if ( isset( $this->strError ) ) echo $this->strError;	?></h2>
</div>

<a href="?cmd=add"><img src="img/wand.png" border="0" hspace="2" align="absmiddle">Agregar <?php echo $viewConfig['name']; ?></a> <br>
<br>
  <?php
		$listPanel->setTableWidth( '100%' );
		$listPanel->display();
		?>
</div>
<?php
include_once ( "footer.php" );
?>