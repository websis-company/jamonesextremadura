<?php 
	$user          = $this->user;
	$ArrRoles      = $this->ArrRoles;
	$ArrRoles      = array_combine( $ArrRoles, $ArrRoles );
	$ArrSaludo     = $this->ArrSaludo;
	$ArrSaludo     = array_combine( $ArrSaludo, $ArrSaludo );
	$ArrConfirmado = $this->ArrConfirmado;
	$ArrConfirmado = array_combine( $ArrConfirmado, $ArrConfirmado );
	$strSubtitles = $this->strSubtitles;
?>

<div class="row">
	<div class="col-md-12">
		<div class="box box-danger">
			<div class="box-header with-border">
				<h3><? echo $strSubtitles; ?></h3>
			</div>
			<form name="frmUser" id="frmUser" method="post" action="user.php" enctype="multipart/form-data">
				<input type="hidden" name="oldPass" id="oldPass" value="<?=$user->getPassword()?>">
				<div class="box-body">
					<div class="form-group">
						<h4 class="box-title">
							Información de la Cuenta
						</h4>
					</div>
					<div class="form-group">
						<label for="name">Email :</label>
						<input type="email" class="form-control" id="email" name="entity[email]" placeholder="Email" value="<?php echo $user->getEmail(); ?>" required="required">
					</div>
					<div class="form-group">
						<label for="name">Password :</label>
						<input type="password" class="form-control" id="password" name="entity[password]" placeholder="Password" value="<?php echo $user->getPassword(); ?>" required="required">
					</div>
					<div class="form-group">
						<label for="role">Rol :</label>
						<?php Util_HTML::CreateDropDown( 'entity[role]', 'role', $ArrRoles, $user->getRole(), 'form-control' ); ?>
					</div>
					<div class="form-group">
						<h4 class="box-title">
							Información Personals
						</h4>
					</div>
					<div class="form-group">
						<label for="name">Nombre :</label>
						<input type="text" class="form-control" id="first_name" name="entity[first_name]" placeholder="Nombre" value="<?php echo $user->getFirstName(); ?>" required="required">
					</div>
					<div class="form-group">
						<label for="name">Apellidos :</label>
						<input type="text" class="form-control" id="last_name" name="entity[last_name]" placeholder="Apellidos" value="<?php echo $user->getLastName();?>" required="required">
					</div>
					<div class="form-group">
						<label for="name">Confirmado :</label>
						<?php 
						Util_HTML::CreateDropDown( 'entity[confirmado]', 'confirmado', $ArrConfirmado, $user->getConfirmado(), 'form-control' );
						?>
					</div>
				</div>
				<div class="box-footer">
					<input type="hidden" name="cmd" value="save">
					<input type="hidden" name="entity[id]" value="<?php echo $user->getProfileId()?>">
					<button type="submit" class="btn btn-primary pull-right" style="margin-left: 15px;">Aceptar</button>
					<button type="button" class="btn btn-default pull-right" onClick="closeForm();">Cancelar</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	function closeForm(){
		$("#divEditForm").slideUp();
		$("#frmUser")[0].reset();
		$("#btnEditForm").show(function(e){
			$("#btnEditForm").attr('disabled',false);
			$(".loading").html('');
			$(".loading").show('');
		});
	}// end function

	$(document).ready(function(e){
		$("#frmUser").validate({
			debug: false,
		});
		//Date range picker with time picker
        //$('#rangeTime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
    });

</script>