<div class="box box-widget widget-user-2">
	<!-- Add the bg color to the header using any of the bg-* classes -->
	<div class="widget-user-header bg-red">
		<h3 class="widget-user-username"><?=$this->trans('user')?>: <?php echo $this->profile; // __toString() invokation = Full Name; ?></h3>
	</div>
	<div class="box-footer">
		<div class="row">
			<div class="col-sm-4 border-right">
				<div class="description-block">
					<h5 class="description-header">ID</h5>
					<span class="description-text"><?php echo $this->profile->getProfileId(); ?></span>
				</div>
				<!-- /.description-block -->
			</div>
			<!-- /.col -->
			<div class="col-sm-4 border-right">
				<div class="description-block">
					<h5 class="description-header">E-mail</h5>
					<span class="description-text"><?php echo $this->profile->getEmail(); ?></span>
				</div>
				<!-- /.description-block -->
			</div>
			<!-- /.col -->
			<div class="col-sm-4">
				<div class="description-block">
					<h5 class="description-header">Role</h5>
					<span class="description-text"><?php echo $this->profile->getRole(); ?></span>
				</div>
				<!-- /.description-block -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</div>
</div>




