<?php
$lang = eCommerce_SDO_Core_Application_LanguageManager::GetActualLanguage();

$users = $this->options->getResults();
$data = array();
foreach($users as $user){
	$data[] = get_object_vars( $user );
}
		
$listPanel = new GUI_ListPanel_ListPanel( 'profile' );
$listPanel->setData( $data );


$hiddenColumns = array('password','confirm_password','saludo','confirmado');

foreach( $hiddenColumns as $col ){
	$listPanel->addHiddenColumn( $col );
}

$columnNamesOverride = array('profile_id' => 'ID', 'first_name' => $this->trans('first_name'), 'last_name' => $this->trans('last_name'), 'role' => $this->trans('role'));

foreach( $columnNamesOverride as $col => $name ){
	$listPanel->addColumnNameOverride( $col, $name );
}


// Add "Order By" capabilities
$listPanel = new GUI_ListPanel_Decorator_Sorter( $listPanel );
$listPanel->setOrderBy( $this->options->getOrderBy() );

// Address Action
$listPanel = new GUI_ListPanel_Decorator_RowActions( $listPanel );
$listPanel->setHeaderLabel( $this->trans('address') );

// Modify Address
/*$actModify = new GUI_ListPanel_Action( '?cmd=editAddress&id={$profile_id}', 'img/house.png' );
$actModify->setImageTitle( $this->trans('edit').' '.$this->trans('address') );
$listPanel->addAction( $actModify );*/
$actModify = new GUI_ListPanel_Action('javascript:;', 'fa fa-home');
$actModify->setImageTitle($this->trans('address'));
$actModify->setOnClickEvent( 'editForm("editAddress","{$profile_id}")' );
$listPanel->addAction( $actModify );


// Add actions
$listPanel = new GUI_ListPanel_Decorator_RowActions( $listPanel );
$listPanel->setHeaderLabel( $this->trans('actions') );

// Modify
$actModify = new GUI_ListPanel_Action('javascript:;', 'fa fa-pencil-square-o');
$actModify->setImageTitle(  $this->trans('edit').' '.$this->trans('user') );
$actModify->setOnClickEvent( 'editForm("edit","{$profile_id}")' );
$listPanel->addAction( $actModify );


// Delete
$actDelete = new GUI_ListPanel_Action( '?cmd=delete&id={$profile_id}', 'fa fa-trash-o');
$actDelete->setImageTitle( $this->trans('delete').' '.$this->trans('user') );
$actDelete->setOnClickEvent( 'return confirm("'.$this->trans('?_confirm_delete').' '.strtolower($this->trans('the')).' '.strtolower($this->trans('user')).' \"{$first_name}\"?");' );
$listPanel->addAction( $actDelete );

// Add "Search" capabilities
$listPanel = new GUI_ListPanel_Decorator_Searcher( $listPanel );
$listPanel->setFieldLabel( '<img src="img/search2_small.png" style="vertical-align:middle">&nbsp;' );
$listPanel->setButtonLabel($this->trans('search'));
$listPanel->setClearButtonLabel($this->trans('clear'));
$listPanel->setSearchString( $this->options->getKeywords() );

// Add paging capabilities
$listPanel = new GUI_ListPanel_Decorator_Pager( $listPanel );
$listPanel->setCurrentPage( $this->options->getPage() );
$listPanel->setPagingLabel($this->trans('page'));
$listPanel->setPreviousLabel('&#9668; '.$this->trans('previous'));
$listPanel->setNextLabel($this->trans('next').' &#9658;');
$listPanel->setFirstPageLabel($this->trans('first(f)').' '.$this->trans('page'));
$listPanel->setLastPageLabel($this->trans('last(f)').' '.$this->trans('page'));
$listPanel->setOffsetLabel($this->trans('results_per').' p&aacute;gina');

//echo $this->totalPages;
$listPanel->setTotalPages( $this->options->getTotalPages() ); //TODO:

//$listPanel->setOffsetIncrement( $this->options->getResultsPerPage() );
$listPanel->setOffset( $this->options->getResultsPerPage() );

//include_once ( "header.php" );
include_once ( "includes/header.php" );
?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1> <?php echo $this->trans('user')?>s</h1>
		<ol class="breadcrumb">
			<li><a href="index.php"><i class="fa fa-home"></i> Home</a></li>
			<li><a href="user.php">Usuarios</a></li>
		</ol>
	</section>

    <div id="divEditForm" class="col-xs-12 col-md-8 col-md-offset-2" style="display: none;"></div>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        	<div class="box">
        		<div class="box-header">
                    <div class="cleafix"></div>

        			<div class="box-title pull-right">
        				<!--<a href="?cmd=add" class="btn btn-default">
	        				<img src="img/wand.png" border="0" hspace="2" align="absmiddle">
	        				<?=$this->trans('add').' '.$this->trans('new').' '.$this->trans('user')?>
        				</a>-->
                        <div class="loading"></div>
                        <a id="btnEditForm" class="btn btn-default" onclick="editForm('new');" style="cursor: pointer;">
                            <!-- <img src="img/wand.png" border="0" hspace="2" align="absmiddle"> -->
                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                            <?=$this->trans('add').' '.$this->trans('new').' '.$this->trans('user')?>
                        </a>
                    </div>
        		</div><!-- /.box-header -->
        		<div class="box-body" style="overflow-x: auto;">
        		<?php
        			$listPanel->setTableWidth( '100%' );
        			$listPanel->display();
        		?>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div><!-- /.col -->
      </div><!-- /.row -->
    </section><!-- /.content -->	
</div><!-- /.content-wrapper -->

<?php
include_once ( "includes/footer.php" );
//include_once ( "footer.php" );
?>


<script type="text/javascript">
    $(function () {
        $("#table").DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },
            "order": [[ 0, "desc" ]]
        });
    });

    function editForm(type,id){
        console.log(type + " " + id);

        if(type == 'new'){
            cmd = 'cmd=add';
        }else if(type == 'edit'){
            cmd = 'cmd=edit&id='+id;
        }else if(type == 'editAddress'){
            cmd = 'cmd=editAddress&id='+id;
        }//end if

        var Url = "<?php echo ABS_HTTP_URL?>";
        $.ajax({
            'type'    :"GET",
            'url'     : Url+'bo/backoffice/user.php',
            'data'    : cmd,
            'dataType': "html",
            beforeSend: function(data){
                $("#btnEditForm").attr('disabled',true);
                $(".loading").html('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');
            },
            success:  function (data){
                $("#btnEditForm").hide();
                $(".loading").hide();
                $("#divEditForm").html(data);
                $("#divEditForm").slideDown();
                $('html, body').stop().animate({scrollTop:0},500,'swing',function(){});
            }
        });       
    }

</script>