<?php
//include_once ( "header.php" );
function dataToInput( $value ){
	return htmlentities( $value, ENT_QUOTES, CHARSET_PROJECT );
}
$errors  = $this->errors;
$address = $this->address;
?>

<div class="row">
	<div class="col-md-12">
		<div class="box box-danger">
			<div class="box-header with-border">
				<h3><?=$this->trans('edit').' '.$this->trans('address')?></h3>
			</div>
			<form name="frmUser" id="frmUser" method="post" action="user.php" onsubmit="">
				<div class="box-body">
					<div class="form-group">
						<?php echo $this->display( 'user/info.php' ); ?>
					</div>

					<div class="form-group">
						<h4 class="box-title">
							Información del Domicilio
						</h4>
					</div>
					<div class="form-group">
						<div class="col-xs-12 col-md-6">
							<label for="phone">Teléfono :</label>
							<input type="text" size="50" maxlength="100" class="form-control" name="entity[phone]" id="phone" value="<?php echo dataToInput( $address->getPhone()  ) ?>" required="required">
						</div>
						<div class="col-xs-12 col-md-6">
							<label for="street"><?=$this->trans('street')?>:</label>
							<input type="text" size="50" maxlength="100" class="form-control" name="entity[street]" id="street" value="<?php echo dataToInput( $address->getStreet()  ) ?>" required="required" >
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12 col-md-6">
							<label for="street4"><?=$this->trans('number')?> Interior:</label>
							<input type="text" size="50" maxlength="100" class="form-control" name="entity[street4]" id="street4" value="<?php echo dataToInput($address->getStreet4())?>" required="required">
						</div>
						<div class="col-xs-12 col-md-6">
							<label for="street3"><?=$this->trans('number')?> Exterior:</label>
							<input type="text" size="50" maxlength="100" class="form-control" name="entity[street3]" id="street3" value="<?php echo dataToInput($address->getStreet3())?>">
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12 col-md-6">
							<label for="street2"><?=$this->trans('residential_development')?>:</label>
							<input type="text" size="50" maxlength="100" class="form-control" name="entity[street2]" id="street2" value="<?php echo dataToInput($address->getStreet2())?>" required="required">
						</div>
						<div class="col-xs-12 col-md-6">
							<label for="zip_code"><?=$this->trans('zip_code')?>:</label>
							<input type="number" size="5" maxlength="5" class="form-control" name="entity[zip_code]" id="zip_code" value="<?php echo dataToInput($address->getZipCode())?>" required="required">
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12 col-md-6">
							<label for="city"><?=$this->trans('city')?>:</label>
							<input type="text" size="30" maxlength="30" class="form-control" name="entity[city]" id="city" value="<?php echo dataToInput( $address->getCity()  ) ?>" required="required">
						</div>
						<div class="col-xs-12 col-md-6">
							<label for="country"><?=$this->trans('country')?>:</label>
							<?php echo eCommerce_SDO_Core_Application_CountryManager::GetHTMLSelect( 'entity[country]', $address->getCountry(), 'country', 'form-control' , 'onChange="_refreshSTATE( this.value )"');?>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12 col-md-6">
							<label for="state"><?=$this->trans('state')?>:</label>
							<?php
								$states = eCommerce_SDO_CountryManager::GetStatesOfCountry( $address->getCountry(), true, 'entity[state]', $address->getState(),'state','form-control', 'required="requiered"', true,'../' );
								echo ( !is_array($states)) ? $states : $states["div"];
							?>	
						</div>
					</div>
				</div>
				<div class="box-footer">
					<input type="hidden" name="entity[profile_id]" value="<?php echo $address->getProfileId() ?>">
					<input type="hidden" name="cmd" value="saveAddress" >
					<button type="submit" class="btn btn-primary pull-right" style="margin-left: 15px;">Aceptar</button>
					<button type="button" class="btn btn-default pull-right" onClick="closeForm();">Cancelar</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	function closeForm(){
		$("#divEditForm").slideUp();
		$("#frmUser")[0].reset();
		$("#btnEditForm").show(function(e){
			$("#btnEditForm").attr('disabled',false);
			$(".loading").html('');
			$(".loading").show('');
		});
	}// end function

	$(document).ready(function(e){
		$("#frmUser").validate({
			debug: false,
		});
		//Date range picker with time picker
        //$('#rangeTime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
    });

</script>