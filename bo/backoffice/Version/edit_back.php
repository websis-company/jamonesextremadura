<?php
include_once ( "header.php" );
$form               = $this->form;
$viewConfig         = $this->viewConfig;
$errors             = $this->errors;
$categoriasActuales = $this->categoriasActuales;
$ArrayImages        = $this->ArrayImages;
$product_id         = $this->product_id;
$cancelParams       = empty($this->cancelParams) ? '' : $this->cancelParams;
$cancelConfirm      = empty($this->cancelConfirm) ? '' : $this->cancelConfirm;
$cmdProducto        = $this->cmdProducto;

$categoria_actual   = $this->categoria_actual;


$cancelBtn          = 'product.php';
$Keywords           = explode(',',$this->Keywords);
$str_keyword        = '';
foreach ($Keywords as $Keyword){
	$str_keyword .= '"'.$Keyword.'",';
}
$str_keyword = substr($str_keyword,0,-1);
$tipo = $this->tipo;

#-- Admin Tool (Costo Tela Banner =  Acabado * M2)
$admin_tool = eCommerce_SDO_AdminTool::LoadAdminTool(1);
$acabado    = $admin_tool->getAcabado();
?>
<link rel="stylesheet" href="css/chosen/prism.css">
<link rel="stylesheet" href="css/chosen/chosen.css">
<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<script language="javascript" type="text/javascript">
<?php if ($categoria_actual==1){?>
$(document).ready(function(){	
	$("#alto").change(function(){
		var precio;
		var base     =parseFloat($("#base").val());
		var alto     = parseFloat($("#alto").val());
		var metro2;
		var metro_lineal ;
		var preciom2 = parseFloat($("#precio_metro").val());
		metro2       = base * alto;
		metro2       = metro2.toFixed(2);
		$("#metro_cuadrado").val(metro2);

		metro_lineal = (base*2)+(alto*2);
		metro_lineal = metro_lineal.toFixed(2);
		$("#metro_lineal").val(metro_lineal);

		precio = metro2 * preciom2;
		precio = precio.toFixed(2);
		$("#price").val(precio);
		});

	$("#precio_metro").change(function(){
		var precio;
		var base     =parseFloat($("#base").val());
		var alto     = parseFloat($("#alto").val());
		var metro2;
		var metro_lineal ;
		var preciom2 = parseFloat($("#precio_metro").val());
		metro2       = base * alto;
		metro2       = metro2.toFixed(2);
		$("#metro_cuadrado").val(metro2);

		metro_lineal = (base*2)+(alto*2);
		metro_lineal = metro_lineal.toFixed(2);
		$("#metro_lineal").val(metro_lineal);

		precio = metro2 * preciom2;
		precio = precio.toFixed(2);
		$("#price").val(precio);
		});

	$("#base").change(function(){
		var precio;
		var base     =parseFloat($("#base").val());
		var alto     = parseFloat($("#alto").val());
		var metro2;
		var metro_lineal ;
		var preciom2 = parseFloat($("#precio_metro").val());
		metro2       = base * alto;
		metro2       = metro2.toFixed(4);
		$("#metro_cuadrado").val(metro2);

		metro_lineal = (base*2)+(alto*2);
		metro_lineal = metro_lineal.toFixed(2);
		$("#metro_lineal").val(metro_lineal);

		precio = metro2 * preciom2;
		precio = precio.toFixed(2);
		$("#price").val(precio);
		});
});
<?php } ?>

<?php if ($categoria_actual==2){?>
$(document).ready(function(){	
	$("#alto").change(function(){
		var precio;
		var base     =parseFloat($("#base").val());;
		var alto     = parseFloat($("#alto").val());;
		var metro2;
		var metro_lineal ;
		var preciom2 = parseFloat($("#costo_banner").val());
		var acabado  =  '<?php echo $acabado ?>';

		metro2       = base * alto;
		metro2       = metro2.toFixed(4);

		$("#metro_cuadrado").val(metro2);

		metro_lineal = (base*2)+(alto*2);
		metro_lineal = metro_lineal.toFixed(2);
		$("#metro_lineal").val(metro_lineal);

		var costo_telabanner = metro2 * acabado;
		$("#costo_telabanner").val(costo_telabanner);

		precio = metro_lineal * preciom2;
		precio = precio.toFixed(2);
		$("#price").val(precio);
	});
	$("#base").change(function(){
		var precio;
		var base =parseFloat($("#base").val());;
		var alto = parseFloat($("#alto").val());;
		var metro2;
		var metro_lineal ;
		var preciom2 = parseFloat($("#costo_banner").val());
		var acabado  =  '<?php echo $acabado ?>';
		metro2 = base * alto;
		metro2 = metro2.toFixed(4);
		$("#metro_cuadrado").val(metro2);
		metro_lineal = (base*2)+(alto*2);
		metro_lineal = metro_lineal.toFixed(2);
		$("#metro_lineal").val(metro_lineal);

		var costo_telabanner = metro2 * acabado;
		$("#costo_telabanner").val(costo_telabanner);

		precio = metro_lineal * preciom2;
		precio = precio.toFixed(2);
		$("#price").val(precio);
		});
	$("#costo_banner").change(function(){
		var precio;
		var base =parseFloat($("#base").val());;
		var alto = parseFloat($("#alto").val());;
		var metro2;
		var metro_lineal ;
		var preciom2 = parseFloat($("#costo_banner").val());
		metro2 = base * alto;
		metro2 = metro2.toFixed(4);
		var acabado  =  '<?php echo $acabado ?>';

		$("#metro_cuadrado").val(metro2);
		metro_lineal = (base*2)+(alto*2);
		metro_lineal = metro_lineal.toFixed(2);
		$("#metro_lineal").val(metro_lineal);

		var costo_telabanner = metro2 * acabado;
		$("#costo_telabanner").val(costo_telabanner);

		precio = metro_lineal * preciom2;
		precio = precio.toFixed(2);
		$("#price").val(precio);
		});
});
<?php } ?>
$(document).ready(function(){	
	validaPadres();
	function validaPadres(){
		$(".categorias").each(function(){
			var padre_id = $(this).val();
			var product_id = $("#product_id").val();
			var msgdata = $("#datacategoria");			
			$.ajax({
	             type: "POST",
	             data: "",
	             url: "Version/validaPadres.php?padre_id="+padre_id+"&product_id="+product_id,	  
	             async : true,               
	             success: 
					function(data){																											
	            	 msgdata.html(data);	  							                       
					}
	    	 });
		});
	}
		$(".categorias").click(function(){
			var padre_id = $(this).val();
			var product_id = $("#product_id").val();
			var msgdata = $("#datacategoria"+padre_id);
			var isChecked = $(this).is(':checked');
			if(isChecked){
			 $.ajax({
                 type: "POST",
                 data: "",
                 url: "Version/subcategorias.php?padre_id="+padre_id+"&product_id="+product_id,
                 beforeSend: function(){								
                         msgdata.html("<img src='img/loading.gif' alt='Cargando' title='Cargando' width='30' />");
                 },
                 success: 
					function(data){	
                	  	msgdata.html("");							
                        msgdata.html(data);							                       
						}
        	 });
		}else{				
				msgdata.html("");
			}			
		});			
	}
);
function muestraPadre(padre_id, product_id){		
	var isChecked = $("#"+padre_id).is(':checked');				
	var msgdata = $("#datacategoria"+padre_id);			
	//if(isChecked){				
	 $.ajax({
         type: "POST",
         data: "",
         url: "Version/subcategorias.php?padre_id="+padre_id+"&product_id="+product_id,                
         success: 
			function(data){	                	  	
                msgdata.html(data);							                       						
			}
	 });
	//}		
}
</script>
 <script language="javascript1.1" type="text/javascript">
function EliminaHttp(field_name){
	documento = document.getElementById('url').value;	
	if(documento.indexOf('http://') != -1)
		doc_clean = documento.replace('http://','');
	else if(documento.indexOf('https://') != -1)
		doc_clean = documento.replace('https://','');	
	document.getElementById(field_name).value = doc_clean;
	documento = document.getElementById('facebook').value;	
	if(documento.indexOf('http://') != -1)
		doc_clean = documento.replace('http://','');
	else if(documento.indexOf('https://') != -1)
		doc_clean = documento.replace('https://','');	
	document.getElementById(field_name).value = doc_clean;
	documento = document.getElementById('twitter').value;	
	if(documento.indexOf('http://') != -1)
		doc_clean = documento.replace('http://','');
	else if(documento.indexOf('https://') != -1)
		doc_clean = documento.replace('https://','');	
	document.getElementById(field_name).value = doc_clean;
	documento = document.getElementById('youtube').value;	
	if(documento.indexOf('http://') != -1)
		doc_clean = documento.replace('http://','');
	else if(documento.indexOf('https://') != -1)
		doc_clean = documento.replace('https://','');	
	document.getElementById(field_name).value = doc_clean;
	documento = document.getElementById('instagram').value;	
	if(documento.indexOf('http://') != -1)
		doc_clean = documento.replace('http://','');
	else if(documento.indexOf('https://') != -1)
		doc_clean = documento.replace('https://','');	
	document.getElementById(field_name).value = doc_clean;
	return true;
}
</script> 
  <!-- calendar stylesheet -->
  <link rel="stylesheet" type="text/css" media="all" href="calendar/css/jscal2.css" title="jscal2" />
  <!-- main calendar program -->
  <script type="text/javascript" src="calendar/js/jscal2.js"></script>
  <!-- language for the calendar -->
  <script type="text/javascript" src="calendar/js/lang/<?=eCommerce_SDO_LanguageManager::LanguageToZendLang()?>.js"></script>
<h2 align="left" style="margin:0px;"><? echo $viewConfig['title']; ?></h2>
<form name="frmUser" id="frmUser" method="post" action="" enctype="multipart/form-data" onsubmit="return EliminaHttp();">
<input type="hidden" value="<?=$cmdProducto?>" name="cmdProducto" id="cmdProducto">
  <div class="error">
		<?php echo $errors->getDescription(); ?>
  </div><br />
  <?php $errors->getHtmlError("generalError"); ?>	
		<?php
			eCommerce_SDO_Core_Application_Form::displayForm( $form, $errors,'frmUser' );		
		?>
<p align='left'>
	<input type="hidden" name="cmd" value="save">
	<input type="submit" value="<?=$this->trans('save')?>" class="frmButton"> 
	<input type="button" value="<?=$this->trans('cancel')?>" class="frmButton" onClick="<?php echo $cancelConfirm; ?>document.location.href='<?=$cancelBtn?><?php echo $cancelParams; ?>'">
</p>
</form>
<?php
include_once ( "footer.php" );
?>
<script src="js/chosen/chosen.jquery.js" type="text/javascript"></script>
<script src="js/chosen/prism.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="css/textext/textext.core.css" type="text/css" />
<link rel="stylesheet" href="css/textext/textext.plugin.tags.css" type="text/css" />
<script src="js/texttext/textext.core.js" type="text/javascript" charset="utf-8"></script>
<script src="js/texttext/textext.plugin.tags.js" type="text/javascript" charset="utf-8"></script>
   <script type="text/javascript">
    var config = {
      '.chosen-select'           : {},
      '.chosen-select-deselect'  : {allow_single_deselect:true},
      '.chosen-select-no-single' : {disable_search_threshold:10},
      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chosen-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    } 
  </script>
  <script type="text/javascript">
    $('#keywords').textext({ plugins: 'tags',tagsItems: [ <?=$str_keyword?>] });
    $('#addtag').bind('click', function(e)
    {
        $('#keywords').textext()[0].tags().addTags([ $('#tagname').val() ]);
        $('#tagname').val('');
    });
</script>