<?php
include_once ( "header.php" );
function dataToInput( $value ){
	return htmlentities( $value, ENT_QUOTES, CHARSET_PROJECT );
}
$errors          = $this->errors;
$product         = $this->product;
$product_id      = $product->getProductId();
$ArrStatus       = $this->ArrStatus;
$ArrNovedad      = $this->ArrNovedad;
$ArrVendido      = $this->ArrVendido;
$ArrCurrences    = $this->ArrCurrences;
$ArrBrands       = $this->ArrBrands;
$ArrLanguages    = eCommerce_SDO_LanguageManager::GetLanguages();
$ArrObjProInLang = $this->ArrObjProInLang;
$language        = eCommerce_SDO_LanguageManager::GetActualLanguage();
$cmdProducto     = $this->cmdProducto;
$cancelBtn       = 'product.php';
$Keywords        = explode(',',$product->getKeywords());
$str_keyword     = '';

foreach ($Keywords as $Keyword){
	$str_keyword     .= '"'.$Keyword.'",';
}

$str_keyword     = substr($str_keyword,0,-1);
?>
<link rel="stylesheet" href="css/chosen/prism.css">
<link rel="stylesheet" href="css/chosen/chosen.css">
<link rel="stylesheet" type="text/css" media="all" href="calendar/css/jscal2.css" title="jscal2" />
<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<script language="javascript" type="text/javascript">
$(document).ready(function(){	
	validaPadres();
	function validaPadres(){
		$(".categorias").each(function(){
			var padre_id = $(this).val();
			var product_id = $("#product_id").val();
			var msgdata = $("#datacategoria");			
			$.ajax({
	             type: "POST",
	             data: "",
	             url: "product/validaPadres.php?padre_id="+padre_id+"&product_id="+product_id,	  
	             async : true,               
	             success: 
					function(data){																											
	            	 msgdata.html(data);	  							                       
					}
	    	 });
		});		
	}
		$(".categorias").click(function(){
			var padre_id = $(this).val();
			var product_id = $("#product_id").val();
			var msgdata = $("#datacategoria"+padre_id);
			var isChecked = $(this).is(':checked');
			if(isChecked){
			 $.ajax({
                 type: "POST",
                 data: "",
                 url: "product/subcategorias.php?padre_id="+padre_id+"&product_id="+product_id,
                 beforeSend: function(){								
                         msgdata.html("<img src='img/loading.gif' alt='Cargando' title='Cargando' width='30' />");
                 },
                 success: 
					function(data){	
                	  	msgdata.html("");							
                        msgdata.html(data);							                       
						}
        	 });
		}else{				
				msgdata.html("");
			}			
		});			
	}
);
function muestraPadre(padre_id, product_id){		
	var isChecked = $("#"+padre_id).is(':checked');				
	var msgdata = $("#datacategoria"+padre_id);			
	//if(isChecked){				
	 $.ajax({
         type: "POST",
         data: "",
         url: "product/subcategorias.php?padre_id="+padre_id+"&product_id="+product_id,                
         success: 
			function(data){	                	  	
                msgdata.html(data);							                       						
			}
	 });
	//}		
}
</script>
 <script language="javascript1.1" type="text/javascript">
function EliminaHttp(field_name){
	documento = document.getElementById('url').value;	
	if(documento.indexOf('http://') != -1)
		doc_clean = documento.replace('http://','');
	else if(documento.indexOf('https://') != -1)
		doc_clean = documento.replace('https://','');	
	document.getElementById(field_name).value = doc_clean;
	documento = document.getElementById('facebook').value;	
	if(documento.indexOf('http://') != -1)
		doc_clean = documento.replace('http://','');
	else if(documento.indexOf('https://') != -1)
		doc_clean = documento.replace('https://','');	
	document.getElementById(field_name).value = doc_clean;
	documento = document.getElementById('twitter').value;	
	if(documento.indexOf('http://') != -1)
		doc_clean = documento.replace('http://','');
	else if(documento.indexOf('https://') != -1)
		doc_clean = documento.replace('https://','');	
	document.getElementById(field_name).value = doc_clean;
	documento = document.getElementById('youtube').value;	
	if(documento.indexOf('http://') != -1)
		doc_clean = documento.replace('http://','');
	else if(documento.indexOf('https://') != -1)
		doc_clean = documento.replace('https://','');	
	document.getElementById(field_name).value = doc_clean;
	documento = document.getElementById('instagram').value;	
	if(documento.indexOf('http://') != -1)
		doc_clean = documento.replace('http://','');
	else if(documento.indexOf('https://') != -1)
		doc_clean = documento.replace('https://','');	
	document.getElementById(field_name).value = doc_clean;
	return true;
}
</script> 
<h2 align="left" style="margin:0px;"><? echo $this->strSubtitles; ?></h2>
<form name="frmProduct" id="frmProduct" method="post" action="product.php" onsubmit="" enctype="multipart/form-data">
<input type="hidden" value="<?=$cmdProducto?>" name="cmdProducto" id="cmdProducto">
  <div class="error">
		<?php echo $errors->getDescription(); ?>
  </div>
	<fieldset>
		<legend><?=$this->trans('general_information')?></legend>
		<!-- <p style="margin-bottom:10px;"><b><?=$this->trans('language')?>:</b> <? echo $ArrLanguages[ $language ]["name"]; ?></p>-->
		<dl class="form">
			<dt><?=$this->trans('name')?>: *</dt>
			<dd>
			<input type="text" size="30" maxlength="100" class="frmInput" name="entity[name]" id="name" value="<?php echo dataToInput( $product->getName()  ) ?>" >
			</dd>
			<?php $errors->getHtmlError("name"); ?>
			<dt><?=$this->trans('short_description')?>: *</dt>
			<dd>
			<textarea cols="50" rows="7" class="frmInput" name="entity[description_short]" id="description_short"><?php echo  utf8_encode($product->getDescriptionShort()); ?></textarea>
			<?php $errors->getHtmlError("short_description"); ?>
			</dd>
			<!--<dt><?=$this->trans('long_description')?>: *</dt>
			<dd>
			<textarea cols="100" rows="20" class="frmInput editorhtml" name="entity[description_long]" id="description_long"><?php echo dataToInput( $product->getDescriptionLong()  ) ?></textarea>
			</dd>
			<?php $errors->getHtmlError("long_description"); ?>
            <dt>Especificaciones: *</dt>
			<dd>
			<textarea cols="100" rows="20" class="frmInput editorhtml" name="entity[especificaciones]" id="especificaciones"><?php echo dataToInput( $product->getEspecificaciones()  ) ?></textarea>
			</dd>
			<?php $errors->getHtmlError("especificaciones"); ?>-->
            <!--<dt>Keywords: *</dt>
			<dd>
			<textarea cols="50" rows="7" class="frmInput" name="entity[keywords]" id="keywords"></textarea>
			</dd>
			<?php $errors->getHtmlError("keywords"); ?>-->
			<dt><?=$this->trans('brand')?>: *</dt>
			<dd>
				<?php
					echo '<select name="entity[brand_id]"><option value=""></option>';
					foreach( $ArrBrands as $Brand){
						echo "<option value='".$Brand->getBrandId()."'";
						if( $product->getBrandId() == $Brand->getBrandId() ){
							echo " selected='selected'";
						}
						echo ">".trim($Brand->getName())."</option>";
					}
					echo "</select>";
				?>
			</dd>
			<?php $errors->getHtmlError("brand_id"); ?>
			<!-- <dt><?=$this->trans('dimensions')?>: </dt>
			<dd>
			<input type="text" class="frmInput" name="entity[dimensions]" id="dimensions" value="<?php echo dataToInput( $product->getDimensions()  ) ?>" >
			</dd>
			<?php //$errors->getHtmlError("dimensions"); ?>
			<dt><? //=$this->trans('finished')?>: </dt>
			<dd>
				<input type="text"class="frmInput" name="entity[finished]" id="finished" value="<?php echo dataToInput( $product->getFinished()  ) ?>" >
			</dd>
			<?php //$errors->getHtmlError("finished"); ?>
			<dt><? //=$this->trans('materials')?>: </dt>
			<dd>
				<input type="text" class="frmInput" name="entity[materials]" id="materials" value="<?php echo dataToInput( $product->getMaterials()  ) ?>">
			</dd>
			<?php //$errors->getHtmlError("materials"); ?>-->		
			<!-- <dt><?=$this->trans('colors')?>: </dt>
			<dd>
				<input type="text"class="frmInput" name="entity[colors]" id="colors" value="<?php echo dataToInput( $product->getColors()  ) ?>">
			</dd> -->
			<?php //$errors->getHtmlError("colors"); ?>
		</dl>
	</fieldset>
<!-- 
	<style>dl.wider_dt dt{width:15em} dl.wider_dt dd{margin-left:15em}</style>
	<fieldset>
		<legend><?=$this->trans('general_information').' '.$this->trans('in').' '.$this->trans('other_languages')?></legend>
		<dl class="form wider_dt">
	<?php
		unset( $ArrLanguages[ eCommerce_SDO_LanguageManager::DEFAULT_LANGUAGE ] );
		foreach( $ArrLanguages as $language ){
			$langId = $language["id"];
			$productInActalLang = empty($ArrObjProInLang[ $langId ]) ? new eCommerce_Entity_Catalog_ProductInDifferentLanguage() : $ArrObjProInLang[ $langId ];
			$langName = $langId.'_name';
	?>
		<dt><?=$this->trans('name').' '.$this->trans('in').' '.$this->trans($langName)?>: *</dt>
		<dd>
		<input type="text" size="30" maxlength="100" class="frmInput" name="entityLang_<?=$langId?>[name]" id="entityLangId_<?=$langId?>_name" value="<?=dataToInput( $productInActalLang->getName() )?>" >
		</dd>
		<dt><?=$this->trans('short_description').' '.$this->trans('in').' '.$this->trans($langName)?>: *</dt>
		<dd>
		<textarea cols="50" rows="3" class="frmInput" name="entityLang_<?=$langId?>[description_short]" id="entityLangId_<?=$langId?>_description_short"><?=dataToInput( $productInActalLang->getDescriptionShort() )?></textarea>
		</dd>
		<dt><?=$this->trans('long_description').' '.$this->trans('in').' '.$this->trans($langName)?>: *</dt>
		<dd>
		<textarea cols="50" rows="7" class="frmInput" name="entityLang_<?=$langId?>[description_long]" id="entityLangId_<?=$langId?>_description_long"><?=dataToInput( $productInActalLang->getDescriptionLong() )?></textarea>
		</dd>
		<dt><?=$this->trans('dimensions').' '.$this->trans('in').' '.$this->trans($langName)?>: </dt>
		<dd>
		<input type="text" class="frmInput" name="entityLang_<?=$langId?>[dimensions]" id="entityLangId_<?=$langId?>_dimensions" value="<?=dataToInput( $productInActalLang->getDimensions() )?>" >
		</dd>
		<dt><?=$this->trans('finished').' '.$this->trans('in').' '.$this->trans($langName)?>: </dt>
		<dd>
			<input type="text"class="frmInput" name="entityLang_<?=$langId?>[finished]" id="entityLangId_<?=$langId?>_finished" value="<?=dataToInput( $productInActalLang->getFinished() )?>" >
		</dd>
		<dt><?=$this->trans('materials').' '.$this->trans('in').' '.$this->trans($langName)?>: </dt>
		<dd>
			<input type="text" class="frmInput" name="entityLang_<?=$langId?>[materials]" id="entityLangId_<?=$langId?>_materials" value="<?=dataToInput( $productInActalLang->getMaterials() )?>">
		</dd>
		<dt><?=$this->trans('colors').' '.$this->trans('in').' '.$this->trans($langName)?>: </dt>
		<dd>
			<input type="text"class="frmInput" name="entityLang_<?=$langId?>[colors]" id="entityLangId_<?=$langId?>_colors" value="<?=dataToInput( $productInActalLang->getColors() )?>">
		</dd>
		</dl>
	<?php	
		}
	?>
	</fieldset>
-->
  <fieldset>
		<legend><?=$this->trans('information')?></legend>
		<dl class="form">
			<dt><?=$this->trans('sku')?>: *</dt>
			<dd>
				<input type="text" size="15" maxlength="100" class="frmInput"
				       name="entity[sku]" id="sku"
				       value="<?php echo dataToInput( $product->getSku()  ) ?>" >
			</dd>
			<?php $errors->getHtmlError("sku"); ?> 
			<!-- 
			<dt><?=$this->trans('price')?>: *</dt>
			<dd>
				<input type="text" size="5" maxlength="10" class="frmInput"
				       name="entity[price]" id="price"
				       value="<?php echo dataToInput( $product->getPrice()  ) ?>" >
			</dd>
			<?php $errors->getHtmlError("price"); ?>
			<dt><?=$this->trans('currency')?>: *</dt>-->
			<!--<dd>
				   <?php echo eCommerce_SDO_CurrencyManager::GetHTMLSelect('entity[currency]', $product->getCurrency(), 'currency','frmInput',''); ?>
			</dd>
			<?php $errors->getHtmlError("currency"); ?>-->
			<dt><?=$this->trans('discount')?>: *</dt>
			<dd>
				<input type="text" size="5" maxlength="10" class="frmInput"
				       name="entity[discount]" id="discount"
				       value="<?php echo dataToInput( $product->getDiscount()  ) ?>" > % [0-100]
			</dd>
			
			<dt><?=$this->trans('status')?>: *</dt>
			<dd>
				<?php
					echo '<select name="entity[status]">';
					foreach( $ArrStatus as $Status){
						echo "<option value='{$Status}'";
						if( $product->getStatus() == $Status ){
							echo " selected='selected'";
						}
						echo ">".$this->trans(strtolower($Status))."</option>";
					}
					echo "</select>";
				?>
			</dd>
<?php /*
			<dt style="width:24em !important; height:34px !important">URL Video: <br />(https://www.youtube.com/embed/YSdojOzx-fY)</dt>
			<dd>
				<input type="text" size="30" maxlength="200" class="frmInput" name="entity[video]" id="video" value="<?php echo dataToInput( $product->getVideo()  ) ?>" style="width:250px">
			</dd>
			<?php $errors->getHtmlError("video"); ?><br /><br />
			<dt>Novedad: *</dt>
			<dd>
				<?php				
					echo '<select name="entity[novedad]">';
					foreach( $ArrNovedad as $Novedad){
						echo "<option value='{$Novedad}'";
						if( $product->getNovedad() == $Novedad ){
							echo " selected='selected'";
						}
						echo ">".$Novedad."</option>";
					}
					echo "</select>";
				?>
			</dd>*/
?>
            <dt>Destacado ?: *</dt>
            <dd>
				<?php
					echo '<select name="entity[mas_vendido]">';
					foreach( $ArrVendido as $Vendido){
						echo "<option value='{$Vendido}'";						
						if( $product->getMasVendido() == $Vendido ){
							echo " selected='selected'";
						}						
						echo ">".$Vendido."</option>";							
					}
					echo "</select>";
				?>
			</dd>
			
            <?php $pFecha = $product->getFecha();?>
            <dt>Fecha</dt>
            <dd>
            	<input type="text" readonly="yes" size="10" class="frmInput" name="entity[fecha]" id="fecha" value="<?=(empty($pFecha)?date("Y-m-d"):$pFecha)?>"/><input type="button" id="f_trigger_b_fecha" value="..." />
            </dd>
			<!--<dt>Color: </dt>
				<dd>
					<input type="text"class="frmInput" name="entity[colors]" id="colors" value="<?=dataToInput( $product->getColors() )?>">
				</dd>
			<dt>Talla: *</dt>
			<dd>
				<?php				
					/*echo '<select name="entity[talla]">';
					foreach( $ArrTalla as $Talla){
						echo "<option value='{$Talla}'";
						if( $product->getTalla() == $Talla ){
							echo " selected='selected'";
						}
						echo ">".$Talla."</option>";
					}
					echo "</select>";*/
				?>
			</dd>-->
			<dt style="width:100%; ">Tama&ntilde;o de Imagen <b>700x487pixeles</b>
</dt><br />
			<dt><?=$this->trans('main_image')?>: *</dt>
						<dd>
						<?						
							$imageHandler = new eCommerce_SDO_Core_Application_ImageHandler('image_id');
							$imageHandler->setArrImages( $product->getImageId() );
							$imageHandler->setMaximum( 1 );
							$html = $imageHandler->createBOImages();
							echo $html .'<br />';
						?>
						</dd>
						<?php $errors->getHtmlError("image_file"); ?>
			<!-- <dt style="width:450px">Tama&ntilde;o de Imagen <b>M&aacute;ximo: 1000x1000 pixeles , M&iacute;nimo 540x540pixeles</b>
</dt><br />		
						 <dt><?=$this->trans('images')?>:</dt>
						<dd>
						<?php
							$imageHandler = new eCommerce_SDO_Core_Application_ImageHandler('ArrayImages');
							$imageHandler->setArrImages( $product->getArrayImages() );
							$imageHandler->setMaximum( 1 );
							$html = $imageHandler->createBOImages();
							echo $html .'<br />';
						?>
						</dd>-->
			<!-- 
			<?php //$errors->getHtmlError("discount"); ?>
			<dt><? //=$this->trans('weight')?>: *</dt>
			<dd>
				<input type="text" size="5" maxlength="10" class="frmInput"
				       name="entity[weight]" id="weight"
				       value="<?php //echo dataToInput( $product->getWeight()  ) ?>" > grs.
			</dd>
			<?php //$errors->getHtmlError("weight"); ?>
			<dt><? //=$this->trans('volume')?>: *</dt>
			<dd>
				<input type="text" size="5" maxlength="10" class="frmInput"
				       name="entity[volume]" id="volume"
				       value="<?php //echo dataToInput( $product->getVolume()  ) ?>" > cm3.
			</dd>
			<?php //$errors->getHtmlError("volume"); ?>
			-->
		</dl>
	</fieldset>
				<?php
/*
			$txtProducto = $this->txtProducto;
			$tipoProducto = $this->tipoProducto;
			// PRODUCTOS RELACIONADOS
			$form['elements'][] = array( 'title' => $txtProducto, 'type'=>'fieldset',  'value'=> "Especialidades" );
			$Productos = eCommerce_SDO_Core_Application_Product::GetAllProducts(NULL,NULL,$tipoProducto);
			$html = '';
			$html .= '<select data-placeholder="Seleccione o Busque un Producto" class="chosen-select" multiple style="width:100%;" id="productos" name="productos[]">';
			$html .= '<option value=""></option>';
			$ProductosActuales = explode(',',$product->getProductosRelacionados());
			foreach($Productos as $producto){
				$producto_id = $producto->getProductId();
				$name = $producto->getName();
				$sku = $producto->getSku();
				$existe = (in_array($producto_id,$ProductosActuales))?'selected="selected"':'';
				$html .= '<option value="'.$producto_id.'" '.$existe.'>'.$name.' - SKU ('.$sku.')</option>';
			}
			$html .= '</select>';
			$form['elements'][] = array( 'title' => 'Productos', 'type'=>'htmltext',  'value'=> $html );
			$data = '<div id="dataespecialidad"></div>';
			echo ' <fieldset style="display:block">
					<legend>'.$txtProducto.'</legend>
					<dl class="form">';
			echo $html;
			echo $data;
			echo "</dl></fieldset>";*/
			//DETALLES PARA TUS INVITADOS
			/*$form['elements'][] = array( 'title' => "Detalles para tus invitados", 'type'=>'fieldset',  'value'=> "Detalles" );
			$Productos = eCommerce_SDO_Core_Application_Product::GetAllProducts(NULL,NULL);
			$html = '';
			$html .= '<select data-placeholder="Seleccione o Busque un Producto" class="chosen-select" multiple style="width:100%;" id="productos_detalles" name="productos_detalles[]">';
			$html .= '<option value=""></option>';
			$ProductosActualesDetalles = explode(',',$product->getProductosDetalles());
			foreach($Productos as $producto){
				$producto_id = $producto->getProductId();
				$name = $producto->getName();
				$sku = $producto->getSku();
				$existe = (in_array($producto_id,$ProductosActualesDetalles))?'selected="selected"':'';
				$html .= '<option value="'.$producto_id.'" '.$existe.'>'.$name.' - SKU ('.$sku.')</option>';
			}
			$html .= '</select>';
			$form['elements'][] = array( 'title' => 'Productos', 'type'=>'htmltext',  'value'=> $html );
			$data = '<div id="dataproductosdetalles"></div>';
			echo ' <fieldset style="display:block">
					<legend>Detalles para tus invitados</legend>
					<dl class="form">';
			echo $html;
			echo $data;
			echo "</dl></fieldset>";*/
			?>
<?php if( $this->cmd !='add'){ ?>
  <fieldset style="display:block">
		<legend><?=$this->trans('categories')?></legend>
		<dl class="form">
			<?php
			$Categorias = eCommerce_SDO_Catalog::GetCategoriesByParentCategory()->getResults();
			$html = '';
			$data = '';
			$CategoriasA = eCommerce_SDO_Core_Application_ProductCategory::GetCategoriesByProduct($product->getProductId(), NULL );
			foreach($CategoriasA as $category){
				$categoriasActuales[] = $category->getCategoryId();
			}			
			foreach($Categorias as $Categoria){
				$categoria_id = $Categoria->getCategoryId();
				$existe = (in_array($categoria_id,$categoriasActuales))?'checked="checked"':'';
				$estilo = (in_array($categoria_id,$categoriasActuales))?'style="color:red"':'';
				$name = $Categoria->getName();
				$html.= '<input name="chkPadre" type="checkbox" value="'.$categoria_id.'" id="'.$categoria_id.'" class="categorias" '.$existe.'>&nbsp;<span '.$estilo.'>'.$name.'</span>&nbsp;';
				$data .= '<div id="datacategoria'.$categoria_id.'"></div>';
			}
			echo $html;
			echo $data;						
			$data = '<div id="datacategoria"></div>';
			echo $data;
			$data = '<div id="datacategoria2"></div>';
			echo $data;
			?>
		</dl>
  </fieldset>		
  <? /*if($product_id){ ?>
					<fieldset>
						<legend>Versiones</legend>
						<dl class="form">
							<?
							$form = new eCommerce_SDO_Core_Application_Form();
							$element['type'] 	= "iframe";
							$element['title'] 	= "Versiones";
							$element['value'] 	= "Version.php?product_id=".$product->getProductId();
							echo $form->buildHTMLElement($element);
							?>
						</dl>
					</fieldset>
				<? } */?>
<fieldset style="display:none">
		<legend><?=$this->trans('attribute')?>s</legend>
		<iframe style='width:650px;height:300px;' frameborder="0" src='inventory.php?idProduct=<?=$product->getProductId(); ?>'></iframe>
	</fieldset>
<?php 
}
?>
	<input type="hidden" name="entity[orden]" value="<?php echo $product->getOrden(); 	?>">
	<input type="hidden" name="cmd" value="save">
	<input type="hidden" name="entity[id]" value="<? echo ( isset( $_REQUEST['entity']['id'] ) ) ? $_REQUEST['entity']['id'] : $_REQUEST['id']; ?>">
	<input type="hidden" name="product_id" id="product_id" value="<? echo ( isset( $_REQUEST['entity']['id'] ) ) ? $_REQUEST['entity']['id'] : $_REQUEST['id']; ?>">
	<input type="submit" value="<?=$this->trans('save')?>" class="frmButton" >	
	<input type="button" value="<?=$this->trans('cancel')?>" class="frmButton" onClick="<?php echo $cancelConfirm; ?>document.location.href='<?=$cancelBtn?><?php echo $cancelParams; ?>'">
</form>
<script type="text/javascript" src="calendar/js/jscal2.js"></script>
<script type="text/javascript" src="calendar/js/lang/<?=eCommerce_SDO_LanguageManager::LanguageToZendLang()?>.js"></script>
<script type="text/javascript">
	Calendar.setup({
		inputField     :    "fecha",
		ifFormat       :    "%Y-%m-%d",
		showsTime      :    false,
		trigger        :    "f_trigger_b_fecha",
		singleClick    :    true,
		step           :    1,
		onSelect       :    function() { this.hide();}
	});
</script>
<?php
include_once ( "footer.php" );
?>
 <script src="js/chosen/chosen.jquery.js" type="text/javascript"></script>
  <script src="js/chosen/prism.js" type="text/javascript" charset="utf-8"></script>
  <link rel="stylesheet" href="css/textext/textext.core.css" type="text/css" />
<link rel="stylesheet" href="css/textext/textext.plugin.tags.css" type="text/css" />
<script src="js/texttext/textext.core.js" type="text/javascript" charset="utf-8"></script>
<script src="js/texttext/textext.plugin.tags.js" type="text/javascript" charset="utf-8"></script>
   <script type="text/javascript">
    var config = {
      '.chosen-select'           : {},
      '.chosen-select-deselect'  : {allow_single_deselect:true},
      '.chosen-select-no-single' : {disable_search_threshold:10},
      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chosen-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    } 
  </script>
   <script type="text/javascript">
    $('#keywords').textext({ plugins: 'tags',tagsItems: [ <?=$str_keyword?>] });
    $('#addtag').bind('click', function(e)
    {
        $('#keywords').textext()[0].tags().addTags([ $('#tagname').val() ]);
        $('#tagname').val('');
    });
</script>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>tinymce.init({ selector:'.editorhtml',
	 				   toolbar: [
	           			'undo redo | styleselect | bold italic | link image | alignleft | aligncenter | alignright'
	         			],
						menubar: false
					});</script>
