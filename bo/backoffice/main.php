<?php
/*require_once( "../common.php" );
eCommerce_FrontEnd_BO_Authentification::verifyAuthentification();
include_once( "header.php" );*/?>
<!-- <h2><?//=$this->trans('exclusive_zone')?></h2>
<style>
.espacio{
	padding-left:10px;
}
</style>-->
<!-- <a href="user.php"><img src="img/usuario.png" border="0" class="espacio" /></a>
<a href="category.php"><img src="img/categoria.png" border="0" class="espacio" /></a>
<a href="product.php"><img src="img/productos.png" border="0" class="espacio" /></a>
<a href="Brand.php"><img src="img/marcas.png" border="0" class="espacio" /></a>
<a href="order.php"><img src="img/ordenes.png" border="0" class="espacio" /></a>
<a href="Talla.php"><img src="img/atributos.png" border="0" class="espacio" /></a>
<a href="Color.php"><img src="img/caracteristicas.png" border="0" class="espacio" /></a>
<a href="Banner.php"><img src="img/banner.png" border="0" class="espacio" /></a> -->
<!-- <a href="AdminTool.php"><img src="img/configuracion.png" border="0" /></a> -->

<?php //include_once( "footer.php" );?>

<?php 
eCommerce_FrontEnd_BO_Authentification::verifyAuthentification();
include_once( "includes/header.php" );
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1> <?//=$config["project_name"];?>Control panel</h1>
		<ol class="breadcrumb">
			<li>
				<a href="<?php echo ABS_HTTP_URL;?>bo/backoffice/index.php"><i class="fa fa-home"></i> Home</a>
			</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- Small boxes (Stat box) -->
		<div class="row">
		<?php
			//$colors = array('1' => 'bg-yellow','2' => 'bg-aqua','3' => 'bg-green','4'=>'bg-red','5'=>'bg-blue','6'=>'bg-orange','7'=>'bg-purple','8'=>'bg-gray');
			$i = 1;
			foreach ($sections as $key => $section) {
				if($key != 'index'){
					if($section["include"]){
		?>
					
	      			<div class="col-md-3 col-sm-6 col-xs-12">
	      				<a href="<?=$key?>.php" style="font-size: 13px;">
	      				<div class="info-box">
	      					<span class="info-box-icon bg-red"><i class="fa <?=$section["icon"];?>"></i></span>
	      					<div class="info-box-content">
	      						<span class="info-box-text">
	      							<h4 style="text-decoration: none; color: #000; font-weight: bold;"><?=$section["name"];?></h4>
	      						</span>
	      						<span class="info-box-number">
	      							<p style="font-size: 14px; font-weight: normal; color: #000;">M&aacute;s Inforaci&oacute;n <i class="fa fa-arrow-circle-right"></i></p>
	      						</span>
	      					</div>
	      					<!-- /.info-box-content -->
	      				</div>
	      				</a>
	      				<!-- /.info-box -->
	      			</div>
    <?php
        			$i++;
        			if($i==9){$i=1;}
        		}//end if
        	}//end if
      	}//end foreach
    ?>
    	</div><!-- /.row -->
  	</section><!-- /.content -->      
</div><!-- /.content-wrapper -->

<?php include_once( "includes/footer.php" ); ?>