<?php
/**
 * This is a dummy Object class which should be used to extend all others.
 * 
 * @author Alejandro Rivera López <alejandro@vicomstudio.com>
 * @copyright VicomStudio
 * @package <none>
 *
 */
class Object {
		
  public function __construct(){
  }
  
  public function trans( $name ){
		$zendTranslate = new Zend_Translate( 'tmx', ABS_PATH . '/_languages/lang.xml', eCommerce_SDO_Core_Application_LanguageManager::LanguageToZendLang());
  	$trans = $zendTranslate->_( $name );
		if(CHARSET_PROJECT == 'ISO-8859-1') {
			return utf8_decode($trans);
		}
		return $trans;
	}
  
}
?>