<?php
class eCommerce_Entity_Noticias extends eCommerce_Entity {

	public function __construct(){
		parent::__construct();
	}

public $noticias_id;
public $titulo;
public $descripcion;
public $fecha;
public $imagen_principal;
public $imagenes;
public $texto;
public $link;

public function getNoticiasId(){ 
return $this->noticias_id;
}

public function setNoticiasId( $NoticiasId){ 
return $this->noticias_id = $NoticiasId;
}

public function getTitulo(){ 
return $this->titulo;
}

public function setTitulo( $Titulo){ 
return $this->titulo = $Titulo;
}

public function getDescripcion(){ 
return $this->descripcion;
}

public function setDescripcion( $Descripcion){ 
return $this->descripcion = $Descripcion;
}

public function getFecha(){ 
return $this->fecha;
}

public function setFecha( $Fecha){ 
return $this->fecha = $Fecha;
}

public function getImagenPrincipal($position = null){ 
$image = explode(',',$this->imagen_principal);
return !is_numeric($position) ? $image : (!empty($image[$position]) ? $image[$position] : 0 );}

public function getHTMLImagenPrincipal($width = null,$height= null, $nimage = 0, $target_blank = false, $urlEffect = false, $imgParam = 'border="0"'){
$imgParam2 = empty($width) ? '' : ' width="'.$width.'"';
$imgParam2 .= empty($height) ? '' : ' height="'.$height.'"';
$src = $this->getImagenPrincipal( $nimage );
$effect = $urlEffect;
if(empty($src)){
	$src = '';
 	$imgParam .= $imgParam2;
 	$urlImage .= $src;
 	$title = '';
}else{
	 $file = eCommerce_SDO_Core_Application_FileManagement::LoadById($src);
	 $title = $file->getDescription();
	 $urlImage = ABS_HTTP_URL . 'bo/file.php?id=' . $src . '&type=image';
	 $src = ABS_HTTP_URL . 'bo/file.php?id=' . $src . '&type=image&img_size=predefined&width='.$width.'&height=' . $height;
}
$img = $target_blank ? '<a '. ($effect ? 'rel="jquery-lightbox"' : '' ) .' href="'.$urlImage.'" id="ImagePreviewId" target="_blank">' : '';
$imgParam .= !$target_blank ? ' title="'.$title.'"' : '';
$img .= "<img src='".$src."' ".$imgParam." class='img-responsive'>";
$img .= ( $target_blank ? '</a>' : '');
return (empty($src)?false:$img);
} 

public function setImagenPrincipal( $ImagenPrincipal){ 
return $this->imagen_principal = $ImagenPrincipal;
}

public function getImagenes($position = null){ 
$image = explode(',',$this->imagenes);
return !is_numeric($position) ? $image : (!empty($image[$position]) ? $image[$position] : 0 );}

public function getHTMLImagenes($width = null,$height= null, $nimage = 0, $target_blank = false, $urlEffect = false, $imgParam = 'border="0"'){
 $imgParam2 = empty($width) ? '' : ' width="'.$width.'"';
 $imgParam2 .= empty($height) ? '' : ' height="'.$height.'"';
 $src = $this->getImagenes( $nimage );
 
 $effect = $urlEffect;
 if(empty($src)){
 	$src = '';
 	$imgParam .= $imgParam2;
 	$urlImage .= $src;
 	$title = '';
 }else{
 $file = eCommerce_SDO_Core_Application_FileManagement::LoadById($src);
 $title = $file->getDescription();
 $urlImage = BO_DIRECTORY . 'file.php?id=' . $src . '&type=image';
 	$src = BO_DIRECTORY . 'file.php?id=' . $src . '&type=image&img_size=predefined&width='.$width.'&height=' . $height;
 }
 $img = $target_blank ? '<a '. ($effect ? 'rel="jquery-lightbox"' : '' ) .' href="'.$urlImage.'" id="ImagePreviewId" target="_blank">' : '';
 $imgParam .= !$target_blank ? ' title="'.$title.'"' : '';
 $img .= "<img src='".$src."' ".$imgParam.">";
  $img .= ( $target_blank ? '</a>' : '');
 return $img;
 } 

public function setImagenes( $Imagenes){ 
return $this->imagenes = $Imagenes;
}

public function getTexto(){ 
return $this->texto;
}

public function setTexto( $Texto){ 
return $this->texto = $Texto;
}

public function getLink(){ 
return $this->link;
}

public function setLink( $Link){ 
return $this->link = $Link;
}


public function getUrlImageId($version,$pos = NULL, $description = false){
	return $this->getUrlArrayFiles($this->imagen_principal,$version,$pos, $description);
}

public function getUrlArrayImages($version,$pos = NULL, $description = false){
	return $this->getUrlArrayFiles($this->imagenes,$version,$pos, $description);
}

public function getFriendlyName(){return Util_String::validStringForUrl( $this->titulo ) ;}
public function getFriendlyNameUrl(){return eCommerce_SDO_Core_Application_Noticias::getFriendlyNameUrl($this);}

}
?>