<?php
class eCommerce_Entity_AdminTool extends eCommerce_Entity {

	public function __construct(){
		parent::__construct();
	}

public $admin_tool_id;
public $exchange_rate_mxn2usd;
public $marco_a;
public $marco_b;
public $marco_c;
public $m_tela;
public $m_lona;
public $acabado;
public $estructura_economica;
public $estructura_profesional;


public function getAdminToolId(){ 
return $this->admin_tool_id;
}

public function setAdminToolId( $AdminToolId){ 
return $this->admin_tool_id = $AdminToolId;
}

public function getExchangeRateMxn2usd(){ 
return $this->exchange_rate_mxn2usd;
}

public function setExchangeRateMxn2usd( $ExchangeRateMxn2usd){ 
return $this->exchange_rate_mxn2usd = $ExchangeRateMxn2usd;
}

public function getMarcoA(){
	return $this->marco_a;
}
public function setMarcoA($MarcoA){
	return $this->marco_a = $MarcoA;
}

public function getMarcoB(){
	return $this->marco_b;
}
public function setMarcoB($MarcoB){
	return $this->marco_b = $MarcoB;
}
public function getMarcoC(){
	return $this->marco_c;
}
public function setMarcoC($MarcoC){
	return $this->marco_c = $MarcoC;
}
public function getMTela(){
	return $this->m_tela;
}
public function setMTela($MTela){
	return $this->m_tela = $MTela;
}
public function getMLona(){
	return $this->m_lona;
}
public function setMLona($MLona){
	return $this->m_lona = $MLona;
}
public function getAcabado(){
	return $this->acabado;
}
public function setAcabado($Acabado){
	return $this->acabado = $Acabado;
}

public function getEstructuraEconomica(){
	return $this->estructura_economica;
}
public function setEstructuraEconomica($Estructura_economica){
	return $this->estructura_economica = $Estructura_economica;
}

public function getEstructuraProfesional(){
	return $this->estructura_profesional;
}
public function setEstructuraProfesional($estructura_profesional){
	return $this->estructura_profesional = $estructura_profesional;
}
}

?>