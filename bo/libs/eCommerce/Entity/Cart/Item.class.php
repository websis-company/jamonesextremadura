<?php
class eCommerce_Entity_Cart_Item extends eCommerce_Entity {
	
	/**
	 * @var int
	 */
	public $productId;
	
	/**
	 * @var int
	 */
	public $quantity;
	
	public $color;
	public $talla;
	public $versionId;
	
	public $price;
	public $attributes;
	public $galeria_id;
	public $file_id;
	public $discount;

	
	public function __construct(){
		parent::__construct();
		
	}

	/**
	 * @return array
	 */
	public function getAttributes(){
		return $this->attributes;
	}

	/**
	 * @param array $attributes
	 */
	public function setAttributes( $attributes ){
		$this->attributes = $attributes;
	}

	public function getDiscount(){
		return $this->discount;
	}

	public function setDiscount($Discount){
		$this->discount = $Discount;
	}




/**
	 * @return int
	 */
	public function getProductId(){
		return $this->productId;
	}

	/**
	 * @param int $productId
	 */
	public function setProductId( $productId ){
		$this->productId = $productId;
	}



	/**
	 * @return int
	 */
	public function getQuantity(){
		return $this->quantity;
	}

	/**
	 * @param int $quantity
	 */
	public function setQuantity( $quantity ){
		$this->quantity = $quantity;
	}
	
	public function getVersionId(){
		return $this->versionId;
	}

	public function setVersionId( $VersionId ){
		$this->versionId = $VersionId;
	}
	
	
	public function getColor(){
		return $this->color;
	}
	
	public function setColor( $color ){
		$this->color = $color;
	}
	
	public function getTalla(){
		return $this->talla;
	}
	
	public function setTalla( $talla ){
		$this->talla = $talla;
	}

	public function getGaleriaId(){
		return $this->galeria_id;
	}

	public function setGaleriaId($GaleriaId){
		$this->galeria_id = $GaleriaId;
	}

	public function getFileId(){
		return $this->file_id;
	}

	public function setFileId($FileId){
		$this->file_id = $FileId;
	}

	public function getPrice(){
		return $this->price;
	}

	public function setPrice($Price){
		$this->price = $Price;
	}

}
?>