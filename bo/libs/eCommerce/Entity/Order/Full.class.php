<?php
class eCommerce_Entity_Order_Full extends eCommerce_Entity_Order {
	
	/**
	 * @var eCommerce_Entity_Util_OrderItemArray
	 */
	public $items;
	
	/**
	 * @var eCommerce_Entity_Util_OrderAddressArray
	 */
	public $addresses;
	
	/**
	 * @var eCommerce_Entity_Util_OrderFeeArray
	 */
	public $fees;
	
	public function __construct(){
		
	}

	/**
	 * @return eCommerce_Entity_Util_OrderAddressArray
	 */
	public function getAddresses(){
		return $this->addresses;
	}

	/**
	 * @param eCommerce_Entity_Util_OrderAddressArray $addresses
	 */
	public function setAddresses( $addresses ){
		$this->addresses = $addresses;
	}

	/**
	 * @return eCommerce_Entity_Util_OrderItemArray
	 */
	public function getItems(){
		return $this->items;
	}

	/**
	 * @param eCommerce_Entity_Util_OrderItemArray $items
	 */
	public function setItems( $items ){
		$this->items = $items;
	}

	/**
	 * Retrieves the Order Grand Total
	 * 
	 * @return float;
	 */
	public function getTotal(){
		$total = 0;
		$total += $this->getProductsSubTotal();
		$total += $this->getExtraChargesSubtTotal();
		$total = round($total,2);
		return $total;
	}
	
	/**
	 * Retrieves the total for the products purchased
	 * @return float
	 */
	public function getProductsSubTotal(){
		$subtotal = 0;
		for( $it = $this->items->getIterator(); $it->valid(); $it->next() ){
			$item = $it->current();
			$subtotal += $item->getTotal(); 
		}
		return $subtotal;
	}
	
	/**
	 * Retrieves the extra charges sub total (taxes, shipping, handling, etc.)
	 *
	 * @return float;
	 * @todo implement this correctly.
	 */
	public function getExtraChargesSubtTotal(){
		$subtotal = 0;
		for( $it = $this->fees->getIterator(); $it->valid(); $it->next() ){
			$fee = $it->current();
			$subtotal+= $fee->getAmount();
		}
		return $subtotal;
	}

	/**
	 * @return eCommerce_Entity_Util_OrderFeeArray
	 */
	public function getFees(){
		return $this->fees;
	}

	/**
	 * @param eCommerce_Entity_Util_OrderFeeArray $fees
	 */
	public function setFees( $fees ){
		$this->fees = $fees;
	}

}
?>