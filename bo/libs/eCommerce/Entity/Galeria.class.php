<?php
class eCommerce_Entity_Galeria extends eCommerce_Entity {

	public function __construct(){
		parent::__construct();
	}

public $galeria_id;
public $galerycategory_id;
public $nombre;
public $descripcion;
public $material_recomendado;
public $formato_imagen;
public $precio;
public $keywords;
public $array_images;
public $destacado;


public function getGaleriaId(){ 
return $this->galeria_id;
}

public function setGaleriaId( $GaleriaId){ 
return $this->galeria_id = $GaleriaId;
}

public function getGalerycategoryId(){ 
return $this->galerycategory_id;
}

public function setGalerycategoryId( $GalerycategoryId){ 
return $this->galerycategory_id = $GalerycategoryId;
}

public function getNombre(){ 
return $this->nombre;
}

public function setNombre( $Nombre){ 
return $this->nombre = $Nombre;
}

public function getDescripcion(){
return $this->descripcion;	
}

public function setDescripcion($Descripcion){
return $this->descripcion = $Descripcion;	
}

public function getMaterialRecomendado(){
return $this->material_recomendado;	
}

public function setMaterialRecomendado($MaterialRecomendado){
return $this->material_recomendado = $MaterialRecomendado;	
}

public function getFormatoImagen(){
return $this->formato_imagen;		
}

public function setFormatoImagen($FormatoImagen){
return $this->formato_imagen = $FormatoImagen;		
}


public function getPrecio(){ 
return $this->precio;
}

public function setPrecio( $Precio){ 
return $this->precio = $Precio;
}

public function getKeywords(){ 
return $this->keywords;
}

public function setKeywords( $Keywords){ 
return $this->keywords = $Keywords;
}

public function getArrayImages( $pos = null){
		$ret = $arr = explode(',',$this->array_images);
		if( is_numeric($pos) ){
			$ret = $arr[ $pos ];
		}
		return $ret;
	}

public function setArrayImages( $ArrayImages){ 
return $this->array_images = $ArrayImages;
}

public function getUrlArrayImages($version,$pos = NULL,$addImageId = false){
		$strIds = ($addImageId ? $this->image_id : '') . ($addImageId && $this->image_id && $this->array_images ? ',' : '') . $this->array_images;
		return $this->getUrlArrayFiles($strIds,$version,$pos);
	}

	
public function getUrlArrayFiles($array_files,$version,$pos = NULL){		
		$path = $version == 'original' ? 'bo' : 'web';		
		$ret = $arr = explode(',',$array_files);
		
		if(count($ret)>0 && !empty($ret[0])){
			$search = new eCommerce_Entity_Search('','',1,-1);			
			if( is_numeric($pos) ){
				$img = eCommerce_SDO_Core_Application_FileManagement::getFileByVersion($arr[ $pos ],$version);
				$pathTmp = $img->getPath();
				$filenameTmp = $img->getFilename();
				if(!empty($pathTmp) && !empty($filenameTmp)){
					$ret = ABS_HTTP_URL . "$path/{$img->getPath()}{$img->getFilename()}";
				}else{
					if(!empty($this->image_id)){
						$ret = ABS_HTTP_URL."bo/file.php?id=".$this->image_id."&type=image&img_size=predefined&height=300";
						
					}else{
						$ret = 	ABS_HTTP_URL."img/default.jpg";
					}
				}
			}else{
				$ret = array();
				foreach($arr as $p){
					$img = eCommerce_SDO_Core_Application_FileManagement::getFileByVersion($p,$version);
					$ret[] = ABS_HTTP_URL . "$path/{$img->getPath()}{$img->getFilename()}";
					
				}
			}	
					
			return $ret;
		}else{
			switch($version){
				case 'small':$size = 'width=160&height=235';break;
				case 'medium':$size = 'width=320&height=470';break;
				case 'large':$size = 'width=800&height=900';break;
			}			
			//return ABS_HTTP_URL.PATH_ACTUAL_LANG . 'system/file.php?id=0&type=image&img_size=predefined&'.$size;			
			//return ABS_HTTP_URL . "bo/file.php?id=0&type=image&img_size=predefined&$size";
			return ABS_HTTP_URL . "img/default.jpg";
		}
	}

public function getDestacado(){ 
return $this->destacado;
}

public function setDestacado( $Destacado){ 
return $this->destacado = $Destacado;
}

	

}
?>