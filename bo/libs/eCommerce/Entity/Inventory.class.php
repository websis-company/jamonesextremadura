<?php
class eCommerce_Entity_Inventory extends eCommerce_Entity {

	public function __construct(){
		parent::__construct();
	}

public $inventory_id;
public $product_id;
public $n_products;

public $attribute;

public function getInventoryId(){ 
return $this->inventory_id;
}

public function setInventoryId( $InventoryId){ 
return $this->inventory_id = $InventoryId;
}

public function getProductId(){ 
return $this->product_id;
}

public function setProductId( $ProductId){ 
return $this->product_id = $ProductId;
}

public function getNProducts(){ 
return $this->n_products;
}

public function setNProducts( $NProducts){ 
return $this->n_products = $NProducts;
}

public function getAttribute() {
return $this->attribute;
}

public function setAttribute($attribute) {
return $this->attribute = $attribute;
}

}
?>