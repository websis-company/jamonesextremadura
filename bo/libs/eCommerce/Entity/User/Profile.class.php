<?php
class eCommerce_Entity_User_Profile extends eCommerce_Entity {
	
	/**
	 * @var int
	 */
	public $profile_id;
	/**
	 * @var string
	 */
	public $first_name;
	/**
	 * @var string
	 */
	public $last_name;
	/**
	 * @var string
	 */
	public $email;
	/**
	 * @var string
	 */
	public $password;
	public $confirm_password;
	/**
	 * @var string
	 */
	public $role;
	public $confirmado;
	public $saludo;
		
	public function __construct(){
		parent::__construct();
	}

	/**
	 * @return string
	 */
	public function getEmail(){
		return $this->email;
	}

	/**
	 * @param string $email
	 */
	public function setEmail( $email ){
		$this->email = $email;
	}

	/**
	 * @return string
	 */
	public function getFirstName(){
		return $this->first_name;
	}

	/**
	 * @param string $first_name
	 */
	public function setFirstName( $firstName ){
		$this->first_name = $firstName;
	}

	/**
	 * @return string
	 */
	public function getLastName(){
		return $this->last_name;
	}

	/**
	 * @param string $last_name
	 */
	public function setLastName( $lastName ){
		$this->last_name = $lastName;
	}

	/**
	 * @return string
	 */
	public function getPassword(){
		return $this->password;
	}

	/**
	 * @param string $password
	 */
	public function setPassword( $password ){
		$this->password = $password;
	}

	/**
	 * @return string $confirm_password
	 */
	public function getConfirmPassword() {
		return $this->confirm_password;
	}

	public function setConfirmPassword($confirm_password) {
		$this->confirm_password = $confirm_password;
	}

	public function getProfileId(){
		return $this->profile_id;
	}

	/**
	 * @param int $profile_id
	 */
	public function setProfileId( $profileId ){
		$this->profile_id = $profileId;
	}

	/**
	 * @return string
	 */
	public function getRole(){
		return $this->role;
	}

	/**
	 * @param string $role
	 */
	public function setRole( $role ){
		$this->role = $role;
	}
	
	public function getConfirmado(){
		return $this->confirmado;
	}
		
	public function setConfirmado( $confirmado ){
		$this->confirmado = $confirmado;
	}
	
	public function getSaludo(){
		return $this->saludo;
	}
	
	public function setSaludo( $Saludo ){
		$this->saludo = $Saludo;
	}

	/**
	 * Utility methods
	 */
	
	public function __toString(){
		return $this->getFirstName() . ' ' . $this->getLastName();
	}
}
?>