<?php
class eCommerce_Entity_Relaciontalla extends eCommerce_Entity {

	public function __construct(){
		parent::__construct();
	}

public $relaciontalla_id;
public $talla_id;
public $tabla_id;
public $tipo;


public function getRelaciontallaId(){ 
return $this->relaciontalla_id;
}

public function setRelaciontallaId( $RelaciontallaId){ 
return $this->relaciontalla_id = $RelaciontallaId;
}

public function getTallaId(){ 
return $this->talla_id;
}

public function setTallaId( $TallaId){ 
return $this->talla_id = $TallaId;
}

public function getTablaId(){ 
return $this->tabla_id;
}

public function setTablaId( $TablaId){ 
return $this->tabla_id = $TablaId;
}

public function getTipo(){ 
return $this->tipo;
}

public function setTipo( $Tipo){ 
return $this->tipo = $Tipo;
}


}
?>