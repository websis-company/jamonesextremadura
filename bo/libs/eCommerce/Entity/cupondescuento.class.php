<?php
class eCommerce_Entity_cupondescuento extends eCommerce_Entity {

	public function __construct(){
		parent::__construct();
	}

public $cupondescuento_id;
public $cupon;
public $descuento;
public $status;


public function getCupondescuentoId(){ 
return $this->cupondescuento_id;
}

public function setCupondescuentoId( $CupondescuentoId){ 
return $this->cupondescuento_id = $CupondescuentoId;
}

public function getCupon(){ 
return $this->cupon;
}

public function setCupon( $Cupon){ 
return $this->cupon = $Cupon;
}

public function getDescuento(){ 
return $this->descuento;
}

public function setDescuento( $Descuento){ 
return $this->descuento = $Descuento;
}

public function getStatus(){ 
return $this->status;
}

public function setStatus( $Status){ 
return $this->status = $Status;
}

}
?>