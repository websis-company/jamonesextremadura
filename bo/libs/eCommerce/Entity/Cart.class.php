<?php
class eCommerce_Entity_Cart extends eCommerce_Entity {
	
	/**
	 * @var eCommerce_Entity_Util_CartItemArray
	 */
	protected $items;
	
	/**
	 * List of addresses related to the order
	 *
	 * @var eCommerce_Entity_Util_OrderAddressArray
	 */
	protected $addresses;
		protected $shipping_type;
	protected $requiere_factura;
	
	public function __construct(){
		parent::__construct();
		$this->items = new eCommerce_Entity_Util_CartItemArray();
		$this->addresses = new eCommerce_Entity_Util_OrderAddressArray();
	}
	
	/**
	 * Sets an array of items
	 *
	 * @param eCommerce_Entity_Util_CartItemArray $items
	 */
	public function setItems( eCommerce_Entity_Util_CartItemArray $items ){
		$this->items = $items;
	}
	
	/**
	 * Retrieve the array of items currently stored in the cart
	 * 
	 * @return eCommerce_Entity_Util_CartItemArray
	 */
	public function getItems(){
		return $this->items;
	}
	
	/**
	 * Adds an item to the cart.
	 *
	 * @param eCommerce_Entity_Cart_Item $item
	 */
	public function addItem( eCommerce_Entity_Cart_Item $item ){
		if(true){
			if( isset($this->items[ $item->getProductId() ]) ){
				unset($this->items[ $item->getProductId() ]);
			}
			$this->items[ $item->getProductId() ] = $item;
		}
	}
	
	/**
	 * Retrieves the cart item using the product id as the reference, or null if it doesn't exist. 
	 *
	 * @param int $productId
	 * @return eCommerce_Entity_Cart_Item
	 */
	public function getItem( $productId ){
		return isset( $this->items[ $productId ] ) ? $this->items[ $productId ] : null;
	}
	
	public function removeItem( $productId ){
		if ( isset( $this->items[ $productId ] ) ){
			unset( $this->items[ $productId ] );
		}
	}

	/**
	 * @return eCommerce_Entity_Util_OrderAddressArray
	 */
	public function getAddresses(){
		return $this->addresses;
	}

	/**
	 * @param eCommerce_Entity_Util_OrderAddressArray $addresses
	 */
	public function setAddresses( eCommerce_Entity_Util_OrderAddressArray $addresses ){
		$this->addresses = $addresses;
	}
	
	public function addAddress( eCommerce_Entity_Order_Address $address ){
		$this->addresses[ $address->getType() ] = $address;
	}
	
	/**
	 * Retrieves the "Ship To" or "Bill To" address of the order. Returns NULL if the type doesn't exist.
	 *
	 * @param string $type - The type of address
	 * @return eCommerce_Entity_Order_Address or null
	 */
	public function getAddressByType( $type ){
		if ( isset( $this->addresses[ $type ] ) ){
			return $this->addresses[ $type ];
		}
		else {
			return null;
		}
	}
	
	/**
	 * Retrieves the "Ship To" or "Bill To" address of the order. Returns an empty eCommerce_Entity_Order_Address if the type doesn't exist.
	 *
	 * @param string $type - The type of address
	 * @return eCommerce_Entity_Order_Address
	 */
	public function getOrderAddressByType( $type ){
		if ( isset( $this->addresses[ $type ] ) && 
			 $this->addresses[ $type ] instanceof eCommerce_Entity_Order_Address
		){
			return $this->addresses[ $type ];
		}
		else {
			//returns an empty eCommerce_Entity_Order_Address
			return new eCommerce_Entity_Order_Address();
		}
	}
	
	public function getShippingType(){
		return $this->shipping_type;
	}
	public function setShippingType($shipping_type){
		$this->shipping_type = $shipping_type;
	}
	
public function getRequiereFactura(){
		return $this->requiere_factura;
	}
	
	public function setRequiereFactura($shipping_type){
		$this->requiere_factura = $shipping_type;
	}
	

}
?>