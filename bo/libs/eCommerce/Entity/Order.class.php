<?php
class eCommerce_Entity_Order extends eCommerce_Entity {
	
	/**
	 * Order ID
	 *
	 * @var int
	 */
	public $order_id;
	/**
	 * Profile ID of the customer
	 * 
	 * @var int
	 */
	public $profile_id;
	/**
	 * The payment method
	 * 
	 * @var string
	 */
	public $payment_method;
	/**
	 * The status of the order
	 * 
	 * @var string
	 */
	public $status;
	/**
	 * User comments on the order
	 * 
	 * @var string
	 */
	public $comments;
	/**
	 * Creation date and time
	 * 
	 * @var string (YYYY-MM-DD HH:MM:SS)
	 */
	public $creation_date;
	/**
	 * Modification date and time
	 * 
	 * @var string (YYYY-MM-DD HH:MM:SS)
	 */
	public $modification_date;
	
	public $currency;
	
	public $language;
	
	public $requiere_factura;
	public $folio;
	public $tipo_cargo;
	public $tipo_tarjeta;
	public $enviado;
	public $descuento;
	
	public function __construct(){
		parent::__construct();
	}

	/**
	 * @return string
	 */
	public function getComments(){
		return $this->comments;
	}

	/**
	 * @param string $comments
	 */
	public function setComments( $comments ){
		$this->comments = $comments;
	}

	/**
	 * @return string
	 */
	public function getCreationDate(){
		return $this->creation_date;
	}

	/**
	 * @param string $creationDate in SQL format (YYYY-MM-DD HH:MM:SS)
	 */
	public function setCreationDate( $creationDate ){
		$this->creation_date = $creationDate;
	}

	/**
	 * @return string
	 */
	public function getModificationDate(){
		return $this->modification_date;
	}

	/**
	 * @param string $modificationDate in SQL format (YYYY-MM-DD HH:MM:SS)
	 */
	public function setModificationDate( $modificationDate ){
		$this->modification_date = $modificationDate;
	}

	/**
	 * @return int
	 */
	public function getOrderId(){
		return $this->order_id;
	}

	/**
	 * @param int $orderId
	 */
	public function setOrderId( $orderId ){
		$this->order_id = $orderId;
	}

	/**
	 * @return string
	 */
	public function getPaymentMethod(){
		return $this->payment_method;
	}

	/**
	 * @param string $paymentMethod
	 */
	public function setPaymentMethod( $paymentMethod ){
		$this->payment_method = $paymentMethod;
	}

	/**
	 * @return int
	 */
	public function getProfileId(){
		return $this->profile_id;
	}

	/**
	 * @param int $profileId
	 */
	public function setProfileId( $profileId ){
		$this->profile_id = $profileId;
	}

	/**
	 * @return string
	 */
	public function getStatus(){
		return $this->status;
	}

	/**
	 * @param string $status
	 */
	public function setStatus( $status ){
		$this->status = $status;
	}


	public function getCurrency(){
		return $this->currency;
	}

	public function setCurrency( $currency ){
		$this->currency = $currency;
	}


	public function getLanguage(){
		return $this->language;
	}

	public function setLanguage( $language ){
		$this->language = $language;
	}
		
	public function getTotal(){
		$order = eCommerce_SDO_Core_Application_OrderMediation::CreateOrderFullFromOrderId( $this->order_id, $this->language );
		return $order->getTotal();
	}
	
public function getRequiereFactura(){
		return $this->requiere_factura;
	}
	
	public function setRequiereFactura($shipping_type){
		$this->requiere_factura = $shipping_type;
	}
	
public function getFolio(){
		return $this->folio;
	}
	
	public function setFolio($folio){
		$this->folio = $folio;
	}
	public function getTipoCargo(){
		return $this->tipo_cargo;
	}
	
	public function setTipoCargo($tipo_cargo){
		$this->tipo_cargo = $tipo_cargo;
	}
	public function getTipoTarjeta(){
		return $this->tipo_tarjeta;
	}
	
	public function setTipoTarjeta($tipo_tarjeta){
		$this->tipo_tarjeta = $tipo_tarjeta;
	}

	public function getEnviado(){
		return $this->enviado;
	}
	
	public function setEnviado($Enviado){
		$this->enviado = $Enviado;
	}

	public function getDescuento(){
		return $this->descuento;	
	}

	public function setDescuento($Descuento){
		$this->descuento = $Descuento;	
	}
}
?>