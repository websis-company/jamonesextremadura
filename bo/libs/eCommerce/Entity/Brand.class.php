<?php
class eCommerce_Entity_Brand extends eCommerce_Entity {

	public function __construct(){
		parent::__construct();
	}

public $brand_id;
public $name;
public $image;


public function getBrandId(){ 
return $this->brand_id;
}

public function setBrandId( $BrandId){ 
return $this->brand_id = $BrandId;
}

public function getName(){ 
return $this->name;
}

public function setName( $Name){ 
return $this->name = $Name;
}

public function getImage($position = null){ 
$image = explode(',',$this->image);
return !is_numeric($position) ? $image : (!empty($image[$position]) ? $image[$position] : 0 );}

public function getHTMLImage($imgAlt, $width = '', $height= '', $nimage = 0, $imgParam='border="0"', $thickbox = false, $imgTitle = '', $defaultImagePath=''){
	$src = empty($nimage) ? $this->getImageId() : $this->getArrayImages( $nimage - 1 );
	if(empty($src)){
		$src = empty($defaultImagePath) ? ABS_HTTP_URL . BO_DIRECTORY . 'ima/fo/default.gif' : $defaultImagePath;
	}else{
		$src = ABS_HTTP_URL . BO_DIRECTORY . 'file.php?id=' . $src . '&type=image&img_size=predefined&width='.$width.'&height='.$height;
	}
	$html = '<img src="'.$src.'" '.$imgParam.' alt="'.$imgAlt.'" '.(empty($imgTitle) ? 'title="'.$imgTitle.'"' : '').'>';
	if($thickbox) {
		$html =
			'<a class="thickbox" href="'.ABS_HTTP_URL.BO_DIRECTORY.'file.php?id='.$this->image_id.'&type=image" title="'.$imgAlt.'">' .
			$html .
			'</a>';
	}
	return $html;
}

public function setImage( $Image){ 
return $this->image = $Image;
}

}
?>