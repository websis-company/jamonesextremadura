<?php
/**
 * This class represents a product for sale eStore
 *
 * @package eCommerce::Entity
 */
class eCommerce_Entity_Catalog_ProductInDifferentLanguage extends eCommerce_Entity {

	public $product_id;
	
	public $language_id;
	
	public $name;
	
	public $description_short;
	
	public $description_long;
	
	public $dimensions;
	
	public $finished;
	
	public $materials;
	
	public $colors;

	
	function __construct(){
		parent::__construct();
	}

	/**
	 * @return int
	 */
	public function getProductId(){
		return $this->product_id;
	}

	/**
	 * @param int $productId
	 */
	public function setProductId( $productId ){
		$this->product_id = $productId;
	}
	/**
	 * @return int
	 */
	public function getLanguageId(){
		return $this->language_id;
	}

	/**
	 * @param int $productId
	 */
	public function setLanguageId( $language_id ){
		$this->language_id = $language_id;
	}
	/**
	 * @return string
	 */
	public function getDescriptionLong(){
		return $this->description_long;
	}


	/**
	 * @param string $description_long
	 */
	public function setDescriptionLong( $description_long ){
		$this->description_long = $description_long;
	}

	/**
	 * @return string
	 */
	public function getDescriptionShort(){
		return $this->description_short;
	}

	/**
	 * @param string $description_short
	 */
	public function setDescriptionShort( $description_short ){
		$this->description_short = $description_short;
	}

	

	/**
	 * @return string
	 */
	public function getName(){
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName( $name ){
		$this->name = $name;
	}




	/**
	 * @return string
	 */
	public function getDimensions(){
		return $this->dimensions;
	}

	/**
	 * @param string $name
	 */
	public function setDimensions( $dimensions ){
		$this->dimensions = $dimensions;
	}
	
	/**
	 * @return string
	 */
	public function getFinished(){
		return $this->finished;
	}

	/**
	 * @param string $name
	 */
	public function setFinished( $finished ){
		$this->finished = $finished;
	}
	
	/**
	 * @return string
	 */
	public function getMaterials(){
		return $this->materials;
	}

	/**
	 * @param string $name
	 */
	public function setMaterials( $materials ){
		$this->materials = $materials;
	}
/**
	 * @return string
	 */
	public function getColors(){
		return $this->colors;
	}

	/**
	 * @param string $name
	 */
	public function setColors( $colors ){
		$this->colors = $colors;
	}
}
?>