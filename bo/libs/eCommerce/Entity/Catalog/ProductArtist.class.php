<?php
class eCommerce_Entity_Catalog_ProductArtist extends eCommerce_Entity {
	
	/**
	 * @var int
	 */
	public $product_id;
	/**
	 * @var int
	 */
	public $artist_id;
	
	/**
	 * @param int $productId
	 * @param int $artistId
	 */
	public function __construct( $productId, $artistId ){
		parent::__construct();
		$this->setProductId( $productId );
		$this->setArtistId( $artistId );
	}
	
	public function setProductId( $productId ){
		$this->product_id = $productId;
	}
	
	public function getProductId(){
		return $this->product_id;
	}
	
	public function setArtistId( $artistId ){
		return $this->artist_id = $artistId;
	}
	
	public function getArtistId(){
		return $this->artist_id;
	}
	
}
?>