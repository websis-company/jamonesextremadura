<?php
/**
 * This class represents a Category in the Catalog system.
 * 
 * @author Alejandro Rivera <alejandro@vicomstudio.com>
 * @package eCommerce::Entity::Catalog
 *
 */
class eCommerce_Entity_Catalog_Category extends eCommerce_Entity {
	
	/**
	 * Category ID (unique)
	 *
	 * @var int
	 */
	public $category_id;
	
	/**
	 * Parent Category ID
	 *
	 * @var int
	 */
	public $parent_category_id;
	
	/**
	 * Category name
	 *
	 * @var string
	 */
	public $name;
	
	/**
	 * Category description
	 *
	 * @var string
	 */
	public $description;
	
	/**
	 * Image (File) ID.
	 *
	 * @var int
	 */
	public $image_id;
	public $banner_id;
	public $orden;
	public $status;
	
	public function __construct(){
		parent::__construct();
	}

	/**
	 * @return int
	 */
	public function getCategoryId(){
		return $this->category_id;
	}

	/**
	 * @param int $category_id
	 */
	public function setCategoryId( $category_id ){
		$this->category_id = $category_id;
	}

	/**
	 * @return string
	 */
	public function getDescription(){
		return $this->getDescriptionInActualLanguage();
		//return $this->description;
	}
	
	public function getDescriptionInDefaultLanguage(){
		return $this->description;
	}
	
	public function getDescriptionInActualLanguage(){
		$actualLang = eCommerce_SDO_LanguageManager::GetActualLanguage();	
		
		if($actualLang == eCommerce_SDO_LanguageManager::DEFAULT_LANGUAGE ){
			return $this->description;
		}else{
			$categoryInDifferent = eCommerce_SDO_CategoryInDifferentLanguage::LoadByCategoryIdAndLanguageId($this->category_id, $actualLang);
			$IdTmp = $categoryInDifferent->getCategoryInDifferentLanguageId();
			if( empty($IdTmp) ){
				return $this->description;
			}else{
				return $categoryInDifferent->getDescription();
			}
			
		}
	}
	
	/**
	 * @param string $description
	 */
	public function setDescription( $description ){
		$this->description = $description;
	}

	/**
	 * @return string
	 */
	public function getName(){
		return $this->getNameInActualLanguage();
		//return $this->name;
	}
	
	public function getNameInDefaultLanguage(){
		return $this->name;
	}
	
	public function getNameInActualLanguage(){
		$actualLang = eCommerce_SDO_LanguageManager::GetActualLanguage();	
		
		if($actualLang == eCommerce_SDO_LanguageManager::DEFAULT_LANGUAGE ){
			return $this->name;
		}else{
			$categoryInDifferent = eCommerce_SDO_CategoryInDifferentLanguage::LoadByCategoryIdAndLanguageId($this->category_id, $actualLang);
			$IdTmp = $categoryInDifferent->getCategoryInDifferentLanguageId();
			if( empty($IdTmp) ){
				return $this->name;
			}else{
				return $categoryInDifferent->getName();
			}
			
		}
	}

	/**
	 * @param string $name
	 */
	public function setName( $name ){
		$this->name = $name;
	}

	/**
	 * @return int
	 */
	public function getParentCategoryId(){
		return $this->parent_category_id;
	}

	/**
	 * @param int $parent_category_id
	 */
	public function setParentCategoryId( $parent_category_id ){
		$this->parent_category_id = $parent_category_id;
	}

	/**
	 *  Imagen Categoría
	 */
	public function getImageId( $pos = null){
		$ret = $arr = explode(',',$this->image_id);
		if( is_numeric($pos) ){
			$ret = $arr[ $pos ];
		}
		return $ret;
	}

	public function setImageId( $ImageId){ 
		return $this->image_id = $ImageId;
	}

	public function getUrlImageId($version,$pos = NULL,$addImageId = false){
		$strIds = ($addImageId ? $this->image_id : '') . ($addImageId && $this->image_id && $this->image_id ? ',' : '') . $this->image_id;
		return $this->getUrlArrayFiles($strIds,$version,$pos);
	}

	/**
	 * Imagen banner Categoría
	 */
	public function getBannerId($pos = null){
		$ret = $arr = explode(',',$this->banner_id);
		if( is_numeric($pos) ){
			$ret = $arr[ $pos ];
		}
		return $ret;
	}

	public function setBannerId($BannerId){
		return $this->banner_id = $BannerId;
	}

	public function getUrlBannerId($version,$pos = NULL,$addImageId = false){
		$strIds = ($addImageId ? $this->banner_id : '') . ($addImageId && $this->banner_id && $this->banner_id ? ',' : '') . $this->banner_id;
		return $this->getUrlArrayFiles($strIds,$version,$pos);
	}


	/**
	 * ArrayFiles
	 */
	public function getUrlArrayFiles($array_files,$version,$pos = NULL){		
		$path = $version == 'original' ? 'bo' : 'web';		
		$ret = $arr = explode(',',$array_files);
		
		if(count($ret)>0 && !empty($ret[0])){
			$search = new eCommerce_Entity_Search('','',1,-1);			
			if( is_numeric($pos) ){
				$img = eCommerce_SDO_Core_Application_FileManagement::getFileByVersion($arr[ $pos ],$version);
				$pathTmp = $img->getPath();
				$filenameTmp = $img->getFilename();
				if(!empty($pathTmp) && !empty($filenameTmp)){
					$ret = ABS_HTTP_URL . "$path/{$img->getPath()}{$img->getFilename()}";
				}else{
					if(!empty($this->image_id)){
						$ret = ABS_HTTP_URL."bo/file.php?id=".$this->image_id."&type=image&img_size=predefined&height=300";
						
					}else{
						$ret = 	ABS_HTTP_URL."img/default.jpg";
					}
				}
			}else{
				$ret = array();
				foreach($arr as $p){
					$img = eCommerce_SDO_Core_Application_FileManagement::getFileByVersion($p,$version);
					$ret[] = ABS_HTTP_URL . "$path/{$img->getPath()}{$img->getFilename()}";
					
				}
			}	
					
			return $ret;
		}else{
			switch($version){
				case 'small':$size = 'width=160&height=235';break;
				case 'medium':$size = 'width=320&height=470';break;
				case 'large':$size = 'width=800&height=900';break;
			}			
			//return ABS_HTTP_URL.PATH_ACTUAL_LANG . 'system/file.php?id=0&type=image&img_size=predefined&'.$size;			
			//return ABS_HTTP_URL . "bo/file.php?id=0&type=image&img_size=predefined&$size";
			return ABS_HTTP_URL . "img/default.jpg";
		}
	}
	
	public function getOrden(){
		return $this->orden;
	}
	
	public function setOrden( $orden ){
		$this->orden = $orden;
	}
	
	public function getStatus(){
		return $this->status;
	}
	
	public function setStatus( $status ){
		$this->status = $status;
	}

	public function getFriendlyName(){return Util_String::validStringForUrl( $this->name ) ;}
	public function getFriendlyNameUrl($p = 1, $menu = ''){return eCommerce_SDO_Core_Application_Category::getFriendlyNameUrl($this->category_id,$p,$menu);}
	
}
?>