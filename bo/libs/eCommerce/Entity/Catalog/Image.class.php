<?php
/**
 * This class represents an image file
 * 
 * @author Alejandro Rivera <alejandro@vicomstudio.com>
 * @package eCommerce_Entity_Image
 *
 */
class eCommerce_Entity_Catalog_Image extends eCommerce_Entity_Util_File  {
	
	public function __construct(){
		parent::__construct();
	}
	
}
?>