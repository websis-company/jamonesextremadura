<?php
class eCommerce_Entity_Boletin extends eCommerce_Entity {

	public function __construct(){
		parent::__construct();
	}

public $boletin_id;
public $nombre;
public $telefono;
public $email;
public $fecha;


public function getBoletinId(){ 
return $this->boletin_id;
}

public function setBoletinId( $BoletinId){ 
return $this->boletin_id = $BoletinId;
}

public function getNombre(){ 
return $this->nombre;
}

public function setNombre( $Nombre){ 
return $this->nombre = $Nombre;
}

public function getTelefono(){ 
return $this->telefono;
}

public function setTelefono( $Telefono){ 
return $this->telefono = $Telefono;
}

public function getEmail(){ 
return $this->email;
}

public function setEmail( $Email){ 
return $this->email = $Email;
}

public function getFecha(){ 
return $this->fecha;
}

public function setFecha( $Fecha){ 
return $this->fecha = $Fecha;
}

}
?>