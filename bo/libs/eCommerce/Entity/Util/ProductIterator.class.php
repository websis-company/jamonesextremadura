<?php
class eCommerce_Entity_Util_ProductIterator extends ArrayIterator {
	
	/**
	 * Returns the current Product entity in the array
	 * @return eCommerce_Entity_Catalog_Product
	 */
	public function current(){
		return parent::current();
	}
	
}
?>