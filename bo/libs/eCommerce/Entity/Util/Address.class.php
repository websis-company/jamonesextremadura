<?php
class eCommerce_Entity_Util_Address extends eCommerce_Entity {
	
/**
	 * Street line 1
	 *
	 * @var string
	 */
	public $street;
	/**
	 * Street line 2
	 *
	 * @var string
	 */
	public $street2;
	/**
	 * Street line 3
	 *
	 * @var string
	 */
	public $street3;
	/**
	 * Street line 4
	 *
	 * @var string
	 */
	public $street4;
	/**
	 * City name
	 *
	 * @var string
	 */
	public $city;
	/**
	 * City state
	 *
	 * @var string
	 */
	public $state;
	/**
	 * Country code (2 Letters)
	 *
	 * @var string
	 */
	public $country;
	/**
	 * Zip Code, 5 digits
	 *
	 * @var string
	 */
	public $zip_code;
	public $phone;
	public $phone2;
	public $phone3;
	public $phone4;
	public $contact_method;
	public $municipio;
	public $contacto;
	
	public function __construct(){
		parent::__construct();
	}

	/**
	 * @return string
	 */
	public function getCity(){
		return $this->city;
	}

	/**
	 * @param string $city
	 */
	public function setCity( $city ){
		$this->city = $city;
	}

	/**
	 * @return string
	 */
	public function getCountry(){
		return $this->country;
	}

	/**
	 * @param string $country
	 */
	public function setCountry( $country ){
		$this->country = $country;
	}

	/**
	 * @return string
	 */
	public function getState(){
		return $this->state;
	}

	/**
	 * @param string $state
	 */
	public function setState( $state ){
		$this->state = $state;
	}

	/**
	 * @return string
	 */
	public function getStreet(){
		return $this->street;
	}

	/**
	 * @param string $street
	 */
	public function setStreet( $street ){
		$this->street = $street;
	}

	/**
	 * @return string
	 */
	public function getStreet2(){
		return $this->street2;
	}

	/**
	 * @param string $street2
	 */
	public function setStreet2( $street2 ){
		$this->street2 = $street2;
	}

	/**
	 * @return string
	 */
	public function getStreet3(){
		return $this->street3;
	}

	/**
	 * @param string $street3
	 */
	public function setStreet3( $street3 ){
		$this->street3 = $street3;
	}

	/**
	 * @return string
	 */
	public function getStreet4(){
		return $this->street4;
	}

	/**
	 * @param string $street4
	 */
	public function setStreet4( $street4 ){
		$this->street4 = $street4;
	}

	/**
	 * @return int
	 */
	public function getProfileId(){
		return $this->profile_id;
	}

	/**
	 * @param int $profileId
	 */
	public function setProfileId( $profileId ){
		$this->profile_id = $profileId;
	}

	/**
	 * @return string
	 */
	public function getZipCode(){
		return $this->zip_code;
	}

	/**
	 * @param string $zip_code
	 */
	public function setZipCode( $zipCode ){
		$this->zip_code = $zipCode;
	}
	
	public function getPhone() {
		return $this->phone;
	}

	public function setPhone($phone) {
		$this->phone = $phone;
	}

	public function getPhone2() {
		return $this->phone2;
	}

	public function setPhone2($phone2) {
		$this->phone2 = $phone2;
	}

	public function getPhone3() {
		return $this->phone3;
	}

	public function setPhone3($phone3) {
		$this->phone3 = $phone3;
	}

	public function getPhone4() {
		return $this->phone4;
	}

	public function setPhone4($phone4) {
		$this->phone4 = $phone4;
	}

	public function getContactMethod() {
		return $this->contact_method;
	}

	public function setContactMethod($contact_method) {
		$this->contact_method = $contact_method;
	}

	public function getMunicipio(){
		return $this->municipio;
	}

	public function setMunicipio($Municipio){
		$this->municipio = $Municipio;
	}

	public function getContacto(){
		return $this->contacto;
	}

	public function setContacto($Contacto){
		$this->contacto = $Contacto;
	}	
}
?>