<?php
class eCommerce_Entity_Util_CategoryIterator extends ArrayIterator {
	
	/**
	 * Returns the current Product entity in the array
	 * @return eCommerce_Entity_Catalog_Category
	 */
	public function current(){
		return parent::current();
	}
	
}
?>