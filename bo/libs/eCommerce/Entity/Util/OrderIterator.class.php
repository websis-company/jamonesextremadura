<?php
class eCommerce_Entity_Util_OrderIterator extends ArrayIterator {
	
	/**
	 * Returns the current Order entity in the array
	 * @return eCommerce_Entity_Order
	 */
	public function current(){
		return parent::current();
	}
	
}
?>