<?php
class eCommerce_Entity_Util_OrderAddressIterator extends ArrayIterator {
	
	/**
	 * Returns the current Address entity in the array
	 * @return eCommerce_Entity_Order_Address
	 */
	public function current(){
		return parent::current();
	}
	
}
?>