<?php
class eCommerce_Entity_Util_OrderItemArray extends ArrayObject {
	
	public function __construct( array $array = array() ){
		parent::__construct( $array, 0, "eCommerce_Entity_Util_OrderItemIterator" );
	}
	
	/**
	 * @return eCommerce_Entity_Util_OrderItemIterator
	 */
	public function getIterator(){
		return parent::getIterator();
	}
	
}
?>