<?php
class eCommerce_Entity_Util_ProductArray extends ArrayObject {
	
	public function __construct( array $array = array() ){
		parent::__construct( $array, 0, "eCommerce_Entity_Util_ProductIterator" );
	}
	
	/**
	 * @return eCommerce_Entity_Util_ProductIterator
	 */
	public function getIterator(){
		return parent::getIterator();
	}
	
	
	
}
?>