<?php
class eCommerce_Entity_Util_CategoryArray extends ArrayObject {
	
	public function __construct( array $array = array() ){
		parent::__construct( $array, 0, "eCommerce_Entity_Util_CategoryIterator" );
	}
	
	/**
	 * @return eCommerce_Entity_Util_CategoryIterator
	 */
	public function getIterator(){
		return parent::getIterator();
	}
	
	
	
}
?>