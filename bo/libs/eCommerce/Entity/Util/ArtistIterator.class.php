<?php
class eCommerce_Entity_Util_ArtistIterator extends ArrayIterator {
	
	/**
	 * Returns the current Product entity in the array
	 * @return eCommerce_Entity_Catalog_Artist
	 */
	public function current(){
		return parent::current();
	}
	
}
?>