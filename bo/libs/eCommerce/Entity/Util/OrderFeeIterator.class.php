<?php
class eCommerce_Entity_Util_OrderFeeIterator extends ArrayIterator {
	
	/**
	 * Returns the current Address entity in the array
	 * @return eCommerce_Entity_Order_Fee
	 */
	public function current(){
		return parent::current();
	}
	
}
?>