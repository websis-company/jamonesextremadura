<?php
class eCommerce_Entity_Util_AlianzasArray extends ArrayObject {
	
	public function __construct( array $array = array() ){
		parent::__construct( $array, 0, "eCommerce_Entity_Util_AlianzasIterator" );
	}
	
	/**
	 * @return eCommerce_Entity_Util_UserIterator
	 */
	public function getIterator(){
		return parent::getIterator();
	}
	
}
?>