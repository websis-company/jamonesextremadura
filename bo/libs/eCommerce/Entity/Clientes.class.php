<?php
class eCommerce_Entity_Clientes extends eCommerce_Entity {

	public function __construct(){
		parent::__construct();
	}

	public $clientes_id;
	public $nombre;
	public $comentario;
	public $image_id;


	public function getClientesId(){ 
	return $this->clientes_id;
	}

	public function setClientesId( $ClientesId){ 
	return $this->clientes_id = $ClientesId;
	}

	public function getNombre(){ 
	return $this->nombre;
	}

	public function setNombre( $Nombre){ 
	return $this->nombre = $Nombre;
	}

	public function getComentario(){ 
	return $this->comentario;
	}

	public function setComentario( $Comentario){ 
	return $this->comentario = $Comentario;
	}

	/**
	 *  Imagen Testimoniales
	 */
	public function getImageId( $pos = null){
		$ret = $arr = explode(',',$this->image_id);
		if( is_numeric($pos) ){
			$ret = $arr[ $pos ];
		}
		return $ret;
	}

	public function setImageId( $ImageId){ 
		return $this->image_id = $ImageId;
	}

	public function getUrlImageId($version,$pos = NULL,$addImageId = false){
		$strIds = ($addImageId ? $this->image_id : '') . ($addImageId && $this->image_id && $this->image_id ? ',' : '') . $this->image_id;
		return $this->getUrlArrayFiles($strIds,$version,$pos);
	}

	/**
	 * ArrayFiles
	 */
	public function getUrlArrayFiles($array_files,$version,$pos = NULL){		
		$path = $version == 'original' ? 'bo' : 'web';		
		$ret = $arr = explode(',',$array_files);
		
		if(count($ret)>0 && !empty($ret[0])){
			$search = new eCommerce_Entity_Search('','',1,-1);			
			if( is_numeric($pos) ){
				$img = eCommerce_SDO_Core_Application_FileManagement::getFileByVersion($arr[ $pos ],$version);
				$pathTmp = $img->getPath();
				$filenameTmp = $img->getFilename();
				if(!empty($pathTmp) && !empty($filenameTmp)){
					$ret = ABS_HTTP_URL . "$path/{$img->getPath()}{$img->getFilename()}";
				}else{
					if(!empty($this->image_id)){
						$ret = ABS_HTTP_URL."bo/file.php?id=".$this->image_id."&type=image&img_size=predefined&height=300";
						
					}else{
						$ret = 	ABS_HTTP_URL."img/default.jpg";
					}
				}
			}else{
				$ret = array();
				foreach($arr as $p){
					$img = eCommerce_SDO_Core_Application_FileManagement::getFileByVersion($p,$version);
					$ret[] = ABS_HTTP_URL . "$path/{$img->getPath()}{$img->getFilename()}";
					
				}
			}	
					
			return $ret;
		}else{
			switch($version){
				case 'large':$size = 'width=400&height=400';break;
			}			
			//return ABS_HTTP_URL.PATH_ACTUAL_LANG . 'system/file.php?id=0&type=image&img_size=predefined&'.$size;			
			//return ABS_HTTP_URL . "bo/file.php?id=0&type=image&img_size=predefined&$size";
			return ABS_HTTP_URL . "img/default.jpg";
		}
	}

}
?>