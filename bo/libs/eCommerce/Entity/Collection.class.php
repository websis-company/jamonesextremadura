<?php
class eCommerce_Entity_Collection extends eCommerce_Entity {

	public function __construct(){
		parent::__construct();
	}

public $collection_id;
public $name;
public $description;
public $actual;
public $image;
public $array_image;


public function getCollectionId(){ 
return $this->collection_id;
}

public function setCollectionId( $CollectionId){ 
return $this->collection_id = $CollectionId;
}

public function getName(){ 
return $this->name;
}

public function setName( $Name){ 
return $this->name = $Name;
}

public function getDescription(){ 
return $this->description;
}

public function setDescription( $Description){ 
return $this->description = $Description;
}

public function getActual(){ 
return $this->actual;
}

public function setActual( $Actual){ 
return $this->actual = $Actual;
}

public function getImage( $pos = null){
	$ret = $arr = explode(',',$this->image);
	if( is_numeric($pos) ){
		$ret = $arr[ $pos ];
	}
	return $ret;
}

public function setImage( $image ){
	$this->image = $image;
}

public function getUrlFotoId($version,$pos){
	return $this->getUrlArrayFiles($this->image,$version,$pos);
	
}
public function getUrlArrayFiles($array_files,$version,$pos = NULL){
	$ret = $arr = explode(',',$array_files);
	if(count($ret)>0 && !empty($ret[0])){
		$search = new eCommerce_Entity_Search('','',1,-1);

		if( is_numeric($pos) ){			
			$img = eCommerce_SDO_Core_Application_FileManagement::getFileByVersion($arr[ $pos ],$version);
			$ret = ABS_HTTP_URL . "web/{$img->getPath()}{$img->getFilename()}";
		}else{
			$ret = array();
			foreach($arr as $p){
				$img = eCommerce_SDO_Core_Application_FileManagement::getFileByVersion($p,$version);
				$ret[] = ABS_HTTP_URL . "web/{$img->getPath()}{$img->getFilename()}";
			}
		}
		return $ret;
	}else{
		switch($version){
			case 'small':$size = 'width=150&height=100';break;
			case 'medium':$size = 'width=300&height=200';break;
			case 'large':$size = 'width=900&height=800';break;
		}
		//return ABS_HTTP_URL.PATH_ACTUAL_LANG . 'system/file.php?id=0&type=image&img_size=predefined&'.$size;
		return false;
	}
}

public function getArrayImage( $pos = null){
	$ret = $arr = explode(',',$this->array_image);
	if( is_numeric($pos) ){
		$ret = $arr[ $pos ];
	}
	return $ret;
}
	
public function setArrayImage( $array_image ){
	$this->array_image = $array_image;
}

}
?>