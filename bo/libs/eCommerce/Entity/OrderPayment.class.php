<?php
class eCommerce_Entity_OrderPayment extends eCommerce_Entity {

	public function __construct(){
		parent::__construct();
	}

public $order_payment_id;
public $order_id;
public $code;
public $ordertransactionid;
public $transactionid;
public $state;
public $trazabilitycode;
public $authorizationcode;
public $responsecode;
public $operationdate;
public $approveddate;
public $updatedate;

/* CAMPOS DEPOSITO EN EFECTIVO */
public $pendingreason;
public $expirationdate;
public $reference;
public $barcode;
public $urlpayment;


public function getOrderPaymentId(){ 
return $this->order_payment_id;
}

public function setOrderPaymentId( $OrderPaymentId){ 
return $this->order_payment_id = $OrderPaymentId;
}

public function getOrderId(){ 
return $this->order_id;
}

public function setOrderId( $OrderId){ 
return $this->order_id = $OrderId;
}

public function getCode(){ 
return $this->code;
}

public function setCode( $Code){ 
return $this->code = $Code;
}

public function getOrdertransactionid(){ 
return $this->ordertransactionid;
}

public function setOrdertransactionid( $Ordertransactionid){ 
return $this->ordertransactionid = $Ordertransactionid;
}

public function getTransactionid(){ 
return $this->transactionid;
}

public function setTransactionid( $Transactionid){ 
return $this->transactionid = $Transactionid;
}

public function getState(){ 
return $this->state;
}

public function setState( $State){ 
return $this->state = $State;
}

public function getTrazabilitycode(){ 
return $this->trazabilitycode;
}

public function setTrazabilitycode( $Trazabilitycode){ 
return $this->trazabilitycode = $Trazabilitycode;
}

public function getAuthorizationcode(){ 
return $this->authorizationcode;
}

public function setAuthorizationcode( $Authorizationcode){ 
return $this->authorizationcode = $Authorizationcode;
}

public function getResponsecode(){ 
return $this->responsecode;
}

public function setResponsecode( $Responsecode){ 
return $this->responsecode = $Responsecode;
}

public function getOperationdate(){ 
return $this->operationdate;
}

public function setOperationdate( $Operationdate){ 
return $this->operationdate = $Operationdate;
}

public function getApproveddate(){ 
return $this->approveddate;
}

public function setApproveddate( $Approveddate){ 
return $this->approveddate = $Approveddate;
}

public function getUpdatedate(){ 
return $this->updatedate;
}

public function setUpdatedate( $Updatedate){ 
return $this->updatedate = $Updatedate;
}

public function getPendingreason(){
return $this->pendingreason;	
}

public function setPendingreason($Pendingreason){
return $this->pendingreason = $Pendingreason;	
}

public function getExpirationdate(){
return $this->expirationdate;
}

public function setExpirationdate($Expirationdate){
return $this->expirationdate = $Expirationdate;	
}

public function getReference(){
return $this->reference;
}

public function setReference($Reference){
return $this->reference = $Reference;	
}

public function getBarcode(){
return $this->barcode;
}

public function setBarcode($Barcode){
return $this->barcode = $Barcode;	
}

public function getUrlpayment(){
return $this->urlpayment;
}

public function setUrlpayment($Urlpayment){
return $this->urlpayment = $Urlpayment;	
}

}
?>