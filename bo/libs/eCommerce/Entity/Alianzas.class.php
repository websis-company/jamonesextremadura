<?php
class eCommerce_Entity_Alianzas extends eCommerce_Entity {

	public function __construct(){
		parent::__construct();
	}

public $alianzas_id;
public $nombre;
public $link;
public $logo;
public $orden;


public function getAlianzasId(){ 
return $this->alianzas_id;
}

public function setAlianzasId( $AlianzasId){ 
return $this->alianzas_id = $AlianzasId;
}

public function getNombre(){ 
return $this->nombre;
}

public function setNombre( $Nombre){ 
return $this->nombre = $Nombre;
}

public function getLink(){ 
return $this->link;
}

public function setLink( $Link){ 
return $this->link = $Link;
}

public function getLogo($position = null){ 
$image = explode(',',$this->logo);
return !is_numeric($position) ? $image : (!empty($image[$position]) ? $image[$position] : 0 );
}

public function setLogo( $Logo){ 
return $this->logo = $Logo;
}

public function getOrden(){ 
return $this->orden;
}

public function setOrden( $Orden){ 
return $this->orden = $Orden;
}

public function getUrlLogo($version,$pos){
	return $this->getUrlArrayFiles($this->logo,$version,$pos);
}

public function getUrlArrayFiles($array_files,$version,$pos = NULL){
	$ret = $arr = explode(',',$array_files);
	if(count($ret)>0 && !empty($ret[0])){
		$search = new eCommerce_Entity_Search('','',1,-1);

		if( is_numeric($pos) ){			
			$img = eCommerce_SDO_Core_Application_FileManagement::getFileByVersion($arr[ $pos ],$version);
			$ret = ABS_HTTP_URL . "web/{$img->getPath()}{$img->getFilename()}";
		}else{
			$ret = array();
			foreach($arr as $p){
				$img = eCommerce_SDO_Core_Application_FileManagement::getFileByVersion($p,$version);
				$ret[] = ABS_HTTP_URL . "web/{$img->getPath()}{$img->getFilename()}";
			}
		}
		return $ret;
	}else{
		switch($version){
			case 'small':$size = 'width=150&height=100';break;
			case 'medium':$size = 'width=300&height=200';break;
			case 'large':$size = 'width=900&height=800';break;
		}
		//return ABS_HTTP_URL.PATH_ACTUAL_LANG . 'system/file.php?id=0&type=image&img_size=predefined&'.$size;
		return false;
	}
}

public function getFriendlyName(){return Util_String::validStringForUrl( $this->nombre ) ;}
public function getFriendlyNameUrl(){return eCommerce_SDO_Core_Application_Alianzas::getFriendlyNameUrl($this->alianzas_id);}
}
?>