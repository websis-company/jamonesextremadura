<?php
class eCommerce_Entity_Galery extends eCommerce_Entity {

	public function __construct(){
		parent::__construct();
	}

public $galery_id;
public $galerycategory_id;
public $nombre;
public $array_images;


public function getGaleryId(){ 
return $this->galery_id;
}

public function setGaleryId( $GaleryId){ 
return $this->galery_id = $GaleryId;
}

public function getGalerycategoryId(){ 
return $this->galerycategory_id;
}

public function setGalerycategoryId( $GalerycategoryId){ 
return $this->galerycategory_id = $GalerycategoryId;
}

public function getNombre(){ 
return $this->nombre;
}

public function setNombre( $Nombre){ 
return $this->nombre = $Nombre;
}

public function getArrayImages($position = null){ 
$image = explode(',',$this->array_images);
return !is_numeric($position) ? $image : (!empty($image[$position]) ? $image[$position] : 0 );}

public function getHTMLArrayImages($width = null,$height= null, $nimage = 0, $target_blank = false, $urlEffect = false, $imgParam = 'border="0"'){
 $imgParam2 = empty($width) ? '' : ' width="'.$width.'"';
 $imgParam2 .= empty($height) ? '' : ' height="'.$height.'"';
 $src = $this->getArrayImages( $nimage );
 
 $effect = $urlEffect;
 if(empty($src)){
 	$src = 'ima/fotomiembro.jpg';
 	$imgParam .= $imgParam2;
 	$urlImage .= $src;
 	$title = '';
 }else{
 $file = eCommerce_SDO_Core_Application_FileManagement::LoadById($src);
 $title = $file->getDescription();
 $urlImage = BO_DIRECTORY . 'file.php?id=' . $src . '&type=image';
 	$src = BO_DIRECTORY . 'file.php?id=' . $src . '&type=image&img_size=predefined&width='.$width.'&height=' . $height;
 }
 $img = $target_blank ? '<a '. ($effect ? 'rel="jquery-lightbox"' : '' ) .' href="'.$urlImage.'" id="ImagePreviewId" target="_blank">' : '';
 $imgParam .= !$target_blank ? ' title="'.$title.'"' : '';
 $img .= "<img src='".$src."' ".$imgParam.">";
  $img .= ( $target_blank ? '</a>' : '');
 return $img;
 } 

public function setArrayImages( $ArrayImages){ 
return $this->array_images = $ArrayImages;
}

}
?>