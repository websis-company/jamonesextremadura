<?php
class eCommerce_Entity_Files extends eCommerce_Entity {

	public function __construct(){
		parent::__construct();
	}

public $files_id;
public $name;
public $size;
public $type;
public $url;
public $title;
public $description;
public $galeria_id;
public $tipo;


public function getFilesId(){ 
return $this->files_id;
}

public function setFilesId( $FilesId){ 
return $this->files_id = $FilesId;
}

public function getName(){ 
return $this->name;
}

public function setName( $Name){ 
return $this->name = $Name;
}

public function getSize(){ 
return $this->size;
}

public function setSize( $Size){ 
return $this->size = $Size;
}

public function getType(){ 
return $this->type;
}

public function setType( $Type){ 
return $this->type = $Type;
}

public function getUrl(){ 
return $this->url;
}

public function setUrl( $Url){ 
return $this->url = $Url;
}

public function getTitle(){ 
return $this->title;
}

public function setTitle( $Title){ 
return $this->title = $Title;
}

public function getDescription(){ 
return $this->description;
}

public function setDescription( $Description){ 
return $this->description = $Description;
}

public function getGaleriaId(){ 
return $this->galeria_id;
}

public function setGaleriaId( $GaleriaId){ 
return $this->galeria_id = $GaleriaId;
}

public function getTipo(){ 
return $this->tipo;
}

public function setTipo( $Tipo){ 
return $this->tipo = $Tipo;
}

}
?>