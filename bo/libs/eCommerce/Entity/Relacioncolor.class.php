<?php
class eCommerce_Entity_Relacioncolor extends eCommerce_Entity {

	public function __construct(){
		parent::__construct();
	}

public $relacioncolor_id;
public $color_id;
public $tabla_id;
public $tipo;


public function getRelacioncolorId(){ 
return $this->relacioncolor_id;
}

public function setRelacioncolorId( $RelacioncolorId){ 
return $this->relacioncolor_id = $RelacioncolorId;
}

public function getColorId(){ 
return $this->color_id;
}

public function setColorId( $FotogaleriaId){ 
return $this->color_id = $FotogaleriaId;
}

public function getTablaId(){ 
return $this->tabla_id;
}

public function setTablaId( $TablaId){ 
return $this->tabla_id = $TablaId;
}

public function getTipo(){ 
return $this->tipo;
}

public function setTipo( $Tipo){ 
return $this->tipo = $Tipo;
}

}
?>