<?php
/**
 * This class defines the accesses
 * 
 */

class eCommerce_Access extends Object{
	const USER_ADD = 		1;
	const USER_EDIT = 	2;
	const USER_SAVE =		3;
	const USER_DELETE = 4;
	const USER_LIST = 	5;
	const USER_ALL_PERMISSIONS = 10;
	
	const PRODUCT_ADD = 		11;
	const PRODUCT_EDIT = 		12;
	const PRODUCT_SAVE =		13;
	const PRODUCT_DELETE =	14;
	const PRODUCT_LIST = 		15;
	const PRODUCT_ALL_PERMISSIONS = 20;
	
	const CATEGORY_ADD = 		21;
	const CATEGORY_EDIT = 	22;
	const CATEGORY_SAVE =		23;
	const CATEGORY_DELETE =	24;
	const CATEGORY_LIST = 	25;
	const CATEGORY_ALL_PERMISSIONS = 30;
	
	const BRAND_ADD = 		31;
	const BRAND_EDIT = 		32;
	const BRAND_SAVE =		33;
	const BRAND_DELETE =	34;
	const BRAND_LIST = 		35;
	const BRAND_ALL_PERMISSIONS = 40;
	
	public $permission;
	public $entity; 
	
	public function loadPermissions(){
		$userProfile = eCommerce_FrontEnd_Util_UserSession::GetIdentity();
		$dao = new eCommerce_SDO_Core_DAO_Permission();
		return $dao->loadProfile( $userProfile->getProfileId() );
	}	

	public function deletePermission($arrPermissions){
		$permission = $this->permission;
		$arrP = array();
		foreach($arrPermissions as $permi){
			if( $permi != $permission){
				$arrP[] = $permi;
			}
		}
		return $arrP;
	}
	
	public function checkPermission($permit, $getOut = true){
		$this->permission = $permit;
		$access = $this->loadPermissions();
		$userProfile = eCommerce_FrontEnd_Util_UserSession::GetIdentity();

		if($userProfile->getRole() == eCommerce_SDO_User::ADMIN_ROLE || array_search($permit, $access) !== false){
		//if(array_search($permit, $access) !== false)
			return true;
		}else{
			if($getOut){
				//header( 'Location:index.php?redirect=' . urlencode( $_SERVER['REQUEST_URI'] ), false );
				eCommerce_FrontEnd_BO_Authentification::goToFirstPage();
				die();
			}
			else return false;
		}
	}
}
?>