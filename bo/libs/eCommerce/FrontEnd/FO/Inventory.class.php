<?php
class eCommerce_FrontEnd_FO_Inventory extends eCommerce_FrontEnd_FO {
	/**
	 * @var eCommerce_DAO_Event
	 */

	public function __construct(){
		parent::__construct();
	}

	public function execute( $cmd='' ){
		
		$this->checkPermission();
		$cmd = $cmd=='' ? $this->getCommand() : $cmd;
		switch( $cmd ){
			case 'loadInventory':
				$this->_loadInventory();
			break;
			case 'getLast3Inventory':
			default:
				$this->_getLast3Inventory();
			break;
		}
	}
	
	protected function _loadInventory(){
		
		$InventoryId = $this->getParameter();
		$Inventory = eCommerce_SDO_Inventory::LoadInventory($InventoryId );
		

		$this->tpl->assign('gallery', $Inventory);
		$this->tpl->display('galeriaView.php');
	}
	
	protected function _getLast3Inventory(){
		$this->search = new eCommerce_Entity_Search(
			'', //disabled search
			'', //column order  param example: fecha DESC
			$this->getParameter('p',true,1), //page number one
			 5 //we need only 5 registers
		);
		$extraConditions = array();
		
		//$extraConditions[] = array( "columName"=>"position","value"=>$surveyPosition,"isInteger"=>false);
		$result = eCommerce_SDO_Inventory::SearchInventory( $this->search, $extraConditions );
		
		$this->tpl->assign('result', $result);
		$this->tpl->display('galeriaMain.php');
	}

}
?>