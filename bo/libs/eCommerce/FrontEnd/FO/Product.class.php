<?php

class eCommerce_FrontEnd_FO_Product extends eCommerce_FrontEnd_FO {

	/**
	 * @var eCommerce_DAO_Event
	 */
	public function __construct(){
		parent::__construct();
		
	}

	public function execute(){
		$cmd = $this->getCommand();
		
		switch( $cmd ){
			case 'details':
				$this->_details();
			break;
			case 'productosDestacados':
				$this->_productosDestacados();
			break;
			case 'searchEspeciales':
				$this->_searchEspeciales();
				break;
			case 'search':
			default:
				$this->_search();
				break;
		}
		
	}

	protected function _details(){		
		$id        = $this->getParameter('id',true,NULL);
		$product   = eCommerce_SDO_Catalog::LoadProduct( $id );
		//$Versiones = eCommerce_SDO_Catalog::GetAllProductVersiones($id);
		//$VersionesAgrupadas = eCommerce_SDO_Catalog::GetProductVersiones($id);
		$Versiones            = eCommerce_SDO_Catalog::GetProductVersionesTodo($id);

		$idCat        = $this->getParameter('idCat',true,NULL);
		
		if(!is_null($idCat)){
			$category = eCommerce_SDO_Catalog::LoadCategory($idCat);
			$this->tpl->assign('Categoria', $category);
		}//end if
	

		/*$parents_category = eCommerce_SDO_Catalog::GetParentCategoryByProductId($id);
		$i = 0;
		foreach ($parents_category as $parent_id) {
			$category                     = eCommerce_SDO_Catalog::LoadCategory($parent_id);
			$categorys[$i]['category_id'] = $category->getCategoryId();
			$categorys[$i]['nom_catego']  = $category->getName();
			$categorys[$i]['url']         = $category->getFriendlyNameUrl();
			$i++;
		}//end foreach*/

		//$Tallas = array();
		//$Sku = array();
		$i = 0;
		foreach($Versiones as $version){			
			if($version['colornombre'] != ""){
				$Colores[$i]['colornombre'] = $version['colornombre'];	
				$Colores[$i]['color_id']    = $version['color_id'];
				$Colores[$i]['version_id']  = $version['version_id'];
				$i++;
			}
			
			if($version['tallanombre'] != ""){
				$Tallas[$i]['tallanombre'] = $version['tallanombre'];	
				$Tallas[$i]['talla_id']    = $version['talla_id'];
				$Tallas[$i]['version_id']  = $version['version_id'];
				$i++;
			}
			
		}//end foreach
		
		//$Colores = array_unique($Colores);
		//$Tallas  = array_unique($Tallas);
		
		if(isset($Colores)){
			$this->tpl->assign('colores',$Colores);	
		}
		if(isset($Tallas)){
			$this->tpl->assign('tallas',$Tallas);	
		}

		$this->tpl->assign('VersionesAgrupadas',$VersionesAgrupadas);
		$this->tpl->assign('Versiones', $Versiones);
		$this->tpl->assign('product', $product );
		
		$this->tpl->display("ProductView.php" );		
	}
	
	protected function _search(){
		$idCategory = $this->getParameter( 'idCat', true, 0);
		$innerJoin = $this->getParameter("inner",false,NULL);
		$idCategoria = $this->getParameter( 'idCat', true, 0);
		$idBrand = $this->getParameter('idBrand',true,NULL);
		$filtroHome = $this->getParameter('filtro',false,NULL);
		
		//$k = $idCategory != 0 ? '9' : '-1';
		$k = $idCategory != 0 ? '-1' : '9';
		#-- Liga Ver todos
		$p = $this->getParameter('p',false,0);
		if($p == 'todos'){
			$k = '-1';
		}

		#-- Order By
		$filtro = $_REQUEST['orden'];

		switch($filtro){
			case 'A-Z':	$orden = 'name ASC';
			break;
			case 'Z-A':$orden = 'name DESC';
			break;
			default: $orden = 'name ASC';
			break;
		}		
		
		$extraConditions   = array();
		$q = $this->getParameter( 'q', false, NULL );
		if(!empty($q)){
			$extraConditions[] = array( "SQL"=>"p.keywords LIKE '%".$q."%'");
			$this->tpl->assign( 'q', $q );	
		}
		if($filtroHome == "recientes"){
			$orden = 'p.fecha DESC';
		}elseif($filtroHome == "ofertas"){
			$orden = 'V.discount DESC';
		}
		
		$this->search = new eCommerce_Entity_Search('',$orden,$this->getParameter( 'p', true, 1 ),$this->getParameter( 'k', true, -1 ));
		$idCategory        = ( $this->getParameter( 'idCat', true, 0 ) > 0 ) ? $this->getParameter( 'idCat', true, 0 ) : null;		
		$currentCategory   = eCommerce_SDO_Catalog::LoadCategory( $idCategory );
		
		$tallas = $this->getParameter('tallas',false,NULL);		
		if($innerJoin === 'true' && !empty($tallas)){
			$strTalla = '';
			foreach($tallas as $talla){
				$strTalla .= 'relaciontalla.talla_id = '.$talla." OR ";
			}
			$strTalla = substr($strTalla,0,-3);
			$strTalla = "(".$strTalla.")";
			//$idCategory = null;
			$extraConditions[] = array( "SQL"=>$strTalla);			
			$joins[] = array('table' => 'relaciontalla', 'on' => "V.version_id = relaciontalla.tabla_id", 'join_type' => 'INNER');			
			$k = -1;
		}		
		$colores = $this->getParameter('colores',false,NULL);		
		if($innerJoin === 'true' && !empty($colores)){
			$strColor = '';
			foreach($colores as $color){
				$strColor .= 'relacioncolor.color_id = '.$color." OR ";
			}
			$strColor = substr($strColor,0,-3);
			$strColor = "(".$strColor.")";
			//$idCategory = null;
			$extraConditions[] = array( "SQL"=>$strColor);
			$joins[] = array('table' => 'relacioncolor', 'on' => "V.version_id = relacioncolor.tabla_id", 'join_type' => 'INNER');
			$k = -1;
		}
		if(!empty($idBrand)){
			$extraConditions[] = array( "SQL"=>"p.brand_id = ".$idBrand);	
		}		
		if($filtroHome == "vendidos"){			
			$extraConditions[] = array( "SQL"=>"p.mas_vendido = 'Si'");	
		}elseif($filtroHome == "ofertas"){
			$extraConditions[] = array( "SQL"=>"V.discount > 0");			
		}
		
		$extraConditions[] = array( "SQL"=>"p.status = 'Active'");		
		$result            = eCommerce_SDO_Catalog::SearchProducts($this->search, $idCategory, true, $extraConditions, $joins );		
		$this->tpl->assign( 'result', $result );
		$this->tpl->assign( 'category', $currentCategory );		
		$this->tpl->display( "ProductMain.php" );		
	}
	
	protected function _searchEspeciales(){
		$idCategory = $this->getParameter( 'idCat', true, 0);
		$subcategory = eCommerce_SDO_Catalog::GetCategoriesByParentCategory($idCategory, 1);
		$this->tpl->assign('idCat',$idCategory);
		$this->tpl->assign( 'result', $subcategory );
		$this->tpl->display( "ProductMainEspeciales.php" );
	}


	protected function _productosDestacados(){
		$this->search = new eCommerce_Entity_Search(		
			$this->getParameter( 'q', false, NULL ),
			$this->getParameter( 'o', false, 'RAND()'),
			$this->getParameter( 'p', true, 1 ),
			$this->getParameter( 'k', true, 5 )
		);

		$extraConditions   = array();
		$extraConditions[] = array( "SQL"=>"p.status = 'Active'");
		$result            = eCommerce_SDO_Catalog::SearchProducts($this->search, $idCategory, true, $extraConditions );

		$this->tpl->assign( 'result', $result );
		$this->tpl->display( "ProductMainDestacados.php" );
	}//end function
}
?>