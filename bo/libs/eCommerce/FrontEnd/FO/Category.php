<?php 
/**
* 
*/
class eCommerce_FrontEnd_FO_Category extends eCommerce_FrontEnd_FO{
	
	protected $lang;
	
	public function __construct(){
		parent::__construct();
		//$this->lang = eCommerce_SDO_Core_Application_LanguageManager::GetActualLanguage();
	}

	public function execute( $cmd='' ){	
		$this->checkPermission();
		$cmd = $cmd=='' ? $this->getCommand() : $cmd;

		switch( $cmd ){
			case 'getLast3':
				return $this->_getLast3();
			break;
			case 'loadGalerycategory':
				$this->_loadCategory();
			break;
			case 'searchCategorias':
				$this->_searchCategorias();
				break;
			case 'search':
			default:
				$this->_searchCategory();
			break;
		}
	}


	protected function _searchCategorias(){
		$idC = $this->getParameter('id',true,null);
		$this->search = new eCommerce_Entity_Search(
			$this->getParameter( 'q', false, NULL ),
			$this->getParameter( 'o', false, 'orden ASC' ),
			$this->getParameter( 'p', true, 1 ),
			$this->getParameter( 'k', true, -1 )
		);

		$currentCategory = eCommerce_SDO_Catalog::LoadCategory($idC);
		
		// Search current subcategories inside current category
		$result = eCommerce_SDO_Catalog::SearchCategoriesByParentCategory($this->search, $idC);

		echo "<pre>";
		var_dump($result);
		echo "</pre>";
		die();

		$this->tpl->assign('id_category', $idC);
		$this->tpl->assign('result', $result);
		$this->tpl->display('CategoriasHome.php');
	}


} 

?>