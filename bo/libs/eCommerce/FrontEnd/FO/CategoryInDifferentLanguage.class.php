<?php
class eCommerce_FrontEnd_FO_CategoryInDifferentLanguage extends eCommerce_FrontEnd_FO {
	/**
	 * @var eCommerce_DAO_Event
	 */

	public function __construct(){
		parent::__construct();
	}

	public function execute( $cmd='' ){
		
		$this->checkPermission();
		$cmd = $cmd=='' ? $this->getCommand() : $cmd;
		switch( $cmd ){
			case 'loadCategoryInDifferentLanguage':
				$this->_loadCategoryInDifferentLanguage();
			break;
			case 'search':
			default:
				$this->_searchCategoryInDifferentLanguage();
			break;
		}
	}
	
	protected function _loadCategoryInDifferentLanguage(){
		
		$CategoryInDifferentLanguageId = $this->getParameter();
		$CategoryInDifferentLanguage = eCommerce_SDO_CategoryInDifferentLanguage::LoadCategoryInDifferentLanguage($CategoryInDifferentLanguageId );
		

		$this->tpl->assign('CategoryInDifferentLanguage', $CategoryInDifferentLanguage);
		$this->tpl->display('view/categoryInDiferrentLanguageView.php');
	}
	
	protected function _searchCategoryInDifferentLanguage(){
		$this->search = new eCommerce_Entity_Search(
			'', //disabled search
			'', //column order  param example: fecha DESC
			$this->getParameter('p',true,1), //page number one
			 5 //we need only 5 registers
		);
		$extraConditions = array();
		
		//$extraConditions[] = array( "columName"=>"position","value"=>$surveyPosition,"isInteger"=>false);
		$result = eCommerce_SDO_CategoryInDifferentLanguage::SearchCategoryInDifferentLanguage( $this->search, $extraConditions );
		
		$this->tpl->assign('result', $result);
		$this->tpl->display('view/categoryInDiferrentLanguageMain.php');
	}

}
?>