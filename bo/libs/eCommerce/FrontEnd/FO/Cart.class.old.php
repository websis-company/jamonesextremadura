<?php
class eCommerce_FrontEnd_FO_Cart extends eCommerce_FrontEnd_FO {

	/**
	 * @var eCommerce_DAO_Event
	 */
	public function __construct(){
		parent::__construct();
	}

	public function execute(){
		
		$cmd = $this->getCommand();
		
		switch( $cmd ){
			case 'add':
				$this->_addProduct();
			break;
			
			case 'remove':
				$this->_removeProduct();
			break;
			
			case 'update':
				$this->_updateCart();
			break;
			case 'getTotalCart':
				$total = eCommerce_SDO_Cart::GetTotalCart();
				echo $total['price']['format'];				
			break;
			case 'getTotalItems':
				$total = eCommerce_SDO_Cart::GetTotalItems();
				echo $total;
			break;
			case 'show':
			default:
				$this->_show();
				break;
		}
		
	}
	
	protected function _addProduct(){
		$productId = $this->getParameter("productId",true,0);
		$versionId = $this->getParameter("versionId",true,0);
		$quantity  = $this->getParameter("quantity", true, 1);
		$atributes = $this->getParameter("atributes",false,null);
		$color     = $this->getParameter("color",true,0);
		$talla     = $this->getParameter("talla",true,0);
		
		$uvc       = $this->getParameter("uvc",false,NULL);
		$uvc       = null;
		$result    = eCommerce_SDO_Cart::addItem( $productId, $quantity, $versionId, $color, $talla );

		$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
		if( $result ){
			
			$message = "Carrito de compras";
			/*if($actualLanguage == 'EN')
				{
					$message = 'The product that you selected has been added to the cart';
				}
				else
				{
					$message = 'El producto seleccionado ha sido a&ntilde;adido al carrito';
				}*/
			$this->tpl->assign( 'messageClass', 'msjOk');
		}
		else{
			if($actualLanguage == 'EN')
			{
				$message = 'Try Again Please';
			}
			else
			{
				$message = 'Por Favor, intente nuevamente';
			}
			$this->tpl->assign( 'messageClass', 'msjErr');
		}
		
		$cart = eCommerce_SDO_Cart::GetCart();
		
		$this->tpl->assignRef( 'message', $message);
		
		$this->_show();
		
	}
	
	public function _addProductXajax($idCategory,$idProduct,$quantity,$color,$talla){
		$productId = $idProduct;
		/*$productId 	= $this->getParameter("productId",false,2);
		$productId 	= $productId."|".$talla."|".$color;*/
		
		$result 	= eCommerce_SDO_Cart::addItem( $productId, $quantity , $color, $talla);
		
		/*$cart 		= eCommerce_SDO_Cart::GetCart();
		$items 		= $cart->getItems();*/
		
		/*$totalqty	= 0;
		//$subtotal	= 0;
		foreach($items as $item){
			$product	 = eCommerce_SDO_Catalog::LoadProduct($productId);
			$totalqty 	+= $item->getQuantity();
			//subtotal	+= $product->getPrice() * $item->getQuantity();
		}*/
		
		$product	= eCommerce_SDO_Catalog::LoadProduct($productId);
		//$color = eCommerce_SDO_Color::LoadColorByColor($color);
		
		/*$this->tpl->assign('talla',$talla);
		$this->tpl->assign('color',$color);
		$this->tpl->assign('qty',$quantity);*/
		/*$this->tpl->assign('precio',"$ " . number_format($product->getPrice(),2) . " " . eCommerce_SDO_CurrencyManager::GetActualCurrency());*/
		//$this->tpl->assign('nombre',"des de aqui");
		//$this->tpl->assign('sku',"desde el metohod ");
		$this->tpl->assign('nombre',$product->getName());
		$this->tpl->assign('sku',$product->getSku());
		//$this->tpl->assign('totalqty',$totalqty);
		//$this->tpl->assign('subtotal',"$ " . number_format($subtotal,2) . " " . eCommerce_SDO_CurrencyManager::GetActualCurrency());
		
		return $this->_showXajax();
		
	}
	
	
	protected function _showXajax(){
		$cart = eCommerce_SDO_Cart::GetCart();
		$this->tpl->assign( 'cart', $cart);
		ob_start();
		$this->tpl->display("CartMainXajax.php");
		$html = ob_get_contents();
		ob_end_clean();
		return $html;
	}
	
	
	public function _showMenuXajax(){
		ob_start();
		$this->tpl->display("CartMenuXajax.php");
		$html = ob_get_contents();
		ob_end_clean();
		return $html;
	}
	
	protected function _removeProduct(){
		
		$productId = $this->getParameter("productId",false,null);
		
		$result = eCommerce_SDO_Cart::RemoveItem( $productId);
		$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
		if( $result ){
			if($actualLanguage == 'EN')
				{
					$message = 'The product that you selected has been removed from the cart';
				}
				else
				{
					$message = 'El producto seleccionado ha sido eliminado del carrito';
				}
			$this->tpl->assign( 'messageClass', 'msjOk');
		}
		else{
			if($actualLanguage == 'EN')
			{
				$message = 'Try Again Please';
			}
			else
			{
				$message = 'Por Favor, intente nuevamente';
			}
			$this->tpl->assign( 'messageClass', 'msjErr');
			$this->tpl->assign( 'messageClass', 'msjErr');
			
		}
		$this->tpl->assignRef( 'message', $message);
		
		$this->_show();
		
	}
	
	protected function _updateCart(){
		$productId = $this->getParameter("productId",false,null);
		$newQuantity = $this->getParameter( 'quantity_' . $productId );
		$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
		try{

			eCommerce_SDO_Cart::UpdateQuantity( $productId, $newQuantity );
			$product = eCommerce_SDO_Catalog::LoadProduct( $productId );
		
			if($actualLanguage == 'EN')
				{
					$msg1 = "Product: " . $product->getName() . " has been updated";
				}
				else
				{
					$msg1 = "El producto " . $product->getName() . " ha sido actualizado";
				}
			$this->tpl->assign('message', $msg1);
			$this->tpl->assign( 'messageClass', 'msjOk');

		}catch( eCommerce_SDO_Exception $e ){
			$item 	= eCommerce_SDO_Core_Application_Cart::GetCart()->getItem( $productId );
			$uvc 	= $item->getUnidadVentaCliente();
			$peso	= eCommerce_SDO_Catalog::LoadProduct($productId)->getWeight(true);
			if($uvc == "Peso"){
				if($actualLanguage == 'EN')$msg2 = "Please enter a valid value of product or quantity, the minimum value must be $peso";
				else $msg2 = "Por favor ingrese un valor v&aacute;lido para el producto o cantidad, el valor m&iacute;nimo debe ser $peso";
			}
			else{
				if($actualLanguage == 'EN')$msg2 = "Please enter a valid value of product or quantity";
				else $msg2 = "Por favor ingrese un valor v&aacute;lido para el producto o cantidad";
			}
			
			$this->tpl->assign('message', $msg2 );
			$this->tpl->assign( 'messageClass', 'msjErr');
		}
		
		$this->_show();
		
	}
	
	/*protected function _show(){
		$cart = eCommerce_SDO_Cart::GetCart();
		$this->tpl->assign( 'cart', $cart);
		$this->tpl->display("CartMain.php");
	}*/
	protected function _show($inAjax = false){
		$inAjax = $inAjax ? $inAjax : $this->getParameter('inAjax',false,false);
		$cart = eCommerce_SDO_Cart::GetCart();
		
		$this->tpl->assign( 'cart', $cart);
		$total = eCommerce_SDO_Cart::GetTotalCart(true, false);
		var_dump($total);
		die();
		
		$this->tpl->display($inAjax ? 'AjaxCartMain.php' : "CartMain.php");
	}
}
?>