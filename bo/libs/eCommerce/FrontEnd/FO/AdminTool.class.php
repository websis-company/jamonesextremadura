<?php
class eCommerce_FrontEnd_FO_AdminTool extends eCommerce_FrontEnd_FO {
	/**
	 * @var eCommerce_DAO_Event
	 */

	public function __construct(){
		parent::__construct();
	}

	public function execute( $cmd='' ){
		
		$this->checkPermission();
		$cmd = $cmd=='' ? $this->getCommand() : $cmd;
		switch( $cmd ){
			case 'loadAdminTool':
				$this->_loadAdminTool();
			break;
			case 'search':
			default:
				$this->_searchAdminTool();
			break;
		}
	}
	
	protected function _loadAdminTool(){
		
		$AdminToolId = $this->getParameter();
		$AdminTool = eCommerce_SDO_AdminTool::LoadAdminTool($AdminToolId );
		

		$this->tpl->assign('AdminTool', $AdminTool);
		$this->tpl->display('view/AdminToolView.php');
	}
	
	protected function _searchAdminTool(){
		$this->search = new eCommerce_Entity_Search(
			'', //disabled search
			'', //column order  param example: fecha DESC
			$this->getParameter('p',true,1), //page number one
			 5 //we need only 5 registers
		);
		$extraConditions = array();
		
		//$extraConditions[] = array( "columName"=>"position","value"=>$surveyPosition,"isInteger"=>false);
		$result = eCommerce_SDO_AdminTool::SearchAdminTool( $this->search, $extraConditions );
		
		$this->tpl->assign('result', $result);
		$this->tpl->display('view/AdminToolMain.php');
	}

}
?>