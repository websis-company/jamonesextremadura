<?php
class eCommerce_FrontEnd_FO_Authentification extends eCommerce_FrontEnd_FO{
	const ERR_RESTRICTED_ACCESS = 303;
	const ADMITED_ROLES = 'Customer,Admin,Anonymous';
	 
	public function __construct(){
		parent::__construct();
	}
	
	public function execute($cmd = NULL){

		$cmd = empty($cmd)?$this->getCommand():$cmd;
		
		switch( $cmd ){
			case 'anonymous_login':
			case 'login':
				$this->login( $_POST["username"], $_POST["password"], $cmd);
			break;
			case 'formLogin': $this->tpl->display( "UserLoginForm.php" );
				break;
			case 'logout':
			eCommerce_FrontEnd_Util_UserSession::DestroyIdentity();
			unset($_SESSION['Auth']);
			$this->redirect(ABS_HTTP_URL, false);
			break;
			
			default:
				if( $this->isValidCode( $_REQUEST["errorCode"] ) ){
					$message = '';
					$add1 = $this->tpl->trans('user_password_incorrect');
					$add2 = $this->tpl->trans('restricted_access');
					$add3 = $this->tpl->trans('system_problem');
					
					switch( $_REQUEST["errorCode"] ){
					
					
						case eCommerce_SDO_Core_Application_Security_Exception::ERR_INVALID_PASSWORD:
						case eCommerce_SDO_Core_Application_Security_Exception::ERR_USER_NOT_FOUND:
							$message .= $add1;
						break;
						
						case eCommerce_FrontEnd_BO_Authentification::ERR_RESTRICTED_ACCESS:
							$message .= $add2;
						break;
						default:
							$message .= $add3;
						break;
					}
					
					
					$this->tpl->assign( 'error', $_REQUEST["errorCode"] );
					$this->tpl->assign( 'message', $message );
				}
				//$this->tpl->display( "UserLogin.php" );
				$this->tpl->display( "UserLogin.php" );
			break;
			case 'setNewPasswordByEmail':$this->_setNewPasswordByEmail();
				break;
			case 'recoverPasswordByEmail':
				$this->_recoverPasswordByEmail();
			case 'recoverpwd':
				$this->_recoverpwd();
			break;
		}
	}
	
	
	protected function _setNewPasswordByEmail(){
		$email = $this->getParameter("email",false,'');
		try{
			$userProfile = eCommerce_SDO_User::LoadUserProfileByEmail($email);
			$email = $userProfile->getEmail();
			$profileId = $userProfile->getProfileId();
			$role = $userProfile->getRole();
			$status = $userProfile->getConfirmado();
			if( !empty($profileId) && !empty($email ) && $role == eCommerce_SDO_User::ANONYMOUS_ROLE && $status == 'No') {
				$psw = eCommerce_SDO_Core_Application_Profile::PasswordReset($profileId);
				$main_message = $this->trans('your').' '.$this->trans('password').' '.$this->trans('is').':' . $psw;
				$styles = array(
						"containerWidth" => "500px",
						"fontSize1" => "12px",
						"fontColor1" => "#666",
						"fontColor2" => "#666",
						"headerImage" => array(
								"src" => Config::getGlobalValue('logo_email_project'),
								"width" => "0px",
								"height" => "0px",
						),
						"divisionLineColor" => "#F00"
				);
				Util_Email::sendHTMLMail(false, true, $userProfile, $this->trans('password_recovery'), $main_message, $styles);
				$msg = 'Hemos enviado su nueva contrase&ntilde;a al correo '.$email;
				$msgClass = 'alert-success';
				$userProfile->setConfirmado('Si');
				$userProfile->setRole('Customer');
				$userProfile->setPassword(md5($psw));
				eCommerce_SDO_User::SaverUserProfile($userProfile,false);
			}else{
				$msg = empty($email ) ? 'Por favor ingrese su correo electr&oacute;nico' : (empty($profileId) ? 'El correo '.$email.' no se encuentra en nuestro sistema.' : 'El email '.$email." no esta asociada
							ninguna cuenta, por favor seleccione solicitar una contrase&ntilde;a para continuar");
				$msgClass = 'alert-danger';
			}
		}
		catch(eCommerce_SDO_Core_Application_Security_Exception $e){
			$msg = 'No pudo enviarse su nueva contrase&ntilde;a, por favor intente mas tarde';
			$msgClass = 'alert-error';
		}
	
		$this->tpl->assign('msg',$msg);
		$this->tpl->assign('msgClass',$msgClass);
		$this->tpl->display("UserLoginForm.php");
	}
	
	protected function _recoverPasswordByEmail(){			
			$email = $this->getParameter("email",false,'');				
			try{
				$userProfile = eCommerce_SDO_User::LoadUserProfileByEmail($email);
				$email = $userProfile->getEmail();
				$profileId = $userProfile->getProfileId();
				$role = $userProfile->getRole();
				$status = $userProfile->getConfirmado();
					
				if( !empty($profileId) && !empty($email ) && $role != eCommerce_SDO_User::ANONYMOUS_ROLE && $status == 'Si') {
						
					$psw = eCommerce_SDO_Core_Application_Profile::PasswordReset($profileId);
					$main_message = $this->trans('your').' '.$this->trans('password').' '.$this->trans('is').':' . $psw;					
					$styles = array(
							"containerWidth" => "500px",
							"fontSize1" => "12px",
							"fontColor1" => "#666",
							"fontColor2" => "#666",
							"headerImage" => array(
									"src" => Config::getGlobalValue('logo_email_project'),
									"width" => "0px",
									"height" => "0px",
							),
							"divisionLineColor" => "#F00"
					);					
					Util_Email::sendHTMLMail(false, true, $userProfile, $this->trans('password_recovery'), $main_message, $styles);
					
					$msg = 'Hemos enviado su nueva contrase&ntilde;a al correo '.$email;
					$msgClass = 'alert-success';
				}else{					
					$msg = empty($email ) ? 'Por favor ingrese su correo electr&oacute;nico' : (empty($profileId) ? 'El correo '.$email.' no se encuentra en nuestro sistema.' : 'El email '.$email." no esta asociada 
							ninguna cuenta, por favor seleccione solicitar una contrase&ntilde;a para continuar");
					$msgClass = 'alert-danger';
				}
			}
			catch(eCommerce_SDO_Core_Application_Security_Exception $e){
				$msg = 'No pudo enviarse su nueva contrase&ntilde;a, por favor intente mas tarde';
				$msgClass = 'alert-error';
			}
		
		$this->tpl->assign('msg',$msg);
		$this->tpl->assign('msgClass',$msgClass);
		$this->tpl->display("UserLoginForm.php");
	}
	
	protected function _recoverpwd(){

		if($this->getParameter("save")){
			$email = $this->getParameter("email",false,'');
			
			try{
				$userProfile = eCommerce_SDO_User::LoadUserProfileByEmail($email);
				$email = $userProfile->getEmail();				
				$profileId = $userProfile->getProfileId();				
				if( !empty($profileId) && !empty($email )) {					
					$psw = eCommerce_SDO_Core_Application_Profile::PasswordReset($profileId);					
					$main_message = $this->trans('your').' '.$this->trans('password').' '.$this->trans('is').':' . $psw;
					$styles = array(
								    "containerWidth" => "500px",
								    "fontSize1" => "12px",
								    "fontColor1" => "#666",
								    "fontColor2" => "#666", 
								    "headerImage" => array(
								       "src" => Config::getGlobalValue('logo_email_project'),
								       "width" => "0px",
								       "height" => "0px",
										),
								    "divisionLineColor" => "#F00"
									);
					Util_Email::sendHTMLMail(false, true, $userProfile, $this->trans('password_recovery'), $main_message, $styles);
					$msg = 'Hemos enviado su nueva contrase&ntilde;a al correo '.$emailPost;
					$msgClass = 'alert-success';
				}else{
					$msg = empty($emailPost ) ? 'Por favor ingrese su correo electr&oacute;nico' : (empty($profileId) ? 'El correo '.$emailPost.' no se encuentra en nuestro sistema.' : '');
					$msgClass = 'alert-error';
				}
			}
			catch(eCommerce_SDO_Core_Application_Security_Exception $e){
				$msg = 'No pudo enviarse su nueva contrase&ntilde;a, por favor intente mas tarde';
				$msgClass = 'alert-error';
			}
		}
		
		
		$this->tpl->assign('msg',$msg);
		$this->tpl->assign('msgClass',$msgClass);
		$this->tpl->display("UserRecoverPassword.php");
	}
	
	public function isValidCode( &$code ) {
		$valid_codes = array();

		$valid_codes[] = eCommerce_SDO_Core_Application_Security_Exception::ERR_CASE_NOT_CONTEMPLATED;
		$valid_codes[] = eCommerce_SDO_Core_Application_Security_Exception::ERR_INVALID_PASSWORD;
		$valid_codes[] = eCommerce_SDO_Core_Application_Security_Exception::ERR_SQL_EXCEPTION;
		$valid_codes[] = eCommerce_SDO_Core_Application_Security_Exception::ERR_USER_NOT_FOUND;
		$valid_codes[] = eCommerce_FrontEnd_BO_Authentification::ERR_RESTRICTED_ACCESS;
				
		return ( isset($code) & is_numeric( $code ) && in_array( $code, $valid_codes ) ); 	
	}
	
	/*public function login( $user, $password , $cmd = 'login'){

		
		try {
			$id = eCommerce_SDO_Security::Authenticate( $user, $password);
			$userProfile = new eCommerce_Entity_User_Profile();
			
			$userProfile = eCommerce_SDO_User::LoadUserProfile( $id );
			
			$admitedRoles = split(",", self::ADMITED_ROLES);
			if( in_array( $userProfile->getRole(), $admitedRoles) ){
				eCommerce_FrontEnd_Util_UserSession::SetIdentity( $userProfile );
				$this->goToFirstPage();
			}
			else{
				$this->redirect(ABS_HTTP_URL."login.php?errorCode=" . eCommerce_FrontEnd_BO_Authentification::ERR_RESTRICTED_ACCESS,false);
			}
		}
		catch(eCommerce_SDO_Core_Application_Security_Exception $e){
			$this->redirect(ABS_HTTP_URL."login.php?errorCode=" . $e->getCode(),false);
		}
		
	}*/	
	public function login( $user, $password, $cmd = 'login'){
		
		try {
			eCommerce_FrontEnd_Util_Session::Set('login_user', $user);			
			$userProfile = new eCommerce_Entity_User_Profile();			
			/*$admitedRoles = preg_split("/,/", self::ADMITED_ROLES);
			$loged = in_array( $userProfile->getRole(), $admitedRoles);*/
			$url_login = ABS_HTTP_URL."login.php?errorCode=" . eCommerce_FrontEnd_BO_Authentification::ERR_RESTRICTED_ACCESS.'&username='.$user;
			$existeUser = eCommerce_SDO_User::LoadUserProfileByEmail($user);			
			$existeProfileId = $existeUser->getProfileId();	
			$userType = $_REQUEST['userType'];							
			if($userType != 'FormularioUser'){
				if(empty($existeProfileId) && $userType != 'new'){						
					if($userType == 'FormularioUser' && empty($existeProfileId)){
						$url_login = ABS_HTTP_URL."user_login.php?error=1";
						$this->redirect($url_login,false);			
						$_SESSION['Autentificado'] = 'false';			
						die();
					}else{							
						$existeUser->setFirstName("");
						$existeUser->setLastName("");
						$existeUser->setPassword(md5("sinpassword"));				
						$existeUser->setConfirmPassword(md5("sinpassword"));
						$existeUser->setEmail( $user );
						$existeUser->setRole( eCommerce_SDO_User::ANONYMOUS_ROLE );						
						$Usuario = eCommerce_SDO_User::SaverUserProfile($existeUser,false);
						$id = $Usuario->getProfileId();
						$cmd = 'anonymous_login';
						$_SESSION['Auth'] = 'true';
						$_SESSION['Autentificado'] = 'false';
					}
				}else{							
					eCommerce_FrontEnd_Util_Session::Delete('login_user'); 
					eCommerce_FrontEnd_Util_Session::Set('find_user', 'true');
					$userProfile      = eCommerce_SDO_User::LoadUserProfileByEmail($user);
					eCommerce_FrontEnd_Util_UserSession::SetIdentity( $userProfile );
					$_SESSION['Auth'] = 'false';
					$_SESSION['Autentificado'] = 'false';
				}
			}
			
			switch($cmd){
				case 'anonymous_login':
					$userProfile->setEmail($user);
					$userProfile->setPassword(md5("sinpassword"));
					$userProfile->setRole(eCommerce_SDO_User::ANONYMOUS_ROLE);
					break;				
				case 'login':
				default:																			
					if($existeUser->getRole() == eCommerce_SDO_User::CUSTOMER_ROLE && !empty($password)){
						$_SESSION['Autentificado'] = 'true';	
					}
					
					if($userType != 'new' && $userType != 'FormularioUser')
						$password = ($existeUser->getRole() == eCommerce_SDO_User::ANONYMOUS_ROLE)?md5('sinpassword'):((($existeUser->getRole() == eCommerce_SDO_User::CUSTOMER_ROLE))?$existeUser->getPassword():$password); 									
					
					$id = eCommerce_SDO_Security::Authenticate( $user, $password);								
					if($id != false){
						$userProfile 	= eCommerce_SDO_User::LoadUserProfile( $id );
						eCommerce_FrontEnd_Util_Session::Set('find_user', 'true');
						
						//if($userProfile->getRole() != eCommerce_SDO_User::ANONYMOUS_ROLE)
						//		$_SESSION['Auth'] = 'true';
					}else{
						/**** Acción cuando no este registraado el usuario **/						
						if($userType == 'new' || $userType == 'FormularioUser')
							$url_login = ABS_HTTP_URL."user_login.php?error=1";
						else
							$url_login = ABS_HTTP_URL."login.php?cmd=anonymous_login&username=".$user;
						$this->redirect($url_login,false);						
						die();
					}
					break;
			}
			
			$admitedRoles = preg_split("/,/", self::ADMITED_ROLES);
			if( in_array( $userProfile->getRole(), $admitedRoles) ){
				eCommerce_FrontEnd_Util_Session::Delete('login_user');
				eCommerce_FrontEnd_Util_UserSession::SetIdentity( $userProfile );				
				$this->goToFirstPage();
			}
			else{
				/*$this->redirect(ABS_HTTP_URL."login.php?errorCode=" . eCommerce_FrontEnd_BO_Authentification::ERR_RESTRICTED_ACCESS,false);*/
				$url_login = ABS_HTTP_URL."login.php?errorCode=" . eCommerce_FrontEnd_BO_Authentification::ERR_RESTRICTED_ACCESS.'&username='.$user;
				$this->redirect($url_login,false);
				
			}
			
			/*$id = eCommerce_SDO_Security::Authenticate( $user, md5($password));
			$userProfile = new eCommerce_Entity_User_Profile();
			
			$userProfile = eCommerce_SDO_User::LoadUserProfile( $id );
			
			$admitedRoles = split(",", self::ADMITED_ROLES);
			if( in_array( $userProfile->getRole(), $admitedRoles) ){
				eCommerce_FrontEnd_Util_UserSession::SetIdentity( $userProfile );
				$this->goToFirstPage();
			}
			else{
				$this->redirect(ABS_HTTP_URL."login.php?errorCode=" . eCommerce_FrontEnd_BO_Authentification::ERR_RESTRICTED_ACCESS,false);
			}*/
		}
		catch(eCommerce_SDO_Core_Application_Security_Exception $e){
			$this->redirect(ABS_HTTP_URL."login.php?errorCode=" . $e->getCode().'&username='.$user,false);
		}
		
	}


	/*public function loginV2($emailUser, $cmd){
		try{
			$id          = eCommerce_SDO_Security::Authenticate( $emailUser, false);	
			$userProfile = eCommerce_SDO_User::LoadUserProfile( $id );

		}catch(eCommerce_SDO_Core_Application_Security_Exception $e){
			$this->redirect(ABS_HTTP_URL."login.php?errorCode=" . $e->getCode().'&username='.$user,false);
		}
	}*/
	
	public function verifyAuthentification( $dieAndRedirect = true, $admin = false, $anonymous = true){
		$userProfile = eCommerce_FrontEnd_Util_UserSession::GetIdentity();
		$admitedRoles = preg_split("/,/", self::ADMITED_ROLES);
  		if( !isset($userProfile)  || !in_array( $userProfile->getRole(), $admitedRoles) ) {  			
  			if( $dieAndRedirect ){
  				eCommerce_FrontEnd_Util_UserSession::DestroyIdentity();  			
  				$protocolo = 'http://';
  				eCommerce_FrontEnd_Util_Session::Set("url_redirect",  $protocolo . $_SERVER['SERVER_NAME'] . $_SERVER["PHP_SELF"] . ( ( isset($_SERVER['QUERY_STRING']) && !empty($_SERVER['QUERY_STRING']) ) ?  "?" . $_SERVER['QUERY_STRING'] : "" )  );
  				//$url_login = ABS_HTTP_URL."login.php?errorCode=" . eCommerce_FrontEnd_BO_Authentification::ERR_RESTRICTED_ACCESS;
  				$url_login = ABS_HTTP_URL."login.php";
  				eCommerce_FrontEnd_BO_Authentification::redirect($url_login,false);
  			}
  			else{
  				return false;
  			}
  		}
		return true;
		
	}

	public function goToFirstPage(){
		$url = eCommerce_FrontEnd_Util_Session::Get("url_redirect");
		if( eCommerce_FrontEnd_FO_Authentification::verifyAuthentification( false ) 
		&& isset( $url ) ){
			eCommerce_FrontEnd_Util_Session::Delete( "url_redirect" );
		}
		else{
			$es_anonimo = eCommerce_FrontEnd_Util_UserSession::GetIdentity()->getRole() == eCommerce_SDO_User::ANONYMOUS_ROLE;			
			if($es_anonimo){
				eCommerce_FrontEnd_Util_UserSession::DestroyIdentity();
				$url = ABS_HTTP_URL."cart.php";
			}else{
				//$url = ABS_HTTP_URL."user.php";
				$userType = $this->getParameter('userType',false,NULL);
				$url = (empty($userType))?ABS_HTTP_URL."cart.php":ABS_HTTP_URL."user.php";
			}			
		}
		eCommerce_FrontEnd_BO_Authentification::redirect( $url,false);
	}
	
}
?>