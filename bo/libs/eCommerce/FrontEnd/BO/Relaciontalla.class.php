<?php
class eCommerce_FrontEnd_BO_Relaciontalla extends eCommerce_FrontEnd_BO {


public function getSDOSearch( $search, $extraConditions ){
	return eCommerce_SDO_Relaciontalla::SearchRelaciontalla( $search, $extraConditions );
}	


/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
	
	/**
	 * @var eCommerce_DAO_Event
	 */
	protected $search;
	protected $idFields;
	
	public function __construct(){
		parent::__construct();
		$this->idFields = 'relaciontalla_id';
	}

	public function execute(){
		//$accessManager = new eCommerce_Access_Relaciontalla( $this );
		//$accessManager->checkPermission(eCommerce_Access::RELACIONTALLA_ALL_PERMISSIONS);
		//$this->checkPermission();
		
		$cmd = $this->getCommand();
		switch( $cmd ){
			case 'remove':
				$this->_remove();
				break;
			case 'delete':
				//$accessManager->checkPermission(eCommerce_Access::RELACIONTALLA_DELETE);
				$this->_delete();
				break;
			case 'add':
				//$accessManager->checkPermission(eCommerce_Access::RELACIONTALLA_ADD);
				$this->_add();
				break;
			case 'edit':
				//$accessManager->checkPermission(eCommerce_Access::RELACIONTALLA_EDIT);
				$this->_edit();
				break;
			case 'save':
				//$accessManager->checkPermission(eCommerce_Access::RELACIONTALLA_SAVE);
				$this->_save();
				break;
			case 'list':
			case 'listIn':
			case 'listOut':
			default:
				//$accessManager->checkPermission(eCommerce_Access::RELACIONTALLA_LIST);
				$this->_list($cmd);
				break;
		}
	}

	protected function _list($cmd = 'listIn'){
		$table_id 	= $this->getParameter('table_id');
		$tipo		= $this->getParameter('tipo', false, NULL);
		
		$versionname = eCommerce_SDO_Core_Application_Version::LoadById($table_id)->getName();
		
		$this->search = new eCommerce_Entity_Search(
			$this->getParameter( 'q', false, NULL ),
			$this->getParameter( 'o', false, '' ), //fecha DESC
			$this->getParameter( 'p', true, 1 ),
			$this->getParameter( 'k', true, -1 )
		);
		$extraConditions = array();
		
	switch ($cmd){
			case 'listOut':							
				$extraConditions[] 	= array( "SQL" => "tabla_id != $table_id OR tabla_id IS NULL");
				$joins[] 			= array('table' => 'relaciontalla', 'on' => "talla.talla_id = relaciontalla.talla_id AND tabla_id = $table_id", 'join_type' => 'LEFT');					
				$result 			= eCommerce_SDO_Talla::SearchTalla($this->search, $extraConditions, $joins);							
				break;
			case 'listIn':
			default:				
				$extraConditions[] = array( "SQL" => "tabla_id = $table_id");
				$extraConditions[] = array( "SQL" => "tipo = '$tipo'");							
				$result = $this->getSDOSearch( $this->search, $extraConditions );
				
				break;
		}
		
		$addLinkObj	= $cmd == 'listOut' ? '' : "?table_id=$table_id&tipo=$tipo&cmd=listOut";
		
		switch ($tipo){
			case 'producto':
				$tituloObj		= $versionname;
				$returnLinkObj	=  $cmd == 'listOut' ? "?table_id=$table_id&tipo=$tipo" : 'Version.php';				
				break;
			case 'evento':
				$tituloObj	= eCommerce_SDO_Evento::LoadEvento($table_id)->getTitulo();
				$returnLinkObj	=  $cmd == 'listOut' ? "?table_id=$table_id&tipo=$tipo" : 'Evento.php';
				break;
		}
		
		
		//$result = $this->getSDOSearch( $this->search, $extraConditions ); 
		
		
		$exportToExcel = $this->getParameter( 'toExcel' );
		if ( $exportToExcel ){
		
			$regiters = $result->getResults();
			$data = array();
			foreach($regiters as $regiter){
				$data[] = get_object_vars( $regiter );
			}
			$this->exportToExcel( $data );

		}
		else {
			$viewConfig = array();			
			$viewConfig['name'] = 'Atributo';
			$viewConfig['title'] = $tituloObj . ' | Atributos ' . ($cmd == 'listOut' ? ' sin relaci&oacute;n' : 'relacionados');
			$viewConfig['id'] = $cmd == 'listOut' ? 'talla_id' : $this->idFields;
						
			
			//---------------------------
			if(strpos($viewConfig['id'],",") > -1){
				$ids = explode(",",$viewConfig['id']);
				$id = $ids[0];
				unset($ids[0]);
				$viewConfig['id'] = '{$'.$id.'}';
				foreach($ids as $id){
					$viewConfig['id'] .= ',{$'.$id.'}';
				}
			}else{
				$viewConfig['id'] = '{$'.$viewConfig['id'].'}';
			}
			//---------------------------
			
			$viewConfig["hiddenColums"]= array('relaciontalla_id','tipo');
			if($cmd == 'listOut'){
				$viewConfig["hiddenColums"][] = 'talla_id';
				$viewConfig["columNamesOverride"]["nombre"] = "Productos";
			}
			else$viewConfig["columNamesOverride"]["talla_id"] = "Atributo";
			
			$viewConfig["columNamesOverride"]["tabla_id"] = "Producto";
			
			
			$this->tpl->assign( 'viewConfig', $viewConfig );
			
			$this->tpl->assign( 'options', $result );
			
			$this->tpl->assign( 'returnLinkObj', $returnLinkObj );
			$this->tpl->assign( 'addLinkObj', $addLinkObj );
			$this->tpl->assign( 'table_id', $table_id );
			$this->tpl->assign( 'tipo', $tipo );
			$this->tpl->assign( 'cmd2', $cmd == 'listOut' ? 'add' : 'remove' );
			$this->tpl->assign( 'icon', $cmd == 'listOut' ? 'apply.png' : 'error.png' );
			$this->tpl->assign( 'lblAction', $cmd == 'listOut' ? 'Agregar' : 'Remover' );
			
			$this->tpl->display(  "Relaciontalla/list.php" );
		}
	}
	
	protected function _add(){
		try{
			$id 		= $this->getParameter();
			$tipo		= $this->getParameter('tipo', false, NULL);
			$table_id	= $this->getParameter('table_id');
			
			$extraConditions[] = array("SQL" => "talla_id = $id");
			$extraConditions[] = array("SQL" => "tabla_id = $table_id");
			$extraConditions[] = array("SQL" => "tipo = '$tipo'");
			
			$relacionTMP = current(eCommerce_SDO_Relaciontalla::SearchRelaciontalla(new eCommerce_Entity_Search('','',1,1),$extraConditions)->getResults());
			
			if(!$relacionTMP){
				
				$relacion = new eCommerce_Entity_Relaciontalla();
				
				$relacion->setTallaId($id);
				$relacion->setTablaId($table_id);
				$relacion->setTipo($tipo);				
				eCommerce_SDO_Relaciontalla::SaverRelaciontalla($relacion);
				$this->tpl->assign( 'strSuccess', 'Se relacion&oacute; un atributo exitosamente' );
			}else{
				$this->tpl->assign( 'strError', 'El atributo fue relacionada anteriormente, no necesita volverla a relacionar' );
			}

			$this->_list('listOut');
		}catch( eCommerce_SDO_Core_Validator_Exception $e){
			$this->tpl->assign( 'strError', 'Intente Nuevamente' );
			$this->_list('listOut');
		}
		
		
	/*	try{
			$Relaciontalla = new eCommerce_Entity_Relaciontalla();
			//$Relaciontalla = eCommerce_SDO_Relaciontalla::SaverRelaciontalla( $Relaciontalla );
			$this->_edit( $Relaciontalla->getRelaciontallaId() );
		}catch( eCommerce_SDO_Core_Validator_Exception $e){
			$this->tpl->assign( 'strError', 'Intente Nuevamente' );
			$this->_list();
		}*/
	}
	
	protected function _edit($id = null){
		//$id = $this->getParameter();
		$id = empty($id ) ? $this->getParameter('id',false,0) : $id;
		//---------------------------
		if( strpos($id,",") > -1 ){
			$idF = explode(",",$this->idFields);
			$ArrId = explode(",",$id);
			$id = array();
			for($i=0; $i< count($idF); $i++){
				$id[ $idF[$i] ] = (empty($ArrId[$i])) ? 0 : $ArrId[$i];
			}
		}
		//---------------------------
		
		$entity = eCommerce_SDO_Relaciontalla::LoadRelaciontalla( $id );
		
		
		$this->tpl->assign( 'errors', new Validator_ErrorHandler() );
		
		$this->edit( $entity, $this->getCommand() );

	}

	public function edit( $entity, $cmd ){

		$this->tpl->assign( 'object',     $entity );
		
		$viewConfig = array();
		$form = array();

		
		$form['name'] = 'RelaciontallaForm';
		$form['elements'] = array();
		
		
		/***********************************************************************************************************/
		
		/***********************************************************************************************************/
		
		//$viewConfig['id'] = 'noticia_evento_id';
		
		$viewConfig['form'] = $form;
		$viewConfig['title'] = ( $cmd =='add' ? 'Agregar' : 'Editar' ) . ' Relaciontalla';
		
		$this->tpl->assign( 'form', $form );	
		$this->tpl->assign( 'viewConfig', $viewConfig );
		
		//$this->tpl->assign( 'ArrayImages', $entity->getArrayImages() );

		$this->tpl->assign( 'cancelParams', $cmd == 'add' ? '?cmd=delete&id=' . $entity->getRelaciontallaId() . '' : '' );
		$this->tpl->assign( 'cancelConfirm', $cmd == 'add' ? 'if(confirm(\'Desea eliminar el registro recientemente creado?\'))'  : '' );
		
			$this->tpl->display( 'Relaciontalla/edit.php' );
	}

	protected function exportToExcel( $data, $fileName = '' ){

		

		$rowO = $data[0];

		

		$contentHtml .= '<table border="1">';

		$contentHtml .= '<tr>

		<td height="72" width="255" bgcolor="#cccccc"><img src="'.ABS_HTTP_URL.'/ima/logo.gif"></td>

		<td colspan="'.(count($rowO) - 1).'"><h2>Order Report - '.date("M d Y").'</h2></td>

		</tr>';

		

		$contentHtml .= '<tr>';

		

		foreach($rowO as $key => $row){

			$key = str_replace("_",' ',$key);

			$key = ucwords($key);

			$contentHtml .= '<th align="center">'.$key.'</th>';

		}

		$contentHtml .= '</tr>';

		

		$i = 0;

		foreach($data as $row){

			

			$bgcolor = (++$i % 2 == 0) ? 'bgcolor="#cccccc"' : '';

			$contentHtml .= '<tr>';

			foreach($row as $rowElement){

				$contentHtml .= '<td '.$bgcolor.' align="right">'.$rowElement.'</td>';

			}

			$contentHtml .= '</tr>';

		}

		

		$contentHtml .= '</table>';

		

		

		$total_bytes = strlen($contentHtml);

		header("Content-type: application/vnd.ms-excel");

		header("Content-disposition: attachment; filename=".$fileName.".xls; size=" . $total_bytes);

		echo $contentHtml;

	}

	protected function _save(){
		
		if ( !empty( $_REQUEST ) && isset( $_REQUEST['entity'] ) ){
	  		
			$entity = $_REQUEST['entity'];
  			
			$Relaciontalla = new eCommerce_Entity_Relaciontalla();
			

			// Save type
			try{
			
			
			/******************************************************/
			/******************************************************/
			
  			/******************************************************/
  			/*****************************************************/
  			
				
				/*
				$RelaciontallaTmp = eCommerce_SDO_Relaciontalla::LoadRelaciontalla( $Relaciontalla->getRelaciontallaId() );
				
				$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('RelaciontallaImages');
				$imageHandler->setArrImages( $RelaciontallaTmp->getArrayImages() );
				
				$imageHandler->setUploadFileDirectory('files/images/noticias/');
				$imageHandler->setUploadFileDescription('Imagen de Evento ' . $Relaciontalla->getRelaciontallaId() );
				
				$imageHandler->setValidTypes( array('word','image') );
				
				$imageHandler->setAccessType( eCommerce_SDO_ImageHandler::GetValidAccessType('public') );
				
				$Relaciontalla->setArrayImages( implode(',',$RelaciontallaTmp->getArrayImages()) );
				$imageHandler->setMaximum( 1 );
				$arrImages = $imageHandler->proccessImages();
				$Relaciontalla->setArrayImages( $arrImages );
				*/
				
				
				eCommerce_SDO_Relaciontalla::SaverRelaciontalla( $Relaciontalla );
				$this->tpl->assign( 'strSuccess', 'Registro Atributo ha sido guardado.' );
				$this->_list();
			}
			catch( eCommerce_SDO_Core_Validator_Exception $e){
				$this->tpl->assign( 'errors', new Validator_ErrorHandler( "Existen Algunos Errores", $e->getErrors() ) );
				$oldCmd = $this->getParameter( 'old_cmd', false, null );
				$this->edit( $Relaciontalla,  $oldCmd );
			}
		}
		else{
			throw new Exception('No hay suficiente informaci&oacute;n intente nuevamente');
		}
	}

	protected function _delete(){
		//$id = $this->getParameter();
		$id = $this->getParameter('id',false,0);
		$id2Str = $id;
		//---------------------------
		if( strpos($id,",") > -1 ){
			$idF = explode(",",$this->idFields);
			$ArrId = explode(",",$id);
			$id = array();
			$id2Str = "(";
			for($i=0; $i< count($idF); $i++){
				$id[ $idF[$i] ] = (empty($ArrId[$i])) ? 0 : $ArrId[$i];
				$id2Str .=($i>0) ? ',' : '';
				$id2Str .= $ArrId[$i] . "";
			}
			$id2Str .= ")";
		}
		//---------------------------
		
			try{
				$Relaciontalla = eCommerce_SDO_Relaciontalla::DeleteRelaciontalla($id );
				//$this->tpl->assign( 'strSuccess', 'Relaciontalla "' . $Relaciontalla->getRelaciontallaId() . '" ha sido eliminado' );
				$this->tpl->assign( 'strSuccess', 'Registro Atributo "' . $id2Str . '" ha sido eliminado' );
				
			}
			catch(eCommerce_SDO_Core_Application_Exception $e) {
				//debug($e);
				$this->tpl->assign( 'strError', $e->getMessage() );
			}
		
		$this->_list();
	}
	
protected function _remove(){
		try{
			$id 		= $this->getParameter();
			
			$relacionTMP = eCommerce_SDO_Relaciontalla::LoadRelaciontalla($id);
			$idTMP = $relacionTMP->getRelaciontallaId();
			if($idTMP == $id){
				eCommerce_SDO_Relaciontalla::DeleteRelaciontalla($id);
				$this->tpl->assign( 'strSuccess', 'El atributo ya no est&aacute; relacionada' );
			}else{
				$this->tpl->assign( 'strError', 'La relaci&oacute;n no existe' );
			}
			
			$this->_list();
		}catch( eCommerce_SDO_Core_Validator_Exception $e){
			$this->tpl->assign( 'strError', 'Intente Nuevamente' );
			$this->_list();
		}
	}
	

}
?>