<?php
class eCommerce_FrontEnd_BO_User extends eCommerce_FrontEnd_BO {

	/**
	 * @var eCommerce_DAO_Event
	 */
	protected $search;

	public function __construct(){
		parent::__construct();
		
	}

	public function execute(){
		eCommerce_FrontEnd_BO_Authentification::verifyAuthentification();
		//$this->checkPermission();
		$cmd = $this->getCommand();
		switch( $cmd ){
			case 'saveAddress':
				$this->_saveAddress();
				break;
			case 'editAddress':
				$this->_editAddress();
				break;
			case 'delete':
				$this->_delete();
				break;
			case 'add':
			case 'edit':
				$this->_edit();
				break;
			case 'save':
				$this->_save();
				break;
			case 'list':
			default:
				$this->_list();
				break;
		}
	}

	protected function _list(){
		
		$this->search = new eCommerce_Entity_Search(
			$this->getParameter( 'q', false, NULL ),
			$this->getParameter( 'o', false, 'first_name' ),
			$this->getParameter( 'p', true, 1 ),
			$this->getParameter( 'k', true, -1 )
		);
		
		$result = eCommerce_SDO_User::SearchProfiles( $this->search );
		
		$exportToExcel = $this->getParameter( 'toExcel' );
		if ( $exportToExcel ){
		
			$users = $result->getResults();
			$data = array();
			foreach($users as $user){
				$data[] = get_object_vars( $user );
			}
			$this->exportToExcel( $data );

		}
		else {
			$this->tpl->assign( 'options', $result );
			
			$this->tpl->display(  "user/list.php" );
		}
	}
		
	protected function _edit(){
		$id = $this->getParameter();

		$entity = eCommerce_SDO_User::LoadUserProfile( $id );
		
		$this->tpl->assign( 'errors', new Validator_ErrorHandler() );
		
		$this->edit( $entity, $this->getCommand() );

	}

	public function edit( $entity, $cmd ){
		$ArrRoles      = eCommerce_SDO_User::GetRoles();
		$ArrSaludo     = eCommerce_SDO_User::GetSaludo();
		$ArrConfirmado = eCommerce_SDO_User::GetConfirmado();
		$this->tpl->assign( 'user',     $entity );
		$this->tpl->assign( 'strSubtitles' , ( $cmd =='add' ? 'Agregar' : 'Editar' ) . ' Usuarios' );
		$this->tpl->assign( 'strCmd'       , $this->getCommand() );		
		$this->tpl->assign( 'ArrRoles', $ArrRoles );
		$this->tpl->assign( 'ArrSaludo', $ArrSaludo );
		$this->tpl->assign( 'ArrConfirmado', $ArrConfirmado );
		$this->tpl->display( 'user/edit.php' );
	}

	protected function exportToExcel( $data ){

		$xls = new ExcelWriter( 'Events_' . date('Y-m-d'), 'Products' );
		$xls->writeHeadersAndDataFromArray( $data );


	}

	protected function _save(){
		
		if ( !empty( $_REQUEST ) && isset( $_REQUEST['entity'] ) ){
	  		
		$entity = $_REQUEST['entity'];
  		$user = new eCommerce_Entity_User_Profile();
  		$user->setProfileId( $entity['id'] );
  		$user->setEmail( $entity['email'] );
  		$user->setFirstName( $entity['first_name'] );
  		$user->setLastName( $entity['last_name'] );
  		
  		$oldPass = $_REQUEST['oldPass'];
  		$tmpPass = $entity['password'];
  		if($oldPass == $tmpPass){
  			$user->setPassword($entity['password']);
  		}else{
  			$user->setPassword(md5($tmpPass));  		
  		}  		  		
  		//$user->setPassword( $entity['password'] );
  		$user->setRole( $entity['role'] );
		$user->setConfirmado( $entity['confirmado'] );
		$user->setSaludo( $entity['saludo'] );
			// Save user
			try{
				eCommerce_SDO_User::SaverUserProfile( $user );
				$this->tpl->assign( 'strSuccess', $this->tpl->trans('the').' '.$this->tpl->trans('user').' "' . $user->getEmail() . '" '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('saved')));
				$this->_list();
			}
			catch( eCommerce_SDO_Core_Validator_Exception $e){
				$this->tpl->assign( 'errors', new Validator_ErrorHandler( $this->tpl->trans("there_are_errors"), $e->getErrors() ) );
				$oldCmd = $this->getParameter( 'old_cmd', false, null );
				$this->edit( $user,  $oldCmd );
			}
		}
		else{
			throw new Exception($this->tpl->trans('not_enough_information').'. '.$this->tpl->trans('try_again'));
		}
	}

	protected function _delete(){
		$id = $this->getParameter();
		if ( !empty( $id ) ) {
			try{
				$user = eCommerce_SDO_Core_Application_Profile::Delete($id );
				$this->tpl->assign( 'strSuccess', $this->tpl->trans('the').' '.$this->tpl->trans('user').' "' . $user->getEmail() . '" '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('deleted')));
			}
			catch(eCommerce_SDO_Core_Application_Exception $e) {
				//debug($e);
				$this->tpl->assign( 'strError', $e->getMessage() );
			}
		}
		$this->_list();
	}

	protected function _editAddress(){
		$profileId = $this->getParameter();
		$address = eCommerce_SDO_User::LoadUserAddress( $profileId );
		
		$this->tpl->assign( 'errors', new Validator_ErrorHandler( ) );
		$this->editAddress( $address );
	}
	
	public function editAddress( eCommerce_Entity_User_Address $address ){
		
		$profile = eCommerce_SDO_User::LoadUserProfile( $address->getProfileId() );
		
		$this->tpl->assign( 'profile', $profile ); 
		$this->tpl->assign( 'address', $address );
		$this->tpl->display( 'user/address.php' );
	}
	
	public function _saveAddress(){
		$entity = $this->getParameter( 'entity', false, array() );
		
		$address = new eCommerce_Entity_User_Address();
		$address->setProfileId( $entity['profile_id'] );
		$address->setStreet( $entity['street'] );
		$address->setStreet2( $entity['street2'] );
		$address->setStreet3( $entity['street3'] );
		$address->setStreet4( $entity['street4'] );
		$address->setCity( $entity['city'] );
		$address->setState( $entity['state'] );
		$address->setCountry( $entity['country'] );
		$address->setZipCode( $entity['zip_code'] );
		$address->setPhone( $entity['phone'] );
		
		try {
			eCommerce_SDO_User::SaveUserAddress( $address );
			$this->tpl->assign( 'strSuccess', $this->tpl->trans('the').' '.$this->tpl->trans('address').' '.$this->tpl->trans('of_the').' '.$this->tpl->trans('user')." ".$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('saved')));
			
			$this->_list();
		}
		catch( eCommerce_SDO_Core_Validator_Exception $e ) {
			$this->tpl->assign( 'errors', new Validator_ErrorHandler( $this->tpl->trans("there_are_errors"), $e->getErrors() ) );
			$this->editAddress( $address );
		}
		
	}
}
?>