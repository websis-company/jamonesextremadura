<?php
class eCommerce_FrontEnd_BO_Banner extends eCommerce_FrontEnd_BO {


public function getSDOSearch( $search, $extraConditions ){
	return eCommerce_SDO_Banner::SearchBanner( $search, $extraConditions );
}	


/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
	
	/**
	 * @var eCommerce_DAO_Event
	 */
	protected $search;
	protected $idFields;
	
	public function __construct(){
		parent::__construct();
		$this->idFields = 'banner_id';
	}

	public function execute(){
//		$accessManager = new eCommerce_Access_Banner( $this );
//		$accessManager->checkPermission(eCommerce_Access::BANNER_ALL_PERMISSIONS);
	//	$this->checkPermission();
		
		$cmd = $this->getCommand();
		switch( $cmd ){
			case 'delete':
//				$accessManager->checkPermission(eCommerce_Access::BANNER_DELETE);
				$this->_delete();
				break;
			case 'add':
//				$accessManager->checkPermission(eCommerce_Access::BANNER_ADD);
//				$this->_add();
//				break;
			case 'edit':
//				$accessManager->checkPermission(eCommerce_Access::BANNER_EDIT);
				$this->_edit();
				break;
			case 'save':
//				$accessManager->checkPermission(eCommerce_Access::BANNER_SAVE);
				$this->_save();
				break;
			case 'list':
			default:
//				$accessManager->checkPermission(eCommerce_Access::BANNER_LIST);
				$this->_list();
				break;
		}
	}

	protected function _list(){

		$this->search = new eCommerce_Entity_Search(
			$this->getParameter( 'q', false, NULL ),
			$this->getParameter( 'o', false, 'banner_id DESC' ), //fecha DESC
			$this->getParameter( 'p', true, 1 ),
			$this->getParameter( 'k', true, 10 )
		);
		$extraConditions = array();
		//$extraConditions[] = array( "columName"=>"type","value"=>'',"isInteger"=>false);
		$result = $this->getSDOSearch( $this->search, $extraConditions ); 
		
		
		$exportToExcel = $this->getParameter( 'toExcel' );
		if ( $exportToExcel ){
		
			$regiters = $result->getResults();
			$data = array();
			foreach($regiters as $regiter){
				$data[] = get_object_vars( $regiter );
			}
			$this->exportToExcel( $data );

		}
		else {
			$viewConfig = array();
			$viewConfig['name'] = 'Banner';
			$viewConfig['title'] = 'Banner';
			$viewConfig['id'] = $this->idFields;
			
			//---------------------------
			if(strpos($viewConfig['id'],",") > -1){
				$ids = explode(",",$viewConfig['id']);
				$id = $ids[0];
				unset($ids[0]);
				$viewConfig['id'] = '{$'.$id.'}';
				foreach($ids as $id){
					$viewConfig['id'] .= ',{$'.$id.'}';
				}
			}else{
				$viewConfig['id'] = '{$'.$viewConfig['id'].'}';
			}
			//---------------------------
			
			$viewConfig["hiddenColums"]= array('banner_id','titulo','orden','link','url');
			$viewConfig["columNamesOverride"] = array("status"=>"Status");
			$viewConfig["columNamesOverride"] = array("imagen_movil"=>"Imagen Movil");
			
			$this->tpl->assign( 'viewConfig', $viewConfig );
			
			$this->tpl->assign( 'options', $result );
			
			$this->tpl->display(  "Banner/list.php" );
		}
	}
	
	protected function _add(){
		try{
			$Banner = new eCommerce_Entity_Banner();
			$Banner = eCommerce_SDO_Banner::SaverBanner( $Banner );
			$this->_edit( $Banner->getBannerId() );
		}catch( eCommerce_SDO_Core_Validator_Exception $e){
			$this->tpl->assign( 'strError', 'Intente Nuevamente' );
			$this->_list();
		}
	}
	
	protected function _edit($id = null){
		//$id = $this->getParameter();
		$id = empty($id ) ? $this->getParameter('id',false,0) : $id;
		//---------------------------
		if( strpos($id,",") > -1 ){
			$idF = explode(",",$this->idFields);
			$ArrId = explode(",",$id);
			$id = array();
			for($i=0; $i< count($idF); $i++){
				$id[ $idF[$i] ] = (empty($ArrId[$i])) ? 0 : $ArrId[$i];
			}
		}
		//---------------------------
		
		$entity = eCommerce_SDO_Banner::LoadBanner( $id );
		
		
		$this->tpl->assign( 'errors', new Validator_ErrorHandler() );
		
		$this->edit( $entity, $this->getCommand() );

	}

	public function edit( $entity, $cmd ){

		$this->tpl->assign( 'object',     $entity );
		
		$viewConfig = array();
		$form = array();

		
		$form['name'] = 'BannerForm';
		$form['elements'] = array();
		
		
		/***********************************************************************************************************/
		$form['elements'][] = array( 'title' => 'BannerId', 'type'=>'hidden', 'options'=>array(), 'name'=>'entity[banner_id]', 'id'=>'banner_id', 'value'=> $entity->getBannerId() );
		$form['elements'][] = array( 'title' => 'Titulo', 'type'=>'hidden', 'options'=>array(), 'name'=>'entity[titulo]', 'id'=>'titulo', 'value'=> $entity->getTitulo() );
		$form['elements'][] = array( 'title' => 'Url', 'type'=>'hidden', 'options'=>array(), 'name'=>'entity[url]', 'id'=>'url', 'value'=> $entity->getUrl() );
		$form['elements'][] = array( 'title' => 'Orden', 'type'=>'hidden', 'options'=>array(), 'name'=>'entity[orden]', 'id'=>'orden', 'value'=> $entity->getOrden() );
		$form['elements'][] = array( 'title' => 'Especificaci&oacute;n ', 'type'=>'label',  'value'=> "Tama&ntilde;o de Imagen <b>1920 x 812 pixeles</b>" );
		$form['elements'][] = array( 'title' => 'Imagen', 'type'=>'array_files', 'options'=>array(), 'name'=>'entity[imagen]', 'id'=>'imagen', 'value'=> $entity->getImagen() , 'max_files'=>1 );
		
		$form['elements'][] = array( 'title' => 'Especificaci&oacute;n ', 'type'=>'label',  'value'=> "Tama&ntilde;o de Imagen <b>768 x 1024 pixeles</b>" );
		$form['elements'][] = array( 'title' => 'Imagen Movil', 'type'=>'array_files', 'options'=>array(), 'name'=>'entity[imagen_movil]', 'id'=>'imagen_movil', 'value'=> $entity->getImagenMovil() , 'max_files'=>1 );
		$form['elements'][] = array( 'title' => 'Url: <strong>ej. http://printproyect.com </strong><br/>', 'type'=>'text', 'options'=>array(), 'name'=>'entity[link]', 'id'=>'link', 'value'=> $entity->getLink());
		$form['elements'][] = array( 'title' => 'Status', 'type'=>'select', 'options'=>eCommerce_SDO_Core_Application_Banner::GetEnumValues('status'), 'name'=>'entity[status]', 'id'=>'status', 'value'=> $entity->getStatus() );
		/***********************************************************************************************************/
		
		//$viewConfig['id'] = 'noticia_evento_id';
		
		$viewConfig['form'] = $form;
		$viewConfig['title'] = ( $cmd =='add' ? 'Agregar' : 'Editar' ) . ' Banner';
		
		$this->tpl->assign( 'form', $form );	
		$this->tpl->assign( 'viewConfig', $viewConfig );
		
		//$this->tpl->assign( 'ArrayImages', $entity->getArrayImages() );

		$this->tpl->assign( 'cancelParams', $cmd == 'add' ? '?cmd=delete&id=' . $entity->getBannerId() . '' : '' );
		$this->tpl->assign( 'cancelConfirm', $cmd == 'add' ? 'if(confirm(\'Desea eliminar el registro recientemente creado?\'))'  : '' );
		
			$this->tpl->display( 'Banner/edit.php' );
	}

	protected function exportToExcel( $data, $fileName = '' ){

		

		$rowO = $data[0];

		

		$contentHtml .= '<table border="1">';

		$contentHtml .= '<tr>

		<td height="72" width="255" bgcolor="#cccccc"><img src="'.ABS_HTTP_URL.'/ima/logo.gif"></td>

		<td colspan="'.(count($rowO) - 1).'"><h2>Order Report - '.date("M d Y").'</h2></td>

		</tr>';

		

		$contentHtml .= '<tr>';

		

		foreach($rowO as $key => $row){

			$key = str_replace("_",' ',$key);

			$key = ucwords($key);

			$contentHtml .= '<th align="center">'.$key.'</th>';

		}

		$contentHtml .= '</tr>';

		

		$i = 0;

		foreach($data as $row){

			

			$bgcolor = (++$i % 2 == 0) ? 'bgcolor="#cccccc"' : '';

			$contentHtml .= '<tr>';

			foreach($row as $rowElement){

				$contentHtml .= '<td '.$bgcolor.' align="right">'.$rowElement.'</td>';

			}

			$contentHtml .= '</tr>';

		}

		

		$contentHtml .= '</table>';

		

		

		$total_bytes = strlen($contentHtml);

		header("Content-type: application/vnd.ms-excel");

		header("Content-disposition: attachment; filename=".$fileName.".xls; size=" . $total_bytes);

		echo $contentHtml;

	}

	protected function _save(){
		
		if ( !empty( $_REQUEST ) && isset( $_REQUEST['entity'] ) ){
	  		
			$entity = $_REQUEST['entity'];
  			
			$Banner = new eCommerce_Entity_Banner();
			

			// Save type
			try{
			
			
			/******************************************************/
			/******************************************************/
			
$txt = (empty($entity['banner_id']) ? '' : $entity['banner_id']);$Banner->setBannerId( strip_tags($txt) );
$txt = (empty($entity['titulo']) ? '' : $entity['titulo']);$Banner->setTitulo( strip_tags($txt) );
$txt = (empty($entity['imagen']) ? '' : $entity['imagen']);$Banner->setImagen( strip_tags($txt) );
$txt = (empty($entity['url']) ? '' : $entity['url']);$Banner->setUrl( strip_tags($txt) );
$txt = (empty($entity['orden']) ? '' : $entity['orden']);$Banner->setOrden( strip_tags($txt) );
$txt = (empty($entity['status']) ? '' : $entity['status']);$Banner->setStatus( strip_tags($txt) );
$txt = (empty($entity['link']) ? '' : $entity['link']);$Banner->setLink( strip_tags($txt) );
  			/******************************************************/
  			/*****************************************************/
$BannerTmp = eCommerce_SDO_Banner::LoadBanner( $Banner->getBannerId() );
$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('imagen');
$imageHandler->setArrImages( $BannerTmp->getImagen() );
$imageHandler->setUploadFileDirectory('files/images/banner/');
$imageHandler->setUploadVersionDirectory('web/');
$imageHandler->setUploadFileDescription('Imagen de Evento ' . $Banner->getBannerId() );
$imageHandler->setValidTypes( array('image') );
$imageHandler->setAccessType('public' );
$Banner->setImagen( implode(',',$BannerTmp->getImagen()) );
$arrVersions = array();
$arrVersions[] = array('version'=>'large',	'path'=>'images/banner/',	'width'=>1920, 'height'=>812);
$imageHandler->setArrVersions($arrVersions);			
$imageHandler->setMaximum( 1 );
$arrImages = $imageHandler->proccessImages();
$Banner->setImagen( $arrImages );


$BannerTmp = eCommerce_SDO_Banner::LoadBanner( $Banner->getBannerId() );
$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('imagen_movil');
$imageHandler->setArrImages( $BannerTmp->getImagenMovil() );
$imageHandler->setUploadFileDirectory('files/images/banner/');
$imageHandler->setUploadVersionDirectory('web/');
$imageHandler->setUploadFileDescription('Imagen de Evento ' . $Banner->getBannerId() );
$imageHandler->setValidTypes( array('image') );
$imageHandler->setAccessType('public' );
$Banner->setImagenMovil( implode(',',$BannerTmp->getImagenMovil()) );
$arrVersions = array();
$arrVersions[] = array('version'=>'large',	'path'=>'images/banner/',	'width'=>768, 'height'=>1024);
$imageHandler->setArrVersions($arrVersions);			
$imageHandler->setMaximum( 1 );
$arrImages = $imageHandler->proccessImages();
$Banner->setImagenMovil( $arrImages );
				
				/*
				$BannerTmp = eCommerce_SDO_Banner::LoadBanner( $Banner->getBannerId() );
				
				$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('BannerImages');
				$imageHandler->setArrImages( $BannerTmp->getArrayImages() );
				
				$imageHandler->setUploadFileDirectory('files/images/noticias/');
				$imageHandler->setUploadFileDescription('Imagen de Evento ' . $Banner->getBannerId() );
				
				$imageHandler->setValidTypes( array('word','image') );
				
				$imageHandler->setAccessType( eCommerce_SDO_ImageHandler::GetValidAccessType('Banner') );
				
				$Banner->setArrayImages( implode(',',$BannerTmp->getArrayImages()) );
				$imageHandler->setMaximum( 1 );
				$arrImages = $imageHandler->proccessImages();
				$Banner->setArrayImages( $arrImages );
				*/
				
				
				eCommerce_SDO_Banner::SaverBanner( $Banner );
				$this->tpl->assign( 'strSuccess', 'Registro Banner ha sido guardado.' );
				$this->_list();
			}
			catch( eCommerce_SDO_Core_Validator_Exception $e){
				$this->tpl->assign( 'errors', new Validator_ErrorHandler( "Existen Algunos Errores", $e->getErrors() ) );
				$oldCmd = $this->getParameter( 'old_cmd', false, null );
				$this->edit( $Banner,  $oldCmd );
			}
		}
		else{
			throw new Exception('No hay suficiente informaci&oacute;n intente nuevamente');
		}
	}

	protected function _delete(){
		//$id = $this->getParameter();
		$id = $this->getParameter('id',false,0);
		$id2Str = $id;
		//---------------------------
		if( strpos($id,",") > -1 ){
			$idF = explode(",",$this->idFields);
			$ArrId = explode(",",$id);
			$id = array();
			$id2Str = "(";
			for($i=0; $i< count($idF); $i++){
				$id[ $idF[$i] ] = (empty($ArrId[$i])) ? 0 : $ArrId[$i];
				$id2Str .=($i>0) ? ',' : '';
				$id2Str .= $ArrId[$i] . "";
			}
			$id2Str .= ")";
		}
		//---------------------------
		
			try{
				$Banner = eCommerce_SDO_Banner::DeleteBanner($id );
				//$this->tpl->assign( 'strSuccess', 'Banner "' . $Banner->getBannerId() . '" ha sido eliminado' );
				$this->tpl->assign( 'strSuccess', 'Registro Banner "' . $id2Str . '" ha sido eliminado' );
				
			}
			catch(eCommerce_SDO_Core_Application_Exception $e) {
				//debug($e);
				$this->tpl->assign( 'strError', $e->getMessage() );
			}
		
		$this->_list();
	}
	
	
	

}
?>