<?php
class eCommerce_FrontEnd_BO_Galeria extends eCommerce_FrontEnd_BO {
public function getSDOSearch( $search, $extraConditions ){
	return eCommerce_SDO_Galeria::SearchGaleria( $search, $extraConditions );
}	
/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
	/**
	 * @var eCommerce_DAO_Event
	 */
	protected $search;
	protected $idFields;
	public function __construct(){
		parent::__construct();
		$this->idFields = 'galeria_id';
	}


	public function execute(){
		$this->checkPermission();
		$cmd = $this->getCommand();
		switch( $cmd ){
			case 'delete':
				$this->_delete();
				break;
			case 'add':
			case 'edit':
				$this->_edit();
				break;
			case 'save':
				$this->_save();
				break;
			case 'list':
			default:
				$this->_list();
				break;
		}
	}


	protected function _list(){
		$this->search = new eCommerce_Entity_Search(
			$this->getParameter( 'q', false, NULL ),
			$this->getParameter( 'o', false, 'galeria_id DESC' ), //fecha DESC
			$this->getParameter( 'p', true, 1 ),
			$this->getParameter( 'k', true, 10 )
		);
		$extraConditions = array();
		//$extraConditions[] = array( "columName"=>"type","value"=>'',"isInteger"=>false);
		$result = $this->getSDOSearch( $this->search, $extraConditions ); 
		$exportToExcel = $this->getParameter( 'toExcel' );
		if ( $exportToExcel ){
			$regiters = $result->getResults();
			$data = array();
			foreach($regiters as $regiter){
				$data[] = get_object_vars( $regiter );
			}
			$this->exportToExcel( $data );
		}
		else {
			$viewConfig = array();
			$viewConfig['name'] = 'Galeria';
			$viewConfig['title'] = 'Galeria';
			$viewConfig['id'] = $this->idFields;
			//---------------------------
			if(strpos($viewConfig['id'],",") > -1){
				$ids = explode(",",$viewConfig['id']);
				$id = $ids[0];
				unset($ids[0]);
				$viewConfig['id'] = '{$'.$id.'}';
				foreach($ids as $id){
					$viewConfig['id'] .= ',{$'.$id.'}';
				}
			}else{
				$viewConfig['id'] = '{$'.$viewConfig['id'].'}';
			}
			//---------------------------
			$viewConfig["hiddenColums"]= array('galeria_id','descripcion','material_recomendado','formato_imagen');
			$viewConfig["columNamesOverride"]["array_images"] = 'Iamgen';
			$viewConfig["columNamesOverride"]["galerycategory_id"] = 'Categoría';
			$this->tpl->assign( 'viewConfig', $viewConfig );
			$this->tpl->assign( 'options', $result );
			$this->tpl->display(  "Galeria/list.php" );
		}
	}


	protected function _add(){
		try{
			$Galeria = new eCommerce_Entity_Galeria();
			$Galeria = eCommerce_SDO_Galeria::SaverGaleria( $Galeria );
			$this->_edit( $Galeria->getGaleriaId() );
		}catch( eCommerce_SDO_Core_Validator_Exception $e){
			$this->tpl->assign( 'strError', $this->tpl->trans('try_again') );
			$this->_list();
		}
	}


	/*protected function getMultipleArray($parent, $sangria, $idParent){
		$search     = new eCommerce_Entity_Search(null,null,1,-1);	// -1 = ALL
		$result     = eCommerce_SDO_Galerycategory::SearchCategoriesByParentCategory($search, $parent);
		$categories = $result->getResults();
		$data = array();
		foreach($categories as $category){
			$data[] = get_object_vars($category);
		}
		foreach($data as $g_category){
			$sel = ($idParent == $g_category['galerycategory_id']) ? 'selected = "selected"' : '';
			$this->multipleLevels .= "<option value='$g_category[galerycategory_id]' $sel>$sangria$g_category[name] </option>";
			$this->getMultipleArray($g_category['galerycategory_id'], $sangria.'&nbsp;&nbsp;&nbsp;&nbsp;', $g_category["parent_category_id"]);
		}
	}*/


	protected function _edit($id = null){
		//$id = $this->getParameter();
		$id = empty($id ) ? $this->getParameter('id',false,0) : $id;
		//---------------------------
		if( strpos($id,",") > -1 ){
			$idF = explode(",",$this->idFields);
			$ArrId = explode(",",$id);
			$id = array();
			for($i=0; $i< count($idF); $i++){
				$id[ $idF[$i] ] = (empty($ArrId[$i])) ? 0 : $ArrId[$i];
			}
		}
		//---------------------------
		$entity = eCommerce_SDO_Galeria::LoadGaleria( $id );
		$this->tpl->assign( 'errors', new Validator_ErrorHandler() );
		$this->edit( $entity, $this->getCommand() );
	}


	public function edit( $entity, $cmd ){
		$this->tpl->assign( 'object',     $entity );
		$viewConfig = array();
		$form = array();
		$arrCat = array();
		$categorias = eCommerce_SDO_Core_Application_Galerycategory::GetAllInArray();
		foreach ($categorias as $key => $value) {
			if(in_array($value, $arrCat,false)){
				$arrCat[$key] = '&nbsp;&nbsp;&nbsp;&nbsp;'.$arrCat[$key];
			}else{
				$arrCat[$key] = $value;	
			}
			$c = eCommerce_SDO_Core_Application_Galerycategory::GetCategoriesByParentCategory($key);
			$res = $c->getResults();
			if(count($res) > 0){
				foreach ($res as $res_i) {
					$arrCat[$res_i->getGalerycategoryId()] = $res_i->getName();
				}//end foreach
			}//end if
		}//end foreach
		$form['name'] = 'GaleriaForm';
		$form['elements'] = array();
		/***********************************************************************************************************/
		$form['elements'][] = array( 'title' => 'GaleriaId:', 'type'=>'hidden', 'options'=>array(), 'name'=>'entity[galeria_id]', 'id'=>'galeria_id', 'value'=> $entity->getGaleriaId() );
		$form['elements'][] = array( 'title' => 'Categoría:', 'type'=>'select', 'options'=>$arrCat, 'name'=>'entity[galerycategory_id]', 'id'=>'galerycategory_id', 'value'=> $entity->getGalerycategoryId() );
		$form['elements'][] = array( 'title' => 'Nombre:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[nombre]', 'id'=>'nombre', 'value'=> $entity->getNombre() );
		
		$form['elements'][] = array( 'title' => 'Descripción Breve:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[descripcion]', 'id'=>'descripcion', 'value'=> $entity->getDescripcion() );
		$form['elements'][] = array( 'title' => 'Material Recomendado:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[material_recomendado]', 'id'=>'material_recomendado', 'value'=> $entity->getMaterialRecomendado() );
		$form['elements'][] = array( 'title' => 'Formato de Imagen:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[formato_imagen]', 'id'=>'formato_imagen', 'value'=> $entity->getFormatoImagen() );
		
		$form['elements'][] = array( 'title' => 'Precio:', 'type'=>'float', 'options'=>array(), 'name'=>'entity[precio]', 'id'=>'precio', 'value'=> $entity->getPrecio() );
		$form['elements'][] = array( 'title' => 'Keywords:', 'type'=>'textarea', 'options'=>array(), 'name'=>'entity[keywords]', 'id'=>'keywords', 'value'=> $entity->getKeywords() );
		$form['elements'][] = array( 'title' => 'Imagen <br/> <strong>600x600pixeles</strong>:', 'type'=>'array_files', 'options'=>array(), 'name'=>'entity[array_images]', 'id'=>'array_images', 'value'=> $entity->getArrayImages(), 'max_file'=>1 );
		$form['elements'][] = array( 'title' => 'Destacado:', 'type'=>'select', 'options'=>eCommerce_SDO_Core_Application_Galeria::GetEnumValues('destacado'), 'name'=>'entity[destacado]', 'id'=>'destacado', 'value'=> $entity->getDestacado() );
		/***********************************************************************************************************/
		//$viewConfig['id'] = 'noticia_evento_id';
		$viewConfig['form'] = $form;
		$viewConfig['title'] = ( $cmd =='add' ? $this->tpl->trans('add') : $this->tpl->trans('edit') ) . ' Galeria';
		$this->tpl->assign( 'form', $form );	
		$this->tpl->assign( 'viewConfig', $viewConfig );
		//$this->tpl->assign( 'ArrayImages', $entity->getArrayImages() );
		$this->tpl->assign( 'cancelParams', $cmd == 'add' ? '?cmd=delete&id=' . $entity->getGaleriaId() . '' : '' );
		$this->tpl->assign( 'cancelConfirm', $cmd == 'add' ? 'if(confirm("'.$this->tpl->trans('?_confirm_delete').' '.strtolower($this->tpl->trans('recently_created_entry')).'?"))'  : '' );
		$this->tpl->assign( 'galeria',     $entity );
		$this->tpl->display( 'Galeria/edit.php' );
	}


	protected function exportToExcel( $data, $fileName = '' ){
		$rowO = $data[0];
		$contentHtml .= '<table border="1">';
		$contentHtml .= '<tr>
		<td height="72" width="255" bgcolor="#cccccc"><img src="'.ABS_HTTP_URL.'/ima/logo.gif"></td>
		<td colspan="'.(count($rowO) - 1).'"><h2>Order Report - '.date("M d Y").'</h2></td>
		</tr>';
		$contentHtml .= '<tr>';
		foreach($rowO as $key => $row){
			$key = str_replace("_",' ',$key);
			$key = ucwords($key);
			$contentHtml .= '<th align="center">'.$key.'</th>';
		}
		$contentHtml .= '</tr>';
		$i = 0;
		foreach($data as $row){
			$bgcolor = (++$i % 2 == 0) ? 'bgcolor="#cccccc"' : '';
			$contentHtml .= '<tr>';
			foreach($row as $rowElement){
				$contentHtml .= '<td '.$bgcolor.' align="right">'.$rowElement.'</td>';
			}
			$contentHtml .= '</tr>';
		}
		$contentHtml .= '</table>';
		$total_bytes = strlen($contentHtml);
		header("Content-type: application/vnd.ms-excel");
		header("Content-disposition: attachment; filename=".$fileName.".xls; size=" . $total_bytes);
		echo $contentHtml;
	}


	protected function _save(){
		if ( !empty( $_REQUEST ) && isset( $_REQUEST['entity'] ) ){
			$entity = $_REQUEST['entity'];
			$Galeria = new eCommerce_Entity_Galeria();
			// Save type
			try{
				/******************************************************/
				/******************************************************/
				$txt = (empty($entity['galeria_id']) ? '' : $entity['galeria_id']);$Galeria->setGaleriaId( strip_tags($txt) );
				$txt = (empty($entity['galerycategory_id']) ? '' : $entity['galerycategory_id']);$Galeria->setGalerycategoryId( strip_tags($txt) );
				$txt = (empty($entity['nombre']) ? '' : $entity['nombre']);$Galeria->setNombre( strip_tags($txt) );

				$txt = (empty($entity['descripcion']) ? '' : $entity['descripcion']);$Galeria->setDescripcion( strip_tags($txt) );
				$txt = (empty($entity['material_recomendado']) ? '' : $entity['material_recomendado']);$Galeria->setMaterialRecomendado( strip_tags($txt) );
				$txt = (empty($entity['formato_imagen']) ? '' : $entity['formato_imagen']);$Galeria->setFormatoImagen( strip_tags($txt) );

				$txt = (empty($entity['precio']) ? '' : $entity['precio']);$Galeria->setPrecio( strip_tags($txt) );
				$txt = (empty($entity['keywords']) ? '' : $entity['keywords']);$Galeria->setKeywords( strip_tags($txt) );
				$txt = (empty($entity['destacado']) ? '' : $entity['destacado']);$Galeria->setDestacado( strip_tags($txt) );
				/////////////////////////////////////////////////////////////////////////////////////////
				$GaleriaTmp = eCommerce_SDO_Galeria::LoadGaleria( $Galeria->getGaleriaId() );
				$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('array_images');
				$imageHandler->setArrImages( $GaleriaTmp->getArrayImages() );
				$imageHandler->setUploadFileDirectory('files/images/galeria/');
				$imageHandler->setUploadVersionDirectory('web/');	
				$imageHandler->setUploadFileDescription( $this->getParameter( $imageHandler->GetDescriptionField(),false,'Imagen de Galeria ' .$Galeria->getGaleriaId() ) );
				$imageHandler->setValidTypes( array('image') );
				$imageHandler->setAccessType( eCommerce_SDO_ImageHandler::GetValidAccessType('public') );
				$arrVersions = array();
				$arrVersions[] = array('version'=>'small',	'path'=>'imagenes/galeria/', 'width'=>256, 'height'=>206);
				$arrVersions[] = array('version'=>'medium', 'path'=>'imagenes/galeria/', 'width'=>512, 'height'=>412);
				$arrVersions[] = array('version'=>'large',  'path'=>'imagenes/galeria/', 'width'=>1024, 'height'=>824);
				$imageHandler->setArrVersions($arrVersions);
				$imageHandler->setMaximum( 1 );
				$arrImages = $imageHandler->proccessImages();
				$Galeria->setArrayImages( $arrImages );
				/////////////////////////////////////////////////////////////////////////////////////////
	  			/******************************************************/
	  			/*****************************************************/
				/*
				$GaleriaTmp = eCommerce_SDO_Galeria::LoadGaleria( $Galeria->getGaleriaId() );
				$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('GaleriaImages');
				$imageHandler->setArrImages( $GaleriaTmp->getArrayImages() );
				$imageHandler->setUploadFileDirectory('files/images/noticias/');
				$imageHandler->setUploadFileDescription('Imagen de Evento ' . $Galeria->getGaleriaId() );
				$imageHandler->setValidTypes( array('word','image') );
				$imageHandler->setAccessType( eCommerce_SDO_ImageHandler::GetValidAccessType('Galeria') );
				$Galeria->setArrayImages( implode(',',$GaleriaTmp->getArrayImages()) );
				$imageHandler->setMaximum( 1 );
				$arrImages = $imageHandler->proccessImages();
				$Galeria->setArrayImages( $arrImages );
				*/
				eCommerce_SDO_Galeria::SaverGaleria( $Galeria );
				$this->tpl->assign( 'strSuccess', $this->tpl->trans('the').' Galeria "' . $Galeria->getGaleriaId() . '" '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('saved')));
				$this->_list();
			}
			catch( eCommerce_SDO_Core_Validator_Exception $e){
				$this->tpl->assign( 'errors', new Validator_ErrorHandler( $this->tpl->trans("there_are_errors"), $e->getErrors() ) );
				$oldCmd = $this->getParameter( 'old_cmd', false, null );
				$this->edit( $Galeria,  $oldCmd );
			}
		}
		else{
			throw new Exception($this->tpl->trans('not_enough_information').'. '.$this->tpl->trans('try_again'));
		}
	}


	protected function _delete(){
		//$id = $this->getParameter();
		$id = $this->getParameter('id',false,0);
		$id2Str = $id;
		//---------------------------
		if( strpos($id,",") > -1 ){
			$idF = explode(",",$this->idFields);
			$ArrId = explode(",",$id);
			$id = array();
			$id2Str = "(";
			for($i=0; $i< count($idF); $i++){
				$id[ $idF[$i] ] = (empty($ArrId[$i])) ? 0 : $ArrId[$i];
				$id2Str .=($i>0) ? ',' : '';
				$id2Str .= $ArrId[$i] . "";
			}
			$id2Str .= ")";
		}
		//---------------------------
			try{
				$Galeria = eCommerce_SDO_Galeria::DeleteGaleria($id );
				$this->tpl->assign( 'strSuccess', $this->tpl->trans('the').' Galeria "' . $Galeria->getGaleriaId() . '" '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('deleted')));
			}
			catch(eCommerce_SDO_Core_Application_Exception $e) {
				//debug($e);
				$this->tpl->assign( 'strError', $e->getMessage() );
			}
		$this->_list();
	}
}
?>