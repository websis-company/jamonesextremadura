<?php
class eCommerce_FrontEnd_BO_Relacioncolor extends eCommerce_FrontEnd_BO {


public function getSDOSearch( $search, $extraConditions ){
	return eCommerce_SDO_Relacioncolor::SearchRelacioncolor( $search, $extraConditions );
}	


/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
	
	/**
	 * @var eCommerce_DAO_Event
	 */
	protected $search;
	protected $idFields;
	
	public function __construct(){
		parent::__construct();
		$this->idFields = 'relacioncolor_id';
	}

	public function execute(){
		//$accessManager = new eCommerce_Access_Relacioncolor( $this );
		//$accessManager->checkPermission(eCommerce_Access::RELACIONGALERIA_ALL_PERMISSIONS);
		//$this->checkPermission();
		
		$cmd = $this->getCommand();		
		switch( $cmd ){
			case 'remove':
				//$accessManager->checkPermission(eCommerce_Access::RELACIONGALERIA_DELETE);
				$this->_remove();
				break;
			case 'add':
				//$accessManager->checkPermission(eCommerce_Access::RELACIONGALERIA_ADD);
				$this->_add();
				break;
			case 'listIn':
			case 'listOut':
			default:
				//$accessManager->checkPermission(eCommerce_Access::RELACIONGALERIA_LIST);
				$this->_list($cmd);
				break;
		}
	}

	protected function _list($cmd = 'listIn'){
		$table_id 	= $this->getParameter('table_id');
		$tipo		= $this->getParameter('tipo', false, NULL);
				
		$versionname = eCommerce_SDO_Core_Application_Version::LoadById($table_id)->getName();
	
		$this->search = new eCommerce_Entity_Search(
			$this->getParameter( 'q', false, NULL ),
			$this->getParameter( 'o', false, '' ), //fecha DESC
			$this->getParameter( 'p', true, 1 ),
			$this->getParameter( 'k', true, -1 )
		);
		
		$extraConditions = array();
		switch ($cmd){
			case 'listOut':							
				$extraConditions[] 	= array( "SQL" => "tabla_id != $table_id OR tabla_id IS NULL");				
				$joins[] 			= array('table' => 'relacioncolor', 'on' => "color.color_id = relacioncolor.color_id AND tabla_id = $table_id", 'join_type' => 'LEFT');
												
				$result 			= eCommerce_SDO_Color::SearchColor($this->search, $extraConditions, $joins);	
						
				break;
			case 'listIn':
			default:				
				$extraConditions[] = array( "SQL" => "tabla_id = $table_id");
				$extraConditions[] = array( "SQL" => "tipo = '$tipo'");							
				$result = $this->getSDOSearch( $this->search, $extraConditions );
				
				break;
		}
		
		$addLinkObj	= $cmd == 'listOut' ? '' : "?table_id=$table_id&tipo=$tipo&cmd=listOut";
		
		switch ($tipo){
			case 'producto':
				$tituloObj		= $versionname;
				$returnLinkObj	=  $cmd == 'listOut' ? "?table_id=$table_id&tipo=$tipo" : 'Version.php';				
				break;
			case 'evento':
				$tituloObj	= eCommerce_SDO_Evento::LoadEvento($table_id)->getTitulo();
				$returnLinkObj	=  $cmd == 'listOut' ? "?table_id=$table_id&tipo=$tipo" : 'Evento.php';
				break;
		}
		
		$exportToExcel = $this->getParameter( 'toExcel' );
		if ( $exportToExcel ){
		
			$regiters = $result->getResults();
			$data = array();
			foreach($regiters as $regiter){
				$data[] = get_object_vars( $regiter );
			}
			$this->exportToExcel( $data );

		}
		else {			
			$viewConfig = array();			
			$viewConfig['name'] = 'Caracteristica';
			$viewConfig['title'] = $tituloObj . ' | Caracteristicas ' . ($cmd == 'listOut' ? ' sin relaci&oacute;n' : 'relacionados');
			$viewConfig['id'] = $cmd == 'listOut' ? 'color_id' : $this->idFields;
			
			//---------------------------
		if(strpos($viewConfig['id'],",") > -1){
				$ids = explode(",",$viewConfig['id']);
				$id = $ids[0];
				unset($ids[0]);
				$viewConfig['id'] = '{$'.$id.'}';
				foreach($ids as $id){
					$viewConfig['id'] .= ',{$'.$id.'}';
				}
			}else{
				$viewConfig['id'] = '{$'.$viewConfig['id'].'}';
			}
			
			//---------------------------
			
			$viewConfig["hiddenColums"]= array('relacioncolor_id','relacioncolor_id','relaciongaleria_id','tipo','imagen_principal','array_images','nombre_en','nombre_fr');
			if($cmd == 'listOut'){
				$viewConfig["hiddenColums"][] = 'color_id';
				$viewConfig["columNamesOverride"]["nombre"] = "Productos";
			}
			else$viewConfig["columNamesOverride"]["color_id"] = "Caracteristica";
			
			$viewConfig["columNamesOverride"]["tabla_id"] = "Producto";
			
			$this->tpl->assign( 'viewConfig', $viewConfig );
			$this->tpl->assign( 'options', $result );
			$this->tpl->assign( 'returnLinkObj', $returnLinkObj );
			$this->tpl->assign( 'addLinkObj', $addLinkObj );
			$this->tpl->assign( 'table_id', $table_id );
			$this->tpl->assign( 'tipo', $tipo );
			$this->tpl->assign( 'cmd2', $cmd == 'listOut' ? 'add' : 'remove' );
			$this->tpl->assign( 'icon', $cmd == 'listOut' ? 'apply.png' : 'error.png' );
			$this->tpl->assign( 'lblAction', $cmd == 'listOut' ? 'Agregar' : 'Remover' );
			
			$this->tpl->display(  "Relacioncolor/list.php" );
		}
	}
	
	protected function _add(){
		try{
			$id 		= $this->getParameter();
			$tipo		= $this->getParameter('tipo', false, NULL);
			$table_id	= $this->getParameter('table_id');
			
			$extraConditions[] = array("SQL" => "color_id = $id");
			$extraConditions[] = array("SQL" => "tabla_id = $table_id");
			$extraConditions[] = array("SQL" => "tipo = '$tipo'");
			
			$relacionTMP = current(eCommerce_SDO_Relacioncolor::SearchRelacioncolor(new eCommerce_Entity_Search('','',1,1),$extraConditions)->getResults());
			
			if(!$relacionTMP){
				
				$relacion = new eCommerce_Entity_Relacioncolor();
				$relacion->setColorId($id);
				$relacion->setTablaId($table_id);
				$relacion->setTipo($tipo);
				eCommerce_SDO_Relacioncolor::SaverRelacioncolor($relacion);
				$this->tpl->assign( 'strSuccess', 'Se relacion&oacute; una caracteristica exitosamente' );
			}else{
				$this->tpl->assign( 'strError', 'La caracteristica fue relacionado anteriormente, no necesita volverla a relacionar' );
			}
			
			
			
			$this->_list('listOut');
		}catch( eCommerce_SDO_Core_Validator_Exception $e){
			$this->tpl->assign( 'strError', 'Intente Nuevamente' );
			$this->_list('listOut');
		}
	}
	
	protected function _remove(){
		try{
			$id 		= $this->getParameter();
			
			$relacionTMP = eCommerce_SDO_Relacioncolor::LoadRelacioncolor($id);
			$idTMP = $relacionTMP->getRelacioncolorId();
			if($idTMP == $id){
				eCommerce_SDO_Relacioncolor::DeleteRelacioncolor($id);
				$this->tpl->assign( 'strSuccess', 'La caracteristica ya no est&aacute; relacionada' );
			}else{
				$this->tpl->assign( 'strError', 'La relaci&oacute;n no existe' );
			}
			
			$this->_list();
		}catch( eCommerce_SDO_Core_Validator_Exception $e){
			$this->tpl->assign( 'strError', 'Intente Nuevamente' );
			$this->_list();
		}
	}
	

}
?>