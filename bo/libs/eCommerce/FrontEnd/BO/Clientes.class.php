<?php
class eCommerce_FrontEnd_BO_Clientes extends eCommerce_FrontEnd_BO {
public function getSDOSearch( $search, $extraConditions ){
	return eCommerce_SDO_Clientes::SearchClientes( $search, $extraConditions );
}	
/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
	/**
	 * @var eCommerce_DAO_Event
	 */
	protected $search;
	protected $idFields;
	public function __construct(){
		parent::__construct();
		$this->idFields = 'clientes_id';
	}
	public function execute(){
		$cmd = $this->getCommand();
		switch( $cmd ){
			case 'delete':
				$this->_delete();
				break;
			case 'add':
			case 'edit':
				$this->_edit();
				break;
			case 'save':
				$this->_save();
				break;
			case 'list':
			default:
				$this->_list();
				break;
		}
	}
	protected function _list(){
		$this->search = new eCommerce_Entity_Search(
			$this->getParameter( 'q', false, NULL ),
			$this->getParameter( 'o', false, '' ), //fecha DESC
			$this->getParameter( 'p', true, 1 ),
			$this->getParameter( 'k', true, 10 )
		);
		$extraConditions = array();
		//$extraConditions[] = array( "columName"=>"type","value"=>'',"isInteger"=>false);
		$result = $this->getSDOSearch( $this->search, $extraConditions ); 
		$exportToExcel = $this->getParameter( 'toExcel' );
		if ( $exportToExcel ){
			$regiters = $result->getResults();
			$data = array();
			foreach($regiters as $regiter){
				$data[] = get_object_vars( $regiter );
			}
			$this->exportToExcel( $data );
		}
		else {
			$viewConfig = array();
			$viewConfig['name'] = 'Clientes';
			$viewConfig['title'] = 'Clientes';
			$viewConfig['id'] = $this->idFields;
			//---------------------------
			if(strpos($viewConfig['id'],",") > -1){
				$ids = explode(",",$viewConfig['id']);
				$id = $ids[0];
				unset($ids[0]);
				$viewConfig['id'] = '{$'.$id.'}';
				foreach($ids as $id){
					$viewConfig['id'] .= ',{$'.$id.'}';
				}
			}else{
				$viewConfig['id'] = '{$'.$viewConfig['id'].'}';
			}
			//---------------------------
			$viewConfig["hiddenColums"]= array('clientes_id');
			$viewConfig["columNamesOverride"]["image_id"] = 'Imagen';

			$this->tpl->assign( 'viewConfig', $viewConfig );
			$this->tpl->assign( 'options', $result );
			$this->tpl->display(  "Clientes/list.php" );
		}
	}


	protected function _add(){
		try{
			$Clientes = new eCommerce_Entity_Clientes();
			$Clientes = eCommerce_SDO_Clientes::SaverClientes( $Clientes );
			$this->_edit( $Clientes->getClientesId() );
		}catch( eCommerce_SDO_Core_Validator_Exception $e){
			$this->tpl->assign( 'strError', $this->tpl->trans('try_again') );
			$this->_list();
		}
	}


	protected function _edit($id = null){
		//$id = $this->getParameter();
		$id = empty($id ) ? $this->getParameter('id',false,0) : $id;
		//---------------------------
		if( strpos($id,",") > -1 ){
			$idF = explode(",",$this->idFields);
			$ArrId = explode(",",$id);
			$id = array();
			for($i=0; $i< count($idF); $i++){
				$id[ $idF[$i] ] = (empty($ArrId[$i])) ? 0 : $ArrId[$i];
			}
		}
		//---------------------------
		$entity = eCommerce_SDO_Clientes::LoadClientes( $id );
		$this->tpl->assign( 'errors', new Validator_ErrorHandler() );
		$this->edit( $entity, $this->getCommand() );
	}


	public function edit( $entity, $cmd ){
		$this->tpl->assign( 'object',     $entity );
		$viewConfig = array();
		$form = array();
		$form['name'] = 'ClientesForm';
		$form['elements'] = array();
		/***********************************************************************************************************/		
		$form['elements'][] = array( 'title' => 'ClientesId:', 'type'=>'hidden', 'options'=>array(), 'name'=>'entity[clientes_id]', 'id'=>'clientes_id', 'value'=> $entity->getClientesId() );
		$form['elements'][] = array( 'title' => 'Nombre Cliente:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[nombre]', 'id'=>'nombre', 'value'=> $entity->getNombre() );
		$form['elements'][] = array( 'title' => 'Comentario:', 'type'=>'textarea', 'options'=>array(), 'name'=>'entity[comentario]', 'id'=>'comentario', 'value'=> $entity->getComentario() );
		$form['elements'][] = array( 'title' => 'Imagen: <br/> <strong>400x400px</strong>', 'type'=>'array_files', 'options'=>array(), 'name'=>'entity[image_id]', 'id'=>'image_id', 'value'=> $entity->getImageId(),'max_files'=>1 );
		/***********************************************************************************************************/
		//$viewConfig['id'] = 'noticia_evento_id';
		$viewConfig['form'] = $form;
		$viewConfig['title'] = ( $cmd =='add' ? $this->tpl->trans('add') : $this->tpl->trans('edit') ) . ' Clientes';
		$this->tpl->assign( 'form', $form );	
		$this->tpl->assign( 'viewConfig', $viewConfig );
		//$this->tpl->assign( 'ArrayImages', $entity->getArrayImages() );
		$this->tpl->assign( 'cancelParams', $cmd == 'add' ? '?cmd=delete&id=' . $entity->getClientesId() . '' : '' );
		$this->tpl->assign( 'cancelConfirm', $cmd == 'add' ? 'if(confirm("'.$this->tpl->trans('?_confirm_delete').' '.strtolower($this->tpl->trans('recently_created_entry')).'?"))'  : '' );
			$this->tpl->display( 'Clientes/edit.php' );
	}

	protected function exportToExcel( $data, $fileName = '' ){
		$rowO = $data[0];
		$contentHtml .= '<table border="1">';
		$contentHtml .= '<tr>
		<td height="72" width="255" bgcolor="#cccccc"><img src="'.ABS_HTTP_URL.'/ima/logo.gif"></td>
		<td colspan="'.(count($rowO) - 1).'"><h2>Order Report - '.date("M d Y").'</h2></td>
		</tr>';
		$contentHtml .= '<tr>';
		foreach($rowO as $key => $row){
			$key = str_replace("_",' ',$key);
			$key = ucwords($key);
			$contentHtml .= '<th align="center">'.$key.'</th>';
		}
		$contentHtml .= '</tr>';
		$i = 0;
		foreach($data as $row){
			$bgcolor = (++$i % 2 == 0) ? 'bgcolor="#cccccc"' : '';
			$contentHtml .= '<tr>';
			foreach($row as $rowElement){
				$contentHtml .= '<td '.$bgcolor.' align="right">'.$rowElement.'</td>';
			}
			$contentHtml .= '</tr>';
		}
		$contentHtml .= '</table>';
		$total_bytes = strlen($contentHtml);
		header("Content-type: application/vnd.ms-excel");
		header("Content-disposition: attachment; filename=".$fileName.".xls; size=" . $total_bytes);
		echo $contentHtml;
	}
	protected function _save(){
		if ( !empty( $_REQUEST ) && isset( $_REQUEST['entity'] ) ){
			$entity = $_REQUEST['entity'];
			$Clientes = new eCommerce_Entity_Clientes();
			// Save type
			try{
			/******************************************************/
			/******************************************************/
			$txt = (empty($entity['clientes_id']) ? '' : $entity['clientes_id']);$Clientes->setClientesId( strip_tags($txt) );
			$txt = (empty($entity['nombre']) ? '' : $entity['nombre']);$Clientes->setNombre( strip_tags($txt) );
			$txt = (empty($entity['comentario']) ? '' : $entity['comentario']);$Clientes->setComentario( strip_tags($txt) );


			/////////////////////////////////////////////////////////////////////////////////////////
			$ClientesTmp = eCommerce_SDO_Clientes::LoadClientes( $Clientes->getClientesId() );
			$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('image_id');
			$imageHandler->setArrImages( $ClientesTmp->getImageId() );
			$imageHandler->setUploadFileDirectory('files/images/testimoniales/');
			$imageHandler->setUploadVersionDirectory('web/');	
			$imageHandler->setUploadFileDescription( $this->getParameter( $imageHandler->GetDescriptionField(),false,'Imagen de Testimoniales ' .$Clientes->getClientesId() ) );
			$imageHandler->setValidTypes( array('image') );
			$imageHandler->setAccessType( eCommerce_SDO_ImageHandler::GetValidAccessType('public') );
			$arrVersions = array();
			$arrVersions[] = array('version'=>'large',  'path'=>'imagenes/testimoniales/', 'width'=>400, 'height'=>400);
			$imageHandler->setArrVersions($arrVersions);
			$imageHandler->setMaximum( 1 );
			$arrImages = $imageHandler->proccessImages();
			$Clientes->setImageId( $arrImages );
			/////////////////////////////////////////////////////////////////////////////////////////

  			/******************************************************/
			/*****************************************************/
			/*
			$ClientesTmp = eCommerce_SDO_Clientes::LoadClientes( $Clientes->getClientesId() );
			$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('ClientesImages');
			$imageHandler->setArrImages( $ClientesTmp->getArrayImages() );
			$imageHandler->setUploadFileDirectory('files/images/noticias/');
			$imageHandler->setUploadFileDescription('Imagen de Evento ' . $Clientes->getClientesId() );
			$imageHandler->setValidTypes( array('word','image') );
			$imageHandler->setAccessType( eCommerce_SDO_ImageHandler::GetValidAccessType('public') );
			$Clientes->setArrayImages( implode(',',$ClientesTmp->getArrayImages()) );
			$imageHandler->setMaximum( 1 );
			$arrImages = $imageHandler->proccessImages();
			$Clientes->setArrayImages( $arrImages );
			*/
				eCommerce_SDO_Clientes::SaverClientes( $Clientes );
				$this->tpl->assign( 'strSuccess', $this->tpl->trans('the').' Clientes "' . $Clientes->getClientesId() . '" '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('saved')));
				$this->_list();
			}
			catch( eCommerce_SDO_Core_Validator_Exception $e){
				$this->tpl->assign( 'errors', new Validator_ErrorHandler( $this->tpl->trans("there_are_errors"), $e->getErrors() ) );
				$oldCmd = $this->getParameter( 'old_cmd', false, null );
				$this->edit( $Clientes,  $oldCmd );
			}
		}
		else{
			throw new Exception($this->tpl->trans('not_enough_information').'. '.$this->tpl->trans('try_again'));
		}
	}
	protected function _delete(){
		//$id = $this->getParameter();
		$id = $this->getParameter('id',false,0);
		$id2Str = $id;
		//---------------------------
		if( strpos($id,",") > -1 ){
			$idF = explode(",",$this->idFields);
			$ArrId = explode(",",$id);
			$id = array();
			$id2Str = "(";
			for($i=0; $i< count($idF); $i++){
				$id[ $idF[$i] ] = (empty($ArrId[$i])) ? 0 : $ArrId[$i];
				$id2Str .=($i>0) ? ',' : '';
				$id2Str .= $ArrId[$i] . "";
			}
			$id2Str .= ")";
		}
		//---------------------------
			try{
				$Clientes = eCommerce_SDO_Clientes::DeleteClientes($id );
				$this->tpl->assign( 'strSuccess', $this->tpl->trans('the').' Clientes "' . $Clientes->getClientesId() . '" '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('deleted')));
			}
			catch(eCommerce_SDO_Core_Application_Exception $e) {
				//debug($e);
				$this->tpl->assign( 'strError', $e->getMessage() );
			}
		$this->_list();
	}
}
?>