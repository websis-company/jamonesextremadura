<?php

class eCommerce_FrontEnd_BO_Category extends eCommerce_FrontEnd_BO {

	/**
	 * @var eCommerce_DAO_Event
	 */
	protected $search;
	protected $multipleLevels;
	public function __construct(){
		parent::__construct();
		$this->search = new eCommerce_Entity_Search();
		$this->multipleLevels = '<option value="0"></option>';
	}

	public function execute(){
		eCommerce_FrontEnd_BO_Authentification::verifyAuthentification();
		$this->checkPermission();
		$cmd = $this->getCommand();
		switch( $cmd ){
			case 'delete':
				$this->_delete();
				break;
			case 'add':
			case 'edit':
				$this->_edit();
				break;
			case 'save':
				$this->_save();
				break;

			case 'list':
			default:
				$this->_list();
				break;
		}
	}

	protected function _list(){
		$currentCategoryId = $this->getParameter( 'category_id', true, 0 ); 
		
		$this->search = new eCommerce_Entity_Search(
			$this->getParameter( 'q', false, NULL ),
			$this->getParameter( 'o', false, 'name' ),
			$this->getParameter( 'p', true, 1 ),
			$this->getParameter( 'k', true, -1 )
		);
		
		$currentCategory = eCommerce_SDO_Catalog::LoadCategory( $currentCategoryId );
		// Search current subcategories inside current category
		$result = eCommerce_SDO_Catalog::SearchCategoriesByParentCategory($this->search, $currentCategoryId );

		$categories = $result->getResults();
		$data = array();
		foreach($categories as $category){
			$data[] = get_object_vars($category);
		}

		$exportToExcel = $this->getParameter( 'toExcel' );
		if ( $exportToExcel ){
			$this->exportToExcel( $data );
		}
		else {
			$this->tpl->assignRef( 'data', $data );
			$this->tpl->assign( 'options', $this->search );
			$this->tpl->assign( 'category', $currentCategory );
			$this->tpl->display(  "category/list.php" );
		}
	}

	

	protected function getMultipleArray($parent, $sangria, $idParent){
		$search = new eCommerce_Entity_Search(null,null,1,-1);	// -1 = ALL
		$result = eCommerce_SDO_Catalog::SearchCategoriesByParentCategory($search, $parent);
		$categories = $result->getResults();

		$data = array();
		foreach($categories as $category){
			$data[] = get_object_vars($category);
		}

		foreach($data as $category){
			$sel = ($idParent == $category['category_id']) ? 'selected = "selected"' : '';
			$this->multipleLevels .= "<option value='$category[category_id]' $sel>$sangria$category[name] </option>";
			$this->getMultipleArray($category['category_id'], $sangria.'&nbsp;&nbsp;&nbsp;&nbsp;', $idParent);
		}
	}

	protected function _edit(){
		$id = $this->getParameter();

		$entity = eCommerce_SDO_Catalog::LoadCategory($id);
		if ( $entity->getParentCategoryId() == 0 ){
			$entity->setParentCategoryId( $this->getParameter( 'parent_category_id'), true, 0 );
		}
		$this->tpl->assign( 'errors', new Validator_ErrorHandler() );
		$this->edit( $entity, $this->getCommand() );

	}

	public function edit( eCommerce_Entity_Catalog_Category $entity, $cmd ){
		$this->getMultipleArray(0,'',$entity->getParentCategoryId());
		$ArrStatus = eCommerce_SDO_Catalog::GetArrCategoryStatus();
		
		$this->tpl->assign( 'ArrStatus', $ArrStatus );
		$this->tpl->assign( 'category',     $entity );
		$this->tpl->assign( 'treeCategories',    $this->multipleLevels );
		$this->tpl->assign( 'strSubtitles' , ( $cmd =='add' ? $this->tpl->trans('add') : $this->tpl->trans('edit') ) . ' '.$this->tpl->trans('category' ) );
		$this->tpl->assign( 'strCmd'       , $this->getCommand() );

		$this->tpl->display( 'category/edit.php' );
	}

	protected function exportToExcel( $data ){

		$xls = new ExcelWriter( 'Events_' . date('Y-m-d'), 'Products' );
		$xls->writeHeadersAndDataFromArray( $data );


	}

	protected function _save(){
		
		if ( !empty( $_REQUEST ) && isset( $_REQUEST['entity'] ) ){
			$entity = $_REQUEST['entity'];



  			$category = new eCommerce_Entity_Catalog_Category();

			try {
				$category->setCategoryId($entity['category_id']);
				$category->setName($entity['name']);
				$category->setDescription($entity['description']);
				$category->setParentCategoryId( $entity['parent_category_id'] );
				$orden = empty($entity['orden'])?0:$entity['orden'];
				$category->setOrden( $orden );
				$category->setStatus( $entity['status'] );

				if ( $category->getParentCategoryId() == 0 ){
					$category->setParentCategoryId( null );
				}


				/////////////////////////////////////////////////////////////////////////////////////////
				$CategoryTmp = eCommerce_SDO_Catalog::LoadCategory( $category->getCategoryId() );
				$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('image_id');
				$imageHandler->setArrImages( $CategoryTmp->getImageId() );
				$imageHandler->setUploadFileDirectory('files/images/categories/');
				$imageHandler->setUploadVersionDirectory('web/');	
				$imageHandler->setUploadFileDescription( $this->getParameter( $imageHandler->GetDescriptionField(),false,'Imagen de Categoría ' .$category->getCategoryId() ) );
				$imageHandler->setValidTypes( array('image') );
				$imageHandler->setAccessType( eCommerce_SDO_ImageHandler::GetValidAccessType('public') );
				$arrVersions = array();
				$arrVersions[] = array('version'=>'small',	'path'=>'imagenes/categories/', 'width'=>256, 'height'=>206);
				$arrVersions[] = array('version'=>'medium', 'path'=>'imagenes/categories/', 'width'=>512, 'height'=>412);
				$arrVersions[] = array('version'=>'large',  'path'=>'imagenes/categories/', 'width'=>1024, 'height'=>824);
				$imageHandler->setArrVersions($arrVersions);
				$imageHandler->setMaximum( 1 );
				$arrImages = $imageHandler->proccessImages();
				$category->setImageId( $arrImages );
				/////////////////////////////////////////////////////////////////////////////////////////

				/////////////////////////////////////////////////////////////////////////////////////////
				$CategoryTmp = eCommerce_SDO_Catalog::LoadCategory( $category->getCategoryId() );
				$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('banner_id');
				$imageHandler->setArrImages( $CategoryTmp->getBannerId() );
				$imageHandler->setUploadFileDirectory('files/images/categories/');
				$imageHandler->setUploadVersionDirectory('web/');	
				$imageHandler->setUploadFileDescription( $this->getParameter( $imageHandler->GetDescriptionField(),false,'Imagen de Banner ' .$category->getCategoryId() ) );
				$imageHandler->setValidTypes( array('image') );
				$imageHandler->setAccessType( eCommerce_SDO_ImageHandler::GetValidAccessType('public') );
				$arrVersions = array();
				$arrVersions[] = array('version'=>'large',  'path'=>'imagenes/categories/', 'width'=>1920, 'height'=>300);
				$imageHandler->setArrVersions($arrVersions);
				$imageHandler->setMaximum( 1 );
				$arrImages = $imageHandler->proccessImages();
				$category->setBannerId( $arrImages );
				/////////////////////////////////////////////////////////////////////////////////////////
				
				eCommerce_SDO_Catalog::SaveCategory($category);
				$this->tpl->assign( 'strSuccess', $this->tpl->trans('the(f)').' '.$this->tpl->trans('category').' "' . $category->getName() . '" '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('saved(f)')));
				$this->_list();

			} catch (eCommerce_SDO_Core_Validator_Exception $e) {
				$this->tpl->assign( 'errors', new Validator_ErrorHandler( $this->tpl->trans("there_are_errors"), $e->getErrors() ) );
				$oldCmd = $this->getParameter( 'old_cmd', false, null );
				$this->edit( $category,  $oldCmd );
			}

			

			//$category->setImageId(0);  //TODO: Images save
			
			

			// Save category
			/*try{
				if( !empty($entity["image_delete"])) {
					$category->setImageId('');
				} elseif( !empty($_FILES["image_file"]["tmp_name"]) ){
					$imageFile = new eCommerce_Entity_Util_FileUpload( "image_file" );
					$imageFile = eCommerce_SDO_Catalog::SaveCategoryImage( $imageFile );
					$category->setImageId( $imageFile->getId() );
				}
			}catch( eCommerce_SDO_Core_Validator_Exception $e){
				$this->tpl->assign( 'errors', new Validator_ErrorHandler( $this->tpl->trans("there_are_errors"), $e->getErrors() ) );
				$oldCmd = $this->getParameter( 'old_cmd', false, null );
				$this->edit( $category,  $oldCmd );
			}*/

		}else{
			throw new Exception($this->tpl->trans('not_enough_information').'. '.$this->tpl->trans('try_again'));
		}
	}

	protected function _delete(){
		$id = $this->getParameter();
		if ( !empty( $id ) ) {
			try{
				$category = eCommerce_SDO_Core_Application_Category::Delete($id );
				$this->tpl->assign( 'strSuccess', $this->tpl->trans('the(f)').' '.$this->tpl->trans('category').' "' . $category->getName() . '" '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('deleted(f)')));
			}catch(eCommerce_SDO_Core_Application_Exception $e) {
				//debug($e);
				$this->tpl->assign( 'strError', $e->getMessage() );
			}
		}
		$this->_list();
	}

}
?>