<?php

class eCommerce_FrontEnd_BO_Product extends eCommerce_FrontEnd_BO {
	
	public function __construct(){
		
		parent::__construct();	
	}
	
	public function execute($cmd2 = ''){
		eCommerce_FrontEnd_BO_Authentification::verifyAuthentification();
		//$this->checkPermission();				
		$cmd = empty($cmd2)?$this->getCommand():$cmd2;
		switch( $cmd ){
			case 'editpaquete':
			case 'addpaquete':
			case 'edit':
			case 'add':
				$this->_edit();
			break;
			case 'deletepaquete':
			case 'delete':
				$this->_delete($cmd);
			break;
			
			case 'save':
				$this->_save();
				break;
			case 'paquetes':
			case 'list':
			default:
				$this->_list($cmd);
			break;
			
		}
	}
	
	protected function _edit(){
		
		$id     = $this->getParameter();
		$cmd    = $this->getCommand();

		//$language='ESP';
		//with this configuration the system return the description, name 
		//in the defaul language
		$language = eCommerce_SDO_Core_Application_LanguageManager::DEFAULT_LANGUAGE;
		$entity = eCommerce_SDO_Catalog::LoadProduct( $id,  $language);
		
		$ArrObjProInLang = eCommerce_SDO_Catalog::GetAllLanguageOfProduct( $entity->getProductId() );
		
		$this->tpl->assign( 'ArrObjProInLang', $ArrObjProInLang);
		
		$this->tpl->assign( 'errors', new Validator_ErrorHandler() );
		
		$this->edit( $entity, $cmd );
	}
	
	protected function edit($entity, $cmd ){
		echo "entra";
		die();

		$this->tpl->assign( 'product',     $entity );
		$this->tpl->assign( 'strSubtitles' , ( $cmd =='add' ? $this->tpl->trans('add') : $this->tpl->trans('edit') ) . ' '.$this->tpl->trans('product') );
		$this->tpl->assign( 'strCmd', $cmd );
		
		$this->tpl->assign( 'ArrObjProInLang', empty($this->tpl->ArrObjProInLang) ? array() : $this->tpl->ArrObjProInLang );
		
		$ArrStatus = eCommerce_SDO_Catalog::GetArrProductsStatus();
		$ArrNovedad = eCommerce_SDO_Catalog::GetArrProductsNovedad();
		$ArrVendido = eCommerce_SDO_Catalog::GetArrProductsVendido();
		
		
		$ArrCurrences = eCommerce_SDO_CurrencyManager::GetCurrencies();
		$ArrBrands = eCommerce_SDO_Brand::GetAllBrand();
		
		$this->tpl->assign( 'ArrStatus', $ArrStatus );
		$this->tpl->assign( 'ArrNovedad', $ArrNovedad );
		$this->tpl->assign( 'ArrVendido', $ArrVendido );
		
		$this->tpl->assign( 'ArrCurrences', $ArrCurrences );
		$this->tpl->assign( 'ArrBrands', $ArrBrands );
		
		$txtProducto = 'Productos Relacionados';
		$tipoProducto = 'producto';
		$this->tpl->assign( 'txtProducto', $txtProducto );
		$this->tpl->assign( 'tipoProducto', $tipoProducto );
		$this->tpl->assign( 'cmdProducto', $cmd );
		
		$this->tpl->display( 'product/edit.php' );
		
	}
	
	protected function _list($cmd = ''){
		
		$search = new eCommerce_Entity_Search(
			$this->getParameter( 'q', false, NULL ),
			$this->getParameter( 'o', false, 'product_id ASC' ),
			$this->getParameter( 'p', true, 1 ),
			$this->getParameter( 'k', true, -1 )
		);		
		
		$extraConditions[] = array( "SQL"=>"p.tipo = 'producto'");
		
		
		$result = eCommerce_SDO_Catalog::SearchProducts( $search , null, true, $extraConditions);
		
		$exportToExcel = $this->getParameter( "toExcel" );
		if( $exportToExcel ){
			$products = $result->getResults();
			$data = array();
			foreach( $products as $product ){
				$data[] = get_object_vars( $product );
			}
			$this->exportToExcel( $data );
		}
		else{
			$this->tpl->assign( 'tipo', $cmd );
			$this->tpl->assign( 'options', $result );
			$this->tpl->display("product/list.php");
		}
	}
	
	protected function exportToExcel( $data ){
		$xls = new ExcelWriter( 'Products_' . date('Y-m-d'), 'Products' );
		$xls->writeHeadersAndDataFromArray( $data );
	}
	
	protected function _delete($cmd = ''){
		$id = $this->getParameter();
		$redirect = ($cmd == 'deletepaquete')?'paquetes':'';
		if ( !empty( $id ) ) {
			try{
				$product = eCommerce_SDO_Catalog::DeleteProduct( $id );

				$this->tpl->assign( 'strSuccess', $this->tpl->trans('the').' '.$this->tpl->trans('product').' "' . $product->getName() . '" '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('deleted')));
								
			}catch(eCommerce_SDO_Core_Application_Exception $e) {
				$this->tpl->assign( 'strError', $e->getMessage() );
			}
		}
		$this->_list($redirect);
	}
	
	protected function _save(){
		if ( !empty( $_REQUEST ) && isset( $_REQUEST['entity'] ) ){
			$cmdProducto = $_REQUEST['cmdProducto'];
			$entity = $_REQUEST['entity'];
	  		$product = new eCommerce_Entity_Catalog_Product();
			$txt = (empty($entity['id']) ? NULL: $entity['id']);$product->setProductId( strip_tags($txt) );
	  		//$product->setProductId( $entity['id'] );
	  		$product->setName( $entity['name'] );
	  		$product->setSku( $entity['sku'] );
	  		$product->setDescriptionShort( utf8_decode($entity['description_short']) );
	  		$product->setDescriptionLong( $entity['description_long'] );
			$product->setEspecificaciones( $entity['especificaciones'] );
			//$product->setKeywords( $entity['keywords'] );
	  		$array = array('[','"',']');
	  		$Keywords = str_replace($array,'',$_REQUEST['entity']['keywords']) ;
	  		$product->setKeywords( strip_tags($Keywords) );
	  		$product->setStatus( $entity['status'] );
	  		$product->setNovedad( $entity['novedad'] );
	  		$product->setPrice( $entity['price'] );
	  		//$product->setCurrency( $entity['currency'] );
			$product->setCurrency( 'USD' );
	  		$product->setDiscount( $entity['discount'] );
	  		$product->setWeight( $entity['weight'] );
	  		$product->setVolume( $entity['volume'] );
			$product->setBrandId( $entity['brand_id'] );
	  		$product->setDimensions( $entity['dimensions'] );
	  		$product->setFinished( $entity['finished'] );
	  		$product->setMaterials( $entity['materials'] );
	  		$product->setColors( $entity['colors'] );
			$product->setVideo( $entity['video'] );
	  		$product->setMasVendido( $entity['mas_vendido'] );
			$product->setFecha( $entity['fecha'] );
			$product->setOrden( $entity['orden'] );
	  		//$product->setImageId( $entity['image_id']>0 ? $entity['image_id'] : null );
	  		//we load the other languages
			$ArrLanguages = eCommerce_SDO_LanguageManager::GetLanguages();
			$ArrObjProInLang = array();
			foreach( $ArrLanguages as $language ){
					$language = $language["id"];
					if ( isset( $_REQUEST['entityLang_'. $language ] ) ){
						$entity = $_REQUEST[ 'entityLang_'. $language ];
						$productLang = new eCommerce_Entity_Catalog_ProductInDifferentLanguage();
						$productLang->setProductId( $product->getProductId() );
						$productLang->setLanguageId( $language );
						$productLang->setName( $entity["name"] );
						$productLang->setDescriptionShort( $entity["description_short"]);
						$productLang->setDescriptionLong( $entity["description_long"]);
						$productLang->setDimensions( $entity["dimensions"]);
						$productLang->setFinished( $entity["finished"]);
						$productLang->setMaterials( $entity["materials"]);
						$productLang->setColors( $entity["colors"]);
						$ArrObjProInLang[ $language ] = $productLang;
						//$productLang = eCommerce_SDO_Core_Application_ProductInDifferentLanguage::Save($productLang);	
					}
				}
			$this->tpl->assign( 'ArrObjProInLang', $ArrObjProInLang);
			// Save Product with his languages
			try{
				$ProductTmp = eCommerce_SDO_Catalog::LoadProduct( $product->getProductId() );
				$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('image_id');
				$imageHandler->setArrImages( $ProductTmp->getImageId() );
				$imageHandler->setUploadFileDirectory('files/images/products/');
				$imageHandler->setUploadVersionDirectory('web/');
				$imageHandler->setUploadFileDescription( $this->getParameter( $imageHandler->GetDescriptionField(),false,'Imagen de Producto ' .$product->getProductId() ) );
				$imageHandler->setValidTypes( array('image') );
				$imageHandler->setAccessType( eCommerce_SDO_ImageHandler::GetValidAccessType('public') );
				$arrVersions = array();
				$arrVersions[] = array('version'=>'small',	'path'=>'images/products/',	'width'=>256, 'height'=>206);
				$arrVersions[] = array('version'=>'medium',	'path'=>'images/products/',	'width'=>512, 'height'=>412);
				$arrVersions[] = array('version'=>'large',	'path'=>'images/products/',	'width'=>1024, 'height'=>824);
				$imageHandler->setArrVersions($arrVersions);
				$imageHandler->setMaximum( 1 );
				$arrImages = $imageHandler->proccessImages();
				$product->setImageId( $arrImages );

				
				/**MULTIPLE IMAGES ***************************************************************/
				/*$ProductTmp = eCommerce_SDO_Catalog::LoadProduct( $product->getProductId() );
				$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('ArrayImages');
				$imageHandler->setArrImages( $ProductTmp->getArrayImages() );
				$imageHandler->setUploadFileDirectory('files/images/products/');
				$imageHandler->setUploadVersionDirectory('web/');
				$imageHandler->setUploadFileDescription( $this->getParameter( $imageHandler->GetDescriptionField(),false,'Imagen de Producto ' .$product->getProductId() ) );
				$imageHandler->setValidTypes( array('image') );
				$imageHandler->setAccessType( eCommerce_SDO_ImageHandler::GetValidAccessType('public') );
				$arrVersions = array();
				$arrVersions[] = array('version'=>'small',	'path'=>'images/products/',	'width'=>220, 'height'=>220);
				$arrVersions[] = array('version'=>'medium',	'path'=>'images/products/',	'width'=>440, 'height'=>440);
				$arrVersions[] = array('version'=>'large',	'path'=>'images/products/',	'width'=>880, 'height'=>880);				
				$imageHandler->setArrVersions($arrVersions);
				$imageHandler->setMaximum( 4 );
				$arrImages = $imageHandler->proccessImages();
				$product->setArrayImages( $arrImages );*/
				/**MULTIPLE IMAGES ***************************************************************/
				/*
				$productTmp = eCommerce_SDO_Catalog::LoadProduct( $product->getProductId() );
				$imageHandler = new eCommerce_SDO_Core_Application_ImageHandler('ProductImages');
				$imageHandler->setArrImages( $productTmp->getArrayImages() );
				$arrImages = $imageHandler->proccessImages();
				$product->setArrayImages( $arrImages );
				*/
				$cmdProducto = $_REQUEST['cmdProducto'];
				$tipo = 'producto';
				$product->setTipo($tipo);
				$ProductosRelacionados = implode(',',$_REQUEST['productos']);
				$product->setProductosRelacionados($ProductosRelacionados);
				$ProductosDetalles = implode(',',$_REQUEST['productos_detalles']);
				$product->setProductosDetalles($ProductosDetalles);
				$product = eCommerce_SDO_Catalog::SaveProduct( $product );
				/* Relacionar Categorias y SubCategorias*/
				$productId = $product->getProductId();
				$Categorias = eCommerce_SDO_Core_Application_ProductCategory::GetCategoriesByProduct($productId);
				foreach($Categorias as $category){
					$categoriasActuales[] = $category->getCategoryId();
				}
				$categoriasSeleccionadas = $_REQUEST['chkSub'];
				$padresIds = $_REQUEST['padresId'];
				if(!empty($categoriasActuales)){
					$categoriasInsertar = array_diff($categoriasSeleccionadas,$categoriasActuales);
					$categoriasBorrar = array_diff($categoriasActuales,$categoriasSeleccionadas);
				}else{
					$categoriasInsertar = $categoriasSeleccionadas;
				}
				if(empty($categoriasInsertar) && empty($categoriasBorrar))
					$categoriasBorrar = $categoriasActuales;
				foreach($categoriasBorrar as $categoria){
					$existe_padre = in_array($categoria,$padresIds);
					if(!$existe_padre)
						eCommerce_SDO_Catalog::DeleteProductFromCategory( $productId, $categoria);
				}
				if(!empty($categoriasInsertar)){
					foreach($categoriasInsertar as $categoria){
						eCommerce_SDO_Catalog::AddProductToCategory( $productId, $categoria);
					}
				}
				if(!empty($padresIds)){
					foreach($padresIds as $categoria){
						$existe_padre = in_array($categoria,$categoriasActuales);
						if(!$existe_padre)
							eCommerce_SDO_Catalog::AddProductToCategory( $productId, $categoria);
					}
				}
				/* Relacionar Categorias y SubCategorias*/
				$this->tpl->assign( 'strSuccess', $this->tpl->trans('the').' '.$this->tpl->trans('product').' "' . $product->getName() . '" '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('saved')));
				//we load the other languages
				foreach( $ArrObjProInLang as $objProLanguage ){
						$productId = $objProLanguage->getProductId();
						if( empty( $productId ) ){
							$objProLanguage->setProductId( $product->getProductId() );
						}
						$productLang = eCommerce_SDO_Core_Application_ProductInDifferentLanguage::Save($objProLanguage);	
				}
				$redirect = ($cmdProducto == 'addpaquete' || $cmdProducto == 'editpaquete')?'paquetes':'';
				$this->_list($redirect);
			}catch( eCommerce_SDO_Core_Validator_Exception $e){
				//debug($e);
				$this->tpl->assign( 'errors', new Validator_ErrorHandler( $this->tpl->trans("there_are_errors"), $e->getErrors() ) );
				$oldCmd = $this->getParameter( 'old_cmd', false, null );
				$this->edit( $product,  $oldCmd );
			}
		}else{
			throw new Exception($this->tpl->trans('not_enough_information').'. '.$this->tpl->trans('try_again'));
		}
	}
}
?>