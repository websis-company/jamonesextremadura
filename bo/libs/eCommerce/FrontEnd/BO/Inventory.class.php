<?php
class eCommerce_FrontEnd_BO_Inventory extends eCommerce_FrontEnd_BO {


public function getSDOSearch( $search, $extraConditions ){
	return eCommerce_SDO_Inventory::SearchInventory( $search, $extraConditions );
}	


/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
	
	/**
	 * @var eCommerce_DAO_Event
	 */
	protected $search;

	public function __construct(){
		parent::__construct();
		
	}

	public function execute(){
		eCommerce_FrontEnd_BO_Authentification::verifyAuthentification();
		
		$this->checkPermission();
		
		$cmd = $this->getCommand();
		switch( $cmd ){
			case 'delete':
				$this->_delete();
				break;
			case 'add':
			case 'edit':
				$this->_edit();
				break;
			case 'save':
				$this->_save();
				break;
			case 'list':
			default:
				$this->_list();
				break;
		}
	}

	protected function _list(){
		$productId = empty($this->tpl->idProduct) ? $this->getParameter('idProduct') : $this->tpl->idProduct;
		
		$this->search = new eCommerce_Entity_Search(
			$this->getParameter( 'q', false, NULL ),
			$this->getParameter( 'o', false, '' ), //fecha DESC
			$this->getParameter( 'p', true, 1 ),
			$this->getParameter( 'k', true, 10 )
		);
		$extraConditions = array();
		
		if( !empty($productId) ){
			$extraConditions[] = array( "columName"=>"product_id","value"=>$productId,"isInteger"=>true);
		}
		$result = $this->getSDOSearch( $this->search, $extraConditions ); 
		
		
		$exportToExcel = $this->getParameter( 'toExcel' );
		if ( $exportToExcel ){
		
			$users = $result->getResults();
			$data = array();
			foreach($users as $user){
				$data[] = get_object_vars( $user );
			}
			$this->exportToExcel( $data );

		}
		else {
			$viewConfig = array();
			$viewConfig['name'] = $this->tpl->trans('inventory');
			$viewConfig['title'] = '';
			$viewConfig['id'] = 'inventory_id';
			
			$viewConfig["hiddenColums"]= array('array_images','product_id');
			$viewConfig['hiddenFields'] = array("idProduct"=>$productId);
			$viewConfig["columNamesOverride"]= array("inventory_id"=>"ID", "n_products"=>$this->trans('number').' '.$this->trans('of').' '.$this->trans('product').'s', 'attribute'=>$this->trans('attribute'));
			
			$this->tpl->assign( 'viewConfig', $viewConfig );
			
			$this->tpl->assign( 'options', $result );
			
			$this->tpl->assign('idProduct', $productId);
			
			$this->tpl->display(  "inventory/list.php" );
		}
	}
		
	protected function _edit(){
		$id = $this->getParameter();

		$entity = eCommerce_SDO_Inventory::LoadInventory( $id );
		
		$pId = $entity->getProductId();
		if( empty($pId) ){
			$idProduct = $this->getParameter('idProduct');
			$entity->setProductId($idProduct);
		}
		
		$this->tpl->assign( 'errors', new Validator_ErrorHandler() );
		
		$this->edit( $entity, $this->getCommand() );

	}

	public function edit( $entity, $cmd ){

		$this->tpl->assign( 'object',  $entity );
		
		$viewConfig = array();
		$form = array();

		
		$form['name'] = 'InventoryForm';
		$form['elements'] = array();
		
		
	$idProduct = $entity->getProductId();
	$arrAttributes = eCommerce_SDO_Inventory::LoadByProductGroupByAttribute($idProduct, 'attribute');

/***********************************************************************************************************/
		
$form['elements'][] = array( 'title' => 'InventoryId', 'type'=>'hidden', 'options'=>array(), 'name'=>'entity[inventory_id]', 'id'=>'inventory_id', 'value'=> $entity->getInventoryId() );
$form['elements'][] = array( 'title' => 'ProductId', 'type'=>'hidden', 'options'=>array(), 'name'=>'entity[product_id]', 'id'=>'product_id', 'value'=> $entity->getProductId() );
$form['elements'][] = array( 'title' => $this->trans('attribute').': *', 'type'=>'selectWithOther', 'options'=>$arrAttributes, 'name'=>'entity[attribute]', 'id'=>'attribute', 'value'=> $entity->getAttribute() );
$form['elements'][] = array( 'title' => $this->trans('number').' '.$this->trans('of').' '.$this->trans('product').'s: *', 'type'=>'text', 'options'=>array(), 'name'=>'entity[n_products]', 'id'=>'n_products', 'value'=> $entity->getNProducts() );
		/***********************************************************************************************************/
		
		//$viewConfig['id'] = 'noticia_evento_id';
		
		$viewConfig['form'] = $form;
		$viewConfig['title'] = ( $cmd =='add' ? $this->tpl->trans('add') : $this->tpl->trans('edit') ) . ' '.$this->tpl->trans('inventory');
		
		$this->tpl->assign( 'form', $form );	
		$this->tpl->assign( 'viewConfig', $viewConfig );
		
		//$this->tpl->assign( 'ArrayImages', $entity->getArrayImages() );
		
		$this->tpl->assign("idProduct", $idProduct );
		
		$this->tpl->display( 'inventory/edit.php' );
	}

	protected function exportToExcel( $data ){
		$xls = new ExcelWriter( 'Noticias_' . date('Y-m-d'), 'Noticias' );
		$xls->writeHeadersAndDataFromArray( $data );
	}

	protected function _save(){
		
		if ( !empty( $_REQUEST ) && isset( $_REQUEST['entity'] ) ){
	  		
			$entity = $_REQUEST['entity'];
  			
			$Inventory = new eCommerce_Entity_Inventory();
			

			// Save type
			try{
			
			
			/******************************************************/
			/******************************************************/
			
$txt = (empty($entity['inventory_id']) ? '' : $entity['inventory_id']);$Inventory->setInventoryId( strip_tags($txt) );
$txt = (empty($entity['product_id']) ? '' : $entity['product_id']);$Inventory->setProductId( strip_tags($txt) );
$txt = (empty($entity['n_products']) ? '' : $entity['n_products']);$Inventory->setNProducts( strip_tags($txt) );


$txt = (empty($entity['attribute']) ? '' : $entity['attribute']);
$txt = str_replace( eCommerce_SDO_Catalog::TOKEN_ORDERITEM_ATTRIBUTE_VALUE,'_',$txt);
$txt = str_replace( eCommerce_SDO_Catalog::TOKEN_CART_PRINCIPAL_TOKEN,'_',$txt);
$txt = str_replace( " ",'_',$txt);
$Inventory->setAttribute( strip_tags($txt) );
  			/******************************************************/
  			/*****************************************************/
  			
				
				/*
				$InventoryTmp = eCommerce_SDO_Inventory::LoadInventory( $Inventory->getInventoryId() );
				
				$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('InventoryImages');
				$imageHandler->setArrImages( $InventoryTmp->getArrayImages() );
				
				$imageHandler->setUploadFileDirectory('files/images/noticias/');
				$imageHandler->setUploadFileDescription('Imagen de Evento ' . $Inventory->getInventoryId() );
				
				$imageHandler->setValidTypes( array('word','image') );
				
				$imageHandler->setAccessType( eCommerce_SDO_ImageHandler::GetValidAccessType('Inventory') );
				
				$Inventory->setArrayImages( implode(',',$InventoryTmp->getArrayImages()) );
				$imageHandler->setMaximum( 1 );
				$arrImages = $imageHandler->proccessImages();
				$Inventory->setArrayImages( $arrImages );
				*/
				
				
				eCommerce_SDO_Inventory::SaverInventory( $Inventory );
				$this->tpl->assign( 'strSuccess', $this->tpl->trans('the').' '.$this->tpl->trans('inventory').' ID ' . $Inventory->getInventoryId() . ' '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('saved')));
								
				$this->tpl->assign( 'idProduct', $Inventory->getProductId() );
				
				$this->_list();
			}
			catch( eCommerce_SDO_Core_Validator_Exception $e){
				$this->tpl->assign( 'errors', new Validator_ErrorHandler( $this->tpl->trans("there_are_errors"), $e->getErrors() ) );
				$oldCmd = $this->getParameter( 'old_cmd', false, null );
				$this->edit( $Inventory,  $oldCmd );
			}
		}
		else{
			throw new Exception($this->tpl->trans('not_enough_information').'. '.$this->tpl->trans('try_again'));
		}
	}

	protected function _delete(){
		$id = $this->getParameter();
			try{
				$Inventory = eCommerce_SDO_Inventory::DeleteInventory($id );
				$this->tpl->assign( 'strSuccess', $this->tpl->trans('the').' '.$this->tpl->trans('inventory').' ID ' . $Inventory->getInventoryId() . ' '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('deleted')));
				$this->tpl->assign( 'idProduct', $Inventory->getProductId() );
			}
			catch(eCommerce_SDO_Core_Application_Exception $e) {
				//debug($e);
				$this->tpl->assign( 'strError', $e->getMessage() );
			}
		
		$this->_list();
	}

}
?>