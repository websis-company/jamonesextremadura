<?php
class eCommerce_FrontEnd_BO_Alianzas extends eCommerce_FrontEnd_BO {


public function getSDOSearch( $search, $extraConditions ){
	return eCommerce_SDO_Alianzas::SearchAlianzas( $search, $extraConditions );
}	


/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
	
	/**
	 * @var eCommerce_DAO_Event
	 */
	protected $search;
	protected $idFields;
	
	public function __construct(){
		parent::__construct();
		$this->idFields = 'alianzas_id';
	}

	public function execute(){
//		$accessManager = new eCommerce_Access_Alianzas( $this );
//		$accessManager->checkPermission(eCommerce_Access::ALIANZAS_ALL_PERMISSIONS);
		$this->checkPermission();
		
		$cmd = $this->getCommand();
		switch( $cmd ){
			case 'delete':
//				$accessManager->checkPermission(eCommerce_Access::ALIANZAS_DELETE);
				$this->_delete();
				break;
			case 'add':
//				$accessManager->checkPermission(eCommerce_Access::ALIANZAS_ADD);
//				$this->_add();
//				break;
			case 'edit':
//				$accessManager->checkPermission(eCommerce_Access::ALIANZAS_EDIT);
				$this->_edit();
				break;
			case 'save':
//				$accessManager->checkPermission(eCommerce_Access::ALIANZAS_SAVE);
				$this->_save();
				break;
			case 'list':
			default:
//				$accessManager->checkPermission(eCommerce_Access::ALIANZAS_LIST);
				$this->_list();
				break;
		}
	}

	protected function _list(){

		$this->search = new eCommerce_Entity_Search(
			$this->getParameter( 'q', false, NULL ),
			$this->getParameter( 'o', false, '' ), //fecha DESC
			$this->getParameter( 'p', true, 1 ),
			$this->getParameter( 'k', true, 10 )
		);
		$extraConditions = array();
		//$extraConditions[] = array( "columName"=>"type","value"=>'',"isInteger"=>false);
		$result = $this->getSDOSearch( $this->search, $extraConditions ); 
		
		
		$exportToExcel = $this->getParameter( 'toExcel' );
		if ( $exportToExcel ){
		
			$regiters = $result->getResults();
			$data = array();
			foreach($regiters as $regiter){
				$data[] = get_object_vars( $regiter );
			}
			$this->exportToExcel( $data );

		}
		else {
			$viewConfig = array();
			$viewConfig['name'] = 'Alianzas';
			$viewConfig['title'] = 'Alianzas';
			$viewConfig['id'] = $this->idFields;
			
			//---------------------------
			if(strpos($viewConfig['id'],",") > -1){
				$ids = explode(",",$viewConfig['id']);
				$id = $ids[0];
				unset($ids[0]);
				$viewConfig['id'] = '{$'.$id.'}';
				foreach($ids as $id){
					$viewConfig['id'] .= ',{$'.$id.'}';
				}
			}else{
				$viewConfig['id'] = '{$'.$viewConfig['id'].'}';
			}
			//---------------------------
			
			$viewConfig["hiddenColums"]= array('alianzas_id','orden');
			
			$this->tpl->assign( 'viewConfig', $viewConfig );
			
			$this->tpl->assign( 'options', $result );
			
			$this->tpl->display(  "Alianzas/list.php" );
		}
	}
	
	protected function _add(){
		try{
			$Alianzas = new eCommerce_Entity_Alianzas();
			$Alianzas = eCommerce_SDO_Alianzas::SaverAlianzas( $Alianzas );
			$this->_edit( $Alianzas->getAlianzasId() );
		}catch( eCommerce_SDO_Core_Validator_Exception $e){
			$this->tpl->assign( 'strError', 'Intente Nuevamente' );
			$this->_list();
		}
	}
	
	protected function _edit($id = null){
		//$id = $this->getParameter();
		$id = empty($id ) ? $this->getParameter('id',false,0) : $id;
		//---------------------------
		if( strpos($id,",") > -1 ){
			$idF = explode(",",$this->idFields);
			$ArrId = explode(",",$id);
			$id = array();
			for($i=0; $i< count($idF); $i++){
				$id[ $idF[$i] ] = (empty($ArrId[$i])) ? 0 : $ArrId[$i];
			}
		}
		//---------------------------
		
		$entity = eCommerce_SDO_Alianzas::LoadAlianzas( $id );
		
		
		$this->tpl->assign( 'errors', new Validator_ErrorHandler() );
		
		$this->edit( $entity, $this->getCommand() );

	}

	public function edit( $entity, $cmd ){

		$this->tpl->assign( 'object',     $entity );
		
		$viewConfig = array();
		$form = array();

		
		$form['name'] = 'AlianzasForm';
		$form['elements'] = array();
		
		
		/***********************************************************************************************************/
		
$form['elements'][] = array( 'title' => 'AlianzasId', 'type'=>'hidden', 'options'=>array(), 'name'=>'entity[alianzas_id]', 'id'=>'alianzas_id', 'value'=> $entity->getAlianzasId() );
$form['elements'][] = array( 'title' => 'Nombre', 'type'=>'text', 'options'=>array(), 'name'=>'entity[nombre]', 'id'=>'nombre', 'value'=> $entity->getNombre() );
$form['elements'][] = array( 'title' => 'Link', 'type'=>'text', 'options'=>array(), 'name'=>'entity[link]', 'id'=>'link', 'value'=> $entity->getLink() );
$form['elements'][] = array( 'title' => 'Logo', 'type'=>'array_files', 'options'=>array(), 'name'=>'entity[logo]', 'id'=>'logo', 'value'=> $entity->getLogo() );
$form['elements'][] = array( 'title' => 'Orden', 'type'=>'text', 'options'=>array(), 'name'=>'entity[orden]', 'id'=>'orden', 'value'=> $entity->getOrden() );
		/***********************************************************************************************************/
		
		//$viewConfig['id'] = 'noticia_evento_id';
		
		$viewConfig['form'] = $form;
		$viewConfig['title'] = ( $cmd =='add' ? 'Agregar' : 'Editar' ) . ' Alianzas';
		
		$this->tpl->assign( 'form', $form );	
		$this->tpl->assign( 'viewConfig', $viewConfig );
		
		//$this->tpl->assign( 'ArrayImages', $entity->getArrayImages() );

		$this->tpl->assign( 'cancelParams', $cmd == 'add' ? '?cmd=delete&id=' . $entity->getAlianzasId() . '' : '' );
		$this->tpl->assign( 'cancelConfirm', $cmd == 'add' ? 'if(confirm(\'Desea eliminar el registro recientemente creado?\'))'  : '' );
		
			$this->tpl->display( 'Alianzas/edit.php' );
	}

	protected function exportToExcel( $data, $fileName = '' ){

		

		$rowO = $data[0];

		

		$contentHtml .= '<table border="1">';

		$contentHtml .= '<tr>

		<td height="72" width="255" bgcolor="#cccccc"><img src="'.ABS_HTTP_URL.'/ima/logo.gif"></td>

		<td colspan="'.(count($rowO) - 1).'"><h2>Order Report - '.date("M d Y").'</h2></td>

		</tr>';

		

		$contentHtml .= '<tr>';

		

		foreach($rowO as $key => $row){

			$key = str_replace("_",' ',$key);

			$key = ucwords($key);

			$contentHtml .= '<th align="center">'.$key.'</th>';

		}

		$contentHtml .= '</tr>';

		

		$i = 0;

		foreach($data as $row){

			

			$bgcolor = (++$i % 2 == 0) ? 'bgcolor="#cccccc"' : '';

			$contentHtml .= '<tr>';

			foreach($row as $rowElement){

				$contentHtml .= '<td '.$bgcolor.' align="right">'.$rowElement.'</td>';

			}

			$contentHtml .= '</tr>';

		}

		

		$contentHtml .= '</table>';

		

		

		$total_bytes = strlen($contentHtml);

		header("Content-type: application/vnd.ms-excel");

		header("Content-disposition: attachment; filename=".$fileName.".xls; size=" . $total_bytes);

		echo $contentHtml;

	}

	protected function _save(){
		
		if ( !empty( $_REQUEST ) && isset( $_REQUEST['entity'] ) ){
	  		
			$entity = $_REQUEST['entity'];
  			
			$Alianzas = new eCommerce_Entity_Alianzas();
			

			// Save type
			try{
			
			
			/******************************************************/
			/******************************************************/
			
$txt = (empty($entity['alianzas_id']) ? '' : $entity['alianzas_id']);$Alianzas->setAlianzasId( strip_tags($txt) );
$txt = (empty($entity['nombre']) ? '' : $entity['nombre']);$Alianzas->setNombre( strip_tags($txt) );
$txt = (empty($entity['link']) ? '' : $entity['link']);$Alianzas->setLink( strip_tags($txt) );
$txt = (empty($entity['logo']) ? '' : $entity['logo']);$Alianzas->setLogo( strip_tags($txt) );
$txt = (empty($entity['orden']) ? '' : $entity['orden']);$Alianzas->setOrden( strip_tags($txt) );
  			/******************************************************/
  			/*****************************************************/
  			
				
				
				$AlianzasTmp = eCommerce_SDO_Alianzas::LoadAlianzas( $Alianzas->getAlianzasId() );
				
				$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('logo');
				$imageHandler->setArrImages( $AlianzasTmp->getLogo() );
				
				$imageHandler->setUploadFileDirectory('files/images/logo/');
				$imageHandler->setUploadVersionDirectory('web/');
				$imageHandler->setUploadFileDescription('Imagen de Evento ' . $Alianzas->getAlianzasId() );
				
				$imageHandler->setValidTypes( array('image') );
				
				$imageHandler->setAccessType(  eCommerce_SDO_ImageHandler::GetValidAccessType('public'));
				$arrVersions = array();
					$arrVersions[] = array('version'=>'small',	'path'=>'images/logo/',	'width'=>90, 'height'=>140);
					$arrVersions[] = array('version'=>'medium',	'path'=>'images/logo/',	'width'=>140, 'height'=>88);
					$imageHandler->setArrVersions($arrVersions);
				$imageHandler->setMaximum( 1 );
				$arrImages = $imageHandler->proccessImages();
				$Alianzas->setLogo( $arrImages );
				
				
	/*$FotogaleriaTmp = eCommerce_SDO_Fotogaleria::LoadFotogaleria( $Fotogaleria->getFotogaleriaId() );
					$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('imagen_principal');
					$imageHandler->setArrImages( $FotogaleriaTmp->getImagenPrincipal() );
					
					$imageHandler->setUploadFileDirectory('files/images/fotogaleria/');
					$imageHandler->setUploadVersionDirectory('web/');
					
					$imageHandler->setUploadFileDescription( $this->getParameter( $imageHandler->GetDescriptionField(),false,'Imagen de Fotogaleria' .$Fotogaleria->getFotogaleriaId() ) );
					
					$imageHandler->setValidTypes( array('word','image') );
					
					$imageHandler->setAccessType( eCommerce_SDO_ImageHandler::GetValidAccessType('public') );
					
					
					$arrVersions = array();
					$arrVersions[] = array('version'=>'small',	'path'=>'images/fotogaleria/',	'width'=>90, 'height'=>140);
					$arrVersions[] = array('version'=>'medium',	'path'=>'images/fotogaleria/',	'width'=>180, 'height'=>280);
					$arrVersions[] = array('version'=>'large',	'path'=>'images/fotogaleria/',	'width'=>900, 'height'=>800);
					$imageHandler->setArrVersions($arrVersions);
					
					
					$imageHandler->setMaximum( 1 );
					$arrImages = $imageHandler->proccessImages();
					$Fotogaleria->setImagenPrincipal( $arrImages );*/			
				eCommerce_SDO_Alianzas::SaverAlianzas( $Alianzas );
				$this->tpl->assign( 'strSuccess', 'Registro Alianzas ha sido guardado.' );
				$this->_list();
			}
			catch( eCommerce_SDO_Core_Validator_Exception $e){
				$this->tpl->assign( 'errors', new Validator_ErrorHandler( "Existen Algunos Errores", $e->getErrors() ) );
				$oldCmd = $this->getParameter( 'old_cmd', false, null );
				$this->edit( $Alianzas,  $oldCmd );
			}
		}
		else{
			throw new Exception('No hay suficiente informaci&oacute;n intente nuevamente');
		}
	}

	protected function _delete(){
		//$id = $this->getParameter();
		$id = $this->getParameter('id',false,0);
		$id2Str = $id;
		//---------------------------
		if( strpos($id,",") > -1 ){
			$idF = explode(",",$this->idFields);
			$ArrId = explode(",",$id);
			$id = array();
			$id2Str = "(";
			for($i=0; $i< count($idF); $i++){
				$id[ $idF[$i] ] = (empty($ArrId[$i])) ? 0 : $ArrId[$i];
				$id2Str .=($i>0) ? ',' : '';
				$id2Str .= $ArrId[$i] . "";
			}
			$id2Str .= ")";
		}
		//---------------------------
		
			try{
				$Alianzas = eCommerce_SDO_Alianzas::DeleteAlianzas($id );
				//$this->tpl->assign( 'strSuccess', 'Alianzas "' . $Alianzas->getAlianzasId() . '" ha sido eliminado' );
				$this->tpl->assign( 'strSuccess', 'Registro Alianzas "' . $id2Str . '" ha sido eliminado' );
				
			}
			catch(eCommerce_SDO_Core_Application_Exception $e) {
				//debug($e);
				$this->tpl->assign( 'strError', $e->getMessage() );
			}
		
		$this->_list();
	}
	
	
	

}
?>