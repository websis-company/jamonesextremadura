<?php
class eCommerce_FrontEnd_BO_Videogaleria extends eCommerce_FrontEnd_BO {


public function getSDOSearch( $search, $extraConditions ){
	return eCommerce_SDO_Videogaleria::SearchVideogaleria( $search, $extraConditions );
}	


/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
	
	/**
	 * @var eCommerce_DAO_Event
	 */
	protected $search;
	protected $idFields;
	
	public function __construct(){
		parent::__construct();
		$this->idFields = 'videogaleria_id';
	}

	public function execute(){
		//$accessManager = new eCommerce_Access_Videogaleria( $this );
		//$accessManager->checkPermission(eCommerce_Access::VIDEOGALERIA_ALL_PERMISSIONS);
		//$this->checkPermission();
		
		$cmd = $this->getCommand();
		switch( $cmd ){
			case 'delete':
				//$accessManager->checkPermission(eCommerce_Access::VIDEOGALERIA_DELETE);
				$this->_delete();
				break;
			case 'add':
				//$accessManager->checkPermission(eCommerce_Access::VIDEOGALERIA_ADD);
				$this->_add();
				break;
			case 'edit':
				//$accessManager->checkPermission(eCommerce_Access::VIDEOGALERIA_EDIT);
				$this->_edit();
				break;
			case 'save':
				//$accessManager->checkPermission(eCommerce_Access::VIDEOGALERIA_SAVE);
				$this->_save();
				break;
			case 'list':
			default:
				//$accessManager->checkPermission(eCommerce_Access::VIDEOGALERIA_LIST);
				$this->_list();
				break;
		}
	}

	protected function _list(){

		$this->search = new eCommerce_Entity_Search(
			$this->getParameter( 'q', false, NULL ),
			$this->getParameter( 'o', false, '' ), //fecha DESC
			$this->getParameter( 'p', true, 1 ),
			$this->getParameter( 'k', true, 10 )
		);
		$extraConditions = array();
		//$extraConditions[] = array( "columName"=>"type","value"=>'',"isInteger"=>false);
		$result = $this->getSDOSearch( $this->search, $extraConditions ); 
		
		
		$exportToExcel = $this->getParameter( 'toExcel' );
		if ( $exportToExcel ){
		
			$regiters = $result->getResults();
			$data = array();
			foreach($regiters as $regiter){
				$data[] = get_object_vars( $regiter );
			}
			$this->exportToExcel( $data );

		}
		else {
			$viewConfig = array();
			$viewConfig['name'] = 'Videogaleria';
			$viewConfig['title'] = 'Videogaleria';
			$viewConfig['id'] = $this->idFields;
			
			//---------------------------
			if(strpos($viewConfig['id'],",") > -1){
				$ids = explode(",",$viewConfig['id']);
				$id = $ids[0];
				unset($ids[0]);
				$viewConfig['id'] = '{$'.$id.'}';
				foreach($ids as $id){
					$viewConfig['id'] .= ',{$'.$id.'}';
				}
			}else{
				$viewConfig['id'] = '{$'.$viewConfig['id'].'}';
			}
			//---------------------------
			
			$viewConfig["hiddenColums"]= array('videogaleria_id','url','descripcion');
			
			$this->tpl->assign( 'viewConfig', $viewConfig );
			
			$this->tpl->assign( 'options', $result );
			
			$this->tpl->display(  "Videogaleria/list.php" );
		}
	}
	
	protected function _add(){
		try{
			$Videogaleria = new eCommerce_Entity_Videogaleria();
			//$Videogaleria = eCommerce_SDO_Videogaleria::SaverVideogaleria( $Videogaleria );
			$this->_edit( $Videogaleria->getVideogaleriaId() );
		}catch( eCommerce_SDO_Core_Validator_Exception $e){
			$this->tpl->assign( 'strError', $this->tpl->trans('try_again') );
			$this->_list();
		}
	}
	
	protected function _edit($id = null){
		//$id = $this->getParameter();
		$id = empty($id ) ? $this->getParameter('id',false,0) : $id;
		//---------------------------
		if( strpos($id,",") > -1 ){
			$idF = explode(",",$this->idFields);
			$ArrId = explode(",",$id);
			$id = array();
			for($i=0; $i< count($idF); $i++){
				$id[ $idF[$i] ] = (empty($ArrId[$i])) ? 0 : $ArrId[$i];
			}
		}
		//---------------------------
		
		$entity = eCommerce_SDO_Videogaleria::LoadVideogaleria( $id );
		
		
		$this->tpl->assign( 'errors', new Validator_ErrorHandler() );
		
		$this->edit( $entity, $this->getCommand() );

	}

	public function edit( $entity, $cmd ){

		$this->tpl->assign( 'object',     $entity );
		
		$viewConfig = array();
		$form = array();

		
		$form['name'] = 'VideogaleriaForm';
		$form['elements'] = array();
		
		
		/***********************************************************************************************************/
		
$form['elements'][] = array( 'title' => 'VideogaleriaId:', 'type'=>'hidden', 'options'=>array(), 'name'=>'entity[videogaleria_id]', 'id'=>'videogaleria_id', 'value'=> $entity->getVideogaleriaId() );
$form['elements'][] = array( 'title' => 'Titulo:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[titulo]', 'id'=>'titulo', 'value'=> $entity->getTitulo() );
$form['elements'][] = array( 'title' => 'Descripcion:', 'type'=>'textarea', 'options'=>array(), 'name'=>'entity[descripcion]', 'id'=>'descripcion', 'value'=> $entity->getDescripcion() );
$form['elements'][] = array( 'title' => 'Imagen:', 'type'=>'array_files', 'options'=>array(), 'name'=>'entity[imagen]', 'id'=>'imagen', 'value'=> $entity->getImagen(), 'max_files'=>1 );
$form['elements'][] = array( 'title' => 'Url:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[url]','comments'=>'Ejemplo: http://www.youtube.com/watch?v=G_hM9Tob0Dg',  'id'=>'url', 'value'=> $entity->getUrl() );
$form['elements'][] = array( 'title' => 'Fecha:', 'type'=>'calendar', 'options'=>array(), 'name'=>'entity[fecha]', 'id'=>'fecha', 'value'=> $entity->getFecha() );
		/***********************************************************************************************************/
		
		//$viewConfig['id'] = 'noticia_evento_id';
		
		$viewConfig['form'] = $form;
		$viewConfig['title'] = ( $cmd =='add' ? $this->tpl->trans('add') : $this->tpl->trans('edit') ) . ' Videogaleria';
		
		$this->tpl->assign( 'form', $form );	
		$this->tpl->assign( 'viewConfig', $viewConfig );
		
		//$this->tpl->assign( 'ArrayImages', $entity->getArrayImages() );

		$this->tpl->assign( 'cancelParams', $cmd == 'add' ? '?cmd=delete&id=' . $entity->getVideogaleriaId() . '' : '' );
		$this->tpl->assign( 'cancelConfirm', $cmd == 'add' ? 'if(confirm("'.$this->tpl->trans('?_confirm_delete').' '.strtolower($this->tpl->trans('recently_created_entry')).'?"))'  : '' );
		
			$this->tpl->display( 'Videogaleria/edit.php' );
	}

	protected function exportToExcel( $data, $fileName = '' ){

		

		$rowO = $data[0];

		

		$contentHtml .= '<table border="1">';

		$contentHtml .= '<tr>

		<td height="72" width="255" bgcolor="#cccccc"><img src="'.ABS_HTTP_URL.'/ima/logo.gif"></td>

		<td colspan="'.(count($rowO) - 1).'"><h2>Order Report - '.date("M d Y").'</h2></td>

		</tr>';

		

		$contentHtml .= '<tr>';

		

		foreach($rowO as $key => $row){

			$key = str_replace("_",' ',$key);

			$key = ucwords($key);

			$contentHtml .= '<th align="center">'.$key.'</th>';

		}

		$contentHtml .= '</tr>';

		

		$i = 0;

		foreach($data as $row){

			

			$bgcolor = (++$i % 2 == 0) ? 'bgcolor="#cccccc"' : '';

			$contentHtml .= '<tr>';

			foreach($row as $rowElement){

				$contentHtml .= '<td '.$bgcolor.' align="right">'.$rowElement.'</td>';

			}

			$contentHtml .= '</tr>';

		}

		

		$contentHtml .= '</table>';

		

		

		$total_bytes = strlen($contentHtml);

		header("Content-type: application/vnd.ms-excel");

		header("Content-disposition: attachment; filename=".$fileName.".xls; size=" . $total_bytes);

		echo $contentHtml;

	}

	protected function _save(){
		
		if ( !empty( $_REQUEST ) && isset( $_REQUEST['entity'] ) ){
	  		
			$entity = $_REQUEST['entity'];
  			
			$Videogaleria = new eCommerce_Entity_Videogaleria();
			

			// Save type
			try{
			
			
			/******************************************************/
			/******************************************************/
			
$txt = (empty($entity['videogaleria_id']) ? '' : $entity['videogaleria_id']);$Videogaleria->setVideogaleriaId( strip_tags($txt) );
$txt = (empty($entity['titulo']) ? '' : $entity['titulo']);$Videogaleria->setTitulo( strip_tags($txt) );
$txt = (empty($entity['descripcion']) ? '' : $entity['descripcion']);$Videogaleria->setDescripcion( strip_tags($txt) );

/////////////////////////////////////////////////////////////////////////////////////////
$VideogaleriaTmp = eCommerce_SDO_Videogaleria::LoadVideogaleria( $Videogaleria->getVideogaleriaId() );
					$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('imagen');
					$imageHandler->setArrImages( $VideogaleriaTmp->getImagen() );
					
					$imageHandler->setUploadFileDirectory('files/images/videogaleria/');
					$imageHandler->setUploadVersionDirectory('web/');
					
					$imageHandler->setUploadFileDescription( $this->getParameter( $imageHandler->GetDescriptionField(),false,'Imagen de Videogaleria' .$Videogaleria->getVideogaleriaId() ) );
					
					$imageHandler->setValidTypes( array('word','image') );
					
					$imageHandler->setAccessType( eCommerce_SDO_ImageHandler::GetValidAccessType('public') );
					
					
					$arrVersions = array();
					$arrVersions[] = array('version'=>'small',	'path'=>'images/videogaleria/',	'width'=>90, 'height'=>140);
					$arrVersions[] = array('version'=>'medium',	'path'=>'images/videogaleria/',	'width'=>180, 'height'=>280);
					$arrVersions[] = array('version'=>'large',	'path'=>'images/videogaleria/',	'width'=>900, 'height'=>800);
					$imageHandler->setArrVersions($arrVersions);
					
					
					$imageHandler->setMaximum( 1 );
					$arrImages = $imageHandler->proccessImages();
					$Videogaleria->setImagen( $arrImages );
/////////////////////////////////////////////////////////////////////////////////////////
$txt = (empty($entity['url']) ? '' : $entity['url']);$Videogaleria->setUrl( strip_tags($txt) );
$txt = (empty($entity['fecha']) ? '' : $entity['fecha']);$Videogaleria->setFecha( strip_tags($txt) );
  			/******************************************************/
  			/*****************************************************/
  			
				
				/*
				$VideogaleriaTmp = eCommerce_SDO_Videogaleria::LoadVideogaleria( $Videogaleria->getVideogaleriaId() );
				
				$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('VideogaleriaImages');
				$imageHandler->setArrImages( $VideogaleriaTmp->getArrayImages() );
				
				$imageHandler->setUploadFileDirectory('files/images/noticias/');
				$imageHandler->setUploadFileDescription('Imagen de Evento ' . $Videogaleria->getVideogaleriaId() );
				
				$imageHandler->setValidTypes( array('word','image') );
				
				$imageHandler->setAccessType( eCommerce_SDO_ImageHandler::GetValidAccessType('public') );
				
				$Videogaleria->setArrayImages( implode(',',$VideogaleriaTmp->getArrayImages()) );
				$imageHandler->setMaximum( 1 );
				$arrImages = $imageHandler->proccessImages();
				$Videogaleria->setArrayImages( $arrImages );
				*/
				
				
				eCommerce_SDO_Videogaleria::SaverVideogaleria( $Videogaleria );
				$this->tpl->assign( 'strSuccess', $this->tpl->trans('the').' Videogaleria "' . $Videogaleria->getVideogaleriaId() . '" '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('saved')));
				$this->_list();
			}
			catch( eCommerce_SDO_Core_Validator_Exception $e){
				$this->tpl->assign( 'errors', new Validator_ErrorHandler( $this->tpl->trans("there_are_errors"), $e->getErrors() ) );
				$oldCmd = $this->getParameter( 'old_cmd', false, null );
				$this->edit( $Videogaleria,  $oldCmd );
			}
		}
		else{
			throw new Exception($this->tpl->trans('not_enough_information').'. '.$this->tpl->trans('try_again'));
		}
	}

	protected function _delete(){
		//$id = $this->getParameter();
		$id = $this->getParameter('id',false,0);
		$id2Str = $id;
		//---------------------------
		if( strpos($id,",") > -1 ){
			$idF = explode(",",$this->idFields);
			$ArrId = explode(",",$id);
			$id = array();
			$id2Str = "(";
			for($i=0; $i< count($idF); $i++){
				$id[ $idF[$i] ] = (empty($ArrId[$i])) ? 0 : $ArrId[$i];
				$id2Str .=($i>0) ? ',' : '';
				$id2Str .= $ArrId[$i] . "";
			}
			$id2Str .= ")";
		}
		//---------------------------
		
			try{
				$Videogaleria = eCommerce_SDO_Videogaleria::DeleteVideogaleria($id );
				$this->tpl->assign( 'strSuccess', $this->tpl->trans('the').' Videogaleria "' . $Videogaleria->getVideogaleriaId() . '" '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('deleted')));
				
			}
			catch(eCommerce_SDO_Core_Application_Exception $e) {
				//debug($e);
				$this->tpl->assign( 'strError', $e->getMessage() );
			}
		
		$this->_list();
	}
	
	
	

}
?>