<?php

class eCommerce_Access_cupondescuento extends eCommerce_Access {
	
	
	protected $application;
	function __construct( eCommerce_FrontEnd_BO $app ){
		$this->application = $app;
	}

	public function loadPermissions(){
		$userProfile = eCommerce_FrontEnd_Util_UserSession::GetIdentity();
		$permissions = parent::loadPermissions();
		$result = false;
		$app = $this->application;
		
		switch( $this->permission ){
			case self::CUPONDESCUENTO_ALL_PERMISSIONS:
				$result = false;
			break;
			break;
			default:
			break;
		}
		
		if( $result ){
			$permissions[] = $this->permission;
		}else{
			$permissions = self::deletePermission($permissions);
		}
		return $permissions;
	}
	
	
}

?>
