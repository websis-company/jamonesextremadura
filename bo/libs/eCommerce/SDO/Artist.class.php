<?php
class eCommerce_SDO_Artist{
	const TYPE_SCULPTOR = eCommerce_SDO_Core_Application_Artist::TYPE_SCULPTOR;
	
	public static function LoadArtist( $artistId ){
		return eCommerce_SDO_Core_Application_Artist::LoadById( $artistId ); 
	}
	
	public static function SearchArtistsByType( eCommerce_Entity_Search $search, $type){
		return eCommerce_SDO_Core_Application_Artist::SearchByType( $search, $type);
	}
	
	public static function SearchArtists( eCommerce_Entity_Search $search){
		return eCommerce_SDO_Core_Application_Artist::Search( $search);
	}
	
	public static function SaveArtist( eCommerce_Entity_Catalog_Artist $artist ){
		return eCommerce_SDO_Core_Application_Artist::Save( $artist );
	}
	
	public static function Delete( $artistId ){
		return eCommerce_SDO_Core_Application_Artist::Delete( $artistId );
	}
	
	public static function GetAllArtists(){
		return eCommerce_SDO_Core_Application_Artist::GetAll();
	}
	
	public static function SearchProductsByArtist( eCommerce_Entity_Search $search, $artistId = null, $ofArtist = true ){
		return eCommerce_SDO_Core_Application_Product::SearchByArtist( $search, $artistId, $ofArtist );
	}
	
	public static function SearchProductsByArtistAndCategory( $search, $artistId, $categoryId, $inCategory = true){
		return eCommerce_SDO_Core_Application_Product::SearchByArtistAndCategory( $search, $artistId, $categoryId, $inCategory);
	}

	public static function GetArtistByProductAndType($productId, $type){
		return eCommerce_SDO_Core_Application_ProductArtist::GetArtistByProductAndType($productId, $type);
	}
	
	public static function GetProductsByArtistId( $artistId ){
		return eCommerce_SDO_Core_Application_ProductArtist::GetProductsByArtist( $artistId );
	}

	public static function AddProductToArtist( $productId, $artistId ){
		return eCommerce_SDO_Core_Application_ProductArtist::AssociateProductToArtist( $artistId , $productId );
	}
	
	public static function DeleteProductFromArtist( $productId, $artistId ){
		return eCommerce_SDO_Core_Application_ProductArtist::DeleteProductArtistAssociation(  $productId, $artistId );
	}
	
	public static function writeHTMLInfoArtist( eCommerce_Entity_Catalog_Artist $product, $position = false ){
		return eCommerce_SDO_Core_Application_Artist::writeHTMLInfoArtist( $product, $position);
	}	

	public static function SaveArtistImage( eCommerce_Entity_Util_FileUpload $imageFile ){
		return eCommerce_SDO_Core_Application_FileManagement::SaveImageUpload( $imageFile, "Image for Artist", "files/images/artists/" );
	}
}
?>