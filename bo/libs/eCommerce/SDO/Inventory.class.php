<?php
class eCommerce_SDO_Inventory{
	public static function GetArrNamesAttributes(){
		return eCommerce_SDO_Core_Application_Inventory::GetArrNamesAttributes();
	}
	
	public static function GetAttributeValuesGroupByProduct( $product_id, $attribute ){
		return eCommerce_SDO_Core_Application_Inventory::GetAttributeValuesGroupByProduct( $product_id, $attribute );
	}
	
	public static function GetAttributesOfAProduct( $productId ){
		return eCommerce_SDO_Core_Application_Inventory::GetAttributesOfAProduct( $productId );
	}
	
	
	
	public static function LoadByProductGroupByAttribute( $productId, $attribute ){
		return eCommerce_SDO_Core_Application_Inventory::LoadByProductGroupByAttribute( $productId, $attribute );
	}
	
	public static function LoadByProductGroupByHeight( $productId ){
		return eCommerce_SDO_Core_Application_Inventory::LoadByProductGroupByHeight( $productId );
	}
	
	public static function LoadByProductGroupByColors( $productId ){
		return eCommerce_SDO_Core_Application_Inventory::LoadByProductGroupByColors( $productId );
	}
	
	public static function UpdateInventory( $orderId ){
		return eCommerce_SDO_Core_Application_Inventory::UpdateInventory( $orderId );
	}
	
	/**
	 * Retrieves all the User Profiles filtered by an specific user role and search parameters. 
	 * If the role is Null or an empty string, 
	 *
	 * @param eCommerce_Entity_Search $search - The search parameters
	 * @param string $role                    - The Role to filter by. Empty for all roles.
	 * @return eCommerce_Entity_Search_Result_Profile
	 */
	public static function SearchInventory( eCommerce_Entity_Search $search, $extraConditions = array() ){
		return eCommerce_SDO_Core_Application_Inventory::Search( $search, $extraConditions );
	}
	
	public static function LoadByProductColorAndHeight( $productId, $color, $height ){
		return eCommerce_SDO_Core_Application_Inventory::LoadByProductColorAndHeight( $productId, $color, $height );
	}
	
	public static function LoadByProductAllAttributes( $productId, $attributes ){
		return eCommerce_SDO_Core_Application_Inventory::LoadByProductAllAttributes( $productId, $attributes );
	}
	
	/**
	 * Load the User Profile from the given ID. Note an empty object is returned if the ID doesn't exist
	 *
	 * @param int $categoryId
	 * @return eCommerce_Entity_User
	 */
	public static function LoadInventory( $InventoryId ){
		return eCommerce_SDO_Core_Application_Inventory::LoadById( $InventoryId ); 
	}
	
	/**
	 * Saves a new or updates an existing User Profile object
	 *
	 * @param eCommerce_Entity_User $profile
	 * @return eCommerce_Entity_User
	 * @throws eCommerce_SDO_Application_Exception
	 */
	public static function SaverInventory( eCommerce_Entity_Inventory $Inventory ){
		
		return eCommerce_SDO_Core_Application_Inventory::Save( $Inventory );
	}

	/**
	 * Deletes a User Profile from the System
	 *
	 * @param int $profileId                            - The ID of the user profile to be deleted
	 * @return eCommerce_Entity_Catalog_Category        - The recently deleted category
	 * @throws eCommerce_SDO_Core_Application_Exception - When the category is not found or couldn't be deleted
	 */
	public static function DeleteInventory( $InventoryId ){
		return eCommerce_SDO_Core_Application_Inventory::Delete( $InventoryId ); 
	}
	
	/**
	 * Retrieves all the Inventory available.
	 * 
	 * @return eCommerce_Entity_Util_Inventoryrray
	 */
	public static function GetAllInventory(){
		return eCommerce_SDO_Core_Application_Inventory::GetAll();
	}
	

}
?>