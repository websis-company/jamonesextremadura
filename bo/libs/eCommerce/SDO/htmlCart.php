<table class="shopping-cart-table table-hover">
	<!-- <caption>Mi compra</caption> -->
	<thead class="shopping-cart-thead">
		<tr>
			<th class="prod-act"></th>
			<th class="prod-img">Imagen</th>
			<th class="prod-des">Descripción</th>
			<th class="prod-sku">SKU</th>
			<th class="prod-qua">Unidades</th>
			<th class="prod-pri">Precio unitario</th>
			<th class="prod-sub">Subtotal</th>
		</tr>
	</thead>

	<tfoot class="shopping-cart-tfoot" >
		<tr>
			<td colspan="5"><small>El Subtotal no incluye el envío. El costo del envío será confirmado una vez que continúes con la compra.</small></td>
			<td colspan="2" class="shopping-cart-subtotal">
				<span>Subtotal</span> <strong>$ 10,897.00</strong>
			</td>
		</tr>
		<tr>
			<td colspan="5"></td>
			<td colspan="2" class="shopping-cart-shipping">
				<span>Envío</span> <strong>$ 170.50</strong>
			</td>
		</tr>
		<tr>
			<td colspan="5"></td>
			<td colspan="2" class="shopping-cart-total">
				<span>Total</span> <strong>$ 10,897.00</strong>
			</td>
		</tr>


		<tr class="shopping-cart-actions">
			<td colspan="7">
				<a href="#" class="button-flat secondary">Seguir comprando</a>
				<a href="#" class="button-flat primary">Comprar</a>
			</td>
		</tr>
	</tfoot>

	<tbody class="shopping-cart-tbody">
		<tr>
			<td class="prod-act"><a href="#" class="icon-delete"></a></td>
			<td class="prod-img"><img src="images-product/01.jpg" alt="Nombre del producto"></td>
			<td class="prod-des">Instant Green Tea Powder Handy Stick Packet</td>
			<td class="prod-sku">55832AFC90</td>
			<td class="prod-qua"><input type="number" id="" name="" value="1"></td>
			<td class="prod-pri">$ 777,777.77</td>
			<td class="prod-sub">$ 777,777.77</td>
		</tr>
		<tr>
			<td class="prod-act"><a href="#" class="icon-delete"></a></td>
			<td class="prod-img"><img src="images-product/02.jpg" alt="Nombre del producto"></td>
			<td class="prod-des">Instant Green Tea Powder Handy Stick Packet</td>
			<td class="prod-sku">55832AFC90</td>
			<td class="prod-qua"><input type="number" id="" name="" value="1"></td>
			<td class="prod-pri">$ 7,445.50</td>
			<td class="prod-sub">$ 777,777.77</td>
		</tr>
		<tr>
			<td class="prod-act"><a href="#" class="icon-delete"></a></td>
			<td class="prod-img"><img src="images-product/01.jpg" alt="Nombre del producto"></td>
			<td class="prod-des">Instant Green Tea Powder Handy Stick Packet</td>
			<td class="prod-sku">55832AFC90</td>
			<td class="prod-qua"><input type="number" id="" name="" value="1"></td>
			<td class="prod-pri">$ 7,445.50</td>
			<td class="prod-sub">$ 777,777.77</td>
		</tr>
		<tr>
			<td class="prod-act"><a href="#" class="icon-delete"></a></td>
			<td class="prod-img"><img src="images-product/02.jpg" alt="Nombre del producto"></td>
			<td class="prod-des">Instant Green Tea Powder Handy Stick Packet</td>
			<td class="prod-sku">55832AFC90</td>
			<td class="prod-qua"><input type="number" id="" name="" value="1"></td>
			<td class="prod-pri">$ 7,445.50</td>
			<td class="prod-sub">$ 777,777.77</td>
		</tr>
	</tbody>
</table>