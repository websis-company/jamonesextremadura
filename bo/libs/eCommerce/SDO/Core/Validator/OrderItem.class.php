<?php
class eCommerce_SDO_Core_Validator_OrderItem extends eCommerce_SDO_Core_Validator {
	
	/**
	 * @var eCommerce_Entity_Order_Item
	 */
	protected $entity;

	public function __construct( eCommerce_Entity_Order_Item $order ){ 
		parent::__construct( $order );
	}
	
	/**
	 * @see eCommerce_SDO_Core_Validator::setup()
	 */
	protected function setup(){
		$this->setupOrderItemId();
		$this->setupOrderId();
		$this->setupSku();
		$this->setupName();
		$this->setupPrice();
		//$this->setupDiscount();
		$this->setupQuantity();
	}
	
	protected function setupOrderItemId(){
		$tester = new Validator_Tester( $this->entity->getOrderItemId(), false );
		$tester->addTest( new Validator_Test_Numeric_UnsignedInteger(),
		                  'Order ID must be a positive integer value, zero or null' );
		$this->validator->addTester( 'order_item_id', $tester );
	}
	
	protected function setupOrderId(){
		$tester = new Validator_Tester( $this->entity->getOrderId(), true );
		$tester->addTest( new Validator_Test_Numeric_UnsignedInteger(),
		                  'Order ID must be a positive integer value, zero or null' );
		$tester->addTest( new eCommerce_SDO_Core_Validator_Test_ExistingEntity( new eCommerce_SDO_Core_DAO_Order() ),
		                  'The Order ID given does not exist in the system.' );
		$this->validator->addTester( 'order_id', $tester );
	}

	protected function setupSku(){
		$tester = new Validator_Tester( $this->entity->getSku(), false );
		$tester->addTest( new Validator_Test_NotEmpty(),
		                  'SKU must not be empty' );
		$tester->addTest( new Validator_Test_String(),
		                  'SKU must be a valid string' );
		$this->validator->addTester( 'sku', $tester );
	}
	
	protected function setupName(){
		$tester = new Validator_Tester( $this->entity->getName(), true );
		$tester->addTest( new Validator_Test_NotEmpty(),
		                  'Product name must not be empty' );
		$tester->addTest( new Validator_Test_String(),
		                  'Product name must be a valid string' );
		$this->validator->addTester( 'name', $tester );
	}
	
	protected function setupPrice(){
		$tester = new Validator_Tester( $this->entity->getPrice(), true );
		$tester->addTest( new Validator_Test_Numeric_Unsigned(),
		                  'Price must be a valid unsigned number (integer or float)' );
		$this->validator->addTester( 'price', $tester );
	}
	
	protected function setupCurrency(){
		$tester = new Validator_Tester( $this->entity->getCurrency(), false );
		
		$tester->addTest( new Validator_Test_RegEx( '/^[A-Z]{3}$/'),
		                  'Currency must be a valid 3-letter code (UPPERCASE).' );
		$this->validator->addTester( 'currency', $tester );
	}
	
	/*protected function setupDiscount(){
		$tester = new Validator_Tester( $this->entity->getDiscount(), false );
		$tester->addTest( new Validator_Test_Numeric(),
		                  'Discount must be a valid number.' );
		$tester->addTest( new Validator_Test_Numeric_Between( 0, 100 ),
		                  'Discount must be between 0 and 100.' );
		$this->validator->addTester( 'discount', $tester );
	}*/
	
	protected function setupQuantity(){
		$tester = new Validator_Tester( $this->entity->getQuantity(), true );
		$tester->addTest( new Validator_Test_Numeric_UnsignedInteger(),
		                  'Quantity must be a positive integer value' );
		$this->validator->addTester( 'quantity', $tester );
	}
}
?>