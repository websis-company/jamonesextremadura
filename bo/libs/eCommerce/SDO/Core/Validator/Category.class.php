<?php
class eCommerce_SDO_Core_Validator_Category extends eCommerce_SDO_Core_Validator {
	
	/**
	 * @var eCommerce_Entity_Catalog_Category
	 */
	protected $entity;

	public function __construct( eCommerce_Entity_Catalog_Category $category ){ 
		parent::__construct( $category );
	}
	
	/**
	 * @see eCommerce_SDO_Core_Validator::setup()
	 */
	protected function setup(){
		$this->setupCategoryId();
		$this->setupParentCategoryId();
		$this->setupName();
		//$this->setupImageId();
	}
	
	protected function setupCategoryId(){
		$tester = new Validator_Tester( $this->entity->getCategoryId(), false );
		$tester->addTest( new Validator_Test_Numeric_UnsignedInteger(),
		                  'Category ID must be a positive integer value, zero or null' );
		$this->validator->addTester( 'category_id', $tester );
	}
	
	/**
	 * Validates the following regarding the Parent Category ID: 
	 * 1. Required value
	 * 2. Unsigned integer
	 * 3. Existing Category
	 * 4. Not a self reference
	 * 
	 * @todo 5. Not a child category
	 */
	protected function setupParentCategoryId(){
		$tester = new Validator_Tester( $this->entity->getParentCategoryId(), false );
		// Verify it's a valid ID numeric value
		$tester->addTest( new Validator_Test_Numeric_UnsignedInteger(),
		                'Parent Category ID must be a positive integer value' );
		
		// Verify the ID actually exists as a category
		$tester->addTest( new eCommerce_SDO_Core_Validator_Test_ExistingEntity( 
		                    new eCommerce_SDO_Core_DAO_Category() ),
		                'Parent Category ID is an invalid value. That category doesn\'t exist.'	);
		                    
		// Verify the ID is not itself
		$tester->addTest( new Validator_Test_NotEqual( $this->entity->getCategoryId() ),
		                'Parent Category ID must not be a self reference to the category in question.'	);
		
		$this->validator->addTester( 'parent_category_id', $tester );
	}
	
	protected function setupName(){
		$tester = new Validator_Tester( $this->entity->getName(), true );
		
		$tester->addTest( new Validator_Test_NotEmpty(), 
		                'Category name is required and must not be empty or null' );
		
		// Verify name is a valid string instead of anything else
		$tester->addTest( new Validator_Test_String(),
		                  'Category name must be a valid string' );
		
		$this->validator->addTester( 'name', $tester );
	}
	
	/*protected function setupImageId(){
		$tester = new Validator_Tester( $this->entity->getImageId(), false );
		// Verify it's a valid ID numeric value
		$tester->addTest( new Validator_Test_Numeric_UnsignedInteger(),'Image ID must be a positive integer value' );
		
		// Verify the ID actually exists as a category
		$tester->addTest( new eCommerce_SDO_Core_Validator_Test_ExistingEntity( 
		                    new eCommerce_SDO_Core_DAO_File() ),
		                'Image ID is an invalid value. That image doesn\'t exist.'	);
		$this->validator->addTester( 'image_id', $tester );
	}*/
	
}
?>