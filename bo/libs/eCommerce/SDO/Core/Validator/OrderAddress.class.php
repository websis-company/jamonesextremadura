<?php
class eCommerce_SDO_Core_Validator_OrderAddress extends eCommerce_SDO_Core_Validator_Address {

	/**
	 * @var eCommerce_Entity_Order_Address
	 */
	protected $entity;

	public function __construct( eCommerce_Entity_Order_Address $address ){ 
		parent::__construct( $address );
	}
	
	/**
	 * @see eCommerce_SDO_Core_Validator_Address::setup()
	 */
	protected function setup(){
		parent::setup();
		$this->setupOrderAddressId();
		$this->setupOrderId();
		$this->setupType();
	}
	
	
	protected function setupOrderAddressId(){
		$tester = new Validator_Tester( $this->entity->getOrderAddressId(), false );
		$tester->addTest( new Validator_Test_Numeric_UnsignedInteger(),
		                  'Order Address ID must be a positive integer value, zero or null' );
		$this->validator->addTester( 'order_address_id', $tester );
	}
	
	/**
	 * Checks the validity of the Order ID. This attribute is NOT required but if present it must be a valid order.
	 */
	protected function setupOrderId(){
		$tester = new Validator_Tester( $this->entity->getOrderId(), true );
		$tester->addTest( new eCommerce_SDO_Core_Validator_Test_ExistingEntity( new eCommerce_SDO_Core_DAO_Order() ),
		                  'Order ID be from a valid order in the system.' );
		$this->validator->addTester( 'order_id', $tester );
	}

	/**
	 * Checks the validity of the Address Type. This attribute IS required and must be a valid string.
	 */
	protected function setupType(){
		$tester = new Validator_Tester( $this->entity->getType(), true );
		$tester->addTest( new Validator_Test_NotEmpty(),
		                  'Address type must not be empty.' );
		
		$tester->addTest( new Validator_Test_String(),
		                  'Address type must not be a valid string.' );
		$this->validator->addTester( 'type', $tester );
	}
}
?>