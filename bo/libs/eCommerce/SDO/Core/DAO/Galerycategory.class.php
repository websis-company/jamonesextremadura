<?php
class eCommerce_SDO_Core_DAO_Galerycategory extends eCommerce_SDO_Core_DAO {
	
	protected $table = 'galerycategory';
	protected $id_field = 'galerycategory_id';
	
	public function __construct(){
		parent::__construct( new eCommerce_SDO_Core_DB() );
		$this->setSearchFields( array( "") );
	}

	public function GetImplodeCategories($category_id, $delimit = ',', $menu = ''){
		$sql = "SELECT CONCAT_WS(c4.name,c3.name,c2.name,c1.name) AS categories
				FROM galerycategory c1
				LEFT JOIN galerycategory c2 ON c1.parent_category_id = c2.category_id
				LEFT JOIN galerycategory c3 ON c2.parent_category_id = c3.category_id
				LEFT JOIN galerycategory c4 ON c3.parent_category_id = c4.category_id
				WHERE c1.galerycategory_id = $category_id "
				.($menu ? " AND (c1.menu = '$menu' OR c2.menu = '$menu' OR c3.menu = '$menu' OR c4.menu = '$menu')" : '');
		return $this->db->sqlGetField($sql);
	}

	public function getCountGaleryById($id_galery){
		$sql = 'SELECT COUNT(galeria_id) AS total FROM galeria WHERE galerycategory_id = '.$id_galery;
		return $this->db->sqlGetRecord( $sql ); 
	}
	
//	public function loadByColum1AndOtherColum2( $email ){
//		
//		$sql = "SELECT *"
//		     . " FROM   " . $this->table
//		     . " WHERE  colum1 = '" . $this->db->escapeString( $colum1 ) . "' 
//			AND colum2 = '" . . $this->db->escapeString( $colum2 ) . . "'";
//		return $this->db->sqlGetRecord( $sql ); 
//	}
	
	
}
?>