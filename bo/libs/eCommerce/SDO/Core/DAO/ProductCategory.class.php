<?php
class eCommerce_SDO_Core_DAO_ProductCategory extends eCommerce_SDO_Core_DAO {
	
	protected $table = 'product_category';
	protected $id_field = '';

	/**
	 * @see DB_DAO_GenericDAO::saveNew()
	 *
	 * @param array $arrEvent
	 * @return unknown
	 */
	public function saveNew( array &$arrEvent ){
		throw new Exception( __METHOD__ . 'Method not available for this DAO ' . __CLASS__ . '. Use SaveOrUpdate() instead');
	}

	/**
	 * @see DB_DAO_GenericDAO::saveOrUpdate()
	 *
	 * @param unknown_type $arrEvent
	 * @return unknown
	 */
	public function saveOrUpdate( &$arrEvent ){
		$fields = $this->prepareFields( $arrEvent );

		$sql = "REPLACE INTO `" . $this->table . "` SET " . implode( ",\n ", $fields ) . "";
		$affectedRows = $this->db->sqlExecute( $sql );
		return $affectedRows;
	}

	/**
	 * @see DB_DAO_GenericDAO::update()
	 *
	 * @param unknown_type $arrEvent
	 * @return unknown
	 */
	public function update( &$arrEvent ){
		throw new Exception( __METHOD__ . 'Method not available for this DAO ' . __CLASS__ . '. Use SaveOrUpdate() instead');
	}

	public function getProductCategories( $productId, $ownership = 'self' ){
		$sql = "SELECT category_id "
		     . "FROM   " . $this->table . " "
		     . "WHERE  product_id = '" . $this->db->escapeString( $productId ) . "'"
		     . (!empty($ownership) ? "AND ownership = '$ownership'" : "");
		return $this->db->sqlGetArray( $sql );
	}
	
	public function getCountGaleryById($id_category){
		$sql = 'SELECT COUNT(product_id) AS total FROM product_category WHERE category_id = '.$id_category;
		return $this->db->sqlGetRecord( $sql );
	}

	public function getCategoryProducts( $categoryId , $limit ){
		$sql = "SELECT product_category.product_id "
		     . "FROM   " . $this->table . " INNER JOIN product ON ( product_category.product_id = product.product_id ) "
		     . "WHERE  product_category.category_id = '" . $this->db->escapeString( $categoryId )."' AND product.status = 'Active'";

		 //echo $sql;
		 //die();

		//$sql .= !empty($limit) ? "' LIMIT ".$limit : "'";
		$sql .= ' ORDER BY product.orden ASC';
		return $this->db->sqlGetArray( $sql );
	}
	
	public function deleteRelationsToProduct( $productId ){
		$sql = "DELETE FROM " . $this->table . " WHERE product_id = '" . $this->db->escapeString( $productId ) . "'";
		return $this->db->sqlExecute( $sql );
	}
	
	public function deleteRelationsToCategory( $categoryId ){
		$sql = "DELETE FROM " . $this->table . " WHERE category_id = '" . $this->db->escapeString( $categoryId ) . "'";
		return $this->db->sqlExecute( $sql );
	}
	
	public function deleteRelation( $productId, $categoryId ){
		$sql = "DELETE FROM " . $this->table . " "
		     . "WHERE  product_id = '" . $this->db->escapeString( $productId ) . "' "
		     . "AND    category_id = '" . $this->db->escapeString( $categoryId ) . "' ";
		return $this->db->sqlExecute( $sql );
	}
}
?>