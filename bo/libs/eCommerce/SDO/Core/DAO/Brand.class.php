<?php
class eCommerce_SDO_Core_DAO_Brand extends eCommerce_SDO_Core_DAO {
	
	protected $table = 'brand';
	protected $id_field = 'brand_id';
	
	public function __construct(){
		parent::__construct( new eCommerce_SDO_Core_DB() );
		$this->setSearchFields( array( "name") );
	}
	
	public function getAllBrandsByCategoryId($categoryId){
		$sql = 'SELECT b.* FROM brand b
				INNER JOIN product p ON p.brand_id = b.brand_id
				INNER JOIN product_category pc ON pc.product_id = p.product_id
				WHERE pc.category_id = '.$categoryId.'
				GROUP BY b.brand_id';	
				return $this->db->sqlGetResult( $sql ); 
	}
	
//	public function loadByColum1AndOtherColum2( $email ){
//		
//		$sql = "SELECT *"
//		     . " FROM   " . $this->table
//		     . " WHERE  colum1 = '" . $this->db->escapeString( $colum1 ) . "' 
//			AND colum2 = '" . . $this->db->escapeString( $colum2 ) . . "'";
//		return $this->db->sqlGetRecord( $sql ); 
//	}
	
	
}
?>