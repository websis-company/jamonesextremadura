<?php
class eCommerce_SDO_Core_DAO_OrderFee extends eCommerce_SDO_Core_DAO {
	
	protected $table = 'order_fee';
	protected $id_field = 'order_fee_id';
	
	public function __construct(){
		parent::__construct();
		$this->setSearchFields( 
			array( 'description', 'amount' )
		);
	}

}
?>