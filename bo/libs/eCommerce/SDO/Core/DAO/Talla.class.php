<?php
class eCommerce_SDO_Core_DAO_Talla extends eCommerce_SDO_Core_DAO {
	
	protected $table = 'talla';
	protected $id_field = 'talla_id';
	
	public function __construct(){
		parent::__construct( new eCommerce_SDO_Core_DB() );
		$this->setSearchFields( array( "nombre") );
	}
	
	public function GetAllTallasByCategoriaId($categoria_id){		
		$sql = "SELECT t.* FROM (((talla t
				LEFT JOIN relaciontalla rt ON rt.talla_id = t.talla_id)
				LEFT JOIN version v ON v.version_id = rt.tabla_id)
				LEFT JOIN product p ON p.product_id = v.product_id)
				LEFT JOIN product_category pc ON p.product_id = pc.product_id
				WHERE ".$categoria_id."
				GROUP BY t.talla_id";
		//echo $sql;
		return $this->db->sqlGetResult( $sql );
	}
		
	public function GetAllTallasByBrandId($brand_id){
		$sql = "SELECT t.* FROM `talla` t
				INNER JOIN relaciontalla rt ON t.talla_id = rt.talla_id
				INNER JOIN product p ON p.brand_id IS NOT NULL
				WHERE p.brand_id = ".$brand_id."
				GROUP BY t.talla_id
				ORDER BY t.talla_id ASC";
		return $this->db->sqlGetResult( $sql );
	}
	
}
?>