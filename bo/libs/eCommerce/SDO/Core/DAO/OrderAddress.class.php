<?php
class eCommerce_SDO_Core_DAO_OrderAddress extends eCommerce_SDO_Core_DAO {
	
	protected $table = 'order_address';
	protected $id_field = 'order_address_id';
	
	public function __construct(){
		parent::__construct();
		$this->setSearchFields( 
			array( 'street', 'city', 'state', 'country', 'zip_code' )
		);
	}

}
?>