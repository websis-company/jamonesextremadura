<?php
class eCommerce_SDO_Core_DAO_Galeria extends eCommerce_SDO_Core_DAO {
	
	protected $table = 'galeria';
	protected $id_field = 'galeria_id';
	
	public function __construct(){
		parent::__construct( new eCommerce_SDO_Core_DB() );
		$this->setSearchFields( array( "galeria_id"," nombre") );
	}
	
//	public function loadByColum1AndOtherColum2( $email ){
//		
//		$sql = "SELECT *"
//		     . " FROM   " . $this->table
//		     . " WHERE  colum1 = '" . $this->db->escapeString( $colum1 ) . "' 
//			AND colum2 = '" . . $this->db->escapeString( $colum2 ) . . "'";
//		return $this->db->sqlGetRecord( $sql ); 
//	}
	

	public function imageRandom($id_category){
		$sql = "SELECT * FROM  " . $this->table." WHERE galerycategory_id = ".$id_category." ORDER BY RAND() LIMIT 1";
		return $this->db->sqlGetResult( $sql ); 
	}


	public function getByCategory($id_category){
		$sql = "SELECT * FROM  " . $this->table." WHERE galerycategory_id = ".$id_category." LIMIT 0,4";
		return $this->db->sqlGetResult( $sql ); 
	}

	public function loadDestacados(){
		$sql = "SELECT * FROM  " . $this->table." WHERE destacado = 'Si'";
		return $this->db->sqlGetResult( $sql ); 	
	}
	
}
?>