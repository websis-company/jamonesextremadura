<?php
class eCommerce_SDO_Core_DAO_Address extends eCommerce_SDO_Core_DAO {
	
	protected $table = 'address';
	protected $id_field = 'profile_id';

		/**
	 * @see DB_DAO_GenericDAO::saveNew()
	 *
	 * @param array $arrEvent
	 * @return unknown
	 */
	public function saveNew( array &$arrEvent ){
		throw new Exception( __METHOD__ . 'Method not available for this DAO ' . __CLASS__ . '. Use SaveOrUpdate() instead');
	}

	/**
	 * @see DB_DAO_GenericDAO::saveOrUpdate()
	 *
	 * @param unknown_type $arrEvent
	 * @return unknown
	 */
	public function saveOrUpdate( &$arrEvent ){
		$fields = $this->prepareFields( $arrEvent );

		$sql = "REPLACE INTO `" . $this->table . "` SET " . implode( ",\n ", $fields ) . "";
		$affectedRows = $this->db->sqlExecute( $sql );
		return $affectedRows;
	}

	/**
	 * @see DB_DAO_GenericDAO::update()
	 *
	 * @param unknown_type $arrEvent
	 * @return unknown
	 */
	public function update( &$arrEvent ){
		throw new Exception( __METHOD__ . 'Method not available for this DAO ' . __CLASS__ . '. Use SaveOrUpdate() instead');
	}
	
}
?>