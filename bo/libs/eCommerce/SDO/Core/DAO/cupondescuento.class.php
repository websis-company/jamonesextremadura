<?php
class eCommerce_SDO_Core_DAO_cupondescuento extends eCommerce_SDO_Core_DAO {
	
	protected $table = 'cupondescuento';
	protected $id_field = 'cupondescuento_id';
	
	public function __construct(){
		parent::__construct( new eCommerce_SDO_Core_DB() );
		$this->setSearchFields( array( "cupon") );
	}
	
//	public function loadByColum1AndOtherColum2( $email ){
//		
//		$sql = "SELECT *"
//		     . " FROM   " . $this->table
//		     . " WHERE  colum1 = '" . $this->db->escapeString( $colum1 ) . "' 
//			AND colum2 = '" . . $this->db->escapeString( $colum2 ) . . "'";
//		return $this->db->sqlGetRecord( $sql ); 
//	}

	public function loadByCuponName($cupon){
		$sql = "SELECT * FROM ".$this->table." WHERE cupon = '".$cupon."' AND status = 'Activo'";
		//return $this->db->sqlGetResult( $sql );
		return $this->db->sqlGetRecord( $sql );
	}
	
	
}
?>