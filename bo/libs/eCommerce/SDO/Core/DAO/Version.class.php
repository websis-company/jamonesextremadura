<?php
class eCommerce_SDO_Core_DAO_Version extends eCommerce_SDO_Core_DAO {
	
	protected $table = 'version';
	protected $id_field = 'version_id';
	
	public function __construct(){
		parent::__construct( new eCommerce_SDO_Core_DB() );
		$this->setSearchFields( array( "titulo") );
	}
	
	public function getAllVersiones($product_id){
		$sql = "SELECT v.*,c.color_id,c.nombre as colornombre, t.talla_id, t.nombre as tallanombre FROM version v
			LEFT JOIN relacioncolor rc ON rc.tabla_id = v.version_id
			LEFT JOIN color c ON c.color_id = rc.color_id
			LEFT JOIN relaciontalla rt ON rt.tabla_id = v.version_id
			LEFT JOIN talla t ON t.talla_id = rt.talla_id
			WHERE v.product_id = ".$product_id." AND v.status = 'Active' GROUP BY v.modelo ORDER BY v.orden ASC";			

		return $this->db->sqlGetResult( $sql );
	}

	public function getAllVersionesTodo($product_id){
		$sql = "SELECT v.*,c.color_id,c.nombre as colornombre, t.talla_id, t.nombre as tallanombre FROM version v
			LEFT JOIN relacioncolor rc ON rc.tabla_id = v.version_id
			LEFT JOIN color c ON c.color_id = rc.color_id
			LEFT JOIN relaciontalla rt ON rt.tabla_id = v.version_id
			LEFT JOIN talla t ON t.talla_id = rt.talla_id
			WHERE v.product_id = ".$product_id." AND v.status = 'Active' GROUP BY v.version_id";

		
		return $this->db->sqlGetResult( $sql );
	}

	public function getAllVersionesColor($product_id,$modelo){
		$sql = "SELECT v.version_id,v.price,c.color_id,c.nombre as colornombre FROM version v
		LEFT JOIN relacioncolor rc ON rc.tabla_id = v.version_id
		LEFT JOIN color c ON c.color_id = rc.color_id	
		WHERE v.product_id = ".$product_id ." AND v.status = 'Active' AND v.modelo = '".$modelo."' GROUP BY c.color_id";
		
		return $this->db->sqlGetResult( $sql );
	}
	
	public function getAllVersionesTalla($product_id,$modelo){
		$sql = "SELECT v.version_id,v.price,t.talla_id, t.nombre as tallanombre FROM version v
		LEFT JOIN relaciontalla rt ON rt.tabla_id = v.version_id
		LEFT JOIN talla t ON t.talla_id = rt.talla_id
		WHERE v.product_id = ".$product_id ." AND v.status = 'Active' AND v.modelo = '".$modelo."' GROUP BY t.talla_id";
		return $this->db->sqlGetResult( $sql );
	}
	
	public function getVersionTalla($version_id){
		$sql = 'SELECT talla.* FROM talla
				INNER JOIN relaciontalla ON talla.talla_id = relaciontalla.talla_id
				INNER JOIN version ON version.version_id = relaciontalla.tabla_id
				WHERE version.version_id ='.$version_id;
		return $this->db->sqlGetResult( $sql );
	}
	
	public function GetTallasByProductId($product_id){
		$sql = 'SELECT talla.*,version.version_id FROM talla
				INNER JOIN relaciontalla ON talla.talla_id = relaciontalla.talla_id
				INNER JOIN version ON version.version_id = relaciontalla.tabla_id
				WHERE version.product_id ='.$product_id." GROUP BY talla.talla_id ORDER BY version.version_id ASC";
		return $this->db->sqlGetResult( $sql );
	}
	
	public function GetColoresByProductId($product_id){
		$sql = 'SELECT color.*,version.version_id FROM color
				INNER JOIN relacioncolor ON color.color_id = relacioncolor.color_id
				INNER JOIN version ON version.version_id = relacioncolor.tabla_id
				WHERE version.product_id ='.$product_id." GROUP BY color.color_id";
		return $this->db->sqlGetResult( $sql );
	}
	
	public function GetTallasByVersionId($version_id){
		$sql = 'SELECT talla.*,version.version_id FROM talla
				INNER JOIN relaciontalla ON talla.talla_id = relaciontalla.talla_id
				INNER JOIN version ON version.version_id = relaciontalla.tabla_id
				WHERE version.version_id ='.$version_id." GROUP BY talla.talla_id";
		return $this->db->sqlGetResult( $sql );
	}
	
	public function GetColoresByVersionId($version_id){
		$sql = 'SELECT color.*,version.version_id FROM color
				INNER JOIN relacioncolor ON color.color_id = relacioncolor.color_id
				INNER JOIN version ON version.version_id = relacioncolor.tabla_id
				WHERE version.version_id ='.$version_id." GROUP BY color.color_id";
		return $this->db->sqlGetResult( $sql );
	}
	
	public function LoadVersionByColorTalla($color_id = NULL, $talla_id = NULL, $product_id){
		if(!empty($color_id)){		
			$strInnerColor = ' INNER JOIN relacioncolor ON relacioncolor.tabla_id = version.version_id ';
			$strWhereColor = ' AND relacioncolor.color_id ='.$color_id;
		}
		if(!empty($talla_id)){		
			$strInnerTalla = ' INNER JOIN relaciontalla ON relaciontalla.tabla_id = version.version_id ';
			$strWhereTalla = ' AND relaciontalla.talla_id ='.$talla_id;
		}
		$sql = 'SELECT version.* FROM version '.$strInnerTalla.$strInnerColor.'				
				INNER JOIN product ON product.product_id = version.product_id
				WHERE product.product_id = '.$product_id.$strWhereTalla.$strWhereColor;
		
		return $this->db->sqlGetRecord( $sql );
	}
	

}
?>