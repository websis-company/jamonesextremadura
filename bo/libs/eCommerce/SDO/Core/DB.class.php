<?php
class eCommerce_SDO_Core_DB extends DB {

	public function __construct(){
		if ( SERVER_DEVELOPMENT ){
			$dsn = "mysql://user_general:123456Abc@192.168.100.9/vicom_jamonextremadura"; //Acceso Remoto
		} 
		elseif ( SERVER_DEMO ){
			$dsn = "mysql://vicom_print:zN8N7OLABelL@localhost/vicom_printproyect";
			//throw new eCommerce_SDO_Core_DB_Exception( 'DB Connection for DEMO server not defined yet.', 1 );
		} 
		elseif ( SERVER_PRODUCTION ){
			$dsn = "mysql://printpro_print:zN8N7OLABelL@localhost/printpro_printproyect";
			
		} 
		else{
			$dsn = "";
		}
		parent::__construct( $dsn . "?new_link=true" );
		$this->db->setCharset( CHARSET_PROJECT );
	}

}
?>