<?php
class eCommerce_SDO_Core_IO_FileApplication extends eCommerce_SDO_Core_Application{
	
	
	
	public function __construct(){
		parent::__construct();
		
	}
	
	public function getIOFileApplication( $type ='all'){
		//we need to verify if we return the object or we redirect to other place
		$dao = self::GetDAO();
		$id = empty($_REQUEST['id']) || !is_numeric($_REQUEST['id']) ? 0 : $_REQUEST['id'];
		
		$file = $dao->loadById( $id );
		$result = self::CheckPermission($file);
		
		if( ($result || empty($file["access_type"]) ||  $file["access_type"] =='public') 
		&& $file["access_type"] !='private' ){
			if( $type =='image' ){
				return new IO_ImageFileApplication( new eCommerce_SDO_Core_DB() );
			}
			else{
				return new IO_FileApplication( new eCommerce_SDO_Core_DB() );
			}
		}else{
			
			header("HTTP/1.0 404 Not Found");
			die();
		}
	}
	
	public static function GetAccessType( $type ){
		$validAccess = array('private','public','Entrevista','Circular','Menu Almuerzo','Calendario Almuerzo','Noticia Evento','Reglamento','Calendario Mensual');
		return !in_array($type,$validAccess) ? $validAccess[0] : $type;
	}
	
	protected static function CheckPermission($file){
		$result = false;
		$user = eCommerce_FrontEnd_Util_UserSession::GetIdentity();
		
		switch( $file["access_type"] ){
			case 'Entrevista':
				$search = new eCommerce_Entity_Search('','fecha DESC',1,1);
				$extraConditions = array();
				$extraConditions[] = array( "SQL"=>$file["id"] . " IN (entrevista.array_images)");
				$result = eCommerce_SDO_Core_Application_Entrevista::Search( $search, $extraConditions );
		
				$entrevistas = $result->getResults();
				
				
				$entrevistas = $entrevistas->getIterator();
				$entrevista = $entrevistas->current();
		
				if( !empty($entrevista) ){
						//and the user is login
						$result = (($entrevista->getMiembroId() == $user->getUserId()) && !empty($user));
				}
			break;
			default:
				//if the user has login correctly then 
				$result = !empty($user);
			break;
		}
		if( !empty($user)){
			$result = ($result==false) ? $user->getRole() == 'Admin' : $result;
		}
		return $result;
	}
	
	
	public static function GetDAO(){
		return new DB_DAO_FileDAO( new eCommerce_SDO_Core_DB() );
	}
}


?>