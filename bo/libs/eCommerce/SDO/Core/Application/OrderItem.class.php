<?php
/**
 * This class provides access to manipulation of Order Items in the System.
 * 
 * @author Alejandro Rivera <alejandro@vicomstudio.com>
 */
class eCommerce_SDO_Core_Application_OrderItem extends eCommerce_SDO_Core_Application {
	
	const ATTRIBUTE_VALUE_TOKEN = ':';
	
	public static function CreateOrderItem($order,$product,$cartItem,$realPrice,$discount){
		$order_id_exp = explode('|',$cartItem->getProductId());
		
		$color = $order_id_exp[2];
		$talla = $order_id_exp[3];
		$version = $order_id_exp[1];
		$version = eCommerce_SDO_Core_Application_Version::LoadById($version);
		
		$orderItem = new eCommerce_Entity_Order_Item();
		$orderItem->setOrderId(  $order->getOrderId() );
		$orderItem->setSku(      $version->getSku() );
		$orderItem->setDiscount( $discount);
		$orderItem->setQuantity( $cartItem->getQuantity() );
		$orderItem->setColor( $color );
		$orderItem->setTalla( $talla );
		
		$orderItem->setVersionId( $version->getVersionId());
		
		$orderItem->setProductId( $product->getProductId() );
		
		//$orderItem->setPrice(eCommerce_SDO_Core_Application_CurrencyManager::ValueInActualCurrency( $version->getPrice(), $version->getCurrency() ));
		$orderItem->setPrice(eCommerce_SDO_Core_Application_CurrencyManager::ValueInActualCurrency( $realPrice, $version->getCurrency() ));
		$orderItem->setAttributes($cartItem->getAttributes());
		$orderItem->setGaleriaId($cartItem->getGaleriaId());
		$orderItem->setFileId($cartItem->getFileId());
		
		return $orderItem;
	}
	
	public static function SetAttributes( $att ){
		$txt = '';
		$ban = false;
		foreach($att as $key =>$value){
			$txt .= ($ban ? eCommerce_SDO_Core_Application_Cart::PRINCIPAL_TOKEN : '') . $key . self::ATTRIBUTE_VALUE_TOKEN .$value;
			$ban = true;
		}
		return $txt;
	}
	
		
	/**
	 * Load the Order Item from the given ID. Note an empty object is returned if the ID doesn't exist
	 *
	 * @param int $address
	 * @return eCommerce_Entity_Order_Item
	 */
	public static function LoadById( $orderItemId ){
		
		$dao = self::GetDAO();
		$entity = $dao->loadById( $orderItemId, true );       
		$item = new eCommerce_Entity_Order_Item();   // Create new clean object to prevent old values being conserved
		$item = eCommerce_SDO_Core_Util_EntityManager::
		            ParseArrayToObject( $entity, $item );  // Convert array to to Object
		return $item;
	}
	
	/**
	 * Saves a new or existing Order Item into the System (validation is part of the process )
	 *
	 * @param eCommerce_Entity_Order_Item $item - The object to be saved
	 * @return eCommerce_Entity_Order_Item         - The recently saved object
	 * @throws eCommerce_SDO_Core_Validator_Exception, SQLException
	 */
	public static function Save( eCommerce_Entity_Order_Item $item ){
		// 1. Validate object
		$validator = new eCommerce_SDO_Core_Validator_OrderItem( $item );
		$validator->validate();
		
		if ( !$validator->isValid() ){
			throw new eCommerce_SDO_Core_Validator_Exception( $validator ); 
		}
		
		// 2. Save or Update Object
		$entity = new ArrayObject( $item );        // Convert Object to Array
		
		$dao = self::GetDAO();
		$dao->saveOrUpdate( $entity );
		
		// 3. Retrieve record from DB
		$item = self::LoadById( $entity[ 'order_item_id' ] );
		if ( $item == new eCommerce_Entity_Order_Item() ){

			throw new Exception( "The Order Item saved could not be retrieved." );      
		}
		// 4. Return recently saved object
		return $item;
	}
	
	/**
	 * Deletes an Order Address from the System
	 *
	 * @param int $orderItemId     - The ID of the Order Address to be deleted
	 * @return eCommerce_Entity_Order_Item - The recently deleted Order Address
	 * @throws eCommerce_SDO_Core_Application_Exception - When the order is not found or couldn't be deleted
	 */
	public static function Delete( $orderItemId ){
		// 1. Ensure the order actually exists
		$item = self::LoadById( $orderItemId );
		
		if ( $item == new eCommerce_Entity_Order_Item() ){
			throw new eCommerce_SDO_Core_Application_Exception( 'The order item to be deleted doesn\'t exist.' );
		}
		else {
			try {
				// 2. Delete the Product
				$dao = self::GetDAO();
				$dao->delete( $orderItemId );
				
				// 3. Return the recently deleted Product
				return $item;
			}
			catch ( SQLException $e ){
				throw new eCommerce_SDO_Core_Application_Exception( 'There was an unexpected error while trying to delete the Order Item', 0, $e );
			}
		}
	}
	
	/**
	 * Retrieves the appropriate DAO
	 *
	 * @return eCommerce_SDO_Core_DAO_OrderItem
	 */
	protected static function GetDAO(){
		return new eCommerce_SDO_Core_DAO_OrderItem();
	}
	
	/**
	 * Retrieves all the Orders Items available for a given Order
	 * 
	 * @return eCommerce_Entity_Util_OrderItemArray
	 */
	public static function GetAllByOrder( $orderId, $language =null ){
		
		$search = new eCommerce_Entity_Search();
		$search->setResultsPerPage( -1 ); // ALL results
		
		$result = self::Search( $search, $orderId , $language);
		
		return $result->getResults();
	}
	
	/**
	 * Searches for all the Items where the Order ID matches the one provided.
	 *
	 * @param eCommerce_Entity_Search $search - Search parameters (pagination, keywords, etc.)
	 * @param int $orderId                    - The Order ID or 0/null for the all prder addresses
	 * @return eCommerce_Entity_Search_Result_OrderItem
	 * @throws eCommerce_SDO_Core_Application_Exception
	 */
	public static function Search( eCommerce_Entity_Search $search, $orderId = null, $language = null ){
		try {
			$dao = self::GetDAO();
			if ( !empty( $orderId ) ){
				$dao->addExtraCondition( "order_id = '" . $dao->getDB()->escapeString( $orderId ) . "'" );
			}
			// Retrieve orders
			$arrItems = $dao->loadAllByParameters(
				$search->getKeywords(),
				$search->getSearchAsPhrase(),
				$search->getOrderBy(),
				$search->getPage(),
				$search->getResultsPerPage()
			);
			// Retrieve total pages
			$totalPages = $dao->loadAllByParameters(
				$search->getKeywords(),
				$search->getSearchAsPhrase(),
				$search->getOrderBy(),
				$search->getPage(),
				$search->getResultsPerPage(),
				true
			);
			
			
			$result = new eCommerce_Entity_Search_Result_OrderItem( $search );
			$result->setTotalPages( $totalPages );
			
			$arrObjItems = self::ParseArrayToObjectArray( $arrItems );


			$language = eCommerce_SDO_Core_Application_LanguageManager::GetValidLanguage($language);
			$language = $language["id"];
			
			//we need to validate the two options because we dont know what is the
			//language of the item 
			
			if( $language != eCommerce_SDO_Core_Application_LanguageManager::DEFAULT_LANGUAGE){
				foreach( $arrObjItems as $ObjItem){
					$productId = $ObjItem->getProductId();
					$productInLanguage = eCommerce_SDO_Core_Application_ProductInDifferentLanguage::GetProductInLanguage($productId, $language);
					$name = $productInLanguage->getName();
					$ObjItem->setName( $name );
					$arrObjItems[ $ObjItem->getOrderItemId() ] = $ObjItem;
				}
			}else{
				foreach( $arrObjItems as $ObjItem){
					$productId = $ObjItem->getProductId();
					$product = eCommerce_SDO_Core_Application_Product::LoadById($productId);
					$name = $product->getName();
					$ObjItem->setName( $name );
					$arrObjItems[ $ObjItem->getOrderItemId() ] = $ObjItem;
				}
			}
			
			$result->setResults( $arrObjItems ); 
			return $result;
		}
		catch( SQLException $e ){
			throw new eCommerce_SDO_Core_Application_Exception( 
				'Error processing the search request. Please see the nested exception for details',
				0, $e );
		}
	}
	
	/**
	 * @param array $arrItems
	 * @return eCommerce_Entity_Util_OrderItemArray
	 */
	protected static function ParseArrayToObjectArray( array $arrItems ){
		// Create the array that will hold the Product objects
		$objItems = new eCommerce_Entity_Util_OrderItemArray(); 
		foreach( $arrItems as $arrItem ){
			// Transform each array into object
			$objItem = eCommerce_SDO_Core_Util_EntityManager::
			               ParseArrayToObject( $arrItem, new eCommerce_Entity_Order_Item() );
			// Add object to Order array  
			$objItems[ $objItem->getOrderItemId() ] = $objItem;  
		}
		return $objItems;
	}

}
?>