<?php
/**
 * This class provides access to manipulation of Order Fees in the System.
 * 
 * @author Alejandro Rivera <alejandro@vicomstudio.com>
 */
class eCommerce_SDO_Core_Application_OrderFee extends eCommerce_SDO_Core_Application {

	/**
	 * Load the Order Item from the given ID. Note an empty object is returned if the ID doesn't exist
	 *
	 * @param int $address
	 * @return eCommerce_Entity_Order_Item
	 */
	public static function LoadById( $orderFeeId ){
				
		$dao = self::GetDAO();

		$entity = $dao->loadById( $orderFeeId, true );
		       
		$fee = new eCommerce_Entity_Order_Fee();   
		// Create new clean object to prevent old values being conserved

		$fee = eCommerce_SDO_Core_Util_EntityManager::ParseArrayToObject( $entity, $fee );

		// Convert array to to Object
		return $fee;
	}
	
	/**
	 * Saves a new or existing Order Fee into the System (validation is part of the process )
	 *
	 * @param eCommerce_Entity_Order_Fee $item - The object to be saved
	 * @return eCommerce_Entity_Order_Fee         - The recently saved object
	 * @throws eCommerce_SDO_Core_Validator_Exception, SQLException
	 */
	public static function Save( eCommerce_Entity_Order_Fee $fee ){
		// 1. Validate object
		$validator = new eCommerce_SDO_Core_Validator_OrderFee( $fee );
		$validator->validate();
		
		if ( !$validator->isValid() ){
			throw new eCommerce_SDO_Core_Validator_Exception( $validator ); 
		}
		// 2. Save or Update Object
		$entity = new ArrayObject( $fee );        // Convert Object to Array
		
		$dao = self::GetDAO();
		$dao->saveOrUpdate( $entity );
		
		// 3. Retrieve record from DB
		$fee = self::LoadById( $entity[ 'order_fee_id' ] );
		
		if ( $fee->getOrderFeeId() == 0 ){
			throw new Exception( "The Order Fee saved could not be retrieved." );
		}
		// 4. Return recently saved object
		return $fee;
	}
	
	/**
	 * Deletes an Order Address from the System
	 *
	 * @param int $orderItemId     - The ID of the Order Address to be deleted
	 * @return eCommerce_Entity_Order_Item - The recently deleted Order Address
	 * @throws eCommerce_SDO_Core_Application_Exception - When the order is not found or couldn't be deleted
	 */
	public static function Delete( $orderItemId ){
		// 1. Ensure the order actually exists
		$item = self::LoadById( $orderItemId );
		
		if ( $item == new eCommerce_Entity_Order_Item() ){
			throw new eCommerce_SDO_Core_Application_Exception( 'The order item to be deleted doesn\'t exist.' );
		}
		else {
			try {
				// 2. Delete the Product
				$dao = self::GetDAO();
				$dao->delete( $orderItemId );
				
				// 3. Return the recently deleted Product
				return $item;
			}
			catch ( SQLException $e ){
				throw new eCommerce_SDO_Core_Application_Exception( 'There was an unexpected error while trying to delete the Order Item', 0, $e );
			}
		}
	}
	
	/**
	 * Retrieves the appropriate DAO
	 *
	 * @return eCommerce_SDO_Core_DAO_OrderFee
	 */
	protected static function GetDAO(){
		return new eCommerce_SDO_Core_DAO_OrderFee();
	}
	
	/**
	 * Retrieves all the Orders Fees available for a given Order
	 * 
	 * @return eCommerce_Entity_Util_OrderFeeArray
	 */
	public static function GetAllByOrder( $orderId ){
		$search = new eCommerce_Entity_Search();
		$search->setResultsPerPage( -1 ); // ALL results
		$result = self::Search( $search, $orderId );
		return $result->getResults();
	}
	
	/**
	 * Searches for all the Items where the Order ID matches the one provided.
	 *
	 * @param eCommerce_Entity_Search $search - Search parameters (pagination, keywords, etc.)
	 * @param int $orderId                    - The Order ID or 0/null for the all prder addresses
	 * @return eCommerce_Entity_Search_Result_OrderItem
	 * @throws eCommerce_SDO_Core_Application_Exception
	 */
	public static function Search( eCommerce_Entity_Search $search, $orderId = null ){
		try {
			$dao = self::GetDAO();
			if ( !empty( $orderId ) ){
				$dao->addExtraCondition( "order_id = '" . $dao->getDB()->escapeString( $orderId ) . "'" );
			}
			// Retrieve orders
			$arrItems = $dao->loadAllByParameters(
				$search->getKeywords(),
				$search->getSearchAsPhrase(),
				$search->getOrderBy(),
				$search->getPage(),
				$search->getResultsPerPage()
			);
			// Retrieve total pages
			$totalPages = $dao->loadAllByParameters(
				$search->getKeywords(),
				$search->getSearchAsPhrase(),
				$search->getOrderBy(),
				$search->getPage(),
				$search->getResultsPerPage(),
				true
			);
			
			$result = new eCommerce_Entity_Search_Result_OrderFee( $search );
			$result->setTotalPages( $totalPages );
			$result->setResults( self::ParseArrayToObjectArray( $arrItems ) ); 
			return $result;
		}
		catch( SQLException $e ){
			throw new eCommerce_SDO_Core_Application_Exception( 
				'Error processing the search request. Please see the nested exception for details',
				0, $e );
		}
	}
	
	/**
	 * @param array $arrFees
	 * @return eCommerce_Entity_Util_OrderFeeArray
	 */
	protected static function ParseArrayToObjectArray( array $arrFees ){
		// Create the array that will hold the Product objects
		$objFees = new eCommerce_Entity_Util_OrderFeeArray(); 
		foreach( $arrFees as $arrFee ){
			// Transform each array into object
			$objFee = eCommerce_SDO_Core_Util_EntityManager::
			               ParseArrayToObject( $arrFee, new eCommerce_Entity_Order_Fee() );
			// Add object to Fees array  
			//$objFees[ $objFee->getOrderItemId() ] = $objFee;  
			$objFees[ $objFee->getOrderFeeId() ] = $objFee;
		}
		return $objFees;
	}

}
?>