<?php
class eCommerce_SDO_Core_Application_OrderMediation extends eCommerce_SDO_Core_Application {

	public static function CreateOrderFullFromOrderId( $orderId, $language = null){
		try{
			$orderFull = new eCommerce_Entity_Order_Full();
			$order     = eCommerce_SDO_Core_Application_Order::LoadById( $orderId );
			$items     = eCommerce_SDO_Core_Application_OrderItem::GetAllByOrder( $order->getOrderId(), empty($language) ? $order->getLanguage() : $language );
			$addresses = eCommerce_SDO_Core_Application_OrderAddress::GetAllByOrder( $order->getOrderId() );
			$fees      = eCommerce_SDO_Core_Application_OrderFee::GetAllByOrder( $order->getOrderId() );

			$orderFull->setOrderId(          $order->getOrderId() );
			$orderFull->setProfileId(        $order->getProfileId() );
			$orderFull->setPaymentMethod(    $order->getPaymentMethod() );
			$orderFull->setStatus(           $order->getStatus() );
			$orderFull->setComments(         $order->getComments() );
			$orderFull->setCreationDate(     $order->getCreationDate() );
			$orderFull->setModificationDate( $order->getModificationDate() );
			$orderFull->setDescuento( $order->getDescuento() );
			$orderFull->setCurrency( $order->getCurrency() );
			$orderFull->setEnviado( $order->getEnviado() );
			// Associate items
			$orderFull->setItems( $items );
			// Associate addresses
			$orderFull->setAddresses( $addresses );
			// Associate fees
			$orderFull->setFees( $fees );
			return $orderFull;
		}
		catch (Exception $e){
			throw $e;
		}
	}

	/**
	 * Creates a new purchase based on the Cart's Items and Addresses.
	 *
	 * @param int $profileId
	 * @param string $comments
	 * @return eCommerce_Entity_Order_Full
	 */
	public static function CreateOrderFromCart( $profileId, $comments = null, $paymentMethod ='', $probableDepositDate = null  ){
		$db = new eCommerce_SDO_Core_DB();
		$db->beginTransaction();
		try {
			$order     = self::CreateOrder( $profileId, $comments, $paymentMethod );
			$items     = self::CreateOrderItems( $order );
			$addresses = self::CreateOrderAddresses( $order );
			$total = eCommerce_SDO_Cart::GetTotalCart(false, false);
			$total = $total['price']['amount'];	
			
			$fees = self::CreateExtras( $order, true,  $total, 0);
			$db->commit();
			// Create a new object that will hold ALL the information for this purchase
			$orderFull = new eCommerce_Entity_Order_Full();
			$orderFull->setOrderId(          $order->getOrderId() );
			$orderFull->setProfileId(        $order->getProfileId() );
			$orderFull->setPaymentMethod(    $order->getPaymentMethod() );
			$orderFull->setStatus(           $order->getStatus() );
			$orderFull->setComments(         $order->getComments() );
			$orderFull->setCreationDate(     $order->getCreationDate() );
			$orderFull->setModificationDate( $order->getModificationDate() );
			$orderFull->setDescuento( $order->getDescuento() );
			$orderFull->setCurrency( $order->getCurrency() );
			$orderFull->setTipoTarjeta($order->getTipoTarjeta());

			//$orderFull->setRequiereFactura( $order->getRequiereFactura() );
			// Associate items
			$orderFull->setItems( $items );
			// Associate addresses
			$orderFull->setAddresses( $addresses );
			// Associate fees
			$orderFull->setFees( $fees );
			return $orderFull;
		}
		catch( Exception $e ){
			$db->rollback();
			throw $e;
		}
	}

	/**
	 * @param int $profileId
	 * @param string $comments
	 * @return eCommerce_Entity_Order
	 */
	public static function CreateOrder( $profileId, $comments = null, $paymentMethod = ''){
		$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
		if ($actualLanguage == 'EN')
		{
			$lang = 0;
		}
		else
		{
			$lang = 1;
		}
		$name[0] = "An order cannot be created from an empty cart.";
		$name[1] = "No se puede crear una orden de un carrito vac&iacute;o";
		// Prevent the creation of an order from an empty cart.
		if ( count( eCommerce_SDO_Core_Application_Cart::GetCart()->getItems() ) == 0 ){
			throw new eCommerce_SDO_Core_Application_Exception( $name[$lang] );
		}
		// Create the Order object to be persisted.
		$order = new eCommerce_Entity_Order();
		$order->setProfileId( $profileId );
		$order->setStatus( eCommerce_SDO_Core_Application_Order::GetValidStatus( 'N' ) );
		$order->setEnviado('No');
		$order->setComments( $comments );
		$order->setPaymentMethod( $paymentMethod );
		$language = eCommerce_SDO_Core_Application_LanguageManager::GetActualLanguage();
		$order->setLanguage($language);
		//we obtain the actual currency
		$order->setCurrency( eCommerce_SDO_Core_Application_CurrencyManager::GetActualCurrency() );
		$order = eCommerce_SDO_Core_Application_Order::Save( $order );
		return $order;
	}

	/**
	 * @param eCommerce_Entity_Order $order
	 * @return eCommerce_Entity_Util_OrderItemArray
	 */
	public static function CreateOrderItems( eCommerce_Entity_Order $order,$realPrice=null){
		$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
		if ($actualLanguage == 'EN')
		{
			$lang = 0;
		}
		else
		{
			$lang = 1;
		}
		$msg01[0] = "Product in Cart with ID";
		$msg01[1] = "El producto con el ID ";
		$msg02[0] = "does not exist anymore";
		$msg02[1] = "Ya no existe en la base de datos";
		$msg03[0] = "Could not create order item for the Product with ID ";
		$msg03[1] = "No se pudo crear una orden para el producto con ID ";
		$msg04[0] = "See details for more information";
		$msg04[1] = "Vea los detalles para m&aacute;s informaci&oacute;n";
		// Create the array that will hold all the saved order items
		$items = new eCommerce_Entity_Util_OrderItemArray();
		for ( $it = eCommerce_SDO_Core_Application_Cart::GetCart()->getItems()->getIterator(); $it->valid(); $it->next() ){
			// Retrieve the cart Item for which we will create the Order Item	
			$cartItem = $it->current();
			try {
				// Retrieve the product details from the DB
				$product = eCommerce_SDO_Core_Application_Product::LoadById( eCommerce_SDO_Core_Application_Cart::GetRealProductId($cartItem->getProductId()) );
				if ( $product->getProductId() == 0 ){
					throw new eCommerce_SDO_Core_Application_Exception( $msg01[$lang] . $cartItem->getProductId() . $msg02[$lang] );
				}
				// Create the actual Order Item with the information from the product and the cart item
				//$orderItem = eCommerce_SDO_Core_Application_OrderItem::CreateOrderItem($order,$product,$cartItem);
				$orderItem = eCommerce_SDO_Core_Application_OrderItem::CreateOrderItem($order,$product,$cartItem,$cartItem->getPrice(),$cartItem->getDiscount());
				//load the product name in the actual language
				$language = eCommerce_SDO_Core_Application_LanguageManager::GetActualLanguage();
				if( $language != eCommerce_SDO_Core_Application_LanguageManager::DEFAULT_LANGUAGE ){
					$id= $product->getProductId();
				$product = eCommerce_SDO_Core_Application_ProductInDifferentLanguage::GetByIdAndLanguage( $id, $language);					
				}
				$orderItem->setName( $product->getName() );
				$orderItem = eCommerce_SDO_Core_Application_OrderItem::Save( $orderItem );
				$items[] = $orderItem;
			}
			catch( Exception $e ){
				// An exception occurred (Application_Exception, SQLException, etc.). Encapsulate exception and re-throw
				throw new eCommerce_SDO_Core_Application_Exception( $msg03[$lang] . $cartItem->getProductId() . $msg04[$lang], 102, $e );
			}
		}
		return $items;
	}

	/**
	 * @param eCommerce_Entity_Order $order
	 * @return eCommerce_Entity_Util_OrderAddressArray
	 */
	public static function CreateOrderAddresses( eCommerce_Entity_Order $order ){
		$addresses = new eCommerce_Entity_Util_OrderAddressArray();
		for( $it = eCommerce_SDO_Core_Application_Cart::GetCart()->getAddresses()->getIterator(); $it->valid(); $it->next() ){			
			$address = $it->current();
			$address->setOrderId( $order->getOrderId() );
			$address = eCommerce_SDO_Core_Application_OrderAddress::Save( $address );
			$addresses[ $address->getOrderAddressId() ] = $address;
		}
		return $addresses;
	}

	public static function CreateExtras( eCommerce_Entity_Order $order, $saveInDB = true, $total, $precioEnvio = NULL){ 
		$fees = new eCommerce_Entity_Util_OrderFeeArray();
		//$fees[] = self::CreateTaxesFee( $order, $saveInDB );
		$fees[] = self::CreateShippingFee( $order, $saveInDB, $total, $precioEnvio );
		//add order extra fee
		//$fees[] = 
		return $fees;
	}

	public static function GetExtras($total, $precioEnvio = NULL){ 
		return self::CreateExtras( new eCommerce_Entity_Order(), false, $total ,$precioEnvio );
	}

	protected static function CreateShippingFee( eCommerce_Entity_Order $order, $save, $total , $precioEnvio = NULL){ 		
		$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
		if ($actualLanguage == 'EN'){
			$lang = 0;
		}
		else{
			$lang = 1;
		}
		$msg01[0] = "Shipping and Handling";
		$msg01[1] = "Env&iacute;o y manejo ";		
		$variable = 100;
		$amount = eCommerce_SDO_Core_Application_OrderBusinessRules::GetPriceEnvio( $total, $precioEnvio);				
		$shipping = new eCommerce_Entity_Order_Fee();
		$shipping->setDescription( $msg01[$lang] );
		$shipping->setAmount( $amount );
		if( $save ){
			$shipping->setOrderId( $order->getOrderId() );
			$shipping = eCommerce_SDO_Core_Application_OrderFee::Save( $shipping );
		}
		return $shipping;
	}


	/**
	 * @param eCommerce_Entity_Order $order
	 * @return eCommerce_Entity_Order_Fee
	 */
	protected static function CreateTaxesFee( eCommerce_Entity_Order $order, $save ){
		$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
		if ($actualLanguage == 'EN')
		{
			$lang = 0;
		}
		else
		{
			$lang = 1;
		}
		$msg01[0] = "Export Taxes";
		$msg01[1] = "Impuestos de exportaci&oacute;n";
		$amount = eCommerce_SDO_Core_Application_OrderBusinessRules::CalculateExportTaxes(
			eCommerce_SDO_Core_Application_Cart::GetCart()
		);
		$shipping = new eCommerce_Entity_Order_Fee();
		$shipping->setDescription( $msg01[$lang] );
		$shipping->setAmount( $amount );
		if( $save ){
			$shipping->setOrderId( $order->getOrderId() );
			$shipping = eCommerce_SDO_Core_Application_OrderFee::Save( $shipping );
		}
		return $shipping;
	}

	public static function GetExtrasTotal($total, $precioEnvio = NULL){
		$fees = self::GetExtras($total, $precioEnvio);
		$subtotal = 0;
		foreach( $fees as $fee){			
			$subtotal += $fee->getAmount();			
		}
		return $subtotal;
	}

}
?>