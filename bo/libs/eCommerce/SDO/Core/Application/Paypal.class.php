<?php
class eCommerce_SDO_Core_Application_Paypal extends eCommerce_SDO_Core_Application {
	protected $data = array();	
	protected $order_id;
	protected $path;
	protected $order;
	public function __construct( $order_id ) {
		parent::__construct();
		$config = Config::getGlobalConfiguration();
		$this->order_id = $order_id;
		$this->order = eCommerce_SDO_Core_Application_OrderMediation::CreateOrderFullFromOrderId( $this->order_id );

		#-- Totales
		foreach ($this->order->getItems() as $item) {
			$total     = $subTotal += $item->getPrice() * $item->getQuantity();
			$descuento += $item->getDiscount() * $item->getQuantity();
		}

		#-- Muestra descuento
		if(!is_null($this->order->getDescuento())){
			$descuento =  $subTotal * $order->getDescuento();
			$total_orden = ($total - $descuento);
		}else{	
			$total_orden =$total;
		}//end if

		if( $config["paypal_test"] ){
			$this->path = ABS_HTTP_URL;
			$this->data['cancel_return'] = $this->path;
			$this->data['return'] = $this->path;		
			$this->data['notify_url'] = $this->path . $config["paypal_notify_url"];
			$this->data['action'] = 'https://www.sandbox.paypal.com/cgi-bin/webscr';			
			$this->data['cpp_header_image'] = $config["paypal_cpp_header_image"];
			$this->data['business'] = $config['paypal_test_business'];		
		}else{
			$this->path = ABS_HTTP_URL;
			$this->data['action'] = 'https://www.paypal.com/cgi-bin/webscr';
			$this->data['cancel_return'] = $this->path;
			$this->data['return'] = $this->path;		
			$this->data['notify_url'] = $this->path . $config["paypal_notify_url"];
			$this->data['cpp_header_image'] = $config["paypal_cpp_header_image"];
			$this->data['business'] = $config['paypal_business'];		
		}
		$this->config();
		$this->loadOrder($total_orden);
	}

	public function loadOrder($total_orden){		 
		$this->data['item_number'] = $this->order->getOrderId();				
		$this->data['amount'] = $total_orden;
		//$this->data['amount'] = 1;
		$profile = eCommerce_SDO_User::LoadUserProfile( $this->order->getProfileId() );
		$this->data['email'] = $profile->getEmail();
		$this->data['first_name'] = $profile->getFirstName();
		$this->data['last_name'] = $profile->getLastName();		
		$this->data['currency_code'] = $this->order->getCurrency();
		$addresses = $this->order->getAddresses();
		$address  = $addresses[ eCommerce_SDO_Order::ADDRESS_SHIP_TO ];		
		if(!empty($address)){		
		$this->data['address1'] = $address->getStreet();
		$this->data['address2'] = $address->getStreet2();
		$this->data['city'] = $address->getCity();
		$this->data['country'] = $address->getCountry();
		$this->data['state'] = $address->getState();
		$this->data['zip'] = $address->getZipCode();		
		//list($this->data['night_phone_a'],$this->data['night_phone_b']) = sscanf($user['telephone'], "%3s%7s"); 
		}
	}
	public function getHtml(){			
	//$("#numero").animate({"font-size": "+=20px"}, 400).animate({"font-size": "-=20px"}, 400).html(n);
		//
		$config = Config::getGlobalConfiguration();
		$html  =
			'<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
			<script language="javascript">
				function conteo(){
					n = parseInt($("#numero").html()) - 1;
					$("#numero").html(n);					
					if(n <= 0){
						clearInterval(intervalo);
						//document.paypal.submit();
						$("#btnSubmit").trigger("click");
					}
				}
				$(document).ready(
					function(){
						intervalo = setInterval("conteo()",1000);
					}
				);
			</script>'
			.'<div class="contenedor" style="margin-top:30px" align="center">
	<div class="rows" style="margin-left:0px">
    	<section id="contenido" >'.			
			'<form action="' .$this->data['action'] . '" method="post" name="paypal" id="formPayment">';
		$html .=  $this->getHiddenFields();
		$html .= '<input id="btnSubmit" type="image" src="'.$config["paypal_input_image"].'" border="0" name="submit" alt="Make payments with PayPal - it\'s fast, free and secure!" />
		<p class="gris16">Redirigiendo ...<span id="numero">10</span> segundos.</p>
		'		
			  . '</form>
			  		</section>
			</div>
			</div>'
			  ;
		return $html;
	}	
	public function getHiddenFields(){
		$html = '';
		foreach($this->data as $key => $value){			
			 $html .= '<input type="hidden" name="' .$key. '" value="' .$value. '" />';
		}		
		return $html;
	}
	protected function config(){
		$config = Config::getGlobalConfiguration();
		$this->data['item_name'] = $config["paypal_item_name"] . ' - Id. de Orden: ' . $this->order_id;		
		$this->data['cmd'] = $config["paypal_cmd"];
		$this->data['undefined_quantity'] = 1;
		$this->data['no_shipping'] = 1;
		$this->data['no_note'] = 1;
		$this->data['tax'] = 0;
		$this->data['charset'] = 'utf-8';
		//$this->data['lc'] = 'US';
	}
	public function getId(){
		return $this->order_id;		
	}
	public function setReturn($return){
		$this->data['return'] = $return;		
	}
}
?>