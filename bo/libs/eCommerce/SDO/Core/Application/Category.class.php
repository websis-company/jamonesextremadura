<?php
/**
 * This class provides access to manipulation of Categories in the System.
 * 
 * @author Alejandro Rivera <alejandro@vicomstudio.com>
 */
class eCommerce_SDO_Core_Application_Category extends eCommerce_SDO_Core_Application {
	const CATEGORY_SCULPTURES_ID = 21;
	const HTACCES_CATEGORY = 'productos/categorias/{$category_id}/{$category_name}.html';
	const HTACCES_CATEGORY_PAGINATION = 'productos/categorias/{$category_id}/{$category_name}/{$counter}.html';
	
	
	/**
	 * Load the Category from the given ID. Note an empty object is returned if the ID doesn't exist
	 *
	 * @param int $categoryId
	 * @return eCommerce_Entity_Catalog_Category
	 */
	public static function LoadById( $categoryId ){
		
		$dao = self::GetDAO();
		$entity = $dao->loadById( $categoryId, true );       
		$category = new eCommerce_Entity_Catalog_Category();   // Create new clean object to prevent old values being conserved
		$category = eCommerce_SDO_Core_Util_EntityManager::ParseArrayToObject( $entity, $category );  // Convert array to to Object
		return $category;
	}
	
	/**
	 * Saves a new or existing Category into the System (validation is part of the process )
	 *
	 * @param eCommerce_Entity_Catalog_Category $category - The object to be saved
	 * @return eCommerce_Entity_Catalog_Category           - The recently saved object
	 * @throws eCommerce_SDO_Core_Validator_Exception, SQLException
	 */
	public static function Save( eCommerce_Entity_Catalog_Category $category ){
		// 1. Validate object
		$validator = new eCommerce_SDO_Core_Validator_Category( $category );
		$validator->validate();
		
		if ( !$validator->isValid() ){
			throw new eCommerce_SDO_Core_Validator_Exception( $validator ); 
		}


		
		// 2. Save or Update Object
		$entity = new ArrayObject( $category );        // Convert Object to Array
		$dao = self::GetDAO();
		$dao->saveOrUpdate( $entity );
		
		// 3. Retrieve record from DB
		$category = self::LoadById( $entity[ 'category_id' ] );
		if ( $category == new eCommerce_Entity_Catalog_Category() ){
			throw new Exception( "The Category saved could not be retrieved." );      
		}
		// 4. Return recently saved object
		return $category;
		
	}
	
	/**
	 * Deletes a Category from the System
	 *
	 * @param int $categoryId                    - The ID of the category to be deleted
	 * @return eCommerce_Entity_Catalog_Category - The recently deleted category
	 * @throws eCommerce_SDO_Core_Application_Exception - When the category is not found or couldn't be deleted
	 */
	public static function Delete( $categoryId ){
		// 1. Ensure the category actually exists
		$category = self::LoadById( $categoryId );
		
		if ( $category == new eCommerce_Entity_Catalog_Category() ){
			throw new eCommerce_SDO_Core_Application_Exception( 'The category to be deleted doesn\'t exist.' );
		}
		else {
			try {
				// 2. Delete the Category
				$dao = self::GetDAO();
				$dao->delete( $categoryId );
				
				// 3. Return the recently deleted Category
				return $category;
			}
			catch ( SQLException $e ){
				throw new eCommerce_SDO_Core_Application_Exception( 'There was an unexpected error while trying to delete the Category', 0, $e );
			}
		}
	}
	
	/**
	 * Retrieves the appropriate DAO
	 *
	 * @return eCommerce_SDO_Core_DAO_Category
	 */
	protected static function GetDAO(){
		return new eCommerce_SDO_Core_DAO_Category();
	}
	
	/**
	 * Retrieves all the Categories available.
	 * 
	 * @return eCommerce_Entity_Util_CategoryArray
	 */
	public static function GetAll(){
		$dao = self::GetDAO();
		$arrCategories = $dao->loadAll();                           // Load all categories as a 2D array
		$objCategories = self::ParseArrayToObjectArray( $arrCategories );
		return $objCategories;
	}

	public static function GetSomeCategorys($excluir){
		$dao = self::GetDAO();
		$arrCategories = $dao->GetSomeCategorys($excluir);

		$objCategories = self::ParseArrayToObjectArray( $arrCategories );
		return $objCategories;
	}

	public static function GetParentCategory($categoriaActual){
		$dao = self::GetDAO();
		$arrCategories = $dao->getParentCategory($categoriaActual);
		$objCategories = self::ParseArrayToObjectArray( $arrCategories );
		return $objCategories;
	}
	
	
	/**
	 * Performs a paged search for Categories
	 *
	 * @param eCommerce_Entity_Search $search
	 * @return eCommerce_Entity_Search_Result_Category
	 * @throws eCommerce_SDO_Core_Application_Exception
	 */
	public static function Search( eCommerce_Entity_Search $search ){
		try {
			$dao = self::GetDAO();
			// Retrieve categories
			$arrCategories = $dao->loadAllByParameters(
				$search->getKeywords(),
				$search->getSearchAsPhrase(),
				$search->getOrderBy(),
				$search->getPage(),
				$search->getResultsPerPage()
			);
			// Retrieve total pages
			$totalPages = $dao->loadAllByParameters(
				$search->getKeywords(),
				$search->getSearchAsPhrase(),
				$search->getOrderBy(),
				$search->getPage(),
				$search->getResultsPerPage(),
				true
			);
			
			$result = new eCommerce_Entity_Search_Result_Category( $search );
			$result->setTotalPages( $totalPages );
			$result->setResults( self::ParseArrayToObjectArray( $arrCategories ) ); 
			return $result;
		}
		catch( SQLException $e ){
			throw new eCommerce_SDO_Core_Application_Exception( 
				'Error processing the search request. Please see the nested exception for details',
				0, $e );
		}
	}
	
	/**
	 * Searches for all the categories where the Parent Category ID matches the one provided
	 *
	 * @param eCommerce_Entity_Search $search - Search parameters (pagination, keywords, etc.)
	 * @param int $parentCategoryId           - The Parennt Category ID or 0/null for the top categories
	 * @return eCommerce_Entity_Search_Result_Category
	 * @throws eCommerce_SDO_Core_Application_Exception
	 */
	public static function SearchByParentCategoryId( eCommerce_Entity_Search $search, $parentCategoryId = null ){
		try {
			$dao = self::GetDAO();
			if ( empty( $parentCategoryId ) ){
				$dao->addExtraCondition( "parent_category_id IS NULL or parent_category_id = 0" );
				//$dao->addExtraCondition( "status = 'activo'" );
			}
			else {
				$dao->addExtraCondition( "parent_category_id = '" . $dao->getDB()->escapeString( $parentCategoryId ) . "'" );
				//$dao->addExtraCondition( "status = 'activo'" );
			}
			// Retrieve categories
			$arrCategories = $dao->loadAllByParameters(
				$search->getKeywords(),
				$search->getSearchAsPhrase(),
				$search->getOrderBy(),
				$search->getPage(),
				$search->getResultsPerPage()
			);
			// Retrieve total pages
			$totalPages = $dao->loadAllByParameters(
				$search->getKeywords(),
				$search->getSearchAsPhrase(),
				$search->getOrderBy(),
				$search->getPage(),
				$search->getResultsPerPage(),
				true
			);
			
			$result = new eCommerce_Entity_Search_Result_Category( $search );
			$result->setTotalPages( $totalPages );
			$result->setResults( self::ParseArrayToObjectArray( $arrCategories ) ); 
			return $result;
		}
		catch( SQLException $e ){
			throw new eCommerce_SDO_Core_Application_Exception( 
				'Error processing the search request. Please see the nested exception for details',
				0, $e );
		}
	}
	
	/**
	 * @param array $arrCategories
	 * @return eCommerce_Entity_Util_CategoryArray
	 */
	protected static function ParseArrayToObjectArray( array $arrCategories ){
		// Create the array that will hold the Category objects
		$objCategories = new eCommerce_Entity_Util_CategoryArray(); 
		foreach( $arrCategories as $arrCategory ){
			// Transform each array into object
			$objCategory = eCommerce_SDO_Core_Util_EntityManager::
			               ParseArrayToObject( $arrCategory, new eCommerce_Entity_Catalog_Category() );
			// Add object to category array  
			$objCategories[ $objCategory->getCategoryId() ] = $objCategory;  
		}
		return $objCategories;
	}

	public function parseHTACCESLink( $args = array(), $type = ''){
		switch( $type ){
			case 'pagination':
			default:
				$return = self::HTACCES_CATEGORY_PAGINATION;
			break;
		}
		foreach($args as $arg => $value){
			$return = str_replace($arg, $value, $return);
		}
		//debug($return);
		return $return;
	}
	
public static function getFriendlyNameUrl($objId,$p = 1, $menu = ''){
		/*$obj 		= self::LoadById($objId);
		
		$url_ids 	= '';
		$url_names 	= '';
		$ban 		= true;
			
		$url_names = Util_String::validStringForUrl(self::GetImplodeCategories($objId,'/',$menu) . "/",false,false);

		$url_ids = "$objId/";
		
		$url = ABS_HTTP_URL . "{$url_ids}productos/{$url_names}/$p/";
		return strtolower($url);*/

		$obj        = self::LoadById($objId);
		$p_obj      = self::LoadById($obj->getParentCategoryId());
		$p_category = $p_obj->getName();
		$url_ids   = '';
		$url_names = '';
		$ban       = true;

		$s_category = $obj->getName();
		$url_names = Util_String::validStringForUrl($s_category . "/",false,false);

		if(!empty($p_category)){
			$url_names .= "/".Util_String::validStringForUrl($p_category . "/",false,false);
		}

		$url_ids   = "$objId/";	
		$url = ABS_HTTP_URL . "{$url_ids}producto/{$url_names}/$p/";
		return strtolower($url);

	}
	
public static function GetImplodeCategories($category_id, $delimit = ',', $menu = ''){
		$dao = self::GetDAO();
		return $dao->GetImplodeCategories($category_id, $delimit, $menu);
	}

public static function GetCategoryStatusArray($options = false){
		$ArrStatus = array();	
		$db = new eCommerce_SDO_Core_DB();	
		//we create the instance here because eCommerce/SDO/Core
		//is restringed to access directly for the FrontEnd
		$daoProduct = self::GetDAO();	
		$enumvalues = $db->sqlGetRecord( "SHOW COLUMNS FROM " . $daoProduct->getTable() . " LIKE 'status'" );	
		//$enumvalues[ "type" ] contents text similar like
		// enum( 'value1', 'value2' ... 'valueN' ) we need only the values
		// 'value1', 'value2' ... 'valueN'
		$enumvalues = substr( $enumvalues[ "type" ], strlen('enum(')  , -1 );	
		$enumvalues = explode(',', $enumvalues);	
		foreach( $enumvalues as $enumvalue ){
			//$enumvalue contents text similar
			// 'valueN' we need only valueN
			if($options)$ArrStatus[substr( $enumvalue, 1, -1 )] = substr( $enumvalue, 1, -1 );
			else $ArrStatus[] = substr( $enumvalue, 1, -1 );				
		}
		return $ArrStatus;
	}

	public static function getCategoriaPadre($categoria){
		$model_c = self::LoadById($categoria);
		$parent  = $model_c->getParentCategoryId();

		if($parent != 2){
			self::getCategoriaPadre($parent);
			//break(1);	
		}else{
			return $parent;
		}
	}


	
	
}
?>