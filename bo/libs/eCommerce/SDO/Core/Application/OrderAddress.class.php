<?php
/**
 * This class provides access to manipulation of Order Addresses in the System.
 * 
 * @author Alejandro Rivera <alejandro@vicomstudio.com>
 */
class eCommerce_SDO_Core_Application_OrderAddress extends eCommerce_SDO_Core_Application {
	
	/**
	 * Load the Order Address from the given ID. Note an empty object is returned if the ID doesn't exist
	 *
	 * @param int $address
	 * @return eCommerce_Entity_Order
	 */
	public static function LoadById( $orderAddressId ){
		
		$dao = self::GetDAO();
		$entity = $dao->loadById( $orderAddressId, true );      
		 
		$address = new eCommerce_Entity_Order_Address();   // Create new clean object to prevent old values being conserved
		
		$address = eCommerce_SDO_Core_Util_EntityManager::
		            ParseArrayToObject( $entity, $address );  // Convert array to to Object
		            
		return $address;
	}
	
	/**
	 * Saves a new or existing Order Address into the System (validation is part of the process )
	 *
	 * @param eCommerce_Entity_Order_Address $address - The object to be saved
	 * @return eCommerce_Entity_Order_Address         - The recently saved object
	 * @throws eCommerce_SDO_Core_Validator_Exception, SQLException
	 */
	public static function Save( eCommerce_Entity_Order_Address $address ){
		
		// 1. Validate object
		$validator = new eCommerce_SDO_Core_Validator_OrderAddress( $address );
		
		
		$validator->validate();
		
		if ( !$validator->isValid() ){
			throw new eCommerce_SDO_Core_Validator_Exception( $validator ); 
		}
		
		// 2. Save or Update Object
		$entity = new ArrayObject( $address );        // Convert Object to Array
		
		$dao = self::GetDAO();
		
		$dao->saveOrUpdate( $entity );
		
		// 3. Retrieve record from DB
		
		$address = self::LoadById( $entity[ 'order_address_id' ] );
		if ( $address == new eCommerce_Entity_Order_Address() ){

			throw new Exception( "The Order Address saved could not be retrieved." );      
		}
		// 4. Return recently saved object
		return $address;
	}
	
	/**
	 * Deletes an Order Address from the System
	 *
	 * @param int $orderAddressId     - The ID of the Order Address to be deleted
	 * @return eCommerce_Entity_Order - The recently deleted Order Address
	 * @throws eCommerce_SDO_Core_Application_Exception - When the order is not found or couldn't be deleted
	 */
	public static function Delete( $orderAddressId ){
		// 1. Ensure the order actually exists
		$address = self::LoadById( $orderAddressId );
		
		if ( $address == new eCommerce_Entity_Order_Address() ){
			throw new eCommerce_SDO_Core_Application_Exception( 'The order address to be deleted doesn\'t exist.' );
		}
		else {
			try {
				// 2. Delete the Product
				$dao = self::GetDAO();
				$dao->delete( $orderAddressId );
				
				// 3. Return the recently deleted Product
				return $address;
			}
			catch ( SQLException $e ){
				throw new eCommerce_SDO_Core_Application_Exception( 'There was an unexpected error while trying to delete the Order Address', 0, $e );
			}
		}
	}
	
	/**
	 * Retrieves the appropriate DAO
	 *
	 * @return eCommerce_SDO_Core_DAO_OrderAddress
	 */
	protected static function GetDAO(){
		return new eCommerce_SDO_Core_DAO_OrderAddress();
	}
	
	/**
	 * Retrieves all the Orders Addresses available.
	 * 
	 * @return eCommerce_Entity_Util_OrderAddressArray
	 */
	public static function GetAllByOrder( $orderId ){
		$search = new eCommerce_Entity_Search();
		$search->setResultsPerPage( -1 ); // ALL results
		$result = self::Search( $search, $orderId );
		
		return $result->getResults();
	}
	
	/**
	 * Searches for all the Address where the Order ID matches the one provided.
	 *
	 * @param eCommerce_Entity_Search $search - Search parameters (pagination, keywords, etc.)
	 * @param int $orderId                    - The Order ID or 0/null for the all prder addresses
	 * @return eCommerce_Entity_Search_Result_OrderAddress
	 * @throws eCommerce_SDO_Core_Application_Exception
	 */
	public static function Search( eCommerce_Entity_Search $search, $orderId = null,  $extraConditions = null ){
		try {
			$dao = self::GetDAO();
			if ( !empty( $orderId ) ){
				$dao->addExtraCondition( "order_id = '" . $dao->getDB()->escapeString( $orderId ) . "'" );
			}
			if ( !empty( $extraConditions ) && is_array($extraConditions) ){
					
				foreach( $extraConditions as $extraCondition){
					if( !empty( $extraCondition["value"] ) && !empty($extraCondition["columName"]) ){
						$condition = $extraCondition["columName"] . " = ";
						if( empty($extraCondition["SQL"]) && empty($extraCondition['isInteger']) || $extraCondition['isInteger'] === false){
							$condition .="'";
						}
			
						$condition .= $dao->getDB()->escapeString( $extraCondition['value'] );
			
						if( empty($extraCondition['isInteger']) || $extraCondition['isInteger'] === false){
							$condition .="'";
						}
			
						$dao->addExtraCondition( $condition );
					}
					else {
						$dao->addExtraCondition( $extraCondition["SQL"] );
					}
				}
			}
			// Retrieve orders
			$arrAddresses = $dao->loadAllByParameters(
				$search->getKeywords(),
				$search->getSearchAsPhrase(),
				$search->getOrderBy(),
				$search->getPage(),
				$search->getResultsPerPage()
			);
			// Retrieve total pages
			$totalPages = $dao->loadAllByParameters(
				$search->getKeywords(),
				$search->getSearchAsPhrase(),
				$search->getOrderBy(),
				$search->getPage(),
				$search->getResultsPerPage(),
				true
			);
			$result = new eCommerce_Entity_Search_Result_OrderAddress( $search );
			$result->setTotalPages( $totalPages );
			$result->setResults( self::ParseArrayToObjectArray( $arrAddresses ) );
			 
			return $result;
		}
		catch( SQLException $e ){
			throw new eCommerce_SDO_Core_Application_Exception( 
				'Error processing the search request. Please see the nested exception for details',
				0, $e );
		}
	}
	
	/**
	 * @param array $arrAddresses
	 * @return eCommerce_Entity_Util_OrderAddressArray
	 */
	protected static function ParseArrayToObjectArray( array $arrAddresses ){
		// Create the array that will hold the Product objects
		$objAddresses = new eCommerce_Entity_Util_OrderAddressArray(); 
		foreach( $arrAddresses as $arrAddress ){
			// Transform each array into object
			$objAddress = eCommerce_SDO_Core_Util_EntityManager::
			               ParseArrayToObject( $arrAddress, new eCommerce_Entity_Order_Address() );
			// Add object to Order array  
			//$objAddresses[ $objAddress->getOrderAddressId() ] = $objAddress;  
			$objAddresses[ $objAddress->getType() ] = $objAddress;
		}
		
		return $objAddresses;
	}
	
	public static function GetAddressByEmail($email = NULL){
		$arr = array();
		$search = new eCommerce_Entity_Search('','order_address_id DESC',1,1);
		$extraCond = array();
		$extraCond[] = array( "SQL"=>"email = '".$email."'");
		$extraCond[] = array( "SQL"=>"type = 'Bill To'");
		
		$registers = self::Search( $search, null, $extraCond)->getResults();
			
		return $registers;
	}
	

}
?>