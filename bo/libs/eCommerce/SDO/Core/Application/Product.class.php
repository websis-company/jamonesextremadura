<?php
/**
 * This class provides access to manipulation of Products in the System.
 * 
 * @author Alejandro Rivera <alejandro@vicomstudio.com>
 */
class eCommerce_SDO_Core_Application_Product extends eCommerce_SDO_Core_Application {
	const NOT_AVAILABLE = 'Inactive';
	const HTACCES_DETAILS = 'productos/detalles/{$product_id}/{$product_name}.html';
	
	
	/**
	 * Load the Product from the given ID. Note an empty object is returned if the ID doesn't exist
	 *
	 * @param int $productId
	 * @return eCommerce_Entity_Catalog_Product
	 */
	public static function LoadById( $productId, $language = eCommerce_SDO_Core_Application_LanguageManager::DEFAULT_LANGUAGE ){
		
		$dao = self::GetDAO();
		$entity = $dao->loadById( $productId, true );
		
		$product = new eCommerce_Entity_Catalog_Product();   // Create new clean object to prevent old values being conserved
		$product = eCommerce_SDO_Core_Util_EntityManager::
		            ParseArrayToObject( $entity, $product );  // Convert array to to Object
		
		//we need to know in what language return the description etc...
		$language = ( empty($language) ) ? eCommerce_SDO_Core_Application_LanguageManager::GetActualLanguage() : $language;
		if($language != eCommerce_SDO_Core_Application_LanguageManager::DEFAULT_LANGUAGE ){
			//we need to load new language of the product's descriptions, and the others 
			//components
			$productInLang = eCommerce_SDO_Core_Application_ProductInDifferentLanguage::GetProductInLanguage($product->getProductId(), $language);
			$product->setName( $productInLang->getName() );
			$product->setDescriptionShort( $productInLang->getDescriptionShort() );
			$product->setDescriptionLong( $productInLang->getDescriptionLong() );
			$product->setDimensions( $productInLang->getDimensions() );
			$product->setFinished( $productInLang->getFinished() );
			$product->setMaterials( $productInLang->getMaterials() );
			$product->setColors( $productInLang->getColors() );
		}
		return $product;
	}
	
	/**
	 * Saves a new or existing Product into the System (validation is part of the process )
	 *
	 * @param eCommerce_Entity_Catalog_Product $product - The object to be saved
	 * @return eCommerce_Entity_Catalog_Product         - The recently saved object
	 * @throws eCommerce_SDO_Core_Validator_Exception, SQLException
	 */
	public static function Save( eCommerce_Entity_Catalog_Product $product ){
		unset($product->brand_name);
		// 1. Validate object
		$validator = new eCommerce_SDO_Core_Validator_Product( $product );
		$validator->validate();
		
		if ( !$validator->isValid() ){
			throw new eCommerce_SDO_Core_Validator_Exception( $validator ); 
		}
		
		// 2. Save or Update Object
		$entity = new ArrayObject( $product );        // Convert Object to Array
		
		$dao = self::GetDAO();
		$dao->saveOrUpdate( $entity );
		
		// 3. Retrieve record from DB
		$product = self::LoadById( $entity[ 'product_id' ] );
		if ( $product == new eCommerce_Entity_Catalog_Product() ){

			throw new Exception( "The Product saved could not be retrieved." );      
		}
		// 4. Return recently saved object
		return $product;
	}
	
	/**
	 * Deletes a Product from the System
	 *
	 * @param int $productId                    - The ID of the product to be deleted
	 * @return eCommerce_Entity_Catalog_Product - The recently deleted product
	 * @throws eCommerce_SDO_Core_Application_Exception - When the product is not found or couldn't be deleted
	 */
	public static function Delete( $productId ){
		// 1. Ensure the product actually exists
		$product = self::LoadById( $productId );
		
		if ( $product == new eCommerce_Entity_Catalog_Product() ){
			throw new eCommerce_SDO_Core_Application_Exception( 'The product to be deleted doesn\'t exist.' );
		}
		else {
			try {
				// 2. Delete the Product
				$dao = self::GetDAO();
				$dao->delete( $productId );
				
				// 3. Return the recently deleted Product
				return $product;
			}
			catch ( SQLException $e ){
				throw new eCommerce_SDO_Core_Application_Exception( 'There was an unexpected error while trying to delete the Product', 0, $e );
			}
		}
	}
	
	/**
	 * Retrieves the appropriate DAO
	 *
	 * @return eCommerce_SDO_Core_DAO_Product
	 */
	protected static function GetDAO(){
		return new eCommerce_SDO_Core_DAO_Product();
	}
	protected static function GetProductInDifferentLanguageDAO(){
		return new eCommerce_SDO_Core_DAO_ProductInDifferentLanguage();
	}
	/**
	 * Retrieves all the Products available.
	 * 
	 * @return eCommerce_Entity_Util_ProductArray
	 */
	public static function GetAll(){
		$dao = self::GetDAO();
		$arrProducts = $dao->loadAll();                           // Load all products as a 2D array
		$objProducts = self::ParseArrayToObjectArray( $arrProducts );
		return $objProducts;
	}
	
	/**
	 * Searches for all the products where the Parent Product ID matches the one provided.
	 * NOTE: If Category ID is provided, only those products strictly beneath that 
	 * category will be returned. This method does NOT retrieve all the products under the category's 
	 * child categories.
	 *
	 * @param eCommerce_Entity_Search $search - Search parameters (pagination, keywords, etc.)
	 * @param int $categoryId                 - The Category ID or 0/null for the all products
	 * @return eCommerce_Entity_Search_Result_Product
	 * @throws eCommerce_SDO_Core_Application_Exception
	 */
	public static function Search( eCommerce_Entity_Search $search, $categoryId = null, $inCategory = true, $extraConditions = array(), $joins = array() ){
		try {
			$dao = self::GetDAO();
		if( count($joins) > 0 ){
				foreach ($joins as $join){
					$dao->addJoin($join['table'], $join['on'],$join['join_type']);
				}
			}
			
			if ( !empty( $categoryId ) ){
				if ( $inCategory ){
					$dao->addExtraCondition( "pc.category_id = '" . $dao->getDB()->escapeString( $categoryId ) . "'" );
				}
				else {
					// Detect IDs currently in the category
					$tmpSearch = new eCommerce_Entity_Search();
					$tmpSearch->setResultsPerPage( -1 );
					$tmpResult = self::Search( $search, $categoryId );
					$tmpProducts = $tmpResult->GetResults();
					$tmpProductIds = array();
					foreach( $tmpProducts as $id => $prod ){
						$tmpProductIds[] = $id;
					}
					// Exclude those product IDs from the search
					if ( !empty( $tmpProductIds ) ){
						$dao->addExtraCondition( "p.product_id NOT IN ( " . join( ',', $tmpProductIds ) . " ) " );
					}
				}
			}
			if(!empty($extraConditions)) {				
				foreach($extraConditions as $extraCondition) {
					$dao->addExtraCondition( $extraCondition["SQL"] );
				}
			}
			// Retrieve products
			$actualLanguage = eCommerce_SDO_Core_Application_LanguageManager::GetActualLanguage();
			if( $actualLanguage != eCommerce_SDO_LanguageManager::DEFAULT_LANGUAGE ){
				$dao->setLanguage( $actualLanguage );
				$dao->addExtraCondition( "pdL.language_id = '" . $actualLanguage . "'" );
			}

			
			$arrProducts = $dao->loadAllByParameters(
				$search->getKeywords(),
				$search->getSearchAsPhrase(),
				$search->getOrderBy(),
				$search->getPage(),
				$search->getResultsPerPage()
			);
			// Retrieve total pages
			$totalPages = $dao->loadAllByParameters(
				$search->getKeywords(),
				$search->getSearchAsPhrase(),
				$search->getOrderBy(),
				$search->getPage(),
				$search->getResultsPerPage(),
				true
			);
			
			$result = new eCommerce_Entity_Search_Result_Product( $search );
			$result->setTotalPages( $totalPages );
			$result->setResults( self::ParseArrayToObjectArray( $arrProducts ) ); 
			return $result;
		}
		catch( SQLException $e ){
			throw new eCommerce_SDO_Core_Application_Exception( 
				'Error processing the search request. Please see the nested exception for details',
				0, $e );
		}
	}
	
	public static function SearchByArtist( eCommerce_Entity_Search $search, $artistId = null, $ofTheArtist = true ){
		try {
			$dao = self::GetDAO();
			if ( !empty( $artistId ) ){
				if ( $ofTheArtist ){
					$dao->addExtraCondition( "pa.artist_id = '" . $dao->getDB()->escapeString( $artistId ) . "'" );
				}
				else {
					// Detect IDs currently in the category
					$tmpSearch = new eCommerce_Entity_Search();
					$tmpSearch->setResultsPerPage( -1 );
					$tmpResult = self::SearchByArtist( $search, $artistId );
					$tmpProducts = $tmpResult->GetResults();
					$tmpProductIds = array();
					foreach( $tmpProducts as $id => $prod ){
						$tmpProductIds[] = $id;
					}
					// Exclude those product IDs from the search
					if ( !empty( $tmpProductIds ) ){
						$dao->addExtraCondition( "p.product_id NOT IN ( " . join( ',', $tmpProductIds ) . " ) " );
					}
				}
			}
			// Retrieve products
			$actualLanguage = eCommerce_SDO_Core_Application_LanguageManager::GetActualLanguage();
			if( $actualLanguage != eCommerce_SDO_LanguageManager::DEFAULT_LANGUAGE ){
				$dao->setLanguage( $actualLanguage );
				$dao->addExtraCondition( "pdL.language_id = '" . $actualLanguage . "'" );
			}

			
			$arrProducts = $dao->loadAllByParameters(
				$search->getKeywords(),
				$search->getSearchAsPhrase(),
				$search->getOrderBy(),
				$search->getPage(),
				$search->getResultsPerPage()
			);
			// Retrieve total pages
			$totalPages = $dao->loadAllByParameters(
				$search->getKeywords(),
				$search->getSearchAsPhrase(),
				$search->getOrderBy(),
				$search->getPage(),
				$search->getResultsPerPage(),
				true
			);
			
			$result = new eCommerce_Entity_Search_Result_Product( $search );
			$result->setTotalPages( $totalPages );
			$result->setResults( self::ParseArrayToObjectArray( $arrProducts ) ); 
			return $result;
		}
		catch( SQLException $e ){
			throw new eCommerce_SDO_Core_Application_Exception( 
				'Error processing the search request. Please see the nested exception for details',
				0, $e );
		}
	}
	
	public static function SearchByArtistAndCategory( eCommerce_Entity_Search $search, $artistId = null, $categoryId = null, $inCategory = true ){
		try {
			$dao = self::GetDAO();
			if ( !empty( $artistId ) ){
				$dao->addExtraCondition( "pa.artist_id = '" . $dao->getDB()->escapeString( $artistId ) . "'" );
				if ( !empty( $categoryId ) ){
					if ( $inCategory ){
						$dao->addExtraCondition( "pc.category_id = '" . $dao->getDB()->escapeString( $categoryId ) . "'" );
					}
					else {
						// Detect IDs currently in the category
						$tmpSearch = new eCommerce_Entity_Search();
						$tmpSearch->setResultsPerPage( -1 );
						$tmpResult = self::SearchByArtistAndCategory( $search, $artistId, $categoryId );
						$tmpProducts = $tmpResult->GetResults();
						$tmpProductIds = array();
						foreach( $tmpProducts as $id => $prod ){
							$tmpProductIds[] = $id;
						}
						// Exclude those product IDs from the search
						if ( !empty( $tmpProductIds ) ){
							$dao->addExtraCondition( "p.product_id NOT IN ( " . join( ',', $tmpProductIds ) . " ) " );
						}
					}
				}
				
			}
			// Retrieve products
			$actualLanguage = eCommerce_SDO_Core_Application_LanguageManager::GetActualLanguage();
			if( $actualLanguage != eCommerce_SDO_LanguageManager::DEFAULT_LANGUAGE ){
				$dao->setLanguage( $actualLanguage );
				$dao->addExtraCondition( "pdL.language_id = '" . $actualLanguage . "'" );
			}

			
			$arrProducts = $dao->loadAllByParameters(
				$search->getKeywords(),
				$search->getSearchAsPhrase(),
				$search->getOrderBy(),
				$search->getPage(),
				$search->getResultsPerPage()
			);
			// Retrieve total pages
			$totalPages = $dao->loadAllByParameters(
				$search->getKeywords(),
				$search->getSearchAsPhrase(),
				$search->getOrderBy(),
				$search->getPage(),
				$search->getResultsPerPage(),
				true
			);
			
			$result = new eCommerce_Entity_Search_Result_Product( $search );
			$result->setTotalPages( $totalPages );
			$result->setResults( self::ParseArrayToObjectArray( $arrProducts ) ); 
			return $result;
		}
		catch( SQLException $e ){
			throw new eCommerce_SDO_Core_Application_Exception( 
				'Error processing the search request. Please see the nested exception for details',
				0, $e );
		}
	}
	
	/**
	 * Retrieves the list of Status available to products
	 *
	 * @return array<string>
	 * @todo Retrieve the list from the actual DB (enum)
	 */
	public static function GetProductStatusArray($options = false){
		$ArrStatus = array();
		
		$db = new eCommerce_SDO_Core_DB();
		
		//we create the instance here because eCommerce/SDO/Core
		//is restringed to access directly for the FrontEnd
		$daoProduct = self::GetDAO();
		
		$enumvalues = $db->sqlGetRecord( "SHOW COLUMNS FROM " . $daoProduct->getTable() . " LIKE 'status'" );
		
		//$enumvalues[ "type" ] contents text similar like 
		// enum( 'value1', 'value2' ... 'valueN' ) we need only the values
		// 'value1', 'value2' ... 'valueN'
		$enumvalues = substr( $enumvalues[ "type" ], strlen('enum(')  , -1 );
		
		$enumvalues = explode(',', $enumvalues);
		
		foreach( $enumvalues as $enumvalue ){
			//$enumvalue contents text similar 
			// 'valueN' we need only valueN
			if($options)$ArrStatus[substr( $enumvalue, 1, -1 )] = substr( $enumvalue, 1, -1 );
			else $ArrStatus[] = substr( $enumvalue, 1, -1 );
			
		}
		return $ArrStatus;
	}
	
	public static function GetProductNovedadArray($options = false){
		$ArrStatus = array();		
		$db = new eCommerce_SDO_Core_DB();		
		$daoProduct = self::GetDAO();		
		$enumvalues = $db->sqlGetRecord( "SHOW COLUMNS FROM " . $daoProduct->getTable() . " LIKE 'novedad'" );		
		$enumvalues = substr( $enumvalues[ "type" ], strlen('enum(')  , -1 );		
		$enumvalues = explode(',', $enumvalues);		
		foreach( $enumvalues as $enumvalue ){
			if($options)$ArrNovedad[substr( $enumvalue, 1, -1 )] = substr( $enumvalue, 1, -1 );
			else $ArrNovedad[] = substr( $enumvalue, 1, -1 );			
		}
		return $ArrNovedad;
	}
	
	public static function GetProductVendidoArray($options = false){
		$ArrVendido = array();		
		$db = new eCommerce_SDO_Core_DB();		
		$daoProduct = self::GetDAO();		
		$enumvalues = $db->sqlGetRecord( "SHOW COLUMNS FROM " . $daoProduct->getTable() . " LIKE 'mas_vendido'" );		
		$enumvalues = substr( $enumvalues[ "type" ], strlen('enum(')  , -1 );		
		$enumvalues = explode(',', $enumvalues);		
		foreach( $enumvalues as $enumvalue ){
			if($options)$ArrVendido[substr( $enumvalue, 1, -1 )] = substr( $enumvalue, 1, -1 );
			else $ArrVendido[] = substr( $enumvalue, 1, -1 );			
		}
		return $ArrVendido;
	}

public static function GetProductTallaArray($options = false){
		$ArrStatus = array();
		
		$db = new eCommerce_SDO_Core_DB();
		
		//we create the instance here because eCommerce/SDO/Core
		//is restringed to access directly for the FrontEnd
		$daoProduct = self::GetDAO();
		
		$enumvalues = $db->sqlGetRecord( "SHOW COLUMNS FROM " . $daoProduct->getTable() . " LIKE 'talla'" );
		
		//$enumvalues[ "type" ] contents text similar like 
		// enum( 'value1', 'value2' ... 'valueN' ) we need only the values
		// 'value1', 'value2' ... 'valueN'
		$enumvalues = substr( $enumvalues[ "type" ], strlen('enum(')  , -1 );
		
		$enumvalues = explode(',', $enumvalues);
		
		foreach( $enumvalues as $enumvalue ){
			//$enumvalue contents text similar 
			// 'valueN' we need only valueN
			if($options)$ArrTalla[substr( $enumvalue, 1, -1 )] = substr( $enumvalue, 1, -1 );
			else $ArrTalla[] = substr( $enumvalue, 1, -1 );
			
		}
		return $ArrTalla;
	}
	
	/**
	 * @param array $arrProducts
	 * @return eCommerce_Entity_Util_ProductArray
	 */
	protected static function ParseArrayToObjectArray( array $arrProducts ){
		// Create the array that will hold the Product objects
		$objProducts = new eCommerce_Entity_Util_ProductArray(); 
		foreach( $arrProducts as $arrProduct ){
			// Transform each array into object
			$objProduct = eCommerce_SDO_Core_Util_EntityManager::
			               ParseArrayToObject( $arrProduct, new eCommerce_Entity_Catalog_Product() );
			// Add object to product array  
			$objProducts[ $objProduct->getProductId() ] = $objProduct;  
		}
		return $objProducts;
	}

	public function parseHTACCESLink( $argParse = array()){
		$return = self::HTACCES_DETAILS;
		$args = array('{$product_id}', '{$product_name}');
		foreach($args as $arg){
			$value = empty($argParse[ $arg ]) ? '' : $argParse[ $arg ];
			$value = str_replace(" ","_",$value);
			$return = str_replace($arg, $value, $return);
		}
		return ABS_PATH_HTTP . $return;
	}
	
	public function GetProductUnidad() {
		$ArrUnidad = array();
		
		$db = new eCommerce_SDO_Core_DB();
		
		//we create the instance here because eCommerce/SDO/Core
		//is restringed to access directly for the FrontEnd
		$daoProduct = self::GetDAO();
		
		$enumvalues = $db->sqlGetRecord( "SHOW COLUMNS FROM " . $daoProduct->getTable() . " LIKE 'unidad'" );
		
		//$enumvalues[ "type" ] contents text similar like 
		// enum( 'value1', 'value2' ... 'valueN' ) we need only the values
		// 'value1', 'value2' ... 'valueN'
		$enumvalues = substr( $enumvalues[ "type" ], strlen('enum(')  , -1 );
		
		$enumvalues = explode(',', $enumvalues);
		
		foreach( $enumvalues as $enumvalue ){
			//$enumvalue contents text similar 
			// 'valueN' we need only valueN
			$ArrUnidad[] = substr( $enumvalue, 1, -1 );
			
		}
		return $ArrUnidad;
	}
	
	public function GetProductEntrega() {
		$ArrEntrega = array();		
		$ArrEntrega["J"] = "INMEDIATO + ENVIO";	
		$ArrEntrega["A"] = "7 DIAS HABILES+ ENVIO";
		$ArrEntrega["B"] = "14 DIAS  HABILES + ENVIO";	
		$ArrEntrega["C"] = "20 DIAS HABILES + ENVIO";	
		$ArrEntrega["D"] = "25 DIAS HABILES + ENVIO";
		$ArrEntrega["E"] = "30 DIAS HABILES + ENVIO";
		$ArrEntrega["JT"] = "CONSULTAR TIEMPO DE ENTREGA";
		
		return $ArrEntrega;
	}
	
	public static function GetAllProducts($product_id = NULL, $idCat = NULL , $tipo = NULL){
		$arr = array();
		$search = new eCommerce_Entity_Search('','product_id ASC',1,-1);
		$extraCond = array();
		$extraCond[] = array( "SQL"=>"p.status = 'Active'");
		if(!empty($product_id))
			$extraCond[] = array( "SQL"=>"p.product_id != ".$product_id);
		if(!empty($idCat))
			$extraCond[] = array( "SQL"=>"pc.category_id = ".$idCat);
		//if(!empty($tipo))
		//	$extraCond[] = array( "SQL"=>"p.tipo = '".$tipo."'");
	
		$registers = self::Search( $search, null, null, $extraCond)->getResults();
	
		return $registers;
	}


	public static function getFriendlyNameUrl($objId,$idCat){

		$obj      = eCommerce_SDO_Core_Application_Product::LoadById($objId);
		$tipoCat  = '';
		//$category = current(eCommerce_SDO_Catalog::GetCategoriesByProduct($objId, $tipoCat));
		/*if(!$category){
			$tipoCat = 'Superior';
			$category = current(eCommerce_SDO_Catalog::GetCategoriesByProduct($objId, $tipoCat));
		}*/
		//if($category){
			/*echo "<pre>";
			print_r($category);
			echo "</pre>";*/
			//$url_names = Util_String::validStringForUrl(eCommerce_SDO_Catalog::GetImplodeCategories($category->getCategoryId(),'/',$tipoCat) . '/',false,false);
			//$url_names = Util_String::validStringForUrl($category->getName());
		//}
		//$url		= ABS_HTTP_URL . "{$objId}/productos/{$url_names}/{$obj->getFriendlyName()}/";
			
		if($idCat != false){
			$category = eCommerce_SDO_Catalog::LoadCategory($idCat);
			$url_names = Util_String::validStringForUrl($category->getName());	
			$url = ABS_HTTP_URL . "{$objId}/producto/{$category->getCategoryId()}/{$url_names}/{$obj->getFriendlyName()}/";
		}else{
			$url = ABS_HTTP_URL . "{$objId}/producto/{$obj->getFriendlyName()}/";
		}//end if
		return strtolower($url);
	}

	public static function LoadDestacados(){
		$dao    = self::GetDAO();
		$entity = $dao->loadDestacados();
		$objProfiles = self::ParseArrayToObjectArray( $entity );
		return $objProfiles;
	}
	
}
?>