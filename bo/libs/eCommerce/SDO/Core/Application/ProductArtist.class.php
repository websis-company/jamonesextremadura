<?php
/**
 * This class provides access to manipulation of the Product-Artist (M:N) Relationship.
 
 */
class eCommerce_SDO_Core_Application_ProductArtist extends eCommerce_SDO_Core_Application {
	
	/**
	 * @var eCommerce_SDO_Core_DAO_ProductArtist
	 */
	static protected $dao;
	
	/**
	 * Associates a new Product-Artist relation. It inserts a new tuple or replaces an existing one. 
	 *
	 * @param int $artistId
	 * @param int $productId
	 */
	protected static function CreateProductArtistRelation( eCommerce_Entity_Catalog_ProductArtist $relation ){
		
		$validator = new eCommerce_SDO_Core_Validator_ProductArtist( $relation );
		$validator-> validate();
		if ( !$validator->isValid() ){
			throw new eCommerce_SDO_Core_Validator_Exception( $validator );
		}
		else {
			$entity = new ArrayObject( $relation );
			$dao = self::GetDAO();
			try {
				$dao->saveOrUpdate( $entity );
			}
			catch( SQLException $e ){
				throw new eCommerce_SDO_Core_Application_Exception( 
					'Could not create association between Product ' . $relation->getProductId() . 
				  ' and Category ' . $relation->getCategoryId() . '. Please see details.',
					0,		// Exception code 
					$e    // Nested exception
				);
			}
		}
	}
	

	/**
	 * Associates a single Category to the given Product
	 *
	 * @param int $productId
	 * @param int $categoryId
	 * @return eCommerce_Entity_Util_CategoryArray - The list of ALL (not just the recently added) categories belonging to the product
	 * @throws eCommerce_SDO_Core_ValidatoException, Exception
	 * @todo Don't throw the SQLException
	 */
	
	public static function AssociateArtistToProduct( $productId, $artistId ){
		return self::AssociateArtistsToProduct( $productId, array( $artistId ) );
	}
	
	/**
	 * Associates several Categories to a Product in a batch (single transaction) 
	 *
	 * @param int $productId
	 * @param array<int> $categoryIds
	 * @return eCommerce_Entity_Util_CategoryArray - The list of ALL (not just the recently added) categories belonging to the product
	 * @throws eCommerce_SDO_Core_Validator_Exception, SQLException, Exception
	 * @todo Don't throw the SQLException
	 */
	public static function AssociateArtistsToProduct( $productId, array $artistIds ){
		
		$dao = self::GetDAO();
		try {
			$dao->getDB()->beginTransaction();
			
			foreach( $artistIds as $artistId ){
				$prodArt = new eCommerce_Entity_Catalog_ProductArtist( $productId, $artistId );
				self::CreateProductArtistRelation( $prodArt );
			}
			$dao->getDB()->commit();
			return self::GetArtistsByProduct( $productId );
		}
		catch( Exception $e ){
			$dao->getDB()->rollback();
			throw $e;
		}
	}
	
	/**
	 * Updates ALL the categories related to a Product in a batch (single transaction). Read the notes.
	 * NOTE: 
	 *  a) This method REMOVES ALL categories relations currently stored in the DB 
	 *  b) This method INSERTS ALL categories relations in the categories array
	 * If an exception is thrown, the whole transaction is rolled back.
	 *
	 * @param int $productId
	 * @param array $categoryIds
	 * @return eCommerce_Entity_Util_CategoryArray
	 * @throws eCommerce_SDO_Core_Validator_Exception, SQLException, Exception
	 * @todo Don't throw the SQLException
	 */
	public static function UpdateProductArtists( $productId, array $artistIds ){
		$dao = self::GetDAO();
		try {
			$dao->getDB()->beginTransaction();
			// We begin the transaction because if there's a rollback when want the deletion of this 
			// relations to be restored.
			$dao->deleteRelationsToProduct( $productId );
			// There's NO commit here because it's handlded inside AssociateCategoriesToProduct()
			return self::AssociateArtistsToProduct( $productId, $artistIds );
		}
		catch( Exception $e ){
			// Rollback is handled by AssociateCategoriesToProduct()
			throw $e;
		}
	}
	
	/**
	 * Deletes ALL Artists associations to the give Product ID
	 *
	 * @param int $productId
	 * @return void
	 */
	public static function DeleteProductArtistsAssociation( $productId ){
		$dao = self::GetDAO();
		$dao->deleteRelationsToProduct( $productId );
	}
	
	

/**
	 * Returns an array of Categories belonging to the given Product ID
	 *
	 * @param int $productId
	 * @return eCommerce_Entity_Util_CategoryArray
	 */
	public static function GetArtistsByProduct( $productId ){
		$dao = self::GetDAO();
		$arrArtIds = $dao->getProductArtists( $productId );
		
		$objArtistArray = new eCommerce_Entity_Util_ArtistArray();
		foreach( $arrArtIds as $artId ){
			$artist = eCommerce_SDO_Core_Application_Artist::LoadById( $artId );
			$objArtistArray[ $artId ] = $artist;
		}
		return $objArtistArray;
	}
	
	public function GetArtistByProductAndType($productId, $type){
		$artists = self::GetArtistsByProduct($productId);
		$artistReturn = new eCommerce_Entity_Catalog_Artist();
		foreach($artists as $artist){
			if($type == $artist->getType() ){
				$artistReturn = $artist;
			}
		}
		return $artistReturn;
	}
	
	/**
	 * Deletes a particular Product-Category relation
	 *
	 * @param int $productId
	 * @param int $categoryId
	 * @return void
	 */
	public static function DeleteProductArtistAssociation( $productId, $artistId ){
		$dao = self::GetDAO();
		
		$dao->deleteRelation( $productId, $artistId );
	}

	/**
	 * Operations to relate Products with Categories
	 */
	/**
	 * Associates a single Category to the given Product
	 *
	 * @param int $categoryId
	 * @param int $productId
	 * @throws eCommerce_SDO_Core_Validator_Exception, SQLException, Exception
	 * @todo Don't throw the SQLException
	 */
	public static function AssociateProductToArtist( $artistId, $productId ){
		return self::AssociateProductsToArtist( $artistId, array( $productId ) );
	}
	
	/**
	 * Associates several Products to a Category in a batch (single transaction) 
	 *
	 * @param int $categoryId
	 * @param array<int> $productIds
	 * @return eCommerce_Entity_Util_ProductArray - The list of ALL (not just the recently added) products belonging to the category
	 * @throws eCommerce_SDO_Core_Validator_Exception, SQLException, Exception
	 * @todo Don't throw the SQLException
	 */
	public static function AssociateProductsToArtist( $artistId, array $productIds ){
		
		$dao = self::GetDAO();
		try {
			$dao->getDB()->beginTransaction();
			
			foreach( $productIds as $productId ){
				$prodArt = new eCommerce_Entity_Catalog_ProductArtist( $productId, $artistId );
				self::CreateProductArtistRelation( $prodArt );
			}
			$dao->getDB()->commit();
			return self::GetProductsByArtist( $artistId );
		}
		catch( Exception $e ){
			$dao->getDB()->rollback();
			throw $e;
		}
	}
	
	/**
	 * Updates ALL the Products related to a Category in a batch (single transaction). Read the notes.
	 * NOTE: 
	 *  a) This method REMOVES ALL product relations currently stored in the DB 
	 *  b) This method INSERTS ALL product relations in the product array
	 * If an exception is thrown, the whole transaction is rolled back.
	 *
	 * @param int $categoryId
	 * @param array $productIds
	 * @return eCommerce_Entity_Util_ProductArray
	 * @throws eCommerce_SDO_Core_Validator_Exception, SQLException, Exception
	 * @todo Don't throw the SQLException
	 */
	public static function UpdateArtistProducts( $artistId, array $productIds ){
		$dao = self::GetDAO();
		try {
			$dao->getDB()->beginTransaction();
			// We begin the transaction because if there's a rollback when want the deletion of this 
			// relations to be restored.
			$dao->deleteRelationsToArtist( $artistId );
			// There's NO commit here because it's handlded inside AssociateCategoriesToProduct()
			return self::AssociateProductsToArtist( $artistId, $productIds );
		}
		catch( Exception $e ){
			// Rollback is handled by AssociateCategoriesToProduct()
			throw $e;
		}
	}
	
	/**
	 * Deletes ALL Product associations to the give Category ID
	 *
	 * @param int $categoryId
	 * @return void
	 */
	public static function DeleteArtistProductsAssociation( $artistId ){
		$dao = self::GetDAO();
		$dao->deleteRelationsToArtist( $artistId );
	}
	
	/**
	 * Returns an array of Products belonging to the given Category ID
	 *
	 * @param int $categoryId
	 * @return eCommerce_Entity_Util_ProductArray
	 */
	public static function GetProductsByArtist( $artistId ){
		$dao = self::GetDAO();
		$arrProdIds = $dao->getArtistProducts( $artistId );
		
		$objProductArray = new eCommerce_Entity_Util_ProductArray();
		foreach( $arrProdIds as $productId ){
			$product = eCommerce_SDO_Core_Application_Product::LoadById( $productId );
			$objProductArray[ $productId ] = $product;
		}
		return $objProductArray;
	}
		
	/**
	 * @return eCommerce_SDO_Core_DAO_ProductCategory
	 */
	protected static function GetDAO(){
		if ( self::$dao == null ){
			self::$dao = new eCommerce_SDO_Core_DAO_ProductArtist();  
		}
		return self::$dao; 
	}

}
?>