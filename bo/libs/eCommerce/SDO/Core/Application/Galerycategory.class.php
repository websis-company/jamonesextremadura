<?php
/**
 * This class provides access to manipulation of Users in the System.
 * 
 */	  
class eCommerce_SDO_Core_Application_Galerycategory extends eCommerce_SDO_Core_Application {
	/**
	 * Retrieves the appropriate DAO
	 *
	 * @return eCommerce_SDO_Core_DAO_Category
	 */
	protected static function GetDAO(){
		return new eCommerce_SDO_Core_DAO_Galerycategory();
	}
	protected function GetEntitySearchResultObject( $search ){
		return new eCommerce_Entity_Search_Result_Galerycategory( $search );
	}

	protected function GetEntityUtilObjectArray(){
		return new eCommerce_Entity_Util_GalerycategoryArray();
	}
	
	protected function GetEntityObject(){
		return new eCommerce_Entity_Galerycategory();
	}
	
	/*
	protected function GetObjectId( $obj ){
		return $obj->getGalerycategoryId();
	}
	*/
	
	public function GetSDOCoreValidatorObject( $profile ){
		return new eCommerce_SDO_Core_Validator_Galerycategory( $profile );
	}
	
//*****************************************************************************************
//*****************************************************************************************
//*****************************************************************************************

/**
	 * Saves a new or existing Profile into the System (validation is part of the process )
	 *
	 * @param eCommerce_Entity_User $category - The object to be saved
	 * @return eCommerce_Entity_User           - The recently saved object
	 * @throws eCommerce_SDO_Core_Validator_Exception, SQLException
	 */
	//public static function Save( eCommerce_Entity_Press $profile ){
	public static function Save( $profile ){
		// 1. Validate object
		//$validator = new eCommerce_SDO_Core_Validator_User( $profile );
		
		$validator = self::GetSDOCoreValidatorObject( $profile );
		
		$validator->validate();
		
		if ( !$validator->isValid() ){
			throw new eCommerce_SDO_Core_Validator_Exception( $validator ); 
		}
		
		// 2. Save or Update Object
		$entity = new ArrayObject( $profile );        // Convert Object to Array
		$dao = self::GetDAO();
		$dao->saveOrUpdate( $entity );
		
		// 3. Retrieve record from DB
		$idsFields = $dao->getIdFields();
		if(is_array($idsFields)){
			$arrIds = array();
			foreach($idsFields as $idField){
				$arrIds[ $idField ] = $entity[ $idField ];
			}
			$idsFields = $arrIds;
			$profile = self::LoadById( $idsFields );
		}else{
			$profile = self::LoadById( $entity[ $dao->getIdField() ] );
		}
		
		if ( $profile == self::GetEntityObject() ){
			throw new Exception( "El registro guardado no existe." );      
		}
		// 4. Return recently saved object
		return $profile;
		
	}
	/**
	 * Performs a paged search for Categories
	 *
	 * @param eCommerce_Entity_Search $search - The search parameters
	 * @param string $role                    - The role to filter by. Null for all user roles	
	 * @return eCommerce_Entity_Search_Result_Category
	 */
	public static function Search( eCommerce_Entity_Search $search, $extraConditions = null ){
		try {
			$dao = self::GetDAO();
			if ( !empty( $extraConditions ) && is_array($extraConditions) ){
			
					foreach( $extraConditions as $extraCondition){
						if( !empty( $extraCondition["value"] ) && !empty($extraCondition["columName"]) ){
							$condition = $extraCondition["columName"] . " = ";
							if( empty($extraCondition["SQL"]) && empty($extraCondition['isInteger']) || $extraCondition['isInteger'] === false){
								$condition .="'"; 
							}

							$condition .= $dao->getDB()->escapeString( $extraCondition['value'] );

							if( empty($extraCondition['isInteger']) || $extraCondition['isInteger'] === false){
								$condition .="'"; 
							}

							$dao->addExtraCondition( $condition );
						}
					  else {
						$dao->addExtraCondition( $extraCondition["SQL"] );
					  }	
					}
				}
			
			// Retrieve categories
			$arrProfiles = $dao->loadAllByParameters( $search->getKeywords(), $search->getSearchAsPhrase(),
			                                          $search->getOrderBy(), $search->getPage(),
			                                          $search->getResultsPerPage() );
			
			// Retrieve total pages
			$totalPages = $dao->loadAllByParameters( $search->getKeywords(), $search->getSearchAsPhrase(),
			                                         $search->getOrderBy(), $search->getPage(),
			                                         $search->getResultsPerPage(),
			                                         true );
			
			$result = self::GetEntitySearchResultObject( $search );

			
			$result->setTotalPages( $totalPages );
			//debug( $result );
			$result->setResults( self::ParseArrayToObjectArray( $arrProfiles ) ); 
			return $result;
		}
		catch( SQLException $e ){
			throw new eCommerce_SDO_Core_Application_Exception( 
				'Error al procesar la b&uacute;squeda',
				0, $e );
		}
	}

		/**
	 * Searches for all the categories where the Parent Category ID matches the one provided
	 *
	 * @param eCommerce_Entity_Search $search - Search parameters (pagination, keywords, etc.)
	 * @param int $parentCategoryId           - The Parennt Category ID or 0/null for the top categories
	 * @return eCommerce_Entity_Search_Result_Category
	 * @throws eCommerce_SDO_Core_Application_Exception
	 */
	public static function SearchByParentCategoryId( eCommerce_Entity_Search $search, $parentCategoryId = null ){
		try {
			$dao = self::GetDAO();
			if ( empty( $parentCategoryId ) ){
				$dao->addExtraCondition( "parent_category_id IS NULL or parent_category_id = 0" );
				//$dao->addExtraCondition( "status = 'activo'" );
			}
			else {
				$dao->addExtraCondition( "parent_category_id = '" . $dao->getDB()->escapeString( $parentCategoryId ) . "'" );
				//$dao->addExtraCondition( "status = 'activo'" );
			}
			// Retrieve categories
			$arrCategories = $dao->loadAllByParameters(
				$search->getKeywords(),
				$search->getSearchAsPhrase(),
				$search->getOrderBy(),
				$search->getPage(),
				$search->getResultsPerPage()
			);
			// Retrieve total pages
			$totalPages = $dao->loadAllByParameters(
				$search->getKeywords(),
				$search->getSearchAsPhrase(),
				$search->getOrderBy(),
				$search->getPage(),
				$search->getResultsPerPage(),
				true
			);
			
			$result = new eCommerce_Entity_Search_Result_Galerycategory( $search );
			$result->setTotalPages( $totalPages );
			$result->setResults( self::ParseArrayToObjectArray( $arrCategories ) ); 
			return $result;
		}
		catch( SQLException $e ){
			throw new eCommerce_SDO_Core_Application_Exception( 
				'Error processing the search request. Please see the nested exception for details',
				0, $e );
		}
	}



	/**
	 * @param array $arrProfiles
	 * @return eCommerce_Entity_Util_ProfileArray
	 */
	protected static function ParseArrayToObjectArray( array $arrObjects ){
		// Create the array that will hold the Category objects
		$objProfiles = self::GetEntityUtilObjectArray();
		
		foreach( $arrObjects as $arrObject ){
			// Transform each array into object

			$obj = eCommerce_SDO_Core_Util_EntityManager::
			               ParseArrayToObject( $arrObject, self::GetEntityObject() );
			// Add object to category array  
			//$objProfiles[ self::GetObjectId( $obj ) ] = $obj;
			$objProfiles[] = $obj;  
		}
		return $objProfiles;
	}

	

	/**
	 * Load the Profile from the given ID. Note: an empty object is returned if the ID doesn't exist
	 *
	 * @param int $profileId
	 * @return eCommerce_Entity_User
	 */
	public static function LoadById( $profileId ){
		
		$dao = self::GetDAO();
		$entity = $dao->loadById( $profileId, false );
 	
		$profile = self::GetEntityObject();   // Create new clean object to prevent old values being conserved
		$profile = eCommerce_SDO_Core_Util_EntityManager::
		            ParseArrayToObject( $entity, $profile );  // Convert array to Object
		return $profile;
	}
	
/**
	 * Deletes a Category from the System
	 *
	 * @param int $categoryId                    - The ID of the category to be deleted
	 * @return eCommerce_Entity_Catalog_Category - The recently deleted category
	 * @throws eCommerce_SDO_Core_Application_Exception - When the category is not found or couldn't be deleted
	 */
	public static function Delete( $profileId ){
		// 1. Ensure the category actually exists
		
		$profile = self::LoadById( $profileId );
		
		if ( $profile == self::GetEntityObject() ){
			throw new eCommerce_SDO_Core_Application_Exception( 'El registro que desea eliminar no existe.' );
		}
		else {
			try {
				// 2. Delete the Category
				$dao = self::GetDAO();
				$dao->delete( $profileId );
				
				// 3. Return the recently deleted Category
				return $profile;
			}
			catch ( SQLException $e ){
				throw new eCommerce_SDO_Core_Application_Exception( 'Ocurri&oacute; un error inesperado al intentar borrar el registro', 0, $e );
			}
		}
	}
//--------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------
	
	
	
	/**
	 * Load the Profile from the given email. Note: an empty object is returned if the email isn't found
	 *
	 * @param string $email
	 * @return eCommerce_Entity_User
	 */
	public static function LoadByEmail( $email ){
		
		$dao = self::GetDAO();
		$entity = $dao->loadByEmail( $email );
		if ( empty( $entity ) ){
			$entity = $dao->loadById( 0, false );       
		}
		$profile = new eCommerce_Entity_User();   // Create new clean object to prevent old values being conserved
		$profile = eCommerce_SDO_Core_Util_EntityManager::
		            ParseArrayToObject( $entity, $profile );  // Convert array to to Object
		return $profile;
	}

	
	public function getCountGaleryById($id_galery){
		$dao = self::GetDAO();
		$entity = $dao->getCountGaleryById($id_galery);
		return $entity;
	}
	

	/**
	 * Retrieves all the Categories available.
	 * 
	 * @return eCommerce_Entity_Util_CategoryArray
	 */
	public static function GetAll($order_by = ''){
		$dao = self::GetDAO();
		$arrProfiles = $dao->loadAll($order_by);                           // Load all categories as a 2D array
		$objProfiles = self::ParseArrayToObjectArray( $arrProfiles );
		return $objProfiles;
	}
	
	public static function GetAllInArray($includeNull = false){

		$arr = array();
		$search = new eCommerce_Entity_Search('','',1,-1);
		$extraCond = array();
		$registers = self::Search( $search, $extraCond)->getResults();

		/*$r = self::Search( $search, $extraCond)->getResults();

		$ordCatego = array();
		foreach ($registers as $register) {
			if($register->getParentCategoryId() == 0){
				$ordCatego[$register->getGalerycategoryId()] = $register->getName();	
			}//end if
		}//end foreach

		$i = 0;
		foreach ($ordCatego as $key => $value) {
			echo "<pre>";
			var_dump($key);
			echo "</pre>";
			foreach ($r as $re) {
				echo "<pre>";
				var_dump($re);
				echo "</pre>";
				$i++;
			}
			$i++;
		}
		

		die();*/


		if($includeNull){
			$arr[0] = "";
		}

		foreach($registers as $register){
			$arr[ $register->getGalerycategoryId() ] = $register->getName();
		}
		return $arr;
	}

/**
	 * Retrieves the list of Roles available to users
	 *
	 * @return array<string>
	 * @todo Retrieve the list from the actual DB (enum)
	 */
	public static function GetEnumValues( $colum ){
		$ArrRoles = array();
		
		$db = new eCommerce_SDO_Core_DB();
		
		//we create the instance here because eCommerce/SDO/Core
		//is restringed to access directly for the FrontEnd
		$daoUserProfile = self::GetDAO();
		
		$enumvalues = $db->sqlGetRecord( "SHOW COLUMNS FROM " . $daoUserProfile->getTable() . " LIKE '".$colum."'" );
		
		//$enumvalues[ "type" ] contents text similar like 
		// enum( 'value1', 'value2' ... 'valueN' ) we need only the values
		// 'value1', 'value2' ... 'valueN'
		$enumvalues = substr( $enumvalues[ "type" ], strlen('enum(')  , -1 );
		
		$enumvalues = explode(',', $enumvalues);
		
		foreach( $enumvalues as $enumvalue ){
			//$enumvalue contents text similar 
			// 'valueN' we need only valueN
			$ArrRoles[ substr( $enumvalue, 1, -1 ) ] = substr( $enumvalue, 1, -1 );
			
		}
		return $ArrRoles;
	}


	public static function GetCategoriesByParentCategory( $parentCategoryId = null, $search_=null ){
		if ($search_ == 1){
			$q = ($_REQUEST['q'])?$_REQUEST['q']:'';
			$o = ($_REQUEST['orden']=='Z-A')?"name DESC":'orden ASC';
			$k = ($_REQUEST['k'])?$_REQUEST['k']:'-1';
			$search = new eCommerce_Entity_Search($q,$o,1,$k);
		}else{
			$search = new eCommerce_Entity_Search('','orden ASC',1,-1);
		}

		return self::SearchByParentCategoryId( $search, $parentCategoryId );
	}
	

	public static function getFriendlyNameUrl($objId,$p = 1, $menu = ''){
		$obj        = self::LoadById($objId);
		$p_obj      = self::LoadById($obj->getParentCategoryId());
		$p_category = $p_obj->getName();
		$url_ids   = '';
		$url_names = '';
		$ban       = true;

		$s_category = $obj->getName();
		$url_names = Util_String::validStringForUrl($s_category . "/",false,false);

		if(!empty($p_category)){
			$url_names .= "/".Util_String::validStringForUrl($p_category . "/",false,false);
		}

		$url_ids   = "$objId/";	
		$url = ABS_HTTP_URL . "{$url_ids}galeria/{$url_names}/$p/";
		return strtolower($url);
	}

	public static function GetImplodeCategories($category_id, $delimit = ',', $menu = ''){
		$dao = self::GetDAO();
		return $dao->GetImplodeCategories($category_id, $delimit, $menu);
	}
	
	
}
?>