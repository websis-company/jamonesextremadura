<?php
/**
 * This class provides access to manipulation of Orders in the System.
 * 
 * @author Alejandro Rivera <alejandro@vicomstudio.com>
 */
class eCommerce_SDO_Core_Application_Order extends eCommerce_SDO_Core_Application {
	const ADDRESS_SHIP_TO = 'Ship To';
	const ADDRESS_BILL_TO = 'Bill To';
	const INVALID_STATUS = 'IN';
	const INVALID_PAYMENT_METHOD = 'INP';
	/**
	 * Load the Product from the given ID. Note an empty object is returned if the ID doesn't exist
	 *
	 * @param int $orderId
	 * @return eCommerce_Entity_Order
	 */
	public static function LoadById( $orderId ){
		$dao = self::GetDAO();
		$entity = $dao->loadById( $orderId, true );       
		$order = new eCommerce_Entity_Order();   // Create new clean object to prevent old values being conserved
		$order = eCommerce_SDO_Core_Util_EntityManager::
		            ParseArrayToObject( $entity, $order );  // Convert array to to Object
		return $order;
	}
	/**
	 * Saves a new or existing Product into the System (validation is part of the process )
	 *
	 * @param eCommerce_Entity_Order $order - The object to be saved
	 * @return eCommerce_Entity_Order       - The recently saved object
	 * @throws eCommerce_SDO_Core_Validator_Exception, SQLException
	 */
	public static function Save( eCommerce_Entity_Order $order ){
		// 1. Validate object
		$validator = new eCommerce_SDO_Core_Validator_Order( $order );
		$validator->validate();
		if ( !$validator->isValid() ){
			throw new eCommerce_SDO_Core_Validator_Exception( $validator ); 
		}
		//check first status
		$statusPrev ='';
		$orderIdTmp = $order->getOrderId();
		if(!empty( $orderIdTmp )){
			$orderTmp = self::LoadById( $order->getOrderId() );
			$statusPrev = $orderTmp->getStatus();
		} 
		// 2. Save or Update Object
		$entity = new ArrayObject( $order );        // Convert Object to Array
		$dao = self::GetDAO();
		$dao->saveOrUpdate( $entity );
		// 3. Retrieve record from DB
		$order = self::LoadById( $entity[ 'order_id' ] );
		if ( $order == new eCommerce_Entity_Order() ){
			throw new Exception( "The Order saved could not be retrieved." );      
		}
		//SEND EMAIL
		/*if(
		$statusPrev != eCommerce_SDO_Core_Application_Order::GetValidStatus('C') && $order->getStatus() == eCommerce_SDO_Core_Application_Order::GetValidStatus('C') 
		){
			eCommerce_SDO_Core_Application_Inventory::UpdateInventory( $order->getOrderId() );
			self::sendNotificationStatus( $order );
		}else{
			if( $statusPrev == eCommerce_SDO_Core_Application_Order::GetValidStatus('C') 
			&& $order->getStatus() != eCommerce_SDO_Core_Application_Order::GetValidStatus('C')
			){
				eCommerce_SDO_Core_Application_Inventory::UpdateInventoryAdd( $order->getOrderId() );
				self::sendNotificationStatus( $order );
			}
		}*/
		// 4. Return recently saved object
		return $order;
	}

	public function sendNotificationNewOrder( $order,$metodoPago = false, $responseId = false ){
		$config        = Config::getGlobalConfiguration();	
		//$mailer      = new eCommerce_SDO_Core_Application_Mailer();
		$profileId     = $order->getProfileId();		
		$customer      = eCommerce_SDO_User::LoadUserProfile($profileId);
		$customerEmail = $customer->getEmail();		
		//$mailer->addAddress($customerEmail,$customer->getFirstName(), "TO");
		
		$orderLanguage = $order->getLanguage();
		$items         = $order->getItems();
		$user          = eCommerce_SDO_User::LoadUserProfile($order->getProfileId());
		$address       = eCommerce_SDO_User::LoadUserAddress($user->getProfileId());
		$orderFull     = eCommerce_SDO_Order::LoadFullById($order->getOrderId());
		$orderPayment  = eCommerce_SDO_OrderPayment::LoadOrderPayment($responseId);
		$addresses     = $orderFull->getAddresses();
		$usuario       = $addresses[eCommerce_SDO_Cart::ADDRESS_SHIP_TO];
		$factura       = $addresses[eCommerce_SDO_Cart::ADDRESS_BILL_TO];

		if($metodoPago == "en_linea"){
			$tipoPago = 'Pago en l&iacute;nea';
		}elseif($metodoPago == "deposito"){
			$tipoPago = 'Deposito en efectivo';
		}


		if($orderPayment->getState() == "pending" || $orderPayment->getState() == "in_process"){
            $paymentEstatus = 'Confirmaci&oacute;n de pago pendiente';
        }elseif($orderPayment->getState() == "approved"){
            $paymentEstatus = 'Pago confirmado';
        }


		
		$fondo 				= '#FEFEFE';
		$fuente 			= 'Arial, Helvetica';
		$colorfuente 		= '#575758';
		$sizefuente 		= '13px';
		$width 				= '770px';
		$colorlink 			= '#B50A47';
		$imgLogo 			= 'images/layout/logo.png';
		$backgroundImagen	= '#FEFEFE';
		$orderid = $order->getOrderId();
		if($orderLanguage == 'EN'){
			$body = 'Your new order ' . $order->getOrderId() . ' has been created.' .
			'<br /><br /> You can check this order in the following link: <br />
			<a href="' . ABS_HTTP_URL . "/user_info.php?cmd=viewOrder&id=" . $order->getOrderId().'">
			' . ABS_HTTP_URL . "/user_info.php?cmd=viewOrder&id=" . $order->getOrderId().'</a>'
			;
			$subject = 'Your new order ' . $order->getOrderId() . '';
		}else{
				/*$body = 'Su nueva orden de compra ' . $order->getOrderId() . ' ha sido guardada.' .
			'<br /><br /> Ud. puede revisar su orden desde la siguiente liga: <br />
			<a href="' . ABS_HTTP_URL . "/user_info.php?cmd=viewOrder&id=" . $order->getOrderId().'">
			' . ABS_HTTP_URL . "/user_info.php?cmd=viewOrder&id=" . $order->getOrderId().'</a>'
			;*/
			$body='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<title>Untitled Document</title>
				<style type="text/css">				
					<!--
					.style5 {font-size: 12px; font-family: Arial, Helvetica, sans-serif; color: black; font-weight: bold; }
					.style6 {
						font-family: Arial, Helvetica, sans-serif;
						font-weight: bold;
						font-size: 14px;
						color: #000000;
					}
					.style9 {font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: black;  font-weight: normal;}
					.info{font-family: Arial, Helvetica, sans-serif; font-size: 18px; color: black;  font-weight: bold;}
					.general{font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: black;  font-weight: normal;}
					.more{font-family: Arial, Helvetica, sans-serif; font-size: 15px; color:#000000;  font-weight: normal;}
				-->
				</style>
			</head>
			<body>
				<table width="770" border="0" align="center" cellpadding="5" cellspacing="0">
  					<tr>
    					<td colspan="2" bgcolor="'.$backgroundImagen.'">
    						<img id="_x0000_i1025" src="'.ABS_HTTP_URL.$imgLogo.'" />
    					</td>
  					</tr>

  					<tr>
  						<td colspan="2">
  							<table width="100%" border="0" cellspacing="0" cellpadding="0">
  								<tr>
									<td width="385" style="padding: 15px;" valign="top">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class="info" colspan="2">Gracias por tu compra</td>
											</tr>
											<tr>
												<td class="general" colspan="2">Tu pedido ha sido procesado exitosamente</td>
											</tr>
											<tr>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<td>&nbsp;</td>
											</tr>
											<tr class="general" style="color: black;">
												<td>Tu n&uacute;mero de compra</td>
												<td class="more" style="color: #00BEFF !important;">' . $usuario->getOrderId() . '</td>
											</tr>
											<tr class="general" style="color: black;">
												<td>La forma de pago seleccionada</td>
												<td class="more" style="color: #00BEFF !important;">'.$tipoPago.'</td>
											</tr>
										</table>								
									</td>';
								if($metodoPago == 'en_linea'){
								$body .='
									<td width="385" style="padding: 15px; border-left: 1px solid #00BEFF;" valign="top">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class="info" colspan="2">Informaci&oacute;n de pago</td>
											</tr>
											<tr class="general" style="color: black;">
												<td>Estatus</td>
												<td class="more" style="color: #00BEFF; padding-left: 10px;" valign="top">'.$paymentEstatus.'</td>
											</tr>
											<tr class="general" style="color: black;">
												<td>Orden Id</td>
												<td class="more" style="color: #00BEFF !important; padding-left: 10px;">'.$orderPayment->getOrdertransactionid().'</td>
											</tr>
											<tr class="general" style="color: black;"> 
												<td>Transacci&oacute;n Id</td>
												<td class="more" style="color:#00BEFF !important; padding-left: 10px;">'.$orderPayment->getTransactionId().'</td>
											</tr>
											
											<tr>
												<td>&nbsp;</td>
											</tr>';
											if($orderPayment->getState() == "pending" || $orderPayment->getState() == "in_process"){
												$body .='
												<tr class="general" style="color: black;">
													<td colspan="2">Su pago esta en proceso de confirmaci&oacute;n, esto puede demorar hasta 5 d&iacute;as h&aacute;biles, cuando el pago sea procesado se enviar&aacute; un correo de confirmaci&oacute;n.</td>
												</tr>';												
											}

											
											$body .= '								
										</table>								
									</td>';	
								}elseif($metodoPago == 'deposito'){
								$body .='
									<td width="385" style="padding: 15px; border-left: 1px solid #00BEFF;" valign="top">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class="info" colspan="2">Informaci&oacute;n de pago</td>
											</tr>
											<tr class="general" style="color: black;">
												<td>Estatus</td>
												<td class="more" style="color: #00BEFF;">'.$paymentEstatus.'</td>
											</tr>
											<tr class="general" style="color: black;">
												<td>Orden Id</td>
												<td class="more" style="color: #00BEFF;">'.$orderPayment->getOrdertransactionid().'</td>
											</tr>
											<tr class="general" style="color: black;"> 
												<td>Transacci&oacute;n Id</td>
												<td class="more" style="color: #00BEFF;">'.$orderPayment->getTransactionId().'</td>
											</tr>
											
											<tr>
												<td>&nbsp;</td>
											</tr>';
											if($orderPayment->getState() == "pending"){
												$body .='
												<tr class="general" style="color: black;">
													<td colspan="2">Su pago esta en proceso de confirmaci&oacute;n, esto puede demorar hasta 5 d&iacute;as h&aacute;biles, cuando el pago sea procesado se enviar&aacute; un correo de confirmaci&oacute;n.</td>
												</tr>';												
											}

											
											$body .= '
											<tr class="general" style="color: black;">
												<td colspan="2">Imprime tu comprobante de pago <a href="'.$orderPayment->getUrlpayment().'" style="color: #00BEFF;">AQU&Iacute;</a></td>
											</tr>							
										</table>								
									</td>';										
								}

  						$body.='</tr>
  							</table>
  						</td>
  					</tr>
  					';
  				$body .= '
					<tr>
						<td colspan="2" bgcolor="#000000" style="padding: 10px;">
							<span align="left" class="style6" style="margin-left: 25px; color: #FFF;">DATOS DE CLIENTE</span>
						</td>
					</tr>
        			<tr>
    					<td width="100%" valign="top" colspan="2">
    						<table width="100%" border="0" cellspacing="0" cellpadding="3">
      							<tr>
        							<td>
        									<table width="100%" border="0" cellspacing="0" cellpadding="0">
        										<tr class="style5">
        											<td width="15%">Nombre: </td>
        											<td width="85%" class="style9">'.utf8_decode($user->getFirstName())." ".utf8_decode($user->getLastName()).'</td>
        										</tr>
        									</table>
        							</td>
      							</tr>
      							<tr>
        							<td>
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr class="style5">
													<td width="15%">E-mail: </td>
													<td width="85%"  class="style9">
														<a href="mailto:'.$user->getEmail().'" class="style9">
															'.$user->getEmail().'
														</a>
													</td>
												</tr>
											</table>
        							</td>
        						</tr>	
    						</table>
    					</td>
					</tr>
					<tr>
						<td colspan="2" bgcolor="#000000" style="padding: 10px;">
							<span align="left" class="style6" style="margin-left: 25px; color: #FFF;">DATOS DE ENV&Iacute;O</span>
						</td>
					</tr>
  					<tr>
    					<td width="100%" valign="top">
    						<table width="100%" border="0" cellspacing="0" cellpadding="3">
      							<tr>
        							<td width="50%" valign="top">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr class="style5">
												<td width="30%" valign="top">Nombre: </td>
												<td width="70%"  class="style9">'.utf8_decode($user->getFirstName())." ".utf8_decode($user->getLastName()).'
												</td>
											</tr>
										</table>
        							</td>
        							<td width="50%" valign="top">
    									<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr class="style5">
												<td width="30%" valign="top">Pa&iacute;s:  </td>
												<td width="70%"  class="style9">'.$usuario->getCountry().'
												</td>
											</tr>
										</table>
        							</td>					
      							</tr>
      							<tr>
        							<td width="50%" valign="top">
    									<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr class="style5">
												<td width="30%" valign="top">E-mail:   </td>
												<td width="70%"  class="style9">'.$user->getEmail().'
												</td>
											</tr>
										</table>
        							</td>
									<td width="50%" valign="top">
	    									<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr class="style5">
													<td width="30%" valign="top">Estado:   </td>
													<td width="70%"  class="style9">'.$usuario->getState().'
													</td>
												</tr>
											</table> 
        							</td>
        							</tr>					
        							<tr>
        							<td width="50%" valign="top">
    									<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr class="style5">
												<td width="30%" valign="top">Tel&eacute;fono:   </td>
												<td width="70%"  class="style9">'.$usuario->getPhone().'
												</td>
											</tr>
										</table>  
        							</td>
        							<td width="50%" valign="top">
	    									<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr class="style5">
													<td width="30%" valign="top">Ciudad:  </td>
													<td width="70%"  class="style9">'.$usuario->getCity().'
													</td>
												</tr>
											</table>
        							</td>															
      							</tr>
      							<tr>
        							<td width="50%" valign="top">
	    									<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr class="style5">
													<td width="30%" valign="top">Direcci&oacute;n:  </td>
													<td width="70%"  class="style9">'.utf8_decode($usuario->getStreet()).' '.utf8_decode($usuario->getStreet3()).', Int '.$usuario->getStreet4().', '.utf8_decode($usuario->getStreet2()).'
													</td>
												</tr>
											</table>
        							</td>
      							</tr>
      							<tr>
        							<td width="50%" valign="top">
        								
    									<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr class="style5">
												<td width="30%" valign="top">C&oacute;digo    Postal:  </td>
												<td width="70%"  class="style9">'.$usuario->getZipCode().'
												</td>
											</tr>
										</table> 
        							</td>
        												
      							</tr>
    						</table>
    					</td>
	  				</tr>'.



        		(!empty($factura) ?

					'<tr>
    					<td colspan="2" bgcolor="#ECECEC">
    						<p align="left" class="style6">Datos de Facturaci&oacute;n</p>
    					</td>
  					</tr>
        			<tr>
    					<td width="50%" valign="top">
    						<table width="100%" border="0" cellspacing="3" cellpadding="0">
      							<tr>
        							<td>
        								<div align="left">
        									<span class="style5">
        										Nombre o Raz&oacute;n Social: <span class="style9">'.$factura->getRazonSocial().'</span>
        									</span>
        								</div>
        							</td>
        							<td>
        								<div align="left">
        									<span class="style5">
        										C&oacute;digo    Postal:<span class="style9"> '.$factura->getZipCode().'</span>
        									</span>
        								</div>
        							</td>
        												
      							</tr>
      							<tr>
        							<td>
        								<div align="left">
        									<span class="style5">
        										E-mail: 
        										<a href="mailto:'.$factura->getEmail().'" class="style9">
        											'.$factura->getEmail().'
        										</a>
        									</span>
        								</div>
        							</td>
									<td>
        								<div align="left">
        									<span class="style5">
        										Estado:<span class="style9">'.$factura->getState().'</span>
        									</span>
        								</div>
        							</td>
        							</tr>					
        							<tr>
        							<td>
        								<div align="left">
        									<span class="style5">
        										RFC:<span class="style9">'.$factura->getRfc().'</span>
        									</span>
        								</div>
        							</td>
        							<td>
        								<div align="left">
        									<span class="style5">Ciudad: 
        										<span class="style9">'.$factura->getCity().'</span>
        									</span>
        								</div>
        							</td>															
      							</tr>
      							<tr>
        							<td>
        								<div align="left">
        									<span class="style5">
        										Direcci&oacute;n: <span class="style9">'.utf8_decode($factura->getStreet()).' '.utf8_decode($factura->getStreet2()).', '.utf8_decode($factura->getStreet3()).'</span>
        									</span>
        								</div>
        							</td>
      							</tr>
        						<tr>
        							<td>
        								<div align="left">
        									<span class="style5">
        										Tel&eacute;fono: <span class="style9"> '.$factura->getPhone().'</span>
        									</span>
        								</div>
        							</td>
        																	
      							</tr>						
      							
    						</table>
    					</td>
    				
	  				</tr>': "").'											
        													
        													
        		<tr>
		    			<td></td>
		    			<td></td>
	  				</tr>
					<tr>
						<td colspan="2" bgcolor="#000000" style="padding: 10px;">
							<span align="left" class="style6" style="margin-left: 25px; color: #FFF;">DATOS DE LA ORDEN</span>
						</td>
					</tr>
  						
	  				<tr>
		    			<td colspan="2" align="center" class="style6" style="color: #000 !important;">
		    				' . self::GetHTMLOrderByEmail($items, $order->getComments(),true, $orderid).'
					    </td>
					</tr>
		    		<tr>
		    		<td>
		    		&nbsp;				
		    		</td>								
		    		</tr>
		    														
														
			';
			$subject = 'Nueva orden de compra: ' . $order->getOrderId() . '.';	
		}
		
		$body .='<tr class="general" style="border-top: solid 1px '.$colorfuente.'"><td><strong>Print</strong>Proyect</td></tr>
			</table></div>
			</body></html>';

		$header = "MIME-Version: 1.0\r\n"; //para el env�o en formato HTML;
		$header .= "Content-type: text/html; charset=iso-8859-1\r\n";
		$header .= "From: {$config["project_name"]} <no_replay@printproyect.com>\r\n";
		mail($customerEmail,$subject,$body,$header);


		$body='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<title>Untitled Document</title>
				<style type="text/css">
				<!--
					.style5 {font-size: 12px; font-family: Arial, Helvetica, sans-serif; color: black; font-weight: bold; }
					.style6 {
						font-family: Arial, Helvetica, sans-serif;
						font-weight: bold;
						font-size: 14px;
						color: #000000;
					}
					.style9 {font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: black;  font-weight: normal;}
					.info{font-family: Arial, Helvetica, sans-serif; font-size: 18px; color: black;  font-weight: bold;}
					.general{font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: black;  font-weight: normal;}
				-->
				</style>
			</head>
			<body>
				<table width="770" border="0" align="center" cellpadding="5" cellspacing="0">
  					<tr>
    					<td colspan="2" bgcolor="'.$backgroundImagen.'">
    						<img id="_x0000_i1025" src="'.ABS_HTTP_URL.$imgLogo.'" />
    					</td>
  					</tr>
  					<tr>
    					<td colspan="2">
    						<p align="center" class="style6">
    							<strong>Se ha creado una nueva orden de compra con el id : ' . $order->getOrderId() . '</strong>
    						</p>
    					</td>
  					</tr>
    				
        													
        			<tr>
    					<td colspan="2" bgcolor="#000000" style="padding: 10px;">
    						<span align="left" class="style6" style="margin-left: 25px; color: #FFF;">DATOS DE CLIENTE</span>
    					</td>
  					</tr>
        			<tr>
    					<td width="50%" valign="top" colspan="2">
    						<table width="100%" border="0" cellspacing="3" cellpadding="0">
      							<tr>
        							<td>
        								<div align="left">
        									<table width="100%" border="0" cellspacing="0" cellpadding="0">
        										<tr class="style5">
        											<td width="15%">Nombre: </td>
        											<td width="85%" class="style9">'.utf8_decode($user->getFirstName())." ".utf8_decode($user->getLastName()).'</td>
        										</tr>
        									</table>
        								</div>
        							</td>
      							</tr>
      							<tr>
        							<td>
        								<div align="left">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr class="style5">
													<td width="15%">E-mail: </td>
													<td width="85%"  class="style9">
														<a href="mailto:'.$user->getEmail().'" class="style9">
															'.$user->getEmail().'
														</a>
													</td>
												</tr>
											</table>
        								</div>
        							</td>
        						</tr>
    						</table>
    					</td>
    				</tr>
	  				<tr>
    					<td colspan="2" bgcolor="#000000" style="padding: 10px;">
    						<span align="left" class="style6" style="margin-left: 25px; color: #FFF;">DATOS DE ENV&Iacute;O</span>
    					</td>
  					</tr>
  					<tr>
    					<td width="50%" valign="top">
    						<table width="100%" border="0" cellspacing="3" cellpadding="0">
    							<tr>
        							<td width="50%" valign="top">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr class="style5">
												<td width="30%" valign="top">Nombre: </td>
												<td width="70%"  class="style9">'.utf8_decode($user->getFirstName())." ".utf8_decode($user->getLastName()).'
												</td>
											</tr>
										</table>
        							</td>
        							<td>
        								<div align="left">
	    									<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr class="style5">
													<td width="30%" valign="top">Pa&iacute;s:  </td>
													<td width="70%"  class="style9">'.$usuario->getCountry().'
													</td>
												</tr>
											</table> 
        								</div>
        							</td>
        												
      							</tr>
      							<tr>
        							<td>
        								<div align="left">
	    									<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr class="style5">
													<td width="30%" valign="top">E-mail:   </td>
													<td width="70%"  class="style9">'.$usuario->getEmail().'
													</td>
												</tr>
											</table>    
        								</div>
        							</td>
									<td>
        								<div align="left">
	    									<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr class="style5">
													<td width="30%" valign="top">Estado:   </td>
													<td width="70%"  class="style9">'.$usuario->getState().'
													</td>
												</tr>
											</table> 
        								</div>
        							</td>
        							</tr>					
        							<tr>
        							<td>
        								<div align="left">
	    									<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr class="style5">
													<td width="30%" valign="top">Tel&eacute;fono:   </td>
													<td width="70%"  class="style9">'.$usuario->getPhone().'
													</td>
												</tr>
											</table>  
        								</div>
        							</td>
        							<td>
        								<div align="left">
	    									<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr class="style5">
													<td width="30%" valign="top">Ciudad:  </td>
													<td width="70%"  class="style9">'.$usuario->getCity().'
													</td>
												</tr>
											</table>
        								</div>
        							</td>															
      							</tr>
      							<tr>
        							<td>
        								<div align="left">
	    									<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr class="style5">
													<td width="30%" valign="top">Direcci&oacute;n:  </td>
													<td width="70%"  class="style9">'.utf8_decode($usuario->getStreet()).' '.utf8_decode($usuario->getStreet3()).', Int '.$usuario->getStreet4().', '.utf8_decode($usuario->getStreet2()).'
													</td>
												</tr>
											</table>
        								</div>
        							</td>
      							</tr>
      							<tr>
        							<td>
        								<div align="left">
    									<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr class="style5">
												<td width="30%" valign="top">C&oacute;digo    Postal:  </td>
												<td width="70%"  class="style9">'.$usuario->getZipCode().'
												</td>
											</tr>
										</table> 
        								</div>
        							</td>
        												
      							</tr>    						
    						</table>
    					</td>
	  				</tr>'.
		(!empty($factura) ?									
        			'<tr>
    					<td colspan="2" bgcolor="#B18BBE">
    						<p align="left" class="style6">Datos de Facturaci&oacute;n</p>
    					</td>
  					</tr>
        			<tr>
    					<td width="50%" valign="top">
    						<table width="100%" border="0" cellspacing="3" cellpadding="0">
      							<tr>
        							<td>
        								<div align="left">
        									<span class="style5">
        										Nombre o Raz&oacute;n Social: <span class="style9">'.$factura->getRazonSocial().'</span>
        									</span>
        								</div>
        							</td>
        												
      							</tr>
      							<tr>
        							<td>
        								<div align="left">
        									<span class="style5">
        										E-mail: 
        										<a href="mailto:'.$factura->getEmail().'" class="style9">
        											'.$factura->getEmail().'
        										</a>
        									</span>
        								</div>
        							</td>
									
        							</tr>					
        							<tr>
        							<td>
        								<div align="left">
        									<span class="style5">
        										RFC:<span class="style9">'.$factura->getRfc().'</span>
        									</span>
        								</div>
        							</td>
      							</tr>
      							<tr>
        							<td>
        								<div align="left">
        									<span class="style5">
        										Direcci&oacute;n: <span class="style9">'.utf8_decode($factura->getStreet()).' '.utf8_decode($factura->getStreet2()).', '.utf8_decode($factura->getStreet3()).'</span>
        									</span>
        								</div>
        							</td>
      							</tr>
        						<tr>
        							<td>
        								<div align="left">
        									<span class="style5">
        										Tel&eacute;fono: <span class="style9"> '.$factura->getPhone().'</span>
        									</span>
        								</div>
        							</td>
      							</tr>
    						</table>
    					</td>
   					<td width="50%" valign="top">
    						<table width="100%" border="0" cellspacing="5" cellpadding="0">
      							<tr>
        							<td>
        								<div align="left">
        									<span class="style5">
        										C&oacute;digo    Postal:<span class="style9"> '.$factura->getZipCode().'</span>
        									</span>
        								</div>
        							</td>
      							</tr>
      							<tr>
        							<td>
        								<div align="left">
        									<span class="style5">
        										Estado:<span class="style9">'.$factura->getState().'</span>
        									</span>
        								</div>
        							</td>
      							</tr>
      							<tr>
        						<td>
        								<div align="left">
        									<span class="style5">Ciudad: 
        										<span class="style9">'.$factura->getCity().'</span>
        									</span>
        								</div>
        							</td>						
        						</tr>
    						</table>
	    					<p align="left" class="style9">&nbsp;</p>
	    				</td>
	  				</tr>' : "").'		
        			
        		<tr>
		    			<td></td>
		    			<td></td>
	  				</tr>
	  				<tr>
    					<td colspan="2" bgcolor="#000000" style="padding: 10px;">
    						<span align="left" class="style6" style="margin-left: 25px; color: #FFF;">DATOS DE LA ORDEN</span>
    					</td>
  					</tr>
		
	  				<tr>
		    			<td colspan="2" align="center" class="style6" style="color: #000 !important;">
		    				' . self::GetHTMLOrderByEmail($items, $order->getComments(),true, $orderid).'
					    </td>
					</tr>
		    		<tr>
		    		<td>
		    		&nbsp;
		    		</td>
		    		</tr>
		    		<tr>
		    		<td colspan="2" class="general">
		    		 Ud. puede verificar la orden desde la siguiente liga : 
		    		</td>
		    		</tr>
		    		<tr>
		    		<td colspan="2" class="general">
		    		<a href="' . ABS_HTTP_URL . DIRECTORY_BO_RELATIVE . "backoffice/order.php?cmd=edit&id=" . $order->getOrderId().'">' 
		. ABS_HTTP_URL . DIRECTORY_BO_RELATIVE . "backoffice/order.php?cmd=edit&id=" . $order->getOrderId().'</a>
		    		</td>
		    		</tr>
		
				</table>
			</body>
		</html>
			';

		$header = "MIME-Version: 1.0\r\n"; //para el env�o en formato HTML;
		$header .= "Content-type: text/html; charset=iso-8859-1\r\n";
		$header .= "From: {$config["project_name"]} <no_replay@printproyect.com>\r\n";
		//$header .= 'Bcc: ecommerce@printproyect.com'."\r\n";
		$header .= "X-Sender:{$config["project_name"]} <no_replay@printproyect.com>\r\n"; 
		$header .= 'X-Mailer: PHP/'.phpversion()."\r\n";
		$header .= 'X-Priority: 1 (Higuest)'."\r\n";

		mail($config["project_email"],$subject,$body,$header);
	}


	/*public static function sendNotificationStatusMp($orderId){
		$config        = Config::getGlobalConfiguration();
		$order         = eCommerce_SDO_Core_Application_Order::LoadById($orderId);
		$mailer        = new eCommerce_SDO_Core_Application_Mailer();
		$profileId     = $order->getProfileId();

		$customer      = eCommerce_SDO_User::LoadUserProfile($profileId);
		$customerEmail = $customer->getEmail();
		$orderLanguage = $order->getLanguage();
		$statusArr     = self::GetArrStatusInLang( $orderLanguage );

		$mailer->addAddress($customerEmail,$customer->getFirstName(), "TO");
		

		$fondo 				= '#ffffff';
		$fuente 			= 'Arial, Helvetica';
		$colorfuente 		= '#575758';
		$sizefuente 		= '13px';
		$width 				= '770px';
		$colorlink 			= '#B50A47';
		//$imgLogo 			= 'img/layout/banner-mail.jpg';
		$imgLogo 			= 'images/layout/logo.png';
		$backgroundImagen	= '#ffffff';
		
		
		if($orderLanguage == 'EN'){
			$body = 'The status of the order ' . $order->getOrderId() . ' has been updated.' .
			'<br /> The new status of the order is: ' . $statusArr[ $order->getStatus() ] .
			'<br /><br /> You can check this order in the following link: <br />
			<a href="' . ABS_HTTP_URL . "/user_info.php?cmd=viewOrder&id=" . $order->getOrderId().'">
			' . ABS_HTTP_URL . "/user_info.php?cmd=viewOrder&id=" . $order->getOrderId().'</a>'
			;
			$subject = 'Your order ' . $order->getOrderId() . ' has been updated.';
		}else{
				$body='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
				<html xmlns="http://www.w3.org/1999/xhtml">
				<head>
					<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
					<title>Untitled Document</title>
					<style type="text/css">
					<!--
						.style5 {font-size: 12px; font-family: Arial, Helvetica, sans-serif; color: #B50A47; }
						.style6 {
							font-family: Arial, Helvetica, sans-serif;
							font-weight: bold;
							font-size: 14px;
							color: #000000;
						}
						.style9 {font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; }
					-->
					</style>
					</head>
					<body>
						<table width="770" border="0" align="center" cellpadding="5" cellspacing="0">
							<tr>
								<td colspan="2" bgcolor="'.$backgroundImagen.'">
									<img id="_x0000_i1025" src="'.ABS_HTTP_URL.$imgLogo.'" />
								</td>
							</tr>
							<tr>
								<td>
									<p style="font-family: Arial, Helvetica, sans-serif; font-size: 17px; color: #000000;">
						      		El status de su orden <strong>' . $order->getOrderId() . '</strong> ha sido actualizado.' .
						      		'<br /> El nuevo status de su orden es: <strong>' . $statusArr[ $order->getStatus() ] .
						      		'</strong><br /><br /> 
						      		</p>
						      	</td>
					    	</tr>';

			$body .='
					<tr><td>&nbsp;</td></tr>
					<tr><td class="style9"><strong>Print</strong> Proyect</td></tr>
					<tr><td style="background-color: #6901A0;">&nbsp;</td></tr>
					</table>
				</body>
			</html>';
			$subject = 'El status de su orden ' . $order->getOrderId() . ' ha sido actualizado.';	
		}
		
		$mailer->setBody( $body );
		$mailer->setSubject( $subject );
		$mailer->sendMails();
		
		$mailer->resetAddress();
		$correos = explode(',',$config["project_email"]);
		foreach($correos as $correo){
			$mailer->addAddress($correo,$config["project_name"], "TO");
		}
		$subject = 'El status de la orden ' . $order->getOrderId() . ' ha sido actualizado.';
		$body = '<img src="'.ABS_HTTP_URL.$imgLogo.'" /> <br/><br/> <span class="style9">El status de la orden de compra ' . $order->getOrderId() . ' ha sido actualizado.' .
		'<br /> El nuevo status de la orden es: <strong>' . $statusArr[ $order->getStatus() ] .'</strong>'.
		'<br /><br /> Ud. puede verificar la orden desde la siguiente liga : ' . 
		'<br /><a href="' . ABS_HTTP_URL . "bo/backoffice/order.php?cmd=edit&id=" . $order->getOrderId().'">' .
		ABS_HTTP_URL . "bo/backoffice/order.php?cmd=edit&id=" . $order->getOrderId().'</a> </span>'
		;
		
		$body .='<br /><br />==========================================
		<br />
		' .$config["project_name"];
		
		$mailer->setBody( $body );
		$mailer->setSubject( $subject );
		$mailer->sendMails();
	}*/



	protected function sendNotificationStatus( $order ){
		$config = Config::getGlobalConfiguration();
		$mailer = new eCommerce_SDO_Core_Application_Mailer();
		$profileId = $order->getProfileId();
		$customer = eCommerce_SDO_User::LoadUserProfile($profileId);
		$customerEmail = $customer->getEmail();
		$mailer->addAddress($customerEmail,$customer->getFirstName(), "TO");
		$orderLanguage = $order->getLanguage();
		$statusArr = self::GetArrStatusInLang( $orderLanguage );
		$fondo 				= '#ffffff';
		$fuente 			= 'Arial, Helvetica';
		$colorfuente 		= '#575758';
		$sizefuente 		= '13px';
		$width 				= '770px';
		$colorlink 			= '#B50A47';
		$imgLogo 			= 'img/layout/banner-mail.jpg';
		$backgroundImagen	= '#ffffff';
		if($orderLanguage == 'EN'){
			$body = 'The status of the order ' . $order->getOrderId() . ' has been updated.' .
			'<br /> The new status of the order is: ' . $statusArr[ $order->getStatus() ] .
			'<br /><br /> You can check this order in the following link: <br />
			<a href="' . ABS_HTTP_URL . "/user_info.php?cmd=viewOrder&id=" . $order->getOrderId().'">
			' . ABS_HTTP_URL . "/user_info.php?cmd=viewOrder&id=" . $order->getOrderId().'</a>'
			;
			$subject = 'Your order ' . $order->getOrderId() . ' has been updated.';
		}else{
				$body='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
				<title>Untitled Document</title>
				<style type="text/css">
				<!--
					.style5 {font-size: 12px; font-family: Arial, Helvetica, sans-serif; color: #B50A47; }
					.style6 {
						font-family: Arial, Helvetica, sans-serif;
						font-weight: bold;
						font-size: 14px;
						color: #000000;
					}
					.style9 {font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; }
				-->
				</style>
			</head>
			<body>
				<table width="770" border="0" align="center" cellpadding="5" cellspacing="0">
  					<tr>
    					<td colspan="2" bgcolor="'.$backgroundImagen.'" style="padding-left:18px">
    						<p align="left">
    							<img id="_x0000_i1025" src="'.ABS_HTTP_URL.$imgLogo.'" />
    						</p>
    					</td>
  					</tr>
  					<tr>
				      <td><p style="color:'.$colorfuente.';font-weight:bold;">
				      	El status de su orden ' . $order->getOrderId() . ' ha sido actualizado.' .
							      	'<br /> El nuevo status de su orden es: ' . $statusArr[ $order->getStatus() ] .
							      	'<br /><br /> Ud. puede revisar su orden desde la siguiente liga: <br />
						<a href="' . ABS_HTTP_URL . "/user_info.php?cmd=viewOrder&id=" . $order->getOrderId().'">
						' . ABS_HTTP_URL . "user_info.php?cmd=viewOrder&id=" . $order->getOrderId().'</a>
						<br>
				        <p>
					</td>
				    </tr>
		    	</table></div>
			</body></html>													
			';
			$subject = 'El status de su orden ' . $order->getOrderId() . ' ha sido actualizado.';	
		}
		$mailer->setBody( $body );
		$mailer->setSubject( $subject );
		$mailer->sendMails();
		$mailer->resetAddress();
		$correos = explode(',',$config["project_email"]);
		foreach($correos as $correo){
			$mailer->addAddress($correo,$config["project_name"], "TO");
		}
		$subject = 'El status de la orden ' . $order->getOrderId() . ' ha sido actualizado.';
		$body = 'El status de la orden de compra ' . $order->getOrderId() . ' ha sido actualizado.' .
		'<br /> El nuevo status de la orden es: ' . $statusArr[ $order->getStatus() ] .
		'<br /><br /> Ud. puede verificar la orden desde la siguiente liga : ' . 
		'<br /><a href="' . ABS_HTTP_URL . "/backoffice/order.php?cmd=edit&id=" . $order->getOrderId().'">' .
		ABS_HTTP_URL . "/backoffice/order.php?cmd=edit&id=" . $order->getOrderId().'</a>'
		;
		$body .='<br /><br />==========================================
		<br />
		' .$config["project_name"];
		$mailer->setBody( $body );
		$mailer->setSubject( $subject );
		$mailer->sendMails();}
	/**
	 * Deletes an Order from the System
	 *
	 * @param int $orderId            - The ID of the product to be deleted
	 * @return eCommerce_Entity_Order - The recently deleted product
	 * @throws eCommerce_SDO_Core_Application_Exception - When the order is not found or couldn't be deleted
	 */

	public static function Delete( $orderId ){
		// 1. Ensure the order actually exists
		$order = self::LoadById( $orderId );
		if ( $order == new eCommerce_Entity_Order() ){
			throw new eCommerce_SDO_Core_Application_Exception( 'The order to be deleted doesn\'t exist.' );
		}
		else {
			try {
				// 2. Delete the Product
				$dao = self::GetDAO();
				$dao->delete( $orderId );
				// 3. Return the recently deleted Product
				return $order;
			}
			catch ( SQLException $e ){
				throw new eCommerce_SDO_Core_Application_Exception( 'There was an unexpected error while trying to delete the Order', 0, $e );
			}
		}
	}

	/**
	 * Retrieves the appropriate DAO
	 *
	 * @return eCommerce_SDO_Core_DAO_Order
	 */
	protected static function GetDAO(){
		return new eCommerce_SDO_Core_DAO_Order();
	}

	/**
	 * Retrieves all the Orders available.
	 * 
	 * @return eCommerce_Entity_Util_OrderArray
	 */
	public static function GetAll(){
		$dao = self::GetDAO();
		$arrOrders = $dao->loadAll();                           // Load all products as a 2D array
		$objOrders = self::ParseArrayToObjectArray( $arrOrders );
		return $objOrders;
	}

	/**
	 * Searches for all the products where the Parent Product ID matches the one provided.
	 * NOTE: If Category ID is provided, only those products strictly beneath that 
	 * category will be returned. This method does NOT retrieve all the products under the category's 
	 * child categories.
	 *
	 * @param eCommerce_Entity_Search $search - Search parameters (pagination, keywords, etc.)
	 * @param int $categoryId                 - The Category ID or 0/null for the all products
	 * @return eCommerce_Entity_Search_Result_Product
	 * @throws eCommerce_SDO_Core_Application_Exception
	 */
	public static function Search( eCommerce_Entity_Search $search, $profileId = null ){
		try {
			$dao = self::GetDAO();
			if ( !empty( $profileId ) ){
				$dao->addExtraCondition( "profile_id = '" . $dao->getDB()->escapeString( $profileId ) . "'" );
			}
			// Retrieve orders
			$arrOrders = $dao->loadAllByParameters(
				$search->getKeywords(),
				$search->getSearchAsPhrase(),
				$search->getOrderBy(),
				$search->getPage(),
				$search->getResultsPerPage()
			);
			// Retrieve total pages
			$totalPages = $dao->loadAllByParameters(
				$search->getKeywords(),
				$search->getSearchAsPhrase(),
				$search->getOrderBy(),
				$search->getPage(),
				$search->getResultsPerPage(),
				true
			);
			$result = new eCommerce_Entity_Search_Result_Order( $search );
			$result->setTotalPages( $totalPages );
			$result->setResults( self::ParseArrayToObjectArray( $arrOrders ) ); 
			return $result;
		}
		catch( SQLException $e ){
			throw new eCommerce_SDO_Core_Application_Exception( 
				'Error processing the search request. Please see the nested exception for details',
				0, $e );
		}
	}

	/**
	 * @param array $arrOrders
	 * @return eCommerce_Entity_Util_ProductArray
	 */
	protected static function ParseArrayToObjectArray( array $arrOrders ){
		// Create the array that will hold the Product objects
		$objOrders = new eCommerce_Entity_Util_OrderArray(); 
		foreach( $arrOrders as $arrOrder ){
			// Transform each array into object
			$objOrder = eCommerce_SDO_Core_Util_EntityManager::
			               ParseArrayToObject( $arrOrder, new eCommerce_Entity_Order() );
			// Add object to Order array  
			$objOrders[ $objOrder->getOrderId() ] = $objOrder;  
		}
		return $objOrders;
	}

	public static function GetHTMLByOrderId( $orderId, $language = null ){
		$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
		if ($actualLanguage == 'EN')
		{
			$lang = 0;
		}
		else
		{
			$lang = 1;
		}
		$name[0]    = "Product Name";
		$name[1]    = "Nombre del producto";
		$qty[0]     = "Qty";
		$qty[1]     = "Cant";
		$unt_pr[0]  = "Unit Price";
		$unt_pr[1]  = "Precio Unitario";
		$pr_disc[0] = "Price with discount";
		$pr_disc[1] = "Precio con descuento";
		$cod[0]     = "Code";
		$cod[1]     = "C&oacute;digo";
		$order      = eCommerce_SDO_Core_Application_OrderMediation::CreateOrderFullFromOrderId( $orderId, $language );
		$serverSite = "";
		$html       ='';
		if( !empty($order) ){
			$items =  $order->getItems(); 

			$orderCurrency = $order->getCurrency();
			$html .= "
			<table width='100%'>
			<tr>";
			$html .= "
				<td width='70' align='center'>&nbsp;</td>
				<th width='100' align='center'>".$cod[$lang]."</th>
				<th width='120' align='center'>".$name[$lang]."</th>
				<th width='50' align='center'>Caracteristica</th>
				<th width='30' align='center'>".$qty[$lang]."</th>
				<th width='100' align='center'>Precio</th>
				<th width='100' align='center'>Descuento</th>
				<th>Total</th>
			</tr>";

			$currency ='';
			foreach( $items as $item ){
				$product = eCommerce_SDO_Core_Application_Product::LoadById( $item->getProductId() );
				$version  = eCommerce_SDO_Core_Application_Version::LoadById( $item->getVersionId() );

				$discount = $item->getDiscount();

				$galeriaId = $item->getGaleriaId();
				if(!empty($galeriaId)){
					$imagen     = eCommerce_SDO_Galeria::LoadGaleria($galeriaId);
					$imageUrl   = $imagen->getUrlArrayImages("small",0);
					$src_imagen = $imagen->getUrlArrayImages("large",0);
				}

				$file_id = $item->getFileId();
				if(!empty($file_id)){
					$imagen = eCommerce_SDO_Files::LoadFiles($file_id);
					$imageUrl = ABS_HTTP_URL."bo/backoffice/server/php/files/".$imagen->getName();
					$src_imagen = ABS_HTTP_URL."bo/backoffice/server/php/files/".$imagen->getName();
				}elseif(empty($file_id) && empty($galeriaId)){
					$imageUrl   = $product->getUrlImageId('small',0);
					$src_imagen = $product->getUrlImageId('large',0);
				}

				$html .= "<tr>";
				$html .= "<td width='70' align='center'><a href='{$src_imagen}' target='_blank'><img src='{$imageUrl}' width='100%'></a></td>";
				$html .= "<td width='100' align='center'>" . $item->getSku() . "</td>";
				$html .= "<td width='120' align='center'><a href='";
				if( !empty($product) ){
					//$html .= $serverSite ."product.php?cmd=details&id=" . $product->getProductId();
					$html .= $serverSite ."Version.php?product_id=" . $product->getProductId();
				}else{
					$html .= $serverSite ."productNotExist.html";
				}
				$html .= "' target='_blank'>";
				$html .= $version->getName();
				$html .= "</a>";
				$html .="</td>";
				/*$html .="<td align='center'>".$color->getNombre();
				$html .="</td>";*/
				/*$html .="<td align='center'>".$talla->getNombre();
				$html .="</td>";*/
				$html .="<td align='center'>".str_replace(",","<br/>",$item->getAttributes());
				$html .="</td>";
				$html .= "<td width='30' align='center'>";
				$html .= $item->getQuantity() ;
				$html .= "</td>";

				$html .= "<td align='center'>" .eCommerce_SDO_Core_Application_CurrencyManager::numberFormat($item->getPrice() + $discount). " " . $orderCurrency . "</td>";
				$html .= "<td align='center'>" .($discount * $item->getQuantity()). " " . $orderCurrency . "</td>";
				$value = $item->getPrice();
				/*$html .= "<td align='center'>" . eCommerce_SDO_Core_Application_CurrencyManager::numberFormat($value) . " " . $orderCurrency . "</td>";*/
				$productTotal = $value * $item->getQuantity();
				$html .= "<td  align='center'>" . eCommerce_SDO_Core_Application_CurrencyManager::numberFormat($productTotal) . " " . $orderCurrency . "</td>";
				$html .= "</tr>";

				$total = $subTotal += $item->getPrice() * $item->getQuantity();
				$descuento += $item->getDiscount() * $item->getQuantity();

			}

			
			$html .= "<tr>";
			$html .= "<td colspan='8'><hr /></td></tr>";
			$html .="<tr align='right'>";
			$html .= "<td colspan='6'><b>Sub Total : </b></td><td colspan='2'>" . eCommerce_SDO_Core_Application_CurrencyManager::numberFormat($subTotal) . " " . $orderCurrency . "</td></tr>";
			//---------------------EXTRAS
			$extras = $order->getFees(); //eCommerce_SDO_Cart::GetExtras();
			foreach( $extras as $fee){
				$html .= "<tr>";
				$html .= "<td colspan='6' align='right'>" . $fee->getDescription() . " : </td>" . 
				"<td align='right' colspan='2'>" .  eCommerce_SDO_Core_Application_CurrencyManager::numberFormat($fee->getAmount()) . " " . $orderCurrency . "</td></tr>";
			}
			#-- Muestra descuento
			if(!is_null($order->getDescuento())){
				$descuento =  $subTotal * $order->getDescuento();
				$html .= "<tr><td colspan='6' align='right'>Descuento (".($order->getDescuento() * 100)." %):</td>";
				$html .= "<td align='right' colspan='2'>".eCommerce_SDO_Core_Application_CurrencyManager::numberFormat($descuento)." ".$orderCurrency."</td></tr>";
				$total_orden = eCommerce_SDO_Core_Application_CurrencyManager::numberFormat(($total - $descuento)) . " " . $orderCurrency;
			}else{	
				$total_orden = eCommerce_SDO_Core_Application_CurrencyManager::numberFormat($total) . " " . $orderCurrency;
			}//end if

			//echo $total_orden;

			$html .= "<tr>";
			$html .= "<td colspan='8'><hr /></td></tr>";
			$html .="<tr align='right'>";
			$html .= "<td colspan='6'><b>TOTAL : </b></td><td align='right' colspan='2'>" . $total_orden."</td></tr>";
			$html .= "</table>";
		}
		return $html;
	}



	public static function GetValidStatus( $statusId ){
		$validStatus = array( 'N', 'C');
		return ( in_array($statusId,$validStatus) ) ? $statusId : eCommerce_SDO_Core_Application_Order::INVALID_STATUS;
	}
	public static function GetArrStatus(){
		$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
		if ($actualLanguage == 'EN'  )
		{
			$pend = "Payment Pending";
			$comp = "Completed";
			$rech = "Rejected Payment";
		}
		else
		{
			$pend = "Pendiente de pago";
			$comp = "Completada";
			$rech = "Pago Rechazado";
		}
		return array( 'N'=> $pend , 'C' => $comp ,'R' => $rech);
	}
	
	public static function GetArrStatusInLang($actualLanguage){
		
		if ($actualLanguage == 'EN'  )
		{
			$pend = "Payment Pending";
			$comp = "Completed";
			$rech = "Rejected Payment";
		}
		else
		{
			$pend = "Pendiente de pago";
			$comp = "Completada";
			$rech = "Pago Rechazado";
		}
		return array( 'N'=> $pend , 'C' => $comp ,'R' => $rech);
	}
	public static function GetStatusById( $statusId ){
		$arrStatus = self::GetArrStatus();
		return ( isset($arrStatus[ $statusId ]) ) ? $arrStatus[ $statusId ] : 'Unknow Status'; 
	}
	public static function GetValidPaymentMethods( $paymentId ){
		$validPayments = array( 'en_linea','P', 'D');
		return ( in_array($paymentId,$validPayments) ) ? $paymentId : eCommerce_SDO_Core_Application_Order::INVALID_PAYMENT_METHOD;
	}
	public static function GetArrPaymentMethods(){
		$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
		if ($actualLanguage == 'EN')
		{
			$depo = "Deposit";
			$POD = "Pay On Delivery";
		}
		else
		{
			$depo = "Dep&oacute;sito";
			$POD = "Pago contra entrega";
		}
		return array( 'en_linea' => 'Tarjeta de cr&eacute;dito','P'=> 'Paypal' , 'D' => $depo, 'paypal'=> 'Paypal' , 'deposito' => "Dep&oacute;sito" , 'transferencia' => "Transferencia Bancaria", 'oxxo' => "Pago en Oxxo");
	}
	public static function GetPaymentMethodById( $paymentId ){
		$arrPayments = self::GetArrPaymentMethods();
		return ( isset($arrPayments[ $paymentId ]) ) ? $arrPayments[ $paymentId ] : "Unknow Payment Method"; 
	}
	public static function GetEnumValues( $colum ){
		$ArrRoles = array();
		$db = new eCommerce_SDO_Core_DB();
		//we create the instance here because eCommerce/SDO/Core
		//is restringed to access directly for the FrontEnd
		$daoUserProfile = self::GetDAO();
		$enumvalues = $db->sqlGetRecord( "SHOW COLUMNS FROM `" . $daoUserProfile->getTable() . "` LIKE '".$colum."'" );
		//$enumvalues[ "type" ] contents text similar like
		// enum( 'value1', 'value2' ... 'valueN' ) we need only the values
		// 'value1', 'value2' ... 'valueN'
		$enumvalues = substr( $enumvalues[ "type" ], strlen('enum(')  , -1 );
		$enumvalues = explode(',', $enumvalues);
		foreach( $enumvalues as $enumvalue ){
			//$enumvalue contents text similar
			// 'valueN' we need only valueN
			$ArrRoles[ substr( $enumvalue, 1, -1 ) ] = substr( $enumvalue, 1, -1 );
		}
		return $ArrRoles;
	}

	protected function GetHTMLOrderByEmail($items, $comments, $showPrices = false, $orderid = NULL){		
		$order = eCommerce_SDO_Core_Application_OrderMediation::CreateOrderFullFromOrderId( $orderid, $language );
		if($showPrices){
			$actualCurrency = eCommerce_SDO_CurrencyManager::GetActualCurrency();
			$codigoPrecio = '<td bgcolor="#00BEFF" class="style9"><p align="center"><strong>Precio</strong></p></td>';
			$codigoST = '<td bgcolor="#00BEFF" class="style9"><p align="center"><strong>Sub-Total</strong></p></td>';
			$total = 0;
		}
		$codigo .= '
			<table border="1" cellspacing="0" cellpadding="5" width="100%">
	      		<tr>
	        		<td bgcolor="#00BEFF" class="style9"><p align="center"><strong>Imagen</strong></p></td>
	        		<td bgcolor="#00BEFF" class="style9"><p align="center"><strong>SKU</strong></p></td>
					<td bgcolor="#00BEFF" class="style9"><p align="center"><strong>Nombre</strong></p></td>	
					<td bgcolor="#00BEFF" class="style9"><p align="center"><strong>Caracteristica</strong></p></td>		
	        		'.$codigoPrecio.'
	        		<td bgcolor="#00BEFF" class="style9"><p align="center"><strong>Cantidad</strong></p></td>
	        		'.$codigoST.'
	      		</tr>';

		foreach ($items as $item){ 
			//$product      = eCommerce_SDO_Core_Application_Version::LoadById( $item->getVersionId() );
			//$productoFull = eCommerce_SDO_Core_Application_Product::LoadById( $item->getProductId() );
			/*$hayImagen    = $product->getImagen(0);
			$file         = eCommerce_SDO_Core_Application_FileManagement::LoadById($product->getImagen());
			$imageUrl     = empty($hayImagen)?$productoFull->getUrlImageId('small',0):$product->getUrlImageId('small',0);*/
			$version      = eCommerce_SDO_Core_Application_Version::LoadById($item->getVersionId());

			$galeriaId = $item->getGaleriaId();
			if(!empty($galeriaId)){
				$imageUrl = eCommerce_SDO_Galeria::LoadGaleria($galeriaId);
				$src_imagen = $imageUrl->getUrlArrayImages("small",0);
			}

			$file_id = $item->getFileId();
			if(!empty($file_id)){
				$imageUrl = eCommerce_SDO_Files::LoadFiles($file_id);
				$src_imagen = ABS_HTTP_URL."bo/backoffice/server/php/files/".$imageUrl->getName();
			}
			
			if($showPrices){
				//$value = eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $item->getPrice(), $version->getCurrency() );
				$price        = eCommerce_SDO_CurrencyManager::NumberFormat( $item->getPrice()) . " MXN";
				$codigoPrecio = '<td class="style9"><p align="center">'.$price.'</p></td>';
				$valueST      = $item->getPrice() * $item->getQuantity();
				$subtotal     = eCommerce_SDO_CurrencyManager::NumberFormat( $valueST) . " MXN";
				$total        = $total + $valueST;
				$codigoST     = '<td class="style9"><p align="center">'.$subtotal.'</p></td>';
			}

			$codigo .='
			<tr>
        		<td>
        			<p align="center">
        				<strong>
        					<img border="0" id="_x0000_i1026" src="'.$src_imagen.'" width="110" style="width:110px"/>
        				</strong>
        			</p>
        		</td>
        		<td class="style9">
        			<p align="center">'.utf8_decode($item->getSku()).'</p>
        		</td>
        		<td class="style9">
        			<p align="center">'.utf8_decode($version->getName()).'</p>
        		</td> 			
        		<td class="style9">
        			<p align="center">'.utf8_decode(str_replace(",","<br/>",$item->getAttributes())).'</p>
        		</td>
        		'.$codigoPrecio.'
        		<td class="style9">
        			<p align="center">'.$item->getQuantity().'</p>
        		</td>
        		'.$codigoST.'
      		</tr>
			';
		}


		if($showPrices){ 
			//---------------------EXTRAS
			$extras = $order->getFees(); 
			//$extras = eCommerce_SDO_Cart::GetExtras($total);
			$envio = 0;
			foreach( $extras as $fee){
                if($fee->getAmount() == '0'){
                    $desc_envio = '';
                }elseif($fee->getAmount() == '100'){
                    $desc_envio = '';
                }elseif($fee->getAmount() == "145") {
                    $desc_envio = '';
                }//end if



				$codigo .= "<tr>";
				$codigo .= "<td colspan='6' align='right' class='style6'>" . $fee->getDescription() . " <br/>".$desc_envio.": </td>" .
						"<td align='center' class='style6'>" . eCommerce_SDO_CurrencyManager::NumberFormat($fee->getAmount()) . " " . $actualCurrency . "</td></tr>";
				$envio +=  $fee->getAmount();
			}

			/*$total = $total + $envio;
			$total = eCommerce_SDO_CurrencyManager::NumberFormat( $total) . " " . $actualCurrency ;*/


			#-- Muestra descuento
			if(!is_null($order->getDescuento())){
				$descuento =  $total * $order->getDescuento();
				$codigo .= "<tr><td colspan='6' align='right'>Descuento (".($order->getDescuento() * 100)." %):</td>";
				$codigo .= "<td align='right' colspan='2'>".eCommerce_SDO_Core_Application_CurrencyManager::numberFormat($descuento)." ".$actualCurrency."</td></tr>";
				$total_orden = "$ ".eCommerce_SDO_Core_Application_CurrencyManager::numberFormat(($total - $descuento)) . " " . $actualCurrency;
			}else{	
				$total_orden = "$ ".eCommerce_SDO_Core_Application_CurrencyManager::numberFormat($total) . " " . $actualCurrency;
			}//end if

			$codigo .= '
				<tr>
	        		<td colspan="6" class="style6">
	        			<p align="right"><strong>Total:</strong></p>
	        		</td>
	        		<td class="style6">
	        			<p align="center"><strong>'.$total_orden.'</strong></p>
	        		</td>
	      		</tr>';
		}
		if($comments != ''){
			$codigo .='
				<tr>
	        		<td>
	        			<p align="left" class="style5"><strong>Comentario:</strong></p>
	        		</td>
	        		<td colspan="6">
	        			<p align="left" class="style9"><strong>'.nl2br($comments).'</strong></p>
	        		</td>
	      		</tr>';
		}
		$codigo .= '<tr>
        		<td colspan="6"></td>
        		<td></td>
      		</tr>
    	</table>';
		return $codigo;
	}

	public static function GetTotalOrder($order, $extra = false) {
		$actualCurrency = eCommerce_SDO_CurrencyManager::GetActualCurrency ();
		$items = $order->getItems ();
		$total_quantity = 0;
		foreach ( $items as $item ) {
			$productId = $item->product_id;
			$sku = $item->sku;
			$product = eCommerce_SDO_Catalog::LoadProduct ( $productId );
			$version = eCommerce_SDO_Core_Application_Version::LoadById( $item->getVersionId() );
			$total_quantity += $item->quantity;
			//$value = eCommerce_SDO_CurrencyManager::ValueInActualCurrency ( $product->getRealPrice (), $product->getCurrency () );
			$value = $version->getPrice ();
			$total_price += $value * $item->quantity;
			if ($extra)
				$total_weight += $product->getWeight () * $item->quantity;
		}
		if ($extra) {
			$extras = eCommerce_SDO_Core_Application_OrderFee::GetAllByOrder ( $order->getOrderId () );
			foreach ( $extras as $fee ) {
				switch ($fee->getDescription ()) {
					case 'Tipo de env&iacute;o' :
					case 'Gastos de env&iacute;o':
						$totalEnvio = $fee->getAmount ();
						break;
				}
			}
			$total_price = $total_price + $totalEnvio;
		}
		return array ('quantity' => $total_quantity, 
		'price' => array ('format' => eCommerce_SDO_CurrencyManager::NumberFormat ( $total_price ) . " " . $actualCurrency, 
		'amount' => $total_price ), 
		'weight' => $total_weight, 
		'fee' => $totalEnvio );
	}
}
?>