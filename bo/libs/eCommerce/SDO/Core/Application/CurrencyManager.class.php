<?php
/**
 * This class provides access to manipulation of Currency in the System.
 * 
 * @author Alberto Pérez <alberto@vicomstudio.com>
 */
class eCommerce_SDO_Core_Application_CurrencyManager extends eCommerce_SDO_Core_Application {
	const SESSION_NAMESPACE = 'CurrencyManager';
	const DEFAULT_CURRENCY ='MXN';
	
	public static function SetActualCurrency( $currency ){
		$currency = self::GetValidCurrency( $currency );
		eCommerce_FrontEnd_Util_Session::Set( self::SESSION_NAMESPACE, $currency );
		//die();
	}
	
	public static function GetValidCurrency( $currency ){
		$currencies = self::GetCurrencies();
		return (!in_array($currency,$currencies) ) ? self::DEFAULT_CURRENCY : $currency;
	}
	
	public static function ValueInActualCurrency( $mont, $currency ='MXN'){
		
		$currencyActual = self::GetActualCurrency();
		
		if( $currencyActual == $currency)
		{
			return $mont;
		}
		else{
			
			return eCommerce_SDO_Core_Application_CurrencyManager::CalculateValueInCurrency( $mont, $currency, $currencyActual );
		}
	}
	
	public static function GetActualCurrency( $defaultCurrency = self::DEFAULT_CURRENCY ){
		//get the actual currency of the session 
		//if session says null then return the defaultCurrency
		//$currency = 'MXN';
		$currency = eCommerce_FrontEnd_Util_Session::Get( self::SESSION_NAMESPACE );
		
		$currency = ( empty($currency) ) ? $defaultCurrency : $currency;
		 
		return $currency;
	}
	/*
	 * Return the value of a currency in other currency
	 */
	public static function CalculateValueInCurrency( $value, $currencyFrom='MXN', $currencyTo ){
		
		$valueCurrencyTo = 1;
		
		$currencyValids = eCommerce_SDO_Core_Application_CurrencyManager::GetCurrencies();

		try{
			if( !in_array($currencyFrom,$currencyValids) ){
				throw new eCommerce_SDO_Core_Application_Exception( 'currencyFrom is not a valid currency ', 0, $e );
			}
			
			if( !in_array($currencyTo,$currencyValids) ){
				throw new eCommerce_SDO_Core_Application_Exception( 'currencyTo is not a valid currency ', 0, $e );
			}
			
			switch( $currencyFrom ){
				default:	
				case 'USD':
					$valueCurrencyTo = eCommerce_SDO_Core_Application_CurrencyManager::calculateUSDToCurrency( $value, $currencyTo );
				break;

				case 'MXN':
					$valueCurrencyTo = eCommerce_SDO_Core_Application_CurrencyManager::calculateMXNToCurrency( $value, $currencyTo );
				break;
			}
			
		}
		catch(Exception $e){
			throw new eCommerce_SDO_Core_Application_Exception( 'Its not possible Change the currency type', 0, $e );
		}
		
		return $valueCurrencyTo;
	}
	
	public static function calculateUSDToCurrency( $valueInUSD, $currencyTo ){
		$adminTool = eCommerce_SDO_AdminTool::LoadActualAdminTool();
		
		$valueCurrencyTo = 1;
		
		$currencyValids = eCommerce_SDO_Core_Application_CurrencyManager::GetCurrencies();
		try{
			if( !in_array($currencyTo, $currencyValids) ){
				throw new eCommerce_SDO_Core_Application_Exception( 'currencyFrom is not a valid currency ', 0, $e );
			}
			
			switch( $currencyTo ){
				case 'USD':$valueCurrencyTo = $valueInUSD * 1; break;
				
				case 'MXN':
					//$valueCurrencyTo = $valueInUSD * 13.30;
					$valueCurrencyTo = $valueInUSD * $adminTool->getExchangeRateMxn2usd();
					
				break;
				
			}
		}
		catch(Exception $e){
			throw new eCommerce_SDO_Core_Application_Exception( 'Its not possible Calculate the currency USD', 0, $e );
		}
		
		$valueCurrencyTo = round( $valueCurrencyTo, 2);
		
		return $valueCurrencyTo;
	}
	
	public static function calculateMXNToCurrency( $valueInMXN, $currencyTo ){
		$adminTool = eCommerce_SDO_AdminTool::LoadActualAdminTool();
		
		$valueCurrencyTo = 1;
		
		$currencyValids = eCommerce_SDO_Core_Application_CurrencyManager::GetCurrencies();

		
		try{
			if( !in_array($currencyTo, $currencyValids) ){
				throw new eCommerce_SDO_Core_Application_Exception( 'currencyFrom is not a valid currency ', 0, $e );
			}
			switch( $currencyTo ){
				
				case 'USD':
					$value = $adminTool->getExchangeRateMxn2usd() > 0 ? $adminTool->getExchangeRateMxn2usd() : 1;
					$valueCurrencyTo = $valueInMXN / $value;
				break;
				
				case 'MXN':$valueCurrencyTo = $valueInMXN * 1;break;
				
			}
		}
		catch(Exception $e){
			throw new eCommerce_SDO_Core_Application_Exception( 'Its not possible Calculate the currency USD', 0, $e );
		}
		//$valueCurrencyTo = round( $valueCurrencyTo, 2);
		return $valueCurrencyTo;
	}
		
	public static function GetCurrencies($arrOptions = false){
		return $arrOptions ? array('MXN'=>'MXN','USD'=>'USD' ) : array('MXN','USD' );
	}
	
	public function GetHTMLSelect( $selectName, $selected = '', $id = '', $CSSclass='', $selectParams ='' ){
		$currencies = self::GetCurrencies();
		$html='';
		
		if( !empty($currencies) ){
			$id = ( $id = '' ) ? $selectName : $id;
			$html = '<select name="' . $selectName . '" id="' . $id . '" class="' . $CSSclass . '" ' . $selectParams . '>';

			foreach( $currencies as $currency ){
				$code = $name = $currency;
				
				$selectedCodeHTML = ( $selected == $code || $selected == $name ) ? 'selected="SELECTED"' : '';
				$html .= '<option class="' . $CSSclass . '" value="' . $code . '" ' . $selectedCodeHTML .' >' . $name . '</option>';
			}
			$html .= '</select>';
		}
		
		return $html;
	}	

	public function numberFormat( $number ){
		return number_format($number,2,'.',',');
	}
}
?>