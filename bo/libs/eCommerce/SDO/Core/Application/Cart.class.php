<?php
class eCommerce_SDO_Core_Application_Cart extends eCommerce_SDO_Core_Application {
	const ADDRESS_SHIP_TO = eCommerce_SDO_Core_Application_Order::ADDRESS_SHIP_TO;
	const ADDRESS_BILL_TO = eCommerce_SDO_Core_Application_Order::ADDRESS_BILL_TO;
	
	//we use this token in the following files: 
	//	eCommerce_SDO_Core_Application_OrderItem::SetAttributes()
	//	SELF::GetAllAtributtes(), SELF::GetRealProductId()
	const PRINCIPAL_TOKEN = '|';
	
	public function GetAllAtributtes( $productId ){
		$productId = empty($productId) ? '' : $productId;
		$attributes = array();
		if(is_numeric($productId) ){
			$productId = explode( self::PRINCIPAL_TOKEN,$productId);
			unset( $productId[0] );
			$att = $productId;
			$ArrAtt = eCommerce_SDO_Core_DAO_Inventory::GetAllAttributes();
			$i=0;
			
			for($i=1; $i<=count($att);$i += 2){
				if( in_array($att[ $i ],$ArrAtt) ){
					$attributes[ $att[ $i ] ] = empty($att[ ($i + 1) ]) ? null : $att[ ($i + 1) ];
				}
			}
			
		}
		return $attributes; 
	}
	
	
	public function GetRealProductId( $productId ){
		$productId = explode( self::PRINCIPAL_TOKEN, $productId);		
		return empty($productId[0]) ? 0 : $productId[0]; 
	}
	
	/**
	 * Adds a new product to the cart. Read Notes for exceptions.
	 * NOTE: Exception thrown when 
	 * a) Product is already in Cart
	 * b) Product does not exist in DB
	 * c) Quantity is invalid
	 *
	 * @param int $productId 
	 * @param int $quantity          - Unsigned integer greater or equal than 1 
	 * @return eCommerce_Entity_Cart
	 * @throws eCommerce_SDO_Core_Application_Exception
	 */
	/*public static function AddItem( $productId, $quantity = 1 , $color, $talla){
		$productId = $productId."|".$color."|".$talla;
		$productId = str_replace(" ","-",$productId);
		
		if ( self::IsProductInCart( $productId ) ){
			//if the product exists we need add the adicional quantity
			$cart = self::GetCart();
			$item = $cart->getItem( $productId );
			return self::UpdateQuantity($productId, $item->getQuantity() + $quantity);
		}
		$test = new Validator_Test_Numeric_UnsignedInteger();
		if ( !$test->isValid( $quantity ) || $quantity < 1 ){
			throw new eCommerce_SDO_Core_Application_Exception( 'Invalid quantity: ' . $quantity, 102 );
		}
		
		if ( eCommerce_SDO_Core_Application_Product::LoadById( self::GetRealProductId($productId) )->getProductId() == null ){
			throw new eCommerce_SDO_Core_Application_Exception( 'Product with ID ' . $productId . ' not found.', 101 );
		}
		
		$item = new eCommerce_Entity_Cart_Item();
		$item->setProductId( $productId );
		$item->setQuantity( $quantity );
		$item->setColor( $color );
		$item->setTalla( $talla );		
		
		
		$item->setAttributes( self::GetAllAtributtes( $productId ) );
		
		$cart = self::GetCart();
		
		$cart->addItem( $item );
		return self::SaveCart( $cart );
	}*/
	
	public static function AddItem( $productId, $quantity = 1 , $versionId , $color = 0, $talla = 0,$attributes,$galeria_id,$file_id,$bastidor_id,$acabado_id,$afinado_id,$nom_material,$precio,$descuento,$c_ancho,$c_alto){
		$productId = $productId."|".$versionId."|".$color."|".$talla."|".$galeria_id."|".$file_id."|".$bastidor_id."|".$acabado_id."|".$afinado_id."|".$nom_material."|".$c_ancho."|".$c_alto."|";

		$productId = str_replace(" ","-",$productId);
		
		if ( self::IsProductInCart( $productId) ){
			//if the product exists we need add the adicional quantity
			$cart = self::GetCart();
			$item = $cart->getItem( $productId );
			return self::UpdateQuantity($productId, $item->getQuantity() + $quantity,$attributes,$galeria_id,$file_id);
		}
		
		$test = new Validator_Test_Numeric_UnsignedInteger();
		if ( !$test->isValid( $quantity ) || $quantity < 1 ){
			throw new eCommerce_SDO_Core_Application_Exception( 'Invalid quantity: ' . $quantity, 102 );
		}
	
		if ( eCommerce_SDO_Core_Application_Product::LoadById( self::GetRealProductId($productId) )->getProductId() == null ){
			throw new eCommerce_SDO_Core_Application_Exception( 'Product with ID ' . $productId . ' not found.', 101 );
		}
	
		$item = new eCommerce_Entity_Cart_Item();
		$item->setProductId( $productId );
		$item->setVersionId( $versionId );
		$item->setQuantity( $quantity );
		$item->setColor( $color );
		$item->setTalla( $talla );
		$item->setPrice( $precio );
		$item->setDiscount( $descuento );
		//$item->setAttributes( self::GetAllAtributtes( $productId ) );
		$item->setAttributes($attributes);
		$item->setGaleriaId($galeria_id);
		$item->setFileId($file_id);
		$cart = self::GetCart();
		$cart->addItem( $item );
		return self::SaveCart( $cart );
	}
	
	/**
	 * Determines if a product is currently stored in the Cart
	 *
	 * @param int $productId
	 * @return boolean
	 */
	public static function IsProductInCart( $productId ){
		$cart = eCommerce_SDO_Core_Application_Cart::GetCart();
		for( $it = $cart->getItems()->getIterator(); $it->valid(); $it->next() ){
			$item = $it->current();
			if ( $item->getProductId() == $productId ){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Updates the quantity of an Item in the Cart.
	 * NOTE: Exception thrown when:
	 * a) Product does not exist in Cart
	 * b) Quantity is invalid
	 *
	 * @param int $productId
	 * @param int $quantity
	 * @return eCommerce_Entity_Cart
	 * @throws eCommerce_SDO_Core_Application_Exception
	 */
	public static function UpdateQuantity( $productId, $quantity,$attributes,$galeria_id,$file_id){
		
		if ( !self::IsProductInCart( $productId ) ){
			throw new eCommerce_SDO_Core_Application_Exception( 'Product not found in Cart', 103 );
		}
		$test = new Validator_Test_Numeric_UnsignedInteger();
		if ( !$test->isValid( $quantity ) || $quantity < 1 ){
			throw new eCommerce_SDO_Core_Application_Exception( 'Invalid quantity: ' . $quantity, 102 );
		}
		
		$cart = eCommerce_SDO_Core_Application_Cart::GetCart();
		$item = $cart->getItem( $productId );
		$item->setQuantity( $quantity );
		$item->setAttributes($attributes);
		$item->setGaleriaId($galeria_id);
		$item->setFileId($file_id);
				
		return self::SaveCart( $cart );
	}
	
	/**
	 * Removes the Item from the Cart based on the Product's ID
	 *
	 * @param int $productId
	 * @return eCommerce_Entity_Cart
	 */
	public static function RemoveItem( $productId ){
		if ( !self::IsProductInCart( $productId ) ){
			throw new eCommerce_SDO_Core_Application_Exception( 'Product with ID ' . $productId . ' not found in Cart', 103 );
		}
		
		$cart = eCommerce_SDO_Core_Application_Cart::GetCart();
		$cart->removeItem( $productId );
		return self::SaveCart( $cart );
	}
	
	/**
	 * Destroys the Cart and retrieves an empty Cart
	 *
	 */
	public static function DestroyCart(){
		eCommerce_SDO_Core_Util_CartSession::DestroyCart();
		return self::GetCart();
	}
	
	/**
	 * Retrieves the cart from the Session
	 * 
	 * @return eCommerce_Entity_Cart
	 */
	public static function GetCart(){
		return eCommerce_SDO_Core_Util_CartSession::GetCart();
	}
	
	/**
	 * Saves the cart in the Session (through the Cart Session manager)
	 *
	 * @param eCommerce_Entity_Cart $cart
	 * @return eCommerce_Entity_Cart
	 */
	public static function SaveCart( eCommerce_Entity_Cart $cart ){
		eCommerce_SDO_Core_Util_CartSession::SetCart( $cart );
		return $cart;
	}
	
	/**
	 * Sets the Shipping Address for the Cart. 
	 * NOTE: This method is a shortcut of AddOrderAddress().  
	 *
	 * @param eCommerce_Entity_User_Address $address
	 * @return eCommerce_Entity_Cart
	 * @throws eCommerce_SDO_Core_Validator_Exception
	 */	
public static function SetShipTo( eCommerce_Entity_Order_Address $address ){
	
	$address->setType( eCommerce_SDO_Core_Application_Cart::ADDRESS_SHIP_TO );
	return self::AddOrderAddress( $address );	}
		/*$orderAddress = new eCommerce_Entity_Order_Address();
		$orderAddress->setStreet( $address->getStreet() );
		$orderAddress->setStreet2( $address->getStreet2() );
		$orderAddress->setStreet3( $address->getStreet3() );
		$orderAddress->setStreet4( $address->getStreet4() );
		$orderAddress->setCity( $address->getCity() );
		$orderAddress->setState( $address->getState() );
		$orderAddress->setZipCode( $address->getZipCode() );
		$orderAddress->setCountry( $address->getCountry() );
		//$orderAddress->setType( 'Ship To' );
		
		$orderAddress->setType( eCommerce_SDO_Core_Application_Cart::ADDRESS_SHIP_TO );
		
		return self::AddOrderAddress( $orderAddress );
	}*/
	
	/**
	 * Sets the Shipping Address for the Cart
	 * NOTE: This method is a shortcut of AddOrderAddress().
	 *
	 * @param eCommerce_Entity_User_Address $address
	 * @return eCommerce_Entity_Cart
	 * @throws eCommerce_SDO_Core_Validator_Exception
	 */
public static function SetBillTo( eCommerce_Entity_Order_Address $address ){		
	$address->setType( eCommerce_SDO_Core_Application_Cart::ADDRESS_BILL_TO );		
	return self::AddOrderAddress( $address );	}
	/*public static function SetBillTo( eCommerce_Entity_Util_Address $address ){
		$orderAddress = new eCommerce_Entity_Order_Address();
		$orderAddress->setStreet( $address->getStreet() );
		$orderAddress->setStreet2( $address->getStreet2() );
		$orderAddress->setStreet3( $address->getStreet3() );
		$orderAddress->setStreet4( $address->getStreet4() );
		$orderAddress->setCity( $address->getCity() );
		$orderAddress->setState( $address->getState() );
		$orderAddress->setZipCode( $address->getZipCode() );
		$orderAddress->setCountry( $address->getCountry() );
		$orderAddress->setType( eCommerce_SDO_Core_Application_Cart::ADDRESS_BILL_TO );
		
		return self::AddOrderAddress( $orderAddress );
	}*/
	
	/**
	 * Adds an Address to the Order. If the address type already exists, it's overwritten. 
	 *
	 * @param eCommerce_Entity_Order_Address $address
	 * @return eCommerce_Entity_Cart
	 */
	public static function AddOrderAddress( eCommerce_Entity_Util_Address $address ){ 
		$validator = new eCommerce_SDO_Core_Validator_Address( $address );
		$validator->validate();
		
		if ( !$validator->isValid() ){
			throw new eCommerce_SDO_Core_Validator_Exception( $validator ); 
		}
		else {

			$cart = self::GetCart();
			$cart->addAddress( $address );
			return self::SaveCart( $cart );
		}
	}
	
	public static function GetShipTo(){
		
	}

	/**
	 * Return if the cart is empty
	 *
	 * @return boolean
	 */
	public static function IsEmptyCart(){
		$cart = self::GetCart();
		$items = $cart->getItems();
		return ( count( $items ) == 0 );
	}
	

	/**
	 * Return if the cart is empty
	 *
	 * @return boolean
	 */
	public static function IsEmptyTheCart( eCommerce_Entity_Cart $cart ){
		$items = $cart->getItems();
		return ( count( $items ) == 0 );
	}
	
public static function setShippingType( $shippingType ){
		$cart = self::GetCart();
		$cart->setShippingType( $shippingType );
		return self::SaveCart( $cart );
	}
	
}
?>