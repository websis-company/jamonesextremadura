<?php
class eCommerce_SDO_Boletin{
	
	/**
	 * Retrieves all the User Profiles filtered by an specific user role and search parameters. 
	 * If the role is Null or an empty string, 
	 *
	 * @param eCommerce_Entity_Search $search - The search parameters
	 * @param string $role                    - The Role to filter by. Empty for all roles.
	 * @return eCommerce_Entity_Search_Result_Profile
	 */
	public static function SearchBoletin( eCommerce_Entity_Search $search, $extraConditions = array() ){
		return eCommerce_SDO_Core_Application_Boletin::Search( $search, $extraConditions );
	}
	

	/**
	 * Load the User Profile from the given ID. Note an empty object is returned if the ID doesn't exist
	 *
	 * @param int $categoryId
	 * @return eCommerce_Entity_User
	 */
	public static function LoadBoletin( $BoletinId ){
		return eCommerce_SDO_Core_Application_Boletin::LoadById( $BoletinId ); 
	}
	
	/**
	 * Saves a new or updates an existing User Profile object
	 *
	 * @param eCommerce_Entity_User $profile
	 * @return eCommerce_Entity_User
	 * @throws eCommerce_SDO_Application_Exception
	 */
	public static function SaverBoletin( eCommerce_Entity_Boletin $Boletin ){
		
		return eCommerce_SDO_Core_Application_Boletin::Save( $Boletin );
	}

	/**
	 * Deletes a User Profile from the System
	 *
	 * @param int $profileId                            - The ID of the user profile to be deleted
	 * @return eCommerce_Entity_Catalog_Category        - The recently deleted category
	 * @throws eCommerce_SDO_Core_Application_Exception - When the category is not found or couldn't be deleted
	 */
	public static function DeleteBoletin( $BoletinId ){
		return eCommerce_SDO_Core_Application_Boletin::Delete( $BoletinId ); 
	}
	
	/**
	 * Retrieves all the Boletin available.
	 * 
	 * @return eCommerce_Entity_Util_Boletinrray
	 */
	public static function GetAllBoletin($order_by = ''){
		return eCommerce_SDO_Core_Application_Boletin::GetAll($order_by);
	}
	
	public static function GetAllInArray(){
		return eCommerce_SDO_Core_Application_Boletin::GetAllInArray();
	}
}
?>