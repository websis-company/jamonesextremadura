<?php
class eCommerce_SDO_Catalog{
	const TOKEN_ORDERITEM_ATTRIBUTE_VALUE = eCommerce_SDO_Core_Application_OrderItem::ATTRIBUTE_VALUE_TOKEN;
	const TOKEN_CART_PRINCIPAL_TOKEN = eCommerce_SDO_Core_Application_Cart::PRINCIPAL_TOKEN;
	
	const CATEGORY_SCULPTURES_ID = eCommerce_SDO_Core_Application_Category::CATEGORY_SCULPTURES_ID;
	/**
	 * Categories Administration
	 */
	
	/**
	 * Saves a new or updates an existing Category object
	 *
	 * @param eCommerce_Entity_Catalog_Category $category
	 * @return eCommerce_Entity_Catalog_Category
	 * @throws eCommerce_SDO_Application_Exception
	 */
	public static function SaveCategory( eCommerce_Entity_Catalog_Category $category ){
		return eCommerce_SDO_Core_Application_Category::Save( $category );
	}
	
	/**
	 * Load the Category from the given ID. Note an empty object is returned if the ID doesn't exist
	 *
	 * @param int $categoryId
	 * @return eCommerce_Entity_Catalog_Category
	 */
	public static function LoadCategory( $categoryId ){
		return eCommerce_SDO_Core_Application_Category::LoadById( $categoryId ); 
	}
	
	public static function sendNotificationNewOrder($order,$paymentMethod,$responseId){
		return eCommerce_SDO_Core_Application_Order::sendNotificationNewOrder($order,$paymentMethod,$responseId);
	}	
/**
	 * Deletes a Category from the System
	 *
	 * @param int $categoryId                           - The ID of the category to be deleted
	 * @return eCommerce_Entity_Catalog_Category        - The recently deleted category
	 * @throws eCommerce_SDO_Core_Application_Exception - When the category is not found or couldn't be deleted
	 */
	public static function DeleteCategory( $categoryId ){
		return eCommerce_SDO_Core_Application_Category::Delete( $categoryId ); 
	}
	
	/**
	 * Retrieves all the Categories available.
	 * 
	 * @return eCommerce_Entity_Util_CategoryArray
	 */
	public static function GetAllCategories(){
		return eCommerce_SDO_Core_Application_Category::GetAll();
	}

	public static function GetSomeCategorys($excluir=false){
		return eCommerce_SDO_Core_Application_Category::GetSomeCategorys($excluir);	
	}
	
	/**
	 * Performs a paged search for Categories
	 *
	 * @param eCommerce_Entity_Search $search
	 * @return eCommerce_Entity_Search_Result_Category
	 */
	public static function SearchCategories(eCommerce_Entity_Search $search ){
		return eCommerce_SDO_Core_Application_Category::Search( $search );
	}
	
	
	public static function GetCategoriesByParentCategory( $parentCategoryId = null, $search_=null ){
		if ($search_ == 1){
			$q = ($_REQUEST['q'])?$_REQUEST['q']:'';
			$o = ($_REQUEST['orden']=='Z-A')?"name DESC":'orden ASC';
			$k = ($_REQUEST['k'])?$_REQUEST['k']:'-1';
			$search = new eCommerce_Entity_Search($q,$o,1,$k);
		}else{
			$search = new eCommerce_Entity_Search('','orden ASC',1,-1);
		}
		return eCommerce_SDO_Core_Application_Category::SearchByParentCategoryId( $search, $parentCategoryId );
	}
	
	public static function SearchCategoriesByParentCategory( eCommerce_Entity_Search $search, $parentCategoryId = null ){
		return eCommerce_SDO_Core_Application_Category::SearchByParentCategoryId( $search, $parentCategoryId );
	}
	
	public static function GetParentCategoryByProductId( $productId ){
		return eCommerce_SDO_Core_Application_ProductCategory::GetParentCategoryByProductId( $productId );
	} 
	
	/**
	 * Products Administration
	 */
	
		/**
	 * Saves a new or updates an existing Product object
	 *
	 * @param eCommerce_Entity_Catalog_Product $product
	 * @return eCommerce_Entity_Catalog_Product
	 * @throws eCommerce_SDO_Application_Exception
	 */
	public static function SaveProduct( eCommerce_Entity_Catalog_Product $product ){
		return eCommerce_SDO_Core_Application_Product::Save( $product );
	}
	
	/**
	 * Load the Product from the given ID. Note an empty object is returned if the ID doesn't exist
	 *
	 * @param int $productId
	 * @return eCommerce_Entity_Catalog_Product
	 */
	public static function LoadProduct( $productId, $language = null ){		
		return eCommerce_SDO_Core_Application_Product::LoadById( $productId, $language ); 
	}
	
	public static function LoadVersion( $versionId, $language = null ){
		return eCommerce_SDO_Core_Application_Version::LoadById( $versionId, $language );
	}
	
	public static function LoadVersionByColorTalla( $colorId = NULL , $tallaId = NULL , $productId ){
		
		return eCommerce_SDO_Core_Application_Version::LoadVersionByColorTalla( $colorId, $tallaId, $productId );
	}
	

	/**
	 * Deletes a Product from the System
	 *
	 * @param int $productId                            - The ID of the category to be deleted
	 * @return eCommerce_Entity_Catalog_Product         - The recently deleted category
	 * @throws eCommerce_SDO_Core_Application_Exception - When the category is not found or couldn't be deleted
	 */
	public static function DeleteProduct( $productId ){
		return eCommerce_SDO_Core_Application_Product::Delete( $productId ); 
	}
	
	/**
	 * Retrieves all the Products available.
	 * 
	 * @return eCommerce_Entity_Util_ProductArray
	 */
	public static function GetAllProducts(){
		return eCommerce_SDO_Core_Application_Product::GetAll();
	}
	
	public static function GetAllProductVersiones($product_id = NULL){
		return eCommerce_SDO_Core_Application_Version::GetAllVersiones($product_id);
	}
	
	public static function GetProductVersiones($product_id = NULL){
		return eCommerce_SDO_Core_Application_Version::GetVersiones($product_id);
	}
	public static function GetProductVersionesTodo($product_id = NULL){
		return eCommerce_SDO_Core_Application_Version::GetVersionesTodo($product_id);
	}
	public static function GetProductVersionesColor($product_id = NULL, $modelo = NULL){
		return eCommerce_SDO_Core_Application_Version::GetVersionesColor($product_id, $modelo);
	}
	public static function GetProductVersionesTalla($product_id = NULL, $modelo = NULL){
		return eCommerce_SDO_Core_Application_Version::GetVersionesTalla($product_id, $modelo);
	}
	
	public static function GetVersionTalla($version_id){
		return eCommerce_SDO_Core_Application_Version::GetVersionTalla($version_id);
	}
	
	/**
	 * Searches for all the products where the Parent Product ID matches the one provided.
	 * NOTE: If Category ID is provided, only those products strictly beneath that 
	 * category will be returned. This method does NOT retrieve all the products under the category's 
	 * child categories.
	 *
	 * @param eCommerce_Entity_Search $search - Search parameters (pagination, keywords, etc.)
	 * @param int $categoryId                 - The Category ID or 0/null for the all products
	 * @return eCommerce_Entity_Search_Result_Product
	 * @throws eCommerce_SDO_Core_Application_Exception
	 */
	public static function SearchProducts( eCommerce_Entity_Search $search, $categoryId = null, $inCategory = true, $extraConditions = array() , $joins = array()){
		
		return eCommerce_SDO_Core_Application_Product::Search( $search, $categoryId, $inCategory, $extraConditions , $joins );
	}
	
	public static function GetProductsByCategoryId( $categoryId , $limit=null){
		return eCommerce_SDO_Core_Application_ProductCategory::GetProductsByCategory( $categoryId, $limit );
	}
	public static function GetCategoriesByProduct($productId){
		return eCommerce_SDO_Core_Application_ProductCategory::GetCategoriesByProduct( $productId );
	}

	public static function AddProductToCategory( $productId, $categoryId ){
		return eCommerce_SDO_Core_Application_ProductCategory::AssociateProductToCategory( $categoryId , $productId );
	}
	
	public static function DeleteProductFromCategory( $productId, $categoryId ){
		return eCommerce_SDO_Core_Application_ProductCategory::DeleteProductCategoryAssociation(  $productId, $categoryId );
	}
	
	public static function GetArrProductsStatus($options = false){
		return eCommerce_SDO_Core_Application_Product::GetProductStatusArray($options);
	}
	
	public static function GetArrCategoryStatus($options = false){
		return eCommerce_SDO_Core_Application_Category::GetCategoryStatusArray($options);
	}
	
	public static function GetArrProductsNovedad($options = false){
			return eCommerce_SDO_Core_Application_Product::GetProductNovedadArray($options);
	}
	
	public static function GetArrProductsVendido($options = false){
		return eCommerce_SDO_Core_Application_Product::GetProductVendidoArray($options);
	}
	
public static function GetArrProductsTalla($options = false){
		return eCommerce_SDO_Core_Application_Product::GetProductTallaArray($options);
	}
	
	public static function SaveProductImage( eCommerce_Entity_Util_FileUpload $imageFile ){
		return eCommerce_SDO_Core_Application_FileManagement::SaveImageUpload( $imageFile, "Image for Product", "files/images/products/" );
	}
	
	public static function SaveCategoryImage( eCommerce_Entity_Util_FileUpload $imageFile ){
		return eCommerce_SDO_Core_Application_FileManagement::SaveImageUpload( $imageFile, "Image for Category", "files/images/categories/" );
		
	}

	public static function GetProductInLanguage( $productId, $language ){
		return eCommerce_SDO_Core_Application_ProductInDifferentLanguage::GetProductInLanguage( $productId, $language );
	}
	public static function GetAllLanguageOfProduct( $productId ){
		return eCommerce_SDO_Core_Application_ProductInDifferentLanguage::GetAll( $productId );
	}

	public static function IsProductASculpture($productId){
		return eCommerce_SDO_Core_Application_ProductCategory::IsProductASculpture($productId);
	}
	
	public function parseHTACCESLink($args,$type = ''){
		return eCommerce_SDO_Core_Application_Category::parseHTACCESLink($args,$type);
	}
	
	public function productParseHTACCESLink($args){
		return eCommerce_SDO_Core_Application_Product::parseHTACCESLink($args);
	}
	
	public function GetProductUnidad() {
		return eCommerce_SDO_Core_Application_Product::GetProductUnidad();
	}
	
	public function GetProductEntrega() {
		return eCommerce_SDO_Core_Application_Product::GetProductEntrega();
	}
	
}
?>