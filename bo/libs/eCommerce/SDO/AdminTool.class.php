<?php
class eCommerce_SDO_AdminTool{
	const ADMIN_TOOL_ID = eCommerce_SDO_Core_Application_AdminTool::ADMIN_TOOL_ID;
	/**
	 * Retrieves all the User Profiles filtered by an specific user role and search parameters. 
	 * If the role is Null or an empty string, 
	 *
	 * @param eCommerce_Entity_Search $search - The search parameters
	 * @param string $role                    - The Role to filter by. Empty for all roles.
	 * @return eCommerce_Entity_Search_Result_Profile
	 */
	public static function SearchAdminTool( eCommerce_Entity_Search $search, $extraConditions = array() ){
		return eCommerce_SDO_Core_Application_AdminTool::Search( $search, $extraConditions );
	}
	

	/**
	 * Load the User Profile from the given ID. Note an empty object is returned if the ID doesn't exist
	 *
	 * @param int $categoryId
	 * @return eCommerce_Entity_User
	 */
	public static function LoadAdminTool( $AdminToolId ){
		return eCommerce_SDO_Core_Application_AdminTool::LoadById( $AdminToolId ); 
	}
	
	public static function LoadActualAdminTool(){
		return eCommerce_SDO_Core_Application_AdminTool::LoadById( self::ADMIN_TOOL_ID ); 
	}
	
	/**
	 * Saves a new or updates an existing User Profile object
	 *
	 * @param eCommerce_Entity_User $profile
	 * @return eCommerce_Entity_User
	 * @throws eCommerce_SDO_Application_Exception
	 */
	public static function SaverAdminTool( eCommerce_Entity_AdminTool $AdminTool ){
		
		return eCommerce_SDO_Core_Application_AdminTool::Save( $AdminTool );
	}

	/**
	 * Deletes a User Profile from the System
	 *
	 * @param int $profileId                            - The ID of the user profile to be deleted
	 * @return eCommerce_Entity_Catalog_Category        - The recently deleted category
	 * @throws eCommerce_SDO_Core_Application_Exception - When the category is not found or couldn't be deleted
	 */
	public static function DeleteAdminTool( $AdminToolId ){
		return eCommerce_SDO_Core_Application_AdminTool::Delete( $AdminToolId ); 
	}
	
	/**
	 * Retrieves all the AdminTool available.
	 * 
	 * @return eCommerce_Entity_Util_AdminToolrray
	 */
	public static function GetAllAdminTool(){
		return eCommerce_SDO_Core_Application_AdminTool::GetAll();
	}
	

}
?>