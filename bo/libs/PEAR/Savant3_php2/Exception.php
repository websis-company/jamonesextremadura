<?php
/**
* 
* Throws PHP5 exceptions for Savant.
*
* @package Savant3
* 
* @author Paul M. Jones <pmjones@ciaweb.net>
* 
* @license http://www.gnu.org/copyleft/lesser.html LGPL
* 
* @version $Id: Exception.php,v 1.1 2008/07/07 16:01:49 Cyberal^ Exp $
* 
*/


/**
* 
* A simple Savant3_Exception class.
* 
* @package Savant3
* 
* @author Paul M. Jones <pmjones@ciaweb.net>
* 
*/

class Savant3_Exception extends Exception {
}
?>