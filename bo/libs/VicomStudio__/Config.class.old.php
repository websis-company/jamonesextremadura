<?php
/**
 * This class contains default configuration options
 * 
 * @author 
 * @copyright 
 */
class Config extends Object {

	public static function Initialize(){
		self::redirecDomain();
		self::SetConstants();
	}
	
	public static function getGlobalValue($value = 'project_name'){
		$config = self::getGlobalConfiguration();
		return $config[$value];
	}
	
	public static function getGlobalConfiguration(){
		define('TEST_EMAIL', true);
		$config = array();
		$protocol 	= strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https') === FALSE ? 'http' : 'https';		

		if(is_null($_SERVER['HTTPS'])){	
			$protocol 	="http";
		}else{
			$protocol 	="https";
		}
		
		if(SERVER_DEVELOPMENT){
			$config["ABS_HTTP_URL"] = $protocol."://{$_SERVER['HTTP_HOST']}/printproyect/web-02/";
		}elseif(SERVER_DEMO){
			$config["ABS_HTTP_URL"] = $protocol.'://www.vicomstudio.com/demos/printproyect/web-02/';
		}else{
			$config["ABS_HTTP_URL"] = $protocol."://{$_SERVER['HTTP_HOST']}/";
		}
		
		$config["project_name"] 	= 'Print Proyect';
		$config["project_email"] 	= TEST_EMAIL ? 'arturo@vicom.mx' : '';
		$config["email_root"] 		= TEST_EMAIL ? 'arturo@vicom.mx' :'';

		$config["smpt_server"] 			= TEST_EMAIL ? 'vicomstudio.com' 			:	'';
		$config["smpt_user"] 			= TEST_EMAIL ? 'arturo@vicom.mx'		:	'';
		$config["smpt_mail_from"] 		= TEST_EMAIL ? 'arturo@vicom.mx'		:	'';
		$config["smpt_pwd"] 			= TEST_EMAIL ? 'password'					:	'';
		$config["smpt_port_conection"] 	= TEST_EMAIL ? 26	 						:	 25;
		$config["smpt_name_from"] 		= 'printproyect';
		
		$config["project_domain"] 	= $_SERVER['HTTP_HOST'];
		$config["logo_project"] 	= ABS_HTTP_URL . "bo/backoffice/img/logo.png";
		$config["bakground_logo"] 	= "#EFEFEF";
		$config["color_th"] 		= "#0050A5";
		$config["color_td"] 		= "#008FD5";
		
		$config["color_login"] 		= "#000000";
		$config["bakground_logo"] 	= "#EFEFEF";
		$config["color_th"] 		= "#0050A5";
		$config["color_td"] 		= "#008FD5";
		
		$config["logo_email"]		= ABS_HTTP_URL . "bo/backoffice/img/banner-bo.jpg";
		
		
		$config["paypal_test"] 				= true;
		$config["paypal_cpp_header_image"] 	= $config["ABS_HTTP_URL"] . "img/layout/banner.jpg";
		//$config["paypal_input_image"] 		= $config["ABS_HTTP_URL"] . 'img/paypal.jpg';
		$config["paypal_input_image"] 		= $config["ABS_HTTP_URL"] . 'img/paypal.gif';
		$config["paypal_notify_url"] 		= 'bo/paypal_ipn.php';
		$config["paypal_item_name"] 		= 'Orden de Compra - '.$config["project_name"] ;
		$config["paypal_cmd"] 				= '_xclick';
		$config["paypal_test_business"] 	= 'tienda_mx_01@vicomstudio.com';
		$config["paypal_business"] 			= 'ventas@tecnocompra.mx';
		
		/*$config["deposito"]["beneficiario"] = 'TecnoCompra';
		$config["deposito"]["sucursal"] = 'Banco';
		$config["deposito"]["no_cuenta"] = 'xxxxxxxxxxxx';
		$config["transferencia"]["no_transf"] = 'xxxxxxxxxxxxxxxxxxxxxxxx';
		$config["deposito"]["clabe"] = 'xxxxxxxxxxxxxxxxxxxx';*/
		/*
		Access Code: 2C747464
		SecureSecret: 3BE6821E4F7240F440C857CB7086A2BB 
		*/
		/*if($_SERVER['REMOTE_ADDR']=="187.189.11.172"){
			$config["banamex_secureSecret"]		="3BE6821E4F7240F440C857CB7086A2BB" ;
			$config["banamex_vpc_AccessCode"]	= "2C747464" ;
			$config["banamex_vpc_Merchant"]		= "TEST1029460";
		}
		else{ */
			/*$config["banamex_secureSecret"]		="934EA359E5D40B8D925111337C1EF41F" ;
			$config["banamex_vpc_AccessCode"]	= "725CFE8C" ;
			$config["banamex_vpc_Merchant"]		= "1029460";*/
		//}

		/*CONFIGURACION PARA NUEVA CARGA DE IMAGENES*/
		$config["delete_type"] 	= 'POST';
		$config["db_host"] 		= 'localhost';
		$config["db_user"] 		= 'vicom_print';
		$config["db_pass"] 		= 'zN8N7OLABelL';
		$config["db_name"] 		= 'vicom_printproyect';
		$config["db_table"] 	= 'files';
		
		return $config;
	}
	

	public static function redirecDomain(){		
		$protocol 	= strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https') === FALSE ? 'http' : 'https';		
		$host		=  $_SERVER['HTTP_HOST'];		
		$uri		= $_SERVER['REQUEST_URI'];		
		$inFO		= strpos($uri, "backoffice") === false;					
		$permanently = true;		
		$conWWW		= false;		
		$conIndex	= false;		
		$domainStc 	= "vicomstudio.com";						
		$redirect	= false;				
		if($domainStc && strpos($host, $domainStc) === false){			
			$redirect = true;			
			$host = $domainStc;		
		}				
		if($conWWW && (strpos($host, 'www.') === false)){			
			$host = "www.$host";			
			$redirect = true;		
		}elseif(!$conWWW && (strpos($host, 'www.') !== false)){			
			$host =  str_replace('www.', '', $host);			
			$redirect = true;		
		}				
		if($conIndex && array_search($uri, array(ABS_HTTP_PATH,'')) !== false && $inFO){			
			$uri = ABS_HTTP_PATH."index.php";			
			$redirect = true;		
		}elseif(!$conIndex && strpos($uri, "index.php") !== false && $inFO){			
			$uri = ABS_HTTP_PATH;			
			$redirect = true;		
		}		
		if($_SERVER['HTTPS'] == 'on' && in_array(Util_Server::getScript(), array('catalogo.php','index.php'))){			
			$redirect = true;			
			$protocol = 'http';			
			$permanently = false;		
		}				
		/*if($_SERVER['HTTPS'] != 'on' && in_array(Util_Server::getScript(), array('detalle-02.php','detalle-publicitario.php','imagen.php'))){			
			$redirect = true;			
			$protocol = 'https';			
			$permanently = false;
		}*/
		if($redirect){			
			$url = "{$protocol}://{$host}{$uri}";			
		if($permanently)Header( "{$_SERVER['SERVER_PROTOCOL']} 301 Moved Permanently" );			
			Header( "Location: $url" );			
			die;		
			}	
	}

	protected  static function SetConstants(){
		$protocol 	= strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https') === FALSE ? 'http' : 'https';		
		if(is_null($_SERVER['HTTPS'])){	
			$protocol 	="http";
		}else{
			$protocol 	="https";		
		}

		if ( isset( $_SERVER['HTTP_HOST'] ) ){
			$serverDev =  self::IsServer( 'localhost', true )  
			           || self::IsServer( '127.0.0.1', true )
			           || self::IsServer( 'Admin2', true )
			|| self::IsServer( 'admin2', true );
		}
		else $serverDev = true;
		
		define( "SERVER_DEVELOPMENT", $serverDev );
		define( "SERVER_DEMO", self::IsServer( 'vicomstudio.com', true ) );
		define( "SERVER_PRODUCTION", self::IsServer( 'tecnocompra.com', true ) || self::IsServer('tecnocompra.vicomstudio.com',true) );
		
		if ( SERVER_DEVELOPMENT ){
			define('ABS_HTTP_URL', $protocol."://{$_SERVER['HTTP_HOST']}/printproyect/web-02/" );
			define('ABS_HTTP_PATH', "/printproyect/web-02/" );
		}
		elseif ( SERVER_DEMO ){
			define('ABS_HTTP_URL', $protocol."://vicomstudio.com/demos/printproyect/web-02/" );
			define('ABS_HTTP_PATH', "/demos/printproyect/web-02/" );
		}elseif ( SERVER_PRODUCTION ){
			define('ABS_HTTP_URL', $protocol."://" . (Config::IsServer( "www.$_SERVER[HTTP_HOST].com", true ) ? ("www.") : "") . "$_SERVER[HTTP_HOST]/" );
			define('ABS_HTTP_PATH', "/" );
		}
	}

	public static function IsServer( $httpHost = 'localhost', $isEqual = true ){
		if ( strpos( $_SERVER[ 'HTTP_HOST'], $httpHost ) !== false ){
			return ( $isEqual ) ? true : false;
		}
		else {
			return ( $isEqual ) ? false : true;
		}
	}
	
	
	public function sendEmail( $subject, $message, $email, $html = true){
		$config = config::getGlobalConfiguration();
		
		$asunto=$subject;
		
		$header ="From: " . $config['project_name'] . "<" . $config['project_email'] . ">\n";
		
		
		
		if($html){
			$header .="Content-Type: text/html; charset=utf-8\n";
			$body ="<font style='font-family:Arial,Helvetica,sans-serif;font-size:18px;color:#CC0000;font-weight:bold;'>". $config['project_name'] . "</font>\n" . $message;
		
			$body = '<p style="font-family:Arial;">' . nl2br($body) . '</p>';
		}else{
			$body =$message
			. "
			============================
			". $config['project_name'] ;
		} 
		$arrEmails = array();
		if( strpos($email, ',') ){
			$arrEmails = split(',',$arrEmails);
		}else{
			$arrEmails[] = $email;
			
		}
		$validator = new Validator_Test_Email();
		$result = count($arrEmails) > 0;
		foreach($arrEmails as $email){
			$email = strtolower($email);
			if( $validator->isValid($email) ){
				$result = $result && @mail($email, $asunto, $body, $header);
			}else{
				$result = false;
			}
		} 
		return $result;
	}
	public function sendAdminEmail( $subject, $message, $html = true){
		$config = config::getGlobalConfiguration();
		$email = $config["project_email"];
		
		return self::sendEmail( $subject, $message, $email, $html);
	}
	
}
?>