<?php
//define("DEVELOPMENT_TEST", false || $_SERVER['REMOTE_ADDR'] == '187.189.11.234');
define("DEVELOPMENT_TEST", false );
if( DEVELOPMENT_TEST ){//|| $_SERVER['REMOTE_ADDR'] == '189.203.203.29'
	ini_set("display_errors",1);
	error_reporting(E_ALL);
}else{
	error_reporting(0);
}

//error_reporting(E_ALL);


/**
 * This function loads ALL classes required by the application
 *
 * @param string $class_name
 * @return void
 */
function __autoload( $class_name ){

	$debug = DEVELOPMENT_TEST;
	$path = array ( );
	
	$pathToLib = 'libs/';
	
	if ( strpos( $class_name, 'Zend_') === 0 ){
		$path[] = $pathToLib . str_replace( "_", "/", $class_name ) . ".php";
	}
	else {
		$path[] = $pathToLib . str_replace( "_", "/", $class_name ) . ".class.php";
		$path[] = $pathToLib . "VicomStudio/" . str_replace( "_", "/", $class_name ) . ".class.php";
		$path[] = $pathToLib . "VicomStudio/Exceptions/" . str_replace( "_", "/", $class_name ) . ".class.php";
		$path[] = $pathToLib . "PEAR/" . str_replace( "_", "/", $class_name ) . ".php";
		$path[] = $pathToLib . "PhpMailer/class." . $class_name . ".php";
		$path[] = $pathToLib . "PayPal/" . str_replace( "_", "/", $class_name ) . ".php";
		$path[] = $pathToLib . "VPCPaymentConnection/" . str_replace( "_", "/", $class_name ) . ".class.php";
		$path[] = $pathToLib . "MercadoPago/" . str_replace( "_", "/", $class_name ) . ".php";
	}
	
	if ( $debug ){
		echo "Looking for <b>$class_name</b><br>";
	}
	
	$found = false;
	
	$include_paths = explode( PATH_SEPARATOR, get_include_path() );
	
	foreach ( $path as $filename ){
		if ( $debug ){
			echo "Filename: " . $filename . ' ... ';
		}
		if(DEVELOPMENT_TEST){
			if ( include_once ( $filename ) ){
				if ( $debug ){
					echo "<B>FOUND</B><br>";
				}
				return;
			} else{
				if ( $debug ){
					echo "Not found <br>";
				}
			}
		}else{
			if ( @include_once ( $filename ) ){
				if ( $debug ){
					echo "<B>FOUND</B><br>";
				}
				return;
			} else{
				if ( $debug ){
					echo "Not found <br>";
				}
			}
		}
	}
	if ( $debug ){
		echo "<br>";
	}
	if ( $found ){
		return true;
	}
	
	// The following line is required so no Fatal error is returned and the exception is launched
	eval( 'class ' . $class_name . ' extends ClassNotFoundException {}' );
	
	// Prepare the exception descriptive text.
	$txt = "The class <b>$class_name</b> could not be found under the following paths:<br>";
	$txt .= '<ul><li>' . join( '</li><li>', $path ) . '</li></ul>';
	
	throw new ClassNotFoundException( $txt );
}

/**
 * Debug function that writes to the output buffer the var_dump() information
 * from the given $var using $msg as label. The var_dump information is 
 * <pre>formatted.
 *
 * @param mixed $var The variable to debug
 * @param string $msg The label to prepend before the debug information
 */
function debug( $var, $msg = null ){
	echo '<pre>';
	echo $msg;
	ob_start();
	var_dump( $var );
	$debug = ob_get_contents();
	ob_end_clean();
	
	$debug = htmlentities( $debug );
	
	echo str_replace( "=>\n", "=>", $debug );
	
	echo '</pre>';
}

@session_start();

// IDENTIFICACI�N DE CHAR ENCODINGS SEG�N LA REGI�N
//setlocale(LC_CTYPE, 'en_US');  // Estados Unidos
//setlocale(LC_CTYPE, 'es_MX');  // M�xico
//setlocale(LC_CTYPE, 'fr_FR');  // Francia

define( 'ABS_PATH', dirname( __FILE__ ) );
define( 'SYSTEM_DIRECTORY', 'bo/');
define( 'BO_DIRECTORY', 'bo/');
define( 'DIRECTORY_BO_RELATIVE', 'bo/');
// Initialize default constants
Config::Initialize();

// Initialize the error handler
ExceptionHandler::initialize();

//define ("CHARSET_PROJECT", empty($CHARSET) ? "ISO-8859-1" : $CHARSET);
define ("CHARSET_PROJECT", empty($CHARSET) ? "UTF-8" : $CHARSET);
define ("CHARSET_PROJECT_DB", "utf8");

header('Content-Type: text/html; charset=' . CHARSET_PROJECT);

if( defined('LANGUAGE') ){
	$lang = eCommerce_SDO_Core_Application_LanguageManager::GetActualLanguage();
	if(LANGUAGE != $lang){
			eCommerce_SDO_Core_Application_LanguageManager::SetActualLanguage( LANGUAGE );
	}
}
if (defined('FO_CUSTOMER')){
	if (FO_CUSTOMER== true){		
		$user = eCommerce_FrontEnd_Util_UserSession::GetIdentity();		
		if ($user){			
			if ( $user->getRole() == eCommerce_SDO_Core_Application_Profile::ADMIN_ROLE){
				eCommerce_FrontEnd_Util_UserSession::DestroyIdentity();
			}
		}
	}
}
if (defined('BO_ADMIN')){		
	if (BO_ADMIN){
		$user = eCommerce_FrontEnd_Util_UserSession::GetIdentity();
		if ($user){
			if ( $user->getRole() != eCommerce_SDO_Core_Application_Profile::ADMIN_ROLE){
				eCommerce_FrontEnd_Util_UserSession::DestroyIdentity();
			}
		}			
	}				
}

?>