<?php
//eCommerce_FrontEnd_FO_Authentification::verifyAuthentification();
require_once('header.php');

$resultado	  = $this->result;
$Promociones = $resultado->getResults();
$hiddenFields = empty($this->hiddenFields) ? array() : $this->hiddenFields;
?>
<section class="flujo-compra">
<div class="container">
 <h2 align="center" class="h2 text-center">PROMOCIONES</h2>
	<hr class="hr" />
<div class="row">
<div class="col-xs-9 col-lg-10">
<section id="teaser3" class="">
    <div class="container">
   
      <div class="row">
      <?php $cont = 1; foreach($Promociones as $Promocion){
		  $titulo = $Promocion->getTitulo();
		  $href = $Promocion->getLink();
		  $img_p = $Promocion->getUrlImageId('medium',0);
		  ?>
        <div class="col-xs-4">
          <a href="<?=$href?>" class="thumbnail">
            <img class="img-responsive" src="<?=$img_p?>" alt="<?=$titulo?>" title="<?=$titulo?>" width="370" height="180" />
          </a>
        </div>
		<?php if($cont%3 == 0){ echo "<div style='clear:both; margin-top:50px'></div>";}
		$cont++;
		}?>
        
      </div>
      <center>
<nav>
  <ul class="pagination">
 <? eCommerce_SDO_Core_Application_Form::foPrintPager($resultado, "FormPromociones",  'normal', 'active', 'vertodos', 5, $hiddenFields, '', '' , false);?>
   
  </ul>
</nav>
</center>
    </div>
  </section>
  </div>

</div>
</div>
</section>

<?php include("toolbar-right.php"); ?>
<?php



require_once('footer.php');
?>
