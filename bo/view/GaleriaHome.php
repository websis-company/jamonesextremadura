<?php 
$Galeria = $this->result->getResults();
?>

<div class="c-layout-sidebar-content ">
    <!-- BEGIN: PAGE CONTENT -->
    <!-- BEGIN: CONTENT/SHOPS/SHOP-RESULT-FILTER-1 -->
    <!-- END: CONTENT/SHOPS/SHOP-RESULT-FILTER-1 -->
    <div class="margin-t-20"></div>
    <!-- BEGIN: CONTENT/SHOPS/SHOP-2-7 -->
    <div class="c-bs-grid-small-space">
        <div class="row">
        	<?php foreach ($Galeria as $galeria_item) {
				$img    = $galeria_item->getUrlArrayImages('medium',0);
				$nombre = $galeria_item->getNombre();
				$precio = $galeria_item->getPrecio();
        	?>
            <div class="col-md-3 col-sm-6 margin-b-20">
                <div class="c-content-product-2 c-bg-blanco c-border">
                    <div class="c-content-overlay">
                        <div class="c-overlay-wrapper">
                            <div class="c-overlay-content">
                                <a href="http://vicomstudio.com/demos/printproyect/web-02/detalle.php" class="btn btn-md c-btn-square c-btn-border-1x c-btn-bold c-btn-uppercase c-btn-grey-1">Ver</a>
                                <a href="http://vicomstudio.com/demos/printproyect/web-02/detalle.php" class="btn btn-md c-btn-square c-btn-border-1x c-btn-bold c-btn-uppercase c-btn-grey-1" title="compartir"> <i class="fa fa-facebook c-font-cuarto redes" title="compartir"></i></a>
                            </div>
                        </div>
                        <div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 230px; background-image: url(<?php echo $img; ?>);"></div>
                    </div>
                    <div class="c-info col-md-12">
                        <div class="col-md-9 col-xs-6">
                        	<p class="c-title c-font-16 c-font-slim c-center"><?php echo $nombre; ?></p>
                        </div>
                        <div class="col-md-1 c-font-septimo hidden-xs">|</div>
                        <div class="col-md-2 col-xs-6" align="center">
                        	<a href="#"><i class="fa fa-facebook c-font-sexto redes c-font-right" title="compartir"></i></a>
                        </div>
                    </div>
                    <div class="row c-center">
                        <div class="col-md-12 col-xs-12">
                        	<p class="c-price c-font-14 c-font-slim c-center">
                        		<?php echo "$ ".number_format($precio, 2, '.', ',');?>
                        		<!-- $548-<span class="c-font-14 c-font-cuarto">-$600</span> -->
                            </p>
                        </div>
                    </div>
                    <div class="btn-group btn-group-justified" role="group">
                        <div class="btn-group c-border-left c-border-top" role="group">
                            <a href="shop-cart.html" class="btn btn-sm c-btn-blanco c-btn-uppercase c-btn-square c-font-grey-3 c-font-blanco-hover c-bg-red-2-hover c-btn-product">Comprar</a>
                        </div>
                    </div>
                </div>
            </div>
        	<?php
        	}//end foreach ?>
        </div>
    </div>
    <!-- END: CONTENT/SHOPS/SHOP-2-7 -->
    <div class="margin-t-20"></div>
    <div class="pull-right">
        <ul class="c-content-pagination c-square c-theme">
            <?php eCommerce_SDO_Core_Application_Form::foPrintPager($this->result,"Form","","","",4,$hiddenFields);?>
        </ul>
    </div>
    <!-- END: PAGE CONTENT -->
</div>