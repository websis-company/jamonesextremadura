<?$IsLogged = eCommerce_FrontEnd_Util_UserSession::HasIdentity();?>
<?
$D_carrito   = IS_SPANISH ? "Carrito de Compra:":"Shopping Bag";
$D_productos = IS_SPANISH ? "productos":"products";
$D_sub_total = IS_SPANISH ? "Subtotal:":"Subtotal";
$D_cerrar	 = IS_SPANISH ? "Cerrar sesi&oacute;n":"Logout";
$D_accesa_tu = IS_SPANISH ? "Acceso a tu cuenta":"Login";
$D_mi_cuenta = IS_SPANISH ? "Mi cuenta":"My account";
$D_registrar = IS_SPANISH ? "Reg&iacute;strate":"New Customer";

$cart 		= eCommerce_SDO_Cart::GetCart();
$items 		= $cart->getItems();
$totalqty	= 0;
foreach($items as $item)$totalqty 	+= $item->getQuantity();

if($totalqty){

	$items = $cart->getItems();
	$actualCurrency = eCommerce_SDO_CurrencyManager::GetActualCurrency();
	$ArrProducts = array();
	foreach( $items as $item ){
		$product = eCommerce_SDO_Catalog::LoadProduct( $item->getProductId() );
	
		$value = eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $product->getPrice(), $product->getCurrency() );
		$ArrProduct["str_price"] = eCommerce_SDO_CurrencyManager::NumberFormat( $value) . " " . $actualCurrency ;
	
		$value = eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $product->getRealPrice(), $product->getCurrency() );
		$ArrProduct["str_price_with_discount"] = eCommerce_SDO_CurrencyManager::NumberFormat($value) . " " . $actualCurrency;
			
		$value = eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $product->getRealPrice(), $product->getCurrency() );
		
		$ArrProduct["quantity"] 	= $item->quantity;
		$quantity = $item->quantity;
						
		$productTotal = $value * $quantity;
		$subTotal += $productTotal;
	}
	
	$str_subtotal = eCommerce_SDO_CurrencyManager::NumberFormat($subTotal) . " " . $actualCurrency;

?>


<div id="n_cart" style="display:inline; " class="blanco12a">
 <span class="rosa12"><a href="<?=ABS_HTTP_URL.PATH_ACTUAL_LANG?>cart.php" class="rosa12" onmouseover="showCarrito()"><?=$D_carrito?></a> </span>
	<?=$totalqty?> <?=$D_productos?>&nbsp;&nbsp;

<span class="rosa12">|&nbsp;&nbsp;</span>

<span class="blanco12a"><?=$D_sub_total?>&nbsp;<?=$str_subtotal?>&nbsp;&nbsp;</span> 

<span class="rosa12">|&nbsp;&nbsp; </span>
    		<?if($IsLogged){?>
    			<a href="<?=ABS_HTTP_URL.PATH_ACTUAL_LANG?>login.php?cmd=logout" class="blanco12a"><?=$D_cerrar?></a>
    		<?}else{?>
    			<a href="<?=ABS_HTTP_URL.PATH_ACTUAL_LANG?>login.php" class="blanco12a">&nbsp;<?=$D_accesa_tu?></a>
    		<?}?>
<span class="rosa12"> |&nbsp;&nbsp; </span>
    		<?if($IsLogged){?>
    			<a href="<?=ABS_HTTP_URL.PATH_ACTUAL_LANG?>user.php" class="blanco12a">&nbsp;<?=$D_mi_cuenta?></a>
    		<?}else{?>
    			<a href="<?=ABS_HTTP_URL.PATH_ACTUAL_LANG?>user.php?cmd=register" class="blanco12a"><?=$D_registrar?></a>
    		<?}?>

</div>
<?}else{?>
<div id="n_cart" style="display:inline; " class="blanco12a"></div>
<?}?>