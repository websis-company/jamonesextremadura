<?php
if( ! @include_once( "header.php" ) ) {
	header('location:../index.php');
	die();
}
//include('menu_customer.php');
function dataToInput( $var ){
	echo "".$var;
}

$user = $this->user;
$address = $this->address;

$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
if ($actualLanguage == 'EN')
{
	$lang = 0;
}
else
{
	$lang = 1;
}

$title1[0] = "Account Information";
$title1[1] = "Informaci&oacute;n de Cuenta";

$title2[0] = "View my orders";
$title2[1] = "Ver mis &oacute;rdenes";

$title3[0] = "Account";
$title3[1] = "Cuenta";

$title4[0] = "Principal Address";
$title4[1] = "Direcci&oacute;n Principal";

$adds[0] = "Address";
$adds[1] = "Direcci&oacute;n";

$contra[0] = "Password";
$contra[1] = "Contrase&ntilde;a";

$name[0] = "First Name";
$name[1] = "Nombre(s)";

$last_name[0] = "Last Name";
$last_name[1] = "Apellido(s)";

$street[0] = "Street";
$street[1] = "Calle";

$city[0] = "City";
$city[1] = "Ciudad";

$state[0] = "State";
$state[1] = "Estado";

$country[0] = "Country";
$country[1] = "Pa&iacute;s";

$zip[0] = "Zip Code";
$zip[1] = "C&oacute;digo Postal";

$continue[0] = "Confirm Order";
$continue[1] = "Confirmar Orden";

$edit[0] = "Edit";
$edit[1] = "Editar";

$ch_psw[0] = "Change Password";
$ch_psw[1] = "Cambiar Contrase&ntilde;a";


?>

<link href="<?=ABS_HTTP_URL?>css/form.css" rel="stylesheet" type="text/css" />
<div class="container">
<section id="main" class="categoria">
<div class="row">
			<div class="row">
				<div class="col-xs-3 col-pc-30">
                <?php include "userInfoSidebar.php";?>
                
				</div>
                
                <div class="col-xs-9 col-pc-70">
                <div  class="row" id="product-list-wrapper" style="padding-right:0; margin:10px 0 25px;">
<fieldset style="background:#FFFFFF; width:100%; float:left; margin-right:10px; display:<?=($_REQUEST['cmdo']=='u' || $_REQUEST['cmdo'] == '')?'block':'none'?>" id="personalinfo">
		<legend style="background:#FFFFFF"><?php echo $title1[$lang]; ?></legend>
		
		<dl class="form">
			<dt>E-mail: </dt>
			<dd><?php echo dataToInput( $user->getEmail() ) ?></dd>
			
			<dt><?php echo $contra[$lang]; ?>: </dt>
			<dd><a href='user_info.php?cmd=edit' class="rojo14"><?php echo $ch_psw[$lang]; ?></a></dd>
			<dt>Nombre: </dt>
			<dd><?php echo dataToInput( $user->getFirstName() ) ?></dd>
			<dt><?php echo $last_name[$lang]; ?>: </dt>
			<dd><?php echo dataToInput( $user->getLastName() ) ?></dd>
            <dt>Saludo: </dt>
			<dd><?php echo dataToInput( $user->getSaludo() ) ?></dd>
            <dt>Teléfono principal: </dt>
			<dd><?php echo dataToInput( $address->getPhone() ) ?></dd>
            <dt>Teléfono alterno: </dt>
			<dd><?php echo dataToInput( $address->getPhone2() ) ?></dd>
            <dt>&nbsp;</dt>
			<dd>&nbsp;</dd>
		</dl>
		<br />
		<a href='user_info.php?cmd=edit' class="btn btn-default btn-block" style="line-height:20px; font-size:12px"><?php echo $edit[$lang]; ?></a></p>
</fieldset>
			<fieldset style="background:#FFFFFF; width:100%; display:<?=($_REQUEST['cmdo']=='d')?'block':'none'?>" id="direccioninfo">
		<legend style="background:#FFFFFF"><?php echo $title4[$lang]; ?></legend>
		
		<dl class="form">
			<dt><?php echo $adds[$lang]; ?>: </dt>
			<dd><?php echo dataToInput( $address->getStreet().", ".$address->getStreet3().", Col. ".$address->getStreet2() ) ?></dd>
			
			<dt><?php echo $zip[$lang]; ?>: </dt>
			<dd><?php echo dataToInput( $address->getZipCode() ) ?></dd>
			
			<dt><?php echo $city[$lang]; ?>: </dt>
			<dd><?php echo dataToInput( $address->getCity() ) ?></dd>
			
			<dt><?php echo $country[$lang]; ?>: </dt>
			<dd><?php 
			$country = eCommerce_SDO_CountryManager::GetCountryById( $address->getCountry() );
			dataToInput( $country["name"] ) ?></dd>
			
			<dt><?php echo $state[$lang]; ?>: </dt>
			<dd><?php 
			$state = eCommerce_SDO_CountryManager::GetStateById( $address->getState(),$address->getCountry() );
			echo $state["name"]; ?></dd>
		</dl>
		<br />
		<a href='user.php?cmd=editAddress' class="btn btn-default btn-block" style="line-height:20px; font-size:12px"><?php echo $edit[$lang]; ?></a>
</fieldset>
                </div>
                
                
                </div>
</div>
</div>		
</section>
</div>
<?php
include_once( "footer.php" );
?>
