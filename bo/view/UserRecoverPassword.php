<?php
if( eCommerce_FrontEnd_FO_Authentification::verifyAuthentification( false ) ){

	eCommerce_FrontEnd_FO_Authentification::goToFirstPage();
	
}

include_once( "header.php" );
include('menu_customer.php');

$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
if ($actualLanguage == 'EN')
{
	$lang = 0;
}
else
{
	$lang = 1;
}

$top_msg[0] = "Recover Password";
$top_msg[1] = "Recuperar Contraseña";

$usr[0] = "Email";
$usr[1] = "Email";

$pss[0] = "Password";
$pss[1] = "Contrase&ntilde;a";

$btn[0] = "Enviar";
$btn[1] = "Enviar";

$btm_msg[0] = "All access attempts will be recorded for security purposes";
$btm_msg[1] = "Todos los intentos de acceso ser&aacute;n almacenadors por razones de seguridad";

$forget[0] = "Forgot your password?";
$forget[1] = "Olvidó su contraseña?";

$clickhere[0] = "Click Here";
$clickhere[1] = "Click Aquí";

$areYou[0] = "If you are not registered";
$areYou[1] = "Si no se ha registrado";

$regiserhere[0] = "Click Here";
$regiserhere[1] = "Regístrese aquí";
 
?>



<table align="center" cellpadding="0" cellspacing="0" width="100%">
	<tr>
<!--     	<td align="center" class="error"> 
        	
        </td> -->
    </tr>
    <tr>
        <td>&nbsp;
        
        </td>
 	</tr>
  	<tr>
        <td>&nbsp;
        
        </td>
  	</tr>
</table>

<br />
<br />
<table style="margin-bottom: 0pt;" align="center" border="0" cellpadding="0" cellspacing="2" width="875" height="332">
  <tbody>
    <tr>
      <td width="530" height="328">
	  <form method="post" action="" style="margin:0px;">
          <table border="0" align="center" width="340">
            <tr>
              <td colspan="2" align="center" bgcolor="#AF8554" style="color: #fff; font-weight: bold;"><?php echo $top_msg[$lang]; ?> </td>
            </tr>
            <tr>
              <td><?php echo $usr[$lang]; ?>:</td>
              <?if($this->msg){?><div class="alert <?=$this->msgClass?>"><button type="button" class="close" data-dismiss="alert">×</button><strong><?=$this->msg?></strong></div><?}?>
              <td><input id="usuario" type="text" tabindex="1" size="20" name="email" /></td>
            </tr>
            <tr>
              <td colspan="2" align="center">
			  <input type='hidden' name='cmd' value='recoverpwd' />
			  <input type='hidden' name='save' value='1' />
                  <input name="submit" type="submit" style="color: #FFFFFF; font-size: 8pt; border-style: solid; border-width: 1px; padding-left: 4px; padding-right: 4px; padding-top: 1px; padding-bottom: 1px; background-color: #AF8554" value="<?php echo $btn[$lang]; ?>" />
              </td>
            </tr>
          </table>
	    </form>
          <table align="center">
            <tr>
              <td><i><?php echo $btm_msg[$lang]; ?></i> </td>
            </tr>
          </table>
          <br />
          <br /></td>
      <td bgcolor="#af8554" valign="top" width="1"><br />
          <br />
      </td>
      <td valign="middle" width="336"><table align="right" bgcolor="#916736" border="0" cellpadding="0" cellspacing="0" width="263" height="80">
        <tbody>
          <tr>
            <td height="80"><p class="texto2" align="center"><?=$areYou[$lang]?></p>
                  <table align="center" bgcolor="#f4f4f4" border="0" cellpadding="0" cellspacing="2" width="134" height="25">
                    <tbody>
                      <tr>
                        <td width="122"><div align="center"><span class="texto2"><strong><a href="user.php?cmd=register"><?=$regiserhere[$lang]?> &#9658;</a></strong></span></div></td>
                      </tr>
                    </tbody>
                  </table></td>
          </tr>
        </tbody>
      </table></td>
    </tr>
  </tbody>
</table>

<?php
include_once( "footer.php" );
?>