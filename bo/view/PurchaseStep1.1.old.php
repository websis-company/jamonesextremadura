﻿<?php 
$header_title = 'Env&iacute;o y Facturaci&oacute;n';
include_once "includes/head.php";
include_once "includes/header.php";
/** USER **/
$userProfile           = eCommerce_FrontEnd_Util_UserSession::GetIdentity();
$userRole              = $userProfile->getRole();
$userProfilePassword   = $userProfile->getPassword();
$userProfileId         = $userProfile->getProfileId();
$userProfileEmail      = $userProfile->getEmail();
$userProfileName       = $userProfile->getFirstName();
$userProfileLastName   = $userProfile->getLastName();
$userProfileAddress    = eCommerce_SDO_User::LoadUserAddress( $userProfileId );
$userPhone             = $userProfileAddress->getPhone();
$userCP                = $userProfileAddress->getZipCode();
$userStreet            = $userProfileAddress->getStreet();
$userExterior          = $userProfileAddress->getStreet3();
$userInterior          = $userProfileAddress->getStreet4();
$userColonia           = $userProfileAddress->getStreet2();
$userProfileConfirmado = $userProfile->getConfirmado();
$userEstado            = $userProfileAddress->getState();
$userCiudad            = $userProfileAddress->getCity();
$userMunicipio         = $userProfileAddress->getMunicipio();

if(is_null($userProfileAddress->getContacto()) || $userProfileAddress->getContacto() == ""){
	$userContacto = $userProfileName." ".$userProfileLastName;
}elseif($userProfileAddress->getContacto() != ""){
	$userContacto = $userProfileAddress->getContacto();	
}else{
	$userContacto = "";	
}

/** USER **/
if($userRole == eCommerce_SDO_User::ANONYMOUS_ROLE){
	$disablefields = '';  
  //$cutString = ($_SESSION['Auth'] == 'true' )?false:true;
	$cutString = ($_SESSION['Auth'] == 'true' || empty($userProfileName) || empty($userProfileLastName) || empty($userCP) || empty($userStreet))?false:true;
	$logOut = "<br/><a class='button-flat primary' href='".ABS_HTTP_URL."login.php?cmd=logout' style='font-size: 12px;'>No soy yo. Cerrar sesión</a>";
}else{
	$disablefields = '';
  //$cutString = ($_SESSION['Auth'] == 'true')?false:true;
	$cutString = ($_SESSION['Auth'] == 'true' || empty($userProfileName) || empty($userProfileLastName) || empty($userCP) || empty($userStreet))?false:true;
	$logOut = "<br/><a class='button-flat primary' href='".ABS_HTTP_URL."login.php?cmd=logout' style='font-size: 12px;'>No soy yo. Cerrar sesión</a>";
}

$msgResponse  = $this->msgResponse;
$cart         = $this->cart;
$message      = $this->message;
$messageClass = $this->messageClass;
$isEmptyCart  = $this->isEmptyCart;
$billTo       = $cart->getOrderAddressByType( eCommerce_SDO_Cart::ADDRESS_BILL_TO );
$ship         = $this->ship;
$bill         = $this->bill;
$attr_ship    = 'errors'.eCommerce_SDO_Cart::ADDRESS_SHIP_TO;
$attr_bill    = 'errors'.eCommerce_SDO_Cart::ADDRESS_BILL_TO;
$errorsShip   = $this->$attr_ship;
$errorsBill   = $this->$attr_bill;
//$errorsBill =  $this->errors;
//$errorsShip 	= $this->errors;
//if($errors)$errorDescription = $errors->getDescription();
$requiere_fac    = $cart->getRequiereFactura();
$actualLanguage  = eCommerce_SDO_LanguageManager::GetActualLanguage();
$spanishLanguage = eCommerce_SDO_LanguageManager::GetValidLanguage('SP');
$spanishLanguage = $spanishLanguage["id"];
if ($actualLanguage == 'EN'){
	$lang = 0;
}
else{
	$lang = 1;
}

$edit_cart[0]    = "Edit Cart";
$edit_cart[1]    = "Editar Carrito";
$bill_address[0] = "Bill Address";
$bill_address[1] = "Direcci&oacute;n de Facturaci&oacute;n";
$street[0]       = "Street";
$street[1]       = "Calle";
$city[0]         = "City";
$city[1]         = "Ciudad";
$state[0]        = "State";
$state[1]        = "Estado";
$country[0]      = "Country";
$country[1]      = "Pa&iacute;s";
$zip[0]          = "Zip Code";
$zip[1]          = "C&oacute;digo Postal";
$continue[0]     = "Continue";
$continue[1]     = "Continuar";
$pur_1[0]        = "Billing address and Shipping address";
$pur_1[1]        = "Dirección de facturación y envío";

function ocultaCadena($string, $tipo){
	$arr = str_split($string);
	if($tipo == 'S'){
		$c = 0;
		foreach ($arr as $val) {
			if($c <= 2){
				$str[$c] = $val;
			}else{
				$str[$c] = '*';
			}
			$c++;
		}
		$cadena = implode('', $str);
	}elseif($tipo == 'N'){
		$length = count($arr);
		$corte  = $length - 4;
		$c = 1;
		foreach ($arr as $val) {
			if($c <= $corte){
				$str[$c] = '*';
			}else{
				$str[$c] = $val;
			}
			$c++;
		}
		$cadena = implode('', $str);
	}
	return $cadena;
}

?>




<div class="c-layout-page page-inside">
	<section class="c-content-box c-size-md">
		<div>
			<div class="container">
				<h1><strong>Mi compra</strong></h1>
			</div>
			<div class="container">
				<div class="row-fluid">
					<div>
						<!--ALERTA DE CONFIRMACION DE SOLICITUD-->
						<?php if(!empty($msgResponse)){?>
						<div>
							<?php echo $msgResponse;?>
						</div>
						<?php }//end if?>

						<!-- Mensaje Cupón-->
						<?php 
							if(isset($_SESSION['payuError'])){
							?>
							<div class="alert alert-danger">
								<?=$_SESSION['payuError'];?>
							</div>
						<?php
							unset($_SESSION['payuError']);
							}		
						?>
						<!-- // -->
						
						<!-- Mensaje Cupón-->
						<?php 
							if(isset($_SESSION['cuponMsjSuccess'])){
							?>
							<div class="alert alert-success">
								<?=$_SESSION['cuponMsjSuccess'];?>
							</div>
						<?php
							unset($_SESSION['cuponMsjSuccess']);
							}			
						?>
						<!-- // -->
					</div>

					<form method="post" action="" name="formPayment" id="formPayment" class="re">
						<input type='hidden' id="nextStep" name='nextStep' value='4' />
						<input type='hidden' id="actualStep" name='actualStep' value='5' />
						<input type='hidden' name='cmd' value='saveStep' />
						<input type="hidden" name="paymentMethod" id="paymentMethod" value="" />
						
						<div class="row checkout-steps">
							<!-- Carrito de compras -->
							<section id="paso1" class="col-lg-4 checkout-step-01">
								<!-- <img class="img-responsive" src="<?=ABS_HTTP_URL?>img/layout/titulo-carrito.jpg" width="357" alt=""> -->
								<h1>Mis productos</h1>

								<!-- CARRITO -->
								<div class="minicart">
								<?php
								if( !$isEmptyCart ){
									echo eCommerce_SDO_Cart::GetHTML(false,false,true,false,false,true);
								}//end if
								?>
								</div>
								<!-- CARRITO -->	
							</section>

							<!-- Datos de envio -->
							<section id="paso2" class="col-md-6 col-lg-4 checkout-step-02">
								<fieldset>
									<legend>Identificación</legend>
									<p>Solicitamos únicamente la información esencial para la finalización de la compra.</p>

									<!-- Identificación. Begin -->
									<?php if($cutString === true){?>
									<!-- User -->
									<div class="current-data">
										<label>Correo</label>
										<span><?=$userProfileEmail?></span>
										<?=$logOut?>
									</div>

									<div class="current-data">
										<label>Nombre(s)</label>
										<span><?php echo ocultaCadena($userProfileName,'S')?></span>
										<input type="hidden" name="nombre"  id="nombre" value="<?=$userProfileName?>" >
									</div>

									<div class="current-data">
										<label>Apellido(s)</label>
										<span><?php echo ocultaCadena($userProfileLastName,'S')?></span>
										<input type="hidden" name="apellido"  id="apellido" value="<?=$userProfileLastName?>" >
									</div>

									<div class="current-data">
										<label>Teléfono/Móvil</label>
										<span><?php echo ocultaCadena($userPhone,'N')?></span>
										<input type="hidden" name="ship[phone]"  id="phone" value="<?=$userPhone?>">
									</div>

									<!-- New user -->
									<?php }else{ ?>
									<div class="control-wrapper">
										<input type="email" placeholder="" name="email"  id="email" value="<?=$userProfileEmail?>" required="required">
									</div>

									<div class="control-wrapper">
										<input type="text" placeholder="Nombre(s)" name="nombre"  id="nombre" value="<?=$userProfileName?>" required="required">
									</div>

									<div class="control-wrapper">
										<input type="text" placeholder="Apellido(s)" name="apellido"  id="apellido" value="<?=$userProfileLastName?>" required="required">
									</div>

									<div class="control-wrapper">
										<input type="text" placeholder="Teléfono" name="ship[phone]"  id="phone" value="<?=$userPhone?>" required="required">
									</div>
									<?php } ?>
								</fieldset>
								<!-- Identificación. End -->

								<!-- Shipping. Begin -->
								<fieldset>
									<legend>Ingresa tus datos de  envío</legend>

									<?php if($cutString === true){?>
									<div class="current-data">
										<label>Código Postal</label>
										<span><?php echo ocultaCadena($userCP,'S')?></span>
										<input type="hidden" name="ship[zip_code]" id="zip_code" value="<?=$userCP?>">
									</div>

									<div class="current-data">
										<label>Calle</label>
										<span><?php echo ocultaCadena($userStreet,'S')?></span>
										<input type="hidden" name="ship[street]"  id="street" value="<?=$userStreet?>">
									</div>

									<div class="current-data float-left">
										<label>No. Exterior</label>
										<span><?php echo ocultaCadena($userExterior,'S')?></span>
										<input type="hidden" name="ship[street3]" id="exterior" value="<?=$userExterior?>" >
									</div>

									<div class="current-data float-right">
										<label>No. Interior</label>
										<span><?php echo ocultaCadena($userInterior,'S')?></span>
										<input type="hidden" name="ship[street4]" id="interior" value="<?=$userInterior?>" >
									</div>

									<div class="current-data">
										<label>Colonia</label>
										<span><?php echo ocultaCadena($userColonia,'S')?></span>
										<input type="hidden" name="ship[street2]" id="colonia" value="<?=$userColonia?>">
									</div>

									<div class="current-data">
										<label>Estado</label>
										<span><?php echo ocultaCadena($userEstado,'S')?></span>
										<input type="hidden" name="ship[state]" id="state" value="<?=$userEstado?>">
									</div>

									<div class="current-data">
										<label>Ciudad</label>
										<span><?php echo ocultaCadena($userCiudad,'S')?></span>
										<input type="hidden" name="ship[city]" id="ciudad" value="<?=$userCiudad?>"> 
									</div>

									<div style="display: none;">
										<label>Municipio</label>
										<span><?php echo ocultaCadena($userMunicipio,'S')?></span>
										<input type="hidden" name="ship[municipio]" id="municipio" value="<?=$userMunicipio?>"> 
									</div>

									<div class="current-data">
										<label>Nombre de la persona que va a recibir</label>
										<span><?php echo ocultaCadena($userContacto,'S')?></span>
										<input type="hidden" name="ship[recibe]"  id="recibe" value="<?=$userContacto?>">
									</div>

									<div>
										<div onclick="<?=($userProfileConfirmado == 'No')?'showSetPassword();':'showFormLogin();'?>" class="button-flat primary">Editar Información</div>
									</div>

									<?php }else{?>
									<div class="control-wrapper">
										<input type="text" placeholder="Calle" name="ship[street]"  id="street" value="<?=$userStreet?>" required="required">
									</div>

									<div class="control-wrapper float-left">
										<input type="text" placeholder="No. Ext." name="ship[street3]"  id="exterior" value="<?=$userExterior?>" required="required">
									</div>
									<div class="control-wrapper float-right">
										<input type="text" placeholder="No. Int." name="ship[street4]"  id="interior" value="<?=$userInterior?>">
									</div>				
									<div class="control-wrapper">
										<input type="text" placeholder="Colonia" name="ship[street2]"  id="colonia" value="<?=$userColonia?>" required="required">
									</div>
									<div class="control-wrapper">
										<input type="text" placeholder="C. P." name="ship[zip_code]"  id="zip_code" value="<?=$userCP?>" required="required">
									</div>						
									<div class="control-wrapper">
									<? $states = eCommerce_SDO_CountryManager::GetStatesOfCountry( 'MX', true, 'ship[state]', $ship->getState(),'shipstate','form-control', '', true, '' );
									echo ( !is_array($states)) ? $states : $states["div"];
									$errorsShip->getHtmlError("state"); ?>
									</div>
									<div class="control-wrapper">
										<div id="shipCity">
											<input type="text" placeholder="Ciudad" name="ship[city]"  id="ciudad" value="<?=$userCiudad?>" required="required">
										</div>
									</div>

									<div class="control-wrapper" style="display: none;">
										<input type="text" placeholder="Municipio / Delegación" name="ship[municipio]"  id="municipio" value="<?=$userMunicipio?>">
									</div>

									<div class="control-wrapper">
										<input type="text" placeholder="Nombre de quien recibe" name="ship[recibe]"  id="recibe" value="<?=trim($userContacto);?>">
									</div>
									<?php }?>
								</fieldset>
								<!-- Shipping. End -->

								<div class="control-wrapper">
									<!-- <span>Tiempo de entrega : 2 - 5 <a id="showDivDias" href="javascript:void(0);">d&iacute;as h&aacute;biles</a></span> -->
									<!--<br/>
									<div>
										<a id="my-tooltip" href="javascript:void(0);"  title="Políticas de compra">Pol&iacute;ticas</a> de compra
									</div>
									<br>-->
								</div>
							</section>
					
							<!-- Información de pago -->
							<section id="paso3" class="col-md-6 col-lg-4 checkout-step-03">
								<legend>Ingresa tu información de pago</legend>
								<ul class="nav nav-tabs" role="tablist">
									<li id="pagoLinea" role="presentation" class="active">
										<a aria-controls="" role="tab" data-toggle="tab">Pago en Línea</a>
									</li>
									<li id="deposito" role="presentation">
										<a aria-controls="" role="tab" data-toggle="tab">Depósito en Efectivo (Oxxo)</a>
									</li>
								</ul>

								<div class="payment-methods">
									<div style="display:none;" id="pagoLineaTxt">
										<p align="center">
											<img src="images/layout/formas-de-pago-mercadopago.png" alt="mercado pago" class="img-cien" />
										</p>
										<div>
											<div id="msjsdk" class="alert alert-danger" style="display: none;"></div>
										</div>

										<input type="hidden" id="cardmember" name="cardmember" placeholder="" value="<?php echo $userProfileEmail;?>"/>
										<div>
											<label for="cardNumber">Número de Tarjeta</label>
											<div class="control-wrapper">
												<input type="text" id="cardNumber" data-checkout="cardNumber" placeholder="" value="" minlength="15" maxlength="16" />
											</div>
										</div>
										<div>
											<label for="cardholderName">Nombre del titular de la tarjeta</label>
											<div class="control-wrapper">
												<input type="text" id="cardholderName" data-checkout="cardholderName" placeholder="" value="" />
											</div>
										</div>
										<div>
											<label>Fecha de vencimiento</label>
											<div class="control-wrapper float-left">
												<select id="cardExpirationMonth" data-checkout="cardExpirationMonth">
													<option value="">Mes</option>
													<?php 
													for ($i=1; $i <= 12; $i++) { 
														if(strlen($i) == 1){
															$i = '0'.$i;
														}
														echo '<option value="'.$i.'">'.$i.'</option>';
													}
													?>
												</select>
											</div>
											<div class="control-wrapper float-right">
												<select id="cardExpirationYear" data-checkout="cardExpirationYear">
													<option value="">A&ntilde;o</option>
													<?php
													$Y = date('Y');
													$last = $Y+10;
													for ($i=$Y; $i <= $last; $i++) { 
														echo '<option value="'.$i.'">'.$i.'</option>';
													}
													?>
												</select>                                                                
											</div>
										</div>

										<div>
											<label for="securityCode">Código de Seguridad</label>
											<div class="control-wrapper float-left">
												<input type="password" id="securityCode" data-checkout="securityCode" placeholder="****" value="" minlength="3" maxlength="4"/>
											</div>
											<div class="control-wrapper float-right">
												<img src="images/layout/cvv.png" alt="credit_card_security_code" />
											</div>
										</div>
										<!--<div>
											<div>
												<button type="button" class="button-flat primary" onClick="sendDatos();">Aceptar</button>
												<button type="button" class="button-flat secondary" onClick="back('paso2','paso3');">Cancel</button>
											</div>
										</div>-->
									</div>

									<div style="display:none;" id="depositoTxt">
										<div>
										<?php 
											/**
											* Consulta para obtener medios con pago en efectivo
											* @link https://www.mercadopago.com.mx/developers/es/solutions/payments/custom-checkout/charge-with-other-methods#get-payment-methods
											**/
										$mp              = new Mercadopago ("APP_USR-8120775165163701-121516-aa4a6a3ec2fc4dd3a8205dd3ea6b3bf2__LC_LB__-200395647");
										$payment_methods = $mp->get ("/v1/payment_methods");
										 $options         = array();
										 foreach($payment_methods['response'] as $method_ticket){
												#-- Guarda opciones de pago en efectivo activos
												if($method_ticket['payment_type_id'] == "ticket" && $method_ticket['name'] == 'Oxxo' && $method_ticket['status'] == "active"){
													$options[] = $method_ticket;
												}//end if
										}
										?>
										</div>
										<div class="col-xs-12" align="center">
											<img src="<?php ABS_HTTP_URL ?>images/layout/oxxo-mercadopago.png" class="img-responsive">
											<h6>Nota: Usted tiene 7 días para realizar el pago en tiendas oxxo, pasado dicho tiempo el ticket expira y no podras realizar el pago.</h6>
											<p><?php 
												#-- Muestra opciones de pago
												foreach($options as $option){
												?>
												<input type="hidden" id="<?php echo $option['id'];?>" name="medio_pago" value="<?php echo $option['id'];?>"/>
												<?php }//end foreach?>
											</p>

											<!-- <div>
												<div>
													<button type="button" class="button-flat primary" onClick="sendDatos();">Aceptar</button>
													<button type="button" class="button-flat secondary" onClick="back('paso2','paso3');">Cancel</button>
												</div>
											</div> -->
										</div>
									</div>
								</div>

								<!-- Totales -->
								<div id="divTotal" class="checkout-summary">
									<?php 
									$items          = $cart->getItems();	

									$subTotal       = 0;
									$actualCurrency = eCommerce_SDO_CurrencyManager::GetActualCurrency();

									foreach ($items as $item) {
										$productTmp = explode('|',$item->getProductId());																					
										$product      = eCommerce_SDO_Catalog::LoadVersion( $productTmp[1] );
										
										$value        = eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $item->getPrice(), $product->getCurrency() );
										//$value        = eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $product->getRealPrice(), $product->getCurrency() );

										$productTotal = $value * $item->quantity;
										$subTotal     += $productTotal;
									}

									$totalProductos = eCommerce_SDO_CurrencyManager::NumberFormat($subTotal) . " " . $actualCurrency;
									$total  = eCommerce_SDO_Cart::GetTotalCart(true,true);

										//$extras = eCommerce_SDO_Cart::GetExtras($total['quantity']);
									$extras = eCommerce_SDO_Cart::GetExtras($total['price']['amount']);
									?>

									<table class="checkout-summary-table">
										<tbody>
											<tr>
												<td class="td-label">Orden <?=$total['quantity'];?></td>
												<td class="td-value"><?=$totalProductos;?></td>
											</tr>
											<tr>
												<td class="td-label">Costo de envío</td>
												<td class="td-value"><?php
													foreach ($extras as $value) {														
														echo eCommerce_SDO_CurrencyManager::NumberFormat($value->getAmount())." ".$actualCurrency;
													}?>
												</td>
											</tr>
											<!--<tr>
												<td class="td-label">IVA </td>
												<td class="td-value">
													<?php 
													//$pIva =  $subTotal * 0.16;
													//echo eCommerce_SDO_CurrencyManager::NumberFormat($pIva)." ".$actualCurrency;?>
												</td>
											</tr>-->
											<?php
											#-- Descuento 
												if(isset($_SESSION['cupon_descuento']) && $_SESSION['cupon_descuento'] == 'true'){
													$descuento  = $subTotal * 0.10;
													$total_str = $total['price']['amount'];
													//$descuento = $total_str * 0.10;
													$total = eCommerce_SDO_CurrencyManager::NumberFormat(($total_str - $descuento))." ".$actualCurrency;
												}else{
													$descuento = 0.00;
													$total_f = $total['price']['format'];
													$amount = $total["price"]["amount"];
												}
											?>
											<!-- <tr>
												<td class="td-label">Descuento 10%</td>
												<td class="td-value">- <?php //echo eCommerce_SDO_CurrencyManager::NumberFormat($descuento)." ".$actualCurrency;?></td>
											</tr> -->
										</tbody>
									</table>

									<div class="total-payment">
										<label>Total</label>
										<span><?=$total_f;?></span>
										<input type="hidden" name="precio_total" id="precio_total" value="<?php echo $amount; ?>">
									</div>

									<div class="text-center">
										<input type="checkbox" id="condiciones" name="condiciones" onclick="showDivDondiciones();"/>
										He leído <a href="<?=ABS_HTTP_URL?>aviso-privacidad.php" target="_blank">Política de privacidad</a> y <a href="<?=ABS_HTTP_URL?>terminos.php" target="_blank">Términos y condiciones</a>
										<div id="divCondiciones" class="alert alert-danger" style="display: none; color: #FFF;">Debe leer y aceptar los terminos y condiciones</div>
									</div>
								</div>

								<a id="comprar" class="button-flat primary" style="display:none; cursor: pointer;">Confirmar pago seguro</a>
								<!-- <input id="comprar" style="display:none;" type="image" name="pagar" src="<?=ABS_HTTP_URL?>img/layout/btn-confirmarpago.png" width="282" height="58" alt=""/> -->
								<!-- Totales -->
							</section>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>

<!-- Fancy Iframes -->
<div id="alertUserFind" style="display: none;">
	<div class="container" style="background-color: #0186C3; height: 100%">
		<div class="row">
			<div class="col-md-6 col-xs-12 col-sm-7">
				<!--<div style="background-color: #0186C3; height: 100%;">-->
				<div style="padding-top: 25px;" class="col-md-4 col-xs-4 col-sm-4">
					<img alt="access" src="<?php echo ABS_HTTP_URL?>img/ICONO-CANDADO.png" width="125" height="125" class="img-responsive"/>
				</div>
				<div style="padding: 10px; color: #FFF;" class="col-md-7 col-sm-8 col-xs-8">
					<span style="font-family: Arial; font-size: 18px; font-weight: bold;">Perfil Detectado !</span><br/>
					<span style="font-family: Arial; font-size: 15px; font-weight: normal;">
						Hemos detectado un perfil asociado al correo que has ingresado. Para continuar comprando bajo este perfil, por favor da clic al botón de "Seguir Comprando".
					</span>
					<div class="clearfix"></div>
					<br/>
					<div style="font-weight: normal; padding-top: 0px;">
						<button style="color: #000; font-family: Arial; font-size: 15px;" class="btn btn-default" onclick="closeFancy();">
							Seguir Comprando
						</button>
					</div>
				</div>
				<!--</div>-->
			</div>
		</div>
	</div>
</div>

<div id="formLogin" style="display:none;">
	<div class="container" style="width:100%; font-family:Arial">
		<div class="row">
			<div class="col-md-12">
				<br>
				<p><strong><?php echo $userProfileEmail;?></strong></p>
				<p style="font-size: 18px;">Ingresar contraseña</p>
				<p>
					<form id="formLogin" name="formLogin" method="post" action="">
						<input type="hidden" name="cmdLogin" value="loginForm" />
						<div class="pull-left">
							<label for="password" style="color: black; font-size: 18px;">Contraseña :</label>
						</div>
						<div class="pull-right">
							<a class="cerrarSesion" style="cursor:pointer; font-weight: normal; font-size: 18px; color:#00BEFF" onclick="showFormPassword();">Olvid&eacute; mi contrase&ntilde;a</a>
						</div>
						<div class="form-group col-xs-12">
							<input type="password" id="passwordLogin" name="passwordLogin" placeholder="password" class="form-control" required="required" style="border: 1px #00BEFF; height: 35px;">
						</div>
						<div class="clearfix"></div>
						<!--<div class="pull-right">
							<a style="cursor:pointer; font-weight: normal; font-size: 20px;" onclick="showSetPassword()">No sabe o no recuerda cuál? Registrala ahora</a>
						</div>-->
						<div class="clearfix"></div>
						<div class="pull-right">
							<button id="btnLogin" type="submit" class="btn btn-primary btn-block" style="padding:10px">Ingresar</button>
						</div>
					</form>
				</p>
			</div>  
		</div>
	</div>
</div>

<div id="formPassword" style="display:none;">
	<div class="">
		<div class="" style="font-size: 15px; font-family:Arial; width:100%">
			<div class="col-md-12">
				<br>
				<p><strong><?php echo $userProfileEmail;?></strong></p>
				<p>Ingresar email</p>
				<p>
					<form id="formLogin" name="formLogin" method="post" action="">
						<input type="hidden" name="cmdLogin" value="recoverPassword" />
						<div class="pull-left">
							<label for="password" style="color: black;">Email :</label>
						</div>
						<div class="col-md-12">
							<input type="email" id="email" name="email" class="col-md-12 form-control" required="required" style="border: 1px solid #00BEFF;">
						</div>
						<div class="clearfix"></div>
						<br>
						<div class="pull-right">
							<button id="btnLogin" type="submit" class="btn btn-editar" >Enviar</button>
						</div>
					</form>
				</p>
			</div>
		</div>
	</div>
</div>

<div id="formSetPassword" style="display:none">
	<div class="container" style="width:100%; font-family:Arial">
		<div class="row">
			<div class="col-md-12">
				<br>
				<p><i class="spritecart identificacion"></i><span style="font-size: 18px; font-weight: bold;">Le enviaremos una clave de seguridad a su correo electrónico</span></p>
				<p style="font-size: 18px; margin: 0;">Registrar una nueva contraseña</p><br />
				<form id="formLogin" name="formLogin" method="post" action="" style="margin: 0;">
					<input type="hidden" name="cmdLogin" value="setPassword" />
					<div class="pull-left">
						<label for="password" style="color: black; font-size: 15px;">Nueva contraseña :</label>
					</div>
					<div class="col-md-12">
						<input type="password" id="password" name="password" class="col-md-12 form-control" required="required" pattern=".{6,10}" title="6 a 10 caracteres mínimo" style="border: 1px solid #00BEFF;">
					</div>
					<div class="pull-left">
						<label for="password" style="color: black; font-size: 15px;">Confirmar nueva contraseña :</label>
					</div>
					<div class="col-md-12">
						<input type="password" id="passwordconfirm" name="passwordconfirm" class="col-md-12 form-control" required="required" pattern=".{6,10}" title="6 a 10 caracteres mínimo" style="border: 1px solid #00BEFF;">
						<span style="font-family: Arial !important; font-size: 13px; !important">El password debe contener 6 caracteres como m&iacute;nimo</span>
					</div>
					<div class="clearfix"></div>
					<br>
					<div class="pull-right">
						<button id="btnLogin" type="submit" class="btn btn-editar">Enviar</button>
					</div>
				</form>
			</div>  
		</div>
	</div>
</div>

<!-- <div id="formCuponDescuento" style="display:none">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<br>
				<p style="font-size: 20px;"><i class="spritecart identificacion"></i>Ingrese el código del cupón para obtener el 10% de descuento en el total de su compra</p>
				<form id="formLogin" name="formLogin" method="post" action="<?//=ABS_HTTP_URL?>cart.php" style="margin: 0;">
					<input type="hidden" name="cmd" value="agregaCupon" />
					<div class="pull-left">
						<label for="cupon" style="color: black; font-size: 20px;">C&oacute;digo :</label>
					</div>
					<div class="form-group col-xs-12">
						<input type="text" id="cupon" name="cupon" class="col-md-12 form-control" required="required" style="border: 1px solid #6901A0; height: 35px; font-size: 18px;">
					</div>
					<div class="clearfix"></div>
					<div class="pull-right">
						<button id="btnLogin" type="submit" class="btn btn-editar" style="margin-top: 0px;">Enviar</button>
					</div>
				</form>
			</div>  
		</div>
	</div>	
</div> -->
<!-- // -->


<?php 
$GLOBALS["scripts"]  .= "
    <script>
    /** Funcion Inicio de Pago GTM*/
    add_event('orderPlaced');</script>";
?>
<?php include("includes/footer.php"); ?>
	
	<!-- Mercado Pago SDK -->
	<script type="text/javascript" src="https://secure.mlstatic.com/sdk/javascript/v1/mercadopago.js"></script> 
	<script type="text/javascript" src="<?=ABS_HTTP_URL?>assets/plugins/tokenmercadopago.js"></script>
	<script type="text/javascript" src="<?=ABS_HTTP_URL?>assets/base/js/validate/dist/localization/messages_es.js"></script>

	<script type="text/javascript">
		$("#comprar").click(function(event){
			if(!$('#condiciones').prop('checked') ) {
				$("#divCondiciones").slideDown();
				return false;
			}else{
				$("#divCondiciones").hide();
				var met_pago = $("#paymentMethod").val();
				if(met_pago == "deposito"){
					$("#formPayment").validate({debug: false}); // Valida campos obligatorios
					$('html, body').animate({
						scrollTop: $("#paso2").offset().top
					}, 1000);
					$("#formPayment").submit();
				}else if(met_pago == "en_linea"){
					/*
					* Válida campos obligatorios y envia información para generar token 
					* @link https://www.mercadopago.com.mx/developers/es/solutions/payments/custom-checkout/charge-with-creditcard/javascript#get-token
					*/
					var campos          = new Array();
					campos['nombre']    = $("#nombre").val();
					campos['apellido']  = $("#apellido").val();
					campos['phone']     = $("#phone").val();
					campos['street']    = $("#street").val();
					campos['exterior']  = $("#exterior").val();
					campos['colonia']   = $("#colonia").val();
					campos['zip_code']  = $("#zip_code").val();
					campos['shipstate'] = $("#shipstate").val();
					campos['ciudad']    = $("#ciudad").val();
					campos['recibe']    = $("#recibe").val();

					for (i in campos) {
						if(campos[i] == ""){
							$("#"+i+"-error").remove();
							$("#"+i).addClass('error');
							$("#nombre").focus();
							$("#"+i).after('<label id="'+i+'-error" class="error" for="'+i+'">Este campo es obligatorio.</label>');
						}
					}//end for

					$("#nombre").keyup(function(e){
						if($("#nombre").val().length >= 1){
							$("#nombre").removeClass('error');
							$("#nombre-error").remove();
						}else{
							$("#nombre-error").remove();
							$("#nombre").addClass('error');
							$("#nombre").after('<label id="nombre-error" class="error" for="nombre">Este campo es obligatorio.</label>');
						}//end if
					});//end function

					$("#apellido").keyup(function(e){
						if($("#apellido").val().length >= 1){
							$("#apellido").removeClass('error');
							$("#apellido-error").remove();
						}else{
							$("#apellido-error").remove();
							$("#apellido").addClass('error');
							$("#apellido").after('<label id="apellido-error" class="error" for="apellido">Este campo es obligatorio.</label>');
						}//end if
					});//end function

					$("#phone").keyup(function(e){
						if($("#phone").val().length >= 1){
							$("#phone").removeClass('error');
							$("#phone-error").remove();
						}else{
							$("#phone-error").remove();
							$("#phone").addClass('error');
							$("#phone").after('<label id="phone-error" class="error" for="phone">Este campo es obligatorio.</label>');
						}//end if
					});//end function

					$("#street").keyup(function(e){
						if($("#street").val().length >= 1){
							$("#street").removeClass('error');
							$("#street-error").remove();
						}else{
							$("#street-error").remove();
							$("#street").addClass('error');
							$("#street").after('<label id="street-error" class="error" for="street">Este campo es obligatorio.</label>');
						}//end if
					});//end function

					$("#exterior").keyup(function(e){
						if($("#exterior").val().length >= 1){
							$("#exterior").removeClass('error');
							$("#exterior-error").remove();
						}else{
							$("#exterior-error").remove();
							$("#exterior").addClass('error');
							$("#exterior").after('<label id="exterior-error" class="error" for="exterior">Este campo es obligatorio.</label>');
						}//end if
					});//end function

					$("#colonia").keyup(function(e){
						if($("#colonia").val().length >= 1){
							$("#colonia").removeClass('error');
							$("#colonia-error").remove();
						}else{
							$("#colonia-error").remove();
							$("#colonia").addClass('error');
							$("#colonia").after('<label id="colonia-error" class="error" for="colonia">Este campo es obligatorio.</label>');
						}//end if
					});//end function	

					$("#zip_code").keyup(function(e){
						if($("#zip_code").val().length >= 1){
							$("#zip_code").removeClass('error');
							$("#zip_code-error").remove();
						}else{
							$("#zip_code-error").remove();
							$("#zip_code").addClass('error');
							$("#zip_code").after('<label id="zip_code-error" class="error" for="zip_code">Este campo es obligatorio.</label>');
						}//end if
					});//end function

					$("#shipstate").keyup(function(e){
						if($("#shipstate").val().length >= 1){
							$("#shipstate").removeClass('error');
							$("#shipstate-error").remove();
						}else{
							$("#shipstate-error").remove();
							$("#shipstate").addClass('error');
							$("#shipstate").after('<label id="shipstate-error" class="error" for="shipstate">Este campo es obligatorio.</label>');
						}//end if
					});//end function

					$("#ciudad").keyup(function(e){
						if($("#ciudad").val().length >= 1){
							$("#ciudad").removeClass('error');
							$("#ciudad-error").remove();
						}else{
							$("#ciudad-error").remove();
							$("#ciudad").addClass('error');
							$("#ciudad").after('<label id="ciudad-error" class="error" for="ciudad">Este campo es obligatorio.</label>');
						}//end if
					});//end function

					$("#recibe").keyup(function(e){
						if($("#recibe").val().length >= 1){
							$("#recibe").removeClass('error');
							$("#recibe-error").remove();
						}else{
							$("#recibe-error").remove();
							$("#recibe").addClass('error');
							$("#recibe").after('<label id="recibe-error" class="error" for="recibe">Este campo es obligatorio.</label>');
						}//end if
					});//end function

					if(campos['nombre'] == "" || campos['apellido'] == "" || campos['phone'] == "" || campos['street'] == "" || campos['exterior'] == "" || campos['colonia'] == "" || campos['zip_code'] == "" || campos['shipstate'] == "" || campos['ciudad'] == "" || campos['recibe'] == "" ){//|| campos['recibe'] == ""
						return false;
					}else{
						doSubmit = false;
						var $form = document.querySelector('#formPayment');
						// La función "sdkResponseHandler" esta definida en tokenmercadopago.js
						Mercadopago.createToken($form, sdkResponseHandler); 
						return false;	
					}//end if
				}//end if
			}//end if
		});
	</script>

	<script type="text/javascript">
		$("#facturasi").click(function(){		
			$("#facturacion").slideDown();
			document.getElementById("razon_social").required = true;
			document.getElementById("rfc").required = true;
			document.getElementById("billstreet").required = true;
			document.getElementById("billstreet3").required = true;
			document.getElementById("billstreet2").required = true;
			document.getElementById("billzip_code").required = true;
			document.getElementById("billcity").required = true;
			document.getElementById("billstate").required = true;
			document.getElementById("billphone").required = true;
		});

		$("#facturano").click(function(){
			$("#facturacion").slideUp();
			document.getElementById("razon_social").required = false;
			document.getElementById("rfc").required = false;
			document.getElementById("billstreet").required = false;
			document.getElementById("billstreet3").required = false;
			document.getElementById("billstreet2").required = false;
			document.getElementById("billzip_code").required = false;
			document.getElementById("billcity").required = false;
			document.getElementById("billstate").required = false;
			document.getElementById("billphone").required = false;

		});

		$("#deposito").click(function(){
			var precio_total   = $("#precio_total").val();
			if(precio_total > parseFloat(10000)){
				alert('Lo sentimos el pago no puede procesarce en tiendas oxxo ya que el monto excede lo permitidio por transacción');
				return false;
			}else{
				clearClasses(); hideAllPayments(); showPayment('d');	
			}
		});

		$("#pagoLinea").click(function(){clearClasses(); hideAllPayments(); showPayment('pl');});

		function clearClasses(){
			$("#deposito").removeClass('active');
			$("#pagoLinea").removeClass('active');
		}//end function

		function hideAllPayments(){
			$("#depositoTxt").css('display','none');
			$("#pagoLineaTxt").css('display','none');
			$("#paymentMethod").val('');
		}//end function

		function showPayment(payment){
			switch(payment){
				case 'd': 
				$("#comprar").css('display','none');
				$("#divTotal").css('display','none');
				$("#depositoTxt").slideDown(function(e){
					$("#deposito").addClass('active');
					$("#comprar").css('display','block');
					$("#paymentMethod").val('deposito');
					$("#divTotal").css('display','block');
					$("#msjsdk").css('display','none');
				});
				break;
				case 'pl':
				$("#comprar").css('display','none');
				$("#divTotal").css('display','none');
				$("#pagoLineaTxt").slideDown(function(e){
					$("#pagoLinea").addClass('active');
					$("#comprar").css('display','block');
					$("#paymentMethod").val('en_linea');
					$("#divTotal").css('display','block');
					$("#msjsdk").css('display','none');
				});                     
				break;
				default:
				break;  
			}
		}
	</script>

	<script type="text/javascript">
		$(".fancybox").fancybox();
	</script>

	<?php
	$findUser = eCommerce_FrontEnd_Util_Session::Get('find_user');
	if($findUser == 'true' && (!empty($userProfileName) || !empty($userProfileLastName) || !empty($userCP) || !empty($userStreet))){
		eCommerce_FrontEnd_Util_Session::Delete('find_user');
	?>
	<script type="text/javascript">
		$(document).ready(function(e){
			var div =  $("#alertUserFind").html();
			$.fancybox({
				padding	: 0,
				width	: '560px',
				height	: '280px',
				autoSize : false,
				closeBtn : false,
				helpers : { 'overlay' :{'closeClick': false,}},
				type : "iframe",
				scrolling: 'no',
				content: div,
			});	


		});
	</script>
	<?php } ?>

	<script type="text/javascript">
		$(document).ready(function(){
			showPayment('pl');

			$("#shipstate").change(function(){
				var estado = $("#shipstate").val();
				msgdataShip = $("#shipCity");
				if(estado != ''){
					$.ajax({
						type: "POST",
						data: "estado="+$("#shipstate").val()+"&tipo=ship",
						url: "<?=ABS_HTTP_URL?>ciudades.php?estado="+$("#shipstate").val()+"tipo=ship",
						beforeSend: function(){								
							msgdataShip.html("<img src='<?=ABS_HTTP_URL?>img/loading.gif' alt='Cargando' title='Cargando' width='30' />");
						},
						success: function(result){					 
							msgdataShip.html(result);						 
						}			
					});
					return false;
				}
			});

			$("#billstate").change(function(){
				var estado = $("#billstate").val();
				msgdataBill = $("#billCity");
				if(estado != ''){
					$.ajax({
						type: "POST",
						data: "estado="+$("#billstate").val()+"&tipo=bill",
						url: "<?=ABS_HTTP_URL?>ciudades.php?estado="+$("#billstate").val()+"tipo=bill",
						beforeSend: function(){								
							msgdataBill.html("<img src='<?=ABS_HTTP_URL?>img/layout/loading.gif' alt='Cargando' title='Cargando' width='30' />");
						},
						success: function(result){					 
							msgdataBill.html(result);						 
						}			
					});
					return false;
				}
			});
		});	//end function

		function showSetPassword(){
			var div =  $("#formSetPassword").html();
			$.fancybox({
				padding : 0,
				width : '400px',
				height  : '350px',
				autoSize : false,
				closeBtn : true,
				helpers : { 'overlay' :{'closeClick': false,}},
				type : "iframe",
				scrolling: 'no',
				content: div,
			});
		}//end function

		function showFormLogin(){
			var div =  $("#formLogin").html();
			$.fancybox({
				padding : 0,
				width : '400px',
				height  : '314px',
				autoSize : false,
				closeBtn : true,
				helpers : { 'overlay' :{'closeClick': false,}},
				type : "iframe",
				scrolling: 'no',
				content: div,
			});
		}//end function

		function showFormPassword(){
			$.fancybox.close();

			setTimeout(function(){
				var div =  $("#formPassword").html();
				$.fancybox({
					padding : 0,
					width : '400px',
					height  : '314px',
					autoSize : false,
					closeBtn : true,
					helpers : { 'overlay' :{'closeClick': false,}},
					type : "iframe",
					scrolling: 'no',
					content: div,
				});
			}, 500);
			
		}//end function

		function showCuponDescuento(){
			var div = $("#formCuponDescuento").html();
			$.fancybox({
				padding : 0,
				width : '400px',
				height  : '314px',
				autoSize : false,
				closeBtn : true,
				helpers : { 'overlay' :{'closeClick': false,}},
				type : "iframe",
				scrolling: 'no',
				content: div,
			});
		}//end function

		function updateQuantity( productId ){
			fieldQuantityName = 'quantity_' + productId;
			quantity       = document.getElementById( fieldQuantityName ).value;
			var nextStep   = $("#nextStep").val();
			var actualStep = $("#actualStep").val();
			document.location.href = 'cart.php?cmd=update&productId=' + productId + '&' + fieldQuantityName + '='+quantity+'&purchase=true|'+nextStep+'|'+actualStep;
		}

		function showFormLogin(){
			var div =  $("#formLogin").html();
			$.fancybox({
				padding : 0,
				width : '400px',
				height  : '314px',
				autoSize : false,
				closeBtn : true,
				helpers : { 'overlay' :{'closeClick': false,}},
				type : "iframe",
				scrolling: 'no',
				content: div,
			});
		}

		function showDivDondiciones(){
			if($('#condiciones').prop('checked') ) {
				$("#divCondiciones").hide();			
				return false;			
			}
		}


		function closeFancy(){
			$.fancybox.close();
		}


		function addValue(input){
			var value = $('#quantity_'+input).val();
			if(value == ""){
				value = 0;
			}
			var total = parseFloat(value) + parseFloat(1);
			if(total <= 1){
				total = 1;
			}
			var value = $('#quantity_'+input).val(total);
		}

		function removeValue(input){
			var value = $('#quantity_'+input).val();
			if(value == ""){
				value = 0;
			}		
			var total = parseFloat(value) - parseFloat(1);
			if(total <= 1){
				total = 1;
			}
			var value = $('#quantity_'+input).val(total);
		}

		function selectOption(option){
			$("#"+option).prop("checked", true);
		}	
	</script>
<div class="clearfix"></div>