<?php
include_once ( "header.php" );
//include('menu_customer.php');
$errors    = $this->errors;
$order   = $this->order; 

if( !empty($order) ){
	
	$orderHtml = $this->orderHtml;
	$addresses = $order->getAddresses();
	
	
	$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
	if ($actualLanguage == 'EN')
	{
		$lang = 0;
	}
	else
	{
		$lang = 1;
	}
	
	$ordr[0] = "Order";
	$ordr[1] = "&Oacute;rden";
	
	$edit_cart[0] = "Edit Cart";
	$edit_cart[1] = "Editar Carrito";
	
	$bill_address[0] = "Bill Address";
	$bill_address[1] = "Direcci&oacute;n de Facturaci&oacute;n";
	
	$ship_address[0] = "Ship Address";
	$ship_address[1] = "Direcci&oacute;n de Env&iacute;o";
	
	$cust_info[0] = "Customer Information";
	$cust_info[1] = "Informaci&oacute;n del cliente";
	
	$street[0] = "Street";
	$street[1] = "Calle";
	
	$city[0] = "City";
	$city[1] = "Ciudad";
	
	$estado[0] = "State";
	$estado[1] = "Estado";
	
	$pais[0] = "Country";
	$pais[1] = "Pa&iacute;s";
	
	$zip[0] = "Zip Code";
	$zip[1] = "C&oacute;digo Postal";
	
	$continue[0] = "Confirm Order";
	$continue[1] = "Confirmar Orden";
	
	$edit_bill[0] = "Edit Bill Adress";
	$edit_bill[1] = "editar Direcci&oacute;n de Facturaci&oacute;n";
	
	$edit_ship[0] = "Edit ship Adress";
	$edit_ship[1] = "editar Direcci&oacute;n de Env&iacute;o";
	
	$pur_3[0] = "Purchase 3 - Ship Address";
	$pur_3[1] = "Paso 3 - Direcci&oacute;n de env&iacute;o";
	
	$method[0] = "Payment Method";
	$method[1] = "M&eacute;todo de Pago";
	
	$comments[0] = "Comments";
	$comments[1] = "Comentarios";
	
	$status[0] = "Order status";
	$status[1] = "Estado de la &oacute;rden";
	
	$add_inf[0] = "Additional Information";
	$add_inf[1] = "Informaci&oacute;n adicional";
	
	$name[0] = "Name";
	$name[1] = "Nombre";
	
	$last_name[0] = "Last Name";
	$last_name[1] = "Apellido";
	
?>
<link href="<?=ABS_HTTP_URL?>css/form.css" rel="stylesheet" type="text/css" />
<div class="container">
<section id="main" class="categoria">
<div class="row">
			<div class="row">
				<div class="col-xs-3 col-pc-30">
                <?php include "userInfoSidebar.php";?>
                
				</div>
                
                <div class="col-xs-9 col-pc-70">
                <div  class="row" id="product-list-wrapper" style="padding-right:0; margin:10px 0 25px;">

<form name="frmOrder" id="frmProduct" method="post" action="order.php" onsubmit="" enctype="multipart/form-data">

  <div class="error">
		<?php //$errors->getDescription(); ?>
  </div>
	
	<fieldset>
		<legend><?php echo $ordr[$lang] ?></legend>
		<?php echo $orderHtml; ?>
	</fieldset>
	<br />
    <?php $address  = $addresses[ eCommerce_SDO_Order::ADDRESS_BILL_TO ];
	if(!empty($address)){
	?>
	<fieldset>
		<legend><?php echo $bill_address[$lang] ?></legend>
		<dl class='form'>
		<?php
			
							
			$country = eCommerce_SDO_CountryManager::GetCountryById( $address->getCountry() );
			$state = eCommerce_SDO_CountryManager::GetStateById( $address->getState(), $country["country_short_name"] );
			$html =
			"<dt>".$street[$lang].":</dt><dd>". $address->getStreet() . "</dd>" .
			"<dt>".$zip[$lang].":</dt><dd>". $address->getZipCode() . "</dd>" .
			"<dt>".$city[$lang].":</dt><dd>". $address->getCity() . "</dd>" .
			"<dt>".$pais[$lang].":</dt><dd>". $country["name"] . "</dd>" .
			"<dt>".$estado[$lang].":</dt><dd>". $state["name"] . "</dd>" .
			"";
			echo $html;

			
		?>
		</dl>
	</fieldset>
    <?php }?>
		<br />
	<fieldset>
		<legend><?php echo $ship_address[$lang] ?></legend>
		<dl class='form'>
		<?php
			$address  = $addresses[ eCommerce_SDO_Order::ADDRESS_SHIP_TO ];
			
			$country = eCommerce_SDO_CountryManager::GetCountryById( $address->getCountry() );
			$state = eCommerce_SDO_CountryManager::GetStateById( $address->getState(), $country["country_short_name"] );
			$html =
			"<dt>".$street[$lang].":</dt><dd>". $address->getStreet() . "</dd>" .
			"<dt>".$zip[$lang].":</dt><dd>". $address->getZipCode() . "</dd>" .
			"<dt>".$city[$lang].":</dt><dd>". $address->getCity() . "</dd>" .
			"<dt>".$pais[$lang].":</dt><dd>". $country["name"] . "</dd>" .
			"<dt>".$estado[$lang].":</dt><dd>". $state["name"] . "</dd>" .
			"";
			
			echo $html;
			
			
			
		?>
		</dl>
	</fieldset>
	<br />
	
	<fieldset>
		<legend><?php echo $cust_info[$lang] ?></legend>
		<dl class='form'>
		<?php
		$profile = eCommerce_SDO_User::LoadUserProfile( $order->getProfileId() );
		
		$html =
		"<dt>Nombre:</dt><dd>". $profile->getFirstName(). "</dd>" .
		"<dt>Apellidos:</dt><dd>". $profile->getLastName(). "</dd>" .
		"<dt>E-mail:</dt><dd>". $profile->getEmail() . "</dd>" .
		"<dt></dt><dd></dd>";
		
		echo $html;
		?>
		</dl>
	</fieldset>
	<br />
	<fieldset>
		<legend><?php echo $comments[$lang] ?></legend>
		<?php echo $order->getComments(); ?>
	</fieldset>
	
	<br />
		<fieldset>
		<legend><?php echo $add_inf[$lang] ?></legend>
		
		<dl class="form">
			<dt><?php echo $method[$lang] ?></dt>
			<dd><?php echo eCommerce_SDO_Order::GetPaymentMethodById( $order->getPaymentMethod() ); ?></dd>
			<br />
			
			<dt><?php echo $status[$lang] ?>&nbsp;</dt>
			<dd>
				<?php
				echo eCommerce_SDO_Order::GetStatusById( $order->getStatus() );
				?>
			</dd>
		</dl>
	</fieldset>
<p align="right"><a href="<?=ABS_HTTP_URL?>user_info.php?cmd=listOrders" class="btn btn-primary" style="padding:10px">Regresar a mis ordenes</a></p>
</form>
 </div>
                
                
                </div>
</div>
</div>		
</section>
</div>
<?php 
}

include_once ( "footer.php" );
?>