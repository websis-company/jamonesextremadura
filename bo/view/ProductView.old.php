<?php
$product = $this->product;
$precios = $this->precios;
$maximo  = "$".eCommerce_SDO_Core_Application_CurrencyManager::numberFormat(max($precios));
$minimo  = "$".eCommerce_SDO_Core_Application_CurrencyManager::numberFormat(min($precios));

//$category = $this->category;
//$brand = $this->brand;
$Versiones          = $this->Versiones;
$VersionesAgrupadas = $this->VersionesAgrupadas;
$Colors             = $this->Colors;

//$Tallas = $this->Tallas;
$baseFriendlyUrl     = ABS_HTTP_URL . 'producto/';
$baseListFriendlyUrl = ABS_HTTP_URL . 'productos/';
$skuProd             = trim($product->getSku());
$descrProd           = nl2br(trim($product->getDescriptionShort()));
$nameProd            = $product->getName();
$header_title        = !empty($nameProd) ? $nameProd : $skuProd;
$header_description  = Util_String::subText(trim($product->getDescriptionShort()), 200, 'chars', '');
$header_keywords     = '';

if(!empty($category)) {
	$header_title      .= ' | ';
	$header_title_path = array();
	$productPathUrl    .= $category->getCategoryId().'/';
	$pagingPathUrl     .= $category->getCategoryId().'/';
	foreach($this->categoriesTree as $c) {
		$name                = trim($c->getName());
		$header_title_path[] = $name;
		$friendlyName        = Util_FriendlyURL::formatText($name);
		$productPathUrl      .= $friendlyName . '/';
		$pagingPathUrl       .= $friendlyName . '/';
	}
	$header_title .= implode(' - ', $header_title_path);
} else if(!empty($brand)) {
	$name           = trim($brand->getName());
	$header_title   .= ' | '.$name;
	$friendlyName   = Util_FriendlyURL::formatText($name);
	$productPathUrl .= $brand->getBrandId().'/'.strtolower($brand->getName()).'-'.$friendlyName.'/';
	$pagingPathUrl  .= $brand->getBrandId().'/'.strtolower($brand->getName()).'-'.$friendlyName.'/';
}

include("header.php");

$descripcion_short = utf8_encode($product->getDescriptionShort());
$descripcion_long  = nl2br($product->getDescriptionLong());
$precio            = $product->getPrice();
$src               = $product->getUrlImageId('medium',0);
$src_0             = $product->getUrlImageId('large',0);
$src_small         = $product->getUrlImageId('small',0);
$imas              = $product->getArrayImages();

?>
<script language="javascript" type="text/javascript">
	function changeImg(cual,cuantas){
		if(cuantas > 0){
			for(i=0;i<=cuantas;i++){
				document.getElementById('pic'+i).style.display = 'none';	
			}	
		document.getElementById('pic'+cual).style.display = 'block';
		}	
	}
	function changeThumb(cual,cuantas){
		if(cuantas > 0){
			for(i=0;i<=cuantas;i++){
				document.getElementById('pic_sm'+i).style.display = 'block';	
			}	
		document.getElementById('pic_sm'+cual).style.display = 'none';
		}		
	}
</script>
<link rel="stylesheet" type="text/css" href="<?=ABS_HTTP_URL?>js/shadow/shadowbox.css">
<script type="text/javascript" src="<?=ABS_HTTP_URL?>js/shadow/shadowbox.js"></script>
<script type="text/javascript">
	Shadowbox.init();
</script>

<!--Microdatos-->
<div itemscope itemtype="http://schema.org/IndividualProduct" itemid="#product" style="display:none">
<span itemprop="name"><?=$nameProd?></span> 
<span itemprop="description"><?=$descrProd?></span>
<span itemprop="image"><?=$src?></span>
<span itemprop="sku"><?=$skuProd?></span>
<span itemprop="url"><?=$product->getFriendlyNameUrl()?></span>

<div itemprop="Brand" itemscope itemtype="http://www.schema.org/Brand">
        <span itemprop="name">Ecobambú</span>    
        <span itemprop="url"><?=ABS_HTTP_URL?></span>    
        <span itemprop="logo"><?=ABS_HTTP_URL?>img/layout/logo.png</span>    
    </div>

</div>
<!--Microdatos-->
<!-- Marcado JSON-LD generado por el Asistente para el marcado de datos estructurados de Google. -->
<script type="application/ld+json">
{
  "@context" : "http://schema.org",
  "@type" : "Product",
  "name" : "<?=$nameProd?>",
  "image" : "<?=$src?>",
  "description" : "<?=$descrProd?>",
  "offers" : {
    "@type" : "Offer",
    "price" : "<?=$minimo?> - <?=$maximo?>"
  }
}
</script>

<div id="main" class="int" >
<div class="container">

<div class="row">

<div class="span7" style="display:block" id="pic0" align="center">
  <a  href="<?=$src_0?>" rel="shadowbox" title="<?=$nameProd?>"> <img src="<?=$src?>" style="max-width:540px;max-height:540px" alt="<?=$nameProd?>"></a>
</div>
<?php $total_img=0; 
	
	foreach($imas as $img){
	  $imagen = $product->getUrlImagesId('large',$total_img);
?>
    <div class="span7" style="display:none" id="pic<?=($total_img+1)?>" align="center">
     <img src="<?=$imagen?>"  style="max-width:540px;max-height:540px" alt="<?=$nameProd?>">
    </div>
<?php $total_img++; }
	
?>

<div class="span5 texto-detalle">
<h1><?=$nameProd?></h1>
<p class="precio"><?=$minimo?> - <?=$maximo?></p>
<p><?=$descrProd?></p>
<h5>DETALLES DEL PRODUCTO</h5>
<ul>
  <li> <?=$descripcion_long?></li>
</ul>

<div class="listado-colores">
<h5>COLORES</h5>
<ul class="thumbnails">
<?php foreach($Colors as $key => $col){
		$color_0 = eCommerce_SDO_Core_Application_Color::LoadById($key);
		$img_color = $color_0->getUrlImageId('small',0);
		$img_color_0 = $color_0->getUrlImageId('large',0);
	?>
	<li style="margin:5px"><a href="<?=$img_color_0?>" rel="shadowbox" title="<?=$col?>"  class="thumbnail"><img  src="<?=$img_color?>" class="ima-item" alt="<?=$col?>" style="width:41px"></a></li>
    <?php }?>		    
</ul>
</div>
<?php if(!empty($imas[0])){?>
<div class="listado-thumbnails" style="height:250px">
<ul class="thumbnails">
<li style="display:none" id="pic_sm0"><a class="thumbnail" onclick="changeImg(0,<?=(count($imas))?>); changeThumb(0,<?=(count($imas))?>)"  style="cursor:pointer"><img alt="prod" src="<?=$src_small?>" style="width:140px; height:140px" class="ima-item"></a></li>
<?php $cont=0; foreach($imas as $img){
		$imagen = $product->getUrlImagesId('small',$cont);
		$cont++;
		?>
		<li id="pic_sm<?=$cont?>" style="display:block"><a class="thumbnail" onclick="changeImg(<?=$cont?>,<?=$total_img?>); changeThumb(<?=$cont?>,<?=$total_img?>)" style="cursor:pointer"><img alt="prod" src="<?=$imagen?>" style="width:140px; height:140px" class="ima-item"></a></li>
    <?php }?>
	    
</ul>
</div>
<?php }?>

</div>

</div>

</div>
<form action="" name="frmCart" id="frmCart" method="post">
<div class="container">
<ul class="thumbnails listado-productos">
<?php foreach($VersionesAgrupadas as $Version){
	$modelo = $Version['modelo'];
	$product_id = $Version['product_id'];
	$Colores = eCommerce_SDO_Catalog::GetProductVersionesColor($product_id, $modelo);		
	$Tallas = eCommerce_SDO_Catalog::GetProductVersionesTalla($product_id, $modelo);			
	$version_id = $Version['version_id'];	
	$product_id = $Version['product_id'];
	$nombre = $Version['name'];
	$VersionFull = eCommerce_SDO_Catalog::LoadVersion($version_id);
	$detalles = $VersionFull->getDetails();
	$img_0 = $VersionFull->getUrlImageId('medium',0);
	$img_v0 = $VersionFull->getUrlImageId('large',0);
	$price = "$".eCommerce_SDO_Core_Application_CurrencyManager::numberFormat($Version['price']);	
	$sku = $Version['sku'];
	$medida = utf8_encode($Version['medida']);
	$price_0 = array();
	foreach($Colores as $Prices)
		$price_0[] = $Prices['price'];
	$max = max($price_0);
	$min = min($price_0);	
	?>
    <li>
    <!--DIV PARA SUSTITUIR-->
    <div id="info<?=$version_id?>"> 
    
	<div class="ima-item span3">  
		<a class="thumbnail" href="<?=$img_v0?>" rel="shadowbox" title="<?=$nombre?>"><img width="220" height="220" alt="<?=$nombre?>" src="<?=$img_0?>"></a>
	</div>
    <div class="span9">
	<h4><?=$nombre?> * <?=$medida?></h4>
	<p class="precio"><?= (($max==$min)? $max : $max." - ".$min)?></p>
    <?php if(!empty($detalles)){ echo "<p>".utf8_decode($descripcion_short).". ".$detalles."</p>";}?>
	</div>
    </div>
    <!--FIN DIV A SUSTITUIR-->
     <div class="span9" style="display:block" id="detalles<?=$version_id?>">
    <div class="row">
    <?php if(count($Colores)>0){?>
    <div class="span3">
    <label>Colores</label>
    <select name="color<?=$version_id?>" id="color<?=$version_id?>" class="colores" rel="<?=$version_id?>">
    <option value="">--Seleccionar color--</option>
        <? foreach($Colores as $color) {							
			$color_name = $color['colornombre'];
			$color_data_id = $color['color_id'];
			$v_id = $color['version_id'];			
			?>
        <option value="<?=$color_data_id."|".$v_id?>"><?=$color_name?></option>    
        <? }?>       
    </select>    
    </div>    
     <?php }?>
    <?php  if(!empty($Tallas[0]['talla_id'])){?>
    <div class="span3">
    <label>Tallas</label><select name="talla<?=$version_id?>" id="talla<?=$version_id?>" class="tallas" rel="<?=$version_id?>">
    <option value="">--Seleccionar talla--</option>
        <? foreach($Tallas as $talla) {					
			$talla_name = $talla['tallanombre'];
			$talla_data_id = $talla['talla_id'];	
			$v_id = $talla['version_id'];		
			?>
        <option value="<?=$talla_data_id?>"><?=$talla_name?></option>
        <? }?>
    </select>
    </div>    
     <?php }?>
    <div class="span3">
    <label>Cantidad</label><select name="producto<?=$version_id?>" id="quantity<?=$version_id?>">
	<?php for($i=1; $i<=20; $i++){?>
    <option value="<?=$i?>"><?=$i?></option>
    <?php }?>
    </select>
    </div>
   
    <div class="span3"><a class="btn btn-primary agregar-btn" style="cursor:pointer" rel="<?=$version_id?>" id="btn<?=$version_id?>" title="Añadir al Carrito">A&ntilde;adir al carrito</a></div>
    </div>
    </div>    
</li>

<?php $contar++; }?>
<input type="hidden" name="productId" value="<?=$product_id?>" />
</ul>

</div>
</form>

</div>

<?
require_once("footer.php");
?>