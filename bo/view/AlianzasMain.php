<?php
$title = "Alianzas |";
$inicio = 7;
require_once('header.php');

$result	  = $this->result;
$Alianzas = $result->getResults();
$hiddenFields = empty($this->hiddenFields) ? array() : $this->hiddenFields;

?>
<div id="contenido">
	<div class="pie1" align="left">
			
			<p><img src="<?=ABS_HTTP_URL?>ima/int/alianzas.png" alt="alianzas" width="69" height="13"  style="padding-top:20px; padding-bottom:15px;" /><br />
			  <br />
	  </p>
	<? 
	if(count($Alianzas) > 0){
		foreach( $Alianzas as $Alianzas_item){
			$nombre = $Alianzas_item->getNombre();
			$link = $Alianzas_item->getLink();
			$link = empty($link)?'':'<a href="'.$link.'" target="_blank" class="blanco12" >';
			$link_end = empty($link)?'':'</a>';
			$img_id = $Alianzas_item->getLogo(0);
			$img_0 = ($img_id != '')?$Alianzas_item->getUrlLogo('medium',0):ABS_HTTP_URL.'ima/int/ind.png';			
		 ?>	
	  <div class="alianza">
	    <div class="img-galeria"><?=$link?><img src="<?=$img_0?>" width="140" height="88" border="0" /><?=$link_end?></div>
        <div class="blanco12 desc" style="padding-top:30px;"><strong><?=$link?><?=$nombre?><?=$link_end?></strong><br />
        </div>
    </div>
    
    <? }
	}else{?>
     <div class="alianza">	   
        <div class="blanco12 desc" style="padding-top:30px;"><strong>No existen alianzas actualmente</strong></div>
    </div>
    <?
	}
	?>

   	  
  </div>
</div>
</div>

<?
//eCommerce_SDO_Core_Application_Form::foPrintPager($result,"FormAlianzas","classResultPages","classActualPage",5,$hiddenFields);
require_once('footer.php');
?>
