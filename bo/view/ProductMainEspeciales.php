<?php

require_once('header.php'); 
?>

<section class="int productos">
	<div class="container">
		<div class="row">
			<div id="sidebar" class="col-xs-3 col-lg-2">
				<?php include("accordion.php"); ?>
			</div>
			<div class="col-xs-9 col-lg-10">
				<div class="toolbar">
						<div class="row">						<div class="col-xs-6">							<ol class="breadcrumb">								<li><a href="<?=ABS_HTTP_URL?>"><span class="glyphicon glyphicon-home"></span></a></li>									<?php $cat_ = eCommerce_SDO_Catalog::LoadCategory($_REQUEST['idCat'] );									$nom_catego = $cat_->getName();									if ($cat_->getParentCategoryId()>0){										$padre = eCommerce_SDO_Catalog::LoadCategory( $padre->getParentCategoryId());										$baseFriendlyUrlPadre =$padre->getFriendlyNameUrl();										echo '<li class="active"><a href="'.$baseFriendlyUrlPadre.'">'.$padre->getName().'</a></li>';										 																		}																										?>								<li class="active"><a href="<?=$cat_->getFriendlyNameUrl().'/1/';?>"><?php echo $cat_->getName();?></a></li>							</ol>						</div>						<?php 						/*if(!empty($nom_catego)){							include('filtros.php');						}//end if*/						?>					</div>
				</div>
				
				<?php
				$subcategorias = $this->result->getResults();
				foreach($subcategorias as $item){
					$productos = eCommerce_SDO_Catalog::GetProductsByCategoryId($item->getCategoryId());
				?>
				<div class="row">	
					<div class="col-xs-12">
						<div style="background-image: url('<?php echo ABS_HTTP_URL?>img/layout/division.png'); height: 49px;">
							<div style="padding-top: 5px; padding-left: 15px; font-size: 20px; font-weight: bold; color: #FFF;">
							<?php echo $item->getName(); ?>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row" style="margin-top: 15px;">	
					<?php 
					foreach($productos as $producto){
						$id				= $producto->getProductId();
						$nombre 		= $producto->getName();
						$descripcion	= Util_String::subText($producto->getDescriptionShort(),97 -strlen($nombre),'chars');
						$href			= $producto->getFriendlyNameUrl($item->getCategoryId());
						$src			= $producto->getUrlImageId('medium',0);
						$alt			= $nombre;
						$precio			= $producto->getPrecio();
					?>				
					<div class="col-xs-4">
						<a href="<?=$href;?>" class="thumbnail">
							<div class="img-thumbnail" style="background-image:url('<?=$src?>')" style="border: 5px solid red;"></div>
							<div class="caption">
								<h4><?=$nombre?> </h4>
								<p class="precio"><?php echo '$ '.number_format($precio,2,'.',',').' MXN';?></p>
								<p class="btn btn-primary">M&aacute;s Informaci&oacute;n</p>
							</div>
						</a>
					</div>				
					<?php 
					}
					?>
				</div>
				
				<?php
				}?>	
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</section>


<?php
require_once('footer.php'); 
?>