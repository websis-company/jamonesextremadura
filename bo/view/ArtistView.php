<?php

$artist = $this->artist;

require_once("header.php");
$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
if ($actualLanguage == 'EN')
{
	$lang = 0;
}
else
{
	$lang = 1;
}

$sculptor[0] = "Sculptor";
$sculptor[1] = "Escultor";

$designer[0] = "Designer";
$designer[1] = "Dise&ntilde;ador";
?>





<p class="titleOfCategory">&nbsp;</p>



<div style="width:100%; padding-left:10px; padding-top:10px;">

	

<!-- Product -->
<?php
if ($artist->getType() == "sculptor")
{
?>
<!-- sin foto -->
<table border="0" cellpadding="0" cellspacing="0" width="700">
  <tbody>
    <tr>
      <td rowspan="2" align="center" valign="top" width="310">&nbsp;
          <p style="margin-left: 5px; margin-right: 20px; margin-top: 5px;" align="left">&nbsp;</p></td>
      <td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="680">
        <tbody>
          <tr>
            <td><span class="titleOfCategory" style="font-weight: bolder;"><?php echo $artist->getName(); ?></span></td>
          </tr>
          <tr>
            <td><span class="defaultText" style="color: rgb(0, 0, 0); font-weight: ; font-size: 14px;"><?=$sculptor[$lang];?></span></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </tbody>
      </table>
          <hr style="border-width: thin;" color="#c0c0c0" size="1" /></td>
    </tr>
    <tr>
      <td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
          <tr>
            <td><img src="img/3px.gif" alt="img" border="0" width="3" height="3" /></td>
          </tr>
          <tr>
            <td width="106" height="5">&nbsp;</td>
          </tr>
          <tr>
            <td valign="top"><p><a href="product.php?idArt=<?php echo $artist->getArtistId(); ?>">
			<?php if($actualLanguage == 'EN'){ ?>
				<img src="ima/fo/viewproduc.gif" width="105" height="20" border="0"/>
			<? } else { ?>
				<img src="img/versusproductos.gif" width="105" height="20" border="0"/>
			<? }?>			
			</a></p>
                  <p>&nbsp;</p></td>
          </tr>
          <tr>
            <td valign="top"><p><b><?php if($actualLanguage == 'EN')
                    {
                        echo "Biography";
                    }
                    else
                    {
                        echo "Biograf&iacute;a";
                    }?></b></p>
                  <p>
				  <?php echo nl2br($artist->getBiographyInActualLanguage()); ?>
				  <br />
                </p></td>
          </tr>
        </tbody>
      </table></td>
    </tr>
  </tbody>
</table>
<!-- sin foto -->
<?php
}
else{
?>
<!-- con foto -->

  <table border="0" cellpadding="0" cellspacing="0" width="700">
    <tbody>
      <tr>
        <td rowspan="2" align="center" valign="top" width="310">
		
		<?php
    	$imgId = $artist->getImageId();
    	if( $imgId > 0 ){
    		$urlImage =  'file.php?id=' . $imgId;
			$imageMini = $urlImage . '&type=image&img_size=predefined&width=310';
    	} 
    	else{
    		$urlImage = $imageMini = 'ima/fo/default.jpg';
    	}
		echo '<img src="' . $imageMini .'" alt="img" border="0" />';

	?>
        <p style="margin-left: 5px; margin-right: 20px; margin-top: 5px;" align="left">&nbsp;</p></td>
        <td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tbody>
              <tr>
                <td><span class="titleOfCategory" style="font-weight: bolder;"><?php echo $artist->getName(); ?></span></td>
              </tr>
              <tr>
                <td><span class="defaultText" style="color: rgb(0, 0, 0); font-size: 14px;"><?=$designer[$lang];?></span><br />
				<?php echo $artist->getEmail(); ?>
				</td>
              </tr>
              <tr>
                <td><span class="defaultText" style="color: rgb(0, 0, 0);">.</span></td>
              </tr>
            </tbody>
        </table>
            <hr style="border-width: thin;" color="#c0c0c0" size="1" /></td>
      </tr>
      <tr>
        <td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tbody>
              <tr>
                <td><img src="img/3px.gif" alt="img" border="0" width="3" height="3" /></td>
              </tr>
              <tr>
                <td width="106" height="5">&nbsp;</td>
              </tr>
              <tr>
                <td valign="top"><p><a href="product.php?idArt=<?php echo $artist->getArtistId(); ?>">
				<?php if($actualLanguage == 'EN'){ ?>
				<img src="ima/fo/viewproduc.gif" width="105" height="20" border="0"/>
			<? } else { ?>
				<img src="img/versusproductos.gif" width="105" height="20" border="0"/>
			<? }?>	
				</a></p>
                    <p>&nbsp;</p></td>
              </tr>
              <tr>
                <td valign="top"><p><b><?php if($actualLanguage == 'EN')
                    {
                        echo "Biography";
                    }
                    else
                    {
                        echo "Biograf&iacute;a";
                    }?></b></p>
                    <p>
					<?php echo nl2br($artist->getBiographyInActualLanguage()); ?>
					<br />
                      <br />
                      <br />
                      <br />
                      <br />
                      <br />
                  </p></td>
              </tr>
            </tbody>
        </table></td>
      </tr>
    </tbody>
  </table>
<!-- con foto -->
<?php
}
?>




  
  <p>

    <!-- Product -->

  </p>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">

    <tr>

      <td height="71">&nbsp;</td>

    </tr>

  </table>

  <p>&nbsp; </p>

</div>



<?php

include_once("footer.php");

?>