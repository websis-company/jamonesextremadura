<?php
if( ! @include_once( "header.php" ) ) {
	header('location:../index.php');
	die();
}

?>
<link href="<?=ABS_HTTP_URL?>css/form.css" rel="stylesheet" type="text/css" />
 <div align="justify" class="container"> 
   <h2 align="center" class="h2 text-center">Confirmación de Pago</h2>
	<hr class="hr" />
<div class="row">
<?php include "userInfoSidebar.php";?>
<div class="col-xs-9">
<?php if(!empty($error)){ echo $error; }?>
	 <div class="col-xs-9">
     
		<form action="http://www.estafeta.com/rastreo/" method="post" name="formpago" id="formpago" target="_blank">        
<div class="form-group">
	<label for="exampleInputEmail1">¿Tu pedido ha sido enviado por el servicio de paquetería y quieres saber dónde se encuentra?, da click en el siguiente link para saber el status de rastreo de tu pedido</label>
     <!--<input  class="form-control" placeholder="" name="entity[numero_confirmacion]"  id="codigorastreo" value="" required="required" pattern=".{10,}" required title="10 digitos" style="width:250px" maxlength="10" onclick="this.value = '';editAction()" onKeyUp="editAction()">  -->
    </div>
    <style>
    .btn-pink:hover{background-color:#ff4742; color:#FFFFFF}
    </style>
    <button type="submit" class="btn btn-pink btn-sm" id="sendRastreo">
          <span class="glyphicon glyphicon-send"></span> Rastrear 
        </button>
</form>
	</div>
</div>
</div>
</div>
<?php
include_once( "footer.php" );
?>
<!--
<script type="text/javascript">
	function editAction(){		
		document.formpago.action = 'http://www.estafeta.com/rastreo/'+document.getElementById('codigorastreo').value;		
	}
	$("#formpago").submit(function(){$("#codigorastreo").val('');});
</script>
-->