<?php
if( ! @include_once( "header.php" ) ) {
	header('location:../index.php');
	die();
}

?>
<link href="<?=ABS_HTTP_URL?>css/form.css" rel="stylesheet" type="text/css" />
<div class="container">
<section id="main" class="categoria">
<div class="row">
			<div class="row">
				<div class="col-xs-3 col-pc-30">
                <?php include "userInfoSidebar.php";?>
                
				</div>
                
                <div class="col-xs-9 col-pc-70">
                <div  class="row" id="product-list-wrapper" style="padding-right:0; margin:10px 0 25px;">
<?php if(!empty($error)){ echo $error; }?>
	 
     
		¿Solicitas factura? Envíanos tus datos a: <a href="mailto:tiendaenlinea@tecnocompra.com.mx" target="_blank">tiendaenlinea@tecnocompra.com.mx</a> y con gusto te enviáremos tu factura en un plazo no mayor a 24 horas. No olvides que para poder facturar deberás de mandar tus datos antes de que finalice el mes en el que realizaste tu compra.
<br />
Datos que solicitamos:        
<ul>

<li>No. de Pedido</li>
<li>Fecha de Compra</li>
<li>Nombre o Razón Social</li>
<li>RFC</li>
<li>Calle</li>
<li>Número</li>
<li>Colonia</li>
<li>Código Postal</li>
<li>Ciudad</li>
<li>Estado</li>
<li>Teléfono</li>

	 </div>
                
                
                </div>
</div>
</div>		
</section>
</div>
<?php
include_once( "footer.php" );
?>
