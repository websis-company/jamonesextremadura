<?php 
$result       = $this->result;
$Banner       =   $result->getResults();
$hiddenFields = empty($this->hiddenFields) ? array() : $this->hiddenFields;
?>

<div class="container">
<!-- Carousel ================================================== -->
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <div class="centrar-control">
      <ol class="carousel-indicators">
          <?php 
          $i = 0;
          foreach ($Banner as $value) {
            $active =  $i == 0 ? "active" : "";
            echo '<li data-target="#myCarousel" data-slide-to="'.$i.'" class="'.$active.'"></li>';  
            $i++;
          }//end foreach
          ?>
        </ol>
    </div>

    <div class="carousel-inner">
      <?php 
      $i = 0;
      foreach ($Banner as $Banner_item) {
        $active     = $i == 0 ? "active" : "";
        $banner_big = $Banner_item->getUrlFotoId('large',0);
        $href       = $Banner_item->getUrl();
        $http       = explode('http://', $href);
        $link       = count($http) == 1 ? 'http://'.$href : $href;
      ?>
      <div class="item <?php echo $active;?>" style="background-image:url()">
        <a href="<?=$link;?>" target="_blank"><img src="<?=$banner_big;?>" width="1140" height="596" alt="Banner"></a>
        <div class="container">
          <div class="carousel-caption">
          </div>
        </div>
      </div>
      <?php 
        $i++;
      }
      ?>
    </div>

    <div class="centrar-control" >
      <a data-slide="prev" role="button" href="#myCarousel" class="left carousel-control">
        <span aria-hidden="true" class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a data-slide="next" role="button" href="#myCarousel" class="right carousel-control">
        <span aria-hidden="true" class="glyphicon glyphicon-chevron-right"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
    <div class="shadow-carousel"></div>
    <div class="arrow-bottom"></div>
  </div>
</div>
<!-- /.carousel -->

