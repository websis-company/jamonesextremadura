<?php include('includes/head.php'); ?>
<?php 
include('includes/header.php'); 
$orderFull       = $this->orderFull;
$paymentMethod   = $this->paymentMethod;
$responseMessage = $this->responseMessage;
?>
<div id="main">
  <div class="c-layout-page padding-t-100">
      <div class="container">
        <h1 class="h1 lemon">Tienda en línea</h1>
      </div>
    </div>	
    <div class="container">
		<div class="col-xs-12">
			<div class="row" align="center">
				<img src="<?php echo ABS_HTTP_URL ?>images/layout/logo-mercadopago.png" alt="Mercado Pago" border="0" class="img-responsive" />
                &nbsp;
			</div>
			<br/>
	    	<div class="row">
		    	<p>
		    		Lo sentimos su orden no puede ser procesada ya que ocurrio un error en el proceso de pago.<br/>
		    	</p>
	    	</div>
			<div class="row">
				<div>A continuación se muestra el motivo : </div>
		    	<div class="alert alert-danger">
		    		<?php echo  $responseMessage; ?>
		    	</div>
	    	</div>
	    	<div class="row" align="center">
	    		<a href="<?=ABS_HTTP_URL?>" class="btn" style="font-family: Arial; font-size: 17px; color: #FFF; background-image: none; background-color: #000; ">Aceptar</a>
	    	</div>
	    </div>
    </div>
</div>
<?php 
unset($_SESSION['Auth']);
unset($_SESSION['cupon_descuento']);
unset($_SESSION['valor_descuento']);
eCommerce_SDO_Cart::DestroyActualCart();
include("includes/footer.php"); 
?>