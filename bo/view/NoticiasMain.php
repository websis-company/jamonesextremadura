<?php

$title_header = 'TecnoCompra | Noticias';
$header_description = '';
require_once('header.php');
$result	  = $this->result;
$Noticias = $result->getResults();
$hiddenFields = empty($this->hiddenFields) ? array() : $this->hiddenFields;


?>






<section id="interior2">

<div class="overlay">
<div class="parallaxeos_animate  fadeIn horizontal_center vertical_center">

<h1 class="h2"><span style="color: #ffffff;">Noticias</span></h1>

</div>
</div>
											
</section>


<section id="testimonios">
	<div style="padding-top:50px; padding-bottom:50px" class="container">
   
	  <div class="row">
      <?php 
        if(count($Noticias) > 0){
          foreach($Noticias as $Noticia){
            $titulo = Util_String::subText($Noticia->getTitulo(),50);
            $descripcion = Util_String::subText($Noticia->getDescripcion(),130);
            $img_0 = $Noticia->getHTMLImagenPrincipal('250','250');
            $href = $Noticia->getFriendlyNameUrl();
				?>
        <div style="padding:35px" align="center" class="col-xs-6 col-sm-4"> <a href="<?=$href?>" title="<?=$titulo?>"><?=$img_0?></a>
          <a href="<?=$href;?>" title="<?=$titulo?>"><h3><?=$titulo?></h3></a>
          <a href="<?=$href;?>" title="<?=$descripcion?>"><p style="color:#999"><?=$descripcion?></p></a>
        </div>
      <?}?>
  	<?}else{?>
        <div style="padding:35px" align="center" class="col-xs-6 col-sm-4">
          <p>No existen noticias actualmente</p>
        </div>
  	<?}?>
    </div>
        
<!--        <div class="row">	</div>-->
        
        
        
        
        
	</div>
</section>






<?php include("footer.php"); ?>
<script type="text/javascript" src="<?=ABS_HTTP_URL?>arquivos/home.js"></script>
<? die;?>
<?php
$header_title = 'Noticias';
$header_description = '';
require_once('header.php');

$result	  = $this->result;
$Noticias = $result->getResults();
$hiddenFields = empty($this->hiddenFields) ? array() : $this->hiddenFields;
?>
<a href="#" title="Contacto">
<div id="bannerint"><img src="<?=ABS_HTTP_URL?>img/pleca/interior.jpg" width="1889" height="247" alt="Contacto"></div></a>
<div id="main" >
<div class="page-header">
  
<h1 class="title">Noticias</h1></div>
<div class="container">
<div class="row">
  <div id="content" class="span12">
  	<div class="row" style="padding-bottom:30px;">
      
        <div id="content" class="span11" style="margin-left:150px">
            <ul class="thumbnails">
            <?php if(count($Noticias) > 0){			
			foreach($Noticias as $Noticia){
				$titulo = Util_String::subText($Noticia->getTitulo(),50);
				$descripcion = Util_String::subText($Noticia->getDescripcion(),130);
				$img_0 = $Noticia->getUrlImageId('medium',0);
				$href = $Noticia->getFriendlyNameUrl();
				?>
          	  <li class="span3">
            	<div class="thumbnail" >
						<a href="<?=$href?>" title="<?=$titulo?>"><img alt="<?=$titulo?>" src="<?=$img_0?>"></a>
                  </div>
                  <div class="caption" style="min-height:104px;max-height:104px;">
                        <h4><?=$titulo?></h4>                        
                        <p><?=$descripcion?></p>
                        
                        
                  </div>
                  <div class="caption" >
                    <p><a class="btn btn-primary" href="<?=$href?>" title="<?=$titulo?>">ver detalles   +</a></p>
                  </div>
            </li>
         <?php } 
		 }else{?>
         <li class="span3">No existen noticias actualmente</li>
         <?php }?>
            </ul>
         <div align="right"><?php eCommerce_SDO_Core_Application_Form::foPrintPager($result,"FormNoticias","negro12","naranja14",'naranja14',5,$hiddenFields);?></div>   
        </div>
    </div>
</div>
</div><!-- /.row -->
</div>


<!-- /#main -->
</div><!-- /.container -->

<?php
require_once('footer.php');
?>
