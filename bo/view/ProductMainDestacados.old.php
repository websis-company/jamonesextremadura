<?php 
$products            = $this->result->getResults();

/*echo "<pre>";
print_r($products);
echo "</pre>";*/

foreach ($products as $product) {
	$id				= $product->getProductId();
	$nombre 		= $product->getName();
	$descripcion	= Util_String::subText($product->getDescriptionShort(),97 -strlen($nombre),'chars');
	$href			= $product->getFriendlyNameUrl();
	$src			= $product->getUrlImageId('medium',0);								
	$alt			= $nombre;
	$precio			= $product->getPrecio();
?>
	<div class="col-xs-2">
		<a class="thumbnail" href="<?=$href;?>">
			<div style="background-image:url('<?=$src;?>'); width: 202px; height: 170px;" class="img-thumbnail" ></div>
			<div class="caption">
				<div style="height: 56px;">
					<h4 class="h4">
						<?=$nombre?>
					</h4>
				</div>
				<p><?php echo '$ '.number_format($precio,2,'.',',').' MXN';?></p>
				<p class="btn btn-primary">M&aacute;s Inofrmaci&oacute;n</p>
			</div>
		</a>
	</div>
<?php
}
?>

