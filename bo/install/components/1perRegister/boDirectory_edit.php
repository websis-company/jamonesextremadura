<?php
include_once ( "header_iframe.php" );

$form = $this->form;
$viewConfig = $this->viewConfig;
$errors = $this->errors;

?>

  <!-- calendar stylesheet -->
  <link rel="stylesheet" type="text/css" media="all" href="calendar/calendar-win2k-cold-1.css" title="win2k-cold-1" />
  <!-- main calendar program -->
  <script type="text/javascript" src="calendar/calendar.js"></script>
  <!-- language for the calendar -->
  <script type="text/javascript" src="calendar/lang/calendar-es.js"></script>
  <!-- the following script defines the Calendar.setup helper function, which makes
       adding a calendar a matter of 1 or 2 lines of code. -->
  <script type="text/javascript" src="calendar/calendar-setup.js"></script>
  
<h2 align="left" style="margin:0px;"><? echo $viewConfig['title']; ?></h2>

<form name="frmUser" id="frmUser" method="post" action="" enctype="multipart/form-data">

  <div class="error">
		<?php echo $errors->getDescription(); ?>
  </div><br />
  <?php $errors->getHtmlError("generalError"); ?>
	
	<fieldset>
		<legend>Informaci&oacute;n</legend>
		
		<dl class="form">
		<?php
			ProjectLibrary_SDO_Core_Application_Form::displayForm( $form, $errors,'frmUser' );
		
		?>

		</dl>
		
	</fieldset>
	
	<input type="hidden" name="cmd" value="save">
	
	<input type="submit" value="Guardar" class="frmButton"> <input type="button" value="Cancelar" class="frmButton" onClick="document.location.href='{boFile}?&columNameFKParam=<?=$this->columNameFKParam?>'">
</form>

<?php
include_once ( "footer.php" );
?>