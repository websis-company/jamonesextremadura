<?php
class ProjectLibrary_FrontEnd_BO_ClassA extends ProjectLibrary_FrontEnd_BO {


public function getSDOSearch( $search, $extraConditions ){
	return ProjectLibrary_SDO_ClassA::SearchClassA( $search, $extraConditions );
}	


/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
	
	/**
	 * @var ProjectLibrary_DAO_Event
	 */
	protected $search;

	public function __construct(){
		parent::__construct();
		
	}

	public function execute(){
		$this->checkPermission();
		
		$cmd = $this->getCommand();
		switch( $cmd ){
			case 'delete':
				$this->_delete();
				break;
			case 'add':
			case 'edit':
				$this->_edit();
				break;
			case 'save':
				$this->_save();
				break;
			case 'list':
			default:
				$this->_list();
				break;
		}
	}

	protected function _list(){

$columNameFKParam = empty($this->tpl->columNameFKParam) ? $this->getParameter('columNameFKParam') : $this->tpl->columNameFKParam;

		$this->search = new ProjectLibrary_Entity_Search(
			$this->getParameter( 'q', false, NULL ),
			$this->getParameter( 'o', false, '' ), //fecha DESC
			$this->getParameter( 'p', true, 1 ),
			$this->getParameter( 'k', true, 10 )
		);
		$extraConditions = array();
		
		if( !empty($columNameFKParam) ){
	$extraConditions[] = array( "columName"=>"columNameFK","value"=>$columNameFKParam,"isInteger"=>true);
		}
		$result = $this->getSDOSearch( $this->search, $extraConditions ); 
		
		
		$exportToExcel = $this->getParameter( 'toExcel' );
		if ( $exportToExcel ){
		
			$users = $result->getResults();
			$data = array();
			foreach($users as $user){
				$data[] = get_object_vars( $user );
			}
			$this->exportToExcel( $data );

		}
		else {
			$viewConfig = array();
			$viewConfig['name'] = 'ClassA';
			$viewConfig['title'] = 'ClassA';
			$viewConfig['id'] = '{$table_id}';
			
			$viewConfig["hiddenColums"]= array('array_images','columNameFK');
			$viewConfig['hiddenFields'] = array("columNameFKParam"=>$columNameFKParam);
			
			$this->tpl->assign( 'viewConfig', $viewConfig );
			
			$this->tpl->assign( 'options', $result );
			
			$this->tpl->assign('columNameFKParam', $columNameFKParam);
			
			$this->tpl->display(  "classA/list.php" );
		}
	}
		
	protected function _edit(){
		//$id = $this->getParameter();
		$id = $this->getParameter('id',false,0);
		
		//---------------------------
		if( strpos($id,",") > -1 ){
			$idF = explode(",",$this->idFields);
			$ArrId = explode(",",$id);
			$id = array();
			for($i=0; $i< count($idF); $i++){
				$id[ $idF[$i] ] = (empty($ArrId[$i])) ? 0 : $ArrId[$i];
			}
		}
		//---------------------------
		$entity = ProjectLibrary_SDO_ClassA::LoadClassA( $id );
		
		$pId = $entity->getgetSetcolumNameFK();
		if( empty($pId) ){
			$columNameFKParam = $this->getParameter('columNameFKParam');
			$this->tpl->assign('columNameFKParam', $columNameFKParam);
			$entity->setgetSetcolumNameFK($columNameFKParam);
		}else{
			$this->tpl->assign('columNameFKParam', $entity->getgetSetcolumNameFK() );
		}
		
		$this->tpl->assign( 'errors', new Validator_ErrorHandler() );

		$this->edit( $entity, $this->getCommand() );

	}

	public function edit( $entity, $cmd ){

		$this->tpl->assign( 'object',     $entity );
		
		$viewConfig = array();
		$form = array();

		
		$form['name'] = 'noticiaEventoForm';
		$form['elements'] = array();
		
		
		/***********************************************************************************************************/
		{$parseToEdit}
		/***********************************************************************************************************/
		
		//$viewConfig['id'] = 'noticia_evento_id';
		
		$viewConfig['form'] = $form;
		$viewConfig['title'] = ( $cmd =='add' ? 'Agregar' : 'Editar' ) . ' ClassA';
		
		$this->tpl->assign( 'form', $form );
		$this->tpl->assign( 'id', $entity->getFolderId() );	
		$this->tpl->assign( 'viewConfig', $viewConfig );
		
		//$this->tpl->assign( 'ArrayImages', $entity->getArrayImages() );

		$this->tpl->display( 'classA/edit.php' );
	}

	protected function exportToExcel( $data, $fileName = '' ){

		

		$rowO = $data[0];

		

		$contentHtml .= '<table border="1">';

		$contentHtml .= '<tr>

		<td height="72" width="255" bgcolor="#cccccc"><img src="'.ABS_HTTP_URL.'/ima/logo.gif"></td>

		<td colspan="'.(count($rowO) - 1).'"><h2>Order Report - '.date("M d Y").'</h2></td>

		</tr>';

		

		$contentHtml .= '<tr>';

		

		foreach($rowO as $key => $row){

			$key = str_replace("_",' ',$key);

			$key = ucwords($key);

			$contentHtml .= '<th align="center">'.$key.'</th>';

		}

		$contentHtml .= '</tr>';

		

		$i = 0;

		foreach($data as $row){

			

			$bgcolor = (++$i % 2 == 0) ? 'bgcolor="#cccccc"' : '';

			$contentHtml .= '<tr>';

			foreach($row as $rowElement){

				$contentHtml .= '<td '.$bgcolor.' align="right">'.$rowElement.'</td>';

			}

			$contentHtml .= '</tr>';

		}

		

		$contentHtml .= '</table>';

		

		

		$total_bytes = strlen($contentHtml);

		header("Content-type: application/vnd.ms-excel");

		header("Content-disposition: attachment; filename=".$fileName.".xls; size=" . $total_bytes);

		echo $contentHtml;

	}

	protected function _save(){
		
		if ( !empty( $_REQUEST ) && isset( $_REQUEST['entity'] ) ){
	  		
			$entity = $_REQUEST['entity'];
  			
			$ClassA = new ProjectLibrary_Entity_ClassA();
			

			// Save type
			try{
			
			
			/******************************************************/
			/******************************************************/
			{$parseToSave}
  			/******************************************************/
  			/*****************************************************/
  			
				
				/*
				$ClassATmp = ProjectLibrary_SDO_ClassA::LoadClassA( $ClassA->getNoticiaEventoId() );
				
				$imageHandler = ProjectLibrary_SDO_ImageHandler::GetImageHanlderObject('ClassAImages');
				$imageHandler->setArrImages( $ClassATmp->getArrayImages() );
				
				$imageHandler->setUploadFileDirectory('files/images/noticias/');
				$imageHandler->setUploadFileDescription('Imagen de Evento ' . $ClassA->getNoticiaEventoId() );
				
				$imageHandler->setValidTypes( array('word','image') );
				
				$imageHandler->setAccessType( ProjectLibrary_SDO_ImageHandler::GetValidAccessType('{$accessType}') );
				
				$ClassA->setArrayImages( implode(',',$ClassATmp->getArrayImages()) );
				$imageHandler->setMaximum( 1 );
				$arrImages = $imageHandler->proccessImages();
				$ClassA->setArrayImages( $arrImages );
				*/
				
				
				ProjectLibrary_SDO_ClassA::SaverClassA( $ClassA );
				$this->tpl->assign( 'strSuccess', 'ClassA ha sigo guardado.' );
				
				$this->tpl->assign( 'columNameFKParam', $ClassA->getgetSetcolumNameFK() );
				
				$this->_list();
			}
			catch( ProjectLibrary_SDO_Core_Validator_Exception $e){
				$this->tpl->assign( 'errors', new Validator_ErrorHandler( "There are some errors", $e->getErrors() ) );
				$oldCmd = $this->getParameter( 'old_cmd', false, null );
				$this->edit( $ClassA,  $oldCmd );
			}
		}
		else{
			throw new Exception('No hay suficiente informaci&oacute;n intente nuevamente');
		}
	}

	protected function _delete(){
			//$id = $this->getParameter();
		$id = $this->getParameter('id',false,0);
		$id2Str = $id;
		//---------------------------
		if( strpos($id,",") > -1 ){
			$idF = explode(",",$this->idFields);
			$ArrId = explode(",",$id);
			$id = array();
			$id2Str = "(";
			for($i=0; $i< count($idF); $i++){
				$id[ $idF[$i] ] = (empty($ArrId[$i])) ? 0 : $ArrId[$i];
				$id2Str .=($i>0) ? ',' : '';
				$id2Str .= $ArrId[$i] . "";
			}
			$id2Str .= ")";
		}
		//---------------------------
			try{
				$ClassA = ProjectLibrary_SDO_ClassA::DeleteClassA($id );
				$this->tpl->assign( 'strSuccess', 'ClassA "' . $id2Str . '" ha sido eliminado' );
				$this->tpl->assign('columNameFKParam', $ClassA->getgetSetcolumNameFK() );
			}
			catch(ProjectLibrary_SDO_Core_Application_Exception $e) {
				//debug($e);
				$this->tpl->assign( 'strError', $e->getMessage() );
			}
		
		$this->_list();
	}

}
?>