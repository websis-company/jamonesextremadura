<?php

if($_REQUEST["apply"]){
		$projectLibrary = $_REQUEST["ProjectLibrary"];
		//$projectLibrary = "eCommerce";
		$_LANG = "SP";
		//$_LANG = "EN";
		
		$dataBase = $_REQUEST["database"];
		$foDirectory = $_REQUEST["foDirectory"];
		$tableTextName = $_REQUEST["tableTextName"];
		$enumColumns = $_REQUEST["enumColumns"];
		//CATEGORY IN DIFFERENT LANGUAGE
		//$relationshipNto1
		//$relationshipNtoN
		
		$CompositionNto1 = false;
		$columNameFK = '';
		if($_REQUEST["CompositionNto1"] == 1){
			$CompositionNto1 = true; // many objects of this class corresponds to a object to other class
									//the relationship is using by the $columNameFK field
									
			$columNameFK = $_REQUEST["columNameFK"];//when $CompositionNto1 FK is the colum in the table
							//where is a Foreing Key for the principal class to create new
							//registers
		}
		
		
		$AgregationNto1 = false;
		$agregationColumNameFK = '';
		if($_REQUEST["AgregationNto1"] == 1){
			$AgregationNto1 = true; // many objects of this class corresponds to a object to other class
									//the relationship is using by the $columNameFK field
									
			$agregationColumNameFK = $_REQUEST["agregationColumNameFK"];//when $CompositionNto1 FK is the colum in the table
							//where is a Foreing Key for the principal class to create new
							//registers
		}
		


		
		
		$asociation1toN = false;
		$columNameFKA1toN = '';
		if($_REQUEST["asociation1toN"] == 1){
			$asociation1toN = true;
			$columNameFKA1toN = $_REQUEST[ "columNameFKA1toN"];
		}
		
		
		
		$className = $_REQUEST['class_name'];
		$table = $_REQUEST['table'];
		$tableId = $_REQUEST['table_id'];
		
		$tableSearchFields = preg_split("/,/",$_REQUEST['tableSearchFields']);
		$row = '';
		foreach($tableSearchFields as $tableSearchField){
			$row .= $row == '' ? '' : ',';
			$row .= '"' . $tableSearchField . '"';
		}
		$tableSearchFields = $row;

		
		$boFile = $_REQUEST['boFile'];
		$boDirectory = $_REQUEST['boDirectory'];
		
		$foFile = $_REQUEST['foFile'];
		
		$foFileView = $_REQUEST['foFileView'];
		$foFileMain = $_REQUEST['foFileMain'];
		
		$BOaccessTypeFiles = $_REQUEST['BOaccessTypeFiles']; //ProjectLibrary_SDO_Core_IO_FileApplication::GetAccessType('Entrevista')
		
		$arrayFiles = array();
		$reqArrayFiles = explode(",",$_REQUEST['arrayFiles']);
		foreach($reqArrayFiles as $reqArrayFile){
			$arrayFiles[] = $reqArrayFile;
		}
		
		$arrayIds = array();
		$reqArrayIds = explode(",",$_REQUEST['arrayIds']);
		foreach($reqArrayIds as $reqArrayFile){
			$arrayIds[] = $reqArrayFile;
		}
		
		
		$arrayExternalRefClass = array();
		$reqArrayIds = explode(",",$_REQUEST['arrayExternalRefClass']);
		$key = '';
		$i=0;
		foreach($reqArrayIds as $reqArrayFile){
			if(++$i % 2 == 0 && !empty($key)){
				$arrayExternalRefClass[ $key ] = $reqArrayFile;
			}else{
				$key = $reqArrayFile;
			}
		}

		
		$arrayFilesConfig = array();
		$reqArrayFilesConfig = explode(",",$_REQUEST['arrayFilesConfig']);
		$rowFileConfig = array();
		$i=0;
		foreach($reqArrayFilesConfig as $reqArrayFile){
			if(empty($rowFileConfig)){
				$rowFileConfig["directory"] = $reqArrayFile;
			}else{
				$rowFileConfig["maximum"] = $reqArrayFile;
				$arrayFilesConfig[ $arrayFiles[ $i ] ] = $rowFileConfig;
				$rowFileConfig=array();
				$i++;
			}
		}
		
		
		//$arrayFiles = array("field1");
		//$arrayFilesConfig = array( "field1" => array("directory"=>"\dir\", "maximum"=>3) );
		
		
		
		//-----------------------------------------------
		//-----------------------------------------------
		//			INIT INSTALLATION
		//-----------------------------------------------
		//-----------------------------------------------
		
		//Entity Class
			$ap = fopen("../libs/".$projectLibrary."/Entity/" . $className . ".class.php","w+");
			$body= file_get_contents( 'libs_ProjectLibrary_Entity_ClassA.php', 'r' );
			
			$parseTable = createEntityFromTable($dataBase,$table, $arrayFiles, $arrayIds);
			$body = str_replace('ClassA',$className, $body);
			$body = str_replace('{$parseToTable}',$parseTable, $body);
			
			$body = str_replace('ProjectLibrary_',$projectLibrary . '_', $body);
			
			fwrite($ap, $body);
		
		
		
		//FO principal File 
			$ap = fopen("../" . $foFile ,"w+");
			$body= file_get_contents( 'foFile.php', 'r' );
			$body = str_replace('ClassA',$className, $body);
		
			$body = str_replace('ProjectLibrary_',$projectLibrary . '_', $body);	
			
			fwrite($ap, $body);
		
		
		//BO principal File 
			$ap = fopen("../backoffice/" . $boFile ,"w+");
			$body= file_get_contents( 'boFile.php', 'r' );
			$body = str_replace('ClassA',$className, $body);
			$body = str_replace('ProjectLibrary_',$projectLibrary . '_', $body);	
			fwrite($ap, $body);
		
		
		//FO directory
			@mkdir("../" . $foDirectory, 0755);
			
			
		//FO directory/viewFile.php File 
			$ap = fopen("../" . $foDirectory . "/".$foFileView,"w+");
			$body= file_get_contents( 'foFileView.php', 'r' );
			$body = str_replace('className',$className, $body);
			$body = str_replace('ProjectLibrary_',$projectLibrary . '_', $body);
			fwrite($ap, $body);
		
		//FO directory/mainFile.php File 
			$ap = fopen("../" . $foDirectory . "/".$foFileMain,"w+");
			$body= file_get_contents( 'foFileMain.php', 'r' );
			$body = str_replace('ProjectLibrary_',$projectLibrary . '_', $body);
			$body = str_replace('className',$className, $body);
			
			$fieldMayus = $tableId;
			$fieldMayus = str_replace("_"," ",$fieldMayus);
			$fieldMayus = ucwords( $fieldMayus );
			$fieldMayus = str_replace(" ","",$fieldMayus);
		
			$body = str_replace('getTitle','get' . $fieldMayus, $body);
			
			
			fwrite($ap, $body);
				
			
		//BO directory
			@mkdir("../backoffice/" . $boDirectory, 0755);
		
		//BO directory/list.php File 
			$ap = fopen("../backoffice/" . $boDirectory . "/list.php","w+");
			
			if($CompositionNto1){
					$body= file_get_contents( 'components/1perRegister/boDirectory_list.php', 'r' );
					$fieldMayus = $columNameFK;
					$fieldMayus = str_replace("_"," ",$fieldMayus);
					$fieldMayus = ucwords( $fieldMayus );
					$fieldMayus = str_replace(" ","",$fieldMayus);
					$columNameFKParam = $getSetcolumNameFK = $fieldMayus;
		
					$body = str_replace('columNameFKParam',$columNameFKParam, $body);
					$body = str_replace('getSetcolumNameFK',$getSetcolumNameFK, $body);				
					$body = str_replace('columNameFK',$columNameFK, $body);
				}else{
					$body= file_get_contents( 'boDirectory_list.php', 'r' );
				}
				
				//LANG 
				if( $_LANG == "EN"){
					$body= str_replace('Acciones',"Actions", $body);
					$body= str_replace('Editar',"Edit", $body);
					$body= str_replace('Eliminar',"Delete", $body);
					$body= str_replace('Esta seguro que desea eliminar el registro',"Are you sure you want to delete the register", $body);
					$body= str_replace('Buscar',"Search", $body);
					$body= str_replace('Limpiar',"Clean", $body);
					$body= str_replace('Agregar',"Add", $body);
				}
				
				$body = str_replace('ProjectLibrary_',$projectLibrary . '_', $body);
				fwrite($ap, $body);
				
				if($asociation1toN){
					$body= file_get_contents( 'components/Asociation1toN/boDirectory_list.php', 'r' );
					$fieldMayus = $columNameFKA1toN;
					$fieldMayus = str_replace("_"," ",$fieldMayus);
					$fieldMayus = ucwords( $fieldMayus );
					$fieldMayus = str_replace(" ","",$fieldMayus);
					$columNameFKParam = $getSetcolumNameFK = $fieldMayus;
		
					$body = str_replace('columNameFKParam',$columNameFKParam, $body);
					$body = str_replace('getSetcolumNameFK',$getSetcolumNameFK, $body);				
					$body = str_replace('columNameFK',$columNameFK, $body);
					
					$ap = fopen("../backoffice/" . $boDirectory . "/listAs1toN.php","w+");
					fwrite($ap, $body);
				}
		
			
			
		
		
		//BO directory/edit.php File 
			$ap = fopen("../backoffice/" . $boDirectory . '/edit.php',"w+");
				if($CompositionNto1){
					$body= file_get_contents( 'components/1perRegister/boDirectory_edit.php', 'r' );
						$fieldMayus = $columNameFK;
						$fieldMayus = str_replace("_"," ",$fieldMayus);
						$fieldMayus = ucwords( $fieldMayus );
						$fieldMayus = str_replace(" ","",$fieldMayus);
						$columNameFKParam = $getSetcolumNameFK = $fieldMayus;
						
						$body = str_replace('columNameFKParam',$columNameFKParam, $body);
						$body = str_replace('getSetcolumNameFK',$getSetcolumNameFK, $body);				
						$body = str_replace('columNameFK',$columNameFK, $body);
				}else{
					$body = file_get_contents( 'boDirectory_edit.php', 'r' );
				}
			
			//LANG 
			if( $_LANG == "EN"){
				$body = str_replace('Guardar','Save', $body);
				$body = str_replace('Cancelar','Cancel', $body);
				$body = str_replace('Informaci&oacute;n','Information', $body);
				
			}
			
			$body = str_replace('ClassA',$className, $body);
			$body = str_replace('ProjectLibrary_',$projectLibrary . '_', $body);
			
			$body = str_replace('{boFile}',$boFile, $body);
			
			fwrite($ap, $body);
		
		
		//ClassAArray.class.php
			$ap = fopen("../libs/".$projectLibrary."/Entity/Util/".$className."Array.class.php","w+");
			$body= file_get_contents( 'libs_ProjectLibrary_Entity_Util_ClassAArray.php', 'r' );
			$body = str_replace('ClassA',$className, $body);
			$body = str_replace('ProjectLibrary_',$projectLibrary . '_', $body);
			fwrite($ap, $body);
		
		//ClassAIterator.class.php
			$ap = fopen("../libs/".$projectLibrary."/Entity/Util/".$className."Iterator.class.php","w+");
			$body= file_get_contents( 'libs_ProjectLibrary_Entity_Util_ClassAIterator.php', 'r' );
			$body = str_replace('ClassA',$className, $body);
			$body = str_replace('ProjectLibrary_',$projectLibrary . '_', $body);
			fwrite($ap, $body);
		
		//Entity Search Result ClassA.class.php
			$ap = fopen("../libs/".$projectLibrary."/Entity/Search/Result/".$className.".class.php","w+");
			$body= file_get_contents( 'libs_ProjectLibrary_Entity_Search_Result_ClassA.php', 'r' );
			$body = str_replace('ClassA',$className, $body);
			$body = str_replace('ProjectLibrary_',$projectLibrary . '_', $body);
			fwrite($ap, $body);	
			
		//Application ClassA.class.php
			$ap = fopen("../libs/".$projectLibrary."/SDO/Core/Application/".$className.".class.php","w+");
			$body= file_get_contents( 'libs_ProjectLibrary_SDO_Core_Application_ClassA.php', 'r' );
			$body = str_replace('ClassA',$className, $body);
		
			$fieldMayus = $tableId;
			$fieldMayus = str_replace("_"," ",$fieldMayus);
			$fieldMayus = ucwords( $fieldMayus );
			$fieldMayus = str_replace(" ","",$fieldMayus);
			$body = str_replace('getNoticiaEventoId',"get" . $fieldMayus, $body);
			$body = str_replace('ProjectLibrary_',$projectLibrary . '_', $body);
			
			$fieldMayus = $tableTextName;
			$fieldMayus = str_replace("_"," ",$fieldMayus);
			$fieldMayus = ucwords( $fieldMayus );
			$fieldMayus = str_replace(" ","",$fieldMayus);
			$body = str_replace('tableTextName',$fieldMayus, $body);
			//$body = str_replace('{$enum_function_calls}', getENUMFunctionCalls( $enumColumns), $body);
			fwrite($ap, $body);		
		
		
		//DAO ClassA.class.php
			$ap = fopen("../libs/".$projectLibrary."/SDO/Core/DAO/".$className.".class.php","w+");
			$body= file_get_contents( 'libs_ProjectLibrary_SDO_Core_DAO_ClassA.php', 'r' );
			$body = str_replace('ClassA',$className, $body);
			$body = str_replace('{$table}',$table, $body);
			$body = str_replace('{$table_id}',$tableId, $body);
			$body = str_replace('{$table_array_fields_search}',$tableSearchFields, $body);
			$body = str_replace('ProjectLibrary_',$projectLibrary . '_', $body);
			fwrite($ap, $body);	
		
		//Validator ClassA.class.php
			$ap = fopen("../libs/".$projectLibrary."/SDO/Core/Validator/".$className.".class.php","w+");
			$body= file_get_contents( 'libs_ProjectLibrary_SDO_Core_Validator_ClassA.php', 'r' );
			$body = str_replace('ClassA',$className, $body);
			$body = str_replace('ProjectLibrary_',$projectLibrary . '_', $body);
			fwrite($ap, $body);	
		
		
		//SDO ClassA.class.php
			$ap = fopen("../libs/".$projectLibrary."/SDO/".$className.".class.php","w+");
			$body= file_get_contents( 'libs_ProjectLibrary_SDO_ClassA.php', 'r' );
			$body = str_replace('ClassA',$className, $body);
			$body = str_replace('ProjectLibrary_',$projectLibrary . '_', $body);
			fwrite($ap, $body);	
		
		
		//FO ClassA.class.php
			$ap = fopen("../libs/".$projectLibrary."/FrontEnd/FO/".$className.".class.php","w+");
			$body= file_get_contents( 'libs_ProjectLibrary_FrontEnd_FO_ClassA.php', 'r' );
			$body = str_replace('ClassA',$className, $body);
			
			$body = str_replace('galeriaView.php',$foFileView, $body);	
			$body = str_replace('galeriaMain.php',$foFileMain, $body);	
			
			$body = str_replace('ProjectLibrary_',$projectLibrary . '_', $body);
			fwrite($ap, $body);	
			
		//BO ClassA.class.php
			$ap = fopen("../libs/".$projectLibrary."/FrontEnd/BO/".$className.".class.php","w+");
			
				if($CompositionNto1){
						$body= file_get_contents( 'components/1perRegister/libs_ProjectLibrary_FrontEnd_BO_ClassA.php', 'r' );
						$fieldMayus = $columNameFK;
						$fieldMayus = str_replace("_"," ",$fieldMayus);
						$fieldMayus = ucwords( $fieldMayus );
						$fieldMayus = str_replace(" ","",$fieldMayus);
						$columNameFKParam = $getSetcolumNameFK = $fieldMayus;
						
						$body = str_replace('columNameFKParam',$columNameFKParam, $body);
						$body = str_replace('getSetcolumNameFK',$getSetcolumNameFK, $body);				
						$body = str_replace('columNameFK',$columNameFK, $body);
						
					
				}else{
					$body= file_get_contents( 'libs_ProjectLibrary_FrontEnd_BO_ClassA.php', 'r' );
				}

		if($_LANG == 'EN'){
			$body = str_replace('Agregar',"Add", $body);
			$body = str_replace('Editar',"Edit", $body);
			$body = str_replace('ha sido guardado',"has been saved", $body);
			$body = str_replace('Registro',"Register", $body);
			$body = str_replace('Existen Algunos Errores',"There are some errors", $body);
			$body = str_replace('ha sido eliminado',"has been deleted", $body);
			
		}
			
			$fieldMayus = $tableId;
			$fieldMayus = str_replace("_"," ",$fieldMayus);
			$fieldMayus = ucwords( $fieldMayus );
			$fieldMayus = str_replace(" ","",$fieldMayus);
			$body = str_replace('getNoticiaEventoId',"get" . $fieldMayus, $body);
			$body = str_replace('getTitulo',"get" . $fieldMayus, $body);	
			
			$body = str_replace('{$parseToSave}',getColumsWithSets( $dataBase, $table, $className, $tableId, $arrayFiles, $arrayFilesConfig, $arrayIds), $body );
			$body = str_replace('{$parseToEdit}',getColumsWithGets( $dataBase, $table, $className, $arrayFiles, $enumColumns, $arrayIds, $arrayExternalRefClass, $tableId), $body);
			
			$body = str_replace('noticiaEventoForm', $className . "Form", $body);
			
			$body = str_replace('ClassA',$className, $body);
			$body = str_replace('classA',$boDirectory, $body);
			
			$body = str_replace('CLASSA',strtoupper($className), $body);
			
			
			$body = str_replace('{$accessType}',$BOaccessTypeFiles, $body);
			
			$body = str_replace('{$table_id}',$tableId, $body);
			
			$body = str_replace('ProjectLibrary_',$projectLibrary . '_', $body);
			fwrite($ap, $body);		
			
			
			if($asociation1toN){
					$body= file_get_contents( 'components/Asociation1toN/libs_ProjectLibrary_FrontEnd_BO_ClassA.php', 'r' );
					$fieldMayus = $columNameFKA1toN;
					$fieldMayus = str_replace("_"," ",$fieldMayus);
					$fieldMayus = ucwords( $fieldMayus );
					$fieldMayus = str_replace(" ","",$fieldMayus);
					$columNameFKParam = $getSetcolumNameFK = $fieldMayus;
						
						$body = str_replace('columNameFKParam',$columNameFKParam, $body);
						$body = str_replace('getSetcolumNameFK',$getSetcolumNameFK, $body);				
						$body = str_replace('columNameFK',$columNameFK, $body);
					
					$ap = fopen("../libs/".$projectLibrary."/FrontEnd/BO/".$className."As1toN.class.php","w+");
					
					$fieldMayus = $tableId;
					$fieldMayus = str_replace("_"," ",$fieldMayus);
					$fieldMayus = ucwords( $fieldMayus );
					$fieldMayus = str_replace(" ","",$fieldMayus);
					$body = str_replace('getNoticiaEventoId',"get" . $fieldMayus, $body);
					$body = str_replace('getTitulo',"get" . $fieldMayus, $body);	
					$body = str_replace('{$parseToSave}',getColumsWithSets( $dataBase, $table, $className, $tableId, $arrayFiles, $arrayFilesConfig, $arrayIds), $body );
					$body = str_replace('{$parseToEdit}',getColumsWithGets( $dataBase, $table, $className, $arrayFiles,$enumColumns, $arrayIds,$arrayExternalRefClass, $tableId), $body);
					$body = str_replace('noticiaEventoForm', $className . "Form", $body);
					$body = str_replace('ClassA',$className, $body);
					$body = str_replace('classA',$boDirectory, $body);
					$body = str_replace('CLASSA',strtoupper($className), $body);
					
					$body = str_replace('{$accessType}',$BOaccessTypeFiles, $body);
					$body = str_replace('{$table_id}',$tableId, $body);
					$body = str_replace('ProjectLibrary_',$projectLibrary . '_', $body);
					fwrite($ap, $body);		
				}
				
				
	//ACCESS ClassA.class.php
			$ap = fopen("../libs/".$projectLibrary."/Access/".$className.".class.php","w+");
			$body= file_get_contents( 'libs_ProjectLibrary_Access_ClassA.class.php', 'r' );
			$body = str_replace('ClassA',$className, $body);
			$body = str_replace('CLASSA',strtoupper($className), $body);

			$body = str_replace('ProjectLibrary_',$projectLibrary . '_', $body);
			fwrite($ap, $body);	
							
		echo "installation complete...<br />";
}
?>

<form action="" method='post'>
<table border='1' cellpadding='5' cellspacing='0'>
<tr>
<td>
front Office Directory:
</td><td> 
<input type='text' name='foDirectory' value='view' />
</td></tr>

<tr>
<td>
Database:
</td><td> 
<input type='text' name='database' value='db' />
</td></tr>

<tr>
<td>
ProjectLibrary:
</td><td> 
<select name='ProjectLibrary'>
	<option value='ProjectLibrary'>ProjectLibrary</option>
	<option value='eCommerce'>eCommerce</option>
</select>
</td></tr>

<tr>
<td>
class_name:
</td><td> 
<input type='text' name='class_name' id='class_name' value='ClassName' onchange='updateDependingValues();' />
</td></tr>

<tr><td>table:</td><td> <input type='text' name='table' id='table' value='' /> </td></tr>

<tr><td>table_id:</td><td> <input type='text' name='table_id' id='table_id' value='' /> field1,field2</td></tr>
<tr><td>table Text Name:</td><td> <input type='text' name='tableTextName' id='tableTextName' value='' />
field: value of an option when the registers result as array: arary[id] = field </td>
</tr>

<tr><td>tableSearchFields:</td><td> <input type='text' name='tableSearchFields' value='' /> field1,field2 </td></tr>
<tr><td>ENUM Columns:</td><td> <input type='text' name='enumColumns' value='' /> field1,field2 </td></tr>
<tr><td>External Class:</td><td> <input type='text' name='arrayExternalRefClass' value='' /> value1(field),value2 (classA)</td></tr>

<tr><td>boFile:</td><td> <input type='text' name='boFile' id='boFile' value='.php' /> </td></tr>
<tr><td>boDirectory:</td><td> <input type='text' name='boDirectory' id='boDirectory' value='' /> p.e. ClassName</td></tr>
<tr><td>foFile:</td><td> <input type='text' name='foFile' id='foFile' value='.php' /> </td></tr>
<tr><td>foFileView:</td><td> <input type='text' name='foFileView' id='foFileView' value='.php' /> </td></tr>
<tr><td>foFileMain:</td><td> <input type='text' name='foFileMain' id='foFileMain' value='.php' /> </td></tr>
<tr><td>BOaccessTypeFiles:</td><td> <input type='text' name='BOaccessTypeFiles' id='BOaccessTypeFiles' value='' /> p.e. ClassName</td></tr>
<tr><td>arrayFiles:</td><td> <input type='text' name='arrayFiles' value='' /> p.e. field1, field2</td></tr>
<tr><td>arrayFilesConfig:</td><td> <input type='text' name='arrayFilesConfig' value='' /> <br />
field1=>directory, field1=>maximum<br />
p. e.: directory, maximum, directory2, maximum2
</td></tr>

<tr><td>arrayIds:</td><td> <input type='text' name='arrayIds' value='' /> p.e. field1, field2</td></tr>

<tr>
<td>
CompositionNto1: 
	<select name='CompositionNto1'>
		<option value='1'>Yes</option>
		<option value='0' selected='selected'>No</option>
	</select> 
</td><td>
columNameFK: <input type='text' name='columNameFK' value='' />
</td></tr>


<tr>
<td>
Agregation Nto1: 
	<select name='AgregationNto1'>
		<option value='1'>Yes</option>
		<option value='0' selected='selected'>No</option>
	</select> 
</td><td>
columNameFK: <input type='text' name='agregationColumNameFK' value='' />
</td></tr>


</table>
<br />
<input type='submit' name='apply' value='Install Now'>
</form>

<script language='javascript'>
function updateDependingValues(){
	className = document.getElementById('class_name').value;
	document.getElementById('table').value = className.toLowerCase();
	document.getElementById('table_id').value = className.toLowerCase() + '_id';
	
	document.getElementById('tableTextName').value = className.toLowerCase() + '_id';
		
	document.getElementById('boFile').value = className+'.php';
	document.getElementById('boDirectory').value = className;
	document.getElementById('foFile').value = className+'.php';
	document.getElementById('foFileView').value = className+'View.php';
	document.getElementById('foFileMain').value = className+'Main.php';
	document.getElementById('BOaccessTypeFiles').value = className;
}
</script>
 <?php
 



function getColumsWithGets( $database = 'montessori', $table = 'noticia_evento', $className='', $arrayFiles ='', $enumColumns = '', $arrayIds = array(), $ArrReferenceClass = array(), $tableId = '' ){
	$connection = mysql_connect('localhost','root','');
	mysql_select_db($database);
	$result = mysql_query("SHOW COLUMNS FROM " . $table);
	
	$FinalResult1 ='';
	$FinalResult2 ='';
	
	$returnHtml = "";
	
	$enumColumns = preg_split("/,/",$enumColumns);
	
	while( $row = mysql_fetch_assoc($result) ){
		$field = $row['Field'];

		$fieldMayus = $field;
		
		$fieldMayus = str_replace("_"," ",$fieldMayus);
		$fieldMayus = ucwords( $fieldMayus );
		$fieldMayus = str_replace(" ","",$fieldMayus);
		$get= 'get' . $fieldMayus;

		$value = "\$entity->".$get."()";
		$typeField = 'text';
		$options = 'array()';
		
		if( in_array($field, $arrayFiles) ){
			$typeField = 'array_files';
		}
		

		if( array_key_exists($field,$ArrReferenceClass) ){
			$options = 'ProjectLibrary_SDO_'. $ArrReferenceClass[$field] .'::GetAllInArray()';
			$typeField = 'select';
		}

		if( in_array($field, $arrayIds) ){
			//we need to get the name of the external class
			//$options = 'ProjectLibrary_SDO_Core_Application_ClassA::GetEnumValues("'.$field.'")';
			$typeField = 'array_ids';
		}
		
		if( in_array($field, $enumColumns) ){
			$options = 'ProjectLibrary_SDO_Core_Application_ClassA::GetEnumValues("'.$field.'")';
			$typeField = 'select';
		}
		
		if( $field == $tableId){
			$typeField = 'hidden';
		}
		
		
		
		$value = "\$form['elements'][] = array( 'title' => '".$fieldMayus.":', 'type'=>'".$typeField."', 'options'=>".$options.", 'name'=>'entity[".$field."]', 'id'=>'".$field."', 'value'=> ".$value." );";
		
		$returnHtml .="\n" . $value;
	}
	return $returnHtml;
}

function getColumsWithSets( $database = 'montessori', $table = 'noticia_evento', $className='',$fieldID = '', $arrayFiles=array(), $arrayFilesConfig=array(), $arrayIds = array()  ){
	
	$connection = mysql_connect('localhost','root','');
	mysql_select_db($database);
	$result = mysql_query("SHOW COLUMNS FROM " . $table);
	
	$FinalResult1 ='';
	$FinalResult2 ='';
	
	$returnHtml = "";


		$fieldIDMayus = str_replace("_"," ",$fieldID);
		$fieldIDMayus = ucwords( $fieldIDMayus);
		$fieldIDMayus = str_replace(" ","",$fieldIDMayus);
		
	while( $row = mysql_fetch_assoc($result) ){
		$field = $row['Field'];

		$fieldMayus = $field;
		
		$fieldMayus = str_replace("_"," ",$fieldMayus);
		$fieldMayus = ucwords( $fieldMayus );
		$fieldMayus = str_replace(" ","",$fieldMayus);
		
		//some time we can add colum tyoe validation and then add striptags deppends the type 
		$set= 'set' . $fieldMayus;
		
		if( !in_array($field, $arrayFiles) && !in_array($field, $arrayIds) ){
			$value = "" .
			"\$txt = (empty(\$entity['".$field."']) ? '' : \$entity['".$field."']);" .
			"\$".$className."->".$set."( strip_tags(\$txt) );";
		}else{
		
			if( in_array($field, $arrayFiles) ){
				$arrayFilesConfig[$field]["directory"] = isset($arrayFilesConfig[$field]["directory"]) ? $arrayFilesConfig[$field]["directory"] : '';
				$arrayFilesConfig[$field]["maximum"] = isset($arrayFilesConfig[$field]["maximum"]) && is_numeric($arrayFilesConfig[$field]["maximum"]) ? $arrayFilesConfig[$field]["maximum"] : 1;
				
				$value = "" .
					"\n/////////////////////////////////////////////////////////////////////////////////////////\n" .
					"\$ClassATmp = ProjectLibrary_SDO_ClassA::LoadClassA( \$ClassA->get".$fieldIDMayus."() );
					\$imageHandler = ProjectLibrary_SDO_ImageHandler::GetImageHanlderObject('".$field."');
					\$imageHandler->setArrImages( \$ClassATmp->get".$fieldMayus."() );
					
					\$imageHandler->setUploadFileDirectory('".$arrayFilesConfig[$field]["directory"]."');
					
					\$imageHandler->setUploadFileDescription( \$this->getParameter( \$imageHandler->GetDescriptionField(),false,'Imagen de ClassA' .\$ClassA->get".$fieldIDMayus."() ) );
					
					\$imageHandler->setValidTypes( array('word','image') );
					
					\$imageHandler->setAccessType( ProjectLibrary_SDO_ImageHandler::GetValidAccessType('{\$accessType}') );
					
					\$imageHandler->setMaximum( ".$arrayFilesConfig[$field]["maximum"]." );
					\$arrImages = \$imageHandler->proccessImages();
					\$ClassA->set".$fieldMayus."( \$arrImages );" .
					"\n/////////////////////////////////////////////////////////////////////////////////////////" .
					"";
			}else{
				$value = "" .
				"\n///////////////////////////////////////////////////////////////////////////////////////\n" .
				"	\$ClassATmp = ProjectLibrary_SDO_ClassA::LoadClassA( \$ClassA->get".$fieldIDMayus."() );
					\$imageHandler = ProjectLibrary_SDO_IdHandler::GetIdsHanlderObject('".$field."');
					\$imageHandler->setArrIds( \$ClassATmp->get".$fieldMayus."() );
					\$imageHandler->setMaximum( 1 );
					\$arrImages = \$imageHandler->proccessIds();
					\$ClassA->set".$fieldMayus."( \$arrImages ); " . 
				"\n/////////////////////////////////////////////////////////////////////////////////////////" .
				"";
			}
		}
		//before line: \$imageHandler->setMaximum( ".$arrayFilesConfig[$field]["maximum"]." );
		//\$ClassA->set".$fieldMayus."( implode(',',\$ClassATmp->get".$fieldMayus."()) );
		
		$returnHtml .="\n" . $value;
	}
	return $returnHtml;
}
		
function createEntityFromTable( $database = 'montessori', $table = 'noticia_evento', $arrayFiles = array(), $arrayIds = array() ){
	$connection = mysql_connect('localhost','root','');
	mysql_select_db($database);
	$result = mysql_query("SHOW COLUMNS FROM " . $table);
	
	$FinalResult1 ='';
	$FinalResult2 ='';
	while( $row = mysql_fetch_assoc($result) ){
		$field = $row['Field'];

		$fieldMayus = $field;
		
		$fieldMayus = str_replace("_"," ",$fieldMayus);
		$fieldMayus = ucwords( $fieldMayus );
		$fieldMayus = str_replace(" ","",$fieldMayus);
		
		$FinalResult1 .= "public \$" . $field . ";\n";
		
		$FinalResult2 .= "\n\npublic function get".$fieldMayus."(";
		if( in_array( $field, $arrayFiles) || in_array( $field, $arrayIds) ){	$FinalResult2 .= "\$position = null";	}
		$FinalResult2 .= "){ \n";

		if( !in_array( $field, $arrayFiles) && !in_array( $field, $arrayIds) ){
			$FinalResult2 .= "return \$this->".$field.";\n";
		}else{

			$FinalResult2 .= "\$image = explode(',',\$this->".$field.");\n";
			$FinalResult2 .= "return !is_numeric(\$position) ? \$image : (!empty(\$image[\$position]) ? \$image[\$position] : 0 );";
		}

		
		$FinalResult2 .= "}\n\n";
		
		if( in_array( $field, $arrayFiles) ){
		$FinalResult2 .= "public function getHTML".$fieldMayus."(\$width = null,\$height= null, \$nimage = 0, \$target_blank = false, \$urlEffect = false, \$imgParam = 'border=\"0\"'){"
			. "\n \$imgParam2 = empty(\$width) ? '' : ' width=\"'.\$width.'\"';"
			. "\n \$imgParam2 .= empty(\$height) ? '' : ' height=\"'.\$height.'\"';"
			. "\n \$src = \$this->get".$fieldMayus."( \$nimage );"
			. "\n "
			. "\n \$effect = \$urlEffect;" 
			. "\n if(empty(\$src)){"
			. "\n 	\$src = 'ima/fotomiembro.jpg';"
			. "\n 	\$imgParam .= \$imgParam2;"
			. "\n 	\$urlImage .= \$src;"
			. "\n 	\$title = '';"			
			. "\n }else{"
			. "\n \$file = ProjectLibrary_SDO_Core_Application_FileManagement::LoadById(\$src);"
			. "\n \$title = \$file->getDescription();"
			. "\n \$urlImage = BO_DIRECTORY . 'file.php?id=' . \$src . '&type=image';"
			. "\n 	\$src = BO_DIRECTORY . 'file.php?id=' . \$src . '&type=image&img_size=predefined&width='.\$width.'&height=' . \$height;"
			. "\n }"
			. "\n \$img = \$target_blank ? '<a '. (\$effect ? 'rel=\"jquery-lightbox\"' : '' ) .' href=\"'.\$urlImage.'\" id=\"ImagePreviewId\" target=\"_blank\">' : '';"
			. "\n \$imgParam .= !\$target_blank ? ' title=\"'.\$title.'\"' : '';"
			. "\n \$img .= \"<img src='\".\$src.\"' \".\$imgParam.\">\";"
  			. "\n  \$img .= ( \$target_blank ? '</a>' : '');"
			. "\n return \$img;"
			. "\n } \n\n";
		}
		$FinalResult2 .= "public function set".$fieldMayus."( \$".$fieldMayus."){ \n";
		$FinalResult2 .= "return \$this->".$field." = \$".$fieldMayus.";\n";		
		$FinalResult2 .= "}";
	}

	return $FinalResult1 . $FinalResult2;
}


?>