<?php
class ProjectLibrary_FrontEnd_Util_SurveySession {
	
	const NAMESPACE = 'ProjectLibrary_survey_session';
	

	/**
	 * @return ProjectLibrary_Entity_User_Profile
	 */
	public static function GetIdentity(){
		return ProjectLibrary_FrontEnd_Util_Session::Get( self::NAMESPACE );
	}
	
	public static function DestroyIdentity(){
		if ( self::HasIdentity() ){
			ProjectLibrary_FrontEnd_Util_Session::Set( self::NAMESPACE, null );
		}
	}
	
	/**
	 * @param ProjectLibrary_Entity_User_Profile $profile
	 */
	public static function SetIdentity( array $surveyResponses ){
		ProjectLibrary_FrontEnd_Util_Session::Set( self::NAMESPACE, $surveyResponses );
	}
	
}
?>