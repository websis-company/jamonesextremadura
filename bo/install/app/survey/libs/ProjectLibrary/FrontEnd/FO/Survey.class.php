<?php
class ProjectLibrary_FrontEnd_FO_Survey extends ProjectLibrary_FrontEnd_FO {

	/**
	 * @var ProjectLibrary_DAO_Event
	 */

	public function __construct(){
		parent::__construct();
		
	}

	public function execute( $cmd='' ){
		
		$this->checkPermission();
		$r=null;
		switch( $cmd ){
			case 'vote':
				$r = $this->_vote();
			break;
			case 'loadSurvey':
				$r = $this->_loadSurvey();
			break;
			case 'getSurvey':
			default:
				$r = $this->_getSurvey();
				break;
		}
		return $r;
	}
	protected function _vote(){
		$surveyResponseId = $this->getParameter('response',true,0);
		$survRes = ProjectLibrary_SDO_Survey::LoadSurveyResponse( $surveyResponseId );
		$survRes->setTotal( $survRes->getTotal()+1 );
		$ban = false;
		try{
			$surveys = ProjectLibrary_FrontEnd_Util_SurveySession::GetIdentity();
			$surveys = empty($surveys) ? array() : $surveys;
			$surveyId = $survRes->getSurveyId();
			if( !in_array($surveyId, $surveys) ){
				$surveys[] = $surveyId;
				ProjectLibrary_FrontEnd_Util_SurveySession::SetIdentity($surveys);
				$surveyR = ProjectLibrary_SDO_Survey::SaverSurveyResponse($survRes);
			}
			
		}catch( Exception $e ){
		}
		return $this->_loadSurvey();
	}
	
	protected function _loadSurvey(){
		$surveyId = $this->getParameter();
		
		$survey = ProjectLibrary_SDO_Survey::LoadSurvey($surveyId );
		return $survey; 
	}
	protected function _getSurvey(){
		$surveyPosition = $this->getParameter('position',false,'');
		
		$this->search = new ProjectLibrary_Entity_Search(
			'', //disabled search
			'date DESC',
			1, //page number one
			 1 //we need only one register
		);
		$extraConditions = array();
		
		$extraConditions[] = array( "columName"=>"position","value"=>$surveyPosition,"isInteger"=>false);
		$extraConditions[] = array( "columName"=>"position","conector"=>' != ', "value"=>'hidden',"isInteger"=>false);
		$result = ProjectLibrary_SDO_Survey::SearchSurvey( $this->search, $extraConditions );
		$survey  = $result->getResults();
		
		$surveyIt = $survey->getIterator();
		$r = $surveyIt->current();
		
		$survey = !empty($r) ? $r : new ProjectLibrary_Entity_Survey();
		
		return $survey; 
	}

}
?>