<?php
require_once("../common.php");

$name = $_REQUEST["selectName"];
$id = $_REQUEST["id"];
$country = $_REQUEST["selectedCountry"];
$class = $_REQUEST["CSSclass"];
$paramsSelect = $_REQUEST["selectParams"];
$defaultText = $_REQUEST["defaultText"];
$defaultValue = $_REQUEST["defaultValue"];

$name = empty($name) ? 'state' : $name;
$id = empty($id) ? 'state' : $id;
$country = empty($country) ? 'MX' : $country;

$states = Util_State::getHTMLSelect($country, $name, '', $id, $class, $paramSelect, array(), $defaultText, $defaultValue);

echo $states;

?>