<?php
$products = $this->result->getResults();

$parent = $this->parent;
$data = array();
foreach( $products as $product ){
	$data[] = get_object_vars( $product );
}

$listPanel = new GUI_ListPanel_ListPanel( "Product" );
$listPanel->setData( $data );

$hiddenColumns = array( "image_id" , "discount","weight","volume","description_short" , "description_long", "dimensions",  "finished","materials", "colors", "array_images", "brand_id" );

foreach( $hiddenColumns as $col ){
	$listPanel->addHiddenColumn( $col );
}

$columnNamesOverride = array( 'product_id'=>'ID', 'sku'=>$this->trans('sku'), 'status'=>$this->trans('status'), 'price'=>$this->trans('price'), 'currency'=>$this->trans('currency'), 'name'=>$this->trans('name'), 'brand_name'=>$this->trans('brand'));

foreach( $columnNamesOverride as $col => $name ){
	$listPanel->addColumnNameOverride( $col, $name );
}


$listPanel = new GUI_ListPanel_Decorator_Sorter( $listPanel );
$listPanel->setOrderBy( $this->result->getOrderBy() );

$listPanel = new GUI_ListPanel_Decorator_RowActions( $listPanel );

$listPanel->addHiddenField("category_id", $this->category->getCategoryId() );
$listPanel->addHiddenField("cmd", ( $this->getProductsInCategory ) ? 'listProducts' : 'listProducts2Add' );

if( $this->getProductsInCategory ){
	// Delete
	$actDelete = new GUI_ListPanel_Action( '?cmd=deleteProduct&category_id=' . $this->category->getCategoryId() . '&idProduct={$product_id}', 'img/delete.png' );
	$actDelete->setImageTitle( $this->trans('delete').' '.$this->trans('product').' '.$this->trans('from').' '.$this->trans('this(f)').' '.$this->trans('category') );
	$actDelete->setOnClickEvent( 'return confirm("'.$this->trans('?_confirm_delete').' '.strtolower($this->trans('the').' '.$this->trans('product').' \"{$name}\" '.$this->trans('from').' '.$this->trans('this(f)').' '.$this->trans('category')).'?");' );
	$listPanel->addAction( $actDelete );
}
else{
	// Add
	$actAdd = new GUI_ListPanel_Action( '?cmd=addProduct&category_id=' . $this->category->getCategoryId() . '&idProduct={$product_id}', 'img/apply.png' );
	$actAdd->setImageTitle( $this->trans('add').' '.$this->trans('product').' '.$this->trans('to').' '.$this->trans('this(f)').' '.$this->trans('category') );
	$actAdd->setOnClickEvent( 'return confirm("'.$this->trans('?_confirm_add').' '.strtolower($this->trans('the').' '.$this->trans('product').' \"{$name}\" '.$this->trans('to').' '.$this->trans('this(f)').' '.$this->trans('category')).'?");' );
	$listPanel->addAction( $actAdd );
}
// Add "Search" capabilities
$listPanel = new GUI_ListPanel_Decorator_Searcher( $listPanel );
$listPanel->setFieldLabel( '<img src="img/search2_small.png" style="vertical-align:middle">&nbsp;' );
$listPanel->setButtonLabel($this->trans('search'));
$listPanel->setClearButtonLabel($this->trans('clear'));
$listPanel->setSearchString( $this->result->getKeywords() );

// Add paging capabilities
$listPanel = new GUI_ListPanel_Decorator_Pager( $listPanel );
$listPanel->setCurrentPage( $this->result->getPage() );
$listPanel->setPagingLabel($this->trans('page'));
$listPanel->setPreviousLabel('&#9668; '.$this->trans('previous'));
$listPanel->setNextLabel($this->trans('next').' &#9658;');
$listPanel->setFirstPageLabel($this->trans('first(f)').' '.$this->trans('page'));
$listPanel->setLastPageLabel($this->trans('last(f)').' '.$this->trans('page'));
$listPanel->setOffsetLabel($this->trans('results_per').' '.strtolower($this->trans('page')));
$listPanel->setTotalPages( $this->result->getTotalPages() ); //TODO:

include_once ( "header.php" );

?>
<div class="container" style="width: 100%; text-align: left;">

	<?php
	$this->display( 'category/info.php' );
	?>
	<div align="right">
	<a href="<?=ABS_HTTP_URL?>bo/backoffice/category.php?cmd=list&category_id=<?php echo $parent; ?>"><img src="img/rewind.png" border="0" hspace="5" style="vertical-align:text-bottom">Regresar a categorias relacionadas</a>	
</div>
	<div class="subtitle"> 
		<?php 
		echo ( $this->getProductsInCategory ) ? 
			$this->trans('product').'s '.strtolower($this->trans('associated_to(p)')).' '.$this->trans('this(f)').' '.$this->trans('category') :
			$this->trans('associate').' '.$this->trans('product').'s '.$this->trans('to').' '.$this->trans('this(f)').' '.$this->trans('category');
		?>
	</div>
	<div class="success">
	<?php if ( isset( $this->strSuccess ) ) echo $this->strSuccess; ?>
</div>

<div class="error">
	<h2><?php if ( isset( $this->strError ) ) echo $this->strError;	?></h2>
</div>

	<?php
	
	if( $this->getProductsInCategory ){
		echo '<a href="?cmd=listProducts2Add&category_id=' . $this->category->getCategoryId() . '"><img src="img/wand.png" border="0" hspace="2" style="vertical-align:middle">'.$this->trans('add').' '.$this->trans('new').' '.$this->trans('product').'</a><br />';
	}
	$listPanel->display();
	?>
</div>
<?php
include_once ( "footer.php" );
?>