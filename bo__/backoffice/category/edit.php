<?php
//$treeCategories = '';
include_once ( "header.php" );
function dataToInput( $value ){
	return htmlentities( $value, ENT_QUOTES, CHARSET_PROJECT );
}

$errors    = $this->errors;
$category  = $this->category;
$ArrStatus = $this->ArrStatus;

?>

<h2 align="left" style="margin:0px;"><? echo $this->strSubtitles; ?></h2>

<form name="frmCategory" id="frmCategory" method="post" action="category.php?category_id=<?php echo $category->getParentCategoryId();?>" onsubmit="" enctype="multipart/form-data">
  <div class="error" align="left"><?php echo $errors->getDescription(); ?></div>	
	<fieldset>
		<legend><?=$this->trans('information')?></legend>
		<dl class="form">
			<dt><?=$this->trans('parent')?>: </dt>
			<dd>
				<select class="frmInput" name="entity[parent_category_id]" id="parent_category_id">
					<? echo $this->treeCategories; ?>
				</select>
			</dd>
			<?php $errors->getHtmlError( 'parent_category_id' ); ?>

			<dt><?=$this->trans('name')?>: *</dt>
			<dd>
				<input type="text" size="30" maxlength="100" class="frmInput"
				       name="entity[name]" id="name"
				       value="<?php echo dataToInput( $category->getNameInDefaultLanguage()  ) ?>">
			</dd>
			<?php $errors->getHtmlError( 'name'); ?>

			<dt><?=$this->trans('description')?>: *</dt>
			<dd>
				<textarea rows="10" cols="50" class="frmInput" name="entity[description]" id="description"
				><?php echo dataToInput( $category->getDescriptionInDefaultLanguage() ) ?></textarea>
			</dd>
			<?php $errors->getHtmlError( 'description'); ?>
			
			<dt>Orden: *</dt>
			<dd>
				<input type="text" size="30" maxlength="100" class="frmInput"
				       name="entity[orden]" id="orden"
				       value="<?php echo dataToInput( $category->getOrden()  ) ?>">
			</dd>
			<?php $errors->getHtmlError( 'orden'); ?>
            
            <dt><?=$this->trans('status')?>: *</dt>
			<dd>
				<?php
					echo '<select name="entity[status]">';
					foreach( $ArrStatus as $Status){
						echo "<option value='{$Status}'";						
						if( $category->getStatus() == $Status ){
							echo " selected='selected'";
						}						
						echo ">".$this->trans(strtolower($Status))."</option>";							
					}
					echo "</select>";
				?>
			</dd>

			<!-- <dt><?//=$this->trans('image')?>: *</dt>
			<dd>
				<input type="file" class="frmInput" name="image_file" size="30" />
				<input type="hidden" name="entity[image_id]" value="<?php //echo dataToInput( $category->getImageId() ) ?>" />
				<?php
					/*if( $category->getImageId() != 0){
						echo '<a href="../file.php?id=' . $category->getImageId() . '&type=image" class="thickbox">'.$this->trans('show').' '.$this->trans('current_image').'</a>'.
						'<br /><label><input type="checkbox" name="entity[image_delete]" style="vertical-align:middle;margin-bottom:6px" />&nbsp;'.$this->trans('delete').' '.$this->trans('current_image').'</label>';
					}*/
				?>
			</dd>
			<?php //$errors->getHtmlError( 'image_file'); ?> -->
			<dt><?=$this->trans('image')?>: <br> Tamaño de Imagen <strong>700x487pixeles</strong>*</dt>
			<dd>
			<?php 
				$hideDes = is_null($element['hide_description']) ? false : $element['hide_description'];
				$imageHandler = new eCommerce_SDO_Core_Application_ImageHandler("image_id", 1 );
				$imageHandler->setArrImages( $category->getImageId() );
				$html = $imageHandler->createBOImages(null,null,$hideDes);
				echo $html;
			?>
			<dd>
			<?php $errors->getHtmlError( 'image_id'); ?>


			<dt><?=$this->trans('Banner')?>: <strong>Tamaño de Imagen 1920x300pixeles</strong>*</dt>
			<dd>
			<?php 
				$hideDes = is_null($element['hide_description']) ? false : $element['hide_description'];
				$imageHandler = new eCommerce_SDO_Core_Application_ImageHandler("banner_id", 1 );
				$imageHandler->setArrImages( $category->getBannerId() );
				$html = $imageHandler->createBOImages(null,null,$hideDes);
				echo $html;
			?>
			<dd>
			<?php $errors->getHtmlError( 'image_id'); ?>
		</dl>
	</fieldset>

<?php if($this->strCmd !='add' && false){ ?>	
	<fieldset>
		<legend><?=$this->trans('other_languages')?></legend>
		<iframe style='width:650px;height:400px;' frameborder="0" src='categoryInDiferrentLanguage.php?&CategoryId=<?=$category->getCategoryId(); ?>'></iframe>
	</fieldset>
<?php } ?>
	
	<input type="hidden" name="entity[category_id]" value="<?php echo dataToInput( $category->getCategoryId() ) ?>">	
	<input type="hidden" name="cmd" value="save">
	<input type="submit" value="<?=$this->trans('save')?>" class="frmButton">
	<input type="button" value="<?=$this->trans('cancel')?>" class="frmButton" onclick="window.location.href='category.php'">
</form>

<?php
include_once ( "footer.php" );
?>