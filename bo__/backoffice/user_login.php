<?php
require_once( "../common.php" );

/*include_once( "header.php" );
?>
<br>
<br>
<div class="error" align="center"><?php if ( isset( $this ) && isset( $this->message ) ) echo $this->message; ?></div>

<form method="post" action="">
<input type='hidden' name='cmd' value='login' />
<table border="0" align="center">
  <tr>
    <td colspan="2" align="center" bgcolor="#ededed" style="color: #000000; font-weight: bold;">
      <?=$this->trans('authorized_access_only')?>
    </td>
  </tr>
  <tr>
    <td><?=$this->trans('username')?>:</td>
    <td><input type="text" name="username" size="20"></td>
  </tr>
  <tr>
    <td><?=$this->trans('password')?>:</td>
    <td><input type="password" name="password" size="20"></td>
  </tr>
  <tr>
    <td colspan="2" align="center">
    <input type="submit" value="<?=$this->trans('access')?>" style="color: #000000; font-size: 8pt; border-style: solid; border-width: 1px; padding-left: 4px; padding-right: 4px; padding-top: 1px; padding-bottom: 1px; background-color: #ededed"> 
    </td>
  </tr>
  <tr>
    <td colspan="2" align="center">
    <i><?=$this->trans('access_attempts_recorded')?></i>
    </td>
  </tr>
</table>
</form>

<?php
include_once( "footer.php" );*/
?>


<?php include_once( "includes/header.php" ); ?>


<div class="login-box-body">
    <div class="login-logo" align="center" style="text-align: -webkit-center;">
        <img src="<?=$config["logo_project"]?>?>" class="img-responsive">
    </div>
    <p class="login-box-msg"><?=$this->trans('authorized_access_only')?></p>
    <?php if(isset($this) && isset($this->message )){?>
        <div class="alert alert-danger" style="text-align: center;">
            <strong><i class="icon fa fa-ban"></i><?php echo $this->message;?></strong>
        </div>
    <?php
    }//end if?>

    <div class="clearfix"></div>
    <form id="loginForm" action="" method="post">
        <input type='hidden' name='cmd' value='login' />
        <div class="form-group has-feedback">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            <input type="email" id="username" name="username" class="form-control" placeholder="Email">
        </div>
        <div class="form-group has-feedback">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            <input type="password" id="password" name="password" class="form-control" placeholder="Password">
        </div>
        <div class="row">
            <div class="col-xs-5 pull-right">
                <button type="submit" class="btn btn-success btn-block btn-flat">Ingresar</button>
            </div><!-- /.col -->
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("#loginForm").validate({
            debug: false,
            rules: {
                username: {required: true, 'email': true},
                password: {required: true,},
            },
            messages: {
                username:{required: 'Username es requerido', email:'Debe ser un correo válidos'},
                password:{required: 'Password es requerido',},
            }
        });
    });
</script>
<?php include_once( "includes/footer.php" ); ?>