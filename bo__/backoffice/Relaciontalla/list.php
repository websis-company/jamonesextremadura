<?php

$users = $this->options->getResults();

$viewConfig = $this->viewConfig;

//params of the variable viewConfig
$paramsUsed = "name,title,id";
$paramsArrUsed = "columNamesOverride,hiddenColums,hiddenFields";

$paramsUsed = explode(",",$paramsUsed);

foreach( $paramsUsed as $param){
	$viewConfig[ $param ] = empty( $viewConfig[ $param ] ) ? '' : $viewConfig[ $param ];
}


$paramsArrUsed = explode(",",$paramsArrUsed);

foreach( $paramsArrUsed as $param){
	$viewConfig[ $param ] = empty( $viewConfig[ $param ] ) ? array() : $viewConfig[ $param ];
}


$data = array();
foreach($users as $user){
	$data[] = get_object_vars( $user );
}
		

$listPanel = new GUI_ListPanel_ListPanel( $viewConfig['name'] );
$listPanel->setData( $data );

function showNombre($product_id){	
	$product = eCommerce_SDO_Core_Application_Version::LoadById($product_id);
	$name = $product->getName();
	
	return $name;
}
function showTalla($tabla_id){	
	return eCommerce_SDO_Talla::LoadTalla($tabla_id)->getNombre();
}
$listPanel->addCallBack('tabla_id', 'showNombre');
$listPanel->addCallBack('talla_id', 'showTalla');


foreach( $viewConfig['columNamesOverride'] as $colum => $rename){
	$listPanel->addColumnNameOverride($colum,$rename);
}


foreach( $viewConfig['hiddenColums'] as $col ){
	$listPanel->addHiddenColumn( $col );
}

foreach( $viewConfig['hiddenFields'] as $name=> $value ){
	$listPanel->addHiddenField($name, $value );
}

// Add "Order By" capabilities
$listPanel = new GUI_ListPanel_Decorator_Sorter( $listPanel );
$listPanel->setOrderBy( $this->options->getOrderBy() );


// Add actions
$listPanel = new GUI_ListPanel_Decorator_RowActions( $listPanel );
$listPanel->setHeaderLabel( 'Acciones' );

// ADD - REMOVE
$actModify = new GUI_ListPanel_Action( '?cmd='.$this->cmd2.'&id=' . $viewConfig['id'] . '&tipo=' . $this->tipo . '&table_id='.$this->table_id, 'img/' . $this->icon );
$actModify->setImageTitle( $this->lblAction );
$listPanel->addAction( $actModify );

// Add "Search" capabilities
$listPanel = new GUI_ListPanel_Decorator_Searcher( $listPanel );
$listPanel->setFieldLabel( '<img src="img/search2_small.png">' );
$listPanel->setButtonLabel( 'Buscar' );
$listPanel->setClearButtonLabel( " Limpiar " );
$listPanel->setSearchString( $this->options->getKeywords() );

// Add paging capabilities
$listPanel = new GUI_ListPanel_Decorator_Pager( $listPanel );
$listPanel->setCurrentPage( $this->options->getPage() );

//echo $this->totalPages;
$listPanel->setTotalPages( $this->options->getTotalPages() ); //TODO:

//$listPanel->setOffsetIncrement( $this->options->getResultsPerPage() );
$listPanel->setOffset( $this->options->getResultsPerPage() );

$strSuccess = $this->strSuccess;
$strError = $this->strError;
include_once ( "header_iframe.php" );
?>

<div class="container" style="width: 100%; text-align: left;">
<div class="subtitle"><?php echo $viewConfig['title']; ?></div>
<?if(!empty($strSuccess)){?><div class="success"><?=$strSuccess?></div><?}?>
<?if(!empty($strError)){?><h2><div class="error"><?=$strError?></h2></div><?}?>

<?if($this->addLinkObj){?><a href="<?=$this->addLinkObj?>" style="float:left;"><img src="img/wand.png" border="0" hspace="2" align="absmiddle">Agregar <?php echo $viewConfig['name']; ?></a><?}?> 
<a href="javascript:window.parent.Shadowbox.close();" style="float:right;"><img src="img/rewind.png" border="0" hspace="2" align="absmiddle">Cerrar</a> <br>
<br>
  <?php
		$listPanel->setTableWidth( '100%' );
		$listPanel->display();
		?>
</div>
<?php
include_once ( "footer.php" );
?>