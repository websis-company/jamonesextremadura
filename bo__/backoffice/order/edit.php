<?php
include_once ( "header.php" );
$errors    = $this->errors;
$order     = $this->order; 
$orderHtml = $this->orderHtml;
$addresses = $order->getAddresses();
?>

<h2 align="left" style="margin:0px;"><? echo $this->strSubtitles; ?></h2><br />

<form name="frmOrder" id="frmProduct" method="post" action="order.php" onsubmit="" enctype="multipart/form-data">

  <div class="error">
		<?php //$errors->getDescription(); ?>
  </div>
	
	<fieldset>
		<legend>Order</legend>
		<?php echo $orderHtml; ?>
	</fieldset>
	<br />
	<!--<fieldset>
		<legend>Direcci&oacute;n de Facturaci&oacute;n</legend>
		<dl class='form'>
		<?php
			//$address  = $addresses[ eCommerce_SDO_Order::ADDRESS_BILL_TO ];			
			/*if(!empty($address)){
				$country = eCommerce_SDO_CountryManager::GetCountryById( $address->getCountry() );
				$state = eCommerce_SDO_CountryManager::GetStateById( $address->getState(), $country["country_short_name"] );
				$html =
				"<dt>Calle:</dt><dd>". $address->getStreet() .", ".$address->getStreet2() . "</dd>" .
				"<dt>Colonia:</dt><dd>". $address->getStreet3(). "</dd>" .
				"<dt>C.P.:</dt><dd>". $address->getZipCode() . "</dd>" .				
				"<dt>Municipio o Delegación:</dt><dd>". $address->getCity() . "</dd>" .
				"<dt>Estado:</dt><dd>". $state["name"] . "</dd>" .
				"<dt>País:</dt><dd>". $country["name"] . "</dd>" .				
				"<dt>Razón Social:</dt><dd>". $address->getRazonSocial() . "</dd>" .
				"<dt>RFC:</dt><dd>". $address->getRfc() . "</dd>" .
				"<dt>Telefono:</dt><dd>". $address->getPhone() . "</dd>" .
				"<dt>Telefono Móvil:</dt><dd>". $address->getPhone2() . "</dd>" .
				"";				
				echo $html;
			}*/
		?>
		</dl>
	</fieldset>-->
		<br />
	<fieldset>
		<legend>Direcci&oacute;n de Env&iacute;o</legend>
		<dl class='form'>
		<?php
			$address  = $addresses[ eCommerce_SDO_Order::ADDRESS_SHIP_TO ];
			$contacto = is_null($address->getContacto()) ? $address->getFirstName()." ".$address->getLastName() : $address->getContacto();
			if(!empty($address)){
				$country = eCommerce_SDO_CountryManager::GetCountryById( $address->getCountry() );
				$state = eCommerce_SDO_CountryManager::GetStateById( $address->getState(), $country["country_short_name"] );				
				$html =
				"<dt>Calle:</dt><dd>". $address->getStreet() .", ".$address->getStreet3() .  "</dd>" .
				"<dt>Colonia:</dt><dd>". $address->getStreet2(). "</dd>" .
				"<dt>C.P.:</dt><dd>". $address->getZipCode() . "</dd>" .
				"<dt>Telefono:</dt><dd>". $address->getPhone() . "</dd>" .
				//"<dt>Telefono Móvil:</dt><dd>". $address->getPhone2() . "</dd>" .
				"<dt>Municipio o Delegación:</dt><dd>". $address->getCity() . "</dd>" .
				"<dt>Estado:</dt><dd>". $state["name"] . "</dd>" .
				"<dt>Pa&iacute;s:</dt><dd>". $country["name"] . "</dd>" .				
				"<dt>Nombre de quien recibe:</dt><dd>". $contacto . "</dd>" .					
				"";				
				echo $html;			
			}
		?>
		</dl>
	</fieldset>
	<br />
	
	<fieldset>
		<legend>Informaci&oacute;n Personal</legend>
		<dl class='form'>
		<?php
		$profile = eCommerce_SDO_User::LoadUserProfile( $order->getProfileId() );
		$html =
		"<dt>Nombre:</dt><dd>". $profile->getFirstName(). "</dd>" .
		"<dt>Apellidos:</dt><dd>". $profile->getLastName(). "</dd>" .
		"<dt>Email:</dt><dd>". $profile->getEmail() . "</dd>" .
		"<br /><dt><a href='user.php?cmd=edit&id=". $profile->getProfileId(). "'>Edit Profile</a></dt>" .
		"<dt></dt><dd></dd>";
		
		echo $html;
		?>
		</dl>
	</fieldset>
	<br />
	<!--<fieldset>
		<legend>Comentarios</legend>
		<?php echo $order->getComments(); ?>
	</fieldset>-->
	
	<br />
		<fieldset>
		<legend>Informaci&oacute;n adicional</legend>
		
		<dl class="form">
			<dt>M&eacute;todo de pago</dt>
			<dd><?php echo eCommerce_SDO_Order::GetPaymentMethodById( $order->getPaymentMethod() ); ?></dd>
			<br />
			<dt>Enviado ?</dt>
			<dd>
				<select name='entity[enviado]'>
				<?php
					$arrEnviado = eCommerce_SDO_Core_Application_Order::GetEnumValues('enviado');

					foreach( $arrEnviado as $key => $status ){
						echo "<option value='{$key}'";
						if( $order->getEnviado() == $key){
							echo " selected='selected'";
						}
						echo ">{$status}</option>";
					}
				?>
				</select>
			</dd>

			<br/>
			<dt>Status de la orden</dt>
			<dd>
				<select name='entity[status]'>
				<?php
					$arrStatus = eCommerce_SDO_Order::GetArrStatus();
					foreach( $arrStatus as $key => $status ){
						echo "<option value='{$key}'";
						if( $order->getStatus() == $key){
							echo " selected='selected'";
						}
						echo ">{$status}</option>";
					}
				?>
				</select>
			</dd>


		</dl>
	</fieldset>
	
	<p align='left'>
	<input type='hidden' name='cmd' value='save'>
	<input type='hidden' name='entity[id]' value='<?=$order->getOrderId()?>'>
	<input type='submit' value='Save' >
	</p>
</form>

<?php
include_once ( "footer.php" );
?>