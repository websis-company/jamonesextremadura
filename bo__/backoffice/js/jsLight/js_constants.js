var BASEDIR = "/";
var LANG = "en-US";
var APP_ID = "1";
var APP_SHORTNAME = "firefox";
var APP_PRETTYNAME = "Firefox";
var LATEST_FIREFOX_VERSION = '3.0.3';
var LATEST_FIREFOX_DEVEL_VERSION = '3.1b1';
var addOnNotAvailableForPlatform = "%1$s is not available for %2$s.";
var error_opensearch_unsupported = "Sorry, you need a Mozilla-based browser (such as Firefox) to install a search plugin.";

var app_compat_update_firefox = '<a href="http://getfirefox.com">Upgrade Firefox</a> to use this add-on';
var app_compat_try_old_version = 'An <a href="%1$s">older version</a> may work';
var app_compat_older_firefox_only = 'This add-on is for older versions of Firefox';
var app_compat_unreleased_version = 'This add-on requires the not-yet released <a href="%1$s">Firefox %2$s</a>';
var app_compat_older_version_or_ignore_check = 'You can <a href="%1$s">try an older version</a> or <a href="#" onclick="%2$s">ignore this check</a>';

var app_compat_ignore_check = 'Ignore version check';

