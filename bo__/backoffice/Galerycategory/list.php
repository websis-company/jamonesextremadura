<?php

function subText($txt) {
	return Util_String::subText($txt, 150);
}

function showImage($image) {
	$image = explode(',', $image);
	if(!empty($image[0])) {
		$file = eCommerce_SDO_Core_Application_FileManagement::LoadById($image[0]);
		$imgSrc = '../file.php?id='.$image[0].'&type=image';
		return  '<a class="thickbox" href="'.$imgSrc.'" title="'.$file->getDescription().'">'.
						'<img src="../file.php?id=' . $image[0] . '&type=image&img_size=predefined&width=50&height=50" border="0" />'.
						'</a>';
	}
	return '';
}

function parseImage($image_id){
	if ( empty( $image_id ) ){
		return '';
	}
	else {
		return '<a class="thickbox" href="../file.php?id=' . $image_id . '&type=image" target="_blank"><img border="0" src="../file.php?id=' . $image_id . '&type=image&img_size=predefined&height=50&width=51" /></a> ';
	}
}

$listPanel = new GUI_ListPanel_ListPanel( 'Galerycategory' );
$listPanel->setData( $this->data );


$hiddenColumns = array( 'parent_category_id','status','image_id');
foreach( $hiddenColumns as $col ){
	$listPanel->addHiddenColumn( $col );
}

$columnNamesOverride = array(
	'galerycategory_id'=>'ID', 
	'name'=>$this->trans('name'), 
	'description'=>$this->trans('description'), 
	'image_id'=>$this->trans('image'),
	'banner_id' => 'Iamgen Banner',
);
foreach($columnNamesOverride as $col => $name) {
	$listPanel->addColumnNameOverride( $col, $name );
}

$listPanel->addCallBack('image_id', 'showImage');
$listPanel->addCallBack('banner_id', 'showImage');

$listPanel->addColumnAlignment( 'name', 'left');
$listPanel->addColumnAlignment( 'description', 'left');

// Add "Order By" capabilities
$listPanel = new GUI_ListPanel_Decorator_Sorter( $listPanel );
$listPanel->setOrderBy( $this->options->getOrderBy() );

// Add actions
$listPanel = new GUI_ListPanel_Decorator_RowActions( $listPanel );
$listPanel->setHeaderLabel( $this->trans('catalog') );

// Modify SubCategories
$actModifySubcategories = new GUI_ListPanel_Action( '?cmd=list&galerycategory_id={$galerycategory_id}', 'img/chart_organisation.png' );
$actModifySubcategories->setImageTitle( $this->trans('manage').' Sub'.strtolower($this->trans('categories')) );
$listPanel->addAction( $actModifySubcategories );

// Show Products
/*$actShowProducts = new GUI_ListPanel_Action( 'productcategory.php?cmd=listProducts&galerycategory_id={$galerycategory_id}', 'img/application_cascade.png' );
$actShowProducts->setImageTitle( $this->trans('manage').' '.$this->trans('product').'s' );
$listPanel->addAction( $actShowProducts );*/

// Add actions
$listPanel = new GUI_ListPanel_Decorator_RowActions( $listPanel );
$listPanel->setHeaderLabel( $this->trans('actions') );


// Modify

$actModify = new GUI_ListPanel_Action( '?cmd=edit&id={$galerycategory_id}&parent={$galerycategory_id}', 'img/write.png' );
$actModify->setImageTitle( $this->trans('edit').' '.$this->trans('category') );
$listPanel->addAction( $actModify );

// Delete
$actDelete = new GUI_ListPanel_Action( '?cmd=delete&id={$galerycategory_id}&galerycategory_id=' . $this->category->getGalerycategoryId(), 'img/delete.png' );
$actDelete->setImageTitle( $this->trans('delete').' '.$this->trans('category') );
$actDelete->setOnClickEvent( 'return confirm("'.$this->trans('?_confirm_delete').' '.strtolower($this->trans('the(f)')).' '.strtolower($this->trans('category')).' \"{$name}\"?");' );
$listPanel->addAction( $actDelete );


// Add "Search" capabilities
$listPanel = new GUI_ListPanel_Decorator_Searcher( $listPanel );
$listPanel->setFieldLabel( '<img src="img/search2_small.png" style="vertical-align:middle">&nbsp;' );
$listPanel->setButtonLabel($this->trans('search'));
$listPanel->setClearButtonLabel($this->trans('clear'));
$listPanel->setSearchString( $this->options->getKeywords() );

// Add paging capabilities
$listPanel = new GUI_ListPanel_Decorator_Pager( $listPanel );
$listPanel->setCurrentPage( $this->options->getPage() );
$listPanel->setPagingLabel($this->trans('page'));
$listPanel->setPreviousLabel('&#9668; '.$this->trans('previous'));
$listPanel->setNextLabel($this->trans('next').' &#9658;');
$listPanel->setFirstPageLabel($this->trans('first(f)').' '.$this->trans('page'));
$listPanel->setLastPageLabel($this->trans('last(f)').' '.$this->trans('page'));
$listPanel->setOffsetLabel($this->trans('results_per').' p&aacute;gina');
$listPanel->setTotalPages( 1 ); //TODO:
$listPanel->setOffset( $this->options->getResultsPerPage() );


include_once ( "header.php" );

?>
	
	<div class="container" style="width: 100%; text-align: left;">
	
	<?php 
		if( $this->category->getGalerycategoryId() != 0 ) {
			$this->display( 'Galerycategory/info.php' );
			?>
			<div align="right">
				<a href="?cmd=list&galerycategory_id=<?php echo $this->category->getParentCategoryId(); ?>"><img src="img/rewind.png" border="0" hspace="5" style="vertical-align:text-bottom"><?=$this->trans('go_back_to_parent_category')?></a>
			</div>
			<?php 
		} 
	?>
	<div class="subtitle"><?php if( $this->category->getGalerycategoryId() != 0 ) echo 'Sub-'; echo $this->trans('categories'); ?></div>
	<div class="success">
		<?php if ( isset( $this->strSuccess ) ) echo $this->strSuccess; ?>
	</div>
	<div class="error">
		<h2><?php if ( isset( $this->strError ) ) echo $this->strError;	?></h2>
	</div>
	
	<a href="?cmd=add&parent_category_id=<?php echo $this->category->getGalerycategoryId(); ?>"><img src="img/wand.png" border="0" hspace="2" align="absmiddle"><?php echo $this->trans('add').' '.$this->trans('new(f)').' '; if( $this->category->getGalerycategoryId() != 0 ) echo 'Sub-'; echo $this->trans('category') ?></a> <br>
	<br>
  <?php
		$listPanel->setTableWidth( '100%' );
		$listPanel->display();
	?>
	<a href="?toExcel=1&<?php echo str_replace( "cmd=edit&",'',$_SERVER['QUERY_STRING']) ?>"><img src="img/excel.png" style="vertical-align:text-bottom" border="0" hspace="5"><?=$this->trans('export_to')?> Excel</a>
</div>
<?php
include_once ( "footer.php" );
?>