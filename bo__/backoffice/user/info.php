<table border="0" width="100%" cellpadding="0" cellspacing="0" 
       style="border-top: 3px double #c0c0c0; border-bottom: 3px double #c0c0c0; margin-top:12px">
	<tr bgcolor="#e0e0e0">
		<td style="font-size: 1.5em; font-weight: bold;">
			<?=$this->trans('user')?>: <?php echo $this->profile; // __toString() invokation = Full Name; ?>
		</td>
		<td>
	</tr>
	<tr>
		<td valign="top">
			<dl class="form">
				<dt>ID: </dt>
				<dd><div class="dd_content">
					<?php echo $this->profile->getProfileId(); ?>
				</div></dd>
				
				<dt>Email: </dt>
				<dd><div class="dd_content">
					<?php echo $this->profile->getEmail(); ?>
				</div></dd>
				
				<dt><?=$this->trans('role')?>: </dt>
				<dd><div class="dd_content">
					<?php echo $this->profile->getRole(); ?>
				</div></dd>
			</dl>
		</td>
	</tr>
	<tr height="6px">
		<td ></td>
	</tr>
</table>
