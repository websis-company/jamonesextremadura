<?php
$lang = eCommerce_SDO_Core_Application_LanguageManager::GetActualLanguage();

$users = $this->options->getResults();
$data = array();
foreach($users as $user){
	$data[] = get_object_vars( $user );
}
		
$listPanel = new GUI_ListPanel_ListPanel( 'profile' );
$listPanel->setData( $data );


$hiddenColumns = array('password','confirm_password','saludo');

foreach( $hiddenColumns as $col ){
	$listPanel->addHiddenColumn( $col );
}

$columnNamesOverride = array('profile_id' => 'ID', 'first_name' => $this->trans('first_name'), 'last_name' => $this->trans('last_name'), 'role' => $this->trans('role'));

foreach( $columnNamesOverride as $col => $name ){
	$listPanel->addColumnNameOverride( $col, $name );
}


// Add "Order By" capabilities
$listPanel = new GUI_ListPanel_Decorator_Sorter( $listPanel );
$listPanel->setOrderBy( $this->options->getOrderBy() );

// Address Action
$listPanel = new GUI_ListPanel_Decorator_RowActions( $listPanel );
$listPanel->setHeaderLabel( $this->trans('address') );

// Modify
$actModify = new GUI_ListPanel_Action( '?cmd=editAddress&id={$profile_id}', 'img/house.png' );
$actModify->setImageTitle( $this->trans('edit').' '.$this->trans('address') );
$listPanel->addAction( $actModify );


// Add actions
$listPanel = new GUI_ListPanel_Decorator_RowActions( $listPanel );
$listPanel->setHeaderLabel( $this->trans('actions') );

// Modify
$actModify = new GUI_ListPanel_Action( '?cmd=edit&id={$profile_id}', 'img/write.png' );
$actModify->setImageTitle( $this->trans('edit').' '.$this->trans('user') );
$listPanel->addAction( $actModify );

// Delete
$actDelete = new GUI_ListPanel_Action( '?cmd=delete&id={$profile_id}', 'img/delete.png' );
$actDelete->setImageTitle( $this->trans('delete').' '.$this->trans('user') );
$actDelete->setOnClickEvent( 'return confirm("'.$this->trans('?_confirm_delete').' '.strtolower($this->trans('the')).' '.strtolower($this->trans('user')).' \"{$first_name}\"?");' );
$listPanel->addAction( $actDelete );

// Add "Search" capabilities
$listPanel = new GUI_ListPanel_Decorator_Searcher( $listPanel );
$listPanel->setFieldLabel( '<img src="img/search2_small.png" style="vertical-align:middle">&nbsp;' );
$listPanel->setButtonLabel($this->trans('search'));
$listPanel->setClearButtonLabel($this->trans('clear'));
$listPanel->setSearchString( $this->options->getKeywords() );

// Add paging capabilities
$listPanel = new GUI_ListPanel_Decorator_Pager( $listPanel );
$listPanel->setCurrentPage( $this->options->getPage() );
$listPanel->setPagingLabel($this->trans('page'));
$listPanel->setPreviousLabel('&#9668; '.$this->trans('previous'));
$listPanel->setNextLabel($this->trans('next').' &#9658;');
$listPanel->setFirstPageLabel($this->trans('first(f)').' '.$this->trans('page'));
$listPanel->setLastPageLabel($this->trans('last(f)').' '.$this->trans('page'));
$listPanel->setOffsetLabel($this->trans('results_per').' p&aacute;gina');

//echo $this->totalPages;
$listPanel->setTotalPages( $this->options->getTotalPages() ); //TODO:

//$listPanel->setOffsetIncrement( $this->options->getResultsPerPage() );
$listPanel->setOffset( $this->options->getResultsPerPage() );

include_once ( "header.php" );
?>

<div class="container" style="width: 100%; text-align: left;">
<div class="subtitle"><?=$this->trans('user')?>s</div>
<div class="success">
	<?php if ( isset( $this->strSuccess ) ) echo $this->strSuccess; ?>
</div>
<div class="error">
	<h2><?php if ( isset( $this->strError ) ) echo $this->strError;	?></h2>
</div>

<a href="?cmd=add"><img src="img/wand.png" border="0" hspace="2" align="absmiddle"><?=$this->trans('add').' '.$this->trans('new').' '.$this->trans('user')?></a> <br>
<br>
  <?php
		$listPanel->setTableWidth( '100%' );
		$listPanel->display();
		?>
	<a href="?toExcel=1&<?php echo str_replace( "cmd=edit&",'',$_SERVER['QUERY_STRING']) ?>"><img src="img/excel.png" style="vertical-align:text-bottom" border="0" hspace="5"><?=$this->trans('export_to')?> Excel</a>
</div>
<?php
include_once ( "footer.php" );
?>