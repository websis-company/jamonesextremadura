<?php

$products = $this->result->getResults();
$data = array();
foreach( $products as $product ){
	$data[] = get_object_vars( $product );
}

$listPanel = new GUI_ListPanel_ListPanel( "Product" );
$listPanel->setData( $data );

$hiddenColumns = array( "image_id" , "description_short" , "description_long", "colors", "materials", "finished", "dimensions", "discount",
"weight","volume","price","currency" );

foreach( $hiddenColumns as $col ){
	$listPanel->addHiddenColumn( $col );
}


$listPanel = new GUI_ListPanel_Decorator_Sorter( $listPanel );
$listPanel->setOrderBy( $this->result->getOrderBy() );

$listPanel = new GUI_ListPanel_Decorator_RowActions( $listPanel );

$listPanel->addHiddenField("artist_id", $this->artist->getArtistId() );
$listPanel->addHiddenField("cmd", ( $this->getProductsInArtist ) ? 'listProducts' : 'listProducts2Add' );

if( $this->getProductsInArtist ){
	// Delete
	$actDelete = new GUI_ListPanel_Action( '?cmd=deleteProduct&artist_id=' . $this->artist->getArtistId() . '&idProduct={$product_id}', 'img/delete.png' );
	$actDelete->setImageTitle( 'Delete product from this artist' );
	$actDelete->setOnClickEvent( 'return confirm("Are you sure you want to delete the product \"{$name}\" from this Artist?");' );
	$listPanel->addAction( $actDelete );
}
else{
	// Add
	$actAdd = new GUI_ListPanel_Action( '?cmd=addProduct&artist_id=' . $this->artist->getArtistId() . '&idProduct={$product_id}', 'img/apply.png' );
	$actAdd->setImageTitle( 'Add product to this artist' );
	$actAdd->setOnClickEvent( 'return confirm("Are you sure you want to add the product \"{$name}\" to this artist?");' );
	$listPanel->addAction( $actAdd );
}
// Add "Search" capabilities
$listPanel = new GUI_ListPanel_Decorator_Searcher( $listPanel );
$listPanel->setFieldLabel( '<img src="img/search2_small.png">' );
$listPanel->setButtonLabel( 'Search' );
$listPanel->setClearButtonLabel( " Clear " );
$listPanel->setSearchString( $this->result->getKeywords() );

// Add paging capabilities
$listPanel = new GUI_ListPanel_Decorator_Pager( $listPanel );
$listPanel->setCurrentPage( $this->result->getPage() );


$listPanel->setTotalPages( $this->result->getTotalPages() ); //TODO:

include_once ( "header.php" );

?>
<div class="container" style="width: 100%; text-align: left;">
	
	<?php
	//$this->display( 'category/info.php' );
	?>
	
	<div class="subtitle">
		<?php 
		echo ( $this->getProductsInArtist ) ? 
			"Products associated to this artist" :
			"Associate Products to this artist";
		?>
	</div>
	<div class="success">
	<?php if ( isset( $this->strSuccess ) ) echo $this->strSuccess; ?>
</div>

<div class="error">
	<h2><?php if ( isset( $this->strError ) ) echo $this->strError;	?></h2>
</div>

	<?php
	if( $this->getProductsInArtist ){
		echo '<a href="?cmd=listProducts2Add&artist_id=' . $this->artist->getArtistId() . '"><img src="img/wand.png" border="0" hspace="2" align="absmiddle">Add New Product</a> <br>';
	}
	$listPanel->display();
	?>
</div>
<?php
include_once ( "footer.php" );
?>