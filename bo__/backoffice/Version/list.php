<?php

$users = $this->options->getResults();

$viewConfig = $this->viewConfig;



$product_id = $this->product_id;



//params of the variable viewConfig

$paramsUsed = "name,title,id";

$paramsArrUsed = "columNamesOverride,hiddenColums,hiddenFields";

$paramsUsed = explode(",",$paramsUsed);

foreach( $paramsUsed as $param){

	$viewConfig[ $param ] = empty( $viewConfig[ $param ] ) ? '' : $viewConfig[ $param ];

}



$paramsArrUsed = explode(",",$paramsArrUsed);



foreach( $paramsArrUsed as $param){

	$viewConfig[ $param ] = empty( $viewConfig[ $param ] ) ? array() : $viewConfig[ $param ];

}





$data = array();

foreach($users as $user){

	$data[] = get_object_vars( $user );

}

		



$listPanel = new GUI_ListPanel_ListPanel( $viewConfig['name'] );

$listPanel->setData( $data );



function renameProduct($product_id){

	$product = eCommerce_SDO_Core_Application_Product::LoadById($product_id)->getName();

	return $product;

}

function renameBrand($brand_id){

	$brand = eCommerce_SDO_Core_Application_Brand::LoadById($brand_id)->getName();

	return $brand;

}

function showImage($image) {

	$image = explode(',', $image);

	

	if(!empty($image[0])) {

		$file = eCommerce_SDO_Core_Application_FileManagement::LoadById($image[0]);

		$imgSrc = '../file.php?id='.$image[0].'&type=image';

		return  '<a class="thickbox" href="'.$imgSrc.'" title="'.$file->getDescription().'">'.

				'<img src="../file.php?id=' . $image[0] . '&type=image&img_size=predefined&width=50&height=50" border="0" />'.

				'</a>';

	}

	return '';

}

$listPanel->addCallBack("product_id","renameProduct");

$listPanel->addCallBack("brand_id","renameBrand");

$listPanel->addCallBack('imagen', 'showImage');



foreach( $viewConfig['columNamesOverride'] as $colum => $rename){

	$listPanel->addColumnNameOverride($colum,$rename);

}





foreach( $viewConfig['hiddenColums'] as $col ){

	$listPanel->addHiddenColumn( $col );

}



foreach( $viewConfig['hiddenFields'] as $name=> $value ){

	$listPanel->addHiddenField($name, $value );

}

$tipo = ($this->tipo == 'producto')?'productos':'paquetes';

$tipo2 = 'producto';

// Add "Order By" capabilities

$listPanel = new GUI_ListPanel_Decorator_Sorter( $listPanel );

$listPanel->setOrderBy( $this->options->getOrderBy() );





// Add actions

$listPanel = new GUI_ListPanel_Decorator_RowActions( $listPanel );

$listPanel->setHeaderLabel( $this->trans('actions') );



// Tallas

$actModify = new GUI_ListPanel_Action( '', 'img/size.jpg' );

$actModify->setImageTitle( 'Administrar Atributos' );

$actModify->setOnClickEvent('Shadowbox.open({ content: "Relaciontalla.php?table_id={$version_id}&tipo='.$tipo2.'", width: 640, height: 640, player:"iframe", title:"Administrar Atributos" }); return false;');

$listPanel->addAction( $actModify );



// Colores

/*$actModify = new GUI_ListPanel_Action( '', 'img/color.png', '_blank' ,'','','');

$actModify->setImageTitle( 'Administrar Caracteristicas' );

$actModify->setOnClickEvent('Shadowbox.open({ content: "Relacioncolor.php?table_id={$version_id}&tipo='.$tipo2.'", width: 640, height: 640, player:"iframe" , title:"Administrar Caracteristicas"}); return false;');

$listPanel->addAction( $actModify );*/



// Modify

$actModify = new GUI_ListPanel_Action( '?cmd=edit&id=' . $viewConfig['id'] . '&product_id='.$product_id.'&tipo='.$tipo, 'img/write.png' );

$actModify->setImageTitle( $this->trans('edit').' '.$viewConfig['name'] );

$listPanel->addAction( $actModify );



// Delete

$actDelete = new GUI_ListPanel_Action( '?cmd=delete&id=' . $viewConfig['id'] . '&product_id='.$product_id.'&tipo='.$tipo, 'img/delete.png' );

$actDelete->setImageTitle( $this->trans('delete').' '.$viewConfig['name'] );

$actDelete->setOnClickEvent( 'return confirm("'.$this->trans('?_confirm_delete').' '.strtolower($this->trans('the')).' '.$viewConfig['name'].' \"'.$viewConfig['id'].'\"?");' );

$listPanel->addAction( $actDelete );



// Add "Search" capabilities

$listPanel = new GUI_ListPanel_Decorator_Searcher( $listPanel );

$listPanel->setFieldLabel( '<img src="img/search2_small.png" style="vertical-align:middle">&nbsp;' );

$listPanel->setButtonLabel($this->trans('search'));

$listPanel->setClearButtonLabel($this->trans('clear'));

$listPanel->setSearchString( $this->options->getKeywords() );



// Add paging capabilities

$listPanel = new GUI_ListPanel_Decorator_Pager( $listPanel );

$listPanel->setCurrentPage( $this->options->getPage() );

$listPanel->setPagingLabel($this->trans('page'));

$listPanel->setPreviousLabel('&#9668; '.$this->trans('previous'));

$listPanel->setNextLabel($this->trans('next').' &#9658;');

$listPanel->setFirstPageLabel($this->trans('first(f)').' '.$this->trans('page'));

$listPanel->setLastPageLabel($this->trans('last(f)').' '.$this->trans('page'));

$listPanel->setOffsetLabel($this->trans('results_per').' p&aacute;gina');



//echo $this->totalPages;

$listPanel->setTotalPages( $this->options->getTotalPages() ); //TODO:



//$listPanel->setOffsetIncrement( $this->options->getResultsPerPage() );

$listPanel->setOffset( $this->options->getResultsPerPage() );



include_once ( "header.php" );

$regresar = ($this->tipo == 'producto')?'product.php':'paquetes.php';

?>

<!--<link rel="stylesheet" type="text/css" href="js/shadow/files/style.css">-->

<link rel="stylesheet" type="text/css" href="js/shadow/shadowbox.css">

<script type="text/javascript" src="js/shadow/shadowbox.js"></script>

<script type="text/javascript">

Shadowbox.init({

    players:    ["img","iframe","html"]

});

</script>

<div class="container" style="width: 100%; text-align: left;">

<div class="subtitle"><?php echo $viewConfig['title']; ?></div>

<div class="success">

	<?php if ( isset( $this->strSuccess ) ) echo $this->strSuccess; ?>

</div>

<div class="error">

	<h2><?php if ( isset( $this->strError ) ) echo $this->strError;	?></h2>

</div>



<a href="?cmd=add&product_id=<?=$_REQUEST['product_id']?>&tipo=<?=$tipo?>"><img src="img/wand.png" border="0" hspace="2" align="absmiddle"><?php echo $this->trans('add').' '.$viewConfig['name']; ?></a> <br>

<a href="<?=ABS_HTTP_URL?>bo/backoffice/<?=$regresar?>" style="float:right;"><img src="img/rewind.png" border="0" hspace="2" align="absmiddle">Regresar a Productos</a> <br>

<br>



  <?php

		$listPanel->setTableWidth( '100%' );

		$listPanel->display();

		?>

</div>

<?php

include_once ( "footer.php" );

?>