<?php 
include_once "../../common.php";
$padre_id = $_REQUEST['padre_id'];
$product_id = $_REQUEST['product_id'];
$categoriaPadre = eCommerce_SDO_Catalog::LoadCategory($padre_id);
$subCategorias = eCommerce_SDO_Catalog::GetCategoriesByParentCategory($padre_id)->getResults();
$html = '<p><b>SUBCATEGORIAS - Categoria Padre '.$categoriaPadre->getName().'</b></p>';
$Categorias = eCommerce_SDO_Core_Application_ProductCategory::GetCategoriesByProduct($product_id );

foreach($Categorias as $category){
	$categoriasActuales[] = $category->getCategoryId();
}
$existe_padre = (in_array($padre_id,$categoriasActuales))?'checked="checked"':'';	

$cont = 1;
foreach($subCategorias as $sub){
	$categoria_id = $sub->getCategoryId();
	$existe = (in_array($categoria_id,$categoriasActuales))?'checked="checked"':'';
	$estilo = (in_array($categoria_id,$categoriasActuales))?'color:red':'';
	$name = $sub->getName();
//	$html.= '<input name="chkSub[]" type="checkbox" value="'.$categoria_id.'" id="'.$categoria_id.'" '.$existe.'>&nbsp;'.$name.'&nbsp;';	
	$html.= '<div style="width:300px; float:left"><div style="width:20px; float:left"><input name="chkSub[]" type="checkbox" value="'.$categoria_id.'" id="'.$categoria_id.'" '.$existe.'></div><div style="width:280px;'.$estilo.'">&nbsp;'.$name.'&nbsp;</div></div>';	
	if($cont%5 == 0){	
		$html.= '<div style="clear:both"></div>';
	}
	$cont++;
}
$html.= '<div style="clear:both"></div><div align="right"><b>Relacionar el producto a la categoria padre </b>?&nbsp;<input name="padresId[]" type="checkbox" value="'.$padre_id.'" id="padreId_'.$padre_id.'" '.$existe_padre.'></div>';
echo $html;
?>