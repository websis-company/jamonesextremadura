<?php
include_once ( "header.php" );

$form = $this->form;
$viewConfig = $this->viewConfig;
$errors = $this->errors;

$ArrayImages = $this->ArrayImages;

$cancelParams = empty($this->cancelParams) ? '' : $this->cancelParams;
$cancelConfirm= empty($this->cancelConfirm) ? '' : $this->cancelConfirm;
?>

  <!-- calendar stylesheet -->
  <link rel="stylesheet" type="text/css" media="all" href="calendar/css/jscal2.css" title="jscal2" />
  <!-- main calendar program -->
  <script type="text/javascript" src="calendar/js/jscal2.js"></script>
  <!-- language for the calendar -->
  <script type="text/javascript" src="calendar/js/lang/<?=eCommerce_SDO_LanguageManager::LanguageToZendLang()?>.js"></script>
  
<h2 align="left" style="margin:0px;"><? echo $viewConfig['title']; ?></h2>

<form name="frmUser" id="frmUser" method="post" action="" enctype="multipart/form-data">

  <div class="error">
		<?php echo $errors->getDescription(); ?>
  </div><br />
  <?php $errors->getHtmlError("generalError"); ?>
	

		<?php
			eCommerce_SDO_Core_Application_Form::displayForm( $form, $errors,'frmUser' );
		
		?>

<p align='left'>
	<input type="hidden" name="cmd" value="save">
	
	<input type="submit" value="<?=$this->trans('save')?>" class="frmButton"> <input type="button" value="<?=$this->trans('cancel')?>" class="frmButton" onClick="<?php echo $cancelConfirm; ?>document.location.href='cupondescuento.php<?php echo $cancelParams; ?>'">
</p>
</form>

<?php
include_once ( "footer.php" );
?>

<script type="text/javascript">
	function cleanString(cadena){
		cadena = cadena.replace(/á/gi,"a");
		cadena = cadena.replace(/é/gi,"e");
		cadena = cadena.replace(/í/gi,"i");
		cadena = cadena.replace(/ó/gi,"o");
		cadena = cadena.replace(/ú/gi,"u");
		cadena = cadena.replace(/ñ/gi,"n");

		$("#cupon").val(cadena);
	}
</script>