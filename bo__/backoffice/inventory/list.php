<?php
include_once ( "header_iframe.php" );

$users = $this->options->getResults();

$viewConfig = $this->viewConfig;

//params of the variable viewConfig
$paramsUsed = "name,title,id";
$paramsArrUsed = "columNamesOverride,hiddenColums";

$paramsUsed = explode(",",$paramsUsed);

foreach( $paramsUsed as $param){
	$viewConfig[ $param ] = empty( $viewConfig[ $param ] ) ? '' : $viewConfig[ $param ];
}


$paramsArrUsed = explode(",",$paramsArrUsed);

foreach( $paramsArrUsed as $param){
	$viewConfig[ $param ] = empty( $viewConfig[ $param ] ) ? array() : $viewConfig[ $param ];
}


$data = array();
foreach($users as $user){
	$data[] = get_object_vars( $user );
}
		

$listPanel = new GUI_ListPanel_ListPanel( $viewConfig['name'] );
$listPanel->setData( $data );

foreach( $viewConfig['columNamesOverride'] as $colum => $rename){
	$listPanel->addColumnNameOverride($colum,$rename);
}


foreach( $viewConfig['hiddenColums'] as $col ){
	$listPanel->addHiddenColumn( $col );
}

foreach( $viewConfig['hiddenFields'] as $key => $col ){
	$listPanel->addHiddenField( $key, $col );
}


// Add "Order By" capabilities
$listPanel = new GUI_ListPanel_Decorator_Sorter( $listPanel );
$listPanel->setOrderBy( $this->options->getOrderBy() );


// Add actions
$listPanel = new GUI_ListPanel_Decorator_RowActions( $listPanel );
$listPanel->setHeaderLabel( $this->trans('actions') );

// Modify
$actModify = new GUI_ListPanel_Action( '?cmd=edit&id={$' . $viewConfig['id'] . '}', 'img/write.png' );
$actModify->setImageTitle( $this->trans('edit').' '. $viewConfig['name'] );
$listPanel->addAction( $actModify );

// Delete
$actDelete = new GUI_ListPanel_Action( '?cmd=delete&id={$' . $viewConfig['id'] . '}', 'img/delete.png' );
$actDelete->setImageTitle( $this->trans('delete').' '. $viewConfig['name'] );
$actDelete->setOnClickEvent( 'return confirm("'.$this->trans('?_confirm_delete').' '.strtolower($this->trans('the')).' '.strtolower($this->trans('attribute')).' ID {$'.$viewConfig['id'].'}?");' );
$listPanel->addAction( $actDelete );

// Add "Search" capabilities
$listPanel = new GUI_ListPanel_Decorator_Searcher( $listPanel );
$listPanel->setFieldLabel( '<img src="img/search2_small.png" style="vertical-align:middle">&nbsp;' );
$listPanel->setButtonLabel($this->trans('search'));
$listPanel->setClearButtonLabel($this->trans('clear'));
$listPanel->setSearchString( $this->options->getKeywords() );

// Add paging capabilities
$listPanel = new GUI_ListPanel_Decorator_Pager( $listPanel );
$listPanel->setCurrentPage( $this->options->getPage() );
$listPanel->setPagingLabel($this->trans('page'));
$listPanel->setPreviousLabel('&#9668; '.$this->trans('previous'));
$listPanel->setNextLabel($this->trans('next').' &#9658;');
$listPanel->setFirstPageLabel($this->trans('first(f)').' '.$this->trans('page'));
$listPanel->setLastPageLabel($this->trans('last(f)').' '.$this->trans('page'));
$listPanel->setOffsetLabel($this->trans('results_per').' '.strtolower($this->trans('page')));
$listPanel->setTotalPages( $this->options->getTotalPages() ); //TODO:
$listPanel->setOffset( $this->options->getResultsPerPage() );

//include_once ( "header.php" );
?>

<div class="container" style="width: 100%; text-align: left;">
<div class="success">
	<?php if ( isset( $this->strSuccess ) ) echo $this->strSuccess; ?>
</div>
<div class="error">
	<h2><?php if ( isset( $this->strError ) ) echo $this->strError;	?></h2>
</div>

<a href="?cmd=add&idProduct=<?=$this->idProduct?>"><img src="img/wand.png" border="0" hspace="2" style="vertical-align:middle"><?php echo $this->trans('add').' '.$viewConfig['name']; ?></a> <br>
<br>
  <?php
		$listPanel->setTableWidth( '500' );
		$listPanel->display();
		?>
		
</div>
<?php
include_once ( "footer_iframe.php" );
?>