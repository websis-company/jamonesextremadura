<?php
abstract class eCommerce_FrontEnd_FO extends Application {

	/**
	 * Template Engine System provided by PEAR::Savant3
	 *
	 * @var Savant3
	 */
	protected $tpl;

	public function __construct(){
		parent::__construct();
		//$this->setPermissionStrategy( new eCommerce_Security_FO_PermissionStrategy( $this, '' ) );
//		$this->tpl = new Savant3();
		
		$lang = $this->getParameter("lang",false, null);
		
		if( !empty($lang) && $lang != eCommerce_SDO_LanguageManager::GetActualLanguage() ){
			eCommerce_SDO_LanguageManager::SetActualLanguage( $lang );
			if($lang =='EN'){
				eCommerce_SDO_CurrencyManager::SetActualCurrency("USD");
			}else{
				eCommerce_SDO_CurrencyManager::SetActualCurrency("MXN");
			}
		}
		$this->tpl = new eCommerce_Savant();
	}

	public function checkPermission(){
		$cmd = $this->getCommand();
		if ( !$this->hasPermission( $cmd ) ){
			echo '<script language="javascript">'
			   . 'alert("You need to be logged in to proceed");' . "\n"
			   . 'window.location.href="index.php";' . "\n"
			   . '</script>';
			die();
		}
	}
}
?>