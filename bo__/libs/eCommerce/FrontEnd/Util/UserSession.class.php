<?php
class eCommerce_FrontEnd_Util_UserSession {
	
	const NAMESPACES = 'user_session';
	
	public static function HasIdentity(){
		$tmp = eCommerce_FrontEnd_Util_Session::Get( self::NAMESPACES );
		if ( !empty( $tmp ) && $tmp instanceof eCommerce_Entity_User_Profile ){
		     	return true; 
		}
		else {
			return false;
		}
	}
	
	/**
	 * @return eCommerce_Entity_User_Profile
	 */
	public static function GetIdentity(){
		if ( self::HasIdentity() ){
			return eCommerce_FrontEnd_Util_Session::Get( self::NAMESPACES );
		}
		else {
			return null;
		}
	}
	
	public static function DestroyIdentity(){
		if ( self::HasIdentity() ){
			eCommerce_FrontEnd_Util_Session::Set( self::NAMESPACES, null );
		}
	}
	
	/**
	 * @param eCommerce_Entity_User_Profile $profile
	 */
	public static function SetIdentity( eCommerce_Entity_User_Profile $profile ){
		eCommerce_FrontEnd_Util_Session::Set( self::NAMESPACES, $profile );
	}
	
}
?>