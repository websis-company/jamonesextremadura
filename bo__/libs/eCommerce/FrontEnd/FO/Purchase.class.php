<?php
class eCommerce_FrontEnd_FO_Purchase extends eCommerce_FrontEnd_FO {
	const ADMITED_ROLES = 'Customer,Admin,Anonymous';
	/**
	 * @var eCommerce_DAO_Event
	 */
	public function __construct(){
		parent::__construct();
	}

	public function execute(){
		$cmdLogin   = $this->getParameter('cmdLogin',false,NULL);
		$cmd        = empty($cmdLogin)?$this->getCommand():$cmdLogin;		
		$actualStep = $this->getParameter( 'actualStep' );
		$nextStep   = $this->getParameter( 'nextStep' );
		//$nextStep = ($nextStep > 0) ? $nextStep : $actualStep;
		$actualStep = ($actualStep > 0) ? $actualStep : $nextStep;
		$saveStep = false;
		switch( $cmd ){
			case 'recoverPassword': 
				$this->_recoverpwd($actualStep, $nextStep, $saveStep);
				break;
			case 'loginForm': 
					$userProfile = eCommerce_FrontEnd_Util_UserSession::GetIdentity();
					$this->login( $userProfile->getEmail(), $_POST["passwordLogin"], $actualStep, $nextStep, $saveStep);
				break;
			case 'setPassword':
				$this->_setPassword($actualStep, $nextStep, $saveStep);
				break;
			case 'saveStep': 
				$saveStep = true;
			default:
				$this->_processStep( $actualStep, $nextStep, $saveStep );
			break;
		}
	}

	public function _setPassword($step, $nextStepToShow = 1, $saveStep = false){
		$password = $this->getParameter('password',false,NULL);
		$passwordconfirm = $this->getParameter('passwordconfirm',false,NULL);
		if($password !== $passwordconfirm){
			unset($_SESSION['Auth']);
			$msgResponse = '<div class="alert alert-danger" role="alert">Las contrase&ntilde;as no coinciden</div>';
		}elseif($password != '' && $passwordconfirm != ''){
			$userProfile = eCommerce_FrontEnd_Util_UserSession::GetIdentity();
			$userEmail = $userProfile->getEmail();
			$existeUser = eCommerce_SDO_User::LoadUserProfileByEmail($userEmail);
			$existeProfileId = $existeUser->getProfileId();		
			if(!empty($existeProfileId)){
				$existeUser->setPassword(md5($password));
				$existeUser->setConfirmPassword(md5($passwordconfirm));
				$existeUser->setEmail( $userEmail );
				$existeUser->setRole( eCommerce_SDO_User::CUSTOMER_ROLE );
				//$existeUser->setConfirmado('Si');
				$userProfile = eCommerce_SDO_User::SaverUserProfile($existeUser,false);
				$userProfile->setEmail($userEmail);
				$userProfile->setPassword(md5($password));
				$userProfile->setRole(eCommerce_SDO_User::CUSTOMER_ROLE);
				$userProfile->setConfirmado('Si');		
				$id = eCommerce_SDO_Security::Authenticate( $userEmail, md5($password));
				//$userProfile 	= eCommerce_SDO_User::LoadUserProfile( $id );
				eCommerce_FrontEnd_Util_Session::Set('find_user', 'true');
				eCommerce_FrontEnd_Util_UserSession::SetIdentity( $userProfile );
				//$_SESSION['Auth'] = "true";
				#-- Envia correo con datos de la contraseña
				$send = $this->sendNewPassword($userProfile);
				$msgResponse = '<div class="alert alert-success" role="alert" style="font-family: Arial; font-size: 13px;">La contrase&ntilde;a fue actualizada, para activarla porfavor siga el enlace que se le envio a su cuenta de correo electr&oacute;nico :'.$userProfile->getEmail().'</div>';
			}
		}else{
			$msgResponse = '<div class="alert alert-danger" role="alert">Las contrase&ntilde;as no coinciden</div>';
			unset($_SESSION['Auth']);
		}
		$this->_processStep( $step, $nextStepToShow, $saveStep, $msgResponse );
	}

	public function codificaId($id){
		$alNumoLet = 1; 
		$verok     = ""; 
        for ($i=0;$i<=5;$i++){
            if ($alNumoLet==1)
                $verok.=chr(rand(ord("="), ord("Z"))); 
            else    
                $verok.= rand(0,9);
            $alNumoLet=rand(0,1);
        }
        $aleatorio=rand(1,9);     
        $idUsrB64=$id*$aleatorio; 
        $verok.=base64_encode($idUsrB64).base64_encode(chr($aleatorio+60)); 
        return $verok;
    }

	public function sendNewPassword($userProfile){
		$config = Config::getGlobalConfiguration();
		$idPerfil =  $this->codificaId($userProfile->getProfileId());
		$html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
				 <html xmlns="http://www.w3.org/1999/xhtml">
				 	<head>
				 		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
				 		<title>'.$config['project_name'].' :: Verificaci&oacute;n de Perfil</title>
				 	</head>
				 	<body>
				 		<table width="750" border="0" cellspacing="0" cellpadding="0" style="width: 750px; font-family:Arial,Helvetica,sans-serif; color: #000;">
				 			<tr>
				 				<td colspan="2">
				 					<img src="'.ABS_HTTP_URL.'images/layout/logo.png" width="auto" height="auto"/>
				 				</td>
				 			</tr>
				 			<tr>
				 				<td width="500">&nbsp;</td>
				 				<td width="250" align="right" style="font-weight: bold; padding: 25px;">Hola! '.$userProfile->getFirstName().' '.$userProfile->getLastName().'</td>
				 			<tr>
				 			<tr>
				 				<td colspan="2" style="font-weight: normal; padding: 25px;">
				 					Tu contrase&ntilde;a ha sido actualizada correctamente, para activar tu cuenta solo tienes que hacer clic en el enlace que se encuentra abajo.
				 					<br/><br/>
				 					Enlace:<br/>
				 						<a href="'.ABS_HTTP_URL.'register.php?a=act&i='.$idPerfil.'" target="_blank">'.ABS_HTTP_URL.'/register.php?a=act&i='.$idPerfil.'</a>
				 					<br/><br/>
				 					Atte: '.$config['project_name'].'
				 				</td>
				 			</tr>
				 			<tr><td style="background-color:#00BEFF; color: #FFF;" colspan="2">&nbsp;</td></tr>
				 		</table>
				 	</body>
				 </html>';
		$header =  'MIME-Version: 1.0' . "\r\n";
		$header .= "Content-type: text/html; charset=iso-8859-1\r\n";
		$header .= "From: ".$config['project_name']." <".$config['project_email'].">\r\n";
		$header .= 'X-Sender:'.$config['project_name'].'<'.$config['project_email'].'>'."\r\n"; 
		$header .= 'X-Mailer: PHP/'.phpversion()."\r\n";
		$header .= 'X-Priority: 1 (Higuest)'."\r\n";
		$body    = utf8_decode($html);
		$Subject = utf8_decode('Activar cuenta');
		@mail(utf8_decode($userProfile->getFirstName()).' '.utf8_decode($userProfile->getLastName()).' <'.$userProfile->getEmail().'>', $Subject, $body, $header);	
	}

	public function login( $user, $password,  $step, $nextStepToShow = 1, $saveStep = false){
		$existeUser      = eCommerce_SDO_User::LoadUserProfileByEmail($user);
		$existeProfileId = $existeUser->getProfileId();	
		$confirmado      = $existeUser->getConfirmado();
		if(!empty($existeProfileId) && $confirmado == "No"){
			$msgResponse = '<div class="alert alert-danger" role="alert">La cuenta aún no se encuentra activada, para activarla porfavor siga el enlace que se le envio a su cuenta de correo electr&oacute;nico : <span style="font-family: Arial; font-size: 15px;">'.$user.'</span> </div>';
			$this->_processStep( $step, $nextStepToShow, $saveStep, $msgResponse );
			die();
		}
		eCommerce_FrontEnd_Util_Session::Set('login_user', $user);			
		$userProfile = new eCommerce_Entity_User_Profile();
		$url_login = ABS_HTTP_URL."login.php?errorCode=" . eCommerce_FrontEnd_BO_Authentification::ERR_RESTRICTED_ACCESS.'&username='.$user;		
		$id = eCommerce_SDO_Security::Authenticate( $user, md5($password));
		if($id != false){
			$userProfile 	= eCommerce_SDO_User::LoadUserProfile( $id );
			eCommerce_FrontEnd_Util_Session::Set('find_user', 'true');
		}else{			
			unset($_SESSION['Auth']);
			$msgResponse = '<div class="alert alert-danger" role="alert">La contrase&ntilde;a es incorrecta</div>';
		}
		$admitedRoles = preg_split("/,/", self::ADMITED_ROLES);
		if( in_array( $userProfile->getRole(), $admitedRoles) ){
			eCommerce_FrontEnd_Util_Session::Delete('login_user');
			eCommerce_FrontEnd_Util_UserSession::SetIdentity( $userProfile );
			$_SESSION['Auth'] = 'true';
			$msgResponse = '<div class="alert alert-success" role="alert">Bienvenido '.$userProfile->getFirstName()." ".$userProfile->getLastName().'</div>';			
			//$this->goToFirstPage();			
		}
		else{
			/*$this->redirect(ABS_HTTP_URL."login.php?errorCode=" . eCommerce_FrontEnd_BO_Authentification::ERR_RESTRICTED_ACCESS,false);*/
			$url_login = ABS_HTTP_URL."login.php?errorCode=" . eCommerce_FrontEnd_BO_Authentification::ERR_RESTRICTED_ACCESS.'&username='.$user;
			//$this->redirect($url_login,false);	
			unset($_SESSION['Auth']);
		}
		$this->_processStep( $step, $nextStepToShow, $saveStep, $msgResponse );
	}

	protected function _recoverpwd($step, $nextStepToShow = 1, $saveStep = false){	
		$emailTmp = $this->getParameter("email",false,'');
		try{			
			//$userProfile = eCommerce_SDO_User::LoadUserProfileByEmail($email);
			$userProfile = eCommerce_FrontEnd_Util_UserSession::GetIdentity();			
			$email       = $userProfile->getEmail();
			$profileId   = $userProfile->getProfileId();
			$userRole    = $userProfile->getRole();
			$userProfileDB 	= eCommerce_SDO_User::LoadUserProfile( $profileId );
			$confirmadoDB = $userProfileDB->getConfirmado();
			if( !empty($profileId) && !empty($email) && $userRole != eCommerce_SDO_User::ANONYMOUS_ROLE && $emailTmp == $email && $confirmadoDB == 'Si') {
				$psw          = eCommerce_SDO_Core_Application_Profile::PasswordReset($profileId);
				$main_message = "<strong style='color: #000;'>".$this->trans('your').' '.$this->trans('password').' '.$this->trans('is').': ' . $psw."</strong>";
				$styles = array(
						"containerWidth" => "775px;",
						"fontSize1" => "12px",
						"fontColor1" => "#666",
						"fontColor2" => "#666",
						"headerImage" => array(
							//"src" => Config::getGlobalValue('logo_project')
							"src" => ABS_HTTP_URL.'images/layout/logo.png',
						),
						"divisionLineColor" => "#00BEFF"
					);
				Util_Email::sendHTMLMail(false, true, $userProfile, $this->trans('password_recovery'), $main_message, $styles);
				$msgResponse = '<div class="alert alert-success" role="alert">Hemos enviado su nueva contrase&ntilde;a al correo : '.$email.'</div>';
				$msg = 'Hemos enviado su nueva contrase\u00f1a al correo '.$email;
				$msgClass = 'alert-success';
				}else{
					if($confirmadoDB != 'Si'){
						$msgResponse =  '<div class="alert alert-danger" role="alert">Tu cuenta no ha sido confirmada, revisa tu correo electr&oacute;nico para activarla';
					}
					else{
						$msgResponse = '<div class="alert alert-danger" role="alert">'.(empty($email ) ? 'El correo '.$email.' no se encuentra en nuestro sistema.' : (empty($profileId) ? 'El correo '.$email.' no se encuentra en nuestro sistema.' : 'La contrase&ntilde;a aun no ha sido registrada')).'</div>';
						$msg = empty($email ) ? 'El correo '.$email.' no se encuentra en nuestro sistema.' : (empty($profileId) ? 'El correo '.$email.' no se encuentra en nuestro sistema.' : 'La contrase\u00f1a aun no ha sido registrada');
					}
					$msgClass = 'alert-error';
				}
			}
			catch(eCommerce_SDO_Core_Application_Security_Exception $e){
				$msg = 'No pudo enviarse su nueva contrase\u00f1a, por favor intente mas tarde';
				$msgClass = 'alert-error';
			}
		$this->_processStep( $step, $nextStepToShow, $saveStep, $msgResponse );
	}

	protected function _processStep( $step, $nextStepToShow = 1, $saveStep = false, $msgResponse = NULL  ){
		$cart        = eCommerce_SDO_Cart::GetCart();
		$isEmptyCart = eCommerce_SDO_Cart::IsEmptyCart();

		$userProfile 	= eCommerce_FrontEnd_Util_UserSession::GetIdentity();
		$userProfile->getRole();
		if($userProfile->getRole() == "Admin"){
			eCommerce_FrontEnd_Util_UserSession::DestroyIdentity();
			unset($_SESSION['Auth']);
			$this->redirect(ABS_HTTP_URL.'login.php', false);
			die();
		}

		/*Carrito vacio*/
		if($isEmptyCart){			
			$this->redirect('cart.php', false);
		}
		

		$this->tpl->assign('cart', $cart );
		$this->tpl->assign('isEmptyCart', $isEmptyCart );
		$total	= eCommerce_SDO_Cart::GetTotalCart(true, true);
		$this->tpl->assign('monto_total', $total['price']['format']);
		$this->tpl->assign('msgResponse',$msgResponse);	


		//process the step	
		switch( $step ){
			case 1:
			default:
				/*$result = $this->_setAddress( eCommerce_SDO_Cart::ADDRESS_BILL_TO, $saveStep );
				$nextStepToShow  = ( $result ) ? $nextStepToShow : 1; */
				$saveStep = ($step == 0)?false:true;
				$cart = eCommerce_SDO_Cart::GetCart();				
				$cart->setRequiereFactura($this->getParameter('requiere_fac',false,$cart->getRequiereFactura()));				
				eCommerce_SDO_Core_Application_Cart::SaveCart($cart);				
				$result = $this->_setAddress( eCommerce_SDO_Cart::ADDRESS_SHIP_TO, $saveStep );
				$nextStepToShow  = ( $result ) ? $nextStepToShow : 1;
				$result = $this->_setAddress( eCommerce_SDO_Cart::ADDRESS_BILL_TO, $cart->getRequiereFactura() == 'No' ? false : $saveStep );				
				$nextStepToShow  = $cart->getRequiereFactura() == 'No' ? $nextStepToShow : (( $result ) ? $nextStepToShow : 1);
				///////$nextStepToShow
				//$nextStepToShow = 4;
				$comments = $this->getParameter('comentarios',false,NULL);
				if($comments)eCommerce_FrontEnd_Util_Session::Set('OrderComment', $comments );
					$this->tpl->assign('comments',eCommerce_FrontEnd_Util_Session::Get('OrderComment'));
			break;		
				/*$result = $this->_setAddress( eCommerce_SDO_Cart::ADDRESS_SHIP_TO, $saveStep );
				$nextStepToShow  = ( $result ) ? $nextStepToShow : 2;*/
				try{ 
					$ship = $cart->getOrderAddressByType( eCommerce_SDO_Cart::ADDRESS_SHIP_TO );
					$bill = $cart->getOrderAddressByType( eCommerce_SDO_Cart::ADDRESS_BILL_TO );
					if(empty($ship->email) || empty($ship->type)){
						throw new Exception ('Necesita llenar los datos de env&iacute;o');
					}
					$comments = $this->getParameter('comentarios',false,NULL);
					if($comments)eCommerce_FrontEnd_Util_Session::Set('OrderComment', $comments );
					$config = Config::getGlobalConfiguration();
					$total	= eCommerce_SDO_Cart::GetTotalCart(true,true);
					//$this->tpl->assign('deposito', $config['deposito']);
					//$this->tpl->assign('transferencia', $config['transferencia']);					
					$this->tpl->assign('monto_total', $total['price']['format']);
					$this->tpl->assign('ship', $ship);
					$this->tpl->assign('bill', $bill);
					$this->tpl->assign('comments',eCommerce_FrontEnd_Util_Session::Get('OrderComment'));
				}catch( Exception $e){
					$this->tpl->assign( 'errors', new Validator_ErrorHandler($e->getMessage() ) );
					$this->tpl->assign( 'messageClass', 'msjErr');
					$result = $this->_setAddress( eCommerce_SDO_Cart::ADDRESS_SHIP_TO, $saveStep );
					$result = $this->_setAddress( eCommerce_SDO_Cart::ADDRESS_BILL_TO, $cart->getRequiereFactura() == 'No' ? false : $saveStep );
					$nextStepToShow = 1;
				}
			break;
			case 3.5: //EXCLUSIVO BANAMEX
				$paymentStatus	= $this->getParameter('status',false,NULL);
				$order_id		= $this->getParameter('order_id');
				$plan		= eCommerce_FrontEnd_Util_Session::Get('plan_id');
				switch ($plan){
					case 'BPWOI1':	$plan = 12;break;
					case 'BPWOI2':	$plan = 9;break;
					case 'BPWOI3':	$plan = 6;break;
					case 'BPWOI4':	$plan = 3;break;
					default:		$plan = 0;break;
				}
				eCommerce_FrontEnd_Util_Session::Delete('plan_id');
				$tipo_tarjeta = Application::getParameter('tipo_tarjeta',false,NULL);
				$plan = !empty($plan) ? "Plan a $plan meses sin intereses" : 'Pago con cargo regular';
				$test=eCommerce_FrontEnd_Util_Session::Get('debug');
				if(($paymentStatus == 'aprobada' || $test) && $order_id){
					$order = eCommerce_SDO_Order::LoadById($order_id);
					$order->setModificationDate(date('Y-m-d H:i:s'));
					$order->setStatus( eCommerce_SDO_Core_Application_Order::GetValidStatus('C') );
					$paymentMethod	= $this->getParameter('tipo_pago',false,NULL);
					$order->setFolio(Application::getParameter('no_aut',false,NULL));
					//$order->setTipoCargo($plan);
					$order->setTipoTarjeta($tipo_tarjeta);
					eCommerce_SDO_Order::SaveOrder($order);
					$userProfile 	= eCommerce_FrontEnd_Util_UserSession::GetIdentity();
					$nextStepToShow = 5;
					$orderFull 		= eCommerce_SDO_Order::LoadFullById($order_id);
					//if(!continuar() || true)eCommerce_SDO_Cart::DestroyActualCart();
					if( true)eCommerce_SDO_Cart::DestroyActualCart();
				}elseif($paymentStatus === 'tarjeta expirada'){
					$nextStepToShow = 4;
					$orderFull 		= eCommerce_SDO_Order::LoadFullById($order_id);
					//$orderFull->setTipoCargo($plan);
					$orderFull->setTipoTarjeta($tipo_tarjeta);
					$this->tpl->assign("mensaje", "Tarjeta Expirada");
					if(!continuar() || true)eCommerce_SDO_Cart::DestroyActualCart();
				}
				elseif($paymentStatus === 'Transaccion declinada'){
					$this->tpl->assign("mensaje", "Transacciï¿½n declinada");
				}
				else{
					//debug($_REQUEST);
					//debug("Transaccion fallida");die;
				}
				break;
			case 4:
				/*$userProfile = eCommerce_FrontEnd_Util_UserSession::GetIdentity();
				$comments = $this->getParameter('comments', false,'');
				$paymentMethod = $this->getParameter('paymentMethod', false);
				$orderFull = eCommerce_SDO_Cart::CreateOrderFromCart( $userProfile->getProfileId(), $comments, $paymentMethod );
				//eCommerce_SDO_Cart::DestroyActualCart();*/	
				$ship 			= eCommerce_SDO_Cart::GetCart()->getOrderAddressByType(eCommerce_SDO_Cart::ADDRESS_SHIP_TO);							
				$bill 			= eCommerce_SDO_Cart::GetCart()->getOrderAddressByType(eCommerce_SDO_Cart::ADDRESS_BILL_TO);					
				$userProfile = eCommerce_FrontEnd_Util_UserSession::GetIdentity();					
					if($userProfile->getRole() == eCommerce_SDO_User::ANONYMOUS_ROLE){
							$userProfile = eCommerce_SDO_User::LoadUserProfileByEmail($ship->getEmail());							
							if($userProfile->getEmail() != $ship->getEmail()){
								$userProfile->setFirstName( $ship->getFirstName() );
								$userProfile->setLastName( $ship->getLastName() );
								$userProfile->setEmail( $ship->getEmail() );
								$userProfile->setRole(eCommerce_SDO_User::ANONYMOUS_ROLE);
								$userProfile = eCommerce_SDO_User::SaverUserProfile($userProfile);
							}
							eCommerce_FrontEnd_Util_UserSession::SetIdentity($userProfile);
						}
						//if(!$this->is_pay)$orderFull = eCommerce_SDO_Cart::CreateOrderFromCart( $userProfile->getProfileId(), $comments, $paymentMethod,'', $banco);
						$loadUserProfile = eCommerce_SDO_User::LoadUserAddress($userProfile->getProfileId());						
				$comments = $this->getParameter('comentarios', false,'');
				$paymentMethod = $this->getParameter('paymentMethod', false);																				
				 $orderFull = eCommerce_SDO_Cart::CreateOrderFromCart( $userProfile->getProfileId(), $comments, $paymentMethod );
				eCommerce_SDO_Cart::DestroyActualCart();				
				/*if($paymentMethod=="c" || $paymentMethod=="C"){
					header("location:creditcard_payment.php?orderid=".$orderFull->getOrderId());
				}*/
				if($paymentMethod == 'en_linea'){					
					$banamex = new Banamex();
					//$total = eCommerce_SDO_Core_Application_Order::GetTotalOrder($orderFull,true);					
					//$total = $total['price']['amount'];//debug($total,'total antes: ');
					$total=$orderFull->getTotal();										
					//debug($total); die;
					/*if($_SERVER['REMOTE_ADDR']=="187.189.11.172"){
						$total="1.00";
					}*/
					if($total <= 0){
							throw new Exception ('El monto a pagar no puede ser igual a 0');
					}
					$total = number_format($total,2);
					$total = str_replace(array('.',','), '', $total);//debug($total,'total despues: ');
					$id_plan	= $this->getParameter('id_plan',false,$id_plan);
					eCommerce_FrontEnd_Util_Session::Set('plan_id', $id_plan);
									//ESTO ERA PARA PRUEBAS CON BANAMEX
									//$total = $this->getParameter('declinar') ? '454' : '100'; //solo en pruebas
					//debug($total); die;
					$url = $banamex->genericVpcUrl($orderFull->getOrderId(), $total, $id_plan); //declinada 454
					//if($_SERVER['REMOTE_ADDR']=="189.203.203.113"){
					//}
					//echo $url; die;
					$this->tpl->assign('url',$url);
					$nextStepToShow = 3.5;
				}				
				break;			
			case 3:
				$nextStepToShow  = 3;
			break;
			case 5:	
					$saveStep        = true;
					$cart            = eCommerce_SDO_Cart::GetCart();
					$userProfile     = eCommerce_FrontEnd_Util_UserSession::GetIdentity();					
					$userEmail       = $userProfile->getEmail();
					$existeUser      = eCommerce_SDO_User::LoadUserProfileByEmail($userEmail);
					$existeProfileId = $existeUser->getProfileId();
					if(!empty($existeProfileId) && $existeUser->getRole() != eCommerce_SDO_User::CUSTOMER_ROLE){
						$existeUser->setFirstName($_REQUEST['nombre']);
						$existeUser->setLastName($_REQUEST['apellido']);
						$existeUser->setPassword(md5("sinpassword")); 
						$existeUser->setConfirmPassword(md5("sinpassword"));
						$existeUser->setEmail( $userEmail );
						$existeUser->setRole( eCommerce_SDO_User::ANONYMOUS_ROLE );						
						$userProfile = eCommerce_SDO_User::SaverUserProfile($existeUser,false);
						$userProfile->setEmail($userEmail);
						$userProfile->setPassword($userProfile->getPassword());
						$userProfile->setRole(eCommerce_SDO_User::ANONYMOUS_ROLE);						
						eCommerce_FrontEnd_Util_UserSession::SetIdentity( $userProfile );
					}elseif($existeProfileId == 0 && $existeUser->getRole() == eCommerce_SDO_User::CUSTOMER_ROLE ){ //en caso de que no este en base de datos se genera un nuevo registro
						$existeUser->setFirstName($_REQUEST['nombre']);
						$existeUser->setLastName($_REQUEST['apellido']);
						$existeUser->setPassword(md5("sinpassword")); 
						$existeUser->setConfirmPassword(md5("sinpassword"));
						$existeUser->setEmail( $userEmail );
						$existeUser->setRole( eCommerce_SDO_User::ANONYMOUS_ROLE );						
						$userProfile = eCommerce_SDO_User::SaverUserProfile($existeUser,false);
						$userProfile->setEmail($userEmail);
						$userProfile->setPassword($userProfile->getPassword());
						$userProfile->setRole(eCommerce_SDO_User::ANONYMOUS_ROLE);						
						eCommerce_FrontEnd_Util_UserSession::SetIdentity( $userProfile );
					}else{
						$existeUser      = eCommerce_SDO_User::LoadUserProfile($userProfile->getProfileId());
						$existeUser->setFirstName($_REQUEST['nombre']);
						$existeUser->setLastName($_REQUEST['apellido']);
						$existeUser->setEmail( $userEmail );
						$userProfile = eCommerce_SDO_User::SaverUserProfile($existeUser,false,true);
						$userProfile->setEmail($userEmail);
						$userProfile->setPassword($userProfile->getPassword());
						$userProfile->setRole($userProfile->getRole());	
						eCommerce_FrontEnd_Util_UserSession::SetIdentity( $userProfile );
					}	
					$requiere = $this->getParameter('factura',false,$cart->getRequiereFactura());
					$cart->setRequiereFactura($this->getParameter('factura',false,$cart->getRequiereFactura()));
					eCommerce_SDO_Core_Application_Cart::SaveCart($cart);					
					$result = $this->_setAddress( eCommerce_SDO_Cart::ADDRESS_SHIP_TO, $saveStep );															
					$result = $this->_setAddress( eCommerce_SDO_Cart::ADDRESS_BILL_TO, $saveStep );					
					$nextStepToShow  = 4;
				break;
		}
		//view next step
		switch( $nextStepToShow ){
			case 1:
			default:	
				//$this->tpl->display( 'PurchaseStep1.php' );
				$this->tpl->display( 'PurchaseStep1.1.php' );
			break;
			case 2:
				$this->tpl->display( 'PurchaseStep2.php' );
			break;
			case 3:
				if($_REQUEST['msj']=="declinada")
					$this->tpl->assign("mensaje","Transacci&oacute;n declinada" );
				$this->tpl->display( 'PurchaseStep3.php' );
			break;
			case 3.5:
				$this->tpl->display( 'PurchaseStep3.5.php' );
			break;
			case 5: //final banamex
				//$total = eCommerce_SDO_Core_Application_Order::GetTotalOrder($orderFull,true);
				//$total = $total['price']['amount'];//debug($total,'total antes: ');
				$orderFull 		= eCommerce_SDO_Order::LoadFullById($orderFull->getOrderId());
				$total =$orderFull->getTotal();
				$this->tpl->assign("amount", $total);
				$this->tpl->assign("autorizacion", $order->getFolio());
				$this->tpl->assign("orderId", $orderFull->getOrderId());				
				$this->tpl->assign("orderFull", $orderFull);
				$this->tpl->assign("paymentMethod",$paymentMethod);				
				 eCommerce_SDO_Catalog::sendNotificationNewOrder($orderFull);
				$this->tpl->display( 'PagosFinal.php' );
			break; 
			case 4:
				$userProfile = eCommerce_FrontEnd_Util_UserSession::GetIdentity();	
				if($userProfile->getRole() == eCommerce_SDO_User::ANONYMOUS_ROLE){
					$nameArray = 'ship';	
					$entityAddress = $this->getParameter( $nameArray, false, array() );

					$country = $entityAddress['country'];
					$address = new eCommerce_Entity_User_Address();
					$address->setProfileId( $userProfile->getProfileId() );
					$address->setStreet( $entityAddress['street'] );
					$address->setStreet2( $entityAddress['street2'] );
					$address->setStreet3( $entityAddress['street3'] );
					$address->setStreet4( $entityAddress['street4'] );
					$address->setCity( $entityAddress['city'] );
					$address->setState( $entityAddress['state'] );
					$address->setCountry(  empty($country) ? 'MX' : $country  );
					$address->setZipCode( $entityAddress['zip_code'] );
					$address->setPhone( $entityAddress['phone'] );
					$address->setMunicipio($entityAddress['municipio']);
					$address->setContacto($entityAddress['recibe']);
					/*$address->setPhone2( $entityAddress['phone2'] );
					 $address->setPhone3( $entityAddress['phone3'] );
					 $address->setPhone4( $entityAddress['phone4'] );
					$address->setContactMethod( $entityAddress['contact_method'] );*/
					eCommerce_SDO_User::SaveUserAddress( $address );
				}else{
					$nameArray = 'ship';
					$entityAddress = $this->getParameter( $nameArray, false, array() );
					$country = $entityAddress['country'];
					$address = new eCommerce_Entity_User_Address();
					$address->setProfileId( $userProfile->getProfileId() );
					$address->setStreet( $entityAddress['street'] );
					$address->setStreet2( $entityAddress['street2'] );
					$address->setStreet3( $entityAddress['street3'] );
					$address->setStreet4( $entityAddress['street4'] );
					$address->setCity( $entityAddress['city'] );
					$address->setState( $entityAddress['state'] );
					$address->setCountry(  empty($country) ? 'MX' : $country  );
					$address->setZipCode( $entityAddress['zip_code'] );
					$address->setPhone( $entityAddress['phone'] );
					$address->setMunicipio($entityAddress['municipio']);
					$address->setContacto($entityAddress['recibe']);
					eCommerce_SDO_User::SaveUserAddress( $address );
				}
				/*if($userProfile->getRole() == eCommerce_SDO_User::ANONYMOUS_ROLE){
					$userProfile = eCommerce_SDO_User::LoadUserProfileByEmail($ship->getEmail());
					if($userProfile->getEmail() != $ship->getEmail()){
						$userProfile->setFirstName( $ship->getFirstName() );
						$userProfile->setLastName( $ship->getLastName() );
						$userProfile->setEmail( $ship->getEmail() );
						$userProfile->setRole(eCommerce_SDO_User::ANONYMOUS_ROLE);
						$userProfile = eCommerce_SDO_User::SaverUserProfile($userProfile);
					}
					eCommerce_FrontEnd_Util_UserSession::SetIdentity($userProfile);
				}	*/	



				$loadUserProfile = eCommerce_SDO_User::LoadUserAddress($userProfile->getProfileId());
				$comments        = $this->getParameter('comentarios', false,'');
				$paymentMethod   = $this->getParameter('paymentMethod', false);
				$orderFull       = eCommerce_SDO_Cart::CreateOrderFromCart( $userProfile->getProfileId(), $comments, $paymentMethod );

				$credit_card = array();
				$credit_card['cardmember']      = $_POST['cardmember'];
				$credit_card['paymentMethodId'] = $_POST['paymentMethodId'];
				$credit_card['token']           = $_POST['token'];
				$medio_pago                     = $_POST['medio_pago'];

				$this->_procesaPago($orderFull,$credit_card,$medio_pago);
				
			break;
		}
	}

	public function _procesaPago($orderFull,$credit_card,$medio_pago){
		#-- Valida el método de pago 
		#-- en_linea => Envia y Guarda parametros Mercado Pago
		#-- deposito => Muestra orden completada
		if(strcmp($orderFull->getPaymentMethod(), 'en_linea') == 0){
			/** 
			* PROCESA Y ENVIA DATOS A SDK MERCADO PAGO 
			**/
			$save = self::_apimercadopago($orderFull,$credit_card,$orderFull->getPaymentMethod(),false);

			#-- Valida si la transacción se realizo correctamente
			if($save['transaction'] == false){
				#-- Muestra mensaje de error
				eCommerce_SDO_Cart::DestroyActualCart();
				$orderPayment = eCommerce_SDO_OrderPayment::LoadOrderPayment($save['responseId']);
				$responseCode = self::decodeResponseCode($orderPayment->getResponsecode(),$credit_card['paymentMethodId']);
				$this->tpl->assign("orderFull", $orderFull);
				$this->tpl->assign("paymentMethod",$orderFull->getPaymentMethod());
				$this->tpl->assign("responseCode",$orderPayment->getResponsecode());
				$this->tpl->assign("responseMessage",$responseCode);
				#-- Order Complete
				#-- Cambia el estatus de la orden ya que fue rechazado el pago
				$order = eCommerce_SDO_Order::LoadById($orderPayment->getOrderId());
				$Order = new eCommerce_Entity_Order();	
				$Order->setOrderId($order->getOrderId());
				$Order->setProfileId($order->getProfileId());
				$Order->setPaymentMethod($order->getPaymentMethod());
				$Order->setStatus("R");
				$Order->setEnviado("No");
				$Order->setComments($order->getComments());
				$Order->setCreationDate($order->getCreationDate());
				$Order->setCurrency($order->getCurrency());
				$Order->setLanguage($order->getLanguage());
				$Order->setRequiereFactura($order->getRequiereFactura());
				$Order->setFolio($order->getFolio());
				$Order->setTipoCargo($order->getTipoCargo());
				$Order->setTipoTarjeta($credit_card['paymentMethodId']);
				#-- Si esta activo cupón de descuento
				if(isset($_SESSION['cupon_descuento']) && $_SESSION['cupon_descuento'] == 'true'){
					$Order->setDescuento($_SESSION["valor_descuento"]);
				}
				eCommerce_SDO_Order::SaveOrder( $Order );						
				$this->tpl->display( 'PurchaseOrderRejected.php' );
			}elseif($save['transaction'] == true){
				$orderPayment = eCommerce_SDO_OrderPayment::LoadOrderPayment($save['responseId']);
				$responseCode = self::decodeResponseCode($orderPayment->getResponsecode(),$credit_card['paymentMethodId']);
				$this->tpl->assign("orderFull", $orderFull);
				$this->tpl->assign("paymentMethod",$orderFull->getPaymentMethod());
				$this->tpl->assign("responseCode",$orderPayment->getResponsecode());
				$this->tpl->assign("responseMessage",$responseCode);
				$this->tpl->assign("amount", $save['amount']);
				$this->tpl->assign("statement_descriptor",$save['description']);
				#-- Order Complete
				#-- Cambia el estatus de la orden de compra si el pago fue exitoso
				if($orderPayment->getState() ==  "approved"){
					$order = eCommerce_SDO_Order::LoadById($orderPayment->getOrderId());
					$Order = new eCommerce_Entity_Order();	
					$Order->setOrderId($order->getOrderId());
					$Order->setProfileId($order->getProfileId());
					$Order->setPaymentMethod($order->getPaymentMethod());
					$Order->setStatus("C");
					$Order->setEnviado("No");
					$Order->setComments($order->getComments());
					$Order->setCreationDate($order->getCreationDate());
					$Order->setCurrency($order->getCurrency());
					$Order->setLanguage($order->getLanguage());
					$Order->setRequiereFactura($order->getRequiereFactura());
					$Order->setFolio($order->getFolio());
					$Order->setTipoCargo($order->getTipoCargo());
					$Order->setTipoTarjeta($credit_card['paymentMethodId']);
					#-- Si esta activo cupón de descuento
					if(isset($_SESSION['cupon_descuento']) && $_SESSION['cupon_descuento'] == 'true'){
						$Order->setDescuento($_SESSION["valor_descuento"]);
					}
					$orderUpdate = eCommerce_SDO_Order::SaveOrder( $Order );
					#-- Envia corre
					//eCommerce_SDO_Core_Application_Order::sendNotificationStatusPayU($orderPayment->getOrderId()); 
				}elseif($orderPayment->getState() == "in_process"){ #-- Pago en proceso de aprovación
					$order = eCommerce_SDO_Order::LoadById($orderPayment->getOrderId());
					$Order = new eCommerce_Entity_Order();	
					$Order->setOrderId($order->getOrderId());
					$Order->setProfileId($order->getProfileId());
					$Order->setPaymentMethod($order->getPaymentMethod());
					$Order->setStatus("N");
					$Order->setEnviado("No");
					$Order->setComments($order->getComments());
					$Order->setCreationDate($order->getCreationDate());
					$Order->setCurrency($order->getCurrency());
					$Order->setLanguage($order->getLanguage());
					$Order->setRequiereFactura($order->getRequiereFactura());
					$Order->setFolio($order->getFolio());
					$Order->setTipoCargo($order->getTipoCargo());
					$Order->setTipoTarjeta($credit_card['paymentMethodId']);
					#-- Si esta activo cupón de descuento
					if(isset($_SESSION['cupon_descuento']) && $_SESSION['cupon_descuento'] == 'true'){
						$Order->setDescuento($_SESSION["valor_descuento"]);
					}
					eCommerce_SDO_Order::SaveOrder( $Order );
				}
				#-- Mande correo de confirmació del pedido y muestra mensaje de pagado
				eCommerce_SDO_Catalog::sendNotificationNewOrder($orderFull,$orderFull->getPaymentMethod(),$save['responseId']);						
				$this->tpl->display( 'PurchaseOrderComplete.php');
				//header('location: PurchaseOrderComplete.php?o='.$orderFull->getOrderId());
			}//end if
		}elseif(strcmp($orderFull->getPaymentMethod(), 'deposito') == 0){
			/** 
			* PROCESA Y ENVIA DATOS A SDK MERCADO PAGO 
			**/
			//$medio_pago = $_POST['medio_pago'];
			$save = self::_apimercadopago($orderFull,$credit_card,$orderFull->getPaymentMethod(),$medio_pago);
			if($save['transaction'] == false){
				#-- Muestra mensaje de error
				unset($_SESSION['Auth']);
				eCommerce_SDO_Cart::DestroyActualCart();
				$orderPayment = eCommerce_SDO_OrderPayment::LoadOrderPayment($save['responseId']);
				$responseCode = self::decodeResponseCode($orderPayment->getResponsecode(),$credit_card['paymentMethodId']);
				$this->tpl->assign("orderFull", $orderFull);
				$this->tpl->assign("paymentMethod",$paymentMethod);
				$this->tpl->assign("responseCode",$orderPayment->getResponsecode());
				$this->tpl->assign("responseMessage",$responseCode);
				#-- Order Complete
				#-- Cambia el estatus de la orden ya que fue rechazado el pago
				$order = eCommerce_SDO_Order::LoadById($orderPayment->getOrderId());
				$Order = new eCommerce_Entity_Order();	
				$Order->setOrderId($order->getOrderId());
				$Order->setProfileId($order->getProfileId());
				$Order->setPaymentMethod($order->getPaymentMethod());
				$Order->setStatus("R");
				$Order->setEnviado("No");
				$Order->setComments($order->getComments());
				$Order->setCreationDate($order->getCreationDate());
				$Order->setCurrency($order->getCurrency());
				$Order->setLanguage($order->getLanguage());
				$Order->setRequiereFactura($order->getRequiereFactura());
				$Order->setFolio($order->getFolio());
				$Order->setTipoCargo($order->getTipoCargo());
				$Order->setTipoTarjeta($medio_pago);
				if(isset($_SESSION['cupon_descuento']) && $_SESSION['cupon_descuento'] == 'true'){
					$Order->setDescuento($_SESSION["valor_descuento"]);
				}
				eCommerce_SDO_Order::SaveOrder( $Order );
				$this->tpl->display( 'PurchaseOrderRejected.php' );
			}elseif($save['transaction'] == true){
				$orderPayment = eCommerce_SDO_OrderPayment::LoadOrderPayment($save['responseId']);
				$responseCode = self::decodeResponseCode($orderPayment->getResponsecode(),$credit_card['paymentMethodId']);
				$this->tpl->assign("orderFull", $orderFull);
				$this->tpl->assign("paymentMethod",$orderFull->getPaymentMethod());
				$this->tpl->assign("responseCode",$orderPayment->getResponsecode());
				$this->tpl->assign("responseMessage",$responseCode);
				$this->tpl->assign("amount", $save['amount']);
				$this->tpl->assign("responseId",$save['responseId']);
				#-- Order Complete
				#-- Cambia el estatus de la orden de compra si el pago fue exitoso
				if($orderPayment->getState() ==  "pending"){
					$order = eCommerce_SDO_Order::LoadById($orderPayment->getOrderId());
					$Order = new eCommerce_Entity_Order();	
					$Order->setOrderId($order->getOrderId());
					$Order->setProfileId($order->getProfileId());
					$Order->setPaymentMethod($order->getPaymentMethod());
					$Order->setStatus("N");
					$Order->setEnviado("No");
					$Order->setComments($order->getComments());
					$Order->setCreationDate($order->getCreationDate());
					$Order->setCurrency($order->getCurrency());
					$Order->setLanguage($order->getLanguage());
					$Order->setRequiereFactura($order->getRequiereFactura());
					$Order->setFolio($order->getFolio());
					$Order->setTipoCargo($order->getTipoCargo());
					$Order->setTipoTarjeta($medio_pago);
					if(isset($_SESSION['cupon_descuento']) && $_SESSION['cupon_descuento'] == 'true'){
						$Order->setDescuento($_SESSION["valor_descuento"]);
					}
					eCommerce_SDO_Order::SaveOrder( $Order );
				}
				#-- Mande correo de confirmació del pedido y muestra mensaje de pagado 
				eCommerce_SDO_Catalog::sendNotificationNewOrder($orderFull,$orderFull->getPaymentMethod(),$save['responseId']);
				$this->tpl->display( 'PurchaseOrderComplete.php');
			}
			die();
		}//end if

		/**********************************************************/
		/******************* Pagos con Pay Pal ********************/
		/**********************************************************/
		if(strcmp($orderFull->getPaymentMethod(), 'paypal') == 0){
			unset($_SESSION['Auth']);
			eCommerce_SDO_Cart::DestroyActualCart();
			$this->tpl->assign("orderFull", $orderFull);
			$this->tpl->assign("paymentMethod",$orderFull->getPaymentMethod());
			eCommerce_SDO_Catalog::sendNotificationNewOrder($orderFull,$paymentMethod,false);
			$this->tpl->display( 'PurchaseOrderComplete.php');
		}
	}

	/** 
	* Función que genera peticiones por medio de la API de Mercado Pago
	* Retorna un arreglo con dos posiciones transaction (bool) true si la transacción se realizo con éxito 
	* responseId (int) Id guardado en la tabla "order_payment"
	* Necesita la inclusión a través del common del SDK de Mercado Pago (bo/libs/MercadoPago/)
	* @version 1
	* @author Arturo Hernández
	* @link https://www.mercadopago.com.mx/developers/es/solutions/payments/custom-checkout/charge-with-creditcard/javascript#charge
	* @param $orderFull (array) información de la orden de compra
	* @param $credit_card (array) información del token que se genero con la tarje de crédito
	* @param $metodo (string) metodo de pago (en_linea, deposito)
	* @param $medio_pago (string) medio de pago (Oxxo,7Eleven, etc.) 
	* @return $return array(transaction=>bool, responseId=>int) 
	*/
	public static function _apimercadopago($orderFull,$credit_card=array(),$metodo,$medio_pago=false){
		$test        = true;
		$items       = $orderFull->getItems();
		$description = "";
		$total_value = "";

		foreach ($items as $item) {
			$version = eCommerce_SDO_Catalog::LoadVersion($item->getVersionId());
			//$description    .=  $item->getQuantity()." ".$item->getName()." | \t\n";
			$description    .=  $version->getName()." | \t\n";
			$totalProductos += $item->getPrice() * $item->getQuantity();
		}//end foreach

		$total       = eCommerce_SDO_Cart::GetTotalCart(true,true);
		#-- Validación para aplicar descuento
		if(isset($_SESSION['cupon_descuento']) && $_SESSION['cupon_descuento'] == 'true'){
			//$descuento   = $total['price']['amount'] * 0.10;
			$descuento   = ($totalProductos * $_SESSION['valor_descuento']);
			$total_value = ($total['price']['amount'] - $descuento);
		}else{
			$total_value = $total['price']['amount'];
		}//end if
		$total_value = (double)number_format($total_value,2,'.','');

		//$total_value = $totalProductos;
		#-- Datos de usuario
		$usuario = eCommerce_SDO_Core_Application_Profile::LoadById($orderFull->getProfileId());
		#-- Genera referencia de pago interna
		$reference = $orderFull->getOrderId()."|".$orderFull->getProfileId()."|".date('Ymd');
		/** 
		* Public key genarada por Mercado Pago 
		* @link https://www.mercadopago.com.mx/developers/es/solutions/payments/custom-checkout/charge-with-creditcard/javascript#get-token
		**/
		$publicKey    = $test == false ? 'TEST-8120775165163701-121516-a3219d14c1e9818d1cfd92fbfa980769__LC_LD__-200395647' : 'APP_USR-8120775165163701-050412-9ba2b758e5b45707f68d8d85cebdfb28__LA_LD__-200395647' ;
		#-- Llamda a la clase del SDK de Mercado Pago (bo/libs/MercadoPago/)
		$mp        = new Mercadopago($publicKey);
		if($metodo == 'en_linea'){
			#-- Genera petición para pagos en línea	
			$payment_data = array(
				"transaction_amount" => $total_value,
				"token"              => $credit_card['token'],
				"description"        => $description,
				"installments"       => 1,
				"payment_method_id"  => $credit_card['paymentMethodId'],
				"payer"              => array (
					"email"      => $credit_card['cardmember']
				),
				"sponsor_id" => 230372236, //Número de identificación del desarrollador
				"notification_url" => "http://printproyect.com/bo/valida_response.php?id_usuario=".$usuario->getProfileId()."&id_pago=".$orderFull->getOrderId(),
				"external_reference" => $reference,
			);
		}elseif($metodo == 'deposito'){
			#-- Genera petición para pagos en efectivo
			$payment_data = array(
				"transaction_amount" => $total_value,
				"description"        => $description,
				"payment_method_id"  => $medio_pago,
				"payer"              => array (
					"email"      => $credit_card['cardmember']
				),
				"sponsor_id" => 230372236, //Número de identificación del desarrollador
				"notification_url" => "http://printproyect.com/bo/valida_response.php?id_usuario=".$usuario->getProfileId()."&id_pago=".$orderFull->getOrderId(),
				"external_reference" => $reference,
			);
		}
		#-- Genera Petición de pago
		try {
			$payment = $mp->post("/v1/payments",$payment_data);	
		} catch (Exception $e) {
			echo "<pre>";
			var_dump($e);
			echo "</pre>";
			die();
		}
		//$payment = $mp->post("/v1/payments",$payment_data);
		#-- Captura la respuesta
		$status               = $payment['status'];
		$responseId           = $payment['response']['id'];
		$date_created         = $payment['response']['date_created'];
		$date_approved        = $payment['response']['date_approved'];
		$payment_method_id    = $payment['response']['payment_method_id'];
		$payment_type_id      = $payment['response']['payment_type_id'];
		$response_status      = $payment['response']['status'];
		$status_detail        = $payment['response']['status_detail'];
		$transaction_amount   = $payment['response']['transaction_amount'];
		$statement_descriptor = $payment['response']['statement_descriptor'];
		if($metodo == 'deposito'){
			$urlpayment = $payment['response']['transaction_details']['external_resource_url'];
		}
		/** HTTP Status 201 OK **/
		/** HTTP Status 400 Bad Request **/
		if($status == 201){
			$OrderPayment = new eCommerce_Entity_OrderPayment();
			$OrderPayment->setOrderPaymentId('');			
			$OrderPayment->setOrderId($orderFull->getOrderId());
			$OrderPayment->setCode($status);
			$OrderPayment->setOrdertransactionid($responseId);
			$OrderPayment->setOperationdate($date_created);
			$OrderPayment->setApproveddate($date_approved);
			$OrderPayment->setTrazabilitycode($payment_method_id.' | '.$payment_type_id); //tipo tarjeta
			$OrderPayment->setState($response_status);
			$OrderPayment->setResponsecode($status_detail);
			$OrderPayment->setReference($reference);
			if($metodo == 'deposito'){
				$OrderPayment->setUrlpayment($urlpayment);
			}//end if

			$save = eCommerce_SDO_OrderPayment::SaverOrderPayment( $OrderPayment );	

			
			#-- Regresa información de pago
			if($save){
				#-- Valida si la transacción fue aprovada
				if($save->getState() == "approved" || $save->getState() == "in_process"){
					$return = array('transaction'=> true,'responseId'=>$save->getOrderPaymentId(),'amount'=>$transaction_amount,'description'=>$statement_descriptor);
				}elseif($save->getState() == "rejected"){
					$return = array('transaction'=> false,'responseId'=>$save->getOrderPaymentId());
				}elseif($save->getState() == "pending") { // Solo pagos en efectivo
					$return = array('transaction'=> true,'responseId'=>$save->getOrderPaymentId());
				}
				return $return;
			}//end if
		}elseif($status == 400){ //HTTP Status 400 Bad Request
			$OrderPayment = new eCommerce_Entity_OrderPayment();
			$OrderPayment->setOrderPaymentId('');			
			$OrderPayment->setOrderId($orderFull->getOrderId());
			$OrderPayment->setCode($status);
			$OrderPayment->setOrdertransactionid($responseId);
			$OrderPayment->setOperationdate($date_created);
			$OrderPayment->setApproveddate($date_approved);
			$OrderPayment->setTrazabilitycode($payment_method_id.' | '.$payment_type_id); //tipo tarjeta
			$OrderPayment->setState($response_status);
			$OrderPayment->setResponsecode($status_detail);
			$OrderPayment->setReference($reference);
			$save = eCommerce_SDO_OrderPayment::SaverOrderPayment( $OrderPayment );
			#-- Regresa información de pago
			if($save){
				#-- Transacción de pago declinada
				$return = array('transaction'=> false,'responseId'=>$save->getOrderPaymentId());
				return $return;
			}//end if			
		}//end if
	}//end funcion

	/** 
	* Función que decodifica respuestas de Mercado Pago
	* @version 1
	* @author Arturo Hernández
	* @param $code string codigo de error
	* @param $paymentmethodid string metodo de pago que regresa Mercado Pago
	* @return string
	**/
	public static function decodeResponseCode($code,$paymentmethodid){
		$method = $paymentmethodid == 'debmaster' ? 'MasterCard' : 'Visa';
		switch ($code) {
			case '106':
				return 'No puedes realizar pagos a usuarios de otros países.';
				break;
			case '109':
				return $method.' no procesa pagos en cuotas. Eliga otra tarjeta u otro medio de pago.';
				break;
			case '126':
			case '145':
			case '160':
				return 'No pudimos procesar su pago.';
				break;
			case '150':
			case '151':
				return 'No puedes realizar pagos.';
				break;
			case '129':
				return $method.' no procesa pagos del monto seleccionado. Elige otra tarjeta u otro medio de pago.';
				break;
			case '204':
				return $method.'  no está disponible en este momento. Elige otra tarjeta u otro medio de pago.';
				break;
			case '801':
				return 'Realizaste un pago similar hace instantes. Intenta nuevamente en unos minutos.';
				break;
			case 'cc_rejected_bad_filled_card_number':
				return "El pago no pudo ser procesado, revise el número de su tarjeta";
				break;
			case 'cc_rejected_bad_filled_date':
				return "El pago no pudo ser procesado, revise la fecha de vencimiento de su tarjeta.";
				break;
			case 'cc_rejected_bad_filled_other':
				return "El pago no pudo ser procesado por un error en los datos de su tarjeta";
				break;
			case 'cc_rejected_bad_filled_security_code':
				return "El pago no pudo ser procesado, revise el código de seguridad de su tarjeta";
				break;
			case 'cc_rejected_card_disabled':
				return "Llama a ".$method." para que active su tarjeta. El teléfono está al dorso de su tarjeta.";
				break;
			case 'cc_rejected_card_error':
				return "No pudimos procesar su pago.";
				break;
			case 'cc_rejected_duplicated_payment':
				return "Ya realizo un pago por ese valor. Si necesita volver a pagar use otra tarjeta u otro medio de pago.";
				break;
			case 'cc_rejected_high_risk':
				return "Su pago fue rechazado. Eliga otro de los medios de pago, te recomendamos con medios en efectivo.";
				break;
			case 'cc_rejected_insufficient_amount':
				return "Su tarjeta no tiene fondos suficientes.";
				break;
			case 'cc_rejected_max_attempts':
				return "Llegó al límite de intentos permitidos. Eliga otra tarjeta u otro medio de pago.";
				break;
			case 'cc_rejected_other_reason':
				return $method." no procesó el pago";
				break;
			case 'cc_rejected_call_for_authorize':
				return "Debe autorizar ante ".$method." el pago de la orden a MercadoPago";
				break;
			default :
				return 'default';
			break;				
		}//end function
	}//end function

	/**
	* Función para procesar pagos con tarjeta con SDK de Pay U
	**/
	protected static function _payuproccess($orderFull,$credit_card=array(),$metodo,$medio_pago=false){
		include_once 'sdk_payu/lib/PayU.php';
		$items = $orderFull->getItems();
		$description = "";
		foreach ($items as $item) {
			$description .=  $item->getQuantity()." ".$item->getName()." | \t\n";
			$totalProductos += ($item->getPrice() * $item->getQuantity());
		}//end foreach
		$total       = eCommerce_SDO_Cart::GetTotalCart(true,true);
		#-- Validación para aplicar descuento
		if(isset($_SESSION['cupon_descuento']) && $_SESSION['cupon_descuento'] == 'true'){
			//$descuento   = $total['price']['amount'] * 0.10;
			$descuento   = $totalProductos * 0.10;
			$total_value = $total['price']['amount'] - $descuento;
		}else{
			$total_value = $total['price']['amount']; //El mónto de pago debe se mayor a $39.00 MXN*/	
		}//end if
		$reference   = $orderFull->getOrderId()."|".$orderFull->getProfileId()."|".date('Ymd');
		//$test = $_SERVER['REMOTE_ADDR'] == '187.190.153.229' ? true : false;
		$test = true;
		if($test == true){
			PayU::$apiKey     = "6u39nqhq8ftd0hlvnjfs66eh8c"; //Ingrese aquí su propio apiKey.
			PayU::$apiLogin   = "11959c415b33d0c"; //Ingrese aquí su propio apiLogin.
			PayU::$merchantId = "500238"; //Ingrese aquí su Id de Comercio.
			PayU::$language   = SupportedLanguages::ES; //Seleccione el idioma.
			PayU::$isTest     = true; //Dejarlo True cuando sean pruebas.
			$accountId         = "500547";
		}else{
			PayU::$apiKey     = "eZrh8xC9IBu5OPXgr3mV4R4nq5"; //Ingrese aquí su propio apiKey.
			PayU::$apiLogin   = "TzCOaHj5Y3q9fgw"; //Ingrese aquí su propio apiLogin.
			PayU::$merchantId = "549993"; //Ingrese aquí su Id de Comercio.
			PayU::$language   = SupportedLanguages::ES; //Seleccione el idioma.
			PayU::$isTest     = false; //Dejarlo True cuando sean pruebas.
			$accountId         = "552288";
		}
		/*echo $accountId;
		die();*/
		/** URL DE PRUEBAS **/
		/** http://developers.payulatam.com/es/sdk/sandbox.html **/
		// URL de Pagos
		if($test == true){
			Environment::setPaymentsCustomUrl("https://stg.api.payulatam.com/payments-api/4.0/service.cgi");
		}else{
			Environment::setPaymentsCustomUrl("https://api.payulatam.com/payments-api/4.0/service.cgi"); 
		}
		/** DATOS DEL CLIENTE **/
		$Address = $orderFull->getAddresses();
		foreach ($Address as $value) {
			$BUYER_NAME          = $value->getFirstName()." ".$value->getLastName();
			$BUYER_EMAIL         = $value->getEmail();
			$BUYER_CONTACT_PHONE = $value->getPhone();
			$BUYER_STREET        = $value->getStreet();
			$BUYER_STREET_2      = $value->getStreet3()." Num. Int.".$value->getStreet4()." ".$value->getStreet2();
			$BUYER_CITY          = $value->getCity();
			$BUYER_STATE         = $value->getState();
			$BUYER_COUNTRY       = $value->getCountry();
			$BUYER_POSTAL_CODE   = $value->getZipCode();
			$BUYER_PHONE         = $value->getPhone();			
			$PAYER_NAME          = $value->getFirstName()." ".$value->getLastName();
			$PAYER_EMAIL         = $value->getEmail();
			$PAYER_CONTACT_PHONE = $value->getPhone();
			$PAYER_STREET        = $value->getStreet();
			$PAYER_STREET_2      = $value->getStreet3()." Num. Int.".$value->getStreet4()." ".$value->getStreet2();
			$PAYER_CITY          = $value->getCity();
			$PAYER_STATE         = $value->getState();
			$PAYER_COUNTRY       = $value->getCountry();
			$PAYER_POSTAL_CODE   = $value->getZipCode();
			$PAYER_PHONE         = $value->getPhone();
		}//end
		if($metodo == 'en_linea'){
			/** DATOS DE TARJETA **/
			$CREDIT_CARD_NUMBER          = $credit_card['credit_number'];
			$CREDIT_CARD_EXPIRATION_DATE = $credit_card['credit_expire_A']."/".$credit_card['credit_expire_M'];
			$CREDIT_CARD_SECURITY_CODE   = $credit_card['credit_security_code'];
			if($credit_card['credit_payment_method'] == "VISA"){
				$PAYMENT_METHOD = PaymentMethods::VISA;
			}elseif($credit_card['credit_payment_method'] == "MASTERCARD"){
				$PAYMENT_METHOD = PaymentMethods::MASTERCARD;
			}elseif($credit_card['credit_payment_method'] == "AMEX") {
				$PAYMENT_METHOD = PaymentMethods::AMEX;
			}			
		}elseif($metodo == 'deposito'){
			$hoy = date('Y-m-d');
			if($medio_pago == 'OXXO'){
				$PAYMENT_METHOD  = PaymentMethods::OXXO;
				$fecha           = new DateTime($hoy);
				$fecha->add(new DateInterval('P10D'));
				$EXPIRATION_DATE =   $fecha->format('Y-m-d')."T23:59:59";
			}elseif($medio_pago == '7ELEVEN'){
				$PAYMENT_METHOD = PaymentMethods::SEVEN_ELEVEN;
				$fecha           = new DateTime($hoy);
				$fecha->add(new DateInterval('P10D'));
				$EXPIRATION_DATE =   $fecha->format('Y-m-d')."T23:59:59";
			}else{
				$PAYMENT_METHOD = $medio_pago;
				$EXPIRATION_DATE = "";
			}//end if
		}//end if
		/** DATOS DE SESSION */
		$DEVICE_SESSION_ID = md5(session_id().microtime());
		$IP_ADDRESS        = $_SERVER['REMOTE_ADDR'];
		$PAYER_COOKIE      = session_id();
		$USER_AGENT        = $_SERVER['HTTP_USER_AGENT'];
		//$reference = "05221090"; //Se debe generar un número de referencia
		//$value = "39"; //El mónto de pago debe se mayor a $39.00 MXN
		if($metodo == 'en_linea'){ // PAGO CON TARJETA
			$parameters = array(
				//Ingrese aquí el identificador de la cuenta.
				PayUParameters::ACCOUNT_ID     => $accountId,
				//Ingrese aquí el código de referencia.
				PayUParameters::REFERENCE_CODE => $reference,
				//Ingrese aquí la descripción.
				PayUParameters::DESCRIPTION    => $description,
				// -- Valores --
				//Ingrese aquí el valor.        
				PayUParameters::VALUE    => $total_value,
				//Ingrese aquí la moneda.
				PayUParameters::CURRENCY => "MXN",
				// -- Comprador 
				//Ingrese aquí el nombre del comprador.
				PayUParameters::BUYER_NAME          => $BUYER_NAME,
				//Ingrese aquí el email del comprador.
				PayUParameters::BUYER_EMAIL         => $BUYER_EMAIL,
				//Ingrese aquí el teléfono de contacto del comprador.
				PayUParameters::BUYER_CONTACT_PHONE => $BUYER_CONTACT_PHONE,
				//Ingrese aquí el documento de contacto del comprador.
				//PayUParameters::BUYER_DNI         => "5415668464654",
				//Ingrese aquí la dirección del comprador.
				PayUParameters::BUYER_STREET        => $BUYER_STREET,
				PayUParameters::BUYER_STREET_2      => $BUYER_STREET_2,
				PayUParameters::BUYER_CITY          => $BUYER_CITY,
				PayUParameters::BUYER_STATE         => $BUYER_STATE,
				PayUParameters::BUYER_COUNTRY       => "MX",
				PayUParameters::BUYER_POSTAL_CODE   => $BUYER_POSTAL_CODE,
				PayUParameters::BUYER_PHONE         => $BUYER_PHONE,
				// -- pagador --
				//Ingrese aquí el nombre del pagador.
				PayUParameters::PAYER_NAME          => $test == true ? 'APPROVED' : $PAYER_NAME,
				//Ingrese aquí el email del pagador.
				PayUParameters::PAYER_EMAIL         => $PAYER_EMAIL,
				//Ingrese aquí el teléfono de contacto del pagador.
				PayUParameters::PAYER_CONTACT_PHONE => $PAYER_CONTACT_PHONE,
				//Ingrese aquí el documento de contacto del pagador.
				//PayUParameters::PAYER_DNI           => "5415668464654",
				//OPCIONAL fecha de nacimiento del pagador YYYY-MM-DD, importante para autorización de pagos en México.
				//PayUParameters::PAYER_BIRTHDATE     => '1980-06-22',
				//Ingrese aquí la dirección del pagador.
				PayUParameters::PAYER_STREET      => $PAYER_STREET,
				PayUParameters::PAYER_STREET_2    => $PAYER_STREET_2,
				PayUParameters::PAYER_CITY        => $PAYER_CITY,
				PayUParameters::PAYER_STATE       => $PAYER_STATE,
				PayUParameters::PAYER_COUNTRY     => "MX",
				PayUParameters::PAYER_POSTAL_CODE => $PAYER_POSTAL_CODE,
				PayUParameters::PAYER_PHONE       => $PAYER_PHONE,
				// -- Datos de la tarjeta de crédito -- 
				//Ingrese aquí el número de la tarjeta de crédito
				PayUParameters::CREDIT_CARD_NUMBER          => $CREDIT_CARD_NUMBER,
				//Ingrese aquí la fecha de vencimiento de la tarjeta de crédito
				PayUParameters::CREDIT_CARD_EXPIRATION_DATE => $CREDIT_CARD_EXPIRATION_DATE,
				//Ingrese aquí el código de seguridad de la tarjeta de crédito
				PayUParameters::CREDIT_CARD_SECURITY_CODE   => $CREDIT_CARD_SECURITY_CODE,
				//Ingrese aquí el nombre de la tarjeta de crédito
				//PaymentMethods::VISA||PaymentMethods::MASTERCARD||PaymentMethods::AMEX    
				PayUParameters::PAYMENT_METHOD              => $PAYMENT_METHOD,
				//Ingrese aquí el número de cuotas.
				PayUParameters::INSTALLMENTS_NUMBER         => "1",
				//Ingrese aquí el nombre del pais.
				PayUParameters::COUNTRY                     => PayUCountries::MX,
				//Session id del device.
				PayUParameters::DEVICE_SESSION_ID           => $DEVICE_SESSION_ID,
				//IP del pagadador
				PayUParameters::IP_ADDRESS                  => $IP_ADDRESS,
				//Cookie de la sesión actual.
				PayUParameters::PAYER_COOKIE                => $PAYER_COOKIE,
				//Cookie de la sesión actual.        
				PayUParameters::USER_AGENT                  => $USER_AGENT
			);			
		}elseif($metodo == 'deposito'){
			$parameters = array(
				//Ingrese aquí el identificador de la cuenta.
				PayUParameters::ACCOUNT_ID      => $accountId,
				//Ingrese aquí el código de referencia.
				PayUParameters::REFERENCE_CODE  => $reference,
				//Ingrese aquí la descripción.
				PayUParameters::DESCRIPTION     => $description,
				// -- Valores --
				//Ingrese aquí el valor.        
				PayUParameters::VALUE           => $total_value,
				//Ingrese aquí la moneda.
				PayUParameters::CURRENCY        => "MXN",
				//Ingrese aquí el email del comprador.
				PayUParameters::BUYER_EMAIL     => $BUYER_EMAIL,
				//Ingrese aquí el nombre del pagador.
				PayUParameters::PAYER_NAME      => $PAYER_NAME,
				//Ingrese aquí el documento de contacto del pagador.
				PayUParameters::PAYER_DNI     => "5415668464654",
				//Ingrese aquí el nombre del método de pago
				//"SANTANDER"||"SCOTIABANK"||"IXE"||"BANCOMER"||PaymentMethods::OXXO||PaymentMethods::SEVEN_ELEVEN
				PayUParameters::PAYMENT_METHOD  => $PAYMENT_METHOD,
				//Ingrese aquí el nombre del pais.
				PayUParameters::COUNTRY         => PayUCountries::MX,
				//Ingrese aquí la fecha de expiración. Sólo para OXXO y SEVEN_ELEVEN
				PayUParameters::EXPIRATION_DATE => $EXPIRATION_DATE,
				//IP del pagadador
				PayUParameters::IP_ADDRESS      => $IP_ADDRESS,
			);			
		}//end if
		/** ENVIA PARAMETROS PARA PROCESAR EL PAGO **/
		try{
			$response = PayUPayments::doAuthorizationAndCapture($parameters);	
		}catch(PayUException $e){
			//eCommerce_SDO_Core_Application_Order::Delete($orderFull->getOrderId());
			$return = array('errorResponse' => false, 'error'=>$e , 'order'=>$orderFull->getOrderId());
			return $return;
		}
		if($metodo == 'en_linea'){
			if ($response) {
				$OrderPayment = new eCommerce_Entity_OrderPayment();
				$OrderPayment->setOrderPaymentId('');			
				$OrderPayment->setOrderId($orderFull->getOrderId());
				$OrderPayment->setCode($response->code);
				$OrderPayment->setOrdertransactionid($response->transactionResponse->orderId);						
				$OrderPayment->setTransactionid($response->transactionResponse->transactionId);
				$OrderPayment->setState($response->transactionResponse->state);
				$OrderPayment->setTrazabilitycode($response->transactionResponse->trazabilityCode);
				$OrderPayment->setAuthorizationcode($response->transactionResponse->authorizationCode);
				$OrderPayment->setResponsecode($response->transactionResponse->responseCode);
				$OrderPayment->setOperationdate($response->transactionResponse->operationDate);
				$save = eCommerce_SDO_OrderPayment::SaverOrderPayment( $OrderPayment );
				if($save){
					#-- Valida si la transacción fue aprovada
					if($save->getState() == "APPROVED"){
						$return = array('transaction'=> true,'responseId'=>$save->getOrderPaymentId());
					}elseif($save->getState() == "DECLINED"){
						$return = array('transaction'=> false,'responseId'=>$save->getOrderPaymentId());
					}elseif($save->getState() == "PENDING"){
						$return = array('transaction'=> true,'responseId'=>$save->getOrderPaymentId());
					}//end if
					return $return;
				}//end if
			}//end if				
		}elseif($metodo == 'deposito'){
			if ($response) {
				$OrderPayment = new eCommerce_Entity_OrderPayment();
				$OrderPayment->setOrderPaymentId('');			
				$OrderPayment->setOrderId($orderFull->getOrderId());
				$OrderPayment->setCode($response->code);
				$OrderPayment->setOrdertransactionid($response->transactionResponse->orderId);						
				$OrderPayment->setTransactionid($response->transactionResponse->transactionId);
				$OrderPayment->setState($response->transactionResponse->state);
				$OrderPayment->setResponsecode($response->transactionResponse->responseCode);
				$OrderPayment->setPendingreason($response->transactionResponse->pendingReason);
				$OrderPayment->setExpirationdate($response->transactionResponse->extraParameters->EXPIRATION_DATE);
				$OrderPayment->setReference($response->transactionResponse->extraParameters->REFERENCE);
				$OrderPayment->setBarcode($response->transactionResponse->extraParameters->BAR_CODE);
				$OrderPayment->setUrlpayment($response->transactionResponse->extraParameters->URL_PAYMENT_RECEIPT_HTML);
				$save = eCommerce_SDO_OrderPayment::SaverOrderPayment( $OrderPayment );
				if($save){
					#-- Valida si la transacción fue aprovada
					if($save->getState() == "APPROVED"){
						$return = array('transaction'=> true,'responseId'=>$save->getOrderPaymentId());
					}elseif($save->getState() == "DECLINED"){
						$return = array('transaction'=> false,'responseId'=>$save->getOrderPaymentId());
					}elseif($save->getState() == "PENDING"){
						$return = array('transaction'=> true,'responseId'=>$save->getOrderPaymentId());
					}//end if
					return $return;
				}//end if
			}//end if
		}//end if
	}//end function	

	protected function _setBillAddress( $save ){
		$address = $cart->getOrderAddressByType( $type );
		$street = $address->getStreet();
		$city   = $address->getCity();
		$state  = $address->getState();
		$country= $address->getCountry();
		$zipCode= $address->getZipCode();
		$street = empty( $street )  ? 'customer Street' : $address->getStreet();
		$city   = empty( $city )    ? 'Puebla ' : $address->getCity();
		$state  = empty( $state )   ? 'Pue ' : $address->getState();
		$country= empty( $country ) ? 'MX' : $address->getCountry();
		$zipCode= empty( $zipCode ) ? 72000 : $address->getZipCode();
		if( $save ){
			//we try to get the params
			$street  = $this->getParameter( 'street',  false);
			$city    = $this->getParameter( 'city',    false);
			$state   = $this->getParameter( 'state',   false);
			$country = $this->getParameter( 'country', false);
			$zipCode = $this->getParameter( 'zipCode', true );
		}
		try{
			$address->setStreet ( $street  );
			$address->setCity   ( $city    );
			$address->setState  ( $state   );
			$address->setCountry( $country );
			$address->setZipCode( $zipCode );
			$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
			if ($actualLanguage == 'EN')
			{
				$lang = 0;
			}
			else
			{
				$lang = 1;
			}
			$add_updtd[0] = "Your address has been updated";
			$add_updtd[1] = "Su direcci&oacute;n ha sido actualizada";
			$err[0] = "Try again please";
			$err[1] = "Por favor intente nuevamente";
			$cart = eCommerce_SDO_Cart::SetBillTo( eCommerce_SDO_Cart::ADDRESS_BILL_TO );
			$msj = ( $save ) ? $add_updtd[$lang] : '';
			$this->tpl->assign('messageSuccess', $msj  );
		}catch( eCommerce_SDO_Exception $e ){
			$this->tpl->assign('messageErr', $err[$lang]." :" . $e->getMessage() );
		}
		$this->tpl->assign('cart', $cart );
		$this->tpl->assign('isEmptyCart', $isEmptyCart );
		$this->tpl->display( 'purchase/step1.php' );
	}

	protected function _setAddress( $type = eCommerce_SDO_Cart::ADDRESS_BILL_TO , $save = false){
		$userProfile = eCommerce_FrontEnd_Util_UserSession::GetIdentity();	
		$userAddress = eCommerce_SDO_User::LoadUserAddress($userProfile->getProfileId());
		$isAnony 	 = ($userProfile->role == eCommerce_SDO_User::ANONYMOUS_ROLE)?false:false;
		//$isAnony = false;
		$result = false;
		$cart = eCommerce_SDO_Cart::GetCart();
		$isEmptyCart = eCommerce_SDO_Cart::IsEmptyCart();
		$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
		if ($actualLanguage == 'EN')
		{
			$lang = 0;
		}
		else
		{
			$lang = 1;
		}
		$add_updtd[0] = "Your address has been updated";
		$add_updtd[1] = "Su direcci&oacute;n ha sido actualizada";
		$err[0] = "There are some errors";
		$err[1] = "Existen algunos errores";
		$empty[0] = "Your Cart is Empty";
		$empty[1] = "Su Carrito Est&aacute; Vac&iacute;o";
		try{
			if( $isEmptyCart ){
				throw new Exception('Carrito de compras vac&iacute;o');
			}
			else{
				$address = $cart->getOrderAddressByType( $type );
				$street    = $isAnony ? $address->getStreet() : (empty($address->street) ? $userAddress->getStreet() : $address->getStreet());					
				$street2   = $isAnony ? $address->getStreet2() : (empty($address->street2) ? $userAddress->getStreet3() : $address->getStreet2());
				$street3   = $isAnony ? $address->getStreet3() : (empty($address->street3) ? $userAddress->getStreet2() : $address->getStreet2());
				$street4   = $isAnony ? $address->getStreet4() : (empty($address->street4) ? $userAddress->getStreet4() : $address->getStreet4());
				$city      = $isAnony ? $address->getCity() : (empty($address->city) ? $userAddress->getCity() : $address->getCity());
				$state     = $isAnony ? $address->getState() : (empty($address->state) ? $userAddress->getState() : $address->getState());
				$country   = $isAnony ? $address->getCountry() : (empty($address->country) ? $userAddress->getCountry() : $address->getCountry());				
				$zipCode   = $isAnony ? $address->getZipCode() : (empty($address->zip_code) ? $userAddress->getZipCode() : $address->getZipCode());				
				$phone     = $isAnony ? $address->getPhone() : (empty($address->phone) ? $userAddress->getPhone() : $address->getPhone());				
				//$municipio = $isAnony ? $address->getMunicipio() : (empty($address->municipio) ? $userAddress->getMunicipio() : $address->getMunicipio());				
				$email     = $isAnony ? $address->getEmail() : $userProfile->getEmail();
				//$contacto = $isAnony ? $address->getContacto() : (empty($address->contacto) ? $userAddress->getContacto() : $address->getContacto());
				switch ($type){
					case eCommerce_SDO_Cart::ADDRESS_SHIP_TO:
						$first_name		= $isAnony ? $address->getFirstName() : (empty($address->first_name) ? $userProfile->getFirstName() : $address->getFirstName());						
						$last_name		= $isAnony ? $address->getLastName() : (empty($address->last_name) ? $userProfile->getLastName() : $address->getLastName());
						//$last_name2 	= $isAnony ? $address->getLastName2() : (empty($address->last_name2) ? $userProfile->getLastName2() : $address->getLastName2());
						$phone2 		= $isAnony ? $address->getPhone2() : $userAddress->getPhone2();
						$contact_method	= $isAnony ? $address->getContactMethod() : $userAddress->getContactMethod();	
						//$confirm_email	= $isAnony ? $address->getConfirmEmail() : '';
						break;
					case eCommerce_SDO_Cart::ADDRESS_BILL_TO:
						$razon_social	= $address->getRazonSocial();
						$rfc			= $address->getRfc();
						break;
				}
				//$nameArray = $type == eCommerce_SDO_Cart::ADDRESS_SHIP_TO ? 'ship' : 'bill';
				$nameArray = $type == eCommerce_SDO_Cart::ADDRESS_SHIP_TO ? 'ship' : 'bill';
				$fieldsTMP = $this->getParameter( $nameArray, false, array() );
				if( $save ){				
					//we try to get the params
					foreach($fieldsTMP as $key => $val)
						$fields[$key] = strip_tags($val);
						$street    = $fields['street'];
						$street2   = $fields['street2'];
						$street3   = $fields['street3'];
						$street4   = $fields['street4'];
						$city      = $fields['city'];
						$city      = empty($city)?'Puebla':$city;
						$state     = empty($fields['state'])?'Puebla':$fields['state'];
						$country   = empty($fields['country'])?'MX':$fields['country'];
						$zipCode   = $fields['zip_code'];
						$phone     = $fields['phone'];
						$municipio = $fields['municipio'];
						$contacto  = $fields['recibe'];
						//$email			= $fields['email'];						
						//$confirm_email	= $fields['confirm_email'];
					switch ($type){
						case eCommerce_SDO_Cart::ADDRESS_SHIP_TO:
							/*$first_name		= $fields['first_name'];
							$last_name		= $fields['last_name'];
							$last_name2 	= $fields['last_name2'];
							$phone2 		= $fields['phone2'];
							$contact_method	= $fields['contact_method'];*/
							$first_name		= empty($_REQUEST['nombre'])?$userProfile->getFirstName():$_REQUEST['nombre'];
							$last_name		= empty($_REQUEST['apellido'])?$userProfile->getLastName():$_REQUEST['apellido'];																		
							break;
						case eCommerce_SDO_Cart::ADDRESS_BILL_TO:
							$razon_social	= $fields['razon_social'];
							$rfc			= $fields['rfc'];
							break;
					}
				}
				try{
					$address->setStreet($street);
					$address->setStreet2($street2);
					$address->setStreet3($street3);
					$address->setStreet4($street4);
					$address->setCity($city);
					$address->setState($state);
					$address->setCountry( empty($country) ? 'MX' : $country );
					$address->setZipCode($zipCode);
					$address->setPhone($phone);
					$address->setEmail($email);
					//$address->setMunicipio($municipio);
					//$address->setContacto($contacto);
					//$address->setConfirmEmail($confirm_email);
					switch ($type){
						case eCommerce_SDO_Cart::ADDRESS_SHIP_TO:
							$address->setFirstName($first_name);
							$address->setLastName($last_name);
							$address->setLastName2($last_name2);
							$address->setPhone2($phone2);
							$address->setContactMethod($contact_method);
							break;
						case eCommerce_SDO_Cart::ADDRESS_BILL_TO:
							$address->setRazonSocial($razon_social);
							$address->SetRfc($rfc);
							break;
					}
					if($save){
						switch ($type){
							case eCommerce_SDO_Cart::ADDRESS_SHIP_TO:
								if(empty($address->email) || empty($address->street3) || empty($address->city)){
									throw new Exception ('Necesita llenar los datos de env&iacute;o');
								}
								$cart = eCommerce_SDO_Cart::SetShipTo( $address );	
								break;
							case eCommerce_SDO_Cart::ADDRESS_BILL_TO:
								$cart = eCommerce_SDO_Cart::SetBillTo( $address );								
								break;
						}
						$msj = $add_updtd[$lang];
						$this->tpl->assign( 'message', $msj );
						$this->tpl->assign( 'messageClass', 'msjOk');
						$result = true;
					}
					$this->tpl->assign( 'errors'.$type, new Validator_ErrorHandler( "") );
				}catch( eCommerce_SDO_Exception $e ){
					$e = $e->getNestedException();
					$this->tpl->assign( 'errors'.$type, new Validator_ErrorHandler( $err[$lang], $e->getErrors() ) );
					$this->tpl->assign( 'message', $err[$lang] );
					$this->tpl->assign( 'messageClass', 'msjErr');
				}
			}
		}catch (Exception $e){
			//header("Location: cart.php");
			$this->tpl->assign( 'errors', new Validator_ErrorHandler( $e->getMessage() ) );
			$this->tpl->assign( 'errors'.$type, new Validator_ErrorHandler( $e->getMessage() ) );
			$this->tpl->assign( 'message', $e->getMessage() );
			$this->tpl->assign( 'messageClass', 'msjErr');
		}
		$this->tpl->assign( $nameArray, $address);
		$this->tpl->assign('cart', $cart );
		$this->tpl->assign('isEmptyCart', $isEmptyCart );
		return $result;
	}
}
?>