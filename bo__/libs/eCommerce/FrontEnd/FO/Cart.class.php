<?php
class eCommerce_FrontEnd_FO_Cart extends eCommerce_FrontEnd_FO {

	/**
	 * @var eCommerce_DAO_Event
	 */
	public function __construct(){
		parent::__construct();
	}

	public function execute(){
		
		$cmd = $this->getCommand();
		switch( $cmd ){
			case 'add':
				$this->_addProduct();
			break;
			case 'remove':
				$this->_removeProduct();
			break;
			case 'update':
				$this->_updateCart();
			break;
			case 'getTotalCart':
				$total = eCommerce_SDO_Cart::GetTotalCart();
				echo $total['price']['format'];				
			break;
			case 'getTotalItemsCount':
				$total = eCommerce_SDO_Cart::GetTotalItemsCount();
				//$total = eCommerce_SDO_Cart::GetTotalItems();
				echo $total;
			break;
			case 'getTotalItems':
				$total = eCommerce_SDO_Cart::GetTotalItems();
				echo $total;
			break;
			case 'getHTMLCart': $this->_getHTMLMiniCart();
				break;
			case 'agregaCupon':
				$this->_agregaCupon();
				break;
			case 'show':
			default:
				$this->_show();
				break;
		}
		
	}

	public function _agregaCupon(){
		$cupon   = $_POST["cupon"];
		$cupones = eCommerce_SDO_Core_Application_cupondescuento::LoadByCuponName($cupon);

		$descuento = explode('%',$cupones->getDescuento());
		$descuento = $descuento[0] / 100 ;

		if($cupones->getCupon() === strtoupper($_POST['cupon'])){
			eCommerce_FrontEnd_Util_Session::Set('cupon_descuento', 'true');
			eCommerce_FrontEnd_Util_Session::Set('valor_descuento', $descuento);
			$_SESSION['cuponMsjSuccess'] = "El código del cupón se agrego correctamente";
			$url_login = ABS_HTTP_URL.'purchase.php';
			$this->redirect($url_login,false);
		}else{
			eCommerce_FrontEnd_Util_Session::Delete('cupon_descuento');
			$_SESSION['payuError'] = "El código del cupón es incorrecto";
			$url_login = ABS_HTTP_URL.'purchase.php';
			$this->redirect($url_login,false);
		}
		die();
	}
	
	public function _getHTMLMiniCart(){
		$cart   = eCommerce_SDO_Cart::GetCart();
		$showExtrasAndTotal = true;
		ob_start();?>
		<div id="minicartScroll" class="minicart">
			<table class="shopping-cart-table">
				<tbody class="shopping-cart-tbody">
				<?php
					$itemsR = $cart->getItems();		
					$total_quantity = 0;
					$actualCurrency = eCommerce_SDO_CurrencyManager::GetActualCurrency();
					foreach ($itemsR as $itemr) {
						$total_quantity += $itemr->quantity;
					}

					if( count( $itemsR ) > 0 ){
						$OUTPUT["extras"] = array();
						if( $showExtrasAndTotal ){
							$TotalFT = eCommerce_SDO_Cart::GetTotalCart(true,false);
							$total   = $TotalFT['price']['amount'];
							$extras  = eCommerce_SDO_Cart::GetExtras($total_quantity);
							//calculate the cost of the extras
							foreach( $extras as $fee){
								$OUTPUT["extras"][] = array(
									"description"=>$fee->getDescription(), 
									"str_amount"=>eCommerce_SDO_CurrencyManager::NumberFormat( $fee->getAmount() ) . " " . $actualCurrency
								);
							}
							$extrasTotal         = eCommerce_SDO_Cart::GetExtrasTotal($total_quantity);
							$OUTPUT["str_total"] = eCommerce_SDO_CurrencyManager::NumberFormat( ($subTotal+$extrasTotal) ) . " " . $actualCurrency;
						}//end if

						$ArrProductsR = array();
						foreach( $itemsR as $itemR ){
							$productId    = $itemR->getProductId();
							//$productId    = $itemR->getVersionId();
							$ArrProductsR = array();
							//$productR     = eCommerce_SDO_Catalog::LoadProduct( $productId );
							/**  Solo versiones **/
							$productR     = eCommerce_SDO_Catalog::LoadProduct( $itemR->getProductId() );
							$versionR     = eCommerce_SDO_Catalog::LoadVersion( $itemR->versionId);				
							$tallaR       = eCommerce_SDO_Catalog::GetVersionTalla( $itemR->versionId);
							#-- Imagenes galeria
							$galeriaId = $itemR->getGaleriaId();
							if(!empty($galeriaId)){
								$imagen = eCommerce_SDO_Galeria::LoadGaleria($galeriaId);
								$src_imagen = $imagen->getUrlArrayImages("small",0);
							}//end if

							#-- Imagenes files
							$file_id = $itemR->getFileId();
							if(!empty($file_id)){
								$imagen = eCommerce_SDO_Files::LoadFiles($file_id);
								$src_imagen = ABS_HTTP_URL."bo/backoffice/server/php/files/".$imagen->getName();
							}elseif(empty($file_id) && empty($galeriaId)){
								$src_imagen = $productR->getUrlImageId('small',0);
							}//end if

							$ArrProductsR["id"]                      = $itemR->productId;
							$ArrProductsR["image_url"]               = $src_imagen;
							$ArrProductsR["codigo"]                  = $productR->getSku();
							$ArrProductsR["name"]                    = $productR->getName();
							$ArrProductsR["quantity"]                = $itemR->quantity;
							//Solo Versiones
							$ArrProductsR["color"]                   = $itemR->color;
							$ArrProductsR["talla"]                   = $talla[0]['talla_id'];
							$ArrProductsR["version_id"]              = $itemR->versionId;
							$ArrProductsR["version_name"]            = $versionR->getName();
							$ArrProductsR["version_sku"]             = $versionR->getSku();
							$ArrProductsR["version_price"]           = $versionR->getPrice();							
							/*$ArrProductsR["version_name"]            = $productR->getName();*/
							/*$ArrProductsR["version_sku"]             = $productR->getSku();
							$ArrProductsR["version_price"]           = $productR->getPrice();*/
							$ArrProductsR['peso']                    = $productR->getWeight();
							$ArrProductsR['str_peso']                = $productR->getWeight(true);
							/*$value                                   = eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $versionR->getPrice(), $versionR->getCurrency() );*/
							/*$ArrProductsR["str_price"]               = eCommerce_SDO_CurrencyManager::NumberFormat( $value) . " " . $actualCurrency ;				
							$value                                   = eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $versionR->getRealPrice(), $versionR->getCurrency() );				
							$ArrProductsR["str_price_with_discount"] = eCommerce_SDO_CurrencyManager::NumberFormat($value) . " " . $actualCurrency;
							$value                                   = eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $versionR->getRealPrice(), $versionR->getCurrency() );*/
							$value                                 = eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $itemR->getPrice() + $itemR->getDiscount(), $versionR->getCurrency() );
							$ArrProduct["str_price"]               = eCommerce_SDO_CurrencyManager::NumberFormat( $value) . " " . $actualCurrency ;
							$value                                 = eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $itemR->getDiscount() * $itemR->getQuantity(), $versionR->getCurrency() );
							$ArrProduct["str_price_with_discount"] = eCommerce_SDO_CurrencyManager::NumberFormat($value) . " " . $actualCurrency;
							$value                                 = eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $itemR->getPrice(), $versionR->getCurrency() );

							$peso                                    = $ArrProductsR['peso'];
							$uv                                      = $ArrProductsR['unidad_venta'];
							$uvc                                     = $ArrProductsR['uvc'];				
							$quantity                                = eCommerce_SDO_Cart::getRealQuantity($itemR->quantity,$peso,$uv,$uvc);
							$productTotal                            = $value * $quantity;				
							$ArrProductsR["str_total_price"]         = eCommerce_SDO_CurrencyManager::NumberFormat($productTotal) . " " . $actualCurrency;				
							$subTotal                                += $productTotal;
							$ArrProducts[]                           = $ArrProductsR;			
							$hrefBorrarR                             =  ABS_HTTP_URL."cart.php?cmd=remove&productId=".$ArrProductsR["id"];

							$OUTPUT["str_subtotal"] = eCommerce_SDO_CurrencyManager::NumberFormat($subTotal) . " " . $actualCurrency;
							?>
							<tr>
								<td class="prod-act"><a  href="<?=$hrefBorrarR?>" class="fa fa-trash-o"></a></td>
								<td class="prod-img"><img src="<?=$ArrProductsR["image_url"]?>" alt="<?=$ArrProductsR["version_name"]?>"></td>
								<td class="prod-des"><?=$ArrProductsR["version_name"]?></td>
								<td class="prod-sku"><?=$ArrProductsR["version_sku"]?></td>
								<td class="prod-qua"><?=$ArrProductsR["quantity"]?></td>
								<td class="prod-pri"><?=$ArrProductsR["str_price"]?></td>
								<td class="prod-dis"><?=$ArrProductsR["str_price_with_discount"]?></td>
								<td class="prod-sub"><?=$ArrProductsR["str_total_price"]?></td>
							</tr>

							<?php 
						}//end foreach
					}//end
				?>
				</tbody>
				<tfoot class="shopping-cart-tfoot" >
					<tr>
						<td colspan="5"><small>El Subtotal no incluye el envío. El costo del envío será confirmado una vez que continúes con la compra.</small></td>
						<td colspan="2" class="shopping-cart-subtotal">
							<span>Subtotal</span> <strong><?=$OUTPUT["str_subtotal"];?></strong>
						</td>
					</tr>

					<tr>
						<td colspan="5"></td>
						<?php 
						foreach ($OUTPUT["extras"] as $extras) {
						?>
						<td colspan="2" class="shopping-cart-shipping">
							<span><?=$extras['description'];?></span> <strong><?=$extras['str_amount'];?></strong>
						</td>
						<? }?>
					</tr>
					<tr>
						<?php 
						$total          = eCommerce_SDO_Cart::GetTotalCart(false, false);
						$totalf         = $total['price']['amount'];
						$actualCurrency = eCommerce_SDO_CurrencyManager::GetActualCurrency();
						$totalextras    = explode('$',$OUTPUT["extras"][0]['str_amount']);
						$totalf         = eCommerce_SDO_CurrencyManager::NumberFormat($totalf + $totalextras[1]) . " " . $actualCurrency;
						?>
						<td colspan="5"></td>
						<td colspan="2" class="shopping-cart-total">
							<span>Total</span> <strong><?php echo $totalf;?></strong>
						</td>
					</tr>

					<!--<tr class="shopping-cart-actions">
						<td colspan="7">
							<a href="index.php?anchor=section-ecommerce" class="button-flat secondary">Seguir comprando</a>
							<a href="cart.php" class="button-flat primary">Comprar</a>
						</td>
					</tr>-->
				</tfoot>
			</table>
			<div class="button-wrapper">
				<a href="<?=ABS_HTTP_URL?>cart.php" title="Carrito de Compras" class="button-flat secondary">Ir al carrito de compras</a>
			</div>
		</div>
		<?php
		$html = ob_get_contents();
		ob_end_clean();
		echo $html;
	}

	/*public function _getHTMLCart(){
		$cart = eCommerce_SDO_Cart::GetCart();
		$total = eCommerce_SDO_Cart::GetTotalCart(true, false);
		$totalf = $total['price']['format'];
		ob_start();?>
		<div class="portal-minicart-ref">
		    <div class="v2-vtexsc-cart vtexsc-cart mouseActivated preLoaded">
		      <div class="vtexsc-bt"></div>
		      <div class="vtexsc-center">
		        <div class="thead">
		            <div class="cartSkuName" style="width:20%">&nbsp;</div>
		            <div class="cartSkuName" style="width:30%">Producto</div>
		            <div class="cartSkuPrice" style="width:17%">Precio</div>
		            <div class="cartSkuQuantity" style="width:15%">Cantidad</div>
		            <div class="cartSkuActions" style="width:10%; float:right">Eliminar</div>
		        </div>
		        <div id="divscroll">
		        	<table width="100%" height="100%" border="0">
						<tbody>
				<?php
				$items = $cart->getItems();
				if( count( $items ) > 0 ){
					$OUTPUT["extras"] = array();
					if( $showExtrasAndTotal ){				
						$TotalFT = self::GetTotalCart(true,false);				
						$total = $TotalFT['price']['amount'];				
						$extras = eCommerce_SDO_Cart::GetExtras($total);								
						//calculate the cost of the extras
						foreach( $extras as $fee){
							$OUTPUT["extras"][] = array("description"=>$fee->getDescription(), "str_amount"=>eCommerce_SDO_CurrencyManager::NumberFormat( $fee->getAmount() ) . " " . $actualCurrency);
						}				
						$extrasTotal = eCommerce_SDO_Cart::GetExtrasTotal($total);				
						$OUTPUT["str_total"] = eCommerce_SDO_CurrencyManager::NumberFormat( ($subTotal+$extrasTotal) ) . " " . $actualCurrency;
					}
					$ArrProducts = array();
					$total      = eCommerce_SDO_Cart::GetTotalCart(false, false);
				
					foreach( $items as $item ){
						$ArrProduct = array();
						$product = eCommerce_SDO_Catalog::LoadProduct( $item->getProductId() );
						$version = eCommerce_SDO_Catalog::LoadVersion( $item->versionId);
						$talla = eCommerce_SDO_Catalog::GetVersionTalla( $item->versionId);
						$ArrProduct["id"] = $item->productId;								
						$ArrProduct["image_url"] = $product->getUrlImageId('small',0);		
						$ArrProduct["codigo"] 		= $product->getSku();
						$ArrProduct["name"] 		= $product->getName();
						$ArrProduct["quantity"] 	= $item->quantity;
						$ArrProduct["color"] 		= $item->color;
						$ArrProduct["talla"]		= $talla[0]['talla_id'];
						$ArrProduct["version_id"] 	= $item->versionId;
						$ArrProduct["version_name"] = $version->getName();
						$ArrProduct["version_sku"] = $version->getSku();
						$ArrProduct["version_price"] = $version->getPrice();												
						$ArrProduct['peso']			= $product->getWeight();
						$ArrProduct['str_peso']			= $product->getWeight(true);
						$value = eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $version->getPrice(), $version->getCurrency() );				
						$ArrProduct["str_price"] = eCommerce_SDO_CurrencyManager::NumberFormat( $value) . " " . $actualCurrency ;				
						$value = eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $version->getRealPrice(), $version->getCurrency() );				
						$ArrProduct["str_price_with_discount"] = eCommerce_SDO_CurrencyManager::NumberFormat($value) . " " . $actualCurrency;
						$value = eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $version->getRealPrice(), $version->getCurrency() );				
						$peso = $ArrProduct['peso'];
						$uv = $ArrProduct['unidad_venta'];
						$uvc = $ArrProduct['uvc'];				
						$quantity = eCommerce_SDO_Cart::getRealQuantity($item->quantity,$peso,$uv,$uvc);
						$productTotal = $value * $quantity;				
						$ArrProduct["str_total_price"] = eCommerce_SDO_CurrencyManager::NumberFormat($productTotal) . " " . $actualCurrency;				
						$subTotal += $productTotal;
						$ArrProducts[] = $ArrProduct;			
						$hrefBorrar =  ABS_HTTP_URL."cart.php?cmd=remove&productId=".$ArrProduct["id"];
					?>
					<tr>
						<td class="cartSkuImage">
							<a  class="sku-imagem"><img width="71" height="71" src="<?=$ArrProduct["image_url"]?>" alt="<?=$ArrProduct["version_name"]?>"></a>
						</td>
						<td class="cartSkuName">
							<!--<a ><?//=$ArrProductsR["name"]?></a>
							<p class="availability"></p>-->
							<span style="font-size: 13px; font-family: Arial;"><?=$ArrProduct["version_name"]?></span>
						</td>
						<td class="cartSkuPrice">
							<div class="cartSkuUnitPrice">
								<!--<span class="bestPrice"><?//=$ArrProductsR["str_price"]?></span>-->
								<span class="bestPrice" style="font-size: 14px; font-family: Arial;"><?=$ArrProduct["str_price"]?></span>
							</div>
						</td>
						<td class="cartSkuQuantity">
							<div class="cartSkuQtt">
								<span class="cartSkuQttTxt vtexsc-skuQtt" style="font-size: 14px; font-family: Arial;"><?=$ArrProduct["quantity"]?></span>
								<!--<span class="cartSkuQttTxt"><span class="vtexsc-skuQtt"><?//=$ArrProductsR["quantity"]?></span></span>-->
							</div>
						</td>
						<td class="cartSkuActions" style="font-size: 14px; font-family: Arial;">
							<span data-index="0" class="cartSkuRemove" style="float:right">
								<a class="text" href="<?=$hrefBorrar?>">X</a>
							</span>
							<div class="vtexsc-overlay"></div>
						</td>
					</tr>
				<?php 
							}
				}?>
				</tbody>
			</table>
	        </div>
	        <div class="cartFooter clearfix" style="font-family: Arial; font-size: 15px;">
          <table cellspacing="0" cellpadding="0" width="100%">
            <tr>
             <?php 
			 
              foreach ($OUTPUT["extras"] as $extras) {
              ?>
              <td width="30%" style="text-align: left;"><?=$extras['description'];?></td>
              <td width="70%" style="text-align: left;"><?=$extras['str_amount'].' MXN';?></td>
              <?php
                //echo '<div class="cartTotal">'.$extras['description'].'<span class="vtexsc-totalCart"><span class="vtexsc-text"> '.$extras['str_amount'].' MXN</span></span></div>';
              }
            ?>
            </tr>
            <tr>
              <?php 
              	$actualCurrency = eCommerce_SDO_CurrencyManager::GetActualCurrency();
                $totalextras = explode('$',$OUTPUT["extras"][0]['str_amount']);
                $totalf     = eCommerce_SDO_CurrencyManager::NumberFormat($total['price']['amount'] + $totalextras[1]) . " " . $actualCurrency;
               ?>
              <td width="30%" style="text-align: left;">Total</td>
              <td width="70%" style="text-align: left;"><?php echo $totalf;?></td>
            </tr>            
          </table>         
           </div>
      </div>
      <div class="vtexsc-bb"></div>
    </div>
  </div>
	       <div id="go_to_cart_wrapper">
		    <a href="<?=ABS_HTTP_URL?>cart.php" title="Carrito de Compras" style="text-transform:lowercase; color: #FFF; background-color: #77BC1F;" class="btn">
		      <span style="font-family: Arial; font-size: 17px;">Ir al carrito de compras</span>
		    </a>
		  </div>	       	       	       
		<?
		$html = ob_get_contents();
		ob_end_clean();
		echo $html;		
	}*/

	protected function _addProduct(){
		$versionId      = $this->getParameter("version_id",true,0);
		$quantity       = $this->getParameter("quantity", true, 1);
		$nom_material   = $this->getParameter("nom_material", false, NULL);
		$version_nombre = $this->getParameter("version_nombre", false, NULL);
		$tipo_marco     = $this->getParameter("tipo_marco", false, NULL);
		$color_marco    = $this->getParameter("color_marco", false, NULL);
		$bastidor_id    = $this->getParameter("bastidor_id", false, NULL);
		$galeria_id     = $this->getParameter("galeria_id", false, NULL);
		$file_id        = $this->getParameter("file_id", false, NULL);
		$c_ancho        = $this->getParameter("c_ancho", false, NULL);
		$c_alto         = $this->getParameter("c_alto", false, NULL);
		$precio_total   = $this->getParameter("precio_total", false, NULL);
		$descuento      = $this->getParameter("descuento", false, NULL);

		#-- Productos promocionales
		$tipo_producto = $this->getParameter("tipo_producto", false, NULL);
		$acabado_id    = $this->getParameter("acabado_id", false, NULL);
		$afinado_id    = $this->getParameter("afinado_id", false, NULL);
		$type_product  = $this->getParameter("type_product", false, NULL); //Estrcutura - Estrucuta e imagen

		$version    = eCommerce_SDO_Version::LoadVersion($versionId);
		if(is_null($tipo_producto)){
			$bastidor   = $bastidor_id == 'true' ? 'Si' : 'No';	
			if(!is_null($c_ancho) && !is_null($c_alto)){
				$medidas = ", Ancho : ".$c_ancho.", Alto : ".$c_alto;
			}else{
				$medidas = "";
			}//end if
			/*********************************/
			if($tipo_marco == 'S'){//sin marco
				$marco = ", Sin Marco ";
			}else{//con marco
				$marco = ", Tipo de Marco : ".$tipo_marco.", Color de Marco : ".$color_marco." ";
			}
			/*********************************/
			if($nom_material == 'Tela'){
				$tipo_bastidor = 'Bastidor de madera/aluminio';
			}elseif($nom_material == 'Vinil'){
				$tipo_bastidor = 'Bastidor de aluminio/tela';
			}elseif($nom_material == 'Acrilico'){
				$tipo_bastidor = 'Bastidor de aluminio';
			}
			/*********************************/
			$attributes = 'Material : '.$nom_material.", Tamaño : ".$version_nombre.$medidas.$marco.", ".$tipo_bastidor." : ".$bastidor;
		}elseif($tipo_producto == "promocionales"){
			/*********************************/
			$acabado     = $acabado_id == 'true' ? 'Acabado : Resolución Profesional' : 'Acabado: No';
			$afinado     = $afinado_id == 'profesional' ? "Afinado: Estructura Profesional" : "Afinado: Estructura Económica";
			if($type_product == 'estructura_impresion'){
				$attributes  = 'Material : '.$nom_material.", Tamaño : ".$version_nombre.", ".$acabado.", ".$afinado;
			}elseif($type_product == "estructura"){
				$attributes = "Estructura - Tamaño : ".$version_nombre.", ".$afinado;
			}else{
				$attributes  = 'Material : '.$nom_material.", Tamaño : ".$version_nombre.", ".$acabado.", ".$afinado;
			}
			/*********************************/
			$color_marco = "";
		}

		// Agrega Producto al carrito de compra
		$result         = eCommerce_SDO_Cart::addItem($version->getProductId(),$quantity,$version->getVersionId(),$color_marco,$version_nombre,$attributes,$galeria_id,$file_id,$bastidor_id,$acabado_id,$afinado_id,$nom_material,$precio_total,$descuento,$c_ancho,$c_alto);
		$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
		if( $result ){
			
			$message = "Carrito de compras";
			/*if($actualLanguage == 'EN')
				{
					$message = 'The product that you selected has been added to the cart';
				}
				else
				{
					$message = 'El producto seleccionado ha sido a&ntilde;adido al carrito';
				}*/
			$this->tpl->assign( 'messageClass', 'msjOk');
		}
		else{
			if($actualLanguage == 'EN')
			{
				$message = 'Try Again Please';
			}
			else
			{
				$message = 'Por Favor, intente nuevamente';
			}
			$this->tpl->assign( 'messageClass', 'msjErr');
		}
		
		$cart = eCommerce_SDO_Cart::GetCart();
			
		$this->tpl->assignRef( 'message', $message);
		
		$this->_show();
		
	}
	
	public function _addProductXajax($idCategory,$idProduct,$quantity,$color,$talla){
		$productId = $idProduct;
		/*$productId 	= $this->getParameter("productId",false,2);
		$productId 	= $productId."|".$talla."|".$color;*/
		
		$result 	= eCommerce_SDO_Cart::addItem( $productId, $quantity , $color, $talla);
		
		/*$cart 		= eCommerce_SDO_Cart::GetCart();
		$items 		= $cart->getItems();*/
		
		/*$totalqty	= 0;
		//$subtotal	= 0;
		foreach($items as $item){
			$product	 = eCommerce_SDO_Catalog::LoadProduct($productId);
			$totalqty 	+= $item->getQuantity();
			//subtotal	+= $product->getPrice() * $item->getQuantity();
		}*/
		
		$product	= eCommerce_SDO_Catalog::LoadProduct($productId);
		//$color = eCommerce_SDO_Color::LoadColorByColor($color);
		
		/*$this->tpl->assign('talla',$talla);
		$this->tpl->assign('color',$color);
		$this->tpl->assign('qty',$quantity);*/
		/*$this->tpl->assign('precio',"$ " . number_format($product->getPrice(),2) . " " . eCommerce_SDO_CurrencyManager::GetActualCurrency());*/
		//$this->tpl->assign('nombre',"des de aqui");
		//$this->tpl->assign('sku',"desde el metohod ");
		$this->tpl->assign('nombre',$product->getName());
		$this->tpl->assign('sku',$product->getSku());
		//$this->tpl->assign('totalqty',$totalqty);
		//$this->tpl->assign('subtotal',"$ " . number_format($subtotal,2) . " " . eCommerce_SDO_CurrencyManager::GetActualCurrency());
		
		return $this->_showXajax();
		
	}
	
	
	protected function _showXajax(){
		$cart = eCommerce_SDO_Cart::GetCart();
		$this->tpl->assign( 'cart', $cart);
		ob_start();
		$this->tpl->display("CartMainXajax.php");
		$html = ob_get_contents();
		ob_end_clean();
		return $html;
	}
	
	
	public function _showMenuXajax(){
		ob_start();
		$this->tpl->display("CartMenuXajax.php");
		$html = ob_get_contents();
		ob_end_clean();
		return $html;
	}
	
	protected function _removeProduct(){
		
		$productId = $this->getParameter("productId",false,null);
		
		$result = eCommerce_SDO_Cart::RemoveItem( $productId);
		$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
		if( $result ){
			if($actualLanguage == 'EN')
				{
					$message = 'The product that you selected has been removed from the cart';
				}
				else
				{
					$message = 'El producto seleccionado ha sido eliminado del carrito';
				}
			$this->tpl->assign( 'messageClass', 'msjOk');
		}
		else{
			if($actualLanguage == 'EN')
			{
				$message = 'Try Again Please';
			}
			else
			{
				$message = 'Por Favor, intente nuevamente';
			}
			$this->tpl->assign( 'messageClass', 'msjErr');
			$this->tpl->assign( 'messageClass', 'msjErr');
			
		}
		$this->tpl->assignRef( 'message', $message);
		
		$this->_show();
		
	}
	
	protected function _updateCart(){
		$productId      = $this->getParameter("productId",false,null);
		$newQuantity    = $this->getParameter( 'quantity_' . $productId );

		#-- Validación para saber que imagen se esta guardando ya sea la que subio el cliente o la de BD
		$cart  = eCommerce_SDO_Cart::GetCart();
		$items = $cart->getItems();
		foreach ($items as $item) {
			$id = $item->getProductId();
			if($id == $productId){
				$attributes = $item->getAttributes();
				$galeria_id = $item->getGaleriaId();
				$file_id    = $item->getfileId();
			}//end if
		}//end foreach

		$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();

		try{

			eCommerce_SDO_Cart::UpdateQuantity( $productId, $newQuantity,$attributes,$galeria_id,$file_id);
			$product = eCommerce_SDO_Catalog::LoadProduct( $productId );
		
			if($actualLanguage == 'EN')
				{
					$msg1 = "Product: " . $product->getName() . " has been updated";
				}
				else
				{
					$msg1 = "El producto " . $product->getName() . " ha sido actualizado";
				}
			$this->tpl->assign('message', $msg1);
			$this->tpl->assign( 'messageClass', 'msjOk');

		}catch( eCommerce_SDO_Exception $e ){
			$item 	= eCommerce_SDO_Core_Application_Cart::GetCart()->getItem( $productId );
			$uvc 	= $item->getUnidadVentaCliente();
			$peso	= eCommerce_SDO_Catalog::LoadProduct($productId)->getWeight(true);
			if($uvc == "Peso"){
				if($actualLanguage == 'EN')$msg2 = "Please enter a valid value of product or quantity, the minimum value must be $peso";
				else $msg2 = "Por favor ingrese un valor v&aacute;lido para el producto o cantidad, el valor m&iacute;nimo debe ser $peso";
			}
			else{
				if($actualLanguage == 'EN')$msg2 = "Please enter a valid value of product or quantity";
				else $msg2 = "Por favor ingrese un valor v&aacute;lido para el producto o cantidad";
			}
			
			$this->tpl->assign('message', $msg2 );
			$this->tpl->assign( 'messageClass', 'msjErr');
		}
		
		$this->_show();
		
	}
	
	/*protected function _show(){
		$cart = eCommerce_SDO_Cart::GetCart();
		$this->tpl->assign( 'cart', $cart);
		$this->tpl->display("CartMain.php");
	}*/
	protected function _show($inAjax = false){
		$inAjax = $inAjax ? $inAjax : $this->getParameter('inAjax',false,false);
		$cart   = eCommerce_SDO_Cart::GetCart();
		$this->tpl->assign( 'cart', $cart);
		$total = eCommerce_SDO_Cart::GetTotalCart(true, false);
		$this->tpl->display($inAjax ? 'AjaxCartMain.php' : "CartMain.php");
	}
}
?>