<?php

$category = $this->category;
$product = $this->product;
$catNum = $this->catNum;
$artID = $this->artID;

$extraParamsLanguage = '?&productDetails=1&id=' . $product->getProductId();

require_once("header.php");


?>


<div style="width:100%; text-align:left; padding-left:5px;padding-top:15px; font-family:Verdana, Arial, Helvetica, sans-serif">
<?php echo $category->getName(); ?>
</div>

<div style="width:100%; padding-left:10px; padding-top:10px;">
	
<!-- Product -->
  <table width="700" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td width="310" rowspan="2" align="center" valign="middle">
    <?php
    	$imgId = $product->getImageId();
		
    	if( $imgId > 0 ){
    		$urlImage =  'file.php?id=' . $imgId;
    		//$imageMini = $urlImage . '&type=image&img_size=predefined&width=310&height=61';
			$imageMini = $urlImage . '&type=image&img_size=predefined&width=310';
    	} 
    	else{
    		$urlImage = $imageMini = 'ima/fo/default.jpg';
    	}

		echo '<img src="' . $imageMini .'" alt="img" width="218" border="0" />';
		
	?>
      <p align="left" style="margin-left: 5px; margin-right: 20px; margin-top: 5px">&nbsp;</p>
      </td>
      <td valign="top">
        <p><span class='detailsName' style="font-size:13px;"><?php echo $product->getName(); ?></span><br />
              <span class='defaultText'><?php echo $product->getSku(); ?></span><br />
          <br />
          
          <?php 
      	if( $product->getStatus() == eCommerce_SDO_Core_Application_Product::NOT_AVAILABLE){
      		echo "<span class='defaultText' align='center'><h2>The product is not available</h2></span> <br />";
      	}
      ?>
              <span class='defaultText'><?php echo $product->getDescriptionShort(); ?></span><br />
          <br />
              <span class='defaultText'><?php echo $product->getDescriptionLong(); ?></span><br />
          <br />
          	<span class='defaultText'><?php echo $product->getDimensions(); ?></span><br />
          <br />
              <span class='defaultTextCurrency'>
              <?php echo eCommerce_SDO_CurrencyManager::NumberFormat(eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $product->getRealPrice(), $product->getCurrency() ) ) . ' ' . eCommerce_SDO_CurrencyManager::GetActualCurrency(); 
			  //$product->getCurrency()
			  ?></span><br />
          <span style="margin-left: 0px; margin-right: 20px; margin-top: 5px; color:#A3A3A3; font-weight:700; font-size:8pt; font-family:Arial, Helvetica, sans-serif">
          <?php if ($catNum == 21){ ?>
          <a href="artist.php?cmd=details&amp;id=<?php echo ($artID); ?>" class="defaultText" style="text-decoration:none">Artist's information</a>
          <?php } ?>
          </span><br />
          </p>
      </td>
    </tr>
    <tr>
      <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td colspan="3"><img src="ima/fo/3px.gif" alt="img" width="3" height="3" border="0" /></td>
          </tr>
          <tr>
            <td width="106"><p style="margin-top: 5px"> 
            <a href='<?php echo $urlImage; ?>'><img src="ima/fo/enlargeimage.gif" alt="img" width="105" height="20" border="0" /></a></p></td>
            <td valign="top" height="60"><p style="margin-top: 5px;"></p></td>
            <td><p style="margin-top: 5px;">
			<a href='cart.php?cmd=add&productId=<?php echo $product->getProductId(); ?>&quantity=1'><img src="imas/interiores/buynow-bot.gif" alt="img" width="106" height="20" border="0" /></a>
			
			</p></td>
          </tr>
		  <tr>
		  <td colspan="3">
		  			<?php 
				$isSculpture = eCommerce_SDO_Catalog::IsProductASculpture($product->getProductId());
				if($isSculpture){
					$sculptor = eCommerce_SDO_Artist::GetArtistByProductAndType($product->getProductId(), eCommerce_SDO_Artist::TYPE_SCULPTOR );
					$sculptorId = $sculptor->getArtistId();
					if( !empty( $sculptorId) ){
						$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
						$viewSculptor ='Sculptor ';

						if($actualLanguage == eCommerce_SDO_LanguageManager::LANGUAGE_SPANISH){
							$viewSculptor ='Escultor ';
						}

						$viewSculptor .= $sculptor->getName();
						echo '<br /><br /><a href="artist.php?cmd=details&id='.$sculptorId.'" style="text-decoration:none">'.$viewSculptor.'</a>';
					}
					//'<a href="cart.php?cmd=add&productId='.$product->getProductId().'&quantity=1"><img src="imas/interiores/buynow-bot.gif" alt="img" width="106" height="20" border="0" /></a>';
				}
			?>
		  </td>
		  </tr>
      </table></td>
    </tr>
  </table>

<!-- Product -->  
</div>

<?php
include_once("footer.php");
?>