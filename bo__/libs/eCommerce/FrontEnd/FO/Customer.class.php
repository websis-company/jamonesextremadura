<?php

class eCommerce_FrontEnd_FO_Customer extends eCommerce_FrontEnd_FO{
	const CMD_NOT_AUTHENTIFICATION = 'register,saveRegister,saveRegisterWithAddress';
	/**
	 * @var eCommerce_DAO_Event
	 */
	public function __construct(){
		parent::__construct();
	}

	public function execute(){
		$cmd = $this->getCommand();
		//verify if the comand needs authentification
		$cmdNotAuthentification = split(',', self::CMD_NOT_AUTHENTIFICATION ); 
		if( !in_array( $cmd, $cmdNotAuthentification) ){
			eCommerce_FrontEnd_FO_Authentification::verifyAuthentification();
		}		
		switch( $cmd ){
			case 'register':
				//$this->_userRegister();
				$this->_userRegisterWithAddress();
			break;
			
			case 'saveRegister':
				$this->_saveRegister();
			break;
				
			case 'saveRegisterWithAddress':
				$this->_saveRegisterWithAddress();
			break;

			case 'save':
				$this->_save();
			break;
			
			case 'edit':
				$this->_edit();
			break;
			
			case 'saveAddress':
				$this->_saveAddress();
				break;
				
			case 'editAddress':
				$this->_editAddress();
			
			case 'listOrders':
				$this->_listOrders();
			break;
			
			case 'viewOrder':
				$this->_viewOrder();
				break;
			case 'confirmPayment':
				$this->_confirmPayment();
				break;
			case 'sendformpago':
				$this->_sendformpago();
				break;
			case 'rastreo':
				$this->_rastreo();
				break;
			case 'factura':
					$this->_factura();
					break;
			default:
			case 'info':
				$this->_info();
			break;
		}
	}
	
	protected function _sendformpago(){		
		$VARIABLE_POST	= "entity";
		$entity = $_POST[$VARIABLE_POST];
		if(!empty($entity['operacion_realizada']) && !empty($entity['numero_confirmacion']) && !empty($entity['numero_pedido']) && !empty($entity['monto'])){
			$errores = '<div class="alert alert-success" role="alert">Tu informaci&oacute;n se envio correctamente, en breve nos comunicaremos contigo, gracias.</div>';
			$htmlTmp .= '<table width="100%" >';
			foreach($entity as $campo => $value){				
				$campo = str_replace('_',' ',$campo);
				$htmlTmp .= '<tr>
						<th width="170" align="right" style="color:#000000;font-weight:bold;">
							<b>'.ucfirst($campo).':</b>
						</th>
						<td align="left"  style="color:#000000">'.nl2br(utf8_decode($value)).'</td></tr>';
			}
			$htmlTmp .= '</table>';
			$imagen = '<tr><td colspan="2" style="border-bottom: solid 2px #B2B2B2;background-color:#FFFFFF;"><img src="'.ABS_HTTP_URL.'bo/backoffice/img/pleca.jpg"></td></tr>';
			$html = '
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
				<head>
					<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
					<title>Untitled Document</title>
					<style type="text/css">
					<!--
						body {
							font-family:Arial,Helvetica,sans-serif;
							font-size:12px;
						}
					-->
					</style>
				</head>
				<body>
					<div style="margin:0 auto;">
						<table width="700">
						'.$imagen;										
						$html .= '<tr><td align="left" colspan="2">'.$htmlTmp.'</td></tr>';									
					$html .= '
			</table>
						    </td>
						</tr>
					</table>
				</body>
			</html>
			';
			$config = Config::getGlobalConfiguration();
			$header = "MIME-Version: 1.0\r\n"; //para el env�o en formato HTML;
			$header .= "Content-type: text/html; charset=iso-8859-1\r\n";
			$header .= "From: {$config["project_name"]} <{$config["project_email"]}>\r\n";
			$subject = "Confirmaci�n de Pago";
			mail($config["project_email"],$subject,$html,$header);
			$user = eCommerce_FrontEnd_Util_UserSession::GetIdentity();			
		}else{
			$errores = '<div class="alert alert-danger" role="alert">Error todos los campos son requeridos</div>';
		}
		$address = eCommerce_SDO_User::LoadUserAddress( $user->getProfileId() );
		$this->tpl->assign( 'address', $address );
		$this->tpl->assign( "user", $user);
		$this->tpl->assign( "error", $errores);
		$this->tpl->display( "UserConfirmpayment.php" );
	}
	
	protected function _userRegister(){
		$add1 = $this->tpl->trans('there_are_errors');
		$add2 = $this->tpl->trans('create').' '.$this->tpl->trans('account');
		$add3 = $this->tpl->trans('register');

		$user = (!empty($this->tpl->user) ) ? $this->tpl->user : new eCommerce_Entity_User_Profile();
		$errors = (!empty($this->tpl->errors) ) ? $this->tpl->errors : new Validator_ErrorHandler();
		
		$this->tpl->assign('user', $user);
		$this->tpl->assign('errors', $errors);
		
		$this->tpl->assign( 'strCmd', 'saveRegister' );
		
		$this->tpl->assign( 'strSubtitles', $add2);
		$this->tpl->assign( 'strButton', $add3 );
		
		$this->tpl->display( "UserRegister.php" );
	}
	
	protected function _userRegisterWithAddress(){
		$username = $this->getParameter( 'username',false);
		$add1 = $this->tpl->trans('there_are_errors');
		$add2 = $this->tpl->trans('create').' '.$this->tpl->trans('account');
		$add3 = $this->tpl->trans('register');

		$user = (!empty($this->tpl->user) ) ? $this->tpl->user : new eCommerce_Entity_User_Profile();
		$errors = (!empty($this->tpl->errors) ) ? $this->tpl->errors : new Validator_ErrorHandler();
		$userAddress = (!empty($this->tpl->userAddress) ) ? $this->tpl->userAddress : new eCommerce_Entity_Order_Address();
		
		$this->tpl->assign('user', $user);
		$this->tpl->assign('errors', $errors);
		$this->tpl->assign( 'address', $userAddress );
		
		$this->tpl->assign( 'strCmd', 'saveRegisterWithAddress' );
		
		$this->tpl->assign( 'strSubtitles', $add2);
		$this->tpl->assign( 'strButton', $add3 );
		
		$this->tpl->assign( 'username', $username );
		$this->tpl->display( "UserRegister.php" );
	}

	
	protected function _save(){
		$userSession = eCommerce_FrontEnd_Util_UserSession::GetIdentity();
		$user = eCommerce_SDO_Core_Application_Profile::LoadById( $userSession->getProfileId() );
		
		if( !empty( $_REQUEST ) && isset($_REQUEST['entity']) ){
			$entity = $_REQUEST['entity'];
			$oldPass = $_REQUEST['oldPass'];
			$tmpPass = $entity['password'];
			
			$entityAddress = $_REQUEST['entityAddress'];			
			$user->setFirstName( $entity['first_name'] );
			$user->setLastName( $entity['last_name'] );
			$user->setSaludo( $entity['saludo'] );
			
			if($oldPass == $tmpPass){
				$user->setPassword($entity['password']);
				$user->setConfirmPassword($entity['password']);
			}else{
				$user->setPassword(md5($tmpPass));
				$user->setConfirmPassword(md5($tmpPass));
			}
			$user->setEmail( $entity['email'] );
			$user->setRole( eCommerce_SDO_User::CUSTOMER_ROLE );
			
			$address = eCommerce_SDO_User::LoadUserAddress($_REQUEST['addressId']);		
			$address->setPhone( $entityAddress['phone'] );
			$address->setPhone2( $entityAddress['phone2'] );	
					
		}
		try{
			$add1 = $this->tpl->trans('there_are_errors');
			$add2 = ucfirst(strtolower($this->tpl->trans('the').' '.$this->tpl->trans('user').' "' . $user->getEmail() . '" '.$this->tpl->trans('has_been').' '.$this->tpl->trans('deleted')));
				
			$user = eCommerce_SDO_User::SaverUserProfile( $user );
			eCommerce_SDO_User::SaveUserAddress( $address );
			
			$userSession = eCommerce_FrontEnd_Util_UserSession::SetIdentity( $user );
			
			$this->tpl->assign( 'strSuccess', $add2 );
		}
		catch( eCommerce_SDO_Core_Validator_Exception $e){
			$this->tpl->assign( 'errors', new Validator_ErrorHandler( $add1, $e->getErrors() ) );
		}
		$this->tpl->assign('user', $user);
		//$this->_edit();
		$this->_info();
	}
	
	protected function _edit(){
		$add1 = $this->tpl->trans('edit').' '.$this->tpl->trans('account');
		$add2 = $this->tpl->trans('save');

		$user = (!empty($this->tpl->user) ) ? $this->tpl->user : eCommerce_FrontEnd_Util_UserSession::GetIdentity();
		$errors = (!empty($this->tpl->errors) ) ? $this->tpl->errors : new Validator_ErrorHandler();
		
		$this->tpl->assign( 'strSubtitles', $add1 );
		$this->tpl->assign( 'strCmd'      , "save" );
		$this->tpl->assign( 'strButton'   , $add2 );
		
		$address = eCommerce_SDO_User::LoadUserAddress( $user->getProfileId() );			
		
		$this->tpl->assign("address", $address);
		$this->tpl->assign("user", $user);
		$this->tpl->assign('errors', $errors);
		
		$this->tpl->display("UserEditProfile.php");
	}

	protected function _saveRegister(){
		$add1 = $this->tpl->trans('there_are_errors');
		$user = new eCommerce_Entity_User_Profile();
		$address = new eCommerce_Entity_User_Address();
		
		$this->tpl->assign( 'address', $address );
		
		$db = new eCommerce_SDO_Core_DB();
		$db->beginTransaction();
		try{
			if( !empty( $_REQUEST ) && isset($_REQUEST['entity']) ){
				$entity = $_REQUEST['entity'];
				$user->setFirstName( $entity['first_name'] );
				$user->setLastName( $entity['last_name'] );
				$user->setPassword($entity['password']);
				$user->setConfirmPassword($entity['confirm_password']);
				$user->setEmail( $entity['email'] );
				$user->setRole( eCommerce_SDO_User::CUSTOMER_ROLE );
			}
			$add2 = ucfirst(strtolower($this->tpl->trans('the').' '.$this->tpl->trans('user').' "' . $user->getEmail() . '" '.$this->tpl->trans('has_been').' '.$this->tpl->trans('deleted')));
			$this->tpl->assign('user', $user);
			
			$profile = eCommerce_SDO_User::SaverUserProfile( $user );
			$this->tpl->assign('strSuccess', $add2 );

			$db->commit();
			
			$auth = new eCommerce_FrontEnd_FO_Authentification();
			$auth->login($user->getEmail(), $user->getPassword());
		}
		catch( Exception $e ){
			$db->rollback();
			$this->tpl->assign( 'errors', new Validator_ErrorHandler( $add1, $e->getErrors() ) );
			$this->_userRegister();
		}
	}
	
	protected function _saveRegisterWithAddress(){
		$add1 = $this->tpl->trans('there_are_errors');
		$user = new eCommerce_Entity_User_Profile();
		$address = new eCommerce_Entity_User_Address();
		
		$this->tpl->assign( 'address', $address );
		
		$db = new eCommerce_SDO_Core_DB();
		$db->beginTransaction();
		try{			
			if( !empty( $_REQUEST ) && isset($_REQUEST['entity']) ){
				$entity = $_REQUEST['entity'];
				$user->setFirstName( $entity['first_name'] );
				$user->setLastName( $entity['last_name'] );
				$user->setPassword($entity['password']);
				$user->setConfirmPassword($entity['confirm_password']);
				$user->setEmail( $entity['email'] );
				$user->setRole( eCommerce_SDO_User::CUSTOMER_ROLE );
			}
			$add2 = ucfirst(strtolower($this->tpl->trans('the').' '.$this->tpl->trans('user').' "' . $user->getEmail() . '" '.$this->tpl->trans('has_been').' '.$this->tpl->trans('deleted')));
			$this->tpl->assign('user', $user);
			
			//-------------operation for the address
			$entityAddress = $this->getParameter( 'entityAddress', false, array() );
			
			$address->setStreet( $entityAddress['street'] );
			$address->setStreet2( $entityAddress['street2'] );
			$address->setStreet3( $entityAddress['street3'] );
			$address->setStreet4( $entityAddress['street4'] );
			$address->setCity( $entityAddress['city'] );
			$address->setState( $entityAddress['state'] );
			$address->setCountry( $entityAddress['country'] );
			$address->setZipCode( $entityAddress['zip_code'] );
			$address->setPhone( $entityAddress['phone'] );
			$address->setPhone2( $entityAddress['phone2'] );
			$address->setPhone3( $entityAddress['phone3'] );
			$address->setPhone4( $entityAddress['phone4'] );
			$address->setContactMethod( $entityAddress['contact_method'] );
			
			$this->tpl->assign('userAddress', $address);
			
			$profile = eCommerce_SDO_User::SaverUserProfile( $user );
			$this->tpl->assign('strSuccess', $add2 );
	
			$address->setProfileId( $profile->getProfileId() );
			$this->tpl->assign('userAddress', $address);
		
			eCommerce_SDO_User::SaveUserAddress( $address );
						
			$db->commit();
			
			$auth = new eCommerce_FrontEnd_FO_Authentification();
			$auth->login($user->getEmail(), $user->getPassword());
		}
		catch( Exception $e ){
			$db->rollback();
			$this->tpl->assign( 'errors', new Validator_ErrorHandler( $add1, $e->getErrors() ) );
			$this->_userRegisterWithAddress();
		}
	}
	
	protected function _factura(){
		$user = eCommerce_FrontEnd_Util_UserSession::GetIdentity();
		$address = eCommerce_SDO_User::LoadUserAddress( $user->getProfileId() );
		$this->tpl->assign( 'address', $address );
		$this->tpl->assign( "user", $user);
		$this->tpl->display( "solicitaFactura.php" );
	}
	
	protected function _rastreo(){
		$user = eCommerce_FrontEnd_Util_UserSession::GetIdentity();
		$address = eCommerce_SDO_User::LoadUserAddress( $user->getProfileId() );
		$this->tpl->assign( 'address', $address );
		$this->tpl->assign( "user", $user);
		$this->tpl->display( "RastreoPedido.php" );
	}
	
	
	protected function _confirmPayment(){
		$user = eCommerce_FrontEnd_Util_UserSession::GetIdentity();
		$address = eCommerce_SDO_User::LoadUserAddress( $user->getProfileId() );
		$this->tpl->assign( 'address', $address );
		$this->tpl->assign( "user", $user);
		$this->tpl->display( "UserConfirmpayment.php" );
	}
	
	protected function _info(){
		$user = eCommerce_FrontEnd_Util_UserSession::GetIdentity();
		$address = eCommerce_SDO_User::LoadUserAddress( $user->getProfileId() );
		$this->tpl->assign( 'address', $address );
		$this->tpl->assign( "user", $user);
		$this->tpl->display( "UserInfo.php" );
	}
	
	protected function _listOrders(){
		$user = eCommerce_FrontEnd_Util_UserSession::GetIdentity();
		$search = new eCommerce_Entity_Search(
			$this->getParameter( 'q', false, NULL ),
			$this->getParameter( 'o', false, 'creation_date DESC' ),
			$this->getParameter( 'p', true, 1 ),
			$this->getParameter( 'k', true, 15 )
		);
		
		$result = eCommerce_SDO_Core_Application_Order::Search( $search, $user->getProfileId() );
		
		$exportToExcel = $this->getParameter( "toExcel" );
		if( $exportToExcel ){
			
			$orders = $result->getResults();
			$data = array();
			foreach( $orders as $order ){
				$data[] = get_object_vars( $product );
			}
			//$this->exportToExcel( $data );
		}
		else{
			$this->tpl->assign( 'options', $result );
			$this->tpl->display("UserOrderMain.php");
		}
		
	}
	
	protected function _viewOrder(){
		$add1 = $this->tpl->trans('view').' '.$this->tpl->trans('order');
		$id = $this->getParameter();
		$order = eCommerce_SDO_Order::LoadFullById( $id );
		
		$actualProfile = eCommerce_FrontEnd_Util_UserSession::GetIdentity();
		$actualProfile = $actualProfile->getProfileId();

		//ATENTION 
		//verified if the property of the order is the actual user
		if( $order->getProfileId() != $actualProfile){
			$this->_listOrders();
		}
		else{
			$html = eCommerce_SDO_Order::GetHTMLByOrderId( $id );
			$this->tpl->assign( "strSubtitles" , $add1);
			$this->tpl->assign( "orderHtml" , $html);
			$this->tpl->assign( "order" , $order);
			$this->tpl->display("UserOrderView.php");
		}
	
	}
	
	
	protected function _editAddress(){
		$user = eCommerce_FrontEnd_Util_UserSession::GetIdentity();
		$profileId = $user->getProfileId();
		
		$address = eCommerce_SDO_User::LoadUserAddress( $profileId );
		
		$this->tpl->assign( 'errors', new Validator_ErrorHandler( ) );
		$this->editAddress( $address );
	}
	
	public function editAddress( eCommerce_Entity_User_Address $address ){
		
		$user = eCommerce_FrontEnd_Util_UserSession::GetIdentity();
		$profileId = $user->getProfileId();
		
		$this->tpl->assign( 'profile', $user ); 
		$this->tpl->assign( 'address', $address );
		$this->tpl->display( "UserEditAddress.php" );
	}
	
	public function _saveAddress(){
		$add1 = $this->tpl->trans('there_are_errors');
		$user = eCommerce_FrontEnd_Util_UserSession::GetIdentity();
		$profileId = $user->getProfileId();
		
		$entity = $this->getParameter( 'entity', false, array() );
		
		$address = new eCommerce_Entity_User_Address();
		$address->setProfileId( $profileId );
		$address->setStreet( $entity['street'] );
		$address->setStreet2( $entity['street2'] );
		$address->setStreet3( $entity['street3'] );
		$address->setStreet4( $entity['street4'] );
		$address->setCity( $entity['city'] );
		$address->setState( $entity['state'] );
		$address->setCountry( $entity['country'] );
		$address->setZipCode( $entity['zip_code'] );
		$address->setPhone( $entity['phone'] );
		$address->setPhone2( $entity['phone2'] );
		$address->setPhone3( $entity['phone3'] );
		$address->setPhone4( $entity['phone4'] );
		$address->setContactMethod( $entity['contact_method'] );
		try {
			eCommerce_SDO_User::SaveUserAddress( $address );
			$this->_info();
		}
		catch( eCommerce_SDO_Core_Validator_Exception $e ) {
			$this->tpl->assign( 'errors', new Validator_ErrorHandler( $add1, $e->getErrors() ) );
			$this->editAddress( $address );
		}
		
	}
}
?>