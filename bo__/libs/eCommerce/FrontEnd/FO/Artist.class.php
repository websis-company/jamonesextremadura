<?php

class eCommerce_FrontEnd_FO_Artist extends eCommerce_FrontEnd_FO {

	/**
	 * @var eCommerce_DAO_Event
	 */
	public function __construct(){
		parent::__construct();
		
	}

	public function execute(){
		
		$cmd = $this->getCommand();
		
		switch( $cmd ){
			case 'details':
				$this->_details();
			break;
			
			case 'search':
			default:
				$this->_search();
				break;
		}
		
	}
	protected function _details(){
		
		$id = $this->getParameter();
		
		$artist = eCommerce_SDO_Artist::LoadArtist( $id );

		$this->tpl->assign( 'artist', $artist );
		
		$this->tpl->display(  "artist/details.php" );
		
	}
	
	protected function _search(){
 
		
		$this->search = new eCommerce_Entity_Search(
		
			$this->getParameter( 'q', false, NULL ),
			$this->getParameter( 'o', false, 'name' ),
			$this->getParameter( 'p', true, 1 ),
			$this->getParameter( 'k', true, 6 )
			
		);
		
		$type = $this->getParameter("type",false,'sculptor');
		
		$result = eCommerce_SDO_Artist::SearchArtistsByType( $this->search, $type );
		
		$this->tpl->assign( 'result', $result );
		$this->tpl->display(  "artist/search.php" );
	}

	

}
?>