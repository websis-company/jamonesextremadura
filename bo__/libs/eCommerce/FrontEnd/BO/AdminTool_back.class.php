<?php
class eCommerce_FrontEnd_BO_AdminTool extends eCommerce_FrontEnd_BO {
public function getSDOSearch( $search, $extraConditions ){
	return eCommerce_SDO_AdminTool::SearchAdminTool( $search, $extraConditions );
}	
/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
	/**
	 * @var eCommerce_DAO_Event
	 */
	protected $search;
	public function __construct(){
		parent::__construct();
	}
	public function execute(){
		eCommerce_FrontEnd_BO_Authentification::verifyAuthentification();
		//$this->checkPermission();
		$cmd = $this->getCommand();
		switch( $cmd ){
			case 'save':
				$this->_save();
			break;
			case 'add':
			case 'edit':
			default:
				$this->_edit();
			break;
		}
	}
	protected function _edit(){
		//$id = $this->getParameter();
		$id = eCommerce_SDO_AdminTool::ADMIN_TOOL_ID;
		$entity = eCommerce_SDO_AdminTool::LoadAdminTool( $id );
		$this->tpl->assign( 'errors', new Validator_ErrorHandler() );
		$this->edit( $entity, $this->getCommand() );
	}
	public function edit( $entity, $cmd ){
		$this->tpl->assign( 'object',     $entity );
		$viewConfig = array();
		$form = array();
		$form['name'] = 'AdminToolForm';
		$form['elements'] = array();
		/***********************************************************************************************************/
		$form['elements'][] = array( 'title' => 'AdminToolId', 'type'=>'hidden', 'options'=>array(), 'name'=>'entity[admin_tool_id]', 'id'=>'admin_tool_id', 'value'=> $entity->getAdminToolId() );
		$form['elements'][] = array( 'title' => 'Exchange Rate <br />(1 USD = MXN)', 'type'=>'hidden', 'options'=>array(), 'name'=>'entity[exchange_rate_mxn2usd]', 'id'=>'exchange_rate_mxn2usd', 'value'=> $entity->getExchangeRateMxn2usd() );
		$form['elements'][] = array( 'title' => 'Marco A', 'type'=>'text', 'options'=>array(), 'name'=>'entity[marco_a]', 'id'=>'marco_a', 'value'=> $entity->getMarcoA() );
		$form['elements'][] = array( 'title' => 'Marco B', 'type'=>'text', 'options'=>array(), 'name'=>'entity[marco_b]', 'id'=>'marco_b', 'value'=> $entity->getMarcoB() );
		$form['elements'][] = array( 'title' => 'Marco C', 'type'=>'text', 'options'=>array(), 'name'=>'entity[marco_c]', 'id'=>'marco_c', 'value'=> $entity->getMarcoC() );
		$form['elements'][] = array( 'title' => 'Impresión m<sup>2</sup> Tela', 'type'=>'text', 'options'=>array(), 'name'=>'entity[m_tela]', 'id'=>'m_tela', 'value'=> $entity->getMTela() );
		$form['elements'][] = array( 'title' => 'Impresión m<sup>2</sup> Lona', 'type'=>'text', 'options'=>array(), 'name'=>'entity[m_lona]', 'id'=>'m_lona', 'value'=> $entity->getMLona() );
		$form['elements'][] = array( 'title' => 'Acabado <br/>(Resolución Profesional)', 'type'=>'text', 'options'=>array(), 'name'=>'entity[acabado]', 'id'=>'acabado', 'value'=> $entity->getAcabado() );
		$form['elements'][] = array( 'title' => 'Afinado <br/>(Estructura Económica)', 'type'=>'text', 'options'=>array(), 'name'=>'entity[estructura_economica]', 'id'=>'estructura_economica', 'value'=> $entity->getEstructuraEconomica() );
		$form['elements'][] = array( 'title' => 'Afinado <br/>(Estructura Profesional)', 'type'=>'text', 'options'=>array(), 'name'=>'entity[estructura_profesional]', 'id'=>'estructura_profesional', 'value'=> $entity->getEstructuraProfesional() );
		/***********************************************************************************************************/
		//$viewConfig['id'] = 'noticia_evento_id';
		$viewConfig['form'] = $form;
		$viewConfig['title'] = ( $cmd =='add' ? '' : '' ) . ' System Configuration';
		$this->tpl->assign( 'form', $form );	
		$this->tpl->assign( 'viewConfig', $viewConfig );
		//$this->tpl->assign( 'ArrayImages', $entity->getArrayImages() );
		$this->tpl->display( 'AdminTool/edit.php' );
	}
	protected function exportToExcel( $data ){
		$xls = new ExcelWriter( 'Noticias_' . date('Y-m-d'), 'AdminTool' );
		$xls->writeHeadersAndDataFromArray( $data );
	}
	protected function _save(){
		if ( !empty( $_REQUEST ) && isset( $_REQUEST['entity'] ) ){
			$entity = $_REQUEST['entity'];
			$AdminTool = new eCommerce_Entity_AdminTool();
			// Save type
			try{
			/******************************************************/
			/******************************************************/
			$txt = (empty($entity['exchange_rate_mxn2usd']) ? '' : $entity['exchange_rate_mxn2usd']);$AdminTool->setExchangeRateMxn2usd( $txt );
			$txt = (empty($entity['marco_a']) ? '' : $entity['marco_a']);$AdminTool->setMarcoA( $txt );
			$txt = (empty($entity['marco_b']) ? '' : $entity['marco_b']);$AdminTool->setMarcoB( $txt );
			$txt = (empty($entity['marco_c']) ? '' : $entity['marco_c']);$AdminTool->setMarcoC( $txt );
			$txt = (empty($entity['m_tela']) ? '' : $entity['m_tela']);$AdminTool->setMTela( $txt );
			$txt = (empty($entity['m_lona']) ? '' : $entity['m_lona']);$AdminTool->setMLona( $txt );
			$txt = (empty($entity['acabado']) ? '' : $entity['acabado']);$AdminTool->setAcabado( $txt );
			$txt = (empty($entity['estructura_economica']) ? '' : $entity['estructura_economica']);$AdminTool->setEstructuraEconomica( $txt );
			$txt = (empty($entity['estructura_profesional']) ? '' : $entity['estructura_profesional']);$AdminTool->setEstructuraProfesional( $txt );
			$entity['admin_tool_id'] = eCommerce_SDO_AdminTool::ADMIN_TOOL_ID;
			$AdminTool->setAdminToolId( $entity['admin_tool_id'] );
  			/******************************************************/
  			/*****************************************************/
				/*
				$AdminToolTmp = eCommerce_SDO_AdminTool::LoadAdminTool( $AdminTool->getAdminToolId() );
				$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('AdminToolImages');
				$imageHandler->setArrImages( $AdminToolTmp->getArrayImages() );
				$imageHandler->setUploadFileDirectory('files/images/noticias/');
				$imageHandler->setUploadFileDescription('Imagen de Evento ' . $AdminTool->getAdminToolId() );
				$imageHandler->setValidTypes( array('word','image') );
				$imageHandler->setAccessType( eCommerce_SDO_ImageHandler::GetValidAccessType('AdminTool') );
				$AdminTool->setArrayImages( implode(',',$AdminToolTmp->getArrayImages()) );
				$imageHandler->setMaximum( 1 );
				$arrImages = $imageHandler->proccessImages();
				$AdminTool->setArrayImages( $arrImages );
				*/
				eCommerce_SDO_AdminTool::SaverAdminTool( $AdminTool );
				$this->tpl->assign( 'strSuccess', 'Configuration has been saved.' );
				$this->_edit();
			}
			catch( eCommerce_SDO_Core_Validator_Exception $e){
				$this->tpl->assign( 'errors', new Validator_ErrorHandler( "There are some errors", $e->getErrors() ) );
				$oldCmd = $this->getParameter( 'old_cmd', false, null );
				$this->edit( $AdminTool,  $oldCmd );
			}
		}
		else{
			throw new Exception('No hay suficiente informaci&oacute;n intente nuevamente');
		}
	}
}
?>