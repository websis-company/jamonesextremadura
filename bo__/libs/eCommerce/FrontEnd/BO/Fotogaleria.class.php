<?php
class eCommerce_FrontEnd_BO_Fotogaleria extends eCommerce_FrontEnd_BO {
public function getSDOSearch( $search, $extraConditions ){
	return eCommerce_SDO_Fotogaleria::SearchFotogaleria( $search, $extraConditions );
}	
/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
	/**
	 * @var eCommerce_DAO_Event
	 */
	protected $search;
	protected $idFields;
	public function __construct(){
		parent::__construct();
		$this->idFields = 'fotogaleria_id';
	}


	public function execute(){
		//$accessManager = new eCommerce_Access_Fotogaleria( $this );
		//$accessManager->checkPermission(eCommerce_Access::FOTOGALERIA_ALL_PERMISSIONS);
		//$this->checkPermission();
		$cmd = $this->getCommand();
		switch( $cmd ){
			case 'delete':
				//$accessManager->checkPermission(eCommerce_Access::FOTOGALERIA_DELETE);
				$this->_delete();
				break;
			case 'add':
				//$accessManager->checkPermission(eCommerce_Access::FOTOGALERIA_ADD);
				$this->_add();
				break;
			case 'edit':
				//$accessManager->checkPermission(eCommerce_Access::FOTOGALERIA_EDIT);
				$this->_edit();
				break;
			case 'save':
				//$accessManager->checkPermission(eCommerce_Access::FOTOGALERIA_SAVE);
				$this->_save();
				break;
			case 'list':
			default:
				//$accessManager->checkPermission(eCommerce_Access::FOTOGALERIA_LIST);
				$this->_list();
				break;
		}
	}


	protected function _list(){
		$this->search = new eCommerce_Entity_Search(
			$this->getParameter( 'q', false, NULL ),
			$this->getParameter( 'o', false, '' ), //fecha DESC
			$this->getParameter( 'p', true, 1 ),
			$this->getParameter( 'k', true, 10 )
		);
		$extraConditions = array();
		//$extraConditions[] = array( "columName"=>"type","value"=>'',"isInteger"=>false);
		$result = $this->getSDOSearch( $this->search, $extraConditions ); 
		$exportToExcel = $this->getParameter( 'toExcel' );
		if ( $exportToExcel ){
			$regiters = $result->getResults();
			$data = array();
			foreach($regiters as $regiter){
				$data[] = get_object_vars( $regiter );
			}
			$this->exportToExcel( $data );
		}
		else {
			$viewConfig = array();
			$viewConfig['name'] = 'Fotogaleria';
			$viewConfig['title'] = 'Fotogaleria';
			$viewConfig['id'] = $this->idFields;
			//---------------------------
			if(strpos($viewConfig['id'],",") > -1){
				$ids = explode(",",$viewConfig['id']);
				$id = $ids[0];
				unset($ids[0]);
				$viewConfig['id'] = '{$'.$id.'}';
				foreach($ids as $id){
					$viewConfig['id'] .= ',{$'.$id.'}';
				}
			}else{
				$viewConfig['id'] = '{$'.$viewConfig['id'].'}';
			}
			//---------------------------
			$viewConfig["hiddenColums"] = array('fotogaleria_id','descripcion','imagenes','fecha','imagen_principal');
			$viewConfig["columNamesOverride"]["galerycategory_id"] = 'Categoría';
			$this->tpl->assign( 'viewConfig', $viewConfig );
			$this->tpl->assign( 'options', $result );
			$this->tpl->display(  "Fotogaleria/list.php" );
		}
	}


	protected function _add(){
		try{
			$Fotogaleria = new eCommerce_Entity_Fotogaleria();
			//$Fotogaleria = eCommerce_SDO_Fotogaleria::SaverFotogaleria( $Fotogaleria );
			$this->_edit( $Fotogaleria->getFotogaleriaId() );
		}catch( eCommerce_SDO_Core_Validator_Exception $e){
			$this->tpl->assign( 'strError', $this->tpl->trans('try_again') );
			$this->_list();
		}
	}


	protected function getMultipleArray($parent, $sangria, $idParent){
		$search     = new eCommerce_Entity_Search(null,null,1,-1);	// -1 = ALL
		$result     = eCommerce_SDO_Galerycategory::SearchCategoriesByParentCategory($search, $parent);
		$categories = $result->getResults();

		$data = array();
		foreach($categories as $category){
			$data[] = get_object_vars($category);
		}

		foreach($data as $g_category){
			$sel = ($idParent == $g_category['galerycategory_id']) ? 'selected = "selected"' : '';
			$this->multipleLevels .= "<option value='$g_category[galerycategory_id]' $sel>$sangria$g_category[name] </option>";
			$this->getMultipleArray($g_category['galerycategory_id'], $sangria.'&nbsp;&nbsp;&nbsp;&nbsp;', $g_category["parent_category_id"]);
		}
	}

	protected function _edit($id = null){
		//$id = $this->getParameter();
		$id = empty($id ) ? $this->getParameter('id',false,0) : $id;
		//---------------------------
		if( strpos($id,",") > -1 ){
			$idF = explode(",",$this->idFields);
			$ArrId = explode(",",$id);
			$id = array();
			for($i=0; $i< count($idF); $i++){
				$id[ $idF[$i] ] = (empty($ArrId[$i])) ? 0 : $ArrId[$i];
			}
		}
		//---------------------------
		$entity = eCommerce_SDO_Fotogaleria::LoadFotogaleria( $id );
		$this->tpl->assign( 'errors', new Validator_ErrorHandler() );
		$this->edit( $entity, $this->getCommand() );
	}


	public function edit( $entity, $cmd ){
		$this->getMultipleArray(0,'',0);
		$this->tpl->assign( 'object',     $entity );
		$viewConfig = array();

		//$form             = array();
		//$form['name']     = 'FotogaleriaForm';
		//$form['elements'] = array();
		/***********************************************************************************************************/
		//$form['elements'][] = array( 'title' => 'FotogaleriaId:', 'type'=>'hidden', 'options'=>array(), 'name'=>'entity[fotogaleria_id]', 'id'=>'fotogaleria_id', 'value'=> $entity->getFotogaleriaId() );
		//$form['elements'][] = array( 'title' => 'Categoría:', 'type'=>'select', 'options'=>array(), 'name'=>'entity[galerycategory_id]', 'id'=>'galerycategory_id', 'value'=> $entity->getGalerycategoryId() );
		//$form['elements'][] = array( 'title' => 'Titulo:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[titulo]', 'id'=>'titulo', 'value'=> $entity->getTitulo() );
		//$form['elements'][] = array( 'title' => 'Descripcion:', 'type'=>'textarea', 'options'=>array(), 'name'=>'entity[descripcion]', 'id'=>'descripcion', 'value'=> $entity->getDescripcion() );
		//$form['elements'][] = array( 'title' => 'ImagenPrincipal:', 'type'=>'array_files', 'options'=>array(), 'name'=>'entity[imagen_principal]', 'id'=>'imagen_principal', 'value'=> $entity->getImagenPrincipal(), 'max_files'=>1 );
		//$form['elements'][] = array( 'title' => 'Imagenes:', 'type'=>'array_files', 'options'=>array(), 'name'=>'entity[imagenes]', 'id'=>'imagenes', 'value'=> $entity->getImagenes(), 'max_files'=>50 );
		//$form['elements'][] = array( 'title' => 'Fecha:', 'type'=>'calendar', 'options'=>array(), 'name'=>'entity[fecha]', 'id'=>'fecha', 'value'=> $entity->getFecha() );
		/***********************************************************************************************************/
		//$viewConfig['id'] = 'noticia_evento_id';
		$viewConfig['form'] = $form;
		$viewConfig['title'] = ( $cmd =='add' ? $this->tpl->trans('add') : $this->tpl->trans('edit') ) . ' Fotogaleria';
		$this->tpl->assign( 'form', $form );	
		$this->tpl->assign( 'categorias', $this->multipleLevels );	
		$this->tpl->assign( 'viewConfig', $viewConfig );
		//$this->tpl->assign( 'ArrayImages', $entity->getArrayImages() );
		$this->tpl->assign( 'cancelParams', $cmd == 'add' ? '?cmd=delete&id=' . $entity->getFotogaleriaId() . '' : '' );
		$this->tpl->assign( 'cancelConfirm', $cmd == 'add' ? 'if(confirm("'.$this->tpl->trans('?_confirm_delete').' '.strtolower($this->tpl->trans('recently_created_entry')).'?"))'  : '' );
		
		$this->tpl->assign( 'fotogaleria',     $entity );
		$this->tpl->display( 'Fotogaleria/edit.php' );
	}


	protected function exportToExcel( $data, $fileName = '' ){
		$rowO = $data[0];
		$contentHtml .= '<table border="1">';
		$contentHtml .= '<tr>
		<td height="72" width="255" bgcolor="#cccccc"><img src="'.ABS_HTTP_URL.'/ima/logo.gif"></td>
		<td colspan="'.(count($rowO) - 1).'"><h2>Order Report - '.date("M d Y").'</h2></td>
		</tr>';
		$contentHtml .= '<tr>';
		foreach($rowO as $key => $row){
			$key = str_replace("_",' ',$key);
			$key = ucwords($key);
			$contentHtml .= '<th align="center">'.$key.'</th>';
		}
		$contentHtml .= '</tr>';
		$i = 0;
		foreach($data as $row){
			$bgcolor = (++$i % 2 == 0) ? 'bgcolor="#cccccc"' : '';
			$contentHtml .= '<tr>';
			foreach($row as $rowElement){
				$contentHtml .= '<td '.$bgcolor.' align="right">'.$rowElement.'</td>';
			}
			$contentHtml .= '</tr>';
		}
		$contentHtml .= '</table>';
		$total_bytes = strlen($contentHtml);
		header("Content-type: application/vnd.ms-excel");
		header("Content-disposition: attachment; filename=".$fileName.".xls; size=" . $total_bytes);
		echo $contentHtml;
	}


	protected function _save(){
		if ( !empty( $_REQUEST ) && isset( $_REQUEST['entity'] ) ){
			$entity = $_REQUEST['entity'];
			$Fotogaleria = new eCommerce_Entity_Fotogaleria();
			// Save type
			try{
				/******************************************************/
				/******************************************************/
				$txt = (empty($entity['fotogaleria_id']) ? '' : $entity['fotogaleria_id']);$Fotogaleria->setFotogaleriaId( strip_tags($txt) );
				$txt = (empty($entity['galerycategory_id']) ? '' : $entity['galerycategory_id']);$Fotogaleria->setGalerycategoryId( strip_tags($txt) );
				$txt = (empty($entity['titulo']) ? '' : $entity['titulo']);$Fotogaleria->setTitulo( strip_tags($txt) );
				$txt = (empty($entity['descripcion']) ? '' : $entity['descripcion']);$Fotogaleria->setDescripcion( strip_tags($txt) );
				/////////////////////////////////////////////////////////////////////////////////////////
				/*$FotogaleriaTmp = eCommerce_SDO_Fotogaleria::LoadFotogaleria( $Fotogaleria->getFotogaleriaId() );
						$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('imagen_principal');
						$imageHandler->setArrImages( $FotogaleriaTmp->getImagenPrincipal() );
						$imageHandler->setUploadFileDirectory('files/images/fotogaleria/');
						$imageHandler->setUploadVersionDirectory('web/');
						$imageHandler->setUploadFileDescription( $this->getParameter( $imageHandler->GetDescriptionField(),false,'Imagen de Fotogaleria' .$Fotogaleria->getFotogaleriaId() ) );
						$imageHandler->setValidTypes( array('word','image') );
						$imageHandler->setAccessType( eCommerce_SDO_ImageHandler::GetValidAccessType('public') );
						$arrVersions = array();
						$arrVersions[] = array('version'=>'small',	'path'=>'images/fotogaleria/',	'width'=>90, 'height'=>140);
						$arrVersions[] = array('version'=>'medium',	'path'=>'images/fotogaleria/',	'width'=>180, 'height'=>280);
						$arrVersions[] = array('version'=>'large',	'path'=>'images/fotogaleria/',	'width'=>1280, 'height'=>720);
						$imageHandler->setArrVersions($arrVersions);
						$imageHandler->setMaximum( 1 );
						$arrImages = $imageHandler->proccessImages();
						$Fotogaleria->setImagenPrincipal( $arrImages );
				////////////////////////////////////////////////////////*//////////////////////////////////
				/////////////////////////////////////////////////////////////////////////////////////////
				/*$FotogaleriaTmp = eCommerce_SDO_Fotogaleria::LoadFotogaleria( $Fotogaleria->getFotogaleriaId() );
						$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('imagenes');
						$imageHandler->setArrImages( $FotogaleriaTmp->getImagenes() );
						$imageHandler->setUploadFileDirectory('files/images/fotogaleria/');
						$imageHandler->setUploadVersionDirectory('web/');
						$imageHandler->setUploadFileDescription( $this->getParameter( $imageHandler->GetDescriptionField(),false,'Imagen de Fotogaleria' .$Fotogaleria->getFotogaleriaId() ) );
						$imageHandler->setValidTypes( array('word','image') );
						$imageHandler->setAccessType( eCommerce_SDO_ImageHandler::GetValidAccessType('public') );
						$arrVersions = array();
						$arrVersions[] = array('version'=>'small',	'path'=>'images/fotogaleria/',	'width'=>90, 'height'=>140);
						$arrVersions[] = array('version'=>'medium',	'path'=>'images/fotogaleria/',	'width'=>180, 'height'=>280);
						$arrVersions[] = array('version'=>'large',	'path'=>'images/fotogaleria/',	'width'=>1280, 'height'=>720);
						$imageHandler->setArrVersions($arrVersions);
						$imageHandler->setMaximum( 50 );
						$arrImages = $imageHandler->proccessImages();
						$Fotogaleria->setImagenes( $arrImages );*/
					/////////////////////////////////////////////////////////////////////////////////////////
				$txt = (empty($entity['fecha']) ? '' : $entity['fecha']);$Fotogaleria->setFecha( strip_tags($txt) );
	  			/******************************************************/
	  			/*****************************************************/
				/*
				$FotogaleriaTmp = eCommerce_SDO_Fotogaleria::LoadFotogaleria( $Fotogaleria->getFotogaleriaId() );
				$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('FotogaleriaImages');
				$imageHandler->setArrImages( $FotogaleriaTmp->getArrayImages() );
				$imageHandler->setUploadFileDirectory('files/images/noticias/');
				$imageHandler->setUploadFileDescription('Imagen de Evento ' . $Fotogaleria->getFotogaleriaId() );
				$imageHandler->setValidTypes( array('word','image') );
				$imageHandler->setAccessType( eCommerce_SDO_ImageHandler::GetValidAccessType('public') );
				$Fotogaleria->setArrayImages( implode(',',$FotogaleriaTmp->getArrayImages()) );
				$imageHandler->setMaximum( 1 );
				$arrImages = $imageHandler->proccessImages();
				$Fotogaleria->setArrayImages( $arrImages );
				*/
				$FotogaleriaSave = eCommerce_SDO_Fotogaleria::SaverFotogaleria( $Fotogaleria );
				$fotogaleria_id  = $FotogaleriaSave->getFotogaleriaId();
				
				#-- Modifica la galeria relacionada a las imagenes
				$imagenes        = eCommerce_SDO_Core_Application_Files::LoadByGaleryId('-1');
				if(!empty($imagenes)){
					foreach ($imagenes as $result) {
						$files = eCommerce_SDO_Core_Application_Files::LoadById($result->getFilesId());
						$files->setGaleriaId($fotogaleria_id);
						eCommerce_SDO_Files::SaverFiles($files);
					}//end forech
				}//end if


				$this->tpl->assign( 'strSuccess', $this->tpl->trans('the').' Fotogaleria "' . $Fotogaleria->getFotogaleriaId() . '" '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('saved')));
				$this->_list();
			}
			catch( eCommerce_SDO_Core_Validator_Exception $e){
				$this->tpl->assign( 'errors', new Validator_ErrorHandler( $this->tpl->trans("there_are_errors"), $e->getErrors() ) );
				$oldCmd = $this->getParameter( 'old_cmd', false, null );
				$this->edit( $Fotogaleria,  $oldCmd );
			}
		}
		else{
			throw new Exception($this->tpl->trans('not_enough_information').'. '.$this->tpl->trans('try_again'));
		}
	}


	protected function _delete(){
		//$id = $this->getParameter();
		$id = $this->getParameter('id',false,0);
		$id2Str = $id;
		//---------------------------
		if( strpos($id,",") > -1 ){
			$idF = explode(",",$this->idFields);
			$ArrId = explode(",",$id);
			$id = array();
			$id2Str = "(";
			for($i=0; $i< count($idF); $i++){
				$id[ $idF[$i] ] = (empty($ArrId[$i])) ? 0 : $ArrId[$i];
				$id2Str .=($i>0) ? ',' : '';
				$id2Str .= $ArrId[$i] . "";
			}
			$id2Str .= ")";
		}
		//---------------------------
			try{
				$Fotogaleria = eCommerce_SDO_Fotogaleria::DeleteFotogaleria($id );
				$this->tpl->assign( 'strSuccess', $this->tpl->trans('the').' Fotogaleria "' . $Fotogaleria->getFotogaleriaId() . '" '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('deleted')));
			}
			catch(eCommerce_SDO_Core_Application_Exception $e) {
				//debug($e);
				$this->tpl->assign( 'strError', $e->getMessage() );
			}
		$this->_list();
	}
}
?>