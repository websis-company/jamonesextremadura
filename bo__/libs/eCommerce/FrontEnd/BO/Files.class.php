<?php
class eCommerce_FrontEnd_BO_Files extends eCommerce_FrontEnd_BO {


public function getSDOSearch( $search, $extraConditions ){
	return eCommerce_SDO_Files::SearchFiles( $search, $extraConditions );
}	


/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
	
	/**
	 * @var eCommerce_DAO_Event
	 */
	protected $search;
	protected $idFields;
	
	public function __construct(){
		parent::__construct();
		$this->idFields = 'files_id';
	}

	public function execute(){
		$accessManager = new eCommerce_Access_Files( $this );
		$accessManager->checkPermission(eCommerce_Access::FILES_ALL_PERMISSIONS);
		$this->checkPermission();
		
		$cmd = $this->getCommand();
		switch( $cmd ){
			case 'delete':
				$accessManager->checkPermission(eCommerce_Access::FILES_DELETE);
				$this->_delete();
				break;
			case 'add':
				$accessManager->checkPermission(eCommerce_Access::FILES_ADD);
				$this->_add();
				break;
			case 'edit':
				$accessManager->checkPermission(eCommerce_Access::FILES_EDIT);
				$this->_edit();
				break;
			case 'save':
				$accessManager->checkPermission(eCommerce_Access::FILES_SAVE);
				$this->_save();
				break;
			case 'list':
			default:
				$accessManager->checkPermission(eCommerce_Access::FILES_LIST);
				$this->_list();
				break;
		}
	}

	protected function _list(){

		$this->search = new eCommerce_Entity_Search(
			$this->getParameter( 'q', false, NULL ),
			$this->getParameter( 'o', false, '' ), //fecha DESC
			$this->getParameter( 'p', true, 1 ),
			$this->getParameter( 'k', true, 10 )
		);
		$extraConditions = array();
		//$extraConditions[] = array( "columName"=>"type","value"=>'',"isInteger"=>false);
		$result = $this->getSDOSearch( $this->search, $extraConditions ); 
		
		
		$exportToExcel = $this->getParameter( 'toExcel' );
		if ( $exportToExcel ){
		
			$regiters = $result->getResults();
			$data = array();
			foreach($regiters as $regiter){
				$data[] = get_object_vars( $regiter );
			}
			$this->exportToExcel( $data );

		}
		else {
			$viewConfig = array();
			$viewConfig['name'] = 'Files';
			$viewConfig['title'] = 'Files';
			$viewConfig['id'] = $this->idFields;
			
			//---------------------------
			if(strpos($viewConfig['id'],",") > -1){
				$ids = explode(",",$viewConfig['id']);
				$id = $ids[0];
				unset($ids[0]);
				$viewConfig['id'] = '{$'.$id.'}';
				foreach($ids as $id){
					$viewConfig['id'] .= ',{$'.$id.'}';
				}
			}else{
				$viewConfig['id'] = '{$'.$viewConfig['id'].'}';
			}
			//---------------------------
			
			$viewConfig["hiddenColums"]= array('files_id');
			
			$this->tpl->assign( 'viewConfig', $viewConfig );
			
			$this->tpl->assign( 'options', $result );
			
			$this->tpl->display(  "Files/list.php" );
		}
	}
	
	protected function _add(){
		try{
			$Files = new eCommerce_Entity_Files();
			$Files = eCommerce_SDO_Files::SaverFiles( $Files );
			$this->_edit( $Files->getFilesId() );
		}catch( eCommerce_SDO_Core_Validator_Exception $e){
			$this->tpl->assign( 'strError', $this->tpl->trans('try_again') );
			$this->_list();
		}
	}
	
	protected function _edit($id = null){
		//$id = $this->getParameter();
		$id = empty($id ) ? $this->getParameter('id',false,0) : $id;
		//---------------------------
		if( strpos($id,",") > -1 ){
			$idF = explode(",",$this->idFields);
			$ArrId = explode(",",$id);
			$id = array();
			for($i=0; $i< count($idF); $i++){
				$id[ $idF[$i] ] = (empty($ArrId[$i])) ? 0 : $ArrId[$i];
			}
		}
		//---------------------------
		
		$entity = eCommerce_SDO_Files::LoadFiles( $id );
		
		
		$this->tpl->assign( 'errors', new Validator_ErrorHandler() );
		
		$this->edit( $entity, $this->getCommand() );

	}

	public function edit( $entity, $cmd ){

		$this->tpl->assign( 'object',     $entity );
		
		$viewConfig = array();
		$form = array();

		
		$form['name'] = 'FilesForm';
		$form['elements'] = array();
		
		
		/***********************************************************************************************************/
		
$form['elements'][] = array( 'title' => 'FilesId:', 'type'=>'hidden', 'options'=>array(), 'name'=>'entity[files_id]', 'id'=>'files_id', 'value'=> $entity->getFilesId() );
$form['elements'][] = array( 'title' => 'Name:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[name]', 'id'=>'name', 'value'=> $entity->getName() );
$form['elements'][] = array( 'title' => 'Size:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[size]', 'id'=>'size', 'value'=> $entity->getSize() );
$form['elements'][] = array( 'title' => 'Type:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[type]', 'id'=>'type', 'value'=> $entity->getType() );
$form['elements'][] = array( 'title' => 'Url:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[url]', 'id'=>'url', 'value'=> $entity->getUrl() );
$form['elements'][] = array( 'title' => 'Title:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[title]', 'id'=>'title', 'value'=> $entity->getTitle() );
$form['elements'][] = array( 'title' => 'Description:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[description]', 'id'=>'description', 'value'=> $entity->getDescription() );
$form['elements'][] = array( 'title' => 'GaleriaId:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[galeria_id]', 'id'=>'galeria_id', 'value'=> $entity->getGaleriaId() );
$form['elements'][] = array( 'title' => 'Tipo:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[tipo]', 'id'=>'tipo', 'value'=> $entity->getTipo() );
		/***********************************************************************************************************/
		
		//$viewConfig['id'] = 'noticia_evento_id';
		
		$viewConfig['form'] = $form;
		$viewConfig['title'] = ( $cmd =='add' ? $this->tpl->trans('add') : $this->tpl->trans('edit') ) . ' Files';
		
		$this->tpl->assign( 'form', $form );	
		$this->tpl->assign( 'viewConfig', $viewConfig );
		
		//$this->tpl->assign( 'ArrayImages', $entity->getArrayImages() );

		$this->tpl->assign( 'cancelParams', $cmd == 'add' ? '?cmd=delete&id=' . $entity->getFilesId() . '' : '' );
		$this->tpl->assign( 'cancelConfirm', $cmd == 'add' ? 'if(confirm("'.$this->tpl->trans('?_confirm_delete').' '.strtolower($this->tpl->trans('recently_created_entry')).'?"))'  : '' );
		
			$this->tpl->display( 'Files/edit.php' );
	}

	protected function exportToExcel( $data, $fileName = '' ){

		

		$rowO = $data[0];

		

		$contentHtml .= '<table border="1">';

		$contentHtml .= '<tr>

		<td height="72" width="255" bgcolor="#cccccc"><img src="'.ABS_HTTP_URL.'/ima/logo.gif"></td>

		<td colspan="'.(count($rowO) - 1).'"><h2>Order Report - '.date("M d Y").'</h2></td>

		</tr>';

		

		$contentHtml .= '<tr>';

		

		foreach($rowO as $key => $row){

			$key = str_replace("_",' ',$key);

			$key = ucwords($key);

			$contentHtml .= '<th align="center">'.$key.'</th>';

		}

		$contentHtml .= '</tr>';

		

		$i = 0;

		foreach($data as $row){

			

			$bgcolor = (++$i % 2 == 0) ? 'bgcolor="#cccccc"' : '';

			$contentHtml .= '<tr>';

			foreach($row as $rowElement){

				$contentHtml .= '<td '.$bgcolor.' align="right">'.$rowElement.'</td>';

			}

			$contentHtml .= '</tr>';

		}

		

		$contentHtml .= '</table>';

		

		

		$total_bytes = strlen($contentHtml);

		header("Content-type: application/vnd.ms-excel");

		header("Content-disposition: attachment; filename=".$fileName.".xls; size=" . $total_bytes);

		echo $contentHtml;

	}

	protected function _save(){
		
		if ( !empty( $_REQUEST ) && isset( $_REQUEST['entity'] ) ){
	  		
			$entity = $_REQUEST['entity'];
  			
			$Files = new eCommerce_Entity_Files();
			

			// Save type
			try{
			
			
			/******************************************************/
			/******************************************************/
			
$txt = (empty($entity['files_id']) ? '' : $entity['files_id']);$Files->setFilesId( strip_tags($txt) );
$txt = (empty($entity['name']) ? '' : $entity['name']);$Files->setName( strip_tags($txt) );
$txt = (empty($entity['size']) ? '' : $entity['size']);$Files->setSize( strip_tags($txt) );
$txt = (empty($entity['type']) ? '' : $entity['type']);$Files->setType( strip_tags($txt) );
$txt = (empty($entity['url']) ? '' : $entity['url']);$Files->setUrl( strip_tags($txt) );
$txt = (empty($entity['title']) ? '' : $entity['title']);$Files->setTitle( strip_tags($txt) );
$txt = (empty($entity['description']) ? '' : $entity['description']);$Files->setDescription( strip_tags($txt) );
$txt = (empty($entity['galeria_id']) ? '' : $entity['galeria_id']);$Files->setGaleriaId( strip_tags($txt) );
$txt = (empty($entity['tipo']) ? '' : $entity['tipo']);$Files->setTipo( strip_tags($txt) );
  			/******************************************************/
  			/*****************************************************/
  			
				
				/*
				$FilesTmp = eCommerce_SDO_Files::LoadFiles( $Files->getFilesId() );
				
				$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('FilesImages');
				$imageHandler->setArrImages( $FilesTmp->getArrayImages() );
				
				$imageHandler->setUploadFileDirectory('files/images/noticias/');
				$imageHandler->setUploadFileDescription('Imagen de Evento ' . $Files->getFilesId() );
				
				$imageHandler->setValidTypes( array('word','image') );
				
				$imageHandler->setAccessType( eCommerce_SDO_ImageHandler::GetValidAccessType('public') );
				
				$Files->setArrayImages( implode(',',$FilesTmp->getArrayImages()) );
				$imageHandler->setMaximum( 1 );
				$arrImages = $imageHandler->proccessImages();
				$Files->setArrayImages( $arrImages );
				*/
				
				
				eCommerce_SDO_Files::SaverFiles( $Files );
				$this->tpl->assign( 'strSuccess', $this->tpl->trans('the').' Files "' . $Files->getFilesId() . '" '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('saved')));
				$this->_list();
			}
			catch( eCommerce_SDO_Core_Validator_Exception $e){
				$this->tpl->assign( 'errors', new Validator_ErrorHandler( $this->tpl->trans("there_are_errors"), $e->getErrors() ) );
				$oldCmd = $this->getParameter( 'old_cmd', false, null );
				$this->edit( $Files,  $oldCmd );
			}
		}
		else{
			throw new Exception($this->tpl->trans('not_enough_information').'. '.$this->tpl->trans('try_again'));
		}
	}

	protected function _delete(){
		//$id = $this->getParameter();
		$id = $this->getParameter('id',false,0);
		$id2Str = $id;
		//---------------------------
		if( strpos($id,",") > -1 ){
			$idF = explode(",",$this->idFields);
			$ArrId = explode(",",$id);
			$id = array();
			$id2Str = "(";
			for($i=0; $i< count($idF); $i++){
				$id[ $idF[$i] ] = (empty($ArrId[$i])) ? 0 : $ArrId[$i];
				$id2Str .=($i>0) ? ',' : '';
				$id2Str .= $ArrId[$i] . "";
			}
			$id2Str .= ")";
		}
		//---------------------------
		
			try{
				$Files = eCommerce_SDO_Files::DeleteFiles($id );
				$this->tpl->assign( 'strSuccess', $this->tpl->trans('the').' Files "' . $Files->getFilesId() . '" '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('deleted')));
				
			}
			catch(eCommerce_SDO_Core_Application_Exception $e) {
				//debug($e);
				$this->tpl->assign( 'strError', $e->getMessage() );
			}
		
		$this->_list();
	}
	
	
	

}
?>