<?php
class eCommerce_FrontEnd_BO_OrderPayment extends eCommerce_FrontEnd_BO {


public function getSDOSearch( $search, $extraConditions ){
	return eCommerce_SDO_OrderPayment::SearchOrderPayment( $search, $extraConditions );
}	


/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
	
	/**
	 * @var eCommerce_DAO_Event
	 */
	protected $search;
	protected $idFields;
	
	public function __construct(){
		parent::__construct();
		$this->idFields = 'order_payment_id';
	}

	public function execute(){
		/*$accessManager = new eCommerce_Access_OrderPayment( $this );
		$accessManager->checkPermission(eCommerce_Access::ORDERPAYMENT_ALL_PERMISSIONS);
		$this->checkPermission();*/
		
		$cmd = $this->getCommand();
		switch( $cmd ){
			case 'delete':
				//$accessManager->checkPermission(eCommerce_Access::ORDERPAYMENT_DELETE);
				$this->_delete();
				break;
			case 'add':
				//$accessManager->checkPermission(eCommerce_Access::ORDERPAYMENT_ADD);
				$this->_add();
				break;
			case 'edit':
				//$accessManager->checkPermission(eCommerce_Access::ORDERPAYMENT_EDIT);
				$this->_edit();
				break;
			case 'save':
				//$accessManager->checkPermission(eCommerce_Access::ORDERPAYMENT_SAVE);
				$this->_save();
				break;
			case 'list':
			default:
				//$accessManager->checkPermission(eCommerce_Access::ORDERPAYMENT_LIST);
				$this->_list();
				break;
		}
	}

	protected function _list(){

		$this->search = new eCommerce_Entity_Search(
			$this->getParameter( 'q', false, NULL ),
			$this->getParameter( 'o', false, '' ), //fecha DESC
			$this->getParameter( 'p', true, 1 ),
			$this->getParameter( 'k', true, 10 )
		);
		$extraConditions = array();
		//$extraConditions[] = array( "columName"=>"type","value"=>'',"isInteger"=>false);
		$result = $this->getSDOSearch( $this->search, $extraConditions ); 
		
		
		$exportToExcel = $this->getParameter( 'toExcel' );
		if ( $exportToExcel ){
		
			$regiters = $result->getResults();
			$data = array();
			foreach($regiters as $regiter){
				$data[] = get_object_vars( $regiter );
			}
			$this->exportToExcel( $data );

		}
		else {
			$viewConfig = array();
			$viewConfig['name'] = 'OrderPayment';
			$viewConfig['title'] = 'OrderPayment';
			$viewConfig['id'] = $this->idFields;
			
			//---------------------------
			if(strpos($viewConfig['id'],",") > -1){
				$ids = explode(",",$viewConfig['id']);
				$id = $ids[0];
				unset($ids[0]);
				$viewConfig['id'] = '{$'.$id.'}';
				foreach($ids as $id){
					$viewConfig['id'] .= ',{$'.$id.'}';
				}
			}else{
				$viewConfig['id'] = '{$'.$viewConfig['id'].'}';
			}
			//---------------------------
			
			$viewConfig["hiddenColums"]= array('order_payment_id');
			
			$this->tpl->assign( 'viewConfig', $viewConfig );
			
			$this->tpl->assign( 'options', $result );
			
			$this->tpl->display(  "OrderPayment/list.php" );
		}
	}
	
	protected function _add(){
		try{
			$OrderPayment = new eCommerce_Entity_OrderPayment();
			$OrderPayment = eCommerce_SDO_OrderPayment::SaverOrderPayment( $OrderPayment );
			$this->_edit( $OrderPayment->getOrderPaymentId() );
		}catch( eCommerce_SDO_Core_Validator_Exception $e){
			$this->tpl->assign( 'strError', $this->tpl->trans('try_again') );
			$this->_list();
		}
	}
	
	protected function _edit($id = null){
		//$id = $this->getParameter();
		$id = empty($id ) ? $this->getParameter('id',false,0) : $id;
		//---------------------------
		if( strpos($id,",") > -1 ){
			$idF = explode(",",$this->idFields);
			$ArrId = explode(",",$id);
			$id = array();
			for($i=0; $i< count($idF); $i++){
				$id[ $idF[$i] ] = (empty($ArrId[$i])) ? 0 : $ArrId[$i];
			}
		}
		//---------------------------
		
		$entity = eCommerce_SDO_OrderPayment::LoadOrderPayment( $id );
		
		
		$this->tpl->assign( 'errors', new Validator_ErrorHandler() );
		
		$this->edit( $entity, $this->getCommand() );

	}

	public function edit( $entity, $cmd ){

		$this->tpl->assign( 'object',     $entity );
		
		$viewConfig = array();
		$form = array();

		
		$form['name'] = 'OrderPaymentForm';
		$form['elements'] = array();
		
		
		/***********************************************************************************************************/
		
$form['elements'][] = array( 'title' => 'OrderPaymentId:', 'type'=>'hidden', 'options'=>array(), 'name'=>'entity[order_payment_id]', 'id'=>'order_payment_id', 'value'=> $entity->getOrderPaymentId() );
$form['elements'][] = array( 'title' => 'OrderId:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[order_id]', 'id'=>'order_id', 'value'=> $entity->getOrderId() );
$form['elements'][] = array( 'title' => 'Code:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[code]', 'id'=>'code', 'value'=> $entity->getCode() );
$form['elements'][] = array( 'title' => 'Ordertransactionid:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[ordertransactionid]', 'id'=>'ordertransactionid', 'value'=> $entity->getOrdertransactionid() );
$form['elements'][] = array( 'title' => 'Transactionid:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[transactionid]', 'id'=>'transactionid', 'value'=> $entity->getTransactionid() );
$form['elements'][] = array( 'title' => 'State:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[state]', 'id'=>'state', 'value'=> $entity->getState() );
$form['elements'][] = array( 'title' => 'Trazabilitycode:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[trazabilitycode]', 'id'=>'trazabilitycode', 'value'=> $entity->getTrazabilitycode() );
$form['elements'][] = array( 'title' => 'Authorizationcode:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[authorizationcode]', 'id'=>'authorizationcode', 'value'=> $entity->getAuthorizationcode() );
$form['elements'][] = array( 'title' => 'Responsecode:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[responsecode]', 'id'=>'responsecode', 'value'=> $entity->getResponsecode() );
$form['elements'][] = array( 'title' => 'Operationdate:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[operationdate]', 'id'=>'operationdate', 'value'=> $entity->getOperationdate() );
		/***********************************************************************************************************/
		
		//$viewConfig['id'] = 'noticia_evento_id';
		
		$viewConfig['form'] = $form;
		$viewConfig['title'] = ( $cmd =='add' ? $this->tpl->trans('add') : $this->tpl->trans('edit') ) . ' OrderPayment';
		
		$this->tpl->assign( 'form', $form );	
		$this->tpl->assign( 'viewConfig', $viewConfig );
		
		//$this->tpl->assign( 'ArrayImages', $entity->getArrayImages() );

		$this->tpl->assign( 'cancelParams', $cmd == 'add' ? '?cmd=delete&id=' . $entity->getOrderPaymentId() . '' : '' );
		$this->tpl->assign( 'cancelConfirm', $cmd == 'add' ? 'if(confirm("'.$this->tpl->trans('?_confirm_delete').' '.strtolower($this->tpl->trans('recently_created_entry')).'?"))'  : '' );
		
			$this->tpl->display( 'OrderPayment/edit.php' );
	}

	protected function exportToExcel( $data, $fileName = '' ){

		

		$rowO = $data[0];

		

		$contentHtml .= '<table border="1">';

		$contentHtml .= '<tr>

		<td height="72" width="255" bgcolor="#cccccc"><img src="'.ABS_HTTP_URL.'/ima/logo.gif"></td>

		<td colspan="'.(count($rowO) - 1).'"><h2>Order Report - '.date("M d Y").'</h2></td>

		</tr>';

		

		$contentHtml .= '<tr>';

		

		foreach($rowO as $key => $row){

			$key = str_replace("_",' ',$key);

			$key = ucwords($key);

			$contentHtml .= '<th align="center">'.$key.'</th>';

		}

		$contentHtml .= '</tr>';

		

		$i = 0;

		foreach($data as $row){

			

			$bgcolor = (++$i % 2 == 0) ? 'bgcolor="#cccccc"' : '';

			$contentHtml .= '<tr>';

			foreach($row as $rowElement){

				$contentHtml .= '<td '.$bgcolor.' align="right">'.$rowElement.'</td>';

			}

			$contentHtml .= '</tr>';

		}

		

		$contentHtml .= '</table>';

		

		

		$total_bytes = strlen($contentHtml);

		header("Content-type: application/vnd.ms-excel");

		header("Content-disposition: attachment; filename=".$fileName.".xls; size=" . $total_bytes);

		echo $contentHtml;

	}

	protected function _save(){
		
		if ( !empty( $_REQUEST ) && isset( $_REQUEST['entity'] ) ){
	  		
			$entity = $_REQUEST['entity'];
  			
			$OrderPayment = new eCommerce_Entity_OrderPayment();
			

			// Save type
			try{
			
			
			/******************************************************/
			/******************************************************/
			
$txt = (empty($entity['order_payment_id']) ? '' : $entity['order_payment_id']);$OrderPayment->setOrderPaymentId( strip_tags($txt) );
$txt = (empty($entity['order_id']) ? '' : $entity['order_id']);$OrderPayment->setOrderId( strip_tags($txt) );
$txt = (empty($entity['code']) ? '' : $entity['code']);$OrderPayment->setCode( strip_tags($txt) );
$txt = (empty($entity['ordertransactionid']) ? '' : $entity['ordertransactionid']);$OrderPayment->setOrdertransactionid( strip_tags($txt) );
$txt = (empty($entity['transactionid']) ? '' : $entity['transactionid']);$OrderPayment->setTransactionid( strip_tags($txt) );
$txt = (empty($entity['state']) ? '' : $entity['state']);$OrderPayment->setState( strip_tags($txt) );
$txt = (empty($entity['trazabilitycode']) ? '' : $entity['trazabilitycode']);$OrderPayment->setTrazabilitycode( strip_tags($txt) );
$txt = (empty($entity['authorizationcode']) ? '' : $entity['authorizationcode']);$OrderPayment->setAuthorizationcode( strip_tags($txt) );
$txt = (empty($entity['responsecode']) ? '' : $entity['responsecode']);$OrderPayment->setResponsecode( strip_tags($txt) );
$txt = (empty($entity['operationdate']) ? '' : $entity['operationdate']);$OrderPayment->setOperationdate( strip_tags($txt) );
  			/******************************************************/
  			/*****************************************************/
  			
				
				/*
				$OrderPaymentTmp = eCommerce_SDO_OrderPayment::LoadOrderPayment( $OrderPayment->getOrderPaymentId() );
				
				$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('OrderPaymentImages');
				$imageHandler->setArrImages( $OrderPaymentTmp->getArrayImages() );
				
				$imageHandler->setUploadFileDirectory('files/images/noticias/');
				$imageHandler->setUploadFileDescription('Imagen de Evento ' . $OrderPayment->getOrderPaymentId() );
				
				$imageHandler->setValidTypes( array('word','image') );
				
				$imageHandler->setAccessType( eCommerce_SDO_ImageHandler::GetValidAccessType('public') );
				
				$OrderPayment->setArrayImages( implode(',',$OrderPaymentTmp->getArrayImages()) );
				$imageHandler->setMaximum( 1 );
				$arrImages = $imageHandler->proccessImages();
				$OrderPayment->setArrayImages( $arrImages );
				*/
				
				
				eCommerce_SDO_OrderPayment::SaverOrderPayment( $OrderPayment );
				$this->tpl->assign( 'strSuccess', $this->tpl->trans('the').' OrderPayment "' . $OrderPayment->getOrderPaymentId() . '" '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('saved')));
				$this->_list();
			}
			catch( eCommerce_SDO_Core_Validator_Exception $e){
				$this->tpl->assign( 'errors', new Validator_ErrorHandler( $this->tpl->trans("there_are_errors"), $e->getErrors() ) );
				$oldCmd = $this->getParameter( 'old_cmd', false, null );
				$this->edit( $OrderPayment,  $oldCmd );
			}
		}
		else{
			throw new Exception($this->tpl->trans('not_enough_information').'. '.$this->tpl->trans('try_again'));
		}
	}

	protected function _delete(){
		//$id = $this->getParameter();
		$id = $this->getParameter('id',false,0);
		$id2Str = $id;
		//---------------------------
		if( strpos($id,",") > -1 ){
			$idF = explode(",",$this->idFields);
			$ArrId = explode(",",$id);
			$id = array();
			$id2Str = "(";
			for($i=0; $i< count($idF); $i++){
				$id[ $idF[$i] ] = (empty($ArrId[$i])) ? 0 : $ArrId[$i];
				$id2Str .=($i>0) ? ',' : '';
				$id2Str .= $ArrId[$i] . "";
			}
			$id2Str .= ")";
		}
		//---------------------------
		
			try{
				$OrderPayment = eCommerce_SDO_OrderPayment::DeleteOrderPayment($id );
				$this->tpl->assign( 'strSuccess', $this->tpl->trans('the').' OrderPayment "' . $OrderPayment->getOrderPaymentId() . '" '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('deleted')));
				
			}
			catch(eCommerce_SDO_Core_Application_Exception $e) {
				//debug($e);
				$this->tpl->assign( 'strError', $e->getMessage() );
			}
		
		$this->_list();
	}
	
	
	

}
?>