<?php
class eCommerce_FrontEnd_BO_Noticias extends eCommerce_FrontEnd_BO {


public function getSDOSearch( $search, $extraConditions ){
	return eCommerce_SDO_Noticias::SearchNoticias( $search, $extraConditions );
}	


/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
	
	/**
	 * @var eCommerce_DAO_Event
	 */
	protected $search;
	protected $idFields;
	
	public function __construct(){
		parent::__construct();
		$this->idFields = 'noticias_id';
	}

	public function execute(){
		//$accessManager = new eCommerce_Access_Noticias( $this );
		//$accessManager->checkPermission(eCommerce_Access::NOTICIAS_ALL_PERMISSIONS);
		//$this->checkPermission();
		
		$cmd = $this->getCommand();
		switch( $cmd ){
			case 'delete':
				//$accessManager->checkPermission(eCommerce_Access::NOTICIAS_DELETE);
				$this->_delete();
				break;
			case 'add':
				//$accessManager->checkPermission(eCommerce_Access::NOTICIAS_ADD);
				$this->_add();
				break;
			case 'edit':
				//$accessManager->checkPermission(eCommerce_Access::NOTICIAS_EDIT);
				$this->_edit();
				break;
			case 'save':
				//$accessManager->checkPermission(eCommerce_Access::NOTICIAS_SAVE);
				$this->_save();
				break;
			case 'list':
			default:
				//$accessManager->checkPermission(eCommerce_Access::NOTICIAS_LIST);
				$this->_list();
				break;
		}
	}

	protected function _list(){

		$this->search = new eCommerce_Entity_Search(
			$this->getParameter( 'q', false, NULL ),
			$this->getParameter( 'o', false, '' ), //fecha DESC
			$this->getParameter( 'p', true, 1 ),
			$this->getParameter( 'k', true, 10 )
		);
		$extraConditions = array();
		//$extraConditions[] = array( "columName"=>"type","value"=>'',"isInteger"=>false);
		$result = $this->getSDOSearch( $this->search, $extraConditions ); 
		
		
		$exportToExcel = $this->getParameter( 'toExcel' );
		if ( $exportToExcel ){
		
			$regiters = $result->getResults();
			$data = array();
			foreach($regiters as $regiter){
				$data[] = get_object_vars( $regiter );
			}
			$this->exportToExcel( $data );

		}
		else {
			$viewConfig = array();
			$viewConfig['name'] = 'Noticias';
			$viewConfig['title'] = 'Noticias';
			$viewConfig['id'] = $this->idFields;
			
			//---------------------------
			if(strpos($viewConfig['id'],",") > -1){
				$ids = explode(",",$viewConfig['id']);
				$id = $ids[0];
				unset($ids[0]);
				$viewConfig['id'] = '{$'.$id.'}';
				foreach($ids as $id){
					$viewConfig['id'] .= ',{$'.$id.'}';
				}
			}else{
				$viewConfig['id'] = '{$'.$viewConfig['id'].'}';
			}
			//---------------------------
			
			$viewConfig["hiddenColums"]= array('noticias_id','descripcion','imagenes','link','texto');
			
			$this->tpl->assign( 'viewConfig', $viewConfig );
			
			$this->tpl->assign( 'options', $result );
			
			$this->tpl->display(  "Noticias/list.php" );
		}
	}
	
	protected function _add(){
		try{
			$Noticias = new eCommerce_Entity_Noticias();
			//$Noticias = eCommerce_SDO_Noticias::SaverNoticias( $Noticias );
			$this->_edit( $Noticias->getNoticiasId() );
		}catch( eCommerce_SDO_Core_Validator_Exception $e){
			$this->tpl->assign( 'strError', $this->tpl->trans('try_again') );
			$this->_list();
		}
	}
	
	protected function _edit($id = null){
		//$id = $this->getParameter();
		$id = empty($id ) ? $this->getParameter('id',false,0) : $id;
		//---------------------------
		if( strpos($id,",") > -1 ){
			$idF = explode(",",$this->idFields);
			$ArrId = explode(",",$id);
			$id = array();
			for($i=0; $i< count($idF); $i++){
				$id[ $idF[$i] ] = (empty($ArrId[$i])) ? 0 : $ArrId[$i];
			}
		}
		//---------------------------
		
		$entity = eCommerce_SDO_Noticias::LoadNoticias( $id );
		
		
		$this->tpl->assign( 'errors', new Validator_ErrorHandler() );
		
		$this->edit( $entity, $this->getCommand() );

	}

	public function edit( $entity, $cmd ){

		$this->tpl->assign( 'object',     $entity );
		
		$viewConfig = array();
		$form = array();

		
		$form['name'] = 'NoticiasForm';
		$form['elements'] = array();
		
		
		/***********************************************************************************************************/
		
$form['elements'][] = array( 'title' => 'NoticiasId:', 'type'=>'hidden', 'options'=>array(), 'name'=>'entity[noticias_id]', 'id'=>'noticias_id', 'value'=> $entity->getNoticiasId() );
$form['elements'][] = array( 'title' => 'Titulo', 'type'=>'text', 'options'=>array(), 'name'=>'entity[titulo]', 'id'=>'titulo', 'value'=> $entity->getTitulo() );
$form['elements'][] = array( 'title' => 'Descripcion', 'type'=>'textarea', 'options'=>array(), 'name'=>'entity[descripcion]', 'id'=>'descripcion', 'value'=> $entity->getDescripcion(),'params'=>'style="width:100%; height:400px"' );
$form['elements'][] = array( 'title' => 'Fecha', 'type'=>'calendar', 'options'=>array(), 'name'=>'entity[fecha]', 'id'=>'fecha', 'value'=> $entity->getFecha() );
$form['elements'][] = array( 'title' => 'Link', 'type'=>'text', 'options'=>array(), 'name'=>'entity[link]', 'id'=>'link', 'value'=> $entity->getLink() );
$form['elements'][] = array( 'title' => 'Texto Link', 'type'=>'text', 'options'=>array(), 'name'=>'entity[texto]', 'id'=>'texto', 'value'=> $entity->getTexto() );
$form['elements'][] = array( 'title' => 'ImagenPrincipal', 'type'=>'array_files', 'options'=>array(), 'name'=>'entity[imagen_principal]', 'id'=>'imagen_principal', 'value'=> $entity->getImagenPrincipal(), 'max_files'=>1);
//$form['elements'][] = array( 'title' => 'Imagenes', 'type'=>'array_files', 'options'=>array(), 'name'=>'entity[imagenes]', 'id'=>'imagenes', 'value'=> $entity->getImagenes() , 'max_files'=>50);
		/***********************************************************************************************************/
		
		//$viewConfig['id'] = 'noticia_evento_id';
		
		$viewConfig['form'] = $form;
		$viewConfig['title'] = ( $cmd =='add' ? $this->tpl->trans('add') : $this->tpl->trans('edit') ) . ' Noticias';
		
		$this->tpl->assign( 'form', $form );	
		$this->tpl->assign( 'viewConfig', $viewConfig );
		
		//$this->tpl->assign( 'ArrayImages', $entity->getArrayImages() );

		$this->tpl->assign( 'cancelParams', $cmd == 'add' ? '?cmd=delete&id=' . $entity->getNoticiasId() . '' : '' );
		$this->tpl->assign( 'cancelConfirm', $cmd == 'add' ? 'if(confirm("'.$this->tpl->trans('?_confirm_delete').' '.strtolower($this->tpl->trans('recently_created_entry')).'?"))'  : '' );
		
			$this->tpl->display( 'Noticias/edit.php' );
	}

	protected function exportToExcel( $data, $fileName = '' ){

		

		$rowO = $data[0];

		

		$contentHtml .= '<table border="1">';

		$contentHtml .= '<tr>

		<td height="72" width="255" bgcolor="#cccccc"><img src="'.ABS_HTTP_URL.'/ima/logo.gif"></td>

		<td colspan="'.(count($rowO) - 1).'"><h2>Order Report - '.date("M d Y").'</h2></td>

		</tr>';

		

		$contentHtml .= '<tr>';

		

		foreach($rowO as $key => $row){

			$key = str_replace("_",' ',$key);

			$key = ucwords($key);

			$contentHtml .= '<th align="center">'.$key.'</th>';

		}

		$contentHtml .= '</tr>';

		

		$i = 0;

		foreach($data as $row){

			

			$bgcolor = (++$i % 2 == 0) ? 'bgcolor="#cccccc"' : '';

			$contentHtml .= '<tr>';

			foreach($row as $rowElement){

				$contentHtml .= '<td '.$bgcolor.' align="right">'.$rowElement.'</td>';

			}

			$contentHtml .= '</tr>';

		}

		

		$contentHtml .= '</table>';

		

		

		$total_bytes = strlen($contentHtml);

		header("Content-type: application/vnd.ms-excel");

		header("Content-disposition: attachment; filename=".$fileName.".xls; size=" . $total_bytes);

		echo $contentHtml;

	}

	protected function _save(){
		
		if ( !empty( $_REQUEST ) && isset( $_REQUEST['entity'] ) ){
	  		
			$entity = $_REQUEST['entity'];
  			
			$Noticias = new eCommerce_Entity_Noticias();
			

			// Save type
			try{
			
			
			/******************************************************/
			/******************************************************/
			
$txt = (empty($entity['noticias_id']) ? '' : $entity['noticias_id']);$Noticias->setNoticiasId( strip_tags($txt) );
$txt = (empty($entity['titulo']) ? '' : $entity['titulo']);$Noticias->setTitulo( strip_tags($txt) );
$txt = (empty($entity['descripcion']) ? '' : $entity['descripcion']);$Noticias->setDescripcion( strip_tags($txt) );
$txt = (empty($entity['fecha']) ? '' : $entity['fecha']);$Noticias->setFecha( strip_tags($txt) );
$txt = (empty($entity['link']) ? '' : $entity['link']);$Noticias->setLink( strip_tags($txt) );
$txt = (empty($entity['texto']) ? '' : $entity['texto']);$Noticias->setTexto( strip_tags($txt) );
/////////////////////////////////////////////////////////////////////////////////////////
$NoticiasTmp = eCommerce_SDO_Noticias::LoadNoticias( $Noticias->getNoticiasId() );
					$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('imagen_principal');
					$imageHandler->setArrImages( $NoticiasTmp->getImagenPrincipal() );
					
					$imageHandler->setUploadFileDirectory('files/images/noticias/');
					
					$imageHandler->setUploadFileDescription( $this->getParameter( $imageHandler->GetDescriptionField(),false,'Imagen de Noticias' .$Noticias->getNoticiasId() ) );
					
					$imageHandler->setValidTypes( array('word','image') );
					
					$imageHandler->setAccessType( eCommerce_SDO_ImageHandler::GetValidAccessType('public') );
					
					/*$arrVersions = array();
					$arrVersions[] = array('version'=>'small',	'path'=>'web/images/noticias/',	'width'=>163, 'height'=>200);
					$arrVersions[] = array('version'=>'medium',	'path'=>'web/images/noticias/',	'width'=>250, 'height'=>250);
					$arrVersions[] = array('version'=>'large',	'path'=>'web/images//noticias/',	'width'=>600, 'height'=>450);
					$imageHandler->setArrVersions($arrVersions);
					*/
					$imageHandler->setMaximum( 1 );
					$arrImages = $imageHandler->proccessImages();
					$Noticias->setImagenPrincipal( $arrImages );
/////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////
$NoticiasTmp = eCommerce_SDO_Noticias::LoadNoticias( $Noticias->getNoticiasId() );
					$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('imagenes');
					$imageHandler->setArrImages( $NoticiasTmp->getImagenes() );
					
					$imageHandler->setUploadFileDirectory('files/images/noticias/');
					
					$imageHandler->setUploadFileDescription( $this->getParameter( $imageHandler->GetDescriptionField(),false,'Imagen de Noticias' .$Noticias->getNoticiasId() ) );
					
					$imageHandler->setValidTypes( array('word','image') );
					
					$imageHandler->setAccessType( eCommerce_SDO_ImageHandler::GetValidAccessType('public') );
					
				/*	$arrVersions = array();
					$arrVersions[] = array('version'=>'small',	'path'=>'imagenes/noticias/',	'width'=>163, 'height'=>200);
					$arrVersions[] = array('version'=>'medium',	'path'=>'imagenes/noticias/',	'width'=>281, 'height'=>343);
					$arrVersions[] = array('version'=>'large',	'path'=>'imagenes/noticias/',	'width'=>1200, 'height'=>1200);
					$imageHandler->setArrVersions($arrVersions);
					*/
					$imageHandler->setMaximum( 50 );
					$arrImages = $imageHandler->proccessImages();
					$Noticias->setImagenes( $arrImages );
/////////////////////////////////////////////////////////////////////////////////////////
  			/******************************************************/
  			/*****************************************************/
  			
				
				/*
				$NoticiasTmp = eCommerce_SDO_Noticias::LoadNoticias( $Noticias->getNoticiasId() );
				
				$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('NoticiasImages');
				$imageHandler->setArrImages( $NoticiasTmp->getArrayImages() );
				
				$imageHandler->setUploadFileDirectory('files/images/noticias/');
				$imageHandler->setUploadFileDescription('Imagen de Evento ' . $Noticias->getNoticiasId() );
				
				$imageHandler->setValidTypes( array('word','image') );
				
				$imageHandler->setAccessType( eCommerce_SDO_ImageHandler::GetValidAccessType('public') );
				
				$Noticias->setArrayImages( implode(',',$NoticiasTmp->getArrayImages()) );
				$imageHandler->setMaximum( 1 );
				$arrImages = $imageHandler->proccessImages();
				$Noticias->setArrayImages( $arrImages );
				*/
				
				
				eCommerce_SDO_Noticias::SaverNoticias( $Noticias );
				$this->tpl->assign( 'strSuccess', $this->tpl->trans('the').' Noticias "' . $Noticias->getNoticiasId() . '" '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('saved')));
				$this->_list();
			}
			catch( eCommerce_SDO_Core_Validator_Exception $e){
				$this->tpl->assign( 'errors', new Validator_ErrorHandler( $this->tpl->trans("there_are_errors"), $e->getErrors() ) );
				$oldCmd = $this->getParameter( 'old_cmd', false, null );
				$this->edit( $Noticias,  $oldCmd );
			}
		}
		else{
			throw new Exception($this->tpl->trans('not_enough_information').'. '.$this->tpl->trans('try_again'));
		}
	}

	protected function _delete(){
		//$id = $this->getParameter();
		$id = $this->getParameter('id',false,0);
		$id2Str = $id;
		//---------------------------
		if( strpos($id,",") > -1 ){
			$idF = explode(",",$this->idFields);
			$ArrId = explode(",",$id);
			$id = array();
			$id2Str = "(";
			for($i=0; $i< count($idF); $i++){
				$id[ $idF[$i] ] = (empty($ArrId[$i])) ? 0 : $ArrId[$i];
				$id2Str .=($i>0) ? ',' : '';
				$id2Str .= $ArrId[$i] . "";
			}
			$id2Str .= ")";
		}
		//---------------------------
		
			try{
				$Noticias = eCommerce_SDO_Noticias::DeleteNoticias($id );
				$this->tpl->assign( 'strSuccess', $this->tpl->trans('the').' Noticias "' . $Noticias->getNoticiasId() . '" '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('deleted')));
				
			}
			catch(eCommerce_SDO_Core_Application_Exception $e) {
				//debug($e);
				$this->tpl->assign( 'strError', $e->getMessage() );
			}
		
		$this->_list();
	}
	
	
	

}
?>