<?php
class eCommerce_FrontEnd_BO_Color extends eCommerce_FrontEnd_BO {


public function getSDOSearch( $search, $extraConditions ){
	return eCommerce_SDO_Color::SearchColor( $search, $extraConditions );
}	


/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
	
	/**
	 * @var eCommerce_DAO_Event
	 */
	protected $search;
	protected $idFields;
	
	public function __construct(){
		parent::__construct();
		$this->idFields = 'color_id';
	}

	public function execute(){
		//$accessManager = new eCommerce_Access_Color( $this );
		//$accessManager->checkPermission(eCommerce_Access::COLOR_ALL_PERMISSIONS);
		//$this->checkPermission();
		
		$cmd = $this->getCommand();
		switch( $cmd ){
			case 'delete':
				//$accessManager->checkPermission(eCommerce_Access::COLOR_DELETE);
				$this->_delete();
				break;
			case 'add':
				//$accessManager->checkPermission(eCommerce_Access::COLOR_ADD);
				$this->_add();
				break;
			case 'edit':
				//$accessManager->checkPermission(eCommerce_Access::COLOR_EDIT);
				$this->_edit();
				break;
			case 'save':
				//$accessManager->checkPermission(eCommerce_Access::COLOR_SAVE);
				$this->_save();
				break;
			case 'list':
			default:
				//$accessManager->checkPermission(eCommerce_Access::COLOR_LIST);
				$this->_list();
				break;
		}
	}

	protected function _list(){
		$this->search = new eCommerce_Entity_Search(
			$this->getParameter( 'q', false, NULL ),
			$this->getParameter( 'o', false, '' ), //fecha DESC
			$this->getParameter( 'p', true, 1 ),
			$this->getParameter( 'k', true, 50 )
		);
		$extraConditions = array();
		//$extraConditions[] = array( "columName"=>"type","value"=>'',"isInteger"=>false);
		$result = $this->getSDOSearch( $this->search, $extraConditions ); 
		
		
		$exportToExcel = $this->getParameter( 'toExcel' );
		if ( $exportToExcel ){
		
			$regiters = $result->getResults();
			$data = array();
			foreach($regiters as $regiter){
				$data[] = get_object_vars( $regiter );
			}
			$this->exportToExcel( $data );

		}
		else {
			$viewConfig = array();
			$viewConfig['name'] = 'Caracteristicas';
			$viewConfig['title'] = 'Caracteristicas';
			$viewConfig['id'] = $this->idFields;
			
			//---------------------------
			if(strpos($viewConfig['id'],",") > -1){
				$ids = explode(",",$viewConfig['id']);
				$id = $ids[0];
				unset($ids[0]);
				$viewConfig['id'] = '{$'.$id.'}';
				foreach($ids as $id){
					$viewConfig['id'] .= ',{$'.$id.'}';
				}
			}else{
				$viewConfig['id'] = '{$'.$viewConfig['id'].'}';
			}
			//---------------------------
			
			$viewConfig["hiddenColums"]= array('color_id','imagen');
			
			
			$this->tpl->assign( 'viewConfig', $viewConfig );
			
			$this->tpl->assign( 'options', $result );
			
			$this->tpl->display(  "Color/list.php" );
		}
	}
	
	protected function _add(){
		try{
			$Color = new eCommerce_Entity_Color();
			//$Color = eCommerce_SDO_Color::SaverColor( $Color );
			$this->_edit( $Color->getColorId() );
		}catch( eCommerce_SDO_Core_Validator_Exception $e){
			$this->tpl->assign( 'strError', $this->tpl->trans('try_again') );
			$this->_list();
		}
	}
	
	protected function _edit($id = null){
		//$id = $this->getParameter();
		$id = empty($id ) ? $this->getParameter('id',false,0) : $id;
		//---------------------------
		if( strpos($id,",") > -1 ){
			$idF = explode(",",$this->idFields);
			$ArrId = explode(",",$id);
			$id = array();
			for($i=0; $i< count($idF); $i++){
				$id[ $idF[$i] ] = (empty($ArrId[$i])) ? 0 : $ArrId[$i];
			}
		}
		//---------------------------
		
		$entity = eCommerce_SDO_Color::LoadColor( $id );
		
		
		$this->tpl->assign( 'errors', new Validator_ErrorHandler() );
		
		$this->edit( $entity, $this->getCommand() );

	}

	public function edit( $entity, $cmd ){

		$this->tpl->assign( 'object',     $entity );
		
		$viewConfig = array();
		$form = array();

		
		$form['name'] = 'ColorForm';
		$form['elements'] = array();
		
		
		/***********************************************************************************************************/
		
$form['elements'][] = array( 'title' => 'ColorId:', 'type'=>'hidden', 'options'=>array(), 'name'=>'entity[color_id]', 'id'=>'color_id', 'value'=> $entity->getColorId() );
$form['elements'][] = array( 'title' => 'Nombre:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[nombre]', 'id'=>'nombre', 'value'=> $entity->getNombre() );
$form['elements'][] = array( 'title' => 'Especificaci&oacute;n ', 'type'=>'hidden',  'value'=> "Tama&ntilde;o de Imagen <b>M&aacute;ximo: 1000x1000 pixeles , M&iacute;nimo 41x41 pixeles</b>" );
$form['elements'][] = array( 'title' => 'Imagen:', 'type'=>'hidden', 'options'=>array(), 'name'=>'entity[imagen]', 'id'=>'imagen', 'value'=> $entity->getImagen() );
		/***********************************************************************************************************/
		
		//$viewConfig['id'] = 'noticia_evento_id';
		
		$viewConfig['form'] = $form;
		$viewConfig['title'] = ( $cmd =='add' ? $this->tpl->trans('add') : $this->tpl->trans('edit') ) . ' Caracteristica';
		
		$this->tpl->assign( 'form', $form );	
		$this->tpl->assign( 'viewConfig', $viewConfig );
		
		//$this->tpl->assign( 'ArrayImages', $entity->getArrayImages() );

		$this->tpl->assign( 'cancelParams', $cmd == 'add' ? '?cmd=delete&id=' . $entity->getColorId() . '' : '' );
		$this->tpl->assign( 'cancelConfirm', $cmd == 'add' ? 'if(confirm("'.$this->tpl->trans('?_confirm_delete').' '.strtolower($this->tpl->trans('recently_created_entry')).'?"))'  : '' );
		
			$this->tpl->display( 'Color/edit.php' );
	}

	protected function exportToExcel( $data, $fileName = '' ){

		

		$rowO = $data[0];

		

		$contentHtml .= '<table border="1">';

		$contentHtml .= '<tr>

		<td height="72" width="255" bgcolor="#cccccc"><img src="'.ABS_HTTP_URL.'/ima/logo.gif"></td>

		<td colspan="'.(count($rowO) - 1).'"><h2>Order Report - '.date("M d Y").'</h2></td>

		</tr>';

		

		$contentHtml .= '<tr>';

		

		foreach($rowO as $key => $row){

			$key = str_replace("_",' ',$key);

			$key = ucwords($key);

			$contentHtml .= '<th align="center">'.$key.'</th>';

		}

		$contentHtml .= '</tr>';

		

		$i = 0;

		foreach($data as $row){

			

			$bgcolor = (++$i % 2 == 0) ? 'bgcolor="#cccccc"' : '';

			$contentHtml .= '<tr>';

			foreach($row as $rowElement){

				$contentHtml .= '<td '.$bgcolor.' align="right">'.$rowElement.'</td>';

			}

			$contentHtml .= '</tr>';

		}

		

		$contentHtml .= '</table>';

		

		

		$total_bytes = strlen($contentHtml);

		header("Content-type: application/vnd.ms-excel");

		header("Content-disposition: attachment; filename=".$fileName.".xls; size=" . $total_bytes);

		echo $contentHtml;

	}

	protected function _save(){
		
		if ( !empty( $_REQUEST ) && isset( $_REQUEST['entity'] ) ){
	  		
			$entity = $_REQUEST['entity'];
  			
			$Color = new eCommerce_Entity_Color();
			

			// Save type
			try{
			
			
			/******************************************************/
			/******************************************************/
			
$txt = (empty($entity['color_id']) ? '' : $entity['color_id']);$Color->setColorId( strip_tags($txt) );
$txt = (empty($entity['nombre']) ? '' : $entity['nombre']);$Color->setNombre( strip_tags($txt) );
  			/******************************************************/
  			/*****************************************************/
/////////////////////////////////////////////////////////////////////////////////////////
$ColorTmp = eCommerce_SDO_Color::LoadColor( $Color->getColorId() );
$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('imagen');
$imageHandler->setArrImages( $ColorTmp->getImagen() );
	
$imageHandler->setUploadFileDirectory('files/images/color/');
$imageHandler->setUploadVersionDirectory('web/');
	
$imageHandler->setUploadFileDescription( $this->getParameter( $imageHandler->GetDescriptionField(),false,'Imagen de Color' .$Color->getColorId() ) );
	
$imageHandler->setValidTypes( array('word','image') );
	
$imageHandler->setAccessType( eCommerce_SDO_ImageHandler::GetValidAccessType('public') );
	
$arrVersions = array();
$arrVersions[] = array('version'=>'small',	'path'=>'images/color/',	'width'=>220, 'height'=>220);
$arrVersions[] = array('version'=>'medium',	'path'=>'images/color/',	'width'=>440, 'height'=>440);
$arrVersions[] = array('version'=>'large',	'path'=>'images/color/',	'width'=>880, 'height'=>880);
$imageHandler->setArrVersions($arrVersions);
	
$imageHandler->setMaximum( 1 );
$arrImages = $imageHandler->proccessImages();
$Color->setImagen( $arrImages );
/////////////////////////////////////////////////////////////////////////////////////////
				
				/*
				$ColorTmp = eCommerce_SDO_Color::LoadColor( $Color->getColorId() );
				
				$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('ColorImages');
				$imageHandler->setArrImages( $ColorTmp->getArrayImages() );
				
				$imageHandler->setUploadFileDirectory('files/images/noticias/');
				$imageHandler->setUploadFileDescription('Imagen de Evento ' . $Color->getColorId() );
				
				$imageHandler->setValidTypes( array('word','image') );
				
				$imageHandler->setAccessType( eCommerce_SDO_ImageHandler::GetValidAccessType('public') );
				
				$Color->setArrayImages( implode(',',$ColorTmp->getArrayImages()) );
				$imageHandler->setMaximum( 1 );
				$arrImages = $imageHandler->proccessImages();
				$Color->setArrayImages( $arrImages );
				*/
				
				
				eCommerce_SDO_Color::SaverColor( $Color );
				$this->tpl->assign( 'strSuccess', $this->tpl->trans('the').' Caracteristica "' . $Color->getColorId() . '" '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('saved')));
				$this->_list();
			}
			catch( eCommerce_SDO_Core_Validator_Exception $e){
				$this->tpl->assign( 'errors', new Validator_ErrorHandler( $this->tpl->trans("there_are_errors"), $e->getErrors() ) );
				$oldCmd = $this->getParameter( 'old_cmd', false, null );
				$this->edit( $Color,  $oldCmd );
			}
		}
		else{
			throw new Exception($this->tpl->trans('not_enough_information').'. '.$this->tpl->trans('try_again'));
		}
	}

	protected function _delete(){
		//$id = $this->getParameter();
		$id = $this->getParameter('id',false,0);
		$id2Str = $id;
		//---------------------------
		if( strpos($id,",") > -1 ){
			$idF = explode(",",$this->idFields);
			$ArrId = explode(",",$id);
			$id = array();
			$id2Str = "(";
			for($i=0; $i< count($idF); $i++){
				$id[ $idF[$i] ] = (empty($ArrId[$i])) ? 0 : $ArrId[$i];
				$id2Str .=($i>0) ? ',' : '';
				$id2Str .= $ArrId[$i] . "";
			}
			$id2Str .= ")";
		}
		//---------------------------
		
			try{
				$Color = eCommerce_SDO_Color::DeleteColor($id );
				$this->tpl->assign( 'strSuccess', $this->tpl->trans('the').' Caracteristica  '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('deleted')));
				
			}
			catch(eCommerce_SDO_Core_Application_Exception $e) {
				//debug($e);
				$this->tpl->assign( 'strError', $e->getMessage() );
			}
		
		$this->_list();
	}
	
	
	

}
?>