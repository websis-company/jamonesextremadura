<?php

class eCommerce_FrontEnd_BO_Version extends eCommerce_FrontEnd_BO {





public function getSDOSearch( $search, $extraConditions ){

	return eCommerce_SDO_Version::SearchVersion( $search, $extraConditions );

}	





/********************************************************************************************/

/********************************************************************************************/

/********************************************************************************************/

	

	/**

	 * @var eCommerce_DAO_Event

	 */

	protected $search;

	protected $idFields;

	

	public function __construct(){

		parent::__construct();

		$this->idFields = 'version_id';

	}



	public function execute(){

		//$accessManager = new eCommerce_Access_Version( $this );

		//$accessManager->checkPermission(eCommerce_Access::VERSION_ALL_PERMISSIONS);

		//$this->checkPermission();

		

		$cmd = $this->getCommand();

		

		switch( $cmd ){

			case 'delete':

				//$accessManager->checkPermission(eCommerce_Access::VERSION_DELETE);

				$this->_delete();

				break;

			case 'add':

				//$accessManager->checkPermission(eCommerce_Access::VERSION_ADD);

				//$this->_add();

				//break;

			case 'edit':

				//$accessManager->checkPermission(eCommerce_Access::VERSION_EDIT);

				$this->_edit();

				break;

			case 'save':

				//$accessManager->checkPermission(eCommerce_Access::VERSION_SAVE);

				$this->_save();

				break;

			case 'list':

			default:

				//$accessManager->checkPermission(eCommerce_Access::VERSION_LIST);

				$this->_list();

				break;

		}

	}



	protected function _list($tipo = ''){

		$product_id = $this->getParameter('product_id');

		$tipo = empty($tipo)?$this->getParameter('tipo',false):$tipo;

		

		$this->search = new eCommerce_Entity_Search(

			$this->getParameter( 'q', false, NULL ),

			$this->getParameter( 'o', false, '' ), //fecha DESC

			$this->getParameter( 'p', true, 1 ),

			$this->getParameter( 'k', true, -1 )

		);		

		$extraConditions = array();

		if($product_id)	$extraConditions[] = array( "SQL" => "product_id = $product_id" );

		else 			$extraConditions[] = array( "SQL" => "product_id IS NULL" );

		

		$result = $this->getSDOSearch( $this->search, $extraConditions ); 

		

		

		$exportToExcel = $this->getParameter( 'toExcel' );

		if ( $exportToExcel ){

		

			$regiters = $result->getResults();

			$data = array();

			foreach($regiters as $regiter){

				$data[] = get_object_vars( $regiter );

			}

			$this->exportToExcel( $data );



		}

		else {

			$productname = eCommerce_SDO_Core_Application_Product::LoadById($product_id)->getName();

			$viewConfig = array();

			$viewConfig['name'] = 'Versiones al Producto '.$productname;

			$viewConfig['title'] = 'Versiones del Producto '.$productname;

			$viewConfig['id'] = $this->idFields;

			

			//---------------------------

			if(strpos($viewConfig['id'],",") > -1){

				$ids = explode(",",$viewConfig['id']);

				$id = $ids[0];

				unset($ids[0]);

				$viewConfig['id'] = '{$'.$id.'}';

				foreach($ids as $id){

					$viewConfig['id'] .= ',{$'.$id.'}';

				}

			}else{				

				$viewConfig['id'] = '{$'.$viewConfig['id'].'}';

				

			}

			//---------------------------			
			$CategoriasA = eCommerce_SDO_Core_Application_ProductCategory::GetCategoriesByProduct($product_id, NULL );
				foreach ($CategoriasA as $cat){
				$categoriaActual = $cat->getCategoryId();
			}
			if ($categoriaActual==1){
				$viewConfig["hiddenColums"]= array('version_id','details','currency','description','array_images','keywords','productos_relacionados','productos_detalles','modelo','medida','especificacion','precio','orden','metro_lineal', 'costo_banner');			
			}
			else{
				$viewConfig["hiddenColums"]= array('version_id','details','currency','description','array_images','keywords','productos_relacionados','productos_detalles','modelo','medida','especificacion','precio','orden', 'precio_metro', 'bastidor');			
			}

			

			$viewConfig["columNamesOverride"] = array("product_id"=>"Producto","brand_id"=>"Marca","price"=>"Precio Venta", "discount"=>'Descuento');



			$viewConfig["hiddenFields"]= array('product_id'=>$product_id);	

			

			$this->tpl->assign( 'product_id', $product_id );

			$this->tpl->assign( 'tipo', $tipo );

			

			$this->tpl->assign( 'viewConfig', $viewConfig );

			

			$this->tpl->assign( 'options', $result );

			

			$this->tpl->display(  "Version/list.php" );

		}

	}

	

	protected function _add(){

		try{

			$Version = new eCommerce_Entity_Version();

			//$Version = eCommerce_SDO_Version::SaverVersion( $Version );

			$this->_edit( $Version->getVersionId() );

		}catch( eCommerce_SDO_Core_Validator_Exception $e){

			$this->tpl->assign( 'strError', $this->tpl->trans('try_again') );

			$this->_list();

		}

	}

	

	protected function _edit($id = null){

		$tipo = $this->getParameter('tipo',false);

		$product_id = $this->getParameter('product_id');

		$id = empty($id ) ? $this->getParameter('id',false,0) : $id;

		//---------------------------

		if( strpos($id,",") > -1 ){

			$idF = explode(",",$this->idFields);

			$ArrId = explode(",",$id);

			$id = array();

			for($i=0; $i< count($idF); $i++){

				$id[ $idF[$i] ] = (empty($ArrId[$i])) ? 0 : $ArrId[$i];

			}

		}

		//---------------------------

		

		$entity = eCommerce_SDO_Version::LoadVersion( $id );

		$this->tpl->assign("admin_tools", eCommerce_SDO_Core_Application_AdminTool::LoadById(1));

		$this->tpl->assign( 'errors', new Validator_ErrorHandler() );

		$productname = eCommerce_SDO_Core_Application_Product::LoadById($product_id)->getName();
		$CategoriasA = eCommerce_SDO_Core_Application_ProductCategory::GetCategoriesByProduct($product_id, NULL );
		foreach ($CategoriasA as $cat){
			$categoriaActual = $cat->getCategoryId();
		}
		$this->tpl->assign("categoria_actual",$categoriaActual);

		$this->edit( $entity, $this->getCommand(), $productname , $tipo,$categoriaActual);



	}



	public function edit( $entity, $cmd , $productname, $tipo,$categoriaActual){



		$this->tpl->assign( 'object',     $entity );

		$this->tpl->assign( 'tipo',     $tipo );

		

		$viewConfig = array();

		$form = array();



		

		$form['name'] = 'VersionForm';

		$form['elements'] = array();

		

		

		/***********************************************************************************************************/

		

$form['elements'][] = array( 'title' => 'VersionId:', 'type'=>'hidden', 'options'=>array(), 'name'=>'entity[version_id]', 'id'=>'version_id', 'value'=> $entity->getVersionId() );

$form['elements'][] = array( 'title' => 'Titulo:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[name]', 'id'=>'name', 'value'=> $entity->getName() );

$form['elements'][] = array( 'title' => 'Descripci&oacute;n Corta:', 'type'=>'textarea', 'options'=>array(), 'name'=>'entity[description]', 'id'=>'description', 'value'=> $entity->getDescription() );

$form['elements'][] = array( 'title' => 'Descripci&oacute;n Larga:', 'type'=>'textarea', 'options'=>array(), 'name'=>'entity[details]', 'id'=>'details', 'value'=> $entity->getDetails() );

$form['elements'][] = array( 'title' => 'Especificaciones:', 'type'=>'textarea', 'options'=>array(), 'name'=>'entity[especificacion]', 'id'=>'especificacion', 'value'=> $entity->getEspecificacion() );

$form['elements'][] = array( 'title' => 'Informaci&oacute; del producto', 'type'=>'fieldset',  'value'=> "" );

$form['elements'][] = array( 'title' => 'Sku:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[sku]', 'id'=>'sku', 'value'=> $entity->getSku() );
$form['elements'][] = array( 'title' => 'Categoria Actual:', 'type'=>'hidden', 'options'=>array(), 'name'=>'entity[categoria_actual]', 'id'=>'categoria_actual', 'value'=> $categoriaActual );

if ($categoriaActual == 1){
$form['elements'][] = array( 'title' => 'Base:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[base]', 'id'=>'base', 'value'=> $entity->getBase(),'params'=>'style="width:60px"' );	
$form['elements'][] = array( 'title' => 'Alto:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[alto]', 'id'=>'alto', 'value'=> $entity->getAlto(),'params'=>'style="width:60px"' );	
$form['elements'][] = array( 'title' => 'Metros Cuadrados:', 'type'=>'readonly', 'options'=>array(), 'name'=>'entity[metro_cuadrado]', 'id'=>'metro_cuadrado', 'value'=> $entity->getMetroCuadrado(),'params'=>'style="width:60px"' );	
$form['elements'][] = array( 'title' => 'Precio M<sup>2</sup> $:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[precio_metro]', 'id'=>'precio_metro', 'value'=> $entity->getPrecioMetro(),'params'=>'style="width:60px"' );	
$form['elements'][] = array( 'title' => 'Bastidor: $', 'type'=>'text', 'options'=>array(), 'name'=>'entity[bastidor]', 'id'=>'bastidor', 'value'=> $entity->getBastidor(),'params'=>'style="width:60px"' );	
}

if ($categoriaActual == 2){
	$form['elements'][] = array( 'title' => 'Base:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[base]', 'id'=>'base', 'value'=> $entity->getBase(),'params'=>'style="width:60px"' );	
$form['elements'][] = array( 'title' => 'Alto:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[alto]', 'id'=>'alto', 'value'=> $entity->getAlto(),'params'=>'style="width:60px"' );	
$form['elements'][] = array( 'title' => 'Metros Cuadrados:', 'type'=>'readonly', 'options'=>array(), 'name'=>'entity[metro_cuadrado]', 'id'=>'metro_cuadrado', 'value'=> $entity->getMetroCuadrado(),'params'=>'style="width:60px"' );	
$form['elements'][] = array( 'title' => 'Metros Lineales:', 'type'=>'readonly', 'options'=>array(), 'name'=>'entity[metro_lineal]', 'id'=>'metro_lineal', 'value'=> $entity->getMetroLineal(),'params'=>'style="width:60px"' );	
$form['elements'][] = array( 'title' => 'Costo Banner:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[costo_banner]', 'id'=>'costo_banner', 'value'=> $entity->getCostoBanner(),'params'=>'style="width:60px"' );	

}


$form['elements'][] = array( 'title' => 'Precio:', 'type'=>'hidden', 'options'=>array(), 'name'=>'entity[precio]', 'id'=>'precio', 'value'=> $entity->getPrecio(),'params'=>'style="width:60px"' );

$form['elements'][] = array( 'title' => 'Precio:', 'type'=>'readonly', 'options'=>array(), 'name'=>'entity[price]', 'id'=>'price', 'value'=> $entity->getPrice(),'params'=>'style="width:60px"' );

$form['elements'][] = array( 'title' => 'Descuento % [0-100]:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[discount]', 'id'=>'discount', 'value'=> $entity->getDiscount(),'params'=>'style="width:60px"' );



$form['elements'][] = array( 'title' => 'Moneda:', 'type'=>'select', 'options'=>eCommerce_SDO_Core_Application_CurrencyManager::GetCurrencies(true), 'name'=>'entity[currency]', 'id'=>'currency', 'value'=> $entity->getCurrency(),'params'=>'style="width:60px"' );

//$form['elements'][] = array( 'title' => 'Medida:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[medida]', 'id'=>'medida', 'value'=> $entity->getMedida() );

$form['elements'][] = array( 'title' => 'Modelo:', 'type'=>'hidden', 'options'=>array(), 'name'=>'entity[modelo]', 'id'=>'modelo', 'value'=> $entity->getModelo() );

$form['elements'][] = array( 'title' => 'Status:', 'type'=>'select', 'options'=>eCommerce_SDO_Core_Application_Version::GetEnumValues('status'), 'name'=>'entity[status]', 'id'=>'status', 'value'=> $entity->getStatus() );

$form['elements'][] = array( 'title' => $this->tpl->trans('brand'), 'type' => 'select', 'options' => eCommerce_SDO_Brand::GetAllInArray(true), 'name' => 'entity[brand_id]', 'id' => 'brand_id', 'value' => $entity->getBrandId() );

$form['elements'][] = array( 'title' => 'Orden de aparici&oacute;n:', 'type'=>'hidden', 'options'=>array(), 'name'=>'entity[orden]', 'id'=>'orden', 'value'=> $entity->getOrden() );

$form['elements'][] = array( 'title' => 'Keywords:', 'type'=>'hidden', 'options'=>array(), 'name'=>'entity[keywords]', 'id'=>'keywords','value'=> "");



$form['elements'][] = array( 'title' => 'Imagenes', 'type'=>'fieldset',  'value'=> "" );

$form['elements'][] = array( 'title' => 'Especificaci&oacute;n ', 'type'=>'label',  'value'=> "Tama&ntilde;o de Imagen <b>M&aacute;ximo: 1000x1000 pixeles , M&iacute;nimo 220x220 pixeles</b>" );

$form['elements'][] = array( 'title' => 'Imagen:', 'type'=>'array_files', 'options'=>array(), 'name'=>'entity[imagen]', 'id'=>'imagen', 'value'=> $entity->getImagen() );

$form['elements'][] = array( 'title' => 'Especificaci&oacute;n ', 'type'=>'label',  'value'=> "Tama&ntilde;o de Imagen <b>M&aacute;ximo: 1000x1000 pixeles , M&iacute;nimo 220x220 pixeles</b>" );

$form['elements'][] = array( 'title' => 'Imagenes Secundarias:', 'type'=>'array_files', 'options'=>array(), 'name'=>'entity[array_images]', 'id'=>'array_images', 'value'=> $entity->getArrayImages(),'max_files'=>5 );

//

/*$form['elements'][] = array( 'title' => 'Categorias', 'type'=>'fieldset',  'value'=> "" );

$Categorias = eCommerce_SDO_Catalog::GetCategoriesByParentCategory()->getResults();

$html = '';

$data = '';

$CategoriasA = eCommerce_SDO_Core_Application_ProductCategory::GetCategoriesByProduct($entity->getProductId() );

foreach($CategoriasA as $category){

	$categoriasActuales[] = $category->getCategoryId();

}

foreach($Categorias as $Categoria){

	$categoria_id = $Categoria->getCategoryId();

	$existe = (in_array($categoria_id,$categoriasActuales))?'checked="checked"':'';

	$estilo = (in_array($categoria_id,$categoriasActuales))?'style="color:red"':'';

	$name = $Categoria->getName();

	$html.= '<input name="chkPadre" type="checkbox" value="'.$categoria_id.'" id="'.$categoria_id.'" class="categorias" '.$existe.'>&nbsp;<span '.$estilo.'>'.$name.'</span>&nbsp;';

	$data .= '<div id="datacategoria'.$categoria_id.'"></div>';

}

$form['elements'][] = array( 'title' => 'Categorias', 'type'=>'htmltext',  'value'=> $html );

	

$form['elements'][] = array( 'title' => 'Categorias', 'type'=>'htmltext',  'value'=> $data );*/



$data = '<div id="datacategoria"></div>';

$form['elements'][] = array( 'title' => 'Categorias', 'type'=>'htmltext',  'value'=> $data );





$txtProducto = 'Productos Relacionados';

$tipoProducto = 'producto';

/* PRODUCTOS RELACIONADOS*/

//$form['elements'][] = array( 'title' => $txtProducto, 'type'=>'fieldset',  'value'=> "Especialidades" );



/*$Productos = eCommerce_SDO_Core_Application_Version::GetAllVersion($entity->getVersionId());



$html = '';

$html .= '<select data-placeholder="Seleccione o Busque un Producto" class="chosen-select" multiple style="width:100%;" id="productos" name="productos[]">';

$html .= '<option value=""></option>';

$ProductosActuales = explode(',',$entity->getProductosRelacionados());

foreach($Productos as $producto){

	$producto_id = $producto->getVersionId();

	$name = $producto->getName();

	$sku = $producto->getSku();

	$existe = (in_array($producto_id,$ProductosActuales))?'selected="selected"':'';

	$html .= '<option value="'.$producto_id.'" '.$existe.'>'.$name.' - SKU ('.$sku.')</option>';

}

$html .= '</select>';

$form['elements'][] = array( 'title' => 'Productos', 'type'=>'htmltext',  'value'=> $html );

$data = '<div id="dataespecialidad"></div>';*/



//DETALLES PARA TUS INVITADOS

/*

$form['elements'][] = array( 'title' => "Detalles para tus invitados", 'type'=>'fieldset',  'value'=> "Detalles" );

$Productos = eCommerce_SDO_Core_Application_Version::GetAllVersion(NULL,NULL);



$html = '';

$html .= '<select data-placeholder="Seleccione o Busque un Producto" class="chosen-select" multiple style="width:100%;" id="productos_detalles" name="productos_detalles[]">';

$html .= '<option value=""></option>';

$ProductosActualesDetalles = explode(',',$entity->getProductosDetalles());

foreach($Productos as $producto){

	$producto_id = $producto->getProductId();

	$name = $producto->getName();

	$sku = $producto->getSku();

	$existe = (in_array($producto_id,$ProductosActualesDetalles))?'selected="selected"':'';

	$html .= '<option value="'.$producto_id.'" '.$existe.'>'.$name.' - SKU ('.$sku.')</option>';

}

$html .= '</select>';

$form['elements'][] = array( 'title' => 'Productos', 'type'=>'htmltext',  'value'=> $html );

$data = '<div id="dataproductosdetalles"></div>';

*/





		/***********************************************************************************************************/

		

		//$viewConfig['id'] = 'noticia_evento_id';

		

		$viewConfig['form'] = $form;

		$viewConfig['title'] = ( $cmd =='add' ? $this->tpl->trans('add') : $this->tpl->trans('edit') ) . ' Version del Producto '.$productname;

		

		$this->tpl->assign( 'form', $form );	

		$this->tpl->assign( 'viewConfig', $viewConfig );

		$this->tpl->assign('product_id',$_REQUEST['product_id']);

		$this->tpl->assign('Keywords',$entity->getKeywords());

		//$this->tpl->assign( 'ArrayImages', $entity->getArrayImages() );



		$this->tpl->assign( 'cancelParams', $cmd == 'add' ? '?cmd=delete&id=' . $entity->getVersionId() . '' : '' );

		$this->tpl->assign( 'cancelConfirm', $cmd == 'add' ? 'if(confirm("'.$this->tpl->trans('?_confirm_delete').' '.strtolower($this->tpl->trans('recently_created_entry')).'?"))'  : '' );

		

			$this->tpl->display( 'Version/edit.php' );

	}



	protected function exportToExcel( $data, $fileName = '' ){



		



		$rowO = $data[0];



		



		$contentHtml .= '<table border="1">';



		$contentHtml .= '<tr>



		<td height="72" width="255" bgcolor="#cccccc"><img src="'.ABS_HTTP_URL.'/ima/logo.gif"></td>



		<td colspan="'.(count($rowO) - 1).'"><h2>Order Report - '.date("M d Y").'</h2></td>



		</tr>';



		



		$contentHtml .= '<tr>';



		



		foreach($rowO as $key => $row){



			$key = str_replace("_",' ',$key);



			$key = ucwords($key);



			$contentHtml .= '<th align="center">'.$key.'</th>';



		}



		$contentHtml .= '</tr>';



		



		$i = 0;



		foreach($data as $row){



			



			$bgcolor = (++$i % 2 == 0) ? 'bgcolor="#cccccc"' : '';



			$contentHtml .= '<tr>';



			foreach($row as $rowElement){



				$contentHtml .= '<td '.$bgcolor.' align="right">'.$rowElement.'</td>';



			}



			$contentHtml .= '</tr>';



		}



		



		$contentHtml .= '</table>';



		



		



		$total_bytes = strlen($contentHtml);



		header("Content-type: application/vnd.ms-excel");



		header("Content-disposition: attachment; filename=".$fileName.".xls; size=" . $total_bytes);



		echo $contentHtml;



	}



	protected function _save(){

		

		if ( !empty( $_REQUEST ) && isset( $_REQUEST['entity'] ) ){

	  		$tipo = $this->getParameter('tipo',false);

			$entity = $_REQUEST['entity'];

  			

			$Version = new eCommerce_Entity_Version();

			

			$product_id = $this->getParameter('product_id',true,null);

			$productname = eCommerce_SDO_Core_Application_Product::LoadById($product_id)->getName();



			// Save type

			try{

			

			

			/******************************************************/

			/******************************************************/

			

$txt = (empty($entity['version_id']) ? NULL: $entity['version_id']);$Version->setVersionId( strip_tags($txt) );



$txt = (empty($product_id) ? '' : $_REQUEST['product_id']);$Version->setProductId( strip_tags($txt) );



$txt = (empty($entity['name']) ? '' : $entity['name']);$Version->setName( strip_tags($txt) );

$txt = (empty($entity['description']) ? '' : $entity['description']);$Version->setDescription( strip_tags($txt) );

$txt = (empty($entity['details']) ? '' : $entity['details']);$Version->setDetails( strip_tags($txt) );

$txt = (empty($entity['especificacion']) ? '' : $entity['especificacion']);$Version->setEspecificacion( strip_tags($txt) );

$txt = (empty($entity['sku']) ? '' : $entity['sku']);$Version->setSku( strip_tags($txt) );

$txt = (empty($entity['price']) ? '' : $entity['price']);$Version->setPrice( strip_tags($txt) );

$txt = (empty($entity['discount']) ? '0' : $entity['discount']);$Version->setDiscount( strip_tags($txt) );

$txt = (empty($entity['precio']) ? '0' : $entity['precio']);$Version->setPrecio( strip_tags($txt) );

$txt = (empty($entity['currency']) ? '' : $entity['currency']);$Version->setCurrency( strip_tags($txt) );

$txt = (empty($entity['medida']) ? '' : $entity['medida']);$Version->setMedida( strip_tags($txt) );

$txt = (empty($entity['status']) ? '' : $entity['status']);$Version->setStatus( strip_tags($txt) );

$txt = (empty($entity['orden']) ? '0' : $entity['orden']);$Version->setOrden( strip_tags($txt) );
// categoria 1 

	$txt = (empty($entity['base']) ? '0' : $entity['base']);$Version->setBase( strip_tags($txt) );
	$txt = (empty($entity['alto']) ? '0' : $entity['alto']);$Version->setAlto( strip_tags($txt) );
	$txt = (empty($entity['metro_cuadrado']) ? '0' : $entity['metro_cuadrado']);$Version->setMetroCuadrado( strip_tags($txt) );
	$txt = (empty($entity['precio_metro']) ? '0' : $entity['precio_metro']);$Version->setPrecioMetro( strip_tags($txt) );
	$txt = (empty($entity['bastidor']) ? '0' : $entity['bastidor']);$Version->setBastidor( strip_tags($txt) );
	$txt = (empty($entity['metro_lineal']) ? '0' : $entity['metro_lineal']);$Version->setMetroLineal( strip_tags($txt) );
	$txt = (empty($entity['costo_banner']) ? '0' : $entity['costo_banner']);$Version->setCostoBanner( strip_tags($txt) );


$txt = (empty($entity['brand_id']) ? '' : $entity['brand_id']);$Version->setBrandId( strip_tags($txt) );



if(!empty($entity['modelo']))

	$Version->setModelo( strip_tags($entity['modelo']) );



/////////////////////////////////////////////////////////////////////////////////////////

$VersionTmp = eCommerce_SDO_Version::LoadVersion( $Version->getVersionId() );

					$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('imagen');

					$imageHandler->setArrImages( $VersionTmp->getImagen() );

					

					$imageHandler->setUploadFileDirectory('files/images/version/');

					$imageHandler->setUploadVersionDirectory('web/');

					

					$imageHandler->setUploadFileDescription( $this->getParameter( $imageHandler->GetDescriptionField(),false,'Imagen de Version' .$Version->getVersionId() ) );

					

					$imageHandler->setValidTypes( array('word','image') );

					

					$imageHandler->setAccessType( eCommerce_SDO_ImageHandler::GetValidAccessType('public') );

					

					$arrVersions = array();

					$arrVersions[] = array('version'=>'small',	'path'=>'images/version/',	'width'=>213, 'height'=>214);

					$arrVersions[] = array('version'=>'medium',	'path'=>'images/version/',	'width'=>426, 'height'=>427);

					$arrVersions[] = array('version'=>'large',	'path'=>'images/version/',	'width'=>852, 'height'=>854);

					$imageHandler->setArrVersions($arrVersions);

					

					$imageHandler->setMaximum( 1 );

					$arrImages = $imageHandler->proccessImages();

					$Version->setImagen( $arrImages );

/////////////////////////////////////////////////////////////////////////////////////////

					/**MULTIPLE IMAGES ***************************************************************/

					$VersionTmp = eCommerce_SDO_Version::LoadVersion( $Version->getVersionId() );

					$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('array_images');

					$imageHandler->setArrImages( $VersionTmp->getArrayImages() );

					$imageHandler->setUploadFileDirectory('files/images/version/');

					$imageHandler->setUploadVersionDirectory('web/');

					$imageHandler->setUploadFileDescription( $this->getParameter( $imageHandler->GetDescriptionField(),false,'Imagen de Producto ' .$Version->getVersionId() ) );

					$imageHandler->setValidTypes( array('image') );

					$imageHandler->setAccessType( eCommerce_SDO_ImageHandler::GetValidAccessType('public') );

					$arrVersions = array();

					$arrVersions[] = array('version'=>'small',	'path'=>'images/version/',	'width'=>213, 'height'=>214);

					$arrVersions[] = array('version'=>'medium',	'path'=>'images/version/',	'width'=>426, 'height'=>427);

					$arrVersions[] = array('version'=>'large',	'path'=>'images/version/',	'width'=>852, 'height'=>854);

					$imageHandler->setArrVersions($arrVersions);

					$imageHandler->setMaximum( 5 );

					$arrImages = $imageHandler->proccessImages();

					$Version->setArrayImages( $arrImages );

					/**MULTIPLE IMAGES ***************************************************************/

					

  			/******************************************************/

  			/*****************************************************/

  			

				

				/*

				$VersionTmp = eCommerce_SDO_Version::LoadVersion( $Version->getVersionId() );

				

				$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('VersionImages');

				$imageHandler->setArrImages( $VersionTmp->getArrayImages() );

				

				$imageHandler->setUploadFileDirectory('files/images/noticias/');

				$imageHandler->setUploadFileDescription('Imagen de Evento ' . $Version->getVersionId() );

				

				$imageHandler->setValidTypes( array('word','image') );

				

				$imageHandler->setAccessType( eCommerce_SDO_ImageHandler::GetValidAccessType('public') );

				

				$Version->setArrayImages( implode(',',$VersionTmp->getArrayImages()) );

				$imageHandler->setMaximum( 1 );

				$arrImages = $imageHandler->proccessImages();

				$Version->setArrayImages( $arrImages );

				*/

					

				$array = array('[','"',']');

				$Keywords = str_replace($array,'',$_REQUEST['entity']['keywords']) ;									

				$Version->setKeywords( strip_tags($Keywords) );

			

				$ProductosRelacionados = implode(',',$_REQUEST['productos']);

				$Version->setProductosRelacionados($ProductosRelacionados);

				

				eCommerce_SDO_Version::SaverVersion( $Version );

				$this->tpl->assign( 'strSuccess', 'La Version del Producto "' . $productname . '" '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('saved')));

				$this->_list($tipo);

			}

			catch( eCommerce_SDO_Core_Validator_Exception $e){

				$this->tpl->assign( 'errors', new Validator_ErrorHandler( $this->tpl->trans("there_are_errors"), $e->getErrors() ) );

				$oldCmd = $this->getParameter( 'old_cmd', false, null );

				$this->edit( $Version,  $oldCmd );

			}

		}

		else{

			throw new Exception($this->tpl->trans('not_enough_information').'. '.$this->tpl->trans('try_again'));

		}

	}



	protected function _delete(){

		//$id = $this->getParameter();

		$product_id = $this->getParameter('product_id',true,null);

		$productname = eCommerce_SDO_Core_Application_Product::LoadById($product_id)->getName();

		

		$id = $this->getParameter('id',false,0);

		$id2Str = $id;

		//---------------------------

		if( strpos($id,",") > -1 ){

			$idF = explode(",",$this->idFields);

			$ArrId = explode(",",$id);

			$id = array();

			$id2Str = "(";

			for($i=0; $i< count($idF); $i++){

				$id[ $idF[$i] ] = (empty($ArrId[$i])) ? 0 : $ArrId[$i];

				$id2Str .=($i>0) ? ',' : '';

				$id2Str .= $ArrId[$i] . "";

			}

			$id2Str .= ")";

		}

		//---------------------------

		

			try{

				$Version = eCommerce_SDO_Version::DeleteVersion($id );

				$this->tpl->assign( 'strSuccess', 'La Version del Producto "' . $productname . '" '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('deleted')));

				

			}

			catch(eCommerce_SDO_Core_Application_Exception $e) {

				//debug($e);

				$this->tpl->assign( 'strError', $e->getMessage() );

			}

		

		$this->_list();

	}

	

	

	



}

?>