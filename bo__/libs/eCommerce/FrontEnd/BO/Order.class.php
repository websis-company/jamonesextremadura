<?php

class eCommerce_FrontEnd_BO_Order extends eCommerce_FrontEnd_BO {
	
	public function __construct(){
		parent::__construct();	
	}
	
	public function execute(){

		eCommerce_FrontEnd_BO_Authentification::verifyAuthentification();
		$this->checkPermission();
		
		$cmd = $this->getCommand();

		switch( $cmd ){
			case 'save':
				$this->_save();
			break;
			case 'delete':
				$this->_delete();
			break;
			
			case 'edit':
				$this->_edit();
			break;
			case 'list':
			default:
				$this->_list();
			break;
			
		}
	}
	
	protected function _edit(){
		
		$id = $this->getParameter();
		
		//$language = eCommerce_SDO_LanguageManager::DEFAULT_LANGUAGE;
		
		$language = $this->getParameter("lang",false);
		$language = eCommerce_SDO_LanguageManager::GetValidLanguage( $language );
		
		$language = $language["id"];
		$html = eCommerce_SDO_Order::GetHTMLByOrderId( $id, $language );
		
		//$order= eCommerce_SDO_Order::LoadById( $id );
		$order = eCommerce_SDO_Order::LoadFullById( $id );
		
		$this->tpl->assign( "strSubtitles" , "Edit Order");
		$this->tpl->assign( "orderHtml" , $html);
		$this->tpl->assign( "order" , $order);
		$this->tpl->display('order/edit.php');
	
	}
	
	protected function _list(){
		
		$search = new eCommerce_Entity_Search(
			$this->getParameter( 'q', false, NULL ),
			$this->getParameter( 'o', false, 'order_id DESC' ),
			$this->getParameter( 'p', true, 1 ),
			$this->getParameter( 'k', true, 50 )
		);
		
		$result = eCommerce_SDO_Core_Application_Order::Search( $search );
		
		$exportToExcel = $this->getParameter( "toExcel" );
		if( $exportToExcel ){
			
			$orders = $result->getResults();
			$data = array();
			foreach( $orders as $order ){
				$data[] = get_object_vars( $product );
			}
			$this->exportToExcel( $data );
		}
		else{
			$this->tpl->assign( 'options', $result );
			$this->tpl->display("order/list.php");
		}
	}
	
	protected function exportToExcel( $data ){
		$xls = new ExcelWriter( 'Orders_' . date('Y-m-d'), 'Order' );
		$xls->writeHeadersAndDataFromArray( $data );
	}
	
	
	protected function _delete(){
		$id = $this->getParameter();
		if ( !empty( $id ) ) {
			try{
				$order = eCommerce_SDO_Cart::DeleteOrder( $id );

				$this->tpl->assign( 'strSuccess', 'The order "' . $order->getOrderId() . '" has been successfully deleted' );
				
			}catch(eCommerce_SDO_Core_Application_Exception $e) {
				$this->tpl->assign( 'strError', $e->getMessage() );
			}
		}
		else {
			$this->tpl->assign( 'strError', 'The order requested for deletion could not be found.' );
		}
		$this->_list();
	}
	
	protected function _save(){
		
		if ( !empty( $_REQUEST ) && isset( $_REQUEST['entity'] )  ){
	  		
			$entity = $_REQUEST['entity'];
				
	  		$order = eCommerce_SDO_Order::LoadById( $entity['id'] );
	  		
	  		$status = eCommerce_SDO_Order::GetValidStatus( $entity['status'] );
	  		
	  		$status = ($status == eCommerce_SDO_Order::INVALID_STATUS) ? $order->getStatus() : $status;
	  		$order->setStatus( $status );
	  		$order->setEnviado( $entity["enviado"] );
	  		
			// Save order
			try{

				eCommerce_SDO_Order::SaveOrder( $order );
				
			}catch( eCommerce_SDO_Core_Validator_Exception $e){
				$this->tpl->assign( 'errors', new Validator_ErrorHandler( "There are some errors", $e->getErrors() ) );
			}
			$this->_list();
		}else{
			throw new Exception('There isn\'t enough information');
		}
	}
}
?>