<?php
class eCommerce_FrontEnd_BO_Authentification extends eCommerce_FrontEnd_BO {
	const ERR_RESTRICTED_ACCESS = 303;
	const ADMIN_ROLE = "Admin";
	
	public function __construct(){
		parent::__construct();
	}
	
	public function execute(){
		//$this->checkPermission();
		
		$cmd = $this->getCommand();
		
		switch( $cmd ){
			
			case 'login':
				$this->login( $_POST["username"], $_POST["password"]);
			break;
			
			case 'logout':
			eCommerce_FrontEnd_Util_UserSession::DestroyIdentity();
			$this->redirect("index.php",false);
			break;
			
			default:
				if($this->verifyAuthentification(false)) {
					$this->tpl->display('main.php');
				} else {
					if( $this->isValidCode( $_REQUEST["errorCode"] ) ){
						
						$message = '<p><font color="#AA0000"><b>';
	
						switch( $_REQUEST["errorCode"] ){
							case eCommerce_SDO_Core_Application_Security_Exception::ERR_INVALID_PASSWORD:
							case eCommerce_SDO_Core_Application_Security_Exception::ERR_USER_NOT_FOUND:
								$message .= $this->tpl->trans('user_password_incorrect');
							break;
							
							case eCommerce_FrontEnd_BO_Authentification::ERR_RESTRICTED_ACCESS:
								$message .= $this->tpl->trans('restricted_access');
							break;
							default:
								$message .= $this->tpl->trans('system_problem');
							break;
						}
						$message .= "</b></font></p>";
						
						$this->tpl->assign( 'error', $_REQUEST["errorCode"] );
						$this->tpl->assign( 'message', $message );
					}
	
					$this->tpl->display( "user_login.php" );
				break;
			}
		}
	}
	
	public function isValidCode( &$code ){
		$valid_codes = array();

		$valid_codes[] = eCommerce_SDO_Core_Application_Security_Exception::ERR_CASE_NOT_CONTEMPLATED;
		$valid_codes[] = eCommerce_SDO_Core_Application_Security_Exception::ERR_INVALID_PASSWORD;
		$valid_codes[] = eCommerce_SDO_Core_Application_Security_Exception::ERR_SQL_EXCEPTION;
		$valid_codes[] = eCommerce_SDO_Core_Application_Security_Exception::ERR_USER_NOT_FOUND;
		$valid_codes[] = eCommerce_FrontEnd_BO_Authentification::ERR_RESTRICTED_ACCESS;
				
		return ( isset($code) & is_numeric( $code ) && in_array( $code, $valid_codes ) ); 	
	}
	
	public function login( $user, $password){
		try {
			$id = eCommerce_SDO_Security::Authenticate( $user, md5($password));
			$userProfile = new eCommerce_Entity_User_Profile();
			
			$userProfile = eCommerce_SDO_User::LoadUserProfile( $id );
			
			if( $userProfile->getRole() == eCommerce_FrontEnd_BO_Authentification::ADMIN_ROLE ){
				eCommerce_FrontEnd_Util_UserSession::SetIdentity( $userProfile );
				$this->goToFirstPage();
			}
			else{
				$this->redirect("index.php?errorCode=" . eCommerce_FrontEnd_BO_Authentification::ERR_RESTRICTED_ACCESS,false);
			}
		}
		catch(eCommerce_SDO_Core_Application_Security_Exception $e){
			$this->redirect("index.php?errorCode=" . $e->getCode(),false);
		}
		
	}
	
	public function verifyAuthentification( $dieAndRedirect = true ){

		$userProfile = eCommerce_FrontEnd_Util_UserSession::GetIdentity();
		
  		if( !isset($userProfile)  || $userProfile->getRole() != eCommerce_FrontEnd_BO_Authentification::ADMIN_ROLE ) {
  			
  			if( $dieAndRedirect ){
  				eCommerce_FrontEnd_Util_Session::Set("url_redirect_admin",  "http://" . $_SERVER['SERVER_NAME'] . $_SERVER["REQUEST_URI"]  );
  				eCommerce_FrontEnd_BO_Authentification::redirect("index.php?errorCode=" . eCommerce_FrontEnd_BO_Authentification::ERR_RESTRICTED_ACCESS,false);
  			}
  			else{
  				return false;
  			}
  		}
		return true;
		
	}

	public function goToFirstPage(){
		$url = eCommerce_FrontEnd_Util_Session::Get("url_redirect_admin");
	
		if( eCommerce_FrontEnd_BO_Authentification::verifyAuthentification( false ) 
		&& isset( $url ) ){
			eCommerce_FrontEnd_Util_Session::Delete( "url_redirect_admin" );
		}
		else{
			$url = "index.php";			
		}

		eCommerce_FrontEnd_BO_Authentification::redirect( $url,false);
	}
	
}
?>