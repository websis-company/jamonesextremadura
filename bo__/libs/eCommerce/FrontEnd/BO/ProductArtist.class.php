<?php

class eCommerce_FrontEnd_BO_ProductArtist extends eCommerce_FrontEnd_BO {

	protected $artist;

	public function __construct(){
		parent::__construct();
		
		$artist_id 	= $this->getParameter( 'artist_id');

		$this->artist = eCommerce_SDO_Artist::LoadArtist( $artist_id );
		
		if( empty( $this->artist ) ){
			
			$this->redirect("artist.php", false);
			
		}
		
	}

	public function execute(){
		eCommerce_FrontEnd_BO_Authentification::verifyAuthentification();
		$this->checkPermission();
		
		$cmd = $this->getCommand();
		switch( $cmd ){
			//case for products
			case 'listProducts2Add':
			case 'listProducts':
				$this->_listProducts();
			break;
			
			case 'addProduct':
				$this->_addProducts();
			break;
			
			case 'deleteProduct':
				$this->_deleteProduct();
			break;
		}
	}

	protected function exportToExcel( $data ){

		$xls = new ExcelWriter( 'Products_' . date('Y-m-d'), 'Products' );
		$xls->writeHeadersAndDataFromArray( $data );


	}
	
	protected function _listProducts(){
	
		$this->search = new eCommerce_Entity_Search(
			$this->getParameter( 'q', false, NULL ),
			$this->getParameter( 'o', false, 'name DESC' ),
			$this->getParameter( 'p', true, 1 ),
			$this->getParameter( 'k', true, 15 )
		);
		
		$getProductsInArtist = (  $this->getCommand() == 'listProducts' 
								||	$this->getCommand() == 'deleteProduct'
		);
		
		$result =  eCommerce_SDO_Artist::SearchProductsByArtist( $this->search, $this->artist->getArtistId() , $getProductsInArtist );
		
		$exportToExcel = $this->getParameter( "toExcel" );
		
		if( $exportToExcel ){
			$this->exportToExcel( $data );
		}else{

			$this->tpl->assign("id", $this->artist->getArtistId() );
			$this->tpl->assign("cmd", $this->getCommand() );
			$this->tpl->assign("artist", $this->artist );
			$this->tpl->assign("getProductsInArtist", $getProductsInArtist );

			$this->tpl->assign( 'result', $result );
			$this->tpl->display("artist/listProducts.php");
		
		}
	}
	
	protected function _deleteProduct(){
		$productId = (int) $this->getParameter( 'idProduct');

		$product = eCommerce_SDO_Catalog::LoadProduct( $productId );
		
		$artist_id = $this->artist->getArtistId();
		
		eCommerce_SDO_Artist::DeleteProductFromArtist( $productId, $artist_id);
		
		$this->tpl->assign("strSuccess", 'The product "' . $product->getName() . '" has been successfully deleted to the artist "' . $this->artist->getName(). '"' );
		
		$this->_listProducts();
	}
	
	protected function _addProducts(){
		
		
		$productId = (int) $this->getParameter( 'idProduct');
		
		$product = eCommerce_SDO_Catalog::LoadProduct( $productId );
		
		$artist_id = $this->artist->getArtistId();
		
		eCommerce_SDO_Artist::AddProductToArtist( $productId, $artist_id);
		
		$this->tpl->assign("strSuccess", 'Product "' . $product->getName() . '" has been successfully add to the artist "' . $this->artist->getName(). '"' );
		
		$this->_listProducts();
	}
}
?>