<?php
class eCommerce_FrontEnd_BO_Galerycategory extends eCommerce_FrontEnd_BO {
public function getSDOSearch( $search, $extraConditions ){
	return eCommerce_SDO_Galerycategory::SearchGalerycategory( $search, $extraConditions );
}	
/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
	/**
	 * @var eCommerce_DAO_Event
	 */
	protected $search;
	protected $idFields;
	protected $multipleLevels;
	public function __construct(){
		parent::__construct();
		$this->idFields = 'galerycategory_id';
		$this->search = new eCommerce_Entity_Search();
		$this->multipleLevels = '<option value="0"></option>';
	}


	public function execute(){
		$this->checkPermission();
		$cmd = $this->getCommand();
		switch( $cmd ){
			case 'delete':
				$this->_delete();
				break;
			case 'add':
			case 'edit':
				$this->_edit();
				break;
			case 'save':
				$this->_save();
				break;
			case 'list':
			default:
				$this->_list();
				break;
		}
	}


	/*protected function _list(){
		$this->search = new eCommerce_Entity_Search(
			$this->getParameter( 'q', false, NULL ),
			$this->getParameter( 'o', false, '' ), //fecha DESC
			$this->getParameter( 'p', true, 1 ),
			$this->getParameter( 'k', true, 10 )
		);
		$extraConditions = array();
		//$extraConditions[] = array( "columName"=>"type","value"=>'',"isInteger"=>false);
		$result = $this->getSDOSearch( $this->search, $extraConditions ); 
		$exportToExcel = $this->getParameter( 'toExcel' );
		if ( $exportToExcel ){
			$regiters = $result->getResults();
			$data = array();
			foreach($regiters as $regiter){
				$data[] = get_object_vars( $regiter );
			}
			$this->exportToExcel( $data );
		}
		else {
			$viewConfig = array();
			$viewConfig['name'] = 'Galerycategory';
			$viewConfig['title'] = 'Galerycategory';
			$viewConfig['id'] = $this->idFields;
			//---------------------------
			if(strpos($viewConfig['id'],",") > -1){
				$ids = explode(",",$viewConfig['id']);
				$id = $ids[0];
				unset($ids[0]);
				$viewConfig['id'] = '{$'.$id.'}';
				foreach($ids as $id){
					$viewConfig['id'] .= ',{$'.$id.'}';
				}
			}else{
				$viewConfig['id'] = '{$'.$viewConfig['id'].'}';
			}
			//---------------------------
			$viewConfig["hiddenColums"]= array('galerycategory_id');
			$this->tpl->assign( 'viewConfig', $viewConfig );
			$this->tpl->assign( 'options', $result );
			$this->tpl->display(  "Galerycategory/list.php" );
		}
	}*/

	protected function _list(){
		$currentCategoryId = $this->getParameter( 'galerycategory_id', true, 0 ); 
		
		$this->search = new eCommerce_Entity_Search(
			$this->getParameter( 'q', false, NULL ),
			$this->getParameter( 'o', false, '' ),
			$this->getParameter( 'p', true, 1 ),
			$this->getParameter( 'k', true, 50 )
		);

		$currentCategory = eCommerce_SDO_Galerycategory::LoadGalerycategory($currentCategoryId);
		
		// Search current subcategories inside current category
		$result = eCommerce_SDO_Galerycategory::SearchCategoriesByParentCategory($this->search, $currentCategoryId);

		$categories = $result->getResults();
		$data = array();
		foreach($categories as $category){
			$data[] = get_object_vars($category);
		}

		$exportToExcel = $this->getParameter( 'toExcel' );
		if ( $exportToExcel ){
			$this->exportToExcel( $data );
		}
		else {
			$this->tpl->assignRef( 'data', $data );
			$this->tpl->assign( 'options', $this->search );
			$this->tpl->assign( 'category', $currentCategory );

			if($_REQUEST["category_id"] != 0 && !empty($_REQUEST["category_id"])){
				header("location: Galerycategory.php?cmd=list&galerycategory_id=".$_REQUEST["category_id"]);
				die();
			}else{
				$this->tpl->display("Galerycategory/list.php" );	
			}
		}
	}


	protected function _add(){
		try{
			$Galerycategory = new eCommerce_Entity_Galerycategory();
			$Galerycategory = eCommerce_SDO_Galerycategory::SaverGalerycategory( $Galerycategory );
			$this->_edit( $Galerycategory->getGalerycategoryId() );
		}catch( eCommerce_SDO_Core_Validator_Exception $e){
			$this->tpl->assign( 'strError', $this->tpl->trans('try_again') );
			$this->_list();
		}
	}

	protected function getMultipleArray($parent, $sangria, $idParent){
		$search     = new eCommerce_Entity_Search(null,null,1,-1);	// -1 = ALL
		$result     = eCommerce_SDO_Galerycategory::SearchCategoriesByParentCategory($search, $parent);
		$categories = $result->getResults();

		$data = array();
		foreach($categories as $category){
			$data[] = get_object_vars($category);
		}

		foreach($data as $g_category){
			$sel = ($idParent == $g_category['galerycategory_id']) ? 'selected = "selected"' : '';
			$this->multipleLevels .= "<option value='$g_category[galerycategory_id]' $sel>$sangria$g_category[name] </option>";
			$this->getMultipleArray($g_category['galerycategory_id'], $sangria.'&nbsp;&nbsp;&nbsp;&nbsp;', $idParent);
		}
	}

	protected function _edit(){
		$id = $this->getParameter();
		$entity = eCommerce_SDO_Galerycategory::LoadGalerycategory($id);

		if ( $entity->getParentCategoryId() == 0 ){
			$entity->setParentCategoryId( $this->getParameter( 'parent_category_id'), true, 0 );
		}
		$this->tpl->assign( 'errors', new Validator_ErrorHandler() );
		$this->edit( $entity, $this->getCommand() );
	}

	public function edit( eCommerce_Entity_Galerycategory $entity, $cmd ){
		$this->getMultipleArray(0,'',$entity->getParentCategoryId());
		$ArrStatus = eCommerce_SDO_Catalog::GetArrCategoryStatus();

		$this->tpl->assign( 'ArrStatus', $ArrStatus );
		$this->tpl->assign( 'category',     $entity );
		$this->tpl->assign( 'treeCategories',    $this->multipleLevels );
		$this->tpl->assign( 'strSubtitles' , ( $cmd =='add' ? $this->tpl->trans('add') : $this->tpl->trans('edit') ) . ' '.$this->tpl->trans('category' ) );
		$this->tpl->assign( 'strCmd'       , $this->getCommand() );

		$this->tpl->display( 'Galerycategory/edit.php' );
	}


	protected function exportToExcel( $data, $fileName = '' ){
		$rowO = $data[0];
		$contentHtml .= '<table border="1">';
		$contentHtml .= '<tr>
		<td height="72" width="255" bgcolor="#cccccc"><img src="'.ABS_HTTP_URL.'/ima/logo.gif"></td>
		<td colspan="'.(count($rowO) - 1).'"><h2>Order Report - '.date("M d Y").'</h2></td>
		</tr>';
		$contentHtml .= '<tr>';
		foreach($rowO as $key => $row){
			$key = str_replace("_",' ',$key);
			$key = ucwords($key);
			$contentHtml .= '<th align="center">'.$key.'</th>';
		}
		$contentHtml .= '</tr>';
		$i = 0;
		foreach($data as $row){
			$bgcolor = (++$i % 2 == 0) ? 'bgcolor="#cccccc"' : '';
			$contentHtml .= '<tr>';
			foreach($row as $rowElement){
				$contentHtml .= '<td '.$bgcolor.' align="right">'.$rowElement.'</td>';
			}
			$contentHtml .= '</tr>';
		}
		$contentHtml .= '</table>';
		$total_bytes = strlen($contentHtml);
		header("Content-type: application/vnd.ms-excel");
		header("Content-disposition: attachment; filename=".$fileName.".xls; size=" . $total_bytes);
		echo $contentHtml;
	}


	protected function _save(){
		if ( !empty( $_REQUEST ) && isset( $_REQUEST['entity'] ) ){
			$entity = $_REQUEST['entity'];
			$Galerycategory = new eCommerce_Entity_Galerycategory();
			// Save type
			try{
			/******************************************************/
			/******************************************************/

			$txt = (empty($entity['galerycategory_id']) ? '0' : $entity['galerycategory_id']);
			$Galerycategory->setGalerycategoryId( strip_tags($txt) );

			$txt = (empty($entity['parent_category_id']) ? NULL : $entity['parent_category_id']);
			$Galerycategory->setParentCategoryId($txt);
			
			$txt = (empty($entity['name']) ? '' : $entity['name']);$Galerycategory->setName( strip_tags($txt) );
			$txt = (empty($entity['description']) ? '' : $entity['description']);$Galerycategory->setDescription( strip_tags($txt) );
			$txt = (empty($entity['image_id']) ? '' : $entity['image_id']);$Galerycategory->setImageId( strip_tags($txt) );
			$txt = (empty($entity['orden']) ? '' : $entity['orden']);$Galerycategory->setOrden( strip_tags($txt) );
			$txt = (empty($entity['status']) ? '' : $entity['status']);$Galerycategory->setStatus( strip_tags($txt) );

			if( !empty($entity["banner_delete"])) {
				$Galerycategory->setBannerId('');
			} elseif( !empty($_FILES["banner_file"]["tmp_name"]) ){
				$imageFile = new eCommerce_Entity_Util_FileUpload( "banner_file" );
				$imageFile = eCommerce_SDO_Galerycategory::SaveCategoryImage( $imageFile );
				$Galerycategory->setBannerId( $imageFile->getId() );
			}else{
				$GalerycategoryTmp = eCommerce_SDO_Galerycategory::LoadGalerycategory( $Galerycategory->getGalerycategoryId() );
				$Galerycategory->setBannerId( $GalerycategoryTmp->getBannerId() );
			}

  			/******************************************************/
  			/*****************************************************/
			/*
			$GalerycategoryTmp = eCommerce_SDO_Galerycategory::LoadGalerycategory( $Galerycategory->getGalerycategoryId() );
			$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('GalerycategoryImages');
			$imageHandler->setArrImages( $GalerycategoryTmp->getArrayImages() );
			$imageHandler->setUploadFileDirectory('files/images/noticias/');
			$imageHandler->setUploadFileDescription('Imagen de Evento ' . $Galerycategory->getGalerycategoryId() );
			$imageHandler->setValidTypes( array('word','image') );
			$imageHandler->setAccessType( eCommerce_SDO_ImageHandler::GetValidAccessType('public') );
			$Galerycategory->setArrayImages( implode(',',$GalerycategoryTmp->getArrayImages()) );
			$imageHandler->setMaximum( 1 );
			$arrImages = $imageHandler->proccessImages();
			$Galerycategory->setArrayImages( $arrImages );
			*/

				eCommerce_SDO_Galerycategory::SaverGalerycategory( $Galerycategory );
				$this->tpl->assign( 'strSuccess', $this->tpl->trans('the').' Galerycategory "' . $Galerycategory->getGalerycategoryId() . '" '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('saved')));
				$this->_list();
			}
			catch( eCommerce_SDO_Core_Validator_Exception $e){
				$this->tpl->assign( 'errors', new Validator_ErrorHandler( $this->tpl->trans("there_are_errors"), $e->getErrors() ) );
				$oldCmd = $this->getParameter( 'old_cmd', false, null );
				$this->edit( $Galerycategory,  $oldCmd );
			}
		}
		else{
			throw new Exception($this->tpl->trans('not_enough_information').'. '.$this->tpl->trans('try_again'));
		}
	}


	protected function _delete(){
		//$id = $this->getParameter();
		$id = $this->getParameter('id',false,0);
		$id2Str = $id;
		//---------------------------
		if( strpos($id,",") > -1 ){
			$idF = explode(",",$this->idFields);
			$ArrId = explode(",",$id);
			$id = array();
			$id2Str = "(";
			for($i=0; $i< count($idF); $i++){
				$id[ $idF[$i] ] = (empty($ArrId[$i])) ? 0 : $ArrId[$i];
				$id2Str .=($i>0) ? ',' : '';
				$id2Str .= $ArrId[$i] . "";
			}
			$id2Str .= ")";
		}
		//---------------------------
			try{
				$Galerycategory = eCommerce_SDO_Galerycategory::DeleteGalerycategory($id );
				$this->tpl->assign( 'strSuccess', $this->tpl->trans('the').' Galerycategory "' . $Galerycategory->getGalerycategoryId() . '" '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('deleted')));
			}
			catch(eCommerce_SDO_Core_Application_Exception $e) {
				//debug($e);
				$this->tpl->assign( 'strError', $e->getMessage() );
			}
		$this->_list();
	}


}
?>