<?php
class eCommerce_FrontEnd_BO_Brand extends eCommerce_FrontEnd_BO {


public function getSDOSearch( $search, $extraConditions ){
	return eCommerce_SDO_Brand::SearchBrand( $search, $extraConditions );
}	


/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
	
	/**
	 * @var eCommerce_DAO_Event
	 */
	protected $search;
	protected $idFields;
	
	public function __construct(){
		parent::__construct();
		$this->idFields = 'brand_id';
	}

	public function execute(){
		$accessManager = new eCommerce_Access_Brand( $this );
		$accessManager->checkPermission(eCommerce_Access::BRAND_ALL_PERMISSIONS);
		$this->checkPermission();
		
		$cmd = $this->getCommand();
		switch( $cmd ){
			case 'delete':
				$accessManager->checkPermission(eCommerce_Access::BRAND_DELETE);
				$this->_delete();
				break;
			case 'add':
				$accessManager->checkPermission(eCommerce_Access::BRAND_ADD);
				$this->_edit();
				break;
			case 'edit':
				$accessManager->checkPermission(eCommerce_Access::BRAND_EDIT);
				$this->_edit();
				break;
			case 'save':
				$accessManager->checkPermission(eCommerce_Access::BRAND_SAVE);
				$this->_save();
				break;
			case 'list':
			default:
				$accessManager->checkPermission(eCommerce_Access::BRAND_LIST);
				$this->_list();
				break;
		}
	}

	protected function _list(){

		$this->search = new eCommerce_Entity_Search(
			$this->getParameter( 'q', false, NULL ),
			$this->getParameter( 'o', false, '' ), //fecha DESC
			$this->getParameter( 'p', true, 1 ),
			$this->getParameter( 'k', true, 10 )
		);
		$extraConditions = array();
		//$extraConditions[] = array( "columName"=>"type","value"=>'',"isInteger"=>false);
		$result = $this->getSDOSearch( $this->search, $extraConditions ); 
		
		
		$exportToExcel = $this->getParameter( 'toExcel' );
		if ( $exportToExcel ){
		
			$regiters = $result->getResults();
			$data = array();
			foreach($regiters as $regiter){
				$data[] = get_object_vars( $regiter );
			}
			$this->exportToExcel( $data );

		}
		else {
			$viewConfig = array();
			$viewConfig['name'] = $this->tpl->trans('brand');
			$viewConfig['title'] = $this->tpl->trans('brand').'s';
			$viewConfig['id'] = $this->idFields;
			
			//---------------------------
			if(strpos($viewConfig['id'],",") > -1){
				$ids = explode(",",$viewConfig['id']);
				$id = $ids[0];
				unset($ids[0]);
				$viewConfig['id'] = '{$'.$id.'}';
				foreach($ids as $id){
					$viewConfig['id'] .= ',{$'.$id.'}';
				}
			}else{
				$viewConfig['id'] = '{$'.$viewConfig['id'].'}';
			}
			//---------------------------
			
			$viewConfig["hiddenColums"]= array('brand_id','image');
			$viewConfig["columNamesOverride"]= array('name'=>$this->tpl->trans('name'), 'image'=>$this->tpl->trans('image'));
			
			$this->tpl->assign( 'viewConfig', $viewConfig );
			
			$this->tpl->assign( 'options', $result );
			
			$this->tpl->display(  "Brand/list.php" );
		}
	}
		
	protected function _edit($id = null){
		//$id = $this->getParameter();
		$id = empty($id ) ? $this->getParameter('id',false,0) : $id;
		//---------------------------
		if( strpos($id,",") > -1 ){
			$idF = explode(",",$this->idFields);
			$ArrId = explode(",",$id);
			$id = array();
			for($i=0; $i< count($idF); $i++){
				$id[ $idF[$i] ] = (empty($ArrId[$i])) ? 0 : $ArrId[$i];
			}
		}
		//---------------------------
		
		$entity = eCommerce_SDO_Brand::LoadBrand( $id );
		
		
		$this->tpl->assign( 'errors', new Validator_ErrorHandler() );
		
		$this->edit( $entity, $this->getCommand() );

	}

	public function edit( $entity, $cmd ){

		$this->tpl->assign( 'object',     $entity );
		
		$viewConfig = array();
		$form = array();

		
		$form['name'] = 'BrandForm';
		$form['elements'] = array();
		
		
		/***********************************************************************************************************/
		
$form['elements'][] = array( 'title' => 'BrandId', 'type'=>'hidden', 'options'=>array(), 'name'=>'entity[brand_id]', 'id'=>'brand_id', 'value'=> $entity->getBrandId() );
$form['elements'][] = array( 'title' => $this->tpl->trans('name').': *', 'type'=>'text', 'options'=>array(), 'name'=>'entity[name]', 'id'=>'name', 'value'=> $entity->getName() );
$form['elements'][] = array( 'title' => $this->tpl->trans('image').':', 'type'=>'hidden', 'options'=>array(), 'name'=>'entity[image]', 'id'=>'image', 'value'=> $entity->getImage() );
		/***********************************************************************************************************/
		
		//$viewConfig['id'] = 'noticia_evento_id';
		
		$viewConfig['form'] = $form;
		$viewConfig['title'] = ( $cmd =='add' ? $this->tpl->trans('add') : $this->tpl->trans('edit') ) . ' '.$this->tpl->trans('brand');
		
		$this->tpl->assign( 'form', $form );	
		$this->tpl->assign( 'viewConfig', $viewConfig );
		
		//$this->tpl->assign( 'ArrayImages', $entity->getArrayImages() );
		
		$this->tpl->display( 'Brand/edit.php' );
	}

	protected function exportToExcel( $data, $fileName = '' ){

		

		$rowO = $data[0];

		

		$contentHtml .= '<table border="1">';

		$contentHtml .= '<tr>

		<td height="72" width="255" bgcolor="#cccccc"><img src="'.ABS_HTTP_URL.'/ima/logo.gif"></td>

		<td colspan="'.(count($rowO) - 1).'"><h2>Order Report - '.date("M d Y").'</h2></td>

		</tr>';

		

		$contentHtml .= '<tr>';

		

		foreach($rowO as $key => $row){

			$key = str_replace("_",' ',$key);

			$key = ucwords($key);

			$contentHtml .= '<th align="center">'.$key.'</th>';

		}

		$contentHtml .= '</tr>';

		

		$i = 0;

		foreach($data as $row){

			

			$bgcolor = (++$i % 2 == 0) ? 'bgcolor="#cccccc"' : '';

			$contentHtml .= '<tr>';

			foreach($row as $rowElement){

				$contentHtml .= '<td '.$bgcolor.' align="right">'.$rowElement.'</td>';

			}

			$contentHtml .= '</tr>';

		}

		

		$contentHtml .= '</table>';

		

		

		$total_bytes = strlen($contentHtml);

		header("Content-type: application/vnd.ms-excel");

		header("Content-disposition: attachment; filename=".$fileName.".xls; size=" . $total_bytes);

		echo $contentHtml;

	}

	protected function _save(){
		
		if ( !empty( $_REQUEST ) && isset( $_REQUEST['entity'] ) ){
	  		
			$entity = $_REQUEST['entity'];
  			
			$Brand = new eCommerce_Entity_Brand();
			

			// Save type
			try{
			
			
			/******************************************************/
			/******************************************************/
			
$txt = (empty($entity['brand_id']) ? '' : $entity['brand_id']);$Brand->setBrandId( strip_tags($txt) );
$txt = (empty($entity['name']) ? '' : $entity['name']);$Brand->setName( strip_tags($txt) );

/////////////////////////////////////////////////////////////////////////////////////////
$BrandTmp = eCommerce_SDO_Brand::LoadBrand( $Brand->getBrandId() );
					$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('image');
					$imageHandler->setArrImages( $BrandTmp->getImage() );
					
					$imageHandler->setUploadFileDirectory('files/images/brands/');
					
					$imageHandler->setUploadFileDescription( $this->getParameter( $imageHandler->GetDescriptionField(),false,'Imagen de Brand' .$Brand->getBrandId() ) );
					
					$imageHandler->setValidTypes( array('word','image') );
					
					$imageHandler->setAccessType( eCommerce_SDO_ImageHandler::GetValidAccessType('public') );
					
					$imageHandler->setMaximum( 1 );
					$arrImages = $imageHandler->proccessImages();
					$Brand->setImage( $arrImages );
/////////////////////////////////////////////////////////////////////////////////////////
  			/******************************************************/
  			/*****************************************************/
  			
				
				/*
				$BrandTmp = eCommerce_SDO_Brand::LoadBrand( $Brand->getBrandId() );
				
				$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('BrandImages');
				$imageHandler->setArrImages( $BrandTmp->getArrayImages() );
				
				$imageHandler->setUploadFileDirectory('files/images/noticias/');
				$imageHandler->setUploadFileDescription('Imagen de Evento ' . $Brand->getBrandId() );
				
				$imageHandler->setValidTypes( array('word','image') );
				
				$imageHandler->setAccessType( eCommerce_SDO_ImageHandler::GetValidAccessType('public') );
				
				$Brand->setArrayImages( implode(',',$BrandTmp->getArrayImages()) );
				$imageHandler->setMaximum( 1 );
				$arrImages = $imageHandler->proccessImages();
				$Brand->setArrayImages( $arrImages );
				*/
				
				
				eCommerce_SDO_Brand::SaverBrand( $Brand );
				$this->tpl->assign( 'strSuccess', $this->tpl->trans('the(f)').' '.$this->tpl->trans('brand').' "' . $Brand->getName() . '" '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('saved')));
				$this->_list();
			}
			catch( eCommerce_SDO_Core_Validator_Exception $e){
				$this->tpl->assign( 'errors', new Validator_ErrorHandler( $this->tpl->trans("there_are_errors"), $e->getErrors() ) );
				$oldCmd = $this->getParameter( 'old_cmd', false, null );
				$this->edit( $Brand,  $oldCmd );
			}
		}
		else{
			throw new Exception($this->tpl->trans('not_enough_information').'. '.$this->tpl->trans('try_again'));
		}
	}

	protected function _delete(){
		//$id = $this->getParameter();
		$id = $this->getParameter('id',false,0);
		$id2Str = $id;
		//---------------------------
		if( strpos($id,",") > -1 ){
			$idF = explode(",",$this->idFields);
			$ArrId = explode(",",$id);
			$id = array();
			$id2Str = "(";
			for($i=0; $i< count($idF); $i++){
				$id[ $idF[$i] ] = (empty($ArrId[$i])) ? 0 : $ArrId[$i];
				$id2Str .=($i>0) ? ',' : '';
				$id2Str .= $ArrId[$i] . "";
			}
			$id2Str .= ")";
		}
		//---------------------------
		
			try{
				$Brand = eCommerce_SDO_Brand::DeleteBrand($id );
				$this->tpl->assign( 'strSuccess', $this->tpl->trans('the').' '.$this->tpl->trans('brand').' "' . $Brand->getName() . '" '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('deleted')));
				
			}
			catch(eCommerce_SDO_Core_Application_Exception $e) {
				//debug($e);
				$this->tpl->assign( 'strError', $e->getMessage() );
			}
		
		$this->_list();
	}
	
	
	

}
?>