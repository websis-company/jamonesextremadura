<?php

class eCommerce_FrontEnd_BO_Artist extends eCommerce_FrontEnd_BO {

	/**
	 * @var eCommerce_DAO_Event
	 */
	protected $search;
	
	public function __construct(){
		parent::__construct();
		$this->search = new eCommerce_Entity_Search();
	}

	public function execute(){
		eCommerce_FrontEnd_BO_Authentification::verifyAuthentification();
		$this->checkPermission();
		$cmd = $this->getCommand();
		switch( $cmd ){
			case 'delete':
				$this->_delete();
				break;
			case 'add':
			case 'edit':
				$this->_edit();
				break;
			case 'save':
				$this->_save();
				break;

			case 'list':
			default:
				$this->_list();
				break;
		}
	}

	protected function _list(){
		$currentArtistId = $this->getParameter( 'artist_id', true, 0 ); 
		
		$this->search = new eCommerce_Entity_Search(
			$this->getParameter( 'q', false, NULL ),
			$this->getParameter( 'o', false, 'name' ),
			$this->getParameter( 'p', true, 1 ),
			$this->getParameter( 'k', true, 15 )
		);
		
		$currentArtist = eCommerce_SDO_Artist::LoadArtist( $currentArtistId );
		$result = eCommerce_SDO_Artist::SearchArtists( $this->search );
		

		$artists = $result->getResults();
		$data = array();
		foreach($artists as $artist){
			$data[] = get_object_vars($artist);
		}

		$exportToExcel = $this->getParameter( 'toExcel' );
		if ( $exportToExcel ){
			$this->exportToExcel( $data );
		}
		else {
			$this->tpl->assignRef( 'data', $data );
			$this->tpl->assign( 'options', $this->search );
			$this->tpl->assign( 'artist', $currentArtist );
			
			$this->tpl->display(  "artist/list.php" );
		}
	}

	
	protected function _edit(){
		$id = $this->getParameter();
		$entity = eCommerce_SDO_Artist::LoadArtist($id);
		
		$this->tpl->assign( 'errors', new Validator_ErrorHandler() );
		$this->edit( $entity, $this->getCommand() );

	}

	public function edit( eCommerce_Entity_Catalog_Artist $entity, $cmd ){

		$this->tpl->assign( 'artist',     $entity );
		$this->tpl->assign( 'strSubtitles' , ( $cmd =='add' ? 'Add' : 'Edit' ) . ' Artist' );
		$this->tpl->assign( 'strCmd'       , $this->getCommand() );
		$this->tpl->display( 'artist/edit.php' );
	}

	protected function exportToExcel( $data ){

		$xls = new ExcelWriter( 'Events_' . date('Y-m-d'), 'Products' );
		$xls->writeHeadersAndDataFromArray( $data );


	}

	protected function _save(){
		if ( !empty( $_REQUEST ) && isset( $_REQUEST['entity'] ) ){
  		$entity = $_REQUEST['entity'];

  			$artist = new eCommerce_Entity_Catalog_Artist();
			$artist->setArtistId($entity['artist_id']);
			$artist->setName($entity['name']);
			$artist->setEmail($entity['email']);
			$artist->setUrl($entity['url']);
			$artist->setType($entity['type']);
			
			$artist->setBiographyEn($entity['biography_en']);
			$artist->setBiographySp($entity['biography_sp']);
			$artist->setImageId($entity['image_id']);

			// Save category
			try{
				
				if( !empty($_FILES["image_id"]["tmp_name"]) ){
					$imageFile = new eCommerce_Entity_Util_FileUpload( "image_id" );
					$imageFile = eCommerce_SDO_Artist::SaveArtistImage( $imageFile );
					$artist->setImageId( $imageFile->getId() );
				}
				
				eCommerce_SDO_Artist::SaveArtist( $artist );
				$this->_list();
			}catch( eCommerce_SDO_Core_Validator_Exception $e){
				$this->tpl->assign( 'errors', new Validator_ErrorHandler( 'Please see errors below:', $e->getErrors() ) );
				$oldCmd = $this->getParameter( 'old_cmd', false, null );
				$this->edit( $artist,  $oldCmd );
			}
		}else{
			throw new Exception('There isn\'t enough information');
		}
	}
	

	protected function _delete(){
		$id = $this->getParameter();
		if ( !empty( $id ) ) {
			try{

				$artist = eCommerce_SDO_Core_Application_Artist::Delete($id );
				$this->tpl->assign( 'strSuccess', 'The category "' . $artist->getName() . '" has been successfully deleted' );
			}catch(eCommerce_SDO_Core_Application_Exception $e) {
				//debug($e);
				$this->tpl->assign( 'strError', $e->getMessage() );
			}
		}
		else {
			$this->tpl->assign( 'strError', 'The category requested for deletion could not be found.' );
		}
		$this->_list();
	}

}
?>