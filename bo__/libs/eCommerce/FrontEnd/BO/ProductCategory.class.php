<?php

class eCommerce_FrontEnd_BO_ProductCategory extends eCommerce_FrontEnd_BO {

	protected $category;

	public function __construct(){
		parent::__construct();
		
		$category_id 	= $this->getParameter( 'category_id');

		$this->category = eCommerce_SDO_Catalog::LoadCategory( $category_id );
		
		if( empty( $this->category ) ){
			
			$this->redirect("category.php", false);
			
		}
		
	}

	public function execute(){
		
		eCommerce_FrontEnd_BO_Authentification::verifyAuthentification();
		$this->checkPermission();
		
		$cmd = $this->getCommand();
		switch( $cmd ){
			//case for products
			case 'listProducts2Add':
			case 'listProducts':
				$this->_listProducts();
			break;
			
			case 'addProduct':
				$this->_addProducts();
			break;
			
			case 'deleteProduct':
				$this->_deleteProduct();
			break;
		}
	}

	protected function exportToExcel( $data ){

		$xls = new ExcelWriter( 'Products_' . date('Y-m-d'), 'Products' );
		$xls->writeHeadersAndDataFromArray( $data );


	}
	
	protected function _listProducts(){
	
		$this->search = new eCommerce_Entity_Search(
			$this->getParameter( 'q', false, NULL ),
			$this->getParameter( 'o', false, 'name DESC' ),
			$this->getParameter( 'p', true, 1 ),
			$this->getParameter( 'k', true, 50 )
		);
		
		$getProductsInCategory = (  $this->getCommand() == 'listProducts' 
								||	$this->getCommand() == 'deleteProduct'
		);
		
		
		$parent = $this->category->getParentCategoryId();
		$result =  eCommerce_SDO_Catalog::SearchProducts( $this->search, $this->category->getCategoryId() , $getProductsInCategory );
		
		$exportToExcel = $this->getParameter( "toExcel" );
		
		if( $exportToExcel ){
			$this->exportToExcel( $data );
		}else{

			$this->tpl->assign("id", $this->category->getCategoryId() );
			$this->tpl->assign("cmd", $this->getCommand() );
			$this->tpl->assign("category", $this->category );
			$this->tpl->assign("parent", $parent );
			$this->tpl->assign("getProductsInCategory", $getProductsInCategory );

			$this->tpl->assign( 'result', $result );
			$this->tpl->display("category/listProducts.php");
		
		}
	}
	
	protected function _deleteProduct(){
		$productId = (int) $this->getParameter( 'idProduct');

		$product = eCommerce_SDO_Catalog::LoadProduct( $productId );
		
		$category_id = $this->category->getCategoryId();
		
		eCommerce_SDO_Catalog::DeleteProductFromCategory( $productId, $category_id);
		
		$this->tpl->assign('strSuccess', $this->tpl->trans('the').' '.$this->tpl->trans('product').' "' . $product->getName() . '" '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('deleted')).' '.$this->tpl->trans('from').' '.strtolower($this->tpl->trans('the(f)')).' '.$this->tpl->trans('category'));
		
		$this->_listProducts();
	}
	
	protected function _addProducts(){
		
		
		$productId = (int) $this->getParameter( 'idProduct');
		
		$product = eCommerce_SDO_Catalog::LoadProduct( $productId );
		
		$category_id = $this->category->getCategoryId();
		
		eCommerce_SDO_Catalog::AddProductToCategory( $productId, $category_id);
		
		$this->tpl->assign('strSuccess', $this->tpl->trans('the').' '.$this->tpl->trans('product').' "' . $product->getName() . '" '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('added')).' '.$this->tpl->trans('to').' '.strtolower($this->tpl->trans('the(f)')).' '.$this->tpl->trans('category'));
		
		$this->_listProducts();
	}
}
?>