<?php
class eCommerce_SDO_Order{
	const INVALID_STATUS = eCommerce_SDO_Core_Application_Order::INVALID_STATUS;
	
	const ADDRESS_SHIP_TO = eCommerce_SDO_Core_Application_Order::ADDRESS_SHIP_TO;
	const ADDRESS_BILL_TO = eCommerce_SDO_Core_Application_Order::ADDRESS_BILL_TO;
	
	public static function GetHTMLByOrderId( $orderId, $language=null ){
		return eCommerce_SDO_Core_Application_Order::GetHTMLByOrderId( $orderId, $language );
	}
	
	public static function LoadFullById( $orderId ){
		return eCommerce_SDO_Core_Application_OrderMediation::CreateOrderFullFromOrderId( $orderId );
	}
	
	
	public static function LoadById( $orderId ){
		return eCommerce_SDO_Core_Application_Order::LoadById( $orderId );
	}

	public static function SaveOrder( $order){
		return eCommerce_SDO_Core_Application_Order::Save( $order );
	}
	
	public static function GetValidStatus( $statusId ){
		return eCommerce_SDO_Core_Application_Order::GetValidStatus( $statusId );
	}
	
	public static function GetArrStatus(){
		return eCommerce_SDO_Core_Application_Order::GetArrStatus();
	}
		
	public static function GetStatusById( $statusId ){
		return eCommerce_SDO_Core_Application_Order::GetStatusById( $statusId );
	}
	
	public static function GetPaymentMethodById( $paymentId ){
		return eCommerce_SDO_Core_Application_Order::GetPaymentMethodById( $paymentId );
	}
	
	public static function GetArrPaymentMethods(){
		return eCommerce_SDO_Core_Application_Order::GetArrPaymentMethods();
	}
	
	public static function GetEnumValues( $colum ){
		if($colum == 'shipping_type'){
			return array('fijo' => 'fijo');
		}
		return eCommerce_SDO_Core_Application_Order::GetEnumValues($colum);
	}
	
}
?>