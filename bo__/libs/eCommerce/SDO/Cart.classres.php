<?php
class eCommerce_SDO_Cart{
	const ADDRESS_SHIP_TO = eCommerce_SDO_Core_Application_Cart::ADDRESS_SHIP_TO;
	const ADDRESS_BILL_TO = eCommerce_SDO_Core_Application_Cart::ADDRESS_BILL_TO;
	/**
	 * Obtain the actual cart
	 * @return eCommerce_Entity_Cart
	 */
	public static function GetCart(){
		return eCommerce_SDO_Core_Application_Cart::GetCart();
	}
	
	public static function addItem( $productId, $quantity, $color, $talla){
		$result = false;
		try{
			eCommerce_SDO_Core_Application_Cart::AddItem( $productId, $quantity, $color, $talla );
			$result = true;
		}
		catch( eCommerce_SDO_Core_Application_Exception $e ){
			if( $e->getCode() == 100 ){
				//if the product exists we need add the adicional quantity
			}
		}
		return $result;
	}
	
	public static function RemoveItem( $productId ){
		$result = false;
		try{
			eCommerce_SDO_Core_Application_Cart::RemoveItem( $productId );
			$result = true;
		}
		catch( eCommerce_SDO_Core_Application_Exception $e ){}
		return $result;
	}
	
	public static function UpdateQuantity( $productId, $newQuantity ){
		try{
		 eCommerce_SDO_Core_Application_Cart::UpdateQuantity( $productId, $newQuantity );
		}
		catch(eCommerce_SDO_Core_Application_Exception $e){
			throw new eCommerce_SDO_Exception( '', $e->getCode() ,$e);
		}
	}
	
	public static function SetShipTo( $shipTo ){
		try{
			return eCommerce_SDO_Core_Application_Cart::SetShipTo( $shipTo );
		}
		catch( eCommerce_SDO_Core_Validator_Exception $e ){
			throw new eCommerce_SDO_Exception( 'Invalid field try again', $e->getCode() , $e);
		}
	}
	
	public static function SetRfc( $rfc ){
		try{
			return eCommerce_SDO_Core_Application_Cart::SetRfc( $rfc );
		}
		catch( eCommerce_SDO_Core_Validator_Exception $e ){
			throw new eCommerce_SDO_Exception( 'Error in rfc updated', $e->getCode() , $e);
		}
	}
	
	public static function SetBillTo( $billTo ){
		try{
			return eCommerce_SDO_Core_Application_Cart::SetBillTo( $billTo );
		}
		catch( eCommerce_SDO_Core_Validator_Exception $e ){
			throw new eCommerce_SDO_Exception( 'Invalid field try again', $e->getCode() , $e);
		}
	}

	public static function IsEmptyCart(){
		return eCommerce_SDO_COre_Application_Cart::IsEmptyCart();
	}
	
	public static function CreateOrderFromCart( $userId, $comments, $paymentMethod,$probableDepositDate = null){
		try{
			//debug("[1]");
			return eCommerce_SDO_Core_Application_OrderMediation::CreateOrderFromCart( $userId, $comments, $paymentMethod,$probableDepositDate );
		}
		catch( Exception $e ){
			throw new eCommerce_SDO_Exception( 'Invalid field try again', $e->getCode() , $e);
			
		}
	}
	
	public static function GetExtras(){
		return eCommerce_SDO_Core_Application_OrderMediation::GetExtras();
	}
	public static function GetExtrasTotal(){
		return eCommerce_SDO_Core_Application_OrderMediation::GetExtrasTotal();
	}
	
	//---------------------------------
	//----------SDO TO BO--------------
	//---------------------------------
	public static function DestroyActualCart(){
		return eCommerce_SDO_Core_Application_Cart::DestroyCart();
	}
	
	public static function UpdatePromotionCode( $promotionCode, $promotionCodeDiscount ){
		
		try{
		 	eCommerce_SDO_Core_Application_Cart::SavePromotionCode( $promotionCode, $promotionCodeDiscount );
		}catch(Exception $e){
			throw new eCommerce_SDO_Core_Application_Exception( 'There was an unexpected error while trying to update the discount cart', 0, $e );
		}
	}
	
	public static function GetHTML( $showLinkToDelte = false , $editProductQuantity =false, $showExtrasAndTotal = false  ){
		$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
		if ($actualLanguage == 'EN')
		{	$lang = 0;	}
		else
		{	$lang = 1;	}
		
		//TAG NAME ARRAY
		
		$code[0] = "Code";
		$code[1] = "C&oacute;digo";
		
		$product_name[0] = "Product Name";
		$product_name[1] = "Nombre del Producto";
		
		$qty[0] = "Qty";
		$qty[1] = "Cantidad";
		
		$unit_price[0] = "Unit Price";
		$unit_price[1] = "Precio Unitario";
		
		$price_disc[0] = "Price with discount";
		$price_disc[1] = "Precio con descuento";
		
		$remove[0] = "REMOVE";
		$remove[1] = "BORRAR";
		
		$update[0] = "update";
		$update[1] = "cambiar";
		
		$weight[0] = "Weight";
		$weight[1] = "Peso";
		
		$piece[0] = "Piece";
		$piece[1] = "Pieza";
		
		$gram[0] = "Gram";
		$gram[1] = "Gramo";
		
		$estimated[0] = "estimated";
		$estimated[1] = "estimado";
		
		$html ='';
		$isEmptyCart = self::IsEmptyCart();
		if( $isEmptyCart ){
			$html ='';
		}else{

			$cart = self::GetCart();
			
			//$actualDiscount = $cart->getPromotionCodeDiscount();
			//$actualDiscount = empty($actualDiscount) ? 0 : $actualDiscount;
			
			$items = $cart->getItems();
			$actualCurrency = eCommerce_SDO_CurrencyManager::GetActualCurrency();

			
			$subTotal = 0;
			$currency ='';
			
			$OUTPUT = array();
			$ArrProducts = array();
			foreach( $items as $item ){
				$ArrProduct = array();

				$product = eCommerce_SDO_Catalog::LoadProduct( $item->getProductId() );
				$ArrProduct["id"] = $item->productId;
				
				//we apply the promotion code discount
				//$TotalDiscount = $product->getDiscount() + $actualDiscount;
				//$product->setDiscount( $TotalDiscount );
				
				//show image
				$ArrProduct["image_url"] = $product->getUrlImageId('small',0);;
		

				$ArrProduct["codigo"] 		= $product->getSku();
				$ArrProduct["name"] 		= $product->getName();
				$ArrProduct["quantity"] 	= $item->quantity;
				$ArrProduct["color"] 		= $item->color;
				$ArrProduct["talla"]		 = $item->talla;
				
				
				
				$ArrProduct['peso']			= $product->getWeight();
				$ArrProduct['str_peso']			= $product->getWeight(true);
				
				
				$value = eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $product->getPrice(), $product->getCurrency() );
				$ArrProduct["str_price"] = eCommerce_SDO_CurrencyManager::NumberFormat( $value) . " " . $actualCurrency ;

				$value = eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $product->getRealPrice(), $product->getCurrency() );
				$ArrProduct["str_price_with_discount"] = eCommerce_SDO_CurrencyManager::NumberFormat($value) . " " . $actualCurrency;
				
				$value = eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $product->getRealPrice(), $product->getCurrency() );
				
				$peso = $ArrProduct['peso'];
				//$uv = $ArrProduct['unidad_venta'];
				//$uvc = $ArrProduct['uvc'];
				
				$quantity = self::getRealQuantity($item->quantity,$peso);
				$productTotal = $value * $quantity;
				
				$ArrProduct["str_total_price"] = eCommerce_SDO_CurrencyManager::NumberFormat($productTotal) . " " . $actualCurrency;
				
				$subTotal += $productTotal;
				$ArrProducts[] = $ArrProduct;
			}
			
			$OUTPUT["array_product"] = $ArrProducts;
			$OUTPUT["str_subtotal"] = eCommerce_SDO_CurrencyManager::NumberFormat($subTotal) . " " . $actualCurrency;
			
			//---------------------EXTRAS
			$OUTPUT["extras"] = array();
			if( $showExtrasAndTotal ){
				//calculate the cost of the extras
				$extras = eCommerce_SDO_Cart::GetExtras();
				foreach( $extras as $fee){
					$OUTPUT["extras"][] = array("description"=>$fee->getDescription(), "str_amount"=>eCommerce_SDO_CurrencyManager::NumberFormat( $fee->getAmount() ) . " " . $actualCurrency);
				}
				
				$extrasTotal = eCommerce_SDO_Cart::GetExtrasTotal();
				
				$OUTPUT["str_total"] = eCommerce_SDO_CurrencyManager::NumberFormat( ($subTotal+$extrasTotal) ) . " " . $actualCurrency;
			}
			
			
			//---------------------EXTRAS			
			$addCol = 0;
			$addCol = $showLinkToDelte ?  $addCol+1 : $addCol;
			$addCol = $editProductQuantity ?  $addCol+1 : $addCol;
			ob_start();
			?>
			<form name="formCart" action="purchase.php" method="get" >
				<table width="100%" border="0" align="right" cellpadding="0" cellspacing="0">
					<tr>
						<td valign="top" bgcolor="#9A9593"><img src="ima/home/trans.gif"
							width="10" height="1" /></td>
					</tr>
					<tr>
						<td valign="top">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<?if($showLinkToDelte){?><td width="67" bgcolor="#E41F2A">&nbsp;</td><?}?>
								<td width="87" bgcolor="#E41F2A">&nbsp;</td>
								<td width="315" align="center" bgcolor="#E41F2A" class="negro12a"><strong><?=$code[$lang]?></strong></td>
								<td width="150" align="center" bgcolor="#E41F2A" class="negro12a"><strong><?=$product_name[$lang]?></strong></td>
								<td width="150" align="center" bgcolor="#E41F2A" class="negro12a"><strong>Color</strong></td>
								<td width="150" align="center" bgcolor="#E41F2A" class="negro12a"><strong>Talla</strong></td>
								<?if($editProductQuantity){?><td width="127" align="center" bgcolor="#E41F2A" class="negro12a"><strong><?=$qty[$lang]?></strong></td><?}else{?>
								<td width="127" align="center" bgcolor="#E41F2A" class="negro12a"><strong><?=$qty[$lang]?></strong></td>
								<?}?>
								<td width="107" align="center" bgcolor="#E41F2A" class="negro12a"><strong><?=$unit_price[$lang]?></strong></td>
								<td width="161" align="center" bgcolor="#E41F2A" class="negro12a"><strong>Subtotal</strong></td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td valign="top" bgcolor="#9A9593"><img src="ima/home/trans.gif"
							width="10" height="1" /></td>
					</tr>
					
					
					
					<?foreach($OUTPUT["array_product"] as $product){
						$id 		= $product['id'];
						$hrefBorrar =  "cart.php?cmd=remove&productId=$id";
						$src		= $product['image_url'];
						$codigo		= $product['codigo'];
						$nombre		= $product['name'];
						$peso		= $product['str_peso'];
						$color		= $product['color'];
						$colores	= eCommerce_SDO_Color::LoadColor($color);
						$color 		= $colores->getNombre();
						
						$talla		= $product['talla'];
						$tallas		= eCommerce_SDO_Talla::LoadTalla($talla);
					
						$talla		= $tallas->getNombre();
						$quantity	= $product['quantity'];
						$pzsEstima	= $uvc == 'Peso' ? floor($quantity / $product['peso']) : 0;
						$lblEstima	= ucfirst(strtolower($uv))."s ". ($uv=='PIEZA'?str_replace('o','a',$estimated[$lang]):$estimated[$lang])."s";
						$unidad		= $uvc=='Pieza'?" $piece[$lang]".($product['quantity']>1?'s':''):($uvc=='Peso'?" $gram[$lang]".($product['quantity']>1?'s<br /><span style="font-weight:normal">'.$lblEstima.': '.$pzsEstima.'</span>':''):'');
						$unidad		= "";
						$precio		= $product['str_price'];
						$subtotal	= $product['str_total_price']?>
						<tr>
							<td valign="top">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<?if($showLinkToDelte){?><td width="67"><div align="center" class="rojo12"><strong><a href="<?=$hrefBorrar?>" class="rojo12">
									<img  src="ima/int/remove.gif" alt="<?=$remove[$lang]?>" border="0"></a></strong></div></td><?}?>
									<td width="110" align="center" style="padding:5px 5px 5px 5px;"><img src="<?=$src?>" height="74" /></td>
									<td width="107" align="center"><strong class="negro12a"><?=$codigo?></strong></td>
									<td width="237" align="center" class="negro12a" align="center"><?=$nombre?></td>
									<td width="150" align="center" class="negro12a" align="center"><?=$color?></td>
									<td width="150" align="center" class="negro12a" align="center"><?=$talla?></td>									
									<?if($editProductQuantity){?><td width="127" align="center" class="negro12a" style="padding-top: 14px;">
										<input name="quantity_<?=$id?>" id="quantity_<?=$id?>" value="<?=$quantity?>" size="4" /><strong><?=$unidad?></strong>
										<br />
										<a href="javascript:updateQuantity('<?=$id?>')" style='text-decoration:none;' class="negro12a"><b><?=$update[$lang] ?></b></a>
									</td><?}else{?>
									<td width="127" align="center" class="negro12a" style="padding-top: 14px;">
										<strong><?=$quantity?></strong>										
									</td>
									<?}?>
									<td width="107" align="center" class="negro12a"><?=$precio?></td>
									<td align="center" class="negro12a" width="161"><strong><?=$subtotal?></strong></td>
								</tr>
							</table>
							</td>
						</tr>
					<?}?>
					<tr>
						<td valign="top" bgcolor="#E10000"><img src="ima/trans.gif"
							width="10" height="1" /></td>
					</tr>
					<tr>
						<td valign="top">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="right" class="negro12a"><strong class="rosa12">Total</strong></td>
								<td width="183" align="center" class="grishome"><strong class="negro12a" ><?=$OUTPUT["str_subtotal"];?></strong></td>
							</tr>
						</table>
						</td>
					</tr>
					<?if( $showExtrasAndTotal ){
						foreach($OUTPUT["extras"] as $extra){?>
						<tr>
						<td valign="top">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="right" class="negro12a" ><strong class="negro12a"><?=$extra["description"];?></strong></td>
		                    	<td width="183" align="center" class="negro12a"><div align="center"><span class="negro12a">
		                    	<?=$extra["str_amount"];?>
		                    	</span></div></td>
							</tr>
						</table>
						</td>
						</tr>
						<?}?>
						<tr>
						<td valign="top">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="right" class="negro12a" ></td>
								<td width="183" align="center" class="negro12a"><hr></td>
							</tr>
						</table>
						</td>
						</tr>
						<tr>
						<td valign="top">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td  align="right" class="negro12a" ><p style="MARGIN-RIGHT: 20px"><span class="negro12a">Total </span></td>
		                    	<td width="183" align="center" class="negro12a"><div align="center"><span class="negro12a"><b><?=$OUTPUT["str_total"];?></b></span></div></td>
							</tr>
						</table>
						</td>
						</tr>
					<?}?>
				</table>
			</form>
			<?$html = ob_get_contents();
			ob_end_clean();
		}
		return $html;
	}


	public static function GetHTMLMiniCart( $showLinkToDelte = false , $editProductQuantity =false, $showCode =false,$showNombre =false ,$showExtrasAndTotal = false  ){
		$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
		if ($actualLanguage == 'EN')
		{	$lang = 0;	}
		else
		{	$lang = 1;	}
		
		//TAG NAME ARRAY
		
		$code[0] = "Code";
		$code[1] = "C&oacute;digo";
		
		$product_name[0] = "Product Name";
		$product_name[1] = "Nombre del Producto";
		
		$qty[0] = "Qty";
		$qty[1] = "Cantidad";
		
		$unit_price[0] = "Unit Price";
		$unit_price[1] = "Precio Unitario";
		
		$price_disc[0] = "Price with discount";
		$price_disc[1] = "Precio con descuento";
		
		$remove[0] = "REMOVE";
		$remove[1] = "BORRAR";
		
		$update[0] = "update";
		$update[1] = "cambiar";
		
		$weight[0] = "Weight";
		$weight[1] = "Peso";
		
		$piece[0] = "Piece";
		$piece[1] = "Pieza";
		
		$gram[0] = "Gram";
		$gram[1] = "Gramo";
		
		$estimated[0] = "estimated";
		$estimated[1] = "estimado";
		
		$html ='';
		$isEmptyCart = self::IsEmptyCart();
		if( $isEmptyCart ){
			$html ='';
		}else{

			$cart = self::GetCart();
			
			//$actualDiscount = $cart->getPromotionCodeDiscount();
			//$actualDiscount = empty($actualDiscount) ? 0 : $actualDiscount;
			
			$items = $cart->getItems();
			$actualCurrency = eCommerce_SDO_CurrencyManager::GetActualCurrency();

			
			$subTotal = 0;
			$currency ='';
			
			$OUTPUT = array();
			$ArrProducts = array();
			foreach( $items as $item ){
				$ArrProduct = array();

				$product = eCommerce_SDO_Catalog::LoadProduct( $item->getProductId() );
				$ArrProduct["id"] = $item->productId;
				
				//we apply the promotion code discount
				//$TotalDiscount = $product->getDiscount() + $actualDiscount;
				//$product->setDiscount( $TotalDiscount );
				
				//show image
				$ArrProduct["image_url"] = $product->getUrlImageId('small',0);;
		

				$ArrProduct["codigo"] 		= $product->getSku();
				$ArrProduct["name"] 		= $product->getName();
				$ArrProduct["quantity"] 	= $item->quantity;
				$ArrProduct["color"] 		= $item->color;
				$ArrProduct["talla"]		 = $item->talla;
				
				$ArrProduct['peso']			= $product->getWeight();
				$ArrProduct['str_peso']			= $product->getWeight(true);
				//$ArrProduct['uvc']			= $item->unidad_venta_cliente; 
				//$ArrProduct['unidad_venta']	= $product->getUnidadVenta();

				$value = eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $product->getPrice(), $product->getCurrency() );
				$ArrProduct["str_price"] = eCommerce_SDO_CurrencyManager::NumberFormat( $value) . " " . $actualCurrency ;

				$value = eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $product->getRealPrice(), $product->getCurrency() );
				$ArrProduct["str_price_with_discount"] = eCommerce_SDO_CurrencyManager::NumberFormat($value) . " " . $actualCurrency;
				
				$value = eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $product->getRealPrice(), $product->getCurrency() );
				
				$peso = $ArrProduct['peso'];
				$uv = $ArrProduct['unidad_venta'];
				$uvc = $ArrProduct['uvc'];
				
				$quantity = self::getRealQuantity($item->quantity,$peso,$uv,$uvc);
				$productTotal = $value * $quantity;
				
				$ArrProduct["str_total_price"] = eCommerce_SDO_CurrencyManager::NumberFormat($productTotal) . " " . $actualCurrency;
				
				$subTotal += $productTotal;
				$ArrProducts[] = $ArrProduct;
			}

			$OUTPUT["array_product"] = $ArrProducts;
			$OUTPUT["str_subtotal"] = eCommerce_SDO_CurrencyManager::NumberFormat($subTotal) . " " . $actualCurrency;
			
			//---------------------EXTRAS
			$OUTPUT["extras"] = array();
			if( $showExtrasAndTotal ){
				//calculate the cost of the extras
				$extras = eCommerce_SDO_Cart::GetExtras();
				foreach( $extras as $fee){
					$OUTPUT["extras"][] = array("description"=>$fee->getDescription(), "str_amount"=>eCommerce_SDO_CurrencyManager::NumberFormat( $fee->getAmount() ) . " " . $actualCurrency);
				}
				
				$extrasTotal = eCommerce_SDO_Cart::GetExtrasTotal();
				
				$OUTPUT["str_total"] = eCommerce_SDO_CurrencyManager::NumberFormat( ($subTotal+$extrasTotal) ) . " " . $actualCurrency;
			}
			
			
			//---------------------EXTRAS			
			$addCol = 0;
			$addCol = $showLinkToDelte ?  $addCol+1 : $addCol;
			$addCol = $editProductQuantity ?  $addCol+1 : $addCol;
			ob_start();
			?>
			<form name="formCart" action="purchase.php" method="get" >
				<table width="100%" border="0" align="right" cellpadding="0" cellspacing="0">
					<tr>
						<td valign="top" bgcolor="#9A9593"><img src="ima/home/trans.gif" width="10" height="1" /></td>
					</tr>
					<tr>
						<td valign="top">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<?if($showLinkToDelte){?><td width="67" bgcolor="#E41F2A">&nbsp;</td><?}?>
								<td width="166" bgcolor="#E41F2A">&nbsp;</td>
								<?if($showCode){?><td width="107" align="center" bgcolor="#E41F2A" class="negro12a"><strong><?=$code[$lang]?></strong></td> <?}?>
								<?if($showNombre){?><td width="237" align="center" bgcolor="#E41F2A" class="negro12a"><strong><?=$product_name[$lang]?></strong></td><?}?>
								<td width="77" align="center" bgcolor="#E41F2A" class="blanco12a"><strong>Color</strong></td> 
								<td width="77" align="center" bgcolor="#E41F2A" class="blanco12a"><strong>Talla</strong></td> 
								<?if($editProductQuantity){?><td width="127" align="center" bgcolor="#E41F2A" class="negro12a"><strong><?=$qty[$lang]?></strong></td><?}else{?>
								<td width="127" align="center" bgcolor="#E41F2A" class="negro12a"><strong><?=$qty[$lang]?></strong></td>
								<?}?>
								<td width="107" align="center" bgcolor="#E41F2A" class="negro12a"><strong><?=$unit_price[$lang]?></strong></td>
								<td width="161" align="center" bgcolor="#E41F2A" class="negro12a"><strong>Subtotal</strong></td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td valign="top" bgcolor="#9A9593"><img src="ima/trans.gif"	width="10" height="1" /></td>
					</tr>
					
					
					
					<?foreach($OUTPUT["array_product"] as $product){
						$id 		= $product['id'];
						$hrefBorrar =  "cart.php?cmd=remove&productId=$id";
						$src		= $product['image_url'];
						$codigo		= $product['codigo'];
						$nombre		= $product['name'];
						$peso		= $product['str_peso'];
						$uvc		= $product['uvc'];
						$uv			= $product['unidad_venta'];
						$quantity	= $product['quantity'];
						
						$color		= $product['color'];
						$colores	= eCommerce_SDO_Color::LoadColor($color);
						$color 		= $colores->getNombre();
						
						$talla		= $product['talla'];
						$tallas		= eCommerce_SDO_Talla::LoadTalla($talla);							
						$talla		= $tallas->getNombre();
						
						
						$pzsEstima	= $uvc == 'Peso' ? floor($quantity / $product['peso']) : 0;
						$lblEstima	= ucfirst(strtolower($uv))."s ". ($uv=='PIEZA'?str_replace('o','a',$estimated[$lang]):$estimated[$lang])."s";
						$unidad		= $uvc=='Pieza'?" $piece[$lang]".($product['quantity']>1?'s':''):($uvc=='Peso'?" $gram[$lang]".($product['quantity']>1?'s<br /><span style="font-weight:normal">'.$lblEstima.': '.$pzsEstima.'</span>':''):'');
						$unidad		= "";
						$precio		= $product['str_price'];
						$subtotal	= $product['str_total_price']?>
						<tr>
							<td valign="top">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<?if($showLinkToDelte){?><td width="67"><div align="center" class="rojo12"><strong><a href="<?=$hrefBorrar?>" class="rojo12">
									<img  src="ima/int/remove.gif" alt="<?=$remove[$lang]?>" border="0"></a></strong></div></td><?}?>
									<td width="110" align="center" style="padding:5px 5px 5px 5px;"><img src="<?=$src?>" height="74" /></td>
									<?if($showCode){?><td width="107" align="center"><strong class="blanco12a"><?=$codigo?></strong></td><?}?>
									<?if($showNombre){?><td width="237" align="center" class="blanco12a" align="center"><?=$nombre?></td><?}?>
									<td width="77" align="center" class="blanco12a" ><?=$color?></td> 
									<td width="77" align="center" class="blanco12a" ><?=$talla?></td> 
									<?if($editProductQuantity){?><td width="127" align="center" class="blanco12a" style="padding-top: 14px;">
										<input name="quantity_<?=$id?>" id="quantity_<?=$id?>" value="<?=$quantity?>" size="4" /><strong><?=$unidad?></strong>
										<br />
										<a href="javascript:updateQuantity('<?=$id?>')" style='text-decoration:none;' class="rosa12"><b><?=$update[$lang] ?></b></a>
									</td><?}else{?>
									<td width="127" align="center" class="blanco12a" style="padding-top: 14px;">
										<strong><?=$quantity?></strong>										
									</td>
									<?}?>
									<td width="107" align="center" class="blanco12a"><?=$precio?></td>
									<td align="center" class="blanco12a" width="161"><strong><?=$subtotal?></strong></td>
								</tr>
							</table>
							</td>
						</tr>
					<?}?>
					<tr>
						<td valign="top" bgcolor="#E10000"><img src="ima/trans.gif"	width="10" height="1" /></td>
					</tr>
					<tr>
						<td valign="top">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="right" class="blanco12a"><strong class="rosa12">Total</strong></td>
								<td width="117" align="center" class="grishome"><strong class="blanco12a" ><?=$OUTPUT["str_subtotal"];?></strong></td>
							</tr>
						</table>
						</td>
					</tr>
					<?if( $showExtrasAndTotal ){
						foreach($OUTPUT["extras"] as $extra){?>
						<tr>
						<td valign="top">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="right" class="blanco12a" ><strong class="blanco12a"><?=$extra["description"];?></strong></td>
		                    	<td width="183" align="center" class="blanco12a"><div align="center"><span class="blanco12a">
		                    	<?=$extra["str_amount"];?>
		                    	</span></div></td>
							</tr>
						</table>
						</td>
						</tr>
						<?}?>
						<tr>
						<td valign="top">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="right" class="blanco12a" ></td>
								<td width="183" align="center" class="blanco12a"><hr></td>
							</tr>
						</table>
						</td>
						</tr>
						<tr>
						<td valign="top">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td  align="right" class="blanco12a" ><p style="MARGIN-RIGHT: 20px"><span class="blanco12a">Total </span></td>
		                    	<td width="183" align="center" class="blanco12a"><div align="center"><span class="blanco12a"><b><?=$OUTPUT["str_total"];?></b></span></div></td>
							</tr>
						</table>
						</td>
						</tr>
					<?}?>
				</table>
			</form>
			<?$html = ob_get_contents();
			ob_end_clean();
		}
		return $html;
	}
	
	
public static function setShippingType( $shippingType ){
		return eCommerce_SDO_Core_Application_Cart::setShippingType($shippingType);
	}
	
public static function GetTotalCart($extra = false, $descuentos = false){
		$actualCurrency 	= eCommerce_SDO_CurrencyManager::GetActualCurrency();
		$cart 				= self::GetCart();				
		$items 				= $cart->getItems();		
		$total_quantity 	= 0; 
		$cartTotalAmount_cupon = 0;
		$cartTotalAmount_certificado = 0;
		foreach($items as $item){
			$productId 		= $item->getProductId();
			
			$product 	= eCommerce_SDO_Catalog::LoadProduct( $productId );
			
			$total_quantity += $item->quantity;
			
			$value 			= eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $product->getRealPrice(), $product->getCurrency() );
			$total_price 	+= $value * $item->quantity;
			
			$total_weight	+= $product->getWeight() * $item->quantity;
			
		}
					
		if($extra){			
			$totalEnvio = eCommerce_SDO_Core_Application_OrderBusinessRules::GetPriceFromShippingToMX($total_weight);			
			
			$total_price = $total_price + $totalEnvio;
		}
				
		return array(	'quantity' => $total_quantity, 
						'price' => array('format' => eCommerce_SDO_CurrencyManager::NumberFormat($total_price) . " " . $actualCurrency,
										 'amount' => $total_price),
						'weight' => $total_weight,
						/*'discount' => array('cupon' => array('format' => eCommerce_SDO_CurrencyManager::NumberFormat($descuentoCupon) . " " . $actualCurrency,
																'amount' => $descuentoCupon))*/
							);
						/*'discount' => array('cupon' => array('format' => eCommerce_SDO_CurrencyManager::NumberFormat($descuentoCupon) . " " . $actualCurrency,
										 					 'amount' => $descuentoCupon),
											'certificado' => array('format' => eCommerce_SDO_CurrencyManager::NumberFormat($descuentoCertificado) . " " . $actualCurrency,
										 					 	   'amount' => $descuentoCertificado)));*/
						
	}
	
	
	public static function DeleteOrder( $orderId ){
		try{
		 	$order = eCommerce_SDO_Core_Application_Order::Delete( $orderId );
		}catch(Exception $e){
			$order = new eCommerce_Entity_Order();
		}
		return $order;
	} 
	
	public function cartParseHTACCESLink($args,$type = ''){
		return eCommerce_SDO_Core_Application_Cart::parseHTACCESLink($args,$type);
	}
	
	public static function getRealQuantity($quantity,$peso,$uv,$uvc){
		switch($uv){
			case 'PIEZA':$quantity = $uvc == 'Peso' ? floor($quantity / $peso) : $quantity;break;
			default:$quantity = $uvc == 'Pieza' ? $quantity : $quantity;break;
		}
		return $quantity;
	}
}
?>