<?php
class eCommerce_SDO_Cart{
	const ADDRESS_SHIP_TO = eCommerce_SDO_Core_Application_Cart::ADDRESS_SHIP_TO;
	const ADDRESS_BILL_TO = eCommerce_SDO_Core_Application_Cart::ADDRESS_BILL_TO;
	/**
	 * Obtain the actual cart
	 * @return eCommerce_Entity_Cart
	 */
	public static function GetCart(){
		return eCommerce_SDO_Core_Application_Cart::GetCart();
	}

	public static function addItem($productId,$quantity,$versionId,$color,$talla,$attributes='',$galeria_id,$file_id,$precio_total){
		$result = false;
		try{
			eCommerce_SDO_Core_Application_Cart::AddItem($productId,$quantity,$versionId,$color,$talla,$attributes,$galeria_id,$file_id,$precio_total);
			$result = true;
		}
		catch( eCommerce_SDO_Core_Application_Exception $e ){
			if( $e->getCode() == 100 ){
				//if the product exists we need add the adicional quantity
			}
		}
		return $result;
	}

	public static function RemoveItem( $productId ){
		$result = false;
		try{
			eCommerce_SDO_Core_Application_Cart::RemoveItem( $productId );
			$result = true;
		}
		catch( eCommerce_SDO_Core_Application_Exception $e ){}
		return $result;
	}

	public static function UpdateQuantity( $productId, $newQuantity ,$attributes,$galeria_id,$file_id){
		try{
		 eCommerce_SDO_Core_Application_Cart::UpdateQuantity( $productId, $newQuantity,$attributes,$galeria_id,$file_id );
		}
		catch(eCommerce_SDO_Core_Application_Exception $e){
			throw new eCommerce_SDO_Exception( '', $e->getCode() ,$e);
		}
	}

	public static function SetShipTo( $shipTo ){
		try{			
			return eCommerce_SDO_Core_Application_Cart::SetShipTo( $shipTo );
		}
		catch( eCommerce_SDO_Core_Validator_Exception $e ){
			throw new eCommerce_SDO_Exception( 'Invalid field try again', $e->getCode() , $e);
		}
	}

	public static function SetRfc( $rfc ){
		try{
			return eCommerce_SDO_Core_Application_Cart::SetRfc( $rfc );
		}
		catch( eCommerce_SDO_Core_Validator_Exception $e ){
			throw new eCommerce_SDO_Exception( 'Error in rfc updated', $e->getCode() , $e);
		}
	}

	public static function SetBillTo( $billTo ){
		try{
			return eCommerce_SDO_Core_Application_Cart::SetBillTo( $billTo );
		}
		catch( eCommerce_SDO_Core_Validator_Exception $e ){
			throw new eCommerce_SDO_Exception( 'Invalid field try again', $e->getCode() , $e);
		}
	}

	public static function IsEmptyCart(){
		return eCommerce_SDO_COre_Application_Cart::IsEmptyCart();
	}

	public static function CreateOrderFromCart( $userId, $comments, $paymentMethod,$probableDepositDate = null){
		try{			
			return eCommerce_SDO_Core_Application_OrderMediation::CreateOrderFromCart( $userId, $comments, $paymentMethod,$probableDepositDate );
		}
		catch( Exception $e ){
			throw new eCommerce_SDO_Exception( 'Invalid field try again', $e->getCode() , $e);
		}
	}

	public static function GetExtras($total, $precioEnvio = NULL ){ 
		return eCommerce_SDO_Core_Application_OrderMediation::GetExtras($total, $precioEnvio);
	}

	public static function GetExtrasTotal($total, $precioEnvio = NULL){
		return eCommerce_SDO_Core_Application_OrderMediation::GetExtrasTotal($total, $precioEnvio);
	}

	//---------------------------------
	//----------SDO TO BO--------------
	//---------------------------------
	public static function DestroyActualCart(){
		return eCommerce_SDO_Core_Application_Cart::DestroyCart();
	}

	public static function UpdatePromotionCode( $promotionCode, $promotionCodeDiscount ){
		try{
		 	eCommerce_SDO_Core_Application_Cart::SavePromotionCode( $promotionCode, $promotionCodeDiscount );
		}catch(Exception $e){
			throw new eCommerce_SDO_Core_Application_Exception( 'There was an unexpected error while trying to update the discount cart', 0, $e );
		}
	}

	public static function GetHTML( $showLinkToDelte = false , $editProductQuantity =false, $showExtrasAndTotal = false, $precioEnvio = NULL){
		$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
		if ($actualLanguage == 'EN')
		{	$lang = 0;	}
		else
		{	$lang = 1;	}
		//TAG NAME ARRAY
		$code[0] = "Code";
		$code[1] = "C&oacute;digo";
		$product_name[0] = "Product";
		$product_name[1] = "Producto";
		$qty[0] = "Qty";
		$qty[1] = "Cant";
		$unit_price[0] = "Unit Price";
		$unit_price[1] = "Precio Unitario";
		$price_disc[0] = "Price with discount";
		$price_disc[1] = "Precio con descuento";
		$remove[0] = "REMOVE";
		$remove[1] = "BORRAR";
		$update[0] = "update";
		$update[1] = "cambiar";
		$weight[0] = "Weight";
		$weight[1] = "Peso";
		$piece[0] = "Piece";
		$piece[1] = "Pieza";
		$gram[0] = "Gram";
		$gram[1] = "Gramo";
		$estimated[0] = "estimated";
		$estimated[1] = "estimado";
		$html ='';
		$isEmptyCart = self::IsEmptyCart();
		if( $isEmptyCart ){
			$html ='';
		}else{
			$cart = self::GetCart();
			//$actualDiscount = $cart->getPromotionCodeDiscount();
			//$actualDiscount = empty($actualDiscount) ? 0 : $actualDiscount;
			$items = $cart->getItems();			
			$actualCurrency = eCommerce_SDO_CurrencyManager::GetActualCurrency();
			$subTotal = 0;
			$currency ='';
			$OUTPUT = array();
			$ArrProducts = array();
			foreach( $items as $item ){
				$ArrProduct = array();
				$product    = eCommerce_SDO_Catalog::LoadProduct( $item->getProductId() );
				$version    = eCommerce_SDO_Catalog::LoadVersion( $item->getVersionId());
				$attributes = $item->getAttributes();

				$galeriaId = $item->getGaleriaId();
				if(!empty($galeriaId)){
					$imagen = eCommerce_SDO_Galeria::LoadGaleria($galeriaId);
					$src_imagen = $imagen->getUrlArrayImages("small",0);
				}

				$file_id = $item->getFileId();
				if(!empty($file_id)){
					$imagen = eCommerce_SDO_Files::LoadFiles($file_id);
					$src_imagen = ABS_HTTP_URL."bo/backoffice/server/php/files/".$imagen->getName();
				}


				
				//$talla            = eCommerce_SDO_Catalog::GetVersionTalla( $item->versionId);
				$ArrProduct["id"] = $item->getProductId();
				//we apply the promotion code discount
				//$TotalDiscount = $product->getDiscount() + $actualDiscount;
				//$product->setDiscount( $TotalDiscount );
				//show image
				//$ArrProduct["image_url"] = $product->getUrlImageId('small',0);
				$ArrProduct["image_url"]               = $src_imagen;
				$ArrProduct["codigo"]                  = $product->getSku();
				$ArrProduct["name"]                    = $version->getName();
				$ArrProduct["quantity"]                = $item->getQuantity();
				//$ArrProduct["color"]                   = $item->color;
				//$ArrProduct["talla"]                   = $talla[0]['talla_id'];
				$ArrProduct["caracteristicas"]              = $item->getAttributes();
				$ArrProduct["version_id"]              = $item->getVersionId();
				$ArrProduct["version_name"]            = $version->getName();
				$ArrProduct["version_sku"]             = $version->getSku();
				$ArrProduct["version_price"]           = $item->getPrice();
				$ArrProduct['peso']                    = $product->getWeight();
				$ArrProduct['str_peso']                = $product->getWeight(true);
				$value                                 = eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $item->getPrice(), $version->getCurrency() );
				$ArrProduct["str_price"]               = eCommerce_SDO_CurrencyManager::NumberFormat( $value) . " " . $actualCurrency ;
				$value                                 = eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $item->getPrice(), $version->getCurrency() );
				$ArrProduct["str_price_with_discount"] = eCommerce_SDO_CurrencyManager::NumberFormat($value) . " " . $actualCurrency;
				$value                                 = eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $item->getPrice(), $version->getCurrency() );
				/*$value = eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $product->getPrice(), $product->getCurrency() );
				$ArrProduct["str_price"] = eCommerce_SDO_CurrencyManager::NumberFormat( $value) . " " . $actualCurrency ;
				$value = eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $product->getRealPrice(), $product->getCurrency() );
				$ArrProduct["str_price_with_discount"] = eCommerce_SDO_CurrencyManager::NumberFormat($value) . " " . $actualCurrency;				
				$value = eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $product->getRealPrice(), $product->getCurrency() );
				*/
				$peso = $ArrProduct['peso'];
				//$uv = $ArrProduct['unidad_venta'];
				//$uvc = $ArrProduct['uvc'];
				//$quantity = self::getRealQuantity($item->quantity,$peso);
				$quantity = $item->getQuantity();
				$productTotal = $value * $quantity;
				$ArrProduct["str_total_price"] = eCommerce_SDO_CurrencyManager::NumberFormat($productTotal) . " " . $actualCurrency;
				$subTotal += $productTotal;
				$ArrProducts[] = $ArrProduct;
			}
			$OUTPUT["array_product"] = $ArrProducts;
			$OUTPUT["str_subtotal"] = eCommerce_SDO_CurrencyManager::NumberFormat($subTotal) . " " . $actualCurrency;
			//---------------------EXTRAS
			$OUTPUT["extras"] = array();
			if( $showExtrasAndTotal ){				
				$TotalFT = self::GetTotalCart(true,false);				
				$total = $TotalFT['price']['amount'];				
				$extras = eCommerce_SDO_Cart::GetExtras($total, $precioEnvio);								
				//calculate the cost of the extras
				foreach( $extras as $fee){
					$txt = $fee->getDescription();					
					/*if($txt == 'Env�o y manejo ' || $txt == 'Env&iacute;o y manejo ' && $precioEnvio == NULL){
						$strAmount = '<select id="costoEnvio" name="costoEnvio" class="form-control"> ';
						$strAmount.='<option value="">Seleccione tipo de env&iacute;o</option>';
						$strAmount.='<option value="0">Recoger en sucursal Zavaleta (Pue. Pue.) $0.00 MXN</option>';
						$strAmount.='<option value="100">Env&iacute;o en Zona Metropolitana de Puebla  $100.00 MXN</option>';
						$strAmount.='<option value="145">Servicio de paqueter&iacute;a Nacional $145.00 MXN</option>';
						$strAmount.='</select><div id="envioError"></div>';
						$strAmount.='<div id="envioCosto"></div>';
						$OUTPUT["extras"][] = array("description"=>$fee->getDescription(), "str_amount"=>$strAmount);
					}else{*/
						$OUTPUT["extras"][] = array("description"=>$fee->getDescription(), "str_amount"=>eCommerce_SDO_CurrencyManager::NumberFormat( $fee->getAmount() ) . " " . $actualCurrency);						
					//}
				}				
				$extrasTotal = eCommerce_SDO_Cart::GetExtrasTotal($total, $precioEnvio);				
				$OUTPUT["str_total"] = eCommerce_SDO_CurrencyManager::NumberFormat( ($subTotal+$extrasTotal) ) . " " . $actualCurrency;
			}
			//---------------------EXTRAS			
			$addCol = 0;
			$addCol = $showLinkToDelte ?  $addCol+1 : $addCol;
			$addCol = $editProductQuantity ?  $addCol+1 : $addCol;
			ob_start();
			?>
			<form name="formCart" action="purchase.php" method="get" id="formCart" >
				<table id="table-titles" width="100%" border="0" align="right" cellpadding="0" cellspacing="0">
					<!--<tr>
						<td valign="top" bgcolor="#000"><img src="<?=ABS_HTTP_URL?>images/trans.gif" width="10" height="1" /></td>
					</tr>-->
					<tr>
						<td valign="top">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr style="color: #FFF; height: 35px;">
								<?if($showLinkToDelte){?><td width="67" bgcolor="#000"><img src="images/trans.gif" style="width:25px"></td><?}?>
								<td width="150" bgcolor="#000"><img src="images/trans.gif" style="width:60px"></td>
								<td width="117" align="center" bgcolor="#000" class="blanco12 tdCode"><strong><?=$code[$lang]?></strong></td>
								<td width="150" align="center" bgcolor="#000" class="blanco12 tdName"><strong><?=$product_name[$lang]?></strong></td>
								<!--<td width="150" align="center" bgcolor="#000" class="blanco12 tdAttr"><strong>Atributo</strong></td>-->								
							    <td width="250" align="center" bgcolor="#000" class="blanco12 tdCar"><strong>Caracter&iacute;stica</strong></td>
								<?if($editProductQuantity){?><td width="127" height="25" align="center" valign="middle" bgcolor="#000" class="blanco12"><strong><?=$qty[$lang]?></strong></td><?}else{?>
				       			 <td width="70" align="center" bgcolor="#000" class="blanco12 tdQty"><strong><?=$qty[$lang]?></strong></td>
								<?}?>
								<td width="107" align="center" bgcolor="#000" class="blanco12 tdPrice"><strong><?=$unit_price[$lang]?></strong></td>
								<td width="161" align="right" bgcolor="#000" class="blanco12 tdSub"><strong>Subtotal</strong></td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td valign="top" height="1" bgcolor="#00BEFF">&nbsp;
							<!-- <img src="<?=ABS_HTTP_URL?>images/trans.gif" width="10" height="1" style="display:block;" /> -->
						</td>
					</tr>
					<? $totalTmp = 0;
					foreach($OUTPUT["array_product"] as $product){
						$id 		= $product['id'];
						$hrefBorrar =  "cart.php?cmd=remove&productId=$id";
						$src		= $product['image_url'];
						$codigo		= $product['codigo'];
						$nombre		= $product['name'];
						$version_codigo		= $product['version_sku'];
						$version_nombre		= $product['version_name'];
						$caracteristicas		= str_replace(",","<br/>",$product['caracteristicas']);
						$peso		= $product['str_peso'];
						/*$color		= $product['color'];
						$colores	= eCommerce_SDO_Color::LoadColor($color);
						$color 		= $colores->getNombre();
						$talla		= $product['talla'];
						$tallas		= eCommerce_SDO_Talla::LoadTalla($talla);
						$talla		= $tallas->getNombre();*/
						$quantity	= $product['quantity'];
						$pzsEstima	= $uvc == 'Peso' ? floor($quantity / $product['peso']) : 0;
						$lblEstima	= ucfirst(strtolower($uv))."s ". ($uv=='PIEZA'?str_replace('o','a',$estimated[$lang]):$estimated[$lang])."s";
						$unidad		= $uvc=='Pieza'?" $piece[$lang]".($product['quantity']>1?'s':''):($uvc=='Peso'?" $gram[$lang]".($product['quantity']>1?'s<br /><span style="font-weight:normal">'.$lblEstima.': '.$pzsEstima.'</span>':''):'');
						$unidad		= "";
						$precio		= $product['str_price'];
						$totalTmp+=$product['quantity']*$product['version_price'];
						$subtotal	= $product['str_total_price']?>
						<tr style="border-bottom: 1px solid #000;">
							<td valign="top" style="background-color:#FFFFFF;">
							<table class="products-wrapper" width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size: 13px;">
								<tr>
									<?if($showLinkToDelte){?><td width="67"><div align="center" class="rojo12"><strong><a href="<?=$hrefBorrar?>" class="rojo12">
									<img src="img/close.png" width="25" alt="<?=$remove[$lang]?>" border="0"></a></strong></div></td><?}?>
									<td width="150" align="center" ><img src="<?=$src?>" style="width:50%" /></td>
									<td width="120" align="center"><strong class="negro12a tdCode"><?=$version_codigo?></strong></td>
									<td width="150" align="center" class="negro12a tdName" align="center"><?=$version_nombre?></td>
									<!--<td width="150" align="center" class="negro12a tdAttr" align="center"><?=utf8_encode($color)?></td>-->
									<td width="250" align="center" class="negro12a tdCar" align="center"><?=$caracteristicas?></td>									
									<?if($editProductQuantity){?><td width="127" align="center" class="negro12a tdQty" >
										<input type="number" name="quantity_<?=$id?>" id="quantity_<?=$id?>" value="<?=$quantity?>" min="1" size="4" style="width:50px; text-align:center;" /><strong><?=$unidad?></strong>
										<br />
										<a href="javascript:updateQuantity('<?=$id?>')" style='text-decoration:none;' class="negro12a"><b><?=$update[$lang] ?></b></a>
									</td><?}else{?>
									<td width="70" align="center" class="negro12a tdQty" >
										<strong><?=$quantity?></strong>										
									</td>
									<?}?>
									<td width="107" align="center" class="negro12a tdPrice"><?=$precio?></td>
									<td align="right" class="negro12a tdSub" width="161"><strong><?=$subtotal?></strong></td>
								</tr>
							</table>
							</td>
						</tr>
					<?}?>
					<!--<tr>
						<td valign="top" height="1" bgcolor="#00BEFF">
							<img src="<?=ABS_HTTP_URL?>images/trans.gif" width="10" height="1" style="display:block;" />
						</td>
					</tr>-->
					<tr>
						<td valign="top" style="background-color:#FFFFFF;">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="shop-cart-table-total" style="font-size: 13px;">
							<tr>
								<td align="right" class="negro12a"><strong class="rosa12">Subtotal</strong></td>
								<td width="183" align="right" class="grishome"><strong class="negro12a" ><?=$OUTPUT["str_subtotal"];?></strong></td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
					<td valign="top">&nbsp;
					</td>
					</tr>
					<?if( $showExtrasAndTotal ){
						foreach($OUTPUT["extras"] as $extra){ ?>
						<tr>
						<td valign="top">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="shop-cart-table-total" style="font-size: 13px;">
							<tr>
								<td align="right" class="negro12a" ><strong class="negro12a"><?=$extra["description"];?></strong></td>
		                    	<td width="200" align="right" class="negro12a"><?=$extra["str_amount"];?></td>
							</tr>
						</table>
						</td>
						</tr>
						<?}?>
						<tr>
						<td valign="top">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="shop-cart-table-total" style="font-size: 13px;">
							<tr>
								<td align="right" class="negro12a" >&nbsp;</td>
								<td width="183" align="right" class="negro12a"><hr></td>
							</tr>
						</table>
						</td>
						</tr>
						<tr>
						<td valign="top">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="shop-cart-table-total" style="font-size: 13px;">
							<tr>
								<td  align="right" class="negro12a" ><p><span class="negro12a" style="font-weight: bold;">Total </span></td>
		                    	<td width="183" align="right" class="negro12a"><strong id="strTotal"><?=$OUTPUT["str_total"];?></strong></td>
							</tr>
							</table>
						</td>
						</tr>
					<?}?>
				</table>
			</form>
			<? $html = ob_get_contents();
			ob_end_clean();
		}
		return $html;
	}

	public static function GetHTMLMiniCart( $showLinkToDelte = false , $editProductQuantity =false, $showCode =false,$showNombre =false ,$showExtrasAndTotal = false  ){
		$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
		if ($actualLanguage == 'EN')
		{	$lang = 0;	}
		else
		{	$lang = 1;	}
		//TAG NAME ARRAY
		$code[0] = "Code";
		$code[1] = "C&oacute;digo";
		$product_name[0] = "Product Name";
		$product_name[1] = "Nombre del Producto";
		$qty[0] = "Qty";
		$qty[1] = "Cantidad";
		$unit_price[0] = "Unit Price";
		$unit_price[1] = "Precio Unitario";
		$price_disc[0] = "Price with discount";
		$price_disc[1] = "Precio con descuento";
		$remove[0] = "REMOVE";
		$remove[1] = "BORRAR";
		$update[0] = "update";
		$update[1] = "cambiar";
		$weight[0] = "Weight";
		$weight[1] = "Peso";
		$piece[0] = "Piece";
		$piece[1] = "Pieza";
		$gram[0] = "Gram";
		$gram[1] = "Gramo";
		$estimated[0] = "estimated";
		$estimated[1] = "estimado";
		$html ='';
		$isEmptyCart = self::IsEmptyCart();
		if( $isEmptyCart ){
			$html ='';
		}else{
			$cart = self::GetCart();
			//$actualDiscount = $cart->getPromotionCodeDiscount();
			//$actualDiscount = empty($actualDiscount) ? 0 : $actualDiscount;
			$items = $cart->getItems();
			$actualCurrency = eCommerce_SDO_CurrencyManager::GetActualCurrency();
			$subTotal = 0;
			$currency ='';
			$OUTPUT = array();
			$ArrProducts = array();
			foreach( $items as $item ){
				$ArrProduct       = array();
				$product          = eCommerce_SDO_Catalog::LoadProduct( $item->getProductId() );
				$version          = eCommerce_SDO_Catalog::LoadVersion( $item->versionId);
				$talla            = eCommerce_SDO_Catalog::GetVersionTalla( $item->versionId);
				$ArrProduct["id"] = $item->productId;
				//we apply the promotion code discount
				//$TotalDiscount = $product->getDiscount() + $actualDiscount;
				//$product->setDiscount( $TotalDiscount );
				//show image
				//$ArrProduct["image_url"] = $product->getUrlImageId('small',0);;

				$galeriaId = $item->getGaleriaId();
				if(!empty($galeriaId)){
					$imagen = eCommerce_SDO_Galeria::LoadGaleria($galeriaId);
					$src_imagen = $imagen->getUrlArrayImages("small",0);
				}

				$file_id = $item->getFileId();
				if(!empty($file_id)){
					$imagen = eCommerce_SDO_Files::LoadFiles($file_id);
					$src_imagen = ABS_HTTP_URL."bo/backoffice/server/php/files/".$imagen->getName();
				}

				$ArrProduct["image_url"]     = $src_imagen;
				$ArrProduct["codigo"]        = $product->getSku();
				$ArrProduct["name"]          = $product->getName();
				$ArrProduct["quantity"]      = $item->quantity;
				$ArrProduct["color"]         = $item->color;
				$ArrProduct["talla"]         = $talla[0]['talla_id'];
				$ArrProduct["version_id"]    = $item->versionId;
				$ArrProduct["version_name"]  = $version->getName();
				$ArrProduct["version_sku"]   = $version->getSku();
				$ArrProduct["version_price"] = $version->getPrice();								
				$ArrProduct['peso']          = $product->getWeight();
				$ArrProduct['str_peso']      = $product->getWeight(true);
				//$ArrProduct['uvc']			= $item->unidad_venta_cliente; 
				//$ArrProduct['unidad_venta']	= $product->getUnidadVenta();
				/*$value = eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $product->getPrice(), $product->getCurrency() );
				$ArrProduct["str_price"] = eCommerce_SDO_CurrencyManager::NumberFormat( $value) . " " . $actualCurrency ;
				$value = eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $product->getRealPrice(), $product->getCurrency() );
				$ArrProduct["str_price_with_discount"] = eCommerce_SDO_CurrencyManager::NumberFormat($value) . " " . $actualCurrency;				
				$value = eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $product->getRealPrice(), $product->getCurrency() );*/
				$value                                 = eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $item->getPrice(), $version->getCurrency() );				
				$ArrProduct["str_price"]               = eCommerce_SDO_CurrencyManager::NumberFormat( $value) . " " . $actualCurrency ;				
				
				/*$value                                 = eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $version->getRealPrice(), $version->getCurrency() );				
				$ArrProduct["str_price_with_discount"] = eCommerce_SDO_CurrencyManager::NumberFormat($value) . " " . $actualCurrency;
				$value                                 = eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $version->getRealPrice(), $version->getCurrency() );*/
				
				/*$peso                                  = $ArrProduct['peso'];
				$uv                                    = $ArrProduct['unidad_venta'];
				$uvc                                   = $ArrProduct['uvc'];*/
				$quantity                              = self::getRealQuantity($item->quantity,$peso,$uv,$uvc);
				$productTotal                          = $value * $quantity;
				$ArrProduct["str_total_price"]         = eCommerce_SDO_CurrencyManager::NumberFormat($productTotal) . " " . $actualCurrency;
				$subTotal                              += $productTotal;
				$ArrProducts[]                         = $ArrProduct;
			}
			$OUTPUT["array_product"] = $ArrProducts;
			$OUTPUT["str_subtotal"] = eCommerce_SDO_CurrencyManager::NumberFormat($subTotal) . " " . $actualCurrency;
			//---------------------EXTRAS
			$OUTPUT["extras"] = array();
			if( $showExtrasAndTotal ){
				//calculate the cost of the extras
				$extras = eCommerce_SDO_Cart::GetExtras();
				foreach( $extras as $fee){
					$OUTPUT["extras"][] = array("description"=>$fee->getDescription(), "str_amount"=>eCommerce_SDO_CurrencyManager::NumberFormat( $fee->getAmount() ) . " " . $actualCurrency);
				}
				$extrasTotal = eCommerce_SDO_Cart::GetExtrasTotal();
				$OUTPUT["str_total"] = eCommerce_SDO_CurrencyManager::NumberFormat( ($subTotal+$extrasTotal) ) . " " . $actualCurrency;
			}
			//---------------------EXTRAS			
			$addCol = 0;
			$addCol = $showLinkToDelte ?  $addCol+1 : $addCol;
			$addCol = $editProductQuantity ?  $addCol+1 : $addCol;
			ob_start();
			?>
			<form name="formCart" action="purchase.php" method="get" >
				<table width="100%" border="0" align="right" cellpadding="0" cellspacing="0">
					<!--<tr>
						<td valign="top" bgcolor="#F00803" align="right"><a onclick="hideCarrito();return false;" style="cursor:pointer"><img src="<?//=ABS_HTTP_URL?>img/x.png" style="width:20px;height:20px" border="0" /></a></td>
					</tr>-->
					<tr>
						<td valign="top">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<?if($showLinkToDelte){?><td width="67" bgcolor="#000" height="30">&nbsp;</td><?}?>
								<td width="100" bgcolor="#000" height="30">&nbsp;</td>
								<?if($showCode){?><td width="107" height="30" align="center" bgcolor="#000" class="blanco12" style="color: #FFF;"><strong><?=$code[$lang]?></strong></td> <?}?>
								<?if($showNombre){?><td width="237" height="30" align="center" bgcolor="#000" class="blanco12" style="color: #FFF;"><strong><?=$product_name[$lang]?></strong></td><?}?>
								<!-- <td width="77" height="25" align="center" valign="middle" bgcolor="#000" class="blanco12" style="color: #FFF;"><strong>Color</strong></td>--> 
						    <td width="77" height="30" align="center" bgcolor="#000" class="blanco12" style="color: #FFF;"><strong>Versi&oacute;n</strong></td> 
								<?if($editProductQuantity){?><td width="127" height="30" align="center" bgcolor="#000" class="negro12a"><strong><?=$qty[$lang]?></strong></td><?}else{?>
								<td width="127" height="30" align="center" bgcolor="#000" class="blanco12" style="color: #FFF;"><strong><?=$qty[$lang]?></strong></td>
								<?}?>
								<td width="107" height="30" align="center" bgcolor="#000" class="blanco12" style="color: #FFF;"><strong><?=$unit_price[$lang]?></strong></td>
								<td width="161" height="30" align="center" bgcolor="#000" class="blanco12" style="color: #FFF;"><strong>Subtotal</strong></td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td valign="top" bgcolor="#00BEFF">&nbsp;</td>
					</tr>
					<? foreach($OUTPUT["array_product"] as $product){
						$id 		= $product['id'];
						$hrefBorrar =  ABS_HTTP_URL."cart.php?cmd=remove&productId=$id";
						$src		= $product['image_url'];
						$codigo		= $product['codigo'];
						$nombre		= $product['name'];
						$version_codigo		= $product['version_sku'];
						$version_nombre		= $product['version_name'];
						$peso		= $product['str_peso'];
						$uvc		= $product['uvc'];
						$uv			= $product['unidad_venta'];
						$quantity	= $product['quantity'];
						$color		= $product['color'];
						$colores	= eCommerce_SDO_Color::LoadColor($color);
						$color 		= $colores->getNombre();
						$talla		= $product['talla'];
						$tallas		= eCommerce_SDO_Talla::LoadTalla($talla);							
						$talla		= $tallas->getNombre();
						$pzsEstima	= $uvc == 'Peso' ? floor($quantity / $product['peso']) : 0;
						$lblEstima	= ucfirst(strtolower($uv))."s ". ($uv=='PIEZA'?str_replace('o','a',$estimated[$lang]):$estimated[$lang])."s";
						$unidad		= $uvc=='Pieza'?" $piece[$lang]".($product['quantity']>1?'s':''):($uvc=='Peso'?" $gram[$lang]".($product['quantity']>1?'s<br /><span style="font-weight:normal">'.$lblEstima.': '.$pzsEstima.'</span>':''):'');
						$unidad		= "";
						$precio		= $product['str_price'];
						$subtotal	= $product['str_total_price'] ?>
						<tr>
							<td valign="top">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size: 14px !important;">
								<tr>
									<? if($showLinkToDelte){?><td width="67"><div align="center" class="rojo12"><strong><a href="<?=$hrefBorrar?>" class="rojo12">
									<img  src="<?=ABS_HTTP_URL?>img/close.png" width="30" alt="<?=$remove[$lang]?>" border="0"></a></strong></div></td><?}?>
									<td width="100" align="center" style="padding:5px 5px 5px 5px;"><img src="<?=$src?>" style="width:60px" /></td>
									<?if($showCode){?><td width="107" align="center"><strong class="negro12"><?=$version_codigo?></strong></td><?}?>
									<?if($showNombre){?><td width="237" align="center" class="negro12" align="center"><?=$version_nombre?></td><?}?>
									<!-- <td width="77" align="center" class="negro12" ><?=$color?></td>--> 
									<td width="77" align="center" class="negro12" ><?=$talla?></td> 
									<?if($editProductQuantity){?><td width="127" align="center" class="negro12" style="padding-top: 14px;">
										<input name="quantity_<?=$id?>" id="quantity_<?=$id?>" value="<?=$quantity?>" size="4" /><strong><?=$unidad?></strong>
										<br />
										<a href="javascript:updateQuantity('<?=$id?>')" style='text-decoration:none;' class="rosa12"><b><?=$update[$lang] ?></b></a>
									</td><?}else{?>
									<td width="127" align="center" class="negro12" style="padding-top: 1px;">
										<strong><?=$quantity?></strong>										
									</td>
									<?}?>
									<td width="107" align="center" class="negro12"><?=$precio?></td>
									<td align="center" class="blanco12a" width="161"><strong><?=$subtotal?></strong></td>
								</tr>
							</table>
							</td>
						</tr>
					<?}?>
					<tr>
						<td valign="top" bgcolor="#00BEFF">&nbsp;</td>
					</tr>
					<tr>
						<td valign="top">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="right" class="blanco12a"><strong class="rosa12">Total</strong></td>
								<td width="200" align="center" class="grishome"><strong class="negro12" ><?=$OUTPUT["str_subtotal"];?></strong></td>
							</tr>
						</table>
						</td>
					</tr>
					<?if( $showExtrasAndTotal ){
						foreach($OUTPUT["extras"] as $extra){?>
						<tr>
						<td valign="top">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="right" class="blanco12a" ><strong class="blanco12a"><?=$extra["description"];?></strong></td>
		                    	<td width="183" align="center" class="blanco12"><div align="center"><span class="blanco12a">
		                    	<?=$extra["str_amount"];?>
		                    	</span></div></td>
							</tr>
						</table>
						</td>
						</tr>
						<?}?>
						<tr>
						<td valign="top">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="right" class="blanco12a" ></td>
								<td width="183" align="center" class="blanco12a"><hr></td>
							</tr>
						</table>
						</td>
						</tr>
						<tr>
						<td valign="top">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td  align="right" class="blanco12a" ><p style="MARGIN-RIGHT: 20px"><span class="blanco12">Total </span></td>
		                    	<td width="183" align="center" class="blanco12a"><div align="center"><span class="blanco12"><b><?=$OUTPUT["str_total"];?></b></span></div></td>
							</tr>
						</table>
						</td>
						</tr>
					<?}?>
				</table>
			</form>
			<?$html = ob_get_contents();
			ob_end_clean();
		}
		return $html;
	}

	public static function setShippingType( $shippingType ){
		return eCommerce_SDO_Core_Application_Cart::setShippingType($shippingType);
	}

	public static function GetTotalCart($extra = false, $descuentos = false){
		$actualCurrency              = eCommerce_SDO_CurrencyManager::GetActualCurrency();
		$cart                        = self::GetCart();				
		$items                       = $cart->getItems();
		$total_quantity              = 0; 
		$cartTotalAmount_cupon       = 0;
		$cartTotalAmount_certificado = 0;


		foreach($items as $item){
			//$productId 		= $item->getProductId();			
			//$product 	= eCommerce_SDO_Catalog::LoadProduct( $productId );
			$productId      = $item->getVersionId();
			$product        = eCommerce_SDO_Catalog::LoadVersion( $productId );
			$total_quantity += $item->quantity;
			$value          = eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $item->price, $product->getCurrency() );
			$total_price    += $value * $item->quantity;
			//$total_weight += $product->getWeight() * $item->quantity;
			$total_weight   = '';
		}
		if($extra){			
			$totalEnvio = eCommerce_SDO_Core_Application_OrderBusinessRules::GetPriceEnvio($total_price);			
			$total_price = $total_price + $totalEnvio;
		}
		return array(	'quantity' => $total_quantity, 
						'price' => array('format' => eCommerce_SDO_CurrencyManager::NumberFormat($total_price) . " " . $actualCurrency,
										 'amount' => $total_price),
						'weight' => $total_weight,
						/*'discount' => array('cupon' => array('format' => eCommerce_SDO_CurrencyManager::NumberFormat($descuentoCupon) . " " . $actualCurrency,
																'amount' => $descuentoCupon))*/
							);
						/*'discount' => array('cupon' => array('format' => eCommerce_SDO_CurrencyManager::NumberFormat($descuentoCupon) . " " . $actualCurrency,
										 					 'amount' => $descuentoCupon),
											'certificado' => array('format' => eCommerce_SDO_CurrencyManager::NumberFormat($descuentoCertificado) . " " . $actualCurrency,
										 					 	   'amount' => $descuentoCertificado)));*/
	}

	public static function GetTotalItems(){
		$actualCurrency 	= eCommerce_SDO_CurrencyManager::GetActualCurrency();
		$cart 				= self::GetCart();
		$items 				= $cart->getItems();	
		$total_quantity = 0;
		foreach($items as $item){
			$productId 		= $item->getVersionId();
			$product 	= eCommerce_SDO_Catalog::LoadVersion( $productId );	
			$total_quantity += $item->quantity;					
		}	
		return $total_quantity;
	}

	public static function GetTotalItemsCount(){
		$actualCurrency 	= eCommerce_SDO_CurrencyManager::GetActualCurrency();
		$cart 				= self::GetCart();
		$items 				= count($cart->getItems());			
		return $items;
	}

	public static function DeleteOrder( $orderId ){
		try{
		 	$order = eCommerce_SDO_Core_Application_Order::Delete( $orderId );
		}catch(Exception $e){
			$order = new eCommerce_Entity_Order();
		}
		return $order;
	} 

	public function cartParseHTACCESLink($args,$type = ''){
		return eCommerce_SDO_Core_Application_Cart::parseHTACCESLink($args,$type);
	}

	public static function getRealQuantity($quantity,$peso,$uv,$uvc){
		switch($uv){
			case 'PIEZA':$quantity = $uvc == 'Peso' ? floor($quantity / $peso) : $quantity;break;
			default:$quantity = $uvc == 'Pieza' ? $quantity : $quantity;break;
		}
		return $quantity;
	}

}
?>