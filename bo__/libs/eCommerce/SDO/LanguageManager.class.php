<?php
class eCommerce_SDO_LanguageManager {
	const LANGUAGE_SPANISH =eCommerce_SDO_Core_Application_LanguageManager::LANGUAGE_SPANISH;
	const DEFAULT_LANGUAGE = eCommerce_SDO_Core_Application_LanguageManager::DEFAULT_LANGUAGE;
	const FILE_MANAGER = eCommerce_SDO_Core_Application_LanguageManager::FILE_MANAGER;
	
	public function GetValidLanguage($language){
		return eCommerce_SDO_Core_Application_LanguageManager::GetValidLanguage($language);
	}
	
	public function GetActualLanguage(){
		return eCommerce_SDO_Core_Application_LanguageManager::GetActualLanguage();
    }
	
	public function GetLanguages(){
		return eCommerce_SDO_Core_Application_LanguageManager::GetLanguages();
	}

	public function SetActualLanguage( $language ){
		return eCommerce_SDO_Core_Application_LanguageManager::SetActualLanguage( $language );
	}
	
	public function CreateSelectHTML( $params , $class ){
		return eCommerce_SDO_Core_Application_LanguageManager::CreateSelectHTML( $params , $class );
	}

	public static function LanguageToZendLang( $lang = null ){
		return eCommerce_SDO_Core_Application_LanguageManager::LanguageToZendLang( $lang);
	}

}
?>