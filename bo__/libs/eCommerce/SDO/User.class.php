<?php
class eCommerce_SDO_User {
	
	const ADMIN_ROLE = eCommerce_SDO_Core_Application_Profile::ADMIN_ROLE;
	const CUSTOMER_ROLE = eCommerce_SDO_Core_Application_Profile::CUSTOMER_ROLE;
	const ANONYMOUS_ROLE	= eCommerce_SDO_Core_Application_Profile::ANONYMOUS_ROLE;
		
	/**
	 * Saves a new or updates an existing User Profile object
	 *
	 * @param eCommerce_Entity_User_Profile $profile
	 * @return eCommerce_Entity_User_Profile
	 * @throws eCommerce_SDO_Application_Exception
	 */
	public static function SaverUserProfile( eCommerce_Entity_User_Profile $profile , $validar_password = true){
		return eCommerce_SDO_Core_Application_Profile::Save( $profile, $validar_password  );
	}
	
	/**
	 * Load the User Profile from the given ID. Note an empty object is returned if the ID doesn't exist
	 *
	 * @param int $categoryId
	 * @return eCommerce_Entity_User_Profile
	 */
	public static function LoadUserProfile( $profileId ){
		return eCommerce_SDO_Core_Application_Profile::LoadById( $profileId ); 
	}
	
	/**
	 * Load the User Profile from the given email. Note an empty object is returned if the email isn't found
	 *
	 * @param string $email
	 * @return eCommerce_Entity_User_Profile
	 */
	public static function LoadUserProfileByEmail( $email ){
		return eCommerce_SDO_Core_Application_Profile::LoadByEmail( $email ); 
	}
	
	/**
	 * Deletes a User Profile from the System
	 *
	 * @param int $profileId                            - The ID of the user profile to be deleted
	 * @return eCommerce_Entity_Catalog_Category        - The recently deleted category
	 * @throws eCommerce_SDO_Core_Application_Exception - When the category is not found or couldn't be deleted
	 */
	public static function DeleteUserProfile( $profileId ){
		return eCommerce_SDO_Core_Application_Profile::Delete( $profileId ); 
	}
	
	/**
	 * Retrieves all the Profiles available.
	 * 
	 * @return eCommerce_Entity_Util_ProfileArray
	 */
	public static function GetAllUserProfiles(){
		return eCommerce_SDO_Core_Application_Profile::GetAll();
	}
	
	/**
	 * Retrieves all the User Profiles filtered by an specific user role and search parameters. 
	 * If the role is Null or an empty string, 
	 *
	 * @param eCommerce_Entity_Search $search - The search parameters
	 * @param string $role                    - The Role to filter by. Empty for all roles.
	 * @return eCommerce_Entity_Search_Result_Profile
	 */
	public static function SearchProfiles( eCommerce_Entity_Search $search, $role = null ){
		return eCommerce_SDO_Core_Application_Profile::Search( $search, $role );
	}
	
	public static function GetRoles( ){
		return eCommerce_SDO_Core_Application_Profile::GetRoles();
	}
	
	public static function GetSaludo( ){
		return eCommerce_SDO_Core_Application_Profile::GetSaludo();
	}
	
	public static function GetConfirmado( ){
		return eCommerce_SDO_Core_Application_Profile::GetConfirmado();
	}

	/**
	 * Saves a new or existing Profile Address (validation is part of the process )
	 *
	 * @param eCommerce_Entity_User_Address $address - The object to be saved
	 * @return eCommerce_Entity_User_Address         - The recently saved object
	 * @throws eCommerce_SDO_Core_Validator_Exception, SQLException
	 */
	public static function SaveUserAddress( eCommerce_Entity_User_Address $address ){
		return eCommerce_SDO_Core_Application_UserAddress::Save( $address );
	}
	
	/**
	 * Retrieves the Address for the given User Profile ID. NOTE: Empty object returned if Address doesn't exist.
	 *
	 * @param int $profileId
	 * @return eCommerce_Entity_User_Address
	 */
	public static function LoadUserAddress( $profileId ){
		return eCommerce_SDO_Core_Application_UserAddress::LoadById( $profileId );
	}
	
	/**
	 * Deletes the Address for the given User Profile ID.
	 *
	 * @param int $profileId
	 * @return eCommerce_Entity_Profile_Address - The recently deleted address
	 * @throws eCommerce_SDO_Core_Application_Exception - When the address is not found or couldn't be deleted
	 */
	public static function DeleteUserAddress( $profileId ){
		return eCommerce_SDO_Core_Application_UserAddress::Delete( $profileId );
	}
}
?>