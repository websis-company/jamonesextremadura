<?php
class eCommerce_SDO_OrderPayment{
	
	/**
	 * Retrieves all the User Profiles filtered by an specific user role and search parameters. 
	 * If the role is Null or an empty string, 
	 *
	 * @param eCommerce_Entity_Search $search - The search parameters
	 * @param string $role                    - The Role to filter by. Empty for all roles.
	 * @return eCommerce_Entity_Search_Result_Profile
	 */
	public static function SearchOrderPayment( eCommerce_Entity_Search $search, $extraConditions = array() ){
		return eCommerce_SDO_Core_Application_OrderPayment::Search( $search, $extraConditions );
	}
	

	/**
	 * Load the User Profile from the given ID. Note an empty object is returned if the ID doesn't exist
	 *
	 * @param int $categoryId
	 * @return eCommerce_Entity_User
	 */
	public static function LoadOrderPayment( $OrderPaymentId ){
		return eCommerce_SDO_Core_Application_OrderPayment::LoadById( $OrderPaymentId ); 
	}

	public static function LoadOrderPaymentByReference( $external_reference ){
		return eCommerce_SDO_Core_Application_OrderPayment::loadOrderPaymentByReference( $external_reference ); 	
	}


	
	/**
	 * Saves a new or updates an existing User Profile object
	 *
	 * @param eCommerce_Entity_User $profile
	 * @return eCommerce_Entity_User
	 * @throws eCommerce_SDO_Application_Exception
	 */
	public static function SaverOrderPayment( eCommerce_Entity_OrderPayment $OrderPayment ){
		
		return eCommerce_SDO_Core_Application_OrderPayment::Save( $OrderPayment );
	}

	/**
	 * Deletes a User Profile from the System
	 *
	 * @param int $profileId                            - The ID of the user profile to be deleted
	 * @return eCommerce_Entity_Catalog_Category        - The recently deleted category
	 * @throws eCommerce_SDO_Core_Application_Exception - When the category is not found or couldn't be deleted
	 */
	public static function DeleteOrderPayment( $OrderPaymentId ){
		return eCommerce_SDO_Core_Application_OrderPayment::Delete( $OrderPaymentId ); 
	}
	
	/**
	 * Retrieves all the OrderPayment available.
	 * 
	 * @return eCommerce_Entity_Util_OrderPaymentrray
	 */
	public static function GetAllOrderPayment($order_by = ''){
		return eCommerce_SDO_Core_Application_OrderPayment::GetAll($order_by);
	}
	
	public static function GetAllInArray(){
		return eCommerce_SDO_Core_Application_OrderPayment::GetAllInArray();
	}
}
?>