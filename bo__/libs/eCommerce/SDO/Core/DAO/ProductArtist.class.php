<?php
class eCommerce_SDO_Core_DAO_ProductArtist extends eCommerce_SDO_Core_DAO {
	
	protected $table = 'product_artist';
	protected $id_field = '';

	/**
	 * @see DB_DAO_GenericDAO::saveNew()
	 *
	 * @param array $arrEvent
	 * @return unknown
	 */
	public function saveNew( array &$arrEvent ){
		throw new Exception( __METHOD__ . 'Method not available for this DAO ' . __CLASS__ . '. Use SaveOrUpdate() instead');
	}

	/**
	 * @see DB_DAO_GenericDAO::saveOrUpdate()
	 *
	 * @param unknown_type $arrEvent
	 * @return unknown
	 */
	public function saveOrUpdate( &$arrEvent ){
		$fields = $this->prepareFields( $arrEvent );

		$sql = "REPLACE INTO `" . $this->table . "` SET " . implode( ",\n ", $fields ) . "";
		$affectedRows = $this->db->sqlExecute( $sql );
		return $affectedRows;
	}

	/**
	 * @see DB_DAO_GenericDAO::update()
	 *
	 * @param unknown_type $arrEvent
	 * @return unknown
	 */
	public function update( &$arrEvent ){
		throw new Exception( __METHOD__ . 'Method not available for this DAO ' . __CLASS__ . '. Use SaveOrUpdate() instead');
	}

	public function getProductArtists( $productId ){
		$sql = "SELECT artist_id "
		     . "FROM   " . $this->table . " "
		     . "WHERE  product_id = '" . $this->db->escapeString( $productId ) . "'";
		return $this->db->sqlGetArray( $sql );
	}
	
	public function getArtistProducts( $artistId ){
		$sql = "SELECT product_id "
		     . "FROM   " . $this->table . " "
		     . "WHERE  artist_id = '" . $this->db->escapeString( $artistId ) . "'";
		return $this->db->sqlGetArray( $sql );
	}
	
	public function deleteRelationsToProduct( $productId ){
		$sql = "DELETE FROM " . $this->table . " WHERE product_id = '" . $this->db->escapeString( $productId ) . "'";
		return $this->db->sqlExecute( $sql );
	}
	
	public function deleteRelationsToArtist( $artistId ){
		$sql = "DELETE FROM " . $this->table . " WHERE artist_id = '" . $this->db->escapeString( $artistId ) . "'";
		return $this->db->sqlExecute( $sql );
	}
	
	public function deleteRelation( $productId, $artistId ){
		$sql = "DELETE FROM " . $this->table . " "
		     . "WHERE  product_id = '" . $this->db->escapeString( $productId ) . "' "
		     . "AND    artist_id = '" . $this->db->escapeString( $artistId ) . "' ";
		     
		
		
		return $this->db->sqlExecute( $sql );
	}
}
?>