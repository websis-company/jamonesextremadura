<?php
class eCommerce_SDO_Core_DAO_Version extends eCommerce_SDO_Core_DAO {
	
	protected $table = 'version';
	protected $id_field = 'version_id';
	
	public function __construct(){
		parent::__construct( new eCommerce_SDO_Core_DB() );
		$this->setSearchFields( array( "titulo") );
	}
	
	public function getAllVersiones($product_id){
		$sql = "SELECT v.* FROM version v
LEFT JOIN relacioncolor rc ON rc.tabla_id = v.version_id
LEFT JOIN color c ON c.color_id = rc.color_id
LEFT JOIN relaciontalla rt ON rt.tabla_id = v.version_id
LEFT JOIN talla t ON t.talla_id = rt.talla_id
WHERE v.product_id = ".$product_id." AND v.status = 'Active' GROUP BY v.modelo ORDER BY v.orden ASC";			

		return $this->db->sqlGetResult( $sql );
	}
	public function getAllVersionesTodo($product_id){
		$sql = "SELECT v.*,c.color_id,c.nombre as colornombre, t.talla_id, t.nombre as tallanombre FROM version v
LEFT JOIN relacioncolor rc ON rc.tabla_id = v.version_id
LEFT JOIN color c ON c.color_id = rc.color_id
LEFT JOIN relaciontalla rt ON rt.tabla_id = v.version_id
LEFT JOIN talla t ON t.talla_id = rt.talla_id				
WHERE v.product_id = ".$product_id." AND v.status = 'Active'";		

		return $this->db->sqlGetResult( $sql );
	}
	public function getAllVersionesColor($product_id,$modelo){
	$sql = "SELECT v.version_id,v.price,c.color_id,c.nombre as colornombre FROM version v
	LEFT JOIN relacioncolor rc ON rc.tabla_id = v.version_id
	LEFT JOIN color c ON c.color_id = rc.color_id	
	WHERE v.product_id = ".$product_id ." AND v.status = 'Active' AND v.modelo = '".$modelo."' GROUP BY c.color_id";
	
	return $this->db->sqlGetResult( $sql );
	}
public function getAllVersionesTalla($product_id,$modelo){
	$sql = "SELECT v.version_id,v.price,t.talla_id, t.nombre as tallanombre FROM version v
	LEFT JOIN relaciontalla rt ON rt.tabla_id = v.version_id
	LEFT JOIN talla t ON t.talla_id = rt.talla_id
	WHERE v.product_id = ".$product_id ." AND v.status = 'Active' AND v.modelo = '".$modelo."' GROUP BY t.talla_id";
	return $this->db->sqlGetResult( $sql );
	}
	
	

}
?>