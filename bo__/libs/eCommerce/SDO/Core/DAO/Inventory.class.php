<?php
class eCommerce_SDO_Core_DAO_Inventory extends eCommerce_SDO_Core_DAO {
	
	const ALL_ATTRIBUTES = 'attribute';
	const TOKEN_ATTRIBUTES = ',';
	
	protected $table = 'inventory';
	protected $id_field = 'inventory_id';
	
	public function __construct(){
		parent::__construct( new eCommerce_SDO_Core_DB() );
		$this->setSearchFields( array( 'inventory_id') );
	}
	
	public function GetAllAttributes(){
		return explode(self::TOKEN_ATTRIBUTES,self::ALL_ATTRIBUTES);
	}
	
	
	public function loadByProduct( $productId){
		$sql = "SELECT *"
		     . " FROM   " . $this->table
		     . " WHERE  product_id= '" . $this->db->escapeString( $productId ) . "'";
		return $this->db->sqlGetRecord( $sql ); 
	}
	
	public function loadByProductAllAttributes( $productId, $attributes){
		$sqlAtt = '';
		$allAtt = self::GetAllAttributes();
		foreach($allAtt as $att){
			$value = empty( $attributes[ $att ] ) ? '' : $attributes[ $att ];
			$sqlAtt .= ' AND ' . $att . ' = "' . $this->db->escapeString($value) . '"'; 
		}
		$sql = "SELECT *"
		     . " FROM   " . $this->table
		     . " WHERE  product_id= '" . $this->db->escapeString( $productId ) . "'"
		     . $sqlAtt; 
		return $this->db->sqlGetRecord( $sql ); 
	}
	
	public function loadByProductGroupByAttribute( $productId, $attribute){
		$atts = self::GetAllAttributes();
		$sqlAtt= '';
		foreach($atts as $att){
			$sqlAtt .=', ' . $att;
		}
		$sql = "SELECT  inventory_id, product_id" . $sqlAtt. ", SUM(n_products) as n_products "
		     . " FROM   " . $this->table
		     . " WHERE  product_id= '" . $this->db->escapeString( $productId ) . "' GROUP BY " . $attribute;
		     
		return $this->db->sqlGetResult( $sql ); 
	}
	
	
	
	public function loadByProductGroupByHeight( $productId){
		$sql = "SELECT  inventory_id, product_id, color, height, SUM(n_products) as n_products "
		     . " FROM   " . $this->table
		     . " WHERE  product_id= '" . $this->db->escapeString( $productId ) . "' GROUP BY height";
		return $this->db->sqlGetResult( $sql ); 
	}
	
	public function loadByProductGroupByColors( $productId){
		$sql = "SELECT  inventory_id, product_id, color, height, SUM(n_products) as n_products "
		     . " FROM   " . $this->table
		     . " WHERE  product_id= '" . $this->db->escapeString( $productId ) . "' GROUP BY color";
		return $this->db->sqlGetResult( $sql ); 
	}
	
	public function loadByProductColorAndHeight( $productId, $color, $height ){
		
		$sql = "SELECT *"
		     . " FROM   " . $this->table
		     . " WHERE  product_id= '" . $this->db->escapeString( $productId ) . "' 
			AND color = '" . $this->db->escapeString( $color ) .  "'
		    AND height = '" . $this->db->escapeString( $height ) .  "'";
		     
		return $this->db->sqlGetRecord( $sql ); 
	}
	
	
}
?>