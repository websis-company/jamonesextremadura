<?php
class eCommerce_SDO_Core_DAO_OrderItem extends eCommerce_SDO_Core_DAO {
	
	protected $table = 'order_item';
	protected $id_field = 'order_item_id';
	
	public function __construct(){
		parent::__construct();
		$this->setSearchFields( 
			array( 'name', 'sku', 'price', 'quantity' )
		);
	}

}
?>