<?php
class eCommerce_SDO_Core_DAO_OrderPayment extends eCommerce_SDO_Core_DAO {
	
	protected $table = 'order_payment';
	protected $id_field = 'order_payment_id';
	
	public function __construct(){
		parent::__construct( new eCommerce_SDO_Core_DB() );
		$this->setSearchFields( array( "order_id","code","state") );
	}
	
//	public function loadByColum1AndOtherColum2( $email ){
//		
//		$sql = "SELECT *"
//		     . " FROM   " . $this->table
//		     . " WHERE  colum1 = '" . $this->db->escapeString( $colum1 ) . "' 
//			AND colum2 = '" . . $this->db->escapeString( $colum2 ) . . "'";
//		return $this->db->sqlGetRecord( $sql ); 
//	}


	public function loadPending($transactionId){
		$sql = "SELECT * FROM order_payment WHERE state = 'pending' AND orderTransactionId = ".$transactionId;

		$db = new eCommerce_SDO_Core_DB();
		return $db->sqlGetResult( $sql );
	}

	public function GetByOrderId($orderId){
		$sql = "SELECT * FROM order_payment WHERE order_id = ".$orderId;
		$db = new eCommerce_SDO_Core_DB();
		return $db->sqlGetResult( $sql );	
	}

	public function loadOrderPaymentByReference($external_reference){
		$sql = "SELECT * FROM order_payment WHERE reference = '".$external_reference."'";
		$db = new eCommerce_SDO_Core_DB();
		return $db->sqlGetRecord( $sql );
	}
}
?>