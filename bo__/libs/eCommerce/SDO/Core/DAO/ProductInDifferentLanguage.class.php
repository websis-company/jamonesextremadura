<?php
class eCommerce_SDO_Core_DAO_ProductInDifferentLanguage extends eCommerce_SDO_Core_DAO {
	
	protected $table = 'product_in_different_language';
	protected $id_field = 'product_id';
	
	public function __construct(){
		parent::__construct( new eCommerce_SDO_Core_DB() );
		//$this->setSearchFields( array( 'first_name', 'last_name', 'email', 'role' ) );
	}
	
	
	
	public function loadByProductId( $id ){
		$sql = "SELECT 	* 
						FROM		`" . $this->table . "`
						WHERE		" . $this->id_field . " = '" . addslashes( $id ) . "'";
		
		//$obj = $this->db->sqlGetRecord( $sql );
		return $this->db->sqlGetResult( $sql );
	}
	
	public function loadByIdAndLanguage( $productId, $languageId ){
		$sql = "SELECT *"
		     . " FROM   " . $this->table
		     . " WHERE  " . $this->id_field . " = " . $productId . " AND 
		     language_id = '" . $languageId . "'";
		return $this->db->sqlGetRecord( $sql ); 
	}
	
	
	//-----------------------------------------
	//-------DB_DAO_GenericDAO-----------------
	//-----------------------------------------
	public function saveOrUpdate( &$arrEvent ){
		$product = self::loadByIdAndLanguage( $arrEvent[ "product_id" ],$arrEvent[ "language_id" ]);
		if ( empty($product) || $product == new eCommerce_Entity_Catalog_ProductInDifferentLanguage() ){
			$return = $this->saveNew( $arrEvent );
		}
		else {
			$return = $this->update( $arrEvent );
		}
		
		return $return;
	}
	
	public function update( &$arrEvent ){
		$id = $arrEvent[ $this->id_field ];
		unset( $arrEvent[ $this->id_field ] );
		
		$fields = $this->prepareFields( $arrEvent );
		
		$sql =  "UPDATE `" . $this->table . "`" .
						" SET   " . join( ',', $fields ) . 
						" WHERE " . $this->id_field . " = '" . addslashes( $id ) . "'" . 
						" AND language_id = '". $arrEvent["language_id"] . "'"; 
		$affectedRows = $this->db->sqlExecute( $sql );
		$arrEvent = $this->loadById( $id );
		return $affectedRows;
	}
}
?>