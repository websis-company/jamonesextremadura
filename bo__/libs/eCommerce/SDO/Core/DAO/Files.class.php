<?php
class eCommerce_SDO_Core_DAO_Files extends eCommerce_SDO_Core_DAO {
	
	protected $table = 'files';
	protected $id_field = 'files_id';
	
	public function __construct(){
		parent::__construct( new eCommerce_SDO_Core_DB() );
		$this->setSearchFields( array( "name") );
	}
	
//	public function loadByColum1AndOtherColum2( $email ){
//		
//		$sql = "SELECT *"
//		     . " FROM   " . $this->table
//		     . " WHERE  colum1 = '" . $this->db->escapeString( $colum1 ) . "' 
//			AND colum2 = '" . . $this->db->escapeString( $colum2 ) . . "'";
//		return $this->db->sqlGetRecord( $sql ); 
//	}

	public function LoadByGaleryId($galeryId){
		$sql = "SELECT * FROM ".$this->table." WHERE galeria_id = '".$galeryId."'";
		$db = new eCommerce_SDO_Core_DB();
		$arr  = $db->sqlGetResult($sql);
		return $arr;
	}
	
	
}
?>