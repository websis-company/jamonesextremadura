<?php
class eCommerce_SDO_Core_DAO_Category extends eCommerce_SDO_Core_DAO {
	
	protected $table = 'category';
	protected $id_field = 'category_id';
	
	
	public function __construct(){
		parent::__construct( new eCommerce_SDO_Core_DB() );
		$this->setSearchFields(
			array( 'category_id', 'parent_category_id', 'name', 'description' )
		);
	}


	public function GetImplodeCategories($category_id, $delimit = ',', $menu = ''){
		$sql = "SELECT CONCAT_WS('$delimit',c4.name,c3.name,c2.name,c1.name) AS categories
				FROM category c1
				LEFT JOIN category c2 ON c1.parent_category_id = c2.category_id
				LEFT JOIN category c3 ON c2.parent_category_id = c3.category_id
				LEFT JOIN category c4 ON c3.parent_category_id = c4.category_id
				WHERE c1.category_id = $category_id "
				.($menu ? " AND (c1.menu = '$menu' OR c2.menu = '$menu' OR c3.menu = '$menu' OR c4.menu = '$menu')" : '');
		return $this->db->sqlGetField($sql);
	}

	public function GetSomeCategorys($excluir){
		$sql = 'SELECT * FROM category WHERE parent_category_id IS NULL';
		if($excluir != false){
			$ids = explode(',',$excluir);	
			foreach ($ids as $id) {
				$sql .= ' AND category_id != '.$id;
			}
		}
		$sql .= ' ORDER BY orden ASC ';
		return $this->db->sqlGetResult($sql);
	}

	public function getParentCategory($categoriaActual){
		$sql = "SELECT category.* FROM category
				INNER JOIN category AS c ON category.category_id = c.parent_category_id
				AND category.parent_category_id = 2 AND c.category_id = ".$categoriaActual;
		return $this->db->sqlGetResult($sql);
	}
	
}
?>