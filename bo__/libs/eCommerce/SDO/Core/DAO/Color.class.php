<?php
class eCommerce_SDO_Core_DAO_Color extends eCommerce_SDO_Core_DAO {
	
	protected $table = 'color';
	protected $id_field = 'color_id';
	
	public function __construct(){
		parent::__construct( new eCommerce_SDO_Core_DB() );
		$this->setSearchFields( array( "nombre") );
	}
	
	public function GetAllColoresByCategoriaId($categoria_id){
		$sql = "SELECT c.* FROM (((color c
				LEFT JOIN relacioncolor rc ON rc.color_id = c.color_id)
				LEFT JOIN version v ON v.version_id = rc.tabla_id)
				LEFT JOIN product p ON p.product_id = v.product_id)
				LEFT JOIN product_category pc ON p.product_id = pc.product_id
				WHERE ".$categoria_id."
				GROUP BY c.color_id";
		//echo $sql;
		return $this->db->sqlGetResult( $sql );
	}
	
	public function GetAllColoresByBrandId($brand_id){
		$sql = "SELECT c.* FROM `color` c
				INNER JOIN relacioncolor rc ON c.color_id = rc.color_id
				INNER JOIN product p ON p.brand_id IS NOT NULL
				WHERE p.brand_id = ".$brand_id."
				GROUP BY c.color_id
				ORDER BY c.color_id ASC";
		return $this->db->sqlGetResult( $sql );
	}
	
}
?>