<?php

class eCommerce_SDO_Core_DAO_Order extends eCommerce_SDO_Core_DAO_Timestampable {
	
	protected $table = 'order';
	protected $id_field = 'order_id';
	
	public function __construct(){
		parent::__construct();
		$this->setSearchFields( 
			array( 'order_id' )
		);
	}
	
	public function getSqlFrom(){
		return "FROM `" . $this->table . "`" ;
	} 

}
?>