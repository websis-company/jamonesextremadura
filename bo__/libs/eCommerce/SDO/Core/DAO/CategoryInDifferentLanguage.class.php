<?php
class eCommerce_SDO_Core_DAO_CategoryInDifferentLanguage extends eCommerce_SDO_Core_DAO {
	
	protected $table = 'category_in_different_language';
	protected $id_field = 'category_in_different_language_id';
	
	public function __construct(){
		parent::__construct( new eCommerce_SDO_Core_DB() );
		$this->setSearchFields( array( 'category_id','name','description') );
	}
	
	public function LoadByCategoryIdAndLanguageId( $categoryId, $languageId ){
		$sql = "SELECT *"
		     . " FROM   " . $this->table
		     . " WHERE  category_id= '" . $this->db->escapeString( $categoryId ) . "' 
			AND language_id = '" .  $this->db->escapeString( $languageId )  . "'";
		return $this->db->sqlGetRecord( $sql ); 
	}

//	public function loadByColum1AndOtherColum2( $email ){
//		
//		$sql = "SELECT *"
//		     . " FROM   " . $this->table
//		     . " WHERE  colum1 = '" . $this->db->escapeString( $colum1 ) . "' 
//			AND colum2 = '" . . $this->db->escapeString( $colum2 ) . . "'";
//		return $this->db->sqlGetRecord( $sql ); 
//	}
	
	
}
?>