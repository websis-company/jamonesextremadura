<?php
class eCommerce_SDO_Core_DAO_Artist extends eCommerce_SDO_Core_DAO {
	
	protected $table = 'artist';
	protected $id_field = 'artist_id';
	
	
	public function __construct(){
		parent::__construct( new eCommerce_SDO_Core_DB() );
		$this->setSearchFields(
			array( 'artist_id', 'name')
		);
	}
	
}
?>