<?php
class eCommerce_SDO_Core_DAO_Product extends eCommerce_SDO_Core_DAO {
	
	protected $table = 'product';
	protected $id_field = 'product_id';
	
	protected $language = eCommerce_SDO_Core_Application_LanguageManager::DEFAULT_LANGUAGE;
	protected $deaultLanguage = eCommerce_SDO_Core_Application_LanguageManager::DEFAULT_LANGUAGE;
	
	public function setLanguage( $lang ){
		$lang = eCommerce_SDO_Core_Application_LanguageManager::GetValidLanguage( $lang );
		$this->language = $lang;
	}
	
	public function __construct(){
		//$this->deaultLanguage = eCommerce_SDO_Core_Application_LanguageManager::DEFAULT_LANGUAGE;
		parent::__construct();
	
		//$actualLanguage = eCommerce_SDO_Core_Application_LanguageManager::GetActualLanguage();
		
		$this->setSearchFields( array( 
				'p.name', 'p.description_short', 'p.description_long', 'p.sku', 'p.product_id', 'p.price',
				'pdL.name', 'pdL.description_short', 'pdL.description_long'
				) );
		/*
		if( $actualLanguage != $this->deaultLanguage){
			$this->setSearchFields( array( 
				'p.name', 'p.description_short', 'p.description_long', 'p.sku', 'p.product_id', 'p.price',
				'pdL.name', 'pdL.description_short', 'pdL.description_long'
				) );
		}else{
			$this->setSearchFields( array('p.name', 'p.description_short', 'p.description_long', 'p.sku', 'p.product_id', 'p.price' )	);
		}
		*/
	}
	/**
	 * @see DB_DAO_AdvancedDAO::getSqlFrom()
	 *
	 * @return unknown
	 */
	protected function getSqlFrom(){
		//debug($this->deaultLanguage,'default:');
		//debug($this->language);
		$sql = "FROM " . $this->table . " AS p "
		     . "LEFT JOIN product_category AS pc ON p.product_id = pc.product_id "
		     . "LEFT JOIN product_artist AS pa ON p.product_id = pa.product_id "
		     . "LEFT JOIN brand AS b ON p.brand_id = b.brand_id "
		     . "LEFT JOIN version AS V ON p.product_id = V.product_id ";
		
		$sql .= "LEFT JOIN product_in_different_language AS pdL ON p.product_id = pdL.product_id ";
		$joins = '';
		if(count($this->joins)>0){
			foreach ($this->joins as $join){
				$joins .= "{$join['type']} JOIN `{$join['table']}` ON {$join['on']} ";
			}
		}
		return $sql. " $joins";
		
		
		
		/*if( $this->language != $this->deaultLanguage){
			$sql .= "LEFT JOIN product_in_different_language AS pdL ON p.product_id = pdL.product_id ";
		}*/
		
		
		return $sql;
	}

	/**
	 * @see DB_DAO_AdvancedDAO::getSqlSelect()
	 *
	 * @return unknown
	 */
	protected function getSqlSelect(){

		if( $this->language === $this->deaultLanguage){
			//$return = "SELECT p.*, b.name AS brand_name, V.keywords, V.price AS precio, V.currency as currency ";
			$return = "SELECT p.*, b.name AS brand_name";
		}else{
			$return ="SELECT p." . $this->getIdField() .",p.sku,p.status, p.price, p.currency,
			p.discount,p.image_id,p.weight, p.volume, pdL.name, pdL.description_short,
			pdL.description_long, pdL.dimensions, pdL.finished,pdL.materials,
			pdL.colors, b.name AS brand_name";
		}
		return $return;
	}

	/**
	 * @see DB_DAO_AdvancedDAO::getSqlGroupBy()
	 *
	 * @return unknown
	 */
	protected function getSqlGroupBy(){
		return "GROUP BY p.product_id";
	}
	
	public function loadById( $id, $loadStructureIfEmpty = false ){
		$sql = $this->getSqlSelect()." ". 
					 $this->getSqlFrom()." ".
					"WHERE		p." . $this->id_field . " = '" . addslashes( $id ) . "'";
		$obj = $this->db->sqlGetRecord( $sql );
		
		if ( empty( $obj ) && $loadStructureIfEmpty ){
			return $this->loadStructure();
		}
		else {
			return $obj;			
		}
	}

	public function loadDestacados(){
		$sql = "SELECT * FROM  " . $this->table." WHERE mas_vendido = 'Si'";
		return $this->db->sqlGetResult( $sql ); 	
	}
	
}
?>