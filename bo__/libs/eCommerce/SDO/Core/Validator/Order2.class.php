<?php
/**
 * This class provides access to manipulation of Orders in the System.
 * 
 * @author Alejandro Rivera <alejandro@vicomstudio.com>
 */
class eCommerce_SDO_Core_Application_Order extends eCommerce_SDO_Core_Application {
	
	const ADDRESS_SHIP_TO = 'Ship To';
	const ADDRESS_BILL_TO = 'Bill To';
	
	const INVALID_STATUS = 'IN';
	const INVALID_PAYMENT_METHOD = 'INP';
	
	
	
	
	/**
	 * Load the Product from the given ID. Note an empty object is returned if the ID doesn't exist
	 *
	 * @param int $orderId
	 * @return eCommerce_Entity_Order
	 */
	public static function LoadById( $orderId ){
		
		$dao = self::GetDAO();
		$entity = $dao->loadById( $orderId, true );       
		$order = new eCommerce_Entity_Order();   // Create new clean object to prevent old values being conserved
		$order = eCommerce_SDO_Core_Util_EntityManager::
		            ParseArrayToObject( $entity, $order );  // Convert array to to Object
		return $order;
	}
	
	/**
	 * Saves a new or existing Product into the System (validation is part of the process )
	 *
	 * @param eCommerce_Entity_Order $order - The object to be saved
	 * @return eCommerce_Entity_Order       - The recently saved object
	 * @throws eCommerce_SDO_Core_Validator_Exception, SQLException
	 */
	public static function Save( eCommerce_Entity_Order $order ){
		// 1. Validate object
		$validator = new eCommerce_SDO_Core_Validator_Order( $order );
		$validator->validate();
		
		if ( !$validator->isValid() ){
			throw new eCommerce_SDO_Core_Validator_Exception( $validator ); 
		}
		
		// 2. Save or Update Object
		
		$entity = new ArrayObject( $order );        // Convert Object to Array
		
		$dao = self::GetDAO();
		$dao->saveOrUpdate( $entity );
		
		// 3. Retrieve record from DB
		$order = self::LoadById( $entity[ 'order_id' ] );
		if ( $order == new eCommerce_Entity_Order() ){

			throw new Exception( "The Order saved could not be retrieved." );      
		}
		// 4. Return recently saved object
		return $order;
	}
	
	/**
	 * Deletes an Order from the System
	 *
	 * @param int $orderId            - The ID of the product to be deleted
	 * @return eCommerce_Entity_Order - The recently deleted product
	 * @throws eCommerce_SDO_Core_Application_Exception - When the order is not found or couldn't be deleted
	 */
	public static function Delete( $orderId ){
		// 1. Ensure the order actually exists
		$order = self::LoadById( $orderId );
		
		if ( $order == new eCommerce_Entity_Order() ){
			throw new eCommerce_SDO_Core_Application_Exception( 'The order to be deleted doesn\'t exist.' );
		}
		else {
			try {
				// 2. Delete the Product
				$dao = self::GetDAO();
				$dao->delete( $orderId );
				
				// 3. Return the recently deleted Product
				return $order;
			}
			catch ( SQLException $e ){
				throw new eCommerce_SDO_Core_Application_Exception( 'There was an unexpected error while trying to delete the Order', 0, $e );
			}
		}
	}
	
	/**
	 * Retrieves the appropriate DAO
	 *
	 * @return eCommerce_SDO_Core_DAO_Order
	 */
	protected static function GetDAO(){
		return new eCommerce_SDO_Core_DAO_Order();
	}
	
	/**
	 * Retrieves all the Orders available.
	 * 
	 * @return eCommerce_Entity_Util_OrderArray
	 */
	public static function GetAll(){
		$dao = self::GetDAO();
		$arrOrders = $dao->loadAll();                           // Load all products as a 2D array
		$objOrders = self::ParseArrayToObjectArray( $arrOrders );
		return $objOrders;
	}
	
	/**
	 * Searches for all the products where the Parent Product ID matches the one provided.
	 * NOTE: If Category ID is provided, only those products strictly beneath that 
	 * category will be returned. This method does NOT retrieve all the products under the category's 
	 * child categories.
	 *
	 * @param eCommerce_Entity_Search $search - Search parameters (pagination, keywords, etc.)
	 * @param int $categoryId                 - The Category ID or 0/null for the all products
	 * @return eCommerce_Entity_Search_Result_Product
	 * @throws eCommerce_SDO_Core_Application_Exception
	 */
	public static function Search( eCommerce_Entity_Search $search, $profileId = null ){
		try {
			$dao = self::GetDAO();
			if ( !empty( $profileId ) ){
				$dao->addExtraCondition( "profile_id = '" . $dao->getDB()->escapeString( $profileId ) . "'" );
			}
			// Retrieve orders
			$arrOrders = $dao->loadAllByParameters(
				$search->getKeywords(),
				$search->getSearchAsPhrase(),
				$search->getOrderBy(),
				$search->getPage(),
				$search->getResultsPerPage()
			);
			// Retrieve total pages
			$totalPages = $dao->loadAllByParameters(
				$search->getKeywords(),
				$search->getSearchAsPhrase(),
				$search->getOrderBy(),
				$search->getPage(),
				$search->getResultsPerPage(),
				true
			);
			
			$result = new eCommerce_Entity_Search_Result_Order( $search );
			$result->setTotalPages( $totalPages );
			$result->setResults( self::ParseArrayToObjectArray( $arrOrders ) ); 
			return $result;
		}
		catch( SQLException $e ){
			throw new eCommerce_SDO_Core_Application_Exception( 
				'Error processing the search request. Please see the nested exception for details',
				0, $e );
		}
	}
	
	/**
	 * @param array $arrOrders
	 * @return eCommerce_Entity_Util_ProductArray
	 */
	protected static function ParseArrayToObjectArray( array $arrOrders ){
		// Create the array that will hold the Product objects
		$objOrders = new eCommerce_Entity_Util_OrderArray(); 
		foreach( $arrOrders as $arrOrder ){
			// Transform each array into object
			$objOrder = eCommerce_SDO_Core_Util_EntityManager::
			               ParseArrayToObject( $arrOrder, new eCommerce_Entity_Order() );
			// Add object to Order array  
			$objOrders[ $objOrder->getOrderId() ] = $objOrder;  
		}
		return $objOrders;
	}

	public static function GetHTMLByOrderId( $orderId, $language = null ){
		
		$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
		if ($actualLanguage == 'EN')
		{
			$lang = 0;
		}
		else
		{
			$lang = 1;
		}
		$name[0] = "Product Name";
		$name[1] = "Nombre del producto";
		
		$qty[0] = "Qty";
		$qty[1] = "Cant";
		
		$unt_pr[0] = "Unit Price";
		$unt_pr[1] = "Precio Unitario";
		
		$pr_disc[0] = "Price with discount";
		$pr_disc[1] = "Precio con descuento";
		
		$cod[0] = "Code";
		$cod[1] = "C&oacute;digo";
		
		$order = eCommerce_SDO_Core_Application_OrderMediation::CreateOrderFullFromOrderId( $orderId, $language );

		$serverSite = ABS_HTTP_URL;
		$html ='';
		
		if( !empty($order) ){
			$items =  $order->getItems(); 
			
			$orderCurrency = $order->getCurrency();

			$html .= "<table><tr>";
			
			$html .= "<td></td><th>".$cod[$lang]."</th><th width='250' align='center'>".$name[$lang]."</th>
			<th>".$qty[$lang]."</th><th width='100'>".$unt_pr[$lang]."</th><th width='100'>".$pr_disc[$lang]."</th><th>Total</th>
			</tr>";
			
			$currency ='';
			foreach( $items as $item ){

				$product = eCommerce_SDO_Core_Application_Product::LoadById( $item->getProductId() );
				if( !empty($product) && $product->getImageId() > 0 ){
					$imageUrl = '"'. $serverSite .'file.php?id=' .$product->getImageId();
					$imageUrl .= '&type=image&img_size=predefined&width=51&height=50"';
				}
				else{
					$imageUrl = '"'. $serverSite .'ima/fo/default.jpg" width="50" height="50"';
				}

				$html .= "<tr>";
				
			
				$html .= "<td><img src={$imageUrl} ></td>";
				$html .= "<td>" . $item->getSku() . "</td>";
				$html .= "<td><a href='";
				
				if( !empty($product) ){
					$html .= $serverSite ."product.php?cmd=details&id=" . $product->getProductId();
				}else{
					$html .= $serverSite ."productNotExist.html";
				}
				
				$html .= "' target='_blank'>";
				$html .= $item->getName();
				
				$html .= "</a></td>";
				$html .= "<td>";

				$html .= $item->getQuantity() ;
				
				$html .= "</td>";
				
				$html .= "<td align='center'>" . $item->getPrice() . " " . $orderCurrency . "</td>";

				$value = $item->getRealPrice();
				$html .= "<td align='center'>" . eCommerce_SDO_Core_Application_CurrencyManager::numberFormat($value) . " " . $orderCurrency . "</td>";
				
				$productTotal = $value * $item->getQuantity();
				
				$html .= "<td>" . eCommerce_SDO_Core_Application_CurrencyManager::numberFormat($productTotal) . " " . $orderCurrency . "</td>";
				$html .= "</tr>";

			}
			$subTotal = $order->getProductsSubTotal();
			
			$html .= "<tr>";

			$html .= "<td colspan='7'><hr /></td></tr>";
			
			$html .="<tr align='right'>";
			
			
			$html .= "<td colspan='6'>Sub Total : </td><td>" . eCommerce_SDO_Core_Application_CurrencyManager::numberFormat($subTotal) . " " . $orderCurrency . "</td></tr>";
			
			//---------------------EXTRAS
			$extras = $order->getFees(); //eCommerce_SDO_Cart::GetExtras();
			foreach( $extras as $fee){
				$html .= "<tr>";

				$html .= "<td colspan='6' align='right'>" . $fee->getDescription() . " : </td>" . 
				"<td align='right'>" . $fee->getAmount() . " " . $orderCurrency . "</td></tr>";
			}
			$html .= "<tr>";
			$html .= "<td colspan='7'><hr /></td></tr>";
			$html .="<tr align='right'>";
			$html .= "<td colspan='6'>TOTAL : </td><td align='left'>" . eCommerce_SDO_Core_Application_CurrencyManager::numberFormat($order->getTotal()) . " " . $orderCurrency . "</td></tr>";
			$html .= "</table>";
		}
	return $html;
	}

	public static function GetValidStatus( $statusId ){
		$validStatus = array( 'N', 'C');
		return ( in_array($statusId,$validStatus) ) ? $statusId : eCommerce_SDO_Core_Application_Order::INVALID_STATUS;
	}
	public static function GetArrStatus(){
		$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
		if ($actualLanguage == 'EN')
		{
			$pend = "Payment Pending";
			$comp = "Completed";
		}
		else
		{
			$pend = "Pago Pendiente";
			$comp = "Completada";
		}
		return array( 'N'=> $pend , 'C' => $comp);
	}
	public static function GetStatusById( $statusId ){
	$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
		if ($actualLanguage == 'EN')
		{
			$pend = "Unknow Status";
		}
		else
		{
			$pend = "Estado Desconocido";
		}
		$arrStatus = self::GetArrStatus();
		return ( isset($arrStatus[ $statusId ]) ) ? $arrStatus[ $statusId ] : $pend; 
	}
	
	public static function GetValidPaymentMethods( $paymentId ){
		$validPayments = array( 'P', 'D');
		return ( in_array($paymentId,$validPayments) ) ? $paymentId : eCommerce_SDO_Core_Application_Order::INVALID_PAYMENT_METHOD;
	}
	public static function GetArrPaymentMethods(){
		$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
		if ($actualLanguage == 'EN')
		{
			$dep = "Deposit";
		}
		else
		{
			$dep = "Dep&oacute;sito";
		}
		return array( 'P'=> 'Paypal' , 'D' => $dep);
	}
	public static function GetPaymentMethodById( $paymentId ){
		$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
		if ($actualLanguage == 'EN')
		{
			$dep = "Unknow Payment Method";
		}
		else
		{
			$dep = "M&eacute;todo de pago desconocido";
		}
		$arrPayments = self::GetArrPaymentMethods();
		return ( isset($arrPayments[ $paymentId ]) ) ? $arrPayments[ $paymentId ] : $dep; 
	}
}
?>