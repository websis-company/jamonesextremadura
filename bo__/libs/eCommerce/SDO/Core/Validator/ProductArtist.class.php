<?php
class eCommerce_SDO_Core_Validator_ProductArtist extends eCommerce_SDO_Core_Validator {
	
	/**
	 * @var eCommerce_Entity_Catalog_ProductArtist
	 */
	protected $entity;

	public function __construct( eCommerce_Entity_Catalog_ProductArtist $productArtist ){ 
		parent::__construct( $productArtist );
	}
	
	/**
	 * @see eCommerce_SDO_Core_Validator::setup()
	 */
	protected function setup(){
		$this->setupArtistId();
		$this->setupProductId();
	}
	
	protected function setupArtistId(){
		$tester = new Validator_Tester( $this->entity->getArtistId(), true );
		$tester->addTest( new Validator_Test_Numeric_UnsignedInteger(),
		                  'Artist ID must be a positive integer value, zero or null' );
		$tester->addTest( new eCommerce_SDO_Core_Validator_Test_ExistingEntity( 
		                      new eCommerce_SDO_Core_DAO_Artist() ),
		                  'The Artist ID provided doesn\'t exist.' );
		$this->validator->addTester( 'artist_id', $tester );
	}
	
	protected function setupProductId(){
		$tester = new Validator_Tester( $this->entity->getProductId(), true );
		$tester->addTest( new Validator_Test_Numeric_UnsignedInteger(),
		                  'Product ID must be a positive integer value, zero or null' );
		$tester->addTest( new eCommerce_SDO_Core_Validator_Test_ExistingEntity( 
		                      new eCommerce_SDO_Core_DAO_Product() ),
		                  'The Product ID provided doesn\'t exist.' );
		$this->validator->addTester( 'product_id', $tester );
	}
}
?>