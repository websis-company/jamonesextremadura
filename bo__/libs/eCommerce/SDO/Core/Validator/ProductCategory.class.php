<?php
class eCommerce_SDO_Core_Validator_ProductCategory extends eCommerce_SDO_Core_Validator {
	
	/**
	 * @var eCommerce_Entity_Catalog_ProductCategory
	 */
	protected $entity;

	public function __construct( eCommerce_Entity_Catalog_ProductCategory $productCategory ){ 
		parent::__construct( $productCategory );
	}
	
	/**
	 * @see eCommerce_SDO_Core_Validator::setup()
	 */
	protected function setup(){
		$this->setupCategoryId();
		$this->setupProductId();
	}
	
	protected function setupCategoryId(){
		$tester = new Validator_Tester( $this->entity->getCategoryId(), true );
		$tester->addTest( new Validator_Test_Numeric_UnsignedInteger(),
		                  'Category ID must be a positive integer value, zero or null' );
		$tester->addTest( new eCommerce_SDO_Core_Validator_Test_ExistingEntity( 
		                      new eCommerce_SDO_Core_DAO_Category() ),
		                  'The Category ID provided doesn\'t exist.' );
		$this->validator->addTester( 'category_id', $tester );
	}
	
	protected function setupProductId(){
		$tester = new Validator_Tester( $this->entity->getProductId(), true );
		$tester->addTest( new Validator_Test_Numeric_UnsignedInteger(),
		                  'Product ID must be a positive integer value, zero or null' );
		$tester->addTest( new eCommerce_SDO_Core_Validator_Test_ExistingEntity( 
		                      new eCommerce_SDO_Core_DAO_Product() ),
		                  'The Product ID provided doesn\'t exist.' );
		$this->validator->addTester( 'product_id', $tester );
	}
}
?>