<?php
class eCommerce_SDO_Core_Validator_Artist extends eCommerce_SDO_Core_Validator {
	
	/**
	 * @var eCommerce_Entity_Catalog_Category
	 */
	protected $entity;

	public function __construct( eCommerce_Entity_Catalog_Artist $artist ){ 
		parent::__construct( $artist );
	}
	
	/**
	 * @see eCommerce_SDO_Core_Validator::setup()
	 */
	protected function setup(){
		$this->setupArtistId();
		$this->setupName();
		$this->setupEmail();
		$this->setupUrl();
		$this->setupType();	
	}
	
	protected function setupArtistId(){
		$tester = new Validator_Tester( $this->entity->getArtistId(), false );
		$tester->addTest( new Validator_Test_Numeric_UnsignedInteger(),
		                  'Artist ID must be a positive integer value, zero or null' );
		$this->validator->addTester( 'artist_id', $tester );
	}
	
	protected function setupType(){
		$tester = new Validator_Tester( $this->entity->getType(), true );
		$tester->addTest( new Validator_Test_NotEmpty(), 'Artist Type is required and must not be empty or null' );
		
		$types = eCommerce_SDO_Core_Application_Artist::GetArtistTypes();
		if( !in_array($this->entity->getType(), $types) ){
			$types = join(",",$types);
			$tester->addTest( new Validator_Test_ReturnFalse(), 'Artist Type must be a valid type [' . $types ."]" );
		}
		$this->validator->addTester( 'type', $tester );
	}

	protected function setupName(){
		$tester = new Validator_Tester( $this->entity->getName(), true );
		
		$tester->addTest( new Validator_Test_NotEmpty(), 'Artist name is required and must not be empty or null' );
		
		// Verify name is a valid string instead of anything else
		$tester->addTest( new Validator_Test_String(),
		                  'Artist name must be a valid string' );
		
		$this->validator->addTester( 'name', $tester );
	}
	
	protected function setupEmail(){
		$tester = new Validator_Tester( $this->entity->getEmail(), true );
		
		$tester->addTest( new Validator_Test_NotEmpty(), 
		                'Artist email is required and must not be empty or null' );
		
		// Verify name is a valid string instead of anything else
		$tester->addTest( new Validator_Test_String(),
		                  'Artist email must be a valid string' );
		
		$this->validator->addTester( 'email', $tester );
	}
	
	protected function setupUrl(){
		$tester = new Validator_Tester( $this->entity->getUrl(), true );
		
		$tester->addTest( new Validator_Test_NotEmpty(), 
		                'Artist url is required and must not be empty or null' );
		
		// Verify name is a valid string instead of anything else
		$tester->addTest( new Validator_Test_String(),
		                  'Artist url must be a valid string' );
		
		$this->validator->addTester( 'url', $tester );
	}
	
}
?>