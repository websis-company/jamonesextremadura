<?php
class eCommerce_SDO_Core_Validator_File extends eCommerce_SDO_Core_Validator {
	
	/**
	 * @var eCommerce_Entity_Util_FileUpload
	 */
	protected $entity;

	public function __construct( eCommerce_Entity_Util_FileUpload $file ){ 
		parent::__construct( $file );
	}
	
	/**
	 * @see eCommerce_SDO_Core_Validator::setup()
	 */
	protected function setup(){
		$tester = new Validator_FileTester( $this->entity->getStructureName(), true );
		$this->addTests( $tester );
		$this->validator->addFileTester( $this->entity->getStructureName(), $tester );
	}
	
	protected function getTests( Validator_FileTester $tester ){
		$tester->addTest( new Validator_Test_FileUpload('.*'), 'Not a valid file.' );
	}
}
?>