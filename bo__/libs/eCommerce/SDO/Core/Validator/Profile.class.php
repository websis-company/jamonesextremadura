<?php
class eCommerce_SDO_Core_Validator_Profile extends eCommerce_SDO_Core_Validator {
	
	/**
	 * @var eCommerce_Entity_User_Profile
	 */
	protected $entity;
	protected $obligatorio;
	public $validar_password;

	public function __construct( eCommerce_Entity_User_Profile $profile , $validar_password = true ){ 
		$this->validar_password = $validar_password;
		parent::__construct( $profile );
	}
	
	/**
	 * @see eCommerce_SDO_Core_Validator::setup()
	 */
	protected function setup(){
		//$this->validar_password = $this->entity->role == eCommerce_SDO_User::ANONYMOUS_ROLE ? false : $this->validar_password;
		$this->obligatorio = $this->entity->role != eCommerce_SDO_User::ANONYMOUS_ROLE;
		
		$this->setupProfileId();
		$this->setupFirstName();
		$this->setupLastName();
		$this->setupEmail();
		
	if($this->validar_password && $this->obligatorio){
			$this->setupPassword($result);
			$this->setupConfirmPassword($result);
		}
		$this->setupRole();
	}
	
	protected function setupProfileId( ){
		$tester = new Validator_Tester( $this->entity->getProfileId(), false );
		
		$tester->addTest( new Validator_Test_Numeric_UnsignedInteger(),
		                  $this->trans('profile_id').' '.str_replace('{n}', '0', $this->trans('integer_number_equal_greater_n_msg' )));
		
		$this->validator->addTester( 'profile_id', $tester );
	}
	
	protected function setupFirstName(){
		$tester = new Validator_Tester( $this->entity->getFirstName(), false );
		
		$tester->addTest( new Validator_Test_NotEmpty(), 
		               $this->trans('first_name').' '.$this->trans('is_required_msg') );
		
		$tester->addTest( new Validator_Test_String_NoHTML(),
		                  $this->trans('first_name').' '.$this->trans('no_html_tags_msg') );
		
		$this->validator->addTester( 'first_name', $tester );
	}

	protected function setupLastName(){
		$tester = new Validator_Tester( $this->entity->getLastName(), false );
		
		$tester->addTest( new Validator_Test_NotEmpty(), 
		               $this->trans('last_name').' '.$this->trans('is_required_msg') );
		
		$tester->addTest( new Validator_Test_String_NoHTML(),
		                  $this->trans('last_name').' '.$this->trans('no_html_tags_msg') );
		
		$this->validator->addTester( 'last_name', $tester );
	}
		
	protected function setupEmail(){
		$tester = new Validator_Tester( $this->entity->getEmail(), true );
		
		$tester->addTest( new Validator_Test_NotEmpty(), 
		               		'Email '.$this->trans('is_required_msg') );
				
		$tester->addTest( new Validator_Test_Email(),
		               		'Email '.$this->trans('not_valid_msg') );
				
		if( !eCommerce_FrontEnd_FO_Authentification::verifyAuthentification( false ) ){
			$tester->addTest( new eCommerce_SDO_Core_Validator_Test_UniqueProfileEmail( $this->entity ),
		               			$this->trans('email_already_chosen_msg'));
		} else {
			$user = eCommerce_FrontEnd_Util_UserSession::GetIdentity();
			if($this->entity->getEmail() != $user->getEmail() ){
				$tester->addTest( new eCommerce_SDO_Core_Validator_Test_UniqueProfileEmail( $this->entity ),
		               			$this->trans('email_already_chosen_msg'));
			}
		}
		
		$this->validator->addTester( 'email', $tester );
	}
	
	protected function setupPassword(){
		$tester = new Validator_Tester( $this->entity->getPassword(), false );
		
		$tester->addTest( new Validator_Test_NotEmpty(), 
		               		$this->trans('password').' '.$this->trans('is_required_msg(f)') );
				
		$tester->addTest( new Validator_Test_RegEx( '/^.{4,}$/'),
		               		$this->trans('password').' '.str_replace('{n}', '4', $this->trans('n_characters_minimum_msg')) );
				
		$this->validator->addTester( 'password', $tester );
	}
	
	protected function setupConfirmPassword(){
		$tester = new Validator_Tester( $this->entity->getConfirmPassword(), false );
		
		$tester->addTest( new Validator_Test_NotEmpty(), 
		               		$this->trans('confirm_password_required_msg') );
				
		$tester->addTest( new Validator_Test_Equal($this->entity->getPassword()),
		               		$this->trans('confirm_password_match_up_msg') );
				
		$this->validator->addTester( 'confirm_password', $tester );
	}
	
	protected function setupRole(){
		$tester = new Validator_Tester( $this->entity->getRole(), true );
		
		$tester->addTest( new Validator_Test_NotEmpty(), 
		               		$this->trans('role').' '.$this->trans('is_required_msg') );
				
		$this->validator->addTester( 'role', $tester );
	}
	
}
?>