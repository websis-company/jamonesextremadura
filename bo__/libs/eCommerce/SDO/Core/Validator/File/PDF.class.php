<?php
class eCommerce_SDO_Core_Validator_File_PDF extends eCommerce_SDO_Core_Validator_File {
	
	protected function addTests( Validator_FileTester $tester ){
		$tester->addTest( new Validator_Test_FileUpload_PDF(), 'Not a valid PDF file.' );
	}
	
}
?>