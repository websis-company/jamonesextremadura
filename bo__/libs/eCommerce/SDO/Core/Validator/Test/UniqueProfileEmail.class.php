<?php
class eCommerce_SDO_Core_Validator_Test_UniqueProfileEmail implements Validator_Test {

	/**
	 * @var eCommerce_Entity_User_Profile
	 */
	protected $profile;
	
	/**
	 * @param eCommerce_Entity_User_Profile $profile - The profile to be validated containing an unique email address
	 */
	public function __construct( eCommerce_Entity_User_Profile $profile ){
		$this->profile = $profile;
	}

	/**
	 * @param int $variable
	 * @see Validator_Test::isValid()
	 */
	public function isValid( $variable ){
		$tmpProfile = eCommerce_SDO_Core_Application_Profile::LoadByEmail( $variable );
		 
		$emailTmp = $tmpProfile->getEmail();
		
		if ( empty( $emailTmp ) || $tmpProfile->getProfileId() == $this->profile->getProfileId() ){
			// The email belongs to the same profile we are validating. No problem here 
			return true;
		}
		else {
			// The email is stored under another user profile. This email is not unique.
			return false;
		}
	}
	
}
?>