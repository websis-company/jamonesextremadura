<?php
class eCommerce_SDO_Core_Validator_Test_ExistingEntity implements Validator_Test {

	/**
	 * @var eCommerce_SDO_Core_DAO
	 */
	protected $dao;
	
	/**
	 * @param eCommerce_SDO_Core_DAO $dao
	 */
	public function __construct( DB_DAO_GenericDAO $dao ){
		$this->dao = $dao;
	}

	/**
	 * @param int $variable
	 * @see Validator_Test::isValid()
	 */
	public function isValid( $variable ){
		$entity = $this->dao->loadById( $variable );
		if ( empty( $entity ) ){
			return false; 
		}
		else {
			return true;
		}
	}
	
}
?>