<?php
/**
 * This class provides access to manipulation of the Product-Category (M:N) Relationship.
 * 
 * @author Alejandro Rivera <alejandro@vicomstudio.com>
 */
class eCommerce_SDO_Core_Application_ProductCategory extends eCommerce_SDO_Core_Application {
	
	/**
	 * @var eCommerce_SDO_Core_DAO_ProductCategory
	 */
	static protected $dao;
	
	/**
	 * Associates a new Product-Category relation. It inserts a new tuple or replaces an existing one. 
	 *
	 * @param int $categoryId
	 * @param int $productId
	 */
	protected static function CreateProductCategoryRelation( eCommerce_Entity_Catalog_ProductCategory $relation ){
		
		$validator = new eCommerce_SDO_Core_Validator_ProductCategory( $relation );
		$validator-> validate();
		if ( !$validator->isValid() ){
			throw new eCommerce_SDO_Core_Validator_Exception( $validator );
		}
		else {
			$entity = new ArrayObject( $relation );
			$dao = self::GetDAO();
			try {
				$dao->saveOrUpdate( $entity );
			}
			catch( SQLException $e ){
				throw new eCommerce_SDO_Core_Application_Exception( 
					'Could not create association between Product ' . $relation->getProductId() . 
				  ' and Category ' . $relation->getCategoryId() . '. Please see details.',
					0,		// Exception code 
					$e    // Nested exception
				);
			}
		}
	}
	
	/**
	 * Products to relate Categories to Products
	 */
	
	/**
	 * Associates a single Category to the given Product
	 *
	 * @param int $productId
	 * @param int $categoryId
	 * @return eCommerce_Entity_Util_CategoryArray - The list of ALL (not just the recently added) categories belonging to the product
	 * @throws eCommerce_SDO_Core_ValidatoException, Exception
	 * @todo Don't throw the SQLException
	 */
	public static function AssociateCategoryToProduct( $productId, $categoryId ){
		return self::AssociateCategoriesToProduct( $productId, array( $categoryId ) );
	}
	
	/**
	 * Associates several Categories to a Product in a batch (single transaction) 
	 *
	 * @param int $productId
	 * @param array<int> $categoryIds
	 * @return eCommerce_Entity_Util_CategoryArray - The list of ALL (not just the recently added) categories belonging to the product
	 * @throws eCommerce_SDO_Core_Validator_Exception, SQLException, Exception
	 * @todo Don't throw the SQLException
	 */
	public static function AssociateCategoriesToProduct( $productId, array $categoryIds ){
		
		$dao = self::GetDAO();
		try {
			$dao->getDB()->beginTransaction();
			
			foreach( $categoryIds as $categoryId ){
				$prodCat = new eCommerce_Entity_Catalog_ProductCategory( $productId, $categoryId );
				self::CreateProductCategoryRelation( $prodCat );
			}
			$dao->getDB()->commit();
			return self::GetCategoriesByProduct( $productId );
		}
		catch( Exception $e ){
			$dao->getDB()->rollback();
			throw $e;
		}
	}
	
	/**
	 * Updates ALL the categories related to a Product in a batch (single transaction). Read the notes.
	 * NOTE: 
	 *  a) This method REMOVES ALL categories relations currently stored in the DB 
	 *  b) This method INSERTS ALL categories relations in the categories array
	 * If an exception is thrown, the whole transaction is rolled back.
	 *
	 * @param int $productId
	 * @param array $categoryIds
	 * @return eCommerce_Entity_Util_CategoryArray
	 * @throws eCommerce_SDO_Core_Validator_Exception, SQLException, Exception
	 * @todo Don't throw the SQLException
	 */
	public static function UpdateProductCategories( $productId, array $categoryIds ){
		$dao = self::GetDAO();
		try {
			$dao->getDB()->beginTransaction();
			// We begin the transaction because if there's a rollback when want the deletion of this 
			// relations to be restored.
			$dao->deleteRelationsToProduct( $productId );
			// There's NO commit here because it's handlded inside AssociateCategoriesToProduct()
			return self::AssociateCategoriesToProduct( $productId, $categoryIds );
		}
		catch( Exception $e ){
			// Rollback is handled by AssociateCategoriesToProduct()
			throw $e;
		}
	}
	
	public static function IsProductASculpture($productId){
		$categories = self::GetCategoriesByProduct( $productId );
		$result = false;
		foreach($categories as $category){
			if($category->getCategoryId() == eCommerce_SDO_Core_Application_Category::CATEGORY_SCULPTURES_ID){
				$result = true;
				break;
			}
		}
		
		return $result;
	}
	/**
	 * Deletes ALL Category associations to the give Product ID
	 *
	 * @param int $productId
	 * @return void
	 */
	public static function DeleteProductCategoriesAssociation( $productId ){
		$dao = self::GetDAO();
		$dao->deleteRelationsToProduct( $productId );
	}
	
	/**
	 * Returns an array of Categories belonging to the given Product ID
	 *
	 * @param int $productId
	 * @return eCommerce_Entity_Util_CategoryArray
	 */
	public static function GetCategoriesByProduct( $productId, $ownership = NULL ){
		$dao = self::GetDAO();
		$arrCatIds = $dao->getProductCategories( $productId , $ownership);
		$objCategoryArray = new eCommerce_Entity_Util_CategoryArray();
		foreach( $arrCatIds as $catId ){
			$category = eCommerce_SDO_Core_Application_Category::LoadById( $catId );
			$objCategoryArray[ $catId ] = $category;
		}
		return $objCategoryArray;
	}
	
	public static function GetParentCategoryByProductId( $productId ){
		$arrCategories = self::GetCategoriesByProduct( $productId );
		//$idParentCategory = 1;
		$idParentCategory = array();
		foreach($arrCategories as $category){
			/*if($category->getCategoryId() == 1 || $category->getCategoryId() == 2){
				$idParentCategory = $category->getCategoryId(); 
				break;
			}*/
			$idParentCategory[] = $category->getCategoryId();
		}
		return $idParentCategory;
	} 
	
	/**
	 * Deletes a particular Product-Category relation
	 *
	 * @param int $productId
	 * @param int $categoryId
	 * @return void
	 */
	public static function DeleteProductCategoryAssociation( $productId, $categoryId ){
		$dao = self::GetDAO();
		$dao->deleteRelation( $productId, $categoryId );
	}
	
	
	/**
	 * Operations to relate Products with Categories
	 */
	
	/**
	 * Associates a single Category to the given Product
	 *
	 * @param int $categoryId
	 * @param int $productId
	 * @return eCommerce_Entity_Util_ProductArray - The list of ALL (not just the recently added) categories belonging to the product
	 * @throws eCommerce_SDO_Core_Validator_Exception, SQLException, Exception
	 * @todo Don't throw the SQLException
	 */
	public static function AssociateProductToCategory( $categoryId, $productId ){
		return self::AssociateProductsToCategory( $categoryId, array( $productId ) );
	}
	
	/**
	 * Associates several Products to a Category in a batch (single transaction) 
	 *
	 * @param int $categoryId
	 * @param array<int> $productIds
	 * @return eCommerce_Entity_Util_ProductArray - The list of ALL (not just the recently added) products belonging to the category
	 * @throws eCommerce_SDO_Core_Validator_Exception, SQLException, Exception
	 * @todo Don't throw the SQLException
	 */
	public static function AssociateProductsToCategory( $categoryId, array $productIds ){
		
		$dao = self::GetDAO();
		try {
			$dao->getDB()->beginTransaction();
			
			foreach( $productIds as $productId ){
				$prodCat = new eCommerce_Entity_Catalog_ProductCategory( $productId, $categoryId );
				self::CreateProductCategoryRelation( $prodCat );
			}
			$dao->getDB()->commit();
			return self::GetProductsByCategory( $categoryId );
		}
		catch( Exception $e ){
			$dao->getDB()->rollback();
			throw $e;
		}
	}
	
	/**
	 * Updates ALL the Products related to a Category in a batch (single transaction). Read the notes.
	 * NOTE: 
	 *  a) This method REMOVES ALL product relations currently stored in the DB 
	 *  b) This method INSERTS ALL product relations in the product array
	 * If an exception is thrown, the whole transaction is rolled back.
	 *
	 * @param int $categoryId
	 * @param array $productIds
	 * @return eCommerce_Entity_Util_ProductArray
	 * @throws eCommerce_SDO_Core_Validator_Exception, SQLException, Exception
	 * @todo Don't throw the SQLException
	 */
	public static function UpdateCategoryProducts( $categoryId, array $productIds ){
		$dao = self::GetDAO();
		try {
			$dao->getDB()->beginTransaction();
			// We begin the transaction because if there's a rollback when want the deletion of this 
			// relations to be restored.
			$dao->deleteRelationsToCategory( $categoryId );
			// There's NO commit here because it's handlded inside AssociateCategoriesToProduct()
			return self::AssociateProductsToCategory( $categoryId, $productIds );
		}
		catch( Exception $e ){
			// Rollback is handled by AssociateCategoriesToProduct()
			throw $e;
		}
	}
	
	/**
	 * Deletes ALL Product associations to the give Category ID
	 *
	 * @param int $categoryId
	 * @return void
	 */
	public static function DeleteCategoryProductsAssociation( $categoryId ){
		$dao = self::GetDAO();
		$dao->deleteRelationsToCategory( $categoryId );
	}
	
	/**
	 * Returns an array of Products belonging to the given Category ID
	 *
	 * @param int $categoryId
	 * @return eCommerce_Entity_Util_ProductArray
	 */
	public static function GetProductsByCategory( $categoryId , $limit = null){
		$dao = self::GetDAO();
		$arrProdIds = $dao->getCategoryProducts( $categoryId , $limit);
		
		$objProductArray = new eCommerce_Entity_Util_ProductArray();
		foreach( $arrProdIds as $productId ){
			$product = eCommerce_SDO_Core_Application_Product::LoadById( $productId );
			$objProductArray[ $productId ] = $product;
		}
		return $objProductArray;
	}
	
	public static function getCountGaleryById($id_category){
		$dao = self::GetDAO();
		$entity = $dao->getCountGaleryById($id_category);
		return $entity;
	}

	/**
	 * @return eCommerce_SDO_Core_DAO_ProductCategory
	 */
	protected static function GetDAO(){
		if ( self::$dao == null ){
			self::$dao = new eCommerce_SDO_Core_DAO_ProductCategory();  
		}
		return self::$dao; 
	}

}
?>