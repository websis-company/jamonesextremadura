<?php
/**
 * This class provides access to manipulation of Currency in the System.
 * 	
 * @author Alberto Pérez <alberto@vicomstudio.com>
 */
class eCommerce_SDO_Core_Application_LanguageManager extends eCommerce_SDO_Core_Application {
	const LANGUAGE_SPANISH ='SP';
	const DEFAULT_LANGUAGE = 'SP';
	const SESSION_NAMESPACE = 'LanguageManager';
	const FILE_MANAGER = 'changeLanguage.php';
	
	
	public static function SetActualLanguage( $language ){
		$language = self::GetValidLanguage( $language );
		eCommerce_FrontEnd_Util_Session::Set( self::SESSION_NAMESPACE, $language['id'] );
	}
	
	public static function GetActualLanguage(){
		//get the actual language of the session 
		//if session says null then return the defaultLanguage
		//pendient to programming
		$tmp = eCommerce_FrontEnd_Util_Session::Get( self::SESSION_NAMESPACE );
		if ( !empty( $tmp ) ){
			$language = self::GetValidLanguage( $tmp );
			$language = $language[ "id" ];
		}else{
			$language = self::DEFAULT_LANGUAGE;
		}
		$language = ( empty($language) ) ? eCommerce_SDO_Core_Application_LanguageManager::DEFAULT_LANGUAGE : $language;
		return $language;
	}
	
	public static function GetValidLanguage( $language ){
		$languages = self::GetLanguages();
		return (empty($languages[ $language ]) ) ? $languages[ self::DEFAULT_LANGUAGE ] : $languages[ $language ];
	}
	
	public static function LanguageToZendLang( $lang = null ){
		$lang = empty($lang) ? self::GetActualLanguage() : $lang;
		switch( $lang ){
			case "SP":$lang = "es"; break;
			case "EN":
			default:
				$lang = "en";
			break;
		}
		return $lang;
	}
	
	public static function GetLanguages(){
		return array();
	}
	
	public static function CreateSelectHTML( $params , $class ){
		$ArrLanguages = self::GetLanguages();
		$actualLanguage = self::GetActualLanguage();
		
		//$relativeReference = $relativeReference . self::FILE_MANAGER;
		
		$html = '<select name="language" ' . $params . ' class="' . $class . '">';
		foreach( $ArrLanguages as $language){
			$html .= "<option value='{$language["id"]}'";
			if( $actualLanguage == $language["id"] ){
				$html .= " selected='selected'";
				
			}
			$html .= ">{$language["name"]}";
			if($language["id"] == self::DEFAULT_LANGUAGE){
				$html .=" - Default";
			}
			$html .= "</option>";
		}
		$html .= "</select>";
		
		return $html;
	}
}
?>