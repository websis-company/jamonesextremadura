<?php
/**
 * This class provides access to manipulation of the Products in the System.
 * 
 * @author Alberto Pérez <alberto@vicomstudio.com>
 */
class eCommerce_SDO_Core_Application_ProductInDifferentLanguage extends eCommerce_SDO_Core_Application {

	/**
	 * @param array $arrProducts
	 * @return eCommerce_Entity_Util_ProductArray
	 */
	public static function GetByIdAndLanguage( $productId, $language ){
		// Create the array that will hold the Product objects
		$dao = self::GetDAO();
		$productEntity = $dao->loadByIdAndLanguage($productId, $language );
		$objProduct = eCommerce_SDO_Core_Util_EntityManager::
			               ParseArrayToObject( $productEntity, new eCommerce_Entity_Catalog_ProductInDifferentLanguage());
		return $objProduct;
	}
	
	public static function Save( eCommerce_Entity_Catalog_ProductInDifferentLanguage $product ){
		// 1. Validate object
		$validator = new eCommerce_SDO_Core_Validator_ProductInDifferentLanguages( $product );
		$validator->validate();
		
		if ( !$validator->isValid() ){
			throw new eCommerce_SDO_Core_Validator_Exception( $validator ); 
		}
		
		// 2. Save or Update Object
		$entity = new ArrayObject( $product );        // Convert Object to Array
		
		$dao = self::GetDAO();
		$dao->saveOrUpdate( $entity );
		
		// 3. Retrieve record from DB
		$product = $dao->loadByIdAndLanguage( $entity[ 'product_id' ], $entity[ 'language_id' ] );
		
		if ( $product == new eCommerce_Entity_Catalog_ProductInDifferentLanguage() ){
			throw new Exception( "The Product Language saved could not be retrieved." );      
		}
		// 4. Return recently saved object
		return $product;
	}

	/* Retrieves all the Products available.
	 * 
	 * @return eCommerce_Entity_Util_ProductArray
	 */
	public static function GetAll( $productId ){
		$dao = self::GetDAO();
		
		$arrPIDLang = $dao->loadByProductId( $productId );// Load all products as a 2D array
		
		$arrProducts = array();
		
		foreach( $arrPIDLang as $PIDLang ){
			$arrProducts[] = $PIDLang;
		}
		//$objProductInDifferentLanguage = new eCommerce_Entity_Util_ProductInDifferentLanguageArray();
		$objProductInDifferentLanguage = self::ParseArrayToObjectArray( $arrProducts );
		//return the product in the differents language different to the default language
		//if the language dosent exist return a empty instance of eCommerce_Entity_Catalog_ProductInDifferentLanguage
		$languages = eCommerce_SDO_LanguageManager::GetLanguages();
		unset( $languages[ eCommerce_SDO_LanguageManager::DEFAULT_LANGUAGE ] );
		foreach( $languages as $language){
			$language = $language["id"];
			if( !isset($objProductInDifferentLanguage[ $language ]) ){
				$objProductInDifferentLanguage[ $language ] = new eCommerce_Entity_Catalog_ProductInDifferentLanguage();
			}
			
		}
		return $objProductInDifferentLanguage;
	}
	
	public static function GetProductInLanguage( $productId, $language ){
		$ArrInDifLang = self::GetAll( $productId );
		return ( empty( $ArrInDifLang[ $language ] ) ) ? new eCommerce_Entity_Catalog_ProductInDifferentLanguage() : $ArrInDifLang[ $language ]; 
	}
	
	//is the array of object that contains the products in differents languages
	protected static function GetDAO(){
		return new eCommerce_SDO_Core_DAO_ProductInDifferentLanguage();
	}
	
 	
	/**
	 * @param array $arrProducts
	 * @return eCommerce_Entity_Util_ProductArray
	 */
	protected static function ParseArrayToObjectArray( array $arrProducts ){
		// Create the array that will hold the Product objects
		$objProducts = new eCommerce_Entity_Util_ProductInDifferentLanguageArray();
		 
		foreach( $arrProducts as $arrProduct ){
			// Transform each array into object
			$objProduct = eCommerce_SDO_Core_Util_EntityManager::
			               ParseArrayToObject( $arrProduct, new eCommerce_Entity_Catalog_ProductInDifferentLanguage());
			// Add object to product array  
			$objProducts[ $objProduct->getLanguageId() ] = $objProduct;  
		}
		return $objProducts;
	}
	
	

}
?>