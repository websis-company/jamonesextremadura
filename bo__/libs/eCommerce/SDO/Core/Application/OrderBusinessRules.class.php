<?php
class eCommerce_SDO_Core_Application_OrderBusinessRules extends eCommerce_SDO_Core_Application {
	
	const TAX_EXPORT = 0.10;	// Exportation Tax
	const TAX_IVA = 0.15;	// IVA Tax
	
	/**
	 * Shipping fee calculation
	 */
	
	/**
	 * Retrieves the estimated shipping fee for a given cart
	 *
	 * @var eCommerce_Entity_Cart $cart - The cart. If none is given, the cart in the session will be used.
	 * @return float
	 * @throws eCommerce_SDO_Core_Application_Exception
	 */
	public static function CalculateShipping( eCommerce_Entity_Cart $cart = null ){
		
		if ( empty( $cart ) ){
			$cart = eCommerce_SDO_Core_Application_Cart::GetCart();
		}
		
		try {
			self::CheckCartForShipping( $cart );
			$weight = self::GetCartWeight( $cart );
			$price = self::GetPriceFromShippingMatrix( $weight, $cart->getAddressByType( eCommerce_SDO_Core_Application_Cart::ADDRESS_SHIP_TO ) );
			return $price;
		}
		catch( Exception $e ){
			throw new eCommerce_SDO_Core_Application_Exception( 
				'There was an error calculating the Shipping fee. ' .
				'See the details for more information', 0, $e 
			);
		}
	}
	
	/**
	 * @param eCommerce_Entity_Cart $cart
	 * @throws eCommerce_SDO_Core_Application_Exception
	 */
	protected static function CheckCartForShipping( eCommerce_Entity_Cart $cart ){
		if ( count( $cart->getItems() ) == 0 ){
			throw new eCommerce_SDO_Core_Application_Exception( 
				'There must be at least one product in the cart to proceed.' );
		}
		if ( $cart->getAddressByType( 'Ship To' ) == null ){
			throw new eCommerce_SDO_Core_Application_Exception( 
				'Cannot calculate Shipping without a Ship To address.' );
		}
		
	}
	
	/**
	 * @param eCommerce_Entity_Cart $cart
	 * @return float
	 * @throws eCommerce_SDO_Core_Application_Exception
	 */
	protected static function GetCartWeight( eCommerce_Entity_Cart $cart ){
		$totalWeight = 0;
		for ( $it = $cart->getItems()->getIterator(); $it->valid(); $it->next() ){
			$item = $it->current();
			$product = eCommerce_SDO_Core_Application_Product::LoadById( $item->getProductId() );
			if ( $product->getProductId() == 0 ){
				throw new eCommerce_SDO_Core_Application_Exception( 'The Product in the Cart with ID ' . $item->getProductId() . ' does not exist in the system.' );
			}
			
			$totalWeight += ($product->getWeight() * $item->getQuantity() );
		}
		return $totalWeight;
	}
	
	
	protected static function GetPriceFromShippingToUS( $weight, $state = null ){
		//prices in MX currency
		$price = 100;
		$centerZone = eCommerce_SDO_Core_Application_CountryManager::GetStatesOfCountryByZone( 'US', 0);
		if(!is_numeric($weight) || $weight< 0){$weight = 0;}
		//---------------------------------------------
		
		//calculate the price to any state by default
		switch( $weight ){
			//less or equal than 1Kg
			case ( $weight >= 0 && $weight <= 1000):
				$price = 100;
			break;
			default:
				$price = 150;
			break;
		}
		
		//WE NEED get the cost in the actual currency
		//return the cost in the actual currency
		$price = eCommerce_SDO_Core_Application_CurrencyManager::ValueInActualCurrency( $price, 'MXN' );
		
		return $price;
		
	}
	
	public static function GetPriceFromShippingToMX( $weight ){		
			$price = 0;		
			$tipoEnvio = eCommerce_SDO_Core_Application_Cart::GetCart()->getShippingType();
			
		switch ($tipoEnvio){
			case 'fijo':
			default:
				$price = 350;
		}		
		$price = eCommerce_SDO_Core_Application_CurrencyManager::ValueInActualCurrency( $price, 'MXN' );		
		return $price;			
		}	
		
		
		
		public static function GetPriceEnvio( $total, $precioEnvio ){	
			/*if($total<=3500){
				$price = 150;
			}elseif($total > 3500 && $total <= 5000){
				$price = 100;
			}elseif($total > 5000){
				$price = 0;
			}*/
			//$price = empty($precioEnvio)?0:$precioEnvio;
			
			$price = eCommerce_SDO_Core_Application_CurrencyManager::ValueInActualCurrency( $precioEnvio, 'MXN' );
			return $price;
		}
		
	
	
	protected static function GetPriceFromShippingToWorld( $weight ){
 
		$price = 0;

		if(!is_numeric($weight) || $weight< 0)
		{
			$weight = 0;
		}
		//---------------------------------------------
		//calculate the price to any state by default
		switch( $weight ){
			//less than 1Kg
			case ( $weight >= 0 && $weight <= 1000):
				$price = 250;
			break;
			default:
				$price = 500;
			break;
		}
		//WE NEED get the cost in the actual currency
		//return the cost in the actual currency
		$price = eCommerce_SDO_Core_Application_CurrencyManager::ValueInActualCurrency( $price, 'MXN' );
		
		return $price;
		
	}
	
	/**
	 * @param float $weight
	 * @param eCommerce_Entity_Util_Address $address
	 * @throws eCommerce_SDO_Core_Application_Exception
	 */
	protected static function GetPriceFromShippingMatrix( $weight, eCommerce_Entity_Util_Address $address ){
		$price = 0;
		
		switch( $address->getCountry() ){
			case 'US':
				$price = self::GetPriceFromShippingToUS( $weight, $address->getState() );
			break;
			
			case 'MX':
				$price = self::GetPriceFromShippingToMX( $weight, $address->getState());
			break;
			
			default:
				$price = self::GetPriceFromShippingToWorld( $weight, $address->getState());
			break;
		}
		
		return $price;
		/*
		throw new eCommerce_SDO_Core_Application_Exception( 'Shipping Price Matrix does not contemplate the weight and destination combination.', 500 );
		*/
	}

	
	
public static function CalculateTaxes( eCommerce_Entity_Cart $cart = null ){
		if ( empty( $cart ) ){
			$cart = eCommerce_SDO_Core_Application_Cart::GetCart();
		}
		
		$itemsTotal = self::GetCartItemsSubTotal( $cart );
		$taxes =0;
		
		$addressShipTo = $cart->getAddressByType( eCommerce_SDO_Core_Application_Cart::ADDRESS_SHIP_TO );
		
		if( !empty( $addressShipTo ) ){
			
			switch ( $addressShipTo->getCountry() ){
				case 'MX':
					// charge the IVA tax
					$taxes = $itemsTotal * ( self::TAX_IVA );
					break;
				case 'US':
					// Pay the US percentage export tax
					//$taxes = $itemsTotal * ( 1 + self::TAX_EXPORT );
					$taxes = $itemsTotal * ( self::TAX_IVA );
					break;
				default:
					// Pay twice as much as the US as export tax
					//$taxes = $itemsTotal * ( 1 + ( 2 * self::TAX_EXPORT ) );
					$taxes = $itemsTotal * ( ( 2 * self::TAX_IVA ) );
					break;
			}
		}
		
		$taxes = round($taxes, 2);
		return $taxes;
	}
	/**
	 * Exportation Taxes
	 */
	
	/**
	 * Retrieves the export fee for a given cart
	 *
	 * @var eCommerce_Entity_Cart $cart - The cart. If none is given, the cart in the session will be used.
	 * @return float
	 * @throws eCommerce_SDO_Core_Application_Exception
	 */
	public static function CalculateExportTaxes( eCommerce_Entity_Cart $cart = null ){
		if ( empty( $cart ) ){
			$cart = eCommerce_SDO_Core_Application_Cart::GetCart();
		}
		
		$itemsTotal = self::GetCartItemsSubTotal( $cart );
		$taxes =0;
		
		$addressShipTo = $cart->getAddressByType( eCommerce_SDO_Core_Application_Cart::ADDRESS_SHIP_TO );
		
		
		if( !empty( $addressShipTo ) ){
			
			switch ( $addressShipTo->getCountry() ){
				case 'MX':
					// Don't charge the export tax
					$taxes = 0;
					break;
				case 'US':
					// Pay the US percentage export tax
					//$taxes = $itemsTotal * ( 1 + self::TAX_EXPORT );
					$taxes = $itemsTotal * ( self::TAX_EXPORT );
					break;
				default:
					// Pay twice as much as the US as export tax
					//$taxes = $itemsTotal * ( 1 + ( 2 * self::TAX_EXPORT ) );
					$taxes = $itemsTotal * ( ( 2 * self::TAX_EXPORT ) );
					break;
			}
		}
		$taxes = 0;
		
		$taxes = round($taxes, 2);
		return $taxes;
	}
	
	protected static function GetCartItemsSubTotal( eCommerce_Entity_Cart $cart ){
		$total = 0;
		
		//$actualDiscount = $cart->getPromotionCodeDiscount();
			
		for ( $it = $cart->getItems()->getIterator(); $it->valid(); $it->next() ){
			$item = $it->current();
			$product = eCommerce_SDO_Core_Application_Product::LoadById( $item->getProductId() );
			if ( $product->getProductId() == 0 ){
				throw new eCommerce_SDO_Core_Application_Exception( 'The Product in the Cart with ID ' . $item->getProductId() . ' does not exist in the system.' );
			}
			
			//we apply the promotion code discount
				//$TotalDiscount = $product->getDiscount() + $actualDiscount;
				//$product->setDiscount( $TotalDiscount );
				
				$value = $product->getRealPrice();
				$productTotal = $value * $item->getQuantity();
				
			//WE NEED get the cost in the actual currency
			//return the cost in the actual currency
			$value = eCommerce_SDO_Core_Application_CurrencyManager::ValueInActualCurrency( $productTotal, $product->getCurrency() );
			$total += $value;
		}
		
		return $total;
	}

}
?>