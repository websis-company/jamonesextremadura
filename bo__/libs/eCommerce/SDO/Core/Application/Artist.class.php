<?php
/**
 * This class provides access to manipulation of Categories in the System.
 * 
 * @author Alejandro Rivera <alejandro@vicomstudio.com>
 */
class eCommerce_SDO_Core_Application_Artist extends eCommerce_SDO_Core_Application {
	const TYPE_SCULPTOR = 'sculptor';
	
	/**
	 * Load the Category from the given ID. Note an empty object is returned if the ID doesn't exist
	 *
	 * @param int $categoryId
	 * @return eCommerce_Entity_Catalog_Category
	 */
	public static function LoadById( $artistId ){
		
		$dao = self::GetDAO();
		$entity = $dao->loadById( $artistId, true );       
		$artist = new eCommerce_Entity_Catalog_Artist();   // Create new clean object to prevent old values being conserved
		$artist = eCommerce_SDO_Core_Util_EntityManager::
		            ParseArrayToObject( $entity, $artist );  // Convert array to to Object
		return $artist;
	}
	
	
	
	public static function SearchByType($search, $type){
		$extraConditions = array();
		$dao = self::GetDAO();
		$extraConditions[] = "type = '" . $dao->getDB()->escapeString( $type ) . "'";
		$result = self::Search($search, $extraConditions);
		
		return $result; 
	}
	
	public static function Search( eCommerce_Entity_Search $search, $extraConditions = array() ){
		try {
			$dao = self::GetDAO();
			if(!empty($extraConditions)){
				foreach($extraConditions as $extraCondition){
					$dao->addExtraCondition( $extraCondition );	
				}
			}
			// Retrieve categories
			$arrArtists = $dao->loadAllByParameters(
				$search->getKeywords(),
				$search->getSearchAsPhrase(),
				$search->getOrderBy(),
				$search->getPage(),
				$search->getResultsPerPage()
			);
			// Retrieve total pages
			$totalPages = $dao->loadAllByParameters(
				$search->getKeywords(),
				$search->getSearchAsPhrase(),
				$search->getOrderBy(),
				$search->getPage(),
				$search->getResultsPerPage(),
				true
			);
			
			$result = new eCommerce_Entity_Search_Result_Artist( $search );
			$result->setTotalPages( $totalPages );
			$result->setResults( self::ParseArrayToObjectArray( $arrArtists ) ); 
			return $result;
		}
		catch( SQLException $e ){
			throw new eCommerce_SDO_Core_Application_Exception( 
				'Error processing the search request. Please see the nested exception for details',
				0, $e );
		}
	}
	
	protected static function GetDAO(){
		return new eCommerce_SDO_Core_DAO_Artist();
	}
	
	
	protected static function ParseArrayToObjectArray( array $arrArtists ){
		// Create the array that will hold the Category objects
		$objArtists = new eCommerce_Entity_Util_ArtistArray(); 
		foreach( $arrArtists as $arrArtist ){
			// Transform each array into object
			$objArtist = eCommerce_SDO_Core_Util_EntityManager::
			               ParseArrayToObject( $arrArtist, new eCommerce_Entity_Catalog_Artist() );
			// Add object to category array
			$objArtists[ $objArtist->getArtistId() ] = $objArtist;  
		}
		return $objArtists;
	}
	
	public static function Save( eCommerce_Entity_Catalog_Artist $artist ){
		
		// 1. Validate object
		$validator = new eCommerce_SDO_Core_Validator_Artist( $artist );
		$validator->validate();
		
		if ( !$validator->isValid() ){
			throw new eCommerce_SDO_Core_Validator_Exception( $validator ); 
		}
		
		// 2. Save or Update Object
		$entity = new ArrayObject( $artist );        // Convert Object to Array
		$dao = self::GetDAO();
		$dao->saveOrUpdate( $entity );
		
		// 3. Retrieve record from DB
		$artist = self::LoadById( $entity[ 'artist_id' ] );
		if ( $artist == new eCommerce_Entity_Catalog_Artist() ){
			throw new Exception( "The Artist saved could not be retrieved." );      
		}
		// 4. Return recently saved object
		return $artist;
		
	}
	
	public static function Delete( $artistId ){
		// 1. Ensure the artist actually exists
		$artist = self::LoadById( $artistId );
		
		if ( $artist == new eCommerce_Entity_Catalog_Artist() ){
			throw new eCommerce_SDO_Core_Application_Exception( 'The artist to be deleted doesn\'t exist.' );
		}
		else {
			try {
				// 2. Delete the Category
				$dao = self::GetDAO();
				$dao->delete( $artistId );
				
				// 3. Return the recently deleted Category
				return $artist;
			}
			catch ( SQLException $e ){
				throw new eCommerce_SDO_Core_Application_Exception( 'There was an unexpected error while trying to delete the Artist', 0, $e );
			}
		}
	}
	
	public static function GetAll(){
		$dao = self::GetDAO();
		$arrArtists = $dao->loadAll();                           // Load all categories as a 2D array
		$objArtists = self::ParseArrayToObjectArray( $arrArtists );
		return $objArtists;
	}
	
	public static function GetArtistTypes(){
		$ArrRegisters = array();
		
		$db = new eCommerce_SDO_Core_DB();
		
		//we create the instance here because eCommerce/SDO/Core
		//is restringed to access directly for the FrontEnd
		$daoProduct = self::GetDAO();
		
		$enumvalues = $db->sqlGetRecord( "SHOW COLUMNS FROM " . $daoProduct->getTable() . " LIKE 'type'" );
		
		//$enumvalues[ "type" ] contents text similar like 
		// enum( 'value1', 'value2' ... 'valueN' ) we need only the values
		// 'value1', 'value2' ... 'valueN'
		if(strpos($enumvalues[ "type" ],'enum(') > -1 ){
			$enumvalues = substr( $enumvalues[ "type" ], strlen('enum(')  , -1 );
			
			$enumvalues = explode(',', $enumvalues);
			
			foreach( $enumvalues as $enumvalue ){
				//$enumvalue contents text similar 
				// 'valueN' we need only valueN
				$ArrRegisters[] = substr( $enumvalue, 1, -1 );
				
			}
		}
		return $ArrRegisters;
	}


	function writeHTMLInfoArtist( eCommerce_Entity_Catalog_Artist $product, $position = false ){
	  ob_start();
	  	$html ='';
	  	if($position){
			$html .= '<div style="border:none; float:left; position:relative; padding-left:0px; padding-right:25px; padding-bottom:7px;">';
		}else{
			$html .= '<div style="border:none;padding-left:0px; float:none; position:inherit; padding-right:25px; padding-top:7px; padding-right:25px;">
			';
		}
		$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
		
	  ?>
		<!-- -->
		
		<table border="0" cellpadding="0" cellspacing="0" width="50%">
		  <tbody><tr>
			<td>
			<table border="0" cellpadding="0" cellspacing="0" width="392">
			<tbody><tr>
			<td valign="top">
			  <p style="margin-left: 15px">
			  <b><?php echo $product->getName(); ?></b><br><br>
			</td>
		  	</tr>
			  <tr>
				<td align="center" width="172">
				<p style="margin-left: 15px">
				<?php echo '<a href="artist.php?cmd=details&id=' . $product->getArtistId() .'">'; ?>
				<?php
				if($actualLanguage =='EN'){
				?>				
					<img src="imas/interiores/moreinfo.gif" alt="moreinfo" border="0" width="105" height="20" align="left">
				<?php
				}else{
				?>
					<img src="img/masinformacion.gif" alt="moreinfo" border="0" width="105" height="20" align="left">
				<?php
				}
				?>
				</a>
				</td>
				<td align="center" width="2"></td>
				<td width="133">
				<p align="right">
				<?php echo '<a href="product.php?idArt=' . $product->getArtistId() . '">'; ?>
				<font color="#A3A3A3"><?php
				if($actualLanguage =='EN'){
					echo 'Show Sculptures';
				}
				else{
					echo 'Mostrar Esculturas';
				}
				?></font>
				</a>
				</td>
				<td width="85">
				<p style="margin-left: 5px">
				<?php echo '<a href="product.php?idArt=' . $product->getArtistId() . '">'; ?>
				<img border="0" src="img/flechaboton.gif" width="23" height="20">
				</a>
				</td>
			  </tr>
			</tbody></table></td>
		  </tr>
		  
		</tbody></table>
		<!--  -->
		
	</div>
	  <?php
	   $html .= ob_get_contents();
	   ob_end_clean();
	   
	   return $html;
 }
}
?>