<?php
/**
 * This class provides access to manipulation of Users in the System.
 * 
 */	  
class eCommerce_SDO_Core_Application_Inventory extends eCommerce_SDO_Core_Application {
	
	public static function GetArrNamesAttributes(){
		$att = eCommerce_SDO_Core_DAO_Inventory::GetAllAttributes();
		$arrayNames = array();
		foreach($att as $a){
			switch($a){
				case 'color':
					$name = "Color";
				break;
				default:
				$name = $a;
				break;
			}
			$arrayNames[ $a ] = $name;
		}
		return $arrayNames;
		
	}
	
	public static function GetAttributesOfAProduct( $productId ){
		$attributes = eCommerce_SDO_Core_DAO_Inventory::GetAllAttributes();
		
		$arrReturn = array();
		foreach($attributes as $attribute){
			//$result = self::GetAttributeValuesGroupByProduct( $productId, $attribute );
			$result = self::LoadByProductGroupByAttribute($productId, $attribute);
			if(!empty($result) ){
				$arrReturn[ $attribute ] = $result;
			}
		}
		return $arrReturn;
	}
	
	public static function GetAttributeValuesGroupByProduct( $product_id, $attribute ){
		$arr = array();
		$values = self::LoadByProductGroupByAttribute($product_id, $attribute);
		debug($inventories);
	//	$function = 'get' . str_replace( ' ', '' , ucwords( str_replace( '_', ' ', $attribute ) ) );
		foreach( $values as $inventory){
		//	debug($inventory);
			//$arr[ $inventory->$function() ] = $inventory->$function();
		}
		return $arr;
	}
	
	/**
	 * Retrieves the appropriate DAO
	 *
	 * @return eCommerce_SDO_Core_DAO_Category
	 */
	protected static function GetDAO(){
		return new eCommerce_SDO_Core_DAO_Inventory();
	}
	protected function GetEntitySearchResultObject( $search ){
		return new eCommerce_Entity_Search_Result_Inventory( $search );
	}

	protected function GetEntityUtilObjectArray(){
		return new eCommerce_Entity_Util_InventoryArray();
	}
	
	protected function GetEntityObject(){
		return new eCommerce_Entity_Inventory();
	}
	
	protected function GetObjectId( $obj ){
		return $obj->getInventoryId();
	}
	
	public function GetSDOCoreValidatorObject( $profile ){
		return new eCommerce_SDO_Core_Validator_Inventory( $profile );
	}
	
//*****************************************************************************************
//*****************************************************************************************
//*****************************************************************************************

/**
	 * Saves a new or existing Profile into the System (validation is part of the process )
	 *
	 * @param eCommerce_Entity_User $category - The object to be saved
	 * @return eCommerce_Entity_User           - The recently saved object
	 * @throws eCommerce_SDO_Core_Validator_Exception, SQLException
	 */
	//public static function Save( eCommerce_Entity_Press $profile ){
	public static function Save( $profile ){
		// 1. Validate object
		//$validator = new eCommerce_SDO_Core_Validator_User( $profile );
		
		$validator = self::GetSDOCoreValidatorObject( $profile );
		
		$validator->validate();
		
		if ( !$validator->isValid() ){
			throw new eCommerce_SDO_Core_Validator_Exception( $validator ); 
		}
		
		// 2. Save or Update Object
		$entity = new ArrayObject( $profile );        // Convert Object to Array
		$dao = self::GetDAO();
		$dao->saveOrUpdate( $entity );
		
		// 3. Retrieve record from DB
		$profile = self::LoadById( $entity[ $dao->getIdField() ] );
		if ( $profile == self::GetEntityObject() ){
			throw new Exception( "El registro guardado no existe." );      
		}
		// 4. Return recently saved object
		return $profile;
		
	}
	/**
	 * Performs a paged search for Categories
	 *
	 * @param eCommerce_Entity_Search $search - The search parameters
	 * @param string $role                    - The role to filter by. Null for all user roles	
	 * @return eCommerce_Entity_Search_Result_Category
	 */
	public static function Search( eCommerce_Entity_Search $search, $extraConditions = null ){
		try {
			$dao = self::GetDAO();
			if ( !empty( $extraConditions ) && is_array($extraConditions) ){
			
					foreach( $extraConditions as $extraCondition){
						if( empty($extraCondition["SQL"]) && !empty( $extraCondition["value"] ) && !empty($extraCondition["columName"]) ){
							$condition = $extraCondition["columName"] . " = ";
							if( empty($extraCondition['isInteger']) || $extraCondition['isInteger'] === false){
								$condition .="'"; 
							}

							$condition .= $dao->getDB()->escapeString( $extraCondition['value'] );

							if( empty($extraCondition['isInteger']) || $extraCondition['isInteger'] === false){
								$condition .="'"; 
							}

							$dao->addExtraCondition( $condition );
						}
					  else {
						$dao->addExtraCondition( $extraCondition["SQL"] );
					  }	
					}
				}
			
			// Retrieve categories
			$arrProfiles = $dao->loadAllByParameters( $search->getKeywords(), $search->getSearchAsPhrase(),
			                                          $search->getOrderBy(), $search->getPage(),
			                                          $search->getResultsPerPage() );
			
			// Retrieve total pages
			$totalPages = $dao->loadAllByParameters( $search->getKeywords(), $search->getSearchAsPhrase(),
			                                         $search->getOrderBy(), $search->getPage(),
			                                         $search->getResultsPerPage(),
			                                         true );
			
			$result = self::GetEntitySearchResultObject( $search );

			
			$result->setTotalPages( $totalPages );
			$result->setResults( self::ParseArrayToObjectArray( $arrProfiles ) ); 
			return $result;
		}
		catch( SQLException $e ){
			throw new eCommerce_SDO_Core_Application_Exception( 
				'Error al procesar la b&uacute;squeda',
				0, $e );
		}
	}



	/**
	 * @param array $arrProfiles
	 * @return eCommerce_Entity_Util_ProfileArray
	 */
	protected static function ParseArrayToObjectArray( array $arrObjects ){
		// Create the array that will hold the Category objects
		$objProfiles = self::GetEntityUtilObjectArray();
		
		foreach( $arrObjects as $arrObject ){
			// Transform each array into object

			$obj = eCommerce_SDO_Core_Util_EntityManager::
			               ParseArrayToObject( $arrObject, self::GetEntityObject() );
			// Add object to category array  
			$objProfiles[ self::GetObjectId( $obj ) ] = $obj;  
		}
		return $objProfiles;
	}
	

	public static function UpdateInventory( $orderId ){
		$order    = eCommerce_SDO_Core_Application_Order::LoadById( $orderId );
		$items    = eCommerce_SDO_Core_Application_OrderItem::GetAllByOrder( $order->getOrderId(), $order->getLanguage() );
		
		foreach($items as $item){
			
			//$inventory = $item->getInventory();
			$inventory = self::LoadByProductAllAttributes( $item->getProductId(), $item->getArrAttributes() );
			$existence = $inventory->getNProducts();
			
			$inventoryId = $inventory->getInventoryId();
			if(!empty($inventory) && !empty($inventoryId)){
				if($item->getQuantity() <= $existence){
					$existence = $existence -$item->getQuantity();
				}else{
					$existence = 0;
				}
				
				$inventory->setNProducts( $existence );
				self::Save($inventory);
			}
		}
	}
	
	
	public static function UpdateInventoryAdd( $orderId ){
		$order    = eCommerce_SDO_Core_Application_Order::LoadById( $orderId );
		$items    = eCommerce_SDO_Core_Application_OrderItem::GetAllByOrder( $order->getOrderId(), $order->getLanguage() );
		
		foreach($items as $item){
			//$inventory = $item->getInventory();
			//$existence = $item->getExistence();
			
			$inventory = self::LoadByProductAllAttributes( $item->getProductId(), $item->getArrAttributes() );
			$existence = $inventory->getNProducts();
			
			$inventoryId = $inventory->getInventoryId();
			if(!empty($inventory) && !empty($inventoryId)){
				$existence = $existence + $item->getQuantity();
				$inventory->setNProducts( $existence );
				self::Save($inventory);
			}
		}
	}
	

	public static function LoadByProductGroupByAttribute( $productId, $attribute ){
		$dao = self::GetDAO();
		$arrEntities = $dao->loadByProductGroupByAttribute( $productId, $attribute );
		
		$arrEntities = self::ParseArrayToObjectArray( $arrEntities);  // Convert array to Object
		
		$arrAttributes = array();
		$function = 'get' . str_replace( ' ', '' , ucwords( str_replace( '_', ' ', $attribute ) ) );
		foreach( $arrEntities as $inventory){
			$val = $inventory->$function();
			if(!empty($val)){
				$arrAttributes[ $val ] = $val;
			}
		}
		return $arrAttributes;
	}

	
	
	public static function LoadByProductGroupByHeight( $productId){
		$dao = self::GetDAO();
		$arrEntities = $dao->loadByProductGroupByHeight( $productId);
		$arrEntities = self::ParseArrayToObjectArray( $arrEntities);  // Convert array to Object
		return $arrEntities;
	}
	
	public static function LoadByProductGroupByColors( $productId){
		$dao = self::GetDAO();
		$arrEntities = $dao->loadByProductGroupByColors( $productId);
		$arrEntities = self::ParseArrayToObjectArray( $arrEntities);  // Convert array to Object
		return $arrEntities;
	}
	
	public static function LoadByProduct( $productId){
		$dao = self::GetDAO();
		$entity = $dao->loadByProduct( $productId);
		$profile = self::GetEntityObject();   // Create new clean object to prevent old values being conserved
		$profile = eCommerce_SDO_Core_Util_EntityManager::
		            ParseArrayToObject( $entity, $profile );  // Convert array to Object
		return $profile;
	}
	
	public static function LoadByProductColorAndHeight( $productId, $color, $height ){
		$dao = self::GetDAO();
		$entity = $dao->loadByProductColorAndHeight( $productId, $color, $height );
		$profile = self::GetEntityObject();   // Create new clean object to prevent old values being conserved
		$profile = eCommerce_SDO_Core_Util_EntityManager::
		            ParseArrayToObject( $entity, $profile );  // Convert array to Object
		return $profile;
	}
	
	public static function LoadByProductAllAttributes( $productId, $attributes ){
		$dao = self::GetDAO();
		$entity = $dao->loadByProductAllAttributes( $productId, $attributes );
		$profile = self::GetEntityObject();   // Create new clean object to prevent old values being conserved
		$profile = eCommerce_SDO_Core_Util_EntityManager::
		            ParseArrayToObject( $entity, $profile );  // Convert array to Object
		return $profile;
	}
	
	
	/**
	 * Load the Profile from the given ID. Note: an empty object is returned if the ID doesn't exist
	 *
	 * @param int $profileId
	 * @return eCommerce_Entity_User
	 */
	public static function LoadById( $profileId ){
		
		$dao = self::GetDAO();
		$entity = $dao->loadById( $profileId, false );
 	
		$profile = self::GetEntityObject();   // Create new clean object to prevent old values being conserved
		$profile = eCommerce_SDO_Core_Util_EntityManager::
		            ParseArrayToObject( $entity, $profile );  // Convert array to Object
		return $profile;
	}
	
/**
	 * Deletes a Category from the System
	 *
	 * @param int $categoryId                    - The ID of the category to be deleted
	 * @return eCommerce_Entity_Catalog_Category - The recently deleted category
	 * @throws eCommerce_SDO_Core_Application_Exception - When the category is not found or couldn't be deleted
	 */
	public static function Delete( $profileId ){
		// 1. Ensure the category actually exists
		
		$profile = self::LoadById( $profileId );
		
		if ( $profile == self::GetEntityObject() ){
			throw new eCommerce_SDO_Core_Application_Exception( 'El registro que desea eliminar no existe.' );
		}
		else {
			try {
				// 2. Delete the Category
				$dao = self::GetDAO();
				$dao->delete( $profileId );
				
				// 3. Return the recently deleted Category
				return $profile;
			}
			catch ( SQLException $e ){
				throw new eCommerce_SDO_Core_Application_Exception( 'Ocurri&oacute; un error inesperado al intentar borrar el registro', 0, $e );
			}
		}
	}
//--------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------
	
	
	
	/**
	 * Load the Profile from the given email. Note: an empty object is returned if the email isn't found
	 *
	 * @param string $email
	 * @return eCommerce_Entity_User
	 */
	public static function LoadByEmail( $email ){
		
		$dao = self::GetDAO();
		$entity = $dao->loadByEmail( $email );
		if ( empty( $entity ) ){
			$entity = $dao->loadById( 0, false );       
		}
		$profile = new eCommerce_Entity_User();   // Create new clean object to prevent old values being conserved
		$profile = eCommerce_SDO_Core_Util_EntityManager::
		            ParseArrayToObject( $entity, $profile );  // Convert array to to Object
		return $profile;
	}

	
	
	

	/**
	 * Retrieves all the Categories available.
	 * 
	 * @return eCommerce_Entity_Util_CategoryArray
	 */
	public static function GetAll(){
		$dao = self::GetDAO();
		$arrProfiles = $dao->loadAll();                           // Load all categories as a 2D array
		$objProfiles = self::ParseArrayToObjectArray( $arrProfiles );
		return $objProfiles;
	}
	
	
	
/**
	 * Retrieves the list of Roles available to users
	 *
	 * @return array<string>
	 * @todo Retrieve the list from the actual DB (enum)
	 */
	public static function GetRoles(){
		$ArrRoles = array();
		
		$db = new eCommerce_SDO_Core_DB();
		
		//we create the instance here because eCommerce/SDO/Core
		//is restringed to access directly for the FrontEnd
		$daoUserProfile = self::GetDAO();
		
		$enumvalues = $db->sqlGetRecord( "SHOW COLUMNS FROM " . $daoUserProfile->getTable() . " LIKE 'role'" );
		
		//$enumvalues[ "type" ] contents text similar like 
		// enum( 'value1', 'value2' ... 'valueN' ) we need only the values
		// 'value1', 'value2' ... 'valueN'
		$enumvalues = substr( $enumvalues[ "type" ], strlen('enum(')  , -1 );
		
		$enumvalues = explode(',', $enumvalues);
		
		foreach( $enumvalues as $enumvalue ){
			//$enumvalue contents text similar 
			// 'valueN' we need only valueN
			$ArrRoles[] = substr( $enumvalue, 1, -1 );
			
		}
		return $ArrRoles;
	}
	
	
	
}
?>