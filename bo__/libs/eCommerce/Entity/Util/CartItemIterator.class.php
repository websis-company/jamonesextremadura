<?php
class eCommerce_Entity_Util_CartItemIterator extends ArrayIterator {
	
	/**
	 * Returns the current Product entity in the array
	 * @return eCommerce_Entity_Cart_Item
	 */
	public function current(){
		return parent::current();
	}
	
}
?>