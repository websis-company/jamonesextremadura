<?php
class eCommerce_Entity_Util_OrderItemIterator extends ArrayIterator {
	
	/**
	 * Returns the current Address entity in the array
	 * @return eCommerce_Entity_Order_Item
	 */
	public function current(){
		return parent::current();
	}
	
}
?>