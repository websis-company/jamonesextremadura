<?php
class eCommerce_Entity_Util_NoticiasArray extends ArrayObject {
	
	public function __construct( array $array = array() ){
		parent::__construct( $array, 0, "eCommerce_Entity_Util_NoticiasIterator" );
	}
	
	/**
	 * @return eCommerce_Entity_Util_UserIterator
	 */
	public function getIterator(){
		return parent::getIterator();
	}
	
}
?>