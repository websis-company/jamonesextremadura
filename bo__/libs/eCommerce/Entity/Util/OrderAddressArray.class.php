<?php
class eCommerce_Entity_Util_OrderAddressArray extends ArrayObject {
	
	public function __construct( array $array = array() ){
		parent::__construct( $array, 0, "eCommerce_Entity_Util_OrderAddressIterator" );
	}
	
	/**
	 * @return eCommerce_Entity_Util_OrderAddressIterator
	 */
	public function getIterator(){
		return parent::getIterator();
	}
	
}
?>