<?php
class eCommerce_Entity_Util_CartItemArray extends ArrayObject {
	
	public function __construct( array $array = array() ){
		parent::__construct( $array, 0, "eCommerce_Entity_Util_CartItemIterator" );
	}
	
	/**
	 * @return eCommerce_Entity_Util_CartItemIterator
	 */
	public function getIterator(){
		return parent::getIterator();
	}
	
}
?>