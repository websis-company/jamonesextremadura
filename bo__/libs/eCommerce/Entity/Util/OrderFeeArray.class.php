<?php
class eCommerce_Entity_Util_OrderFeeArray extends ArrayObject {
	
	public function __construct( array $array = array() ){
		parent::__construct( $array, 0, "eCommerce_Entity_Util_OrderFeeIterator" );
	}
	
	/**
	 * @return eCommerce_Entity_Util_OrderFeeIterator
	 */
	public function getIterator(){
		return parent::getIterator();
	}
	
}
?>