<?php
/**
 * This class represents a Currecy for sale eStore
 *
 * @author Alberto Perez <alberto@vicomstudio.com>
 * @package eCommerce::Entity
 */
class eCommerce_Entity_Country extends eCommerce_Entity {

	/**
	 * Currency ID
	 *
	 * @var int
	 */
	public $country_id;
	
	/**
	 * Currency name
	 *
	 * @var string
	 */
	public $name;

	
	function __construct(){
		parent::__construct();
	}

	/**
	 * @return string
	 */
	public function getCountryId(){
		return $this->country_id;
	}

	/**
	 * @param int $currencyId
	 */
	public function setCountryId( $countryId ){
		$this->country_id = $countryId;
	}

	
	/**
	 * @return string
	 */
	public function getName(){
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName( $name ){
		$this->name = $name;
	}

}
?>