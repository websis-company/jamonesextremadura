<?php
class eCommerce_Entity_Order_Address extends eCommerce_Entity_Util_Address {
	
	/**
	 * The ID of this Order Address
	 * 
	 * @var int
	 */
	public $order_address_id;
	
	/**
	 * Reference to the Order related to this address
	 *
	 * @var int
	 */
	public $order_id;

	/**
	 * The type of Address this is: Ship To, or Bill To. 
	 *
	 * @var string - Must not exceed 50 chars.
	 */
	public $type;
	
	public $first_name;
	public $last_name;
	public $last_name2;
	public $razon_social;
	public $rfc;
	public $email;
	public $confirm_email;
	public $contacto;
	public $municipio;
	
	public function getConfirmEmail(){
		return $this->confirm_email;
	}
	
	public function setConfirmEmail( $email ){
		return $this->confirm_email = $email;
	}

	public function getContacto(){
		return $this->contacto;
	}
	
	public function setContacto( $Contacto ){
		return $this->contacto = $Contacto;
	}
	
	public function getEmail(){
		return $this->email;
	}
	
	public function setEmail($email){
		return $this->email = $email;
	}
	
	public function getFirstName(){
		return $this->first_name;
	}
	
	public function setFirstName($first_name){
		return $this->first_name = $first_name;
	}
	
	public function getLastName(){
		return $this->last_name;
	}
	
	public function setLastName($last_name){
		return $this->last_name = $last_name;
	}
	
	public function getLastName2(){
		return $this->last_name2;
	}
	
	public function setLastName2($last_name){
		return $this->last_name2 = $last_name;
	}
	
	public function setRazonSocial( $rs){
		return $this->razon_social = $rs;
	}
	
	public function getRazonSocial(){
		return $this->razon_social;
	}
	
	public function getRfc(){
		return $this->rfc;
	}
	
	public function SetRfc($rfc){
		return $this->rfc = $rfc;
	}
	
	
	
	
	

	/**
	 * @return int
	 */
	public function getOrderAddressId(){
		return $this->order_address_id;
	}

	/**
	 * @param int $orderAddressId
	 */
	public function setOrderAddressId( $orderAddressId ){
		$this->order_address_id = $orderAddressId;
	}
	
	/**
	 * @return int
	 */
	public function getOrderId(){
		return $this->order_id;
	}

	/**
	 * @param int $order_id
	 */
	public function setOrderId( $orderId ){
		$this->order_id = $orderId;
	}

	/**
	 * @return string
	 */
	public function getType(){
		return $this->type;
	}

	/**
	 * @param string $type
	 */
	public function setType( $type ){
		$this->type = $type;
	}

	public function getMunicipio(){
		return $this->municipio;
	}

	public function setMunicipio($Municipio){
		$this->municipio =  $Municipio;
	}	


}
?>
