<?php
class eCommerce_Entity_Order_Item extends eCommerce_Entity {
	
	/**
	 * Order Item ID.
	 * 
	 * @var int
	 */
	public $order_item_id;
	/**
	 * @var mixed
	 */
	public $order_id;
	/**
	 * Product ID (only or reference purposes. Might not exist anymore)
	 * 
	 * @var int
	 */
	public $product_id;
	/**
	 * Product SKU 
	 * 
	 * @var string (255 chars)
	 */
	public $sku;
	/**
	 * Name of the product/item
	 * 
	 * @var string (255 chars)
	 */
	public $name;
	/**
	 * Price of the item
	 * 
	 * @var float
	 */
	public $price;
	
	/**
	 * Percentage of Discount (0-100%) 
	 * 
	 * @var float 
	 */
	public $discount;
	/**
	 * Number of products
	 * 
	 * @var int
	 */
	public $quantity;
	
	public $color;
	public $talla;
	
	public $attributes;
	public $version_id;
	public $galeria_id;
	public $file_id;
	
	public function __construct(){
		parent::__construct();
		
	}

	/**
	 * @return mixed
	 */
	public function getOrderId(){
		return $this->order_id;
	}

	/**
	 * @param mixed $orderId
	 */
	public function setOrderId( $orderId ){
		$this->order_id = $orderId;
	}

	/**
	 * Retrieves the Percentaje discount (0-100%)
	 * 
	 * @return float
	 */
	public function getDiscount(){
		return empty( $this->discount ) ? 0 : $this->discount;
	}

	/**
	 * @param float $discount
	 */
	public function setDiscount( $discount ){
		$this->discount = $discount;
	}

	/**
	 * @return string
	 */
	public function getName(){
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName( $name ){
		$this->name = $name;
	}

	/**
	 * @return int
	 */
	public function getOrderItemId(){
		return $this->order_item_id;
	}

	/**
	 * @param int $orderItemId
	 */
	public function setOrderItemId( $orderItemId ){
		$this->order_item_id = $orderItemId;
	}

	/**
	 * @return float
	 */
	public function getPrice(){
		return $this->price;
	}

	/**
	 * @param float $price
	 */
	public function setPrice( $price ){
		$this->price = $price;
	}

	/**
	 * @return int
	 */
	public function getProductId(){
		return $this->product_id;
	}

	/**
	 * @param int $product_id
	 */
	public function setProductId( $productId ){
		$this->product_id = $productId;
	}

	/**
	 * @return int
	 */
	public function getQuantity(){
		return $this->quantity;
	}

	/**
	 * @param int $quantity
	 */
	public function setQuantity( $quantity ){
		$this->quantity = $quantity;
	}

	/**
	 * @return string
	 */
	public function getSku(){
		return $this->sku;
	}

	/**
	 * @param string $sku
	 */
	public function setSku( $sku ){
		$this->sku = $sku;
	}

	/**
	 * Retrieves the total for this item taking into consideration the unitary price, 
	 * the quantity and the discount.
	 * 
	 * @return float
	 */
	public function getTotal(){
		// Calculate sub-total
		
		/*$subtotal = $this->getPrice() * $this->getQuantity();
		// Convert discount to the range 0-1 (ie. 25% = 0.25)
		$discount = $this->getDiscount() / 100;	// Empty is to prevent errors with NULL discounts
		// Apply discount to subtotal 
		$total = $subtotal *  ( 1 - $discount );
		*/
		$total = $this->getRealPrice() * $this->getQuantity();
		return $total;
	}
	public function getRealPrice(){
		$discount = $this->getDiscount() / 100;	// Empty is to prevent errors with NULL discounts
		$realPrice = $this->getPrice() *  ( 1 - $discount );
		return $realPrice; 
	}
	
	public function getExistence(){
		$inventory = $this->getInventory();
		return empty($inventory) ? 0 : $inventory->getNProducts();
	}
	public function getInventory(){
		return eCommerce_SDO_Inventory::LoadByProductAllAttributes( $productId, $this->attributes );
		//return eCommerce_SDO_Inventory::LoadByProductColorAndHeight($this->product_id, $this->color, $this->height);
	}
	
	public function getArrAttributes(){
		$attributes = explode(eCommerce_SDO_Core_Application_Cart::PRINCIPAL_TOKEN,$this->attributes);
		$attributesRet = array();
		foreach($attributes as $att ){
			$content = explode(eCommerce_SDO_Core_Application_OrderItem::ATTRIBUTE_VALUE_TOKEN, $att);
			$attName = $content[0];
			$attValue = $content[1];
			
			$attributesRet[ $attName ] = $attValue; 
		}
		return $attributesRet;
	}
	
	public function getAttributesToString(){
		$str ='';
		$att = $this->getArrAttributes();
		$ban = false;
		foreach($att as $attName => $value){
			if(!empty($value)){
				$str .= ($ban ? ', ' : '') . $attName . ':' . $value;
				$ban = true;
			} 
		}
		
		return $str;
	}
	
	public function getAttributes(){
		return $this->attributes;
	}

	/**
	 * @param string $sku
	 */
	public function setAttributes( $att ){
		$this->attributes = $att;
	}
	
	public function getColor(){
		return $this->color;
	}
	
	
	public function setColor( $color ){
		$this->color = $color;
	}
	
	public function getTalla(){
		return $this->talla;
	}
	
	
	public function setTalla( $talla ){
		$this->talla = $talla;
	}
	
	
	public function getVersionId(){
		return $this->version_id;
	}
	
	/**
	 * @param mixed $orderId
	 */
	public function setVersionId( $versionId ){
		$this->version_id = $versionId;
	}

	public function getGaleriaId(){
		return $this->galeria_id;
	}

	public function setGaleriaId($GaleriaId){
		$this->galeria_id = $GaleriaId;
	}

	public function getFileId(){
		return $this->file_id;
	}

	public function setFileId($FileId){
		$this->file_id = $FileId;
	}
	


}
?>