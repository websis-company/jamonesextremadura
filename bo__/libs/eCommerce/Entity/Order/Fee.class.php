<?php
class eCommerce_Entity_Order_Fee extends eCommerce_Entity {
	
	/**
	 * @var int
	 */
	public $order_fee_id;
	
	/**
	 * @var int
	 */
	public $order_id;
	
	/**
	 * @var string
	 */
	public $description;
	
	/**
	 * @var float
	 */
	public $amount;
	
	public function __construct(){
		parent::__construct();
	}

	/**
	 * @return float
	 */
	public function getAmount(){
		return $this->amount;
	}

	/**
	 * @param float $amount
	 */
	public function setAmount( $amount ){
		$this->amount = $amount;
	}

	/**
	 * @return string
	 */
	public function getDescription(){
		return $this->description;
	}

	/**
	 * @param string $description
	 */
	public function setDescription( $description ){
		$this->description = $description;
	}

	/**
	 * @return int
	 */
	public function getOrderFeeId(){
		return $this->order_fee_id;
	}

	/**
	 * @param int $orderFeeId
	 */
	public function setOrderFeeId( $orderFeeId ){
		$this->order_fee_id = $orderFeeId;
	}

	/**
	 * @return int
	 */
	public function getOrderId(){
		return $this->order_id;
	}

	/**
	 * @param int $orderId
	 */
	public function setOrderId( $orderId ){
		$this->order_id = $orderId;
	}
	
}
?>