<?php
class eCommerce_Entity_Galerycategory extends eCommerce_Entity {

	public function __construct(){
		parent::__construct();
	}

	public $galerycategory_id;
	public $parent_category_id;
	public $name;
	public $description;
	public $image_id;
	public $banner_id;
	public $orden;
	public $status;


	public function getGalerycategoryId(){ 
	return $this->galerycategory_id;
	}

	public function setGalerycategoryId( $GalerycategoryId){ 
	return $this->galerycategory_id = $GalerycategoryId;
	}

	public function getParentCategoryId(){ 
	return $this->parent_category_id;
	}

	public function setParentCategoryId( $ParentCategoryId){ 
	return $this->parent_category_id = $ParentCategoryId;
	}

	public function getName(){ 
	return $this->name;
	}

	public function setName( $Name){ 
	return $this->name = $Name;
	}

	public function getDescription(){ 
	return $this->description;
	}

	public function setDescription( $Description){ 
	return $this->description = $Description;
	}

	/*public function getImageId(){ 
	return $this->image_id;
	}

	public function setImageId( $ImageId){ 
	return $this->image_id = $ImageId;
	}*/

	public function getImageId( $pos = null){
		$ret = $arr = explode(',',$this->image_id);
		if( is_numeric($pos) ){
			$ret = $arr[ $pos ];
		}
		return $ret;
	}

	public function setImageId( $ImageId ){
		$this->image_id = $ImageId;
	}

	public function getUrlImageId($version,$pos){
		return $this->getUrlArrayFiles($this->image_id,$version,$pos);
	}


	public function getBannerId( $pos = null){
		/*$ret = $arr = explode(',',$this->banner_id);
		if( is_numeric($pos) ){
			$ret = $arr[ $pos ];
		}
		return $ret;*/
		return $this->banner_id;
	}

	public function setBannerId( $BannerId){ 
	return $this->banner_id = $BannerId;
	}

	public function getUrlBannerId($version,$pos){
		return $this->getUrlArrayFiles($this->banner_id,$version,$pos);
	}

	public function getOrden(){ 
	return $this->orden;
	}

	public function setOrden( $Orden){ 
	return $this->orden = $Orden;
	}

	public function getStatus(){ 
	return $this->status;
	}

	public function setStatus( $Status){ 
	return $this->status = $Status;
	}


	public function getUrlArrayFiles($array_files,$version,$pos = NULL){		
		$path = $version == 'original' ? 'bo' : 'web';		
		$ret = $arr = explode(',',$array_files);
		
		if(count($ret)>0 && !empty($ret[0])){
			$search = new eCommerce_Entity_Search('','',1,-1);			
			if( is_numeric($pos) ){
				$img = eCommerce_SDO_Core_Application_FileManagement::getFileByVersion($arr[ $pos ],$version);
				$pathTmp = $img->getPath();
				$filenameTmp = $img->getFilename();
				if(!empty($pathTmp) && !empty($filenameTmp)){
					$ret = ABS_HTTP_URL . "$path/{$img->getPath()}{$img->getFilename()}";
				}else{
					if(!empty($this->image_id)){
						$ret = ABS_HTTP_URL."bo/file.php?id=".$this->image_id."&type=image&img_size=predefined&height=300";
						
					}else{
						$ret = 	ABS_HTTP_URL."img/default.jpg";
					}
				}
			}else{
				$ret = array();
				foreach($arr as $p){
					$img = eCommerce_SDO_Core_Application_FileManagement::getFileByVersion($p,$version);
					$ret[] = ABS_HTTP_URL . "$path/{$img->getPath()}{$img->getFilename()}";
					
				}
			}	
					
			return $ret;
		}else{
			switch($version){
				case 'small':$size = 'width=512&height=384';break;
				case 'medium':$size = 'width=853&height=640';break;
				case 'large':$size = 'width=1024&height=768';break;
			}			
			//return ABS_HTTP_URL.PATH_ACTUAL_LANG . 'system/file.php?id=0&type=image&img_size=predefined&'.$size;			
			//return ABS_HTTP_URL . "bo/file.php?id=0&type=image&img_size=predefined&$size";
			return ABS_HTTP_URL . "img/default.jpg";
		}
	}

	public function getFriendlyName(){return Util_String::validStringForUrl( $this->name ) ;}
	public function getFriendlyNameUrl($p = 1, $menu = ''){return eCommerce_SDO_Core_Application_Galerycategory::getFriendlyNameUrl($this->galerycategory_id,$p,$menu);}


}
?>