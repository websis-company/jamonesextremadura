<?php

class eCommerce_Entity_Version extends eCommerce_Entity {



	public function __construct(){

		parent::__construct();

	}



public $version_id;

public $product_id;

public $name;

public $sku;

public $description;

public $details;

public $especificacion;

public $precio;

public $price;
public $base;
public $alto;
public $metro_cuadrado;
public $precio_metro;
public $bastidor;
public $metro_lineal;
public $costo_banner;
public $costo_telabanner;

public $discount;

public $currency;

public $medida;

public $status;

public $modelo;

public $orden;

public $array_images;

public $brand_id;

public $keywords;

public $productos_relacionados;

public $productos_detalles;

public $imagen;





public function getVersionId(){ 

return $this->version_id;

}



public function setVersionId( $VersionId){ 

return $this->version_id = $VersionId;

}



public function getProductId(){

	return $this->product_id;

}



public function setProductId( $ProductId){

	return $this->product_id = $ProductId;

}



public function getName(){

		return $this->name;

}



public function setName( $name ){

		$this->name = $name;

}



public function getSku(){ 

return $this->sku;

}



public function setSku( $Sku){ 

return $this->sku = $Sku;

}



public function getDescription(){

	return $this->description;

}



public function setDescription( $Description){

	return $this->description = $Description;

}



public function getDetails(){

	return $this->details;

}



public function setDetails( $Details){

	return $this->details = $Details;

}



public function getEspecificacion(){

	return $this->especificacion;

}



public function setEspecificacion( $Especificacion){

	return $this->especificacion = $Especificacion;

}





public function getPrice(){ 

return $this->price;

}



public function setPrice( $Price){ 

return $this->price = $Price;

}



public function getDiscount(){

	return empty( $this->discount ) ? 0 : $this->discount;

}



public function setDiscount( $discount ){

	$this->discount = $discount;

}

public function getPrecio(){

	//$precio_f = eCommerce_SDO_Core_Application_CurrencyManager::numberFormat($this->price);

	return $this->precio;

	//return $this->price;

}



/**

 * @param float $price

 */

public function setPrecio( $precio ){

	$this->precio = $precio;

}





public function getCurrency(){

	return $this->currency;

}



public function setCurrency( $currency ){

	$this->currency = $currency;

}



public function getMedida(){ 

return $this->medida;

}



public function setMedida( $Medida){ 

return $this->medida = $Medida;

}



public function getStatus(){

	return $this->status;

}



public function setStatus( $status ){

	$this->status = $status;

}



public function getModelo(){

	return $this->modelo;

}



public function setModelo( $modelo ){

	$this->modelo = $modelo;

}



public function getOrden(){

	return $this->orden;

}



public function setOrden( $orden ){

	$this->orden = $orden;

}



public function getRealPrice(){

	return $this->getPrice() * ( 1 - ( $this->getDiscount() / 100 ) );

}



public function getImagen($position = null){ 

$image = explode(',',$this->imagen);

return !is_numeric($position) ? $image : (!empty($image[$position]) ? $image[$position] : 0 );}



public function getHTMLImagen($width = null,$height= null, $nimage = 0, $target_blank = false, $urlEffect = false, $imgParam = 'border="0"'){

 $imgParam2 = empty($width) ? '' : ' width="'.$width.'"';

 $imgParam2 .= empty($height) ? '' : ' height="'.$height.'"';

 $src = $this->getImagen( $nimage );

 

 $effect = $urlEffect;

 if(empty($src)){

 	$src = 'ima/fotomiembro.jpg';

 	$imgParam .= $imgParam2;

 	$urlImage .= $src;

 	$title = '';

 }else{

 $file = eCommerce_SDO_Core_Application_FileManagement::LoadById($src);

 $title = $file->getDescription();

 $urlImage = BO_DIRECTORY . 'file.php?id=' . $src . '&type=image';

 	$src = BO_DIRECTORY . 'file.php?id=' . $src . '&type=image&img_size=predefined&width='.$width.'&height=' . $height;

 }

 $img = $target_blank ? '<a '. ($effect ? 'rel="jquery-lightbox"' : '' ) .' href="'.$urlImage.'" id="ImagePreviewId" target="_blank">' : '';

 $imgParam .= !$target_blank ? ' title="'.$title.'"' : '';

 $img .= "<img src='".$src."' ".$imgParam.">";

  $img .= ( $target_blank ? '</a>' : '');

 return $img;

 } 



public function setImagen( $Imagen){ 

return $this->imagen = $Imagen;

}



public function getUrlImageId($version,$pos){

	return $this->getUrlArrayFiles($this->imagen,$version,$pos);

}



public function getUrlImagesId($version,$pos){

	return $this->getUrlArrayFiles($this->array_images,$version,$pos);

}



public function getArrayImages( $pos = null){

	$ret = $arr = explode(',',$this->array_images);

	if( is_numeric($pos) ){

		$ret = $arr[ $pos ];

	}

	return $ret;

}



public function setArrayImages( $images ){

	$this->array_images = $images;

}





public function getUrlArrayFiles($array_files,$version,$pos = NULL){

	$path = $version == 'original' ? 'bo' : 'web';

	$ret = $arr = explode(',',$array_files);



	if(count($ret)>0 && !empty($ret[0])){

		$search = new eCommerce_Entity_Search('','',1,-1);

		if( is_numeric($pos) ){

			$img = eCommerce_SDO_Core_Application_FileManagement::getFileByVersion($arr[ $pos ],$version);

			$ret = ABS_HTTP_URL . "$path/{$img->getPath()}{$img->getFilename()}";

		}else{

			$ret = array();

			foreach($arr as $p){

				$img = eCommerce_SDO_Core_Application_FileManagement::getFileByVersion($p,$version);

				$ret[] = ABS_HTTP_URL . "$path/{$img->getPath()}{$img->getFilename()}";

					

			}

		}

		return $ret;

	}else{

		switch($version){

			case 'small':$size = 'width=160&height=235';break;

			case 'medium':$size = 'width=320&height=470';break;

			case 'large':$size = 'width=800&height=900';break;

		}

		//return ABS_HTTP_URL.PATH_ACTUAL_LANG . 'system/file.php?id=0&type=image&img_size=predefined&'.$size;

		//return ABS_HTTP_URL . "bo/file.php?id=0&type=image&img_size=predefined&$size";

		return ABS_HTTP_URL . "img/default.jpg";

	}

}



public function getBrandId() {

	return $this->brand_id;

}



public function setBrandId($brand_id) {

	$this->brand_id = $brand_id;

}



public function getKeywords() {

	return $this->keywords;

}

public function setKeywords($Keywords) {

	$this->keywords = $Keywords;

}



public function getProductosRelacionados() {

	return $this->productos_relacionados;

}

public function setProductosRelacionados($ProductosRelacionados) {

	$this->productos_relacionados = $ProductosRelacionados;

}



public function getProductosDetalles() {

	return $this->productos_detalles;

}

public function setProductosDetalles($ProductosDetalles) {

	$this->productos_detalles = $ProductosDetalles;

}


public function getBase(){
	return$this->base;
}

public function  setBase ($Base){
	return $this->base = $Base;
}

public function getAlto(){
	return$this->alto;
}

public function  setAlto ($Alto){
	return $this->alto = $Alto;
}
public function getMetroCuadrado(){
	return$this->metro_cuadrado;
}

public function  setMetroCuadrado ($MetroCuadrado){
	return $this->metro_cuadrado = $MetroCuadrado;
}

public function getPrecioMetro(){
	return$this->precio_metro;
}

public function  setPrecioMetro ($PrecioMetro){
	return $this->precio_metro = $PrecioMetro;
}
public function getBastidor(){
	return$this->bastidor;
}

public function  setBastidor ($Bastidor){
	return $this->bastidor = $Bastidor;
}

public function getMetroLineal(){
	return$this->metro_lineal;
}

public function  setMetroLineal ($MetroLineal){
	return $this->metro_lineal = $MetroLineal;
}

public function getCostoBanner(){
	return$this->costo_banner;
}

public function  setCostoBanner ($CostoBanner){
	return $this->costo_banner = $CostoBanner;
}

public function getCostoTelabanner(){
	return$this->costo_telabanner;
}

public function  setCostoTelabanner ($CostoTelabanner){
	return $this->costo_telabanner = $CostoTelabanner;
}
}

?>