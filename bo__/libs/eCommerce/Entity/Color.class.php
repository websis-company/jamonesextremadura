<?php
class eCommerce_Entity_Color extends eCommerce_Entity {

	public function __construct(){
		parent::__construct();
	}

public $color_id;
public $nombre;
public $imagen;

public function getColorId(){ 
return $this->color_id;
}

public function setColorId( $ColorId){ 
return $this->color_id = $ColorId;
}

public function getNombre(){ 
return $this->nombre;
}

public function setNombre( $Nombre){ 
return $this->nombre = $Nombre;
}

public function getImagen($position = null){
	$image = explode(',',$this->imagen);
	return !is_numeric($position) ? $image : (!empty($image[$position]) ? $image[$position] : 0 );}

	public function getHTMLImagen($width = null,$height= null, $nimage = 0, $target_blank = false, $urlEffect = false, $imgParam = 'border="0"'){
		$imgParam2 = empty($width) ? '' : ' width="'.$width.'"';
		$imgParam2 .= empty($height) ? '' : ' height="'.$height.'"';
		$src = $this->getImagen( $nimage );

		$effect = $urlEffect;
		if(empty($src)){
			$src = 'ima/fotomiembro.jpg';
			$imgParam .= $imgParam2;
			$urlImage .= $src;
			$title = '';
		}else{
			$file = eCommerce_SDO_Core_Application_FileManagement::LoadById($src);
			$title = $file->getDescription();
			$urlImage = BO_DIRECTORY . 'file.php?id=' . $src . '&type=image';
			$src = BO_DIRECTORY . 'file.php?id=' . $src . '&type=image&img_size=predefined&width='.$width.'&height=' . $height;
		}
		$img = $target_blank ? '<a '. ($effect ? 'rel="jquery-lightbox"' : '' ) .' href="'.$urlImage.'" id="ImagePreviewId" target="_blank">' : '';
		$imgParam .= !$target_blank ? ' title="'.$title.'"' : '';
		$img .= "<img src='".$src."' ".$imgParam.">";
		$img .= ( $target_blank ? '</a>' : '');
		return $img;
	}

	public function setImagen( $Imagen){
		return $this->imagen = $Imagen;
	}

	public function getUrlImageId($version,$pos){
		return $this->getUrlArrayFiles($this->imagen,$version,$pos);
	}
	public function getUrlArrayFiles($array_files,$version,$pos = NULL){
		$path = $version == 'original' ? 'bo' : 'web';
		$ret = $arr = explode(',',$array_files);
	
		if(count($ret)>0 && !empty($ret[0])){
			$search = new eCommerce_Entity_Search('','',1,-1);
			if( is_numeric($pos) ){
				$img = eCommerce_SDO_Core_Application_FileManagement::getFileByVersion($arr[ $pos ],$version);
				$ret = ABS_HTTP_URL . "$path/{$img->getPath()}{$img->getFilename()}";
			}else{
				$ret = array();
				foreach($arr as $p){
					$img = eCommerce_SDO_Core_Application_FileManagement::getFileByVersion($p,$version);
					$ret[] = ABS_HTTP_URL . "$path/{$img->getPath()}{$img->getFilename()}";
						
				}
			}
			return $ret;
		}else{
			switch($version){
				case 'small':$size = 'width=160&height=235';break;
				case 'medium':$size = 'width=320&height=470';break;
				case 'large':$size = 'width=800&height=900';break;
			}
			//return ABS_HTTP_URL.PATH_ACTUAL_LANG . 'system/file.php?id=0&type=image&img_size=predefined&'.$size;
			//return ABS_HTTP_URL . "bo/file.php?id=0&type=image&img_size=predefined&$size";
			return ABS_HTTP_URL . "img/default.jpg";
		}
	}

}
?>