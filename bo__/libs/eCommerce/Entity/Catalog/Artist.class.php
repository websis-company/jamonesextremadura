<?php
/**
 * This class represents a Category in the Catalog system.
 * 
  * @package eCommerce::Entity::Artist
 *
 */
class eCommerce_Entity_Catalog_Artist extends eCommerce_Entity {
	
	/**
	 * Category ID (unique)
	 *
	 * @var int
	 */
	public $artist_id;
	
	public $type;
	
	/**
	 * Artist name
	 *
	 * @var string
	 */
	public $name;
	
	/**
	 * Category description
	 *
	 * @var string
	 */
	public $email;
	
	public $url;
	
	public $image_id;
	
	public $biography_sp;
	public $biography_en;


public function getImageId(){ 
return $this->image_id;
}

public function setImageId( $ImageId){ 
return $this->image_id = $ImageId;
}

public function getBiographySp(){ 
return $this->biography_sp;
}

public function setBiographySp( $BiographySp){ 
return $this->biography_sp = $BiographySp;
}

public function getBiographyEn(){ 
return $this->biography_en;
}

public function setBiographyEn( $BiographyEn){ 
return $this->biography_en = $BiographyEn;
}

	public function __construct(){
		parent::__construct();
	}

	/**
	 * @return int
	 */
	public function getArtistId(){
		return $this->artist_id;
	}

	/**
	 * @param int $artist_id
	 */
	public function setArtistId( $artistId ){
		$this->artist_id = $artistId;
	}

	/**
	 * @return string
	 */
	public function getEmail(){
		return $this->email;
	}

	public function setEmail( $email ){
		$this->email = $email;
	}

	/**
	 * @return string
	 */
	public function getUrl(){
		return $this->url;
	}

	/**
	 * @param string $name
	 */
	public function setUrl( $url ){
		$this->url = $url;
	}
	
	public function getName(){
		return $this->name;
	}

	public function setName( $name ){
		$this->name = $name;
	}
	
	public function getType(){
		return $this->type;
	}

	public function setType( $type ){
		$this->type = $type;
	}
	
	
	public function getBiographyInActualLanguage(){
		$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
		$biography = '';
		
		$langSP = eCommerce_SDO_LanguageManager::GetValidLanguage('SP');
		
		switch($actualLanguage){
			case $langSP["id"]:
				$biography = $this->getBiographySp();
			break;
			default:
				$biography = $this->getBiographyEn();
			break;
		}
		return $biography;
		
	}
	
}
?>