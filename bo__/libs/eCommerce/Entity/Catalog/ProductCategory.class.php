<?php
class eCommerce_Entity_Catalog_ProductCategory extends eCommerce_Entity {
	
	/**
	 * @var int
	 */
	public $product_id;
	/**
	 * @var int
	 */
	public $category_id;
	
	/**
	 * @param int $productId
	 * @param int $categoryId
	 */
	public function __construct( $productId, $categoryId ){
		parent::__construct();
		$this->setProductId( $productId );
		$this->setCategoryId( $categoryId );
	}
	
	public function setProductId( $productId ){
		$this->product_id = $productId;
	}
	
	public function getProductId(){
		return $this->product_id;
	}
	
	public function setCategoryId( $categoryId ){
		return $this->category_id = $categoryId;
	}
	
	public function getCategoryId(){
		return $this->category_id;
	}
	
}
?>