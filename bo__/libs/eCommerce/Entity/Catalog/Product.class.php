<?php
/**
 * This class represents a product for sale eStore
 *
 * @author Alejandro Rivera <alejandro@vicomstudio.com>
 * @package eCommerce::Entity
 */
class eCommerce_Entity_Catalog_Product extends eCommerce_Entity {

	/**
	 * Product ID
	 *
	 * @var int
	 */
	public $product_id;
	/**
	 * Stock Keeping Unit code
	 *
	 * @var string
	 */
	public $sku;
	
	/**
	 * Product name
	 *
	 * @var string
	 */
	public $name;
	public $brand_name;
	/**
	 * Product status (such as available, discontinued, disabled, etc.)
	 *
	 * @var string
	 */
	public $status;
	/**
	 * Product price
	 *
	 * @var float
	 */
	public $price;
	/**
	 * Product price currency (USD, MXN, etc.)
	 *
	 * @var string
	 */
	public $currency;
	/**
	 * Product discount percentage (where 1 = 100% and 0.5 = 50%)
	 * 
	 * @var float
	 */
	public $discount;
	/**
	 * Weight of the product
	 *
	 * @var float
	 */
	public $weight;
	/**
	 * Product Image ID
	 *
	 * @var int
	 * @see eCommerce_Entity_Catalog_Image
	 */
	public $image_id;
	
	public $volume;
	
	/**
	 * Product short description
	 * @var string
	 */
	public $description_short;
	/**
	 * Product long description. Possible HTML
	 *
	 * @var string
	 */
	public $description_long;
	public $especificaciones;
	
	public $dimensions;
	
	public $finished;
	
	public $materials;
	
	public $colors; 
	
	public $array_images;
	public $brand_id;
	
	public $novedad;
	
	public $productos_relacionados;
	public $tipo;
	public $productos_detalles;

	public $keywords;
	public $precio;
	public $video;
	public $mas_vendido;
	public $fecha;
	public $orden;

	function __construct(){
		parent::__construct();
	}

	/**
	 * @return string
	 */
	public function getCurrency(){
		return $this->currency;
	}

	/**
	 * @param string $currency
	 */
	public function setCurrency( $currency ){
		$this->currency = $currency;
	}

	/**
	 * @return string
	 */
	public function getDescriptionLong(){		
		return $this->description_long;
	}
	
	public function getEspecificaciones(){		
		return $this->especificaciones;
	}

	/**
	 * @return int
	 */

public function getImageId( $pos = null){
		$ret = $arr = explode(',',$this->image_id);
		if( is_numeric($pos) ){
			$ret = $arr[ $pos ];
		}
		return $ret;
	}

	/**
	 * @param int $image_id
	 */
	public function setImageId( $image_id ){
		$this->image_id = $image_id;
	}
	
	/**
	 * @param string $description_long
	 */
	public function setDescriptionLong( $description_long ){
		$this->description_long = $description_long;
	}
	
	public function setEspecificaciones( $especificaciones ){
		$this->especificaciones = $especificaciones;
	}

	/**
	 * @return string
	 */
	public function getDescriptionShort( $length = 0, $atom = 'chars' ){
		return Util_String::subText($this->description_short, $length, $atom );
	}
	
	/**
	 * @param string $description_short
	 */
	public function setDescriptionShort( $description_short ){
		$this->description_short = $description_short;
	}

	/**
	 * Retrieves the Percentaje discount (0-100%)
	 * 
	 * @return float
	 */
	public function getDiscount(){
		return empty( $this->discount ) ? 0 : $this->discount;
	}

	/**
	 * @param float $discount
	 */
	public function setDiscount( $discount ){
		$this->discount = $discount;
	}

	/**
	 * @return string
	 */
	public function getName(){
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName( $name ){
		$this->name = $name;
	}

	/**
	 * @return float
	 */
	public function getPrice(){
		//$precio_f = eCommerce_SDO_Core_Application_CurrencyManager::numberFormat($this->price);
		return $this->price;				
		//return $this->price;
	}

	/**
	 * @param float $price
	 */
	public function setPrice( $price ){
		$this->price = $price;
	}

	/**
	 * @return int
	 */
	public function getProductId(){
		return $this->product_id;
	}

	/**
	 * @param int $productId
	 */
	public function setProductId( $productId ){
		$this->product_id = $productId;
	}

	/**
	 * @return string
	 */
	public function getSku(){
		return $this->sku;
	}

	/**
	 * @param string $sku
	 */
	public function setSku( $sku ){
		$this->sku = $sku;
	}

	/**
	 * @return string
	 */
	public function getStatus(){
		return $this->status;
	}

	/**
	 * @param string $status
	 */
	public function setStatus( $status ){
		$this->status = $status;
	}

	/**
	 * Retrieves the real price for the product (after applying the discount)
	 *
	 * @return float;
	 */
	public function getRealPrice(){
		return $this->getPrice() * ( 1 - ( $this->getDiscount() / 100 ) );
	}

	/**
	 * @return float
	 */
	public function getWeight(){
		return empty( $this->weight ) ? 0 : $this->weight;
	}

	/**
	 * @param float $weight
	 */
	public function setWeight( $weight ){
		$this->weight = $weight;
	}

	
	/**
	 * @return float
	 */
	public function getVolume(){
		return empty( $this->volume ) ? 0 : $this->volume;
	}

	/**
	 * @param float $weight
	 */
	public function setVolume( $volume ){
		$this->volume = $volume;
	}
	
	/**
	 * @return string
	 */
	public function getDimensions(){
		return $this->dimensions;
	}

	/**
	 * @param string $name
	 */
	public function setDimensions( $dimensions ){
		$this->dimensions = $dimensions;
	}
	
	/**
	 * @return string
	 */
	public function getFinished(){
		return $this->finished;
	}

	/**
	 * @param string $name
	 */
	public function setFinished( $finished ){
		$this->finished = $finished;
	}
	
	/**
	 * @return string
	 */
	public function getMaterials(){
		return $this->materials;
	}

	/**
	 * @param string $name
	 */
	public function setMaterials( $materials ){
		$this->materials = $materials;
	}
/**
	 * @return string
	 */
	public function getColors(){
		return $this->colors;
	}

	/**
	 * @param string $name
	 */
	public function setColors( $colors ){
		$this->colors = $colors;
	}
	
	public function getArrayImages( $pos = null){
		$ret = $arr = explode(',',$this->array_images);
		if( is_numeric($pos) ){
			$ret = $arr[ $pos ];
		}
		return $ret;
	}
	
	public function setArrayImages( $images ){
		$this->array_images = $images;
	}

public function getUrlImageId($version,$pos){
		return $this->getUrlArrayFiles($this->image_id,$version,$pos);
	}

	
public function getUrlImagesId($version,$pos){	
		return $this->getUrlArrayFiles($this->array_images,$version,$pos);
	}

public function getUrlArrayImages($version,$pos = NULL,$addImageId = false){
		$strIds = ($addImageId ? $this->image_id : '') . ($addImageId && $this->image_id && $this->array_images ? ',' : '') . $this->array_images;
		return $this->getUrlArrayFiles($strIds,$version,$pos);
	}
	
public function getUrlArrayFiles($array_files,$version,$pos = NULL){		
		$path = $version == 'original' ? 'bo' : 'web';		
		$ret = $arr = explode(',',$array_files);
		
		if(count($ret)>0 && !empty($ret[0])){
			$search = new eCommerce_Entity_Search('','',1,-1);			
			if( is_numeric($pos) ){
				$img = eCommerce_SDO_Core_Application_FileManagement::getFileByVersion($arr[ $pos ],$version);
				$ret = ABS_HTTP_URL . "$path/{$img->getPath()}{$img->getFilename()}";
			}else{
				$ret = array();
				foreach($arr as $p){
					$img = eCommerce_SDO_Core_Application_FileManagement::getFileByVersion($p,$version);
					$ret[] = ABS_HTTP_URL . "$path/{$img->getPath()}{$img->getFilename()}";
					
				}
			}			
			return $ret;
		}else{
			switch($version){
				case 'small':$size = 'width=160&height=235';break;
				case 'medium':$size = 'width=320&height=470';break;
				case 'large':$size = 'width=800&height=900';break;
			}			
			//return ABS_HTTP_URL.PATH_ACTUAL_LANG . 'system/file.php?id=0&type=image&img_size=predefined&'.$size;			
			//return ABS_HTTP_URL . "bo/file.php?id=0&type=image&img_size=predefined&$size";
			return ABS_HTTP_URL . "img/default.jpg";
		}
	}
	
	public function getHTMLImg($imgAlt, $width = '', $height= '', $nimage = 0, $imgParam='border="0"', $thickbox = false, $imgTitle = '', $defaultImagePath=''){
		$src = empty($nimage) ? $this->getImageId() : $this->getArrayImages( $nimage - 1 );
		if(empty($src)){
			$src = empty($defaultImagePath) ? ABS_HTTP_URL . BO_DIRECTORY . 'ima/fo/default.gif' : $defaultImagePath;
		}else{
			$src = ABS_HTTP_URL . BO_DIRECTORY . 'file.php?id=' . $src . '&type=image&img_size=predefined&width='.$width.'&height='.$height;
		}
		$html = '<img src="'.$src.'" '.$imgParam.' alt="'.$imgAlt.'" '.(empty($imgTitle) ? 'title="'.$imgTitle.'"' : '').'>';
		if($thickbox) {
			$html =
				'<a class="thickbox" href="'.ABS_HTTP_URL.BO_DIRECTORY.'file.php?id='.$this->image_id.'&type=image" title="'.$imgAlt.'">' .
				$html .
				'</a>';
		}
		return $html;
	}
			
	/**
	 * @return the $brand_id
	 */
	public function getBrandId() {
		return $this->brand_id;
	}

	/**
	 * @param $brand_id the $brand_id to set
	 */
	public function setBrandId($brand_id) {
		$this->brand_id = $brand_id;
	}

	/**
	 * @return the $brand_name
	 */
	public function getBrandName() {
		return $this->brand_name;
	}

	/**
	 * @param $brand_name the $brand_name to set
	 */
	public function setBrandName($brand_name) {
		$this->brand_name = $brand_name;
	}
	
public function getNovedad() {
		return $this->novedad;
	}

	/**
	 * @param $brand_id the $brand_id to set
	 */
	public function setNovedad($novedad) {
		$this->novedad = $novedad;
	}

	public function getProductosRelacionados() {
		return $this->productos_relacionados;
	}
	public function setProductosRelacionados($ProductosRelacionados) {
		$this->productos_relacionados = $ProductosRelacionados;
	}
	
	public function getTipo() {
		return $this->tipo;
	}
	public function setTipo($Tipo) {
		$this->tipo = $Tipo;
	}
	
	public function getProductosDetalles() {
		return $this->productos_detalles;
	}
	public function setProductosDetalles($ProductosDetalles) {
		$this->productos_detalles = $ProductosDetalles;
	}
	
	
public function getFriendlyName(){return Util_String::validStringForUrl( $this->name ) ;}
	public function getFriendlyNameUrl($idCat=false){return eCommerce_SDO_Core_Application_Product::getFriendlyNameUrl($this->product_id,$idCat);}

		public function getKeywords(){
		return $this->keywords;
	}

	public function setKeywords($Keywords){
		return $this->keywords = $Keywords;
	}

	public function getPrecio(){
		return $this->precio;
	}

	public function setPrecio($Precio){
		return $this->precio = $Precio;
	}
	
	public function getVideo(){
		return $this->video;
	}

	public function setVideo($Video){
		return $this->video = $Video;
	}
	
	public function getMasVendido(){
		return $this->mas_vendido;
	}

	public function setMasVendido($MasVendido){
		return $this->mas_vendido = $MasVendido;
	}
	
	public function getFecha(){
		return $this->fecha;
	}

	public function setFecha($Fecha){
		return $this->fecha = $Fecha;
	}

	public function getOrden(){
		return $this->orden;
	}

	public function setOrden($Orden){
		return $this->orden = $Orden;
	}

}



?>