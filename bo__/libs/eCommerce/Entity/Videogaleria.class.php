<?php
class eCommerce_Entity_Videogaleria extends eCommerce_Entity {

	public function __construct(){
		parent::__construct();
	}

public $videogaleria_id;
public $titulo;
public $descripcion;
public $imagen;
public $url;
public $fecha;


public function getVideogaleriaId(){ 
return $this->videogaleria_id;
}

public function setVideogaleriaId( $VideogaleriaId){ 
return $this->videogaleria_id = $VideogaleriaId;
}

public function getTitulo(){ 
return $this->titulo;
}

public function setTitulo( $Titulo){ 
return $this->titulo = $Titulo;
}

public function getDescripcion(){ 
return $this->descripcion;
}

public function setDescripcion( $Descripcion){ 
return $this->descripcion = $Descripcion;
}

public function getImagen($position = null){ 
$image = explode(',',$this->imagen);
return !is_numeric($position) ? $image : (!empty($image[$position]) ? $image[$position] : 0 );}

public function getHTMLImagen($width = null,$height= null, $nimage = 0, $target_blank = false, $urlEffect = false, $imgParam = 'border="0"'){
 $imgParam2 = empty($width) ? '' : ' width="'.$width.'"';
 $imgParam2 .= empty($height) ? '' : ' height="'.$height.'"';
 $src = $this->getImagen( $nimage );
 
 $effect = $urlEffect;
 if(empty($src)){
 	$src = 'ima/fotomiembro.jpg';
 	$imgParam .= $imgParam2;
 	$urlImage .= $src;
 	$title = '';
 }else{
 $file = eCommerce_SDO_Core_Application_FileManagement::LoadById($src);
 $title = $file->getDescription();
 $urlImage = BO_DIRECTORY . 'file.php?id=' . $src . '&type=image';
 	$src = BO_DIRECTORY . 'file.php?id=' . $src . '&type=image&img_size=predefined&width='.$width.'&height=' . $height;
 }
 $img = $target_blank ? '<a '. ($effect ? 'rel="jquery-lightbox"' : '' ) .' href="'.$urlImage.'" id="ImagePreviewId" target="_blank">' : '';
 $imgParam .= !$target_blank ? ' title="'.$title.'"' : '';
 $img .= "<img src='".$src."' ".$imgParam.">";
  $img .= ( $target_blank ? '</a>' : '');
 return $img;
 } 

public function setImagen( $Imagen){ 
return $this->imagen = $Imagen;
}

public function getUrl(){ 
return $this->url;
}

public function setUrl( $Url){ 
return $this->url = $Url;
}

public function getFecha(){ 
return $this->fecha;
}

public function setFecha( $Fecha){ 
return $this->fecha = $Fecha;
}

public function getUrlFoto($version,$pos){
	return $this->getUrlArrayFiles($this->imagen,$version,$pos);
}


public function getUrlArrayFiles($array_files,$version,$pos = NULL){
	$ret = $arr = explode(',',$array_files);
	if(count($ret)>0 && !empty($ret[0])){
		$search = new eCommerce_Entity_Search('','',1,-1);

		if( is_numeric($pos) ){			
			$img = eCommerce_SDO_Core_Application_FileManagement::getFileByVersion($arr[ $pos ],$version);
			$ret = ABS_HTTP_URL . "web/{$img->getPath()}{$img->getFilename()}";
		}else{
			$ret = array();
			foreach($arr as $p){
				$img = eCommerce_SDO_Core_Application_FileManagement::getFileByVersion($p,$version);
				$ret[] = ABS_HTTP_URL . "web/{$img->getPath()}{$img->getFilename()}";
			}
		}
		return $ret;
	}else{
		switch($version){
			case 'small':$size = 'width=150&height=100';break;
			case 'medium':$size = 'width=300&height=200';break;
			case 'large':$size = 'width=900&height=800';break;
		}
		//return ABS_HTTP_URL.PATH_ACTUAL_LANG . 'system/file.php?id=0&type=image&img_size=predefined&'.$size;
		return false;
	}
}

public function getFriendlyName(){return Util_String::validStringForUrl( $this->titulo ) ;}
public function getFriendlyNameUrl(){return eCommerce_SDO_Core_Application_Videogaleria::getFriendlyNameUrl($this->videogaleria_id);}

}
?>