<?php
class eCommerce_Entity_Search_Result_Artist extends eCommerce_Entity_Search_Result {

	public function __construct( eCommerce_Entity_Search $search ){
		parent::__construct( $search );
	}	
	/**
	 * @see eCommerce_Entity_Search_Result::getResults()
	 *
	 * @return eCommerce_Entity_Util_CategoryArray
	 */
	public function getResults(){
		return parent::getResults();
	}

	/**
	 * @see eCommerce_Entity_Search_Result::setResults()
	 *
	 * @param eCommerce_Entity_Util_CategoryArray $results
	 */
	public function setResults( eCommerce_Entity_Util_ArtistArray $results ){
		parent::setResults( $results );
	}

}
?>