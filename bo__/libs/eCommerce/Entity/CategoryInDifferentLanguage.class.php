<?php
class eCommerce_Entity_CategoryInDifferentLanguage extends eCommerce_Entity {

	public function __construct(){
		parent::__construct();
	}

public $category_in_different_language_id;
public $category_id;
public $language_id;
public $name;
public $description;


public function getCategoryInDifferentLanguageId(){ 
return $this->category_in_different_language_id;
}

public function setCategoryInDifferentLanguageId( $CategoryInDifferentLanguageId){ 
return $this->category_in_different_language_id = $CategoryInDifferentLanguageId;
}

public function getCategoryId(){ 
return $this->category_id;
}

public function setCategoryId( $CategoryId){ 
return $this->category_id = $CategoryId;
}

public function getLanguageId(){ 
return $this->language_id;
}

public function setLanguageId( $LanguageId){ 
return $this->language_id = $LanguageId;
}

public function getName(){ 
return $this->name;
}

public function setName( $Name){ 
return $this->name = $Name;
}

public function getDescription(){ 
return $this->description;
}

public function setDescription( $Description){ 
return $this->description = $Description;
}

}
?>