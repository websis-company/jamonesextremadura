<?php
class eCommerce_Entity_Banner extends eCommerce_Entity {

	public function __construct(){
		parent::__construct();
	}

public $banner_id;
public $titulo;
public $imagen;
public $imagen_movil;
public $url;
public $orden;
public $status;
public $link;

public function getBannerId(){ 
return $this->banner_id;
}

public function setBannerId( $BannerId){ 
return $this->banner_id = $BannerId;
}

public function getTitulo(){
	return $this->titulo;
}

public function setTitulo( $Titulo){
	return $this->titulo = $Titulo;
}

public function getImagen($position = null){ 
$image = explode(',',$this->imagen);
return !is_numeric($position) ? $image : (!empty($image[$position]) ? $image[$position] : 0 );}

public function getImagenMovil($position = null){ 
$image = explode(',',$this->imagen_movil);
return !is_numeric($position) ? $image : (!empty($image[$position]) ? $image[$position] : 0 );}

public function getHTMLArrayImages($width = null,$height= null, $nimage = 0, $target_blank = false, $urlEffect = false, $imgParam = 'border="0"'){
 $imgParam2 = empty($width) ? '' : ' width="'.$width.'"';
 $imgParam2 .= empty($height) ? '' : ' height="'.$height.'"';
 $src = $this->getImagen( $nimage );
 
 $effect = $urlEffect;
 if(empty($src)){
 	$src = 'ima/fotomiembro.jpg';
 	$imgParam .= $imgParam2;
 	$urlImage .= $src;
 	$title = '';
 }else{
 $file = ProjectLibrary_SDO_Core_Application_FileManagement::LoadById($src);
 $title = $file->getDescription();
 $urlImage = BO_DIRECTORY . 'file.php?id=' . $src . '&type=image';
 	$src = BO_DIRECTORY . 'file.php?id=' . $src . '&type=image&img_size=predefined&width='.$width.'&height=' . $height;
 }
 $img = $target_blank ? '<a '. ($effect ? 'rel="jquery-lightbox"' : '' ) .' href="'.$urlImage.'" id="ImagePreviewId" target="_blank">' : '';
 $imgParam .= !$target_blank ? ' title="'.$title.'"' : '';
 $img .= "<img src='".$src."' ".$imgParam.">";
  $img .= ( $target_blank ? '</a>' : '');
 return $img;
 } 

public function setImagen( $Imagen){ 
return $this->imagen = $Imagen;
}

public function setImagenMovil( $ImagenMovil){ 
return $this->imagen_movil = $ImagenMovil;
}

public function getUrlFotoHomeId($version,$pos){
	return $this->getUrlArrayFiles($this->tipohome,$version,$pos);	
}

public function getUrlFotoId($version,$pos){
	return $this->getUrlArrayFiles($this->imagen,$version,$pos);	
}

public function getUrlFotoMovilId($version,$pos){
	return $this->getUrlArrayFiles($this->imagen_movil,$version,$pos);	
}
public function getUrlArrayFiles($array_files,$version,$pos = NULL){
	$ret = $arr = explode(',',$array_files);
	if(count($ret)>0 && !empty($ret[0])){
		$search = new eCommerce_Entity_Search('','',1,-1);
		
		if( is_numeric($pos) ){
			$img = eCommerce_SDO_Core_Application_FileManagement::getFileByVersion($arr[ $pos ],$version);
			$ret = ABS_HTTP_URL . "web/{$img->getPath()}{$img->getFilename()}";
		}else{
			$ret = array();
			foreach($arr as $p){
				$img = eCommerce_SDO_Core_Application_FileManagement::getFileByVersion($p,$version);
				$ret[] = ABS_HTTP_URL . "web/{$img->getPath()}{$img->getFilename()}";
			}
		}
		return $ret;
	}else{
		switch($version){
			case 'large':$size = 'width=1920&height=812';break;
			case 'small':$size = 'width=100&height=70';break;
			case 'medium':$size = 'width=1439&height=800';break;
		}
		//return ABS_HTTP_URL.PATH_ACTUAL_LANG . 'system/file.php?id=0&type=image&img_size=predefined&'.$size;
		return false; 
	}
}

public function getUrl(){ 
return $this->url;
}

public function setUrl( $Url){ 
return $this->url = $Url;
}

public function getOrden(){ 
	return $this->orden;
}

public function setOrden( $Orden){ 
	return $this->orden = $Orden;
}

public function getStatus(){ 
	return $this->status;
}

public function setStatus( $Status){ 
	return $this->status = $Status;
}

public function getLink(){ 
	return $this->link;
}

public function setLink( $link){ 
	return $this->link = $link;
}

}
?>