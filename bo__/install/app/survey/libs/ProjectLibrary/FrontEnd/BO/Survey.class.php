<?php
class ProjectLibrary_FrontEnd_BO_Survey extends ProjectLibrary_FrontEnd_BO {

	/**
	 * @var ProjectLibrary_DAO_Event
	 */
	protected $search;

	public function __construct(){
		parent::__construct();
		
	}

	public function execute(){
		$app ='survey';
		$this->checkPermission( $app );
		
		$cmd = $this->getCommand();
		switch( $cmd ){
			case 'addResponse':
			case 'editSurveyResponse':
				$this->_editResponse();
				break;
			case 'saveResponse':
				$this->_saveResponse();
				break;
			case 'deleteResponse':
				$this->_deleteResponse();	
			break;
			case 'delete':
				$this->_delete();
				break;
			case 'add':
			$survey = new ProjectLibrary_Entity_Survey();
  			$survey->setDate( date("Y-m-d") );
			// Save type
				try{
					$survey->setQuestion('');
					$survey->setPosition(0);
					$surveyId = ProjectLibrary_SDO_Survey::SaverSurvey( $survey );
					$surveyId = $surveyId->getSurveyId();
				}
				catch( ProjectLibrary_SDO_Core_Validator_Exception $e){
					$this->tpl->assign( 'errors', new Validator_ErrorHandler( "Hay algunos errores", $e->getErrors() ) );
					//$oldCmd = $this->getParameter( 'old_cmd', false, null );
					$this->_list();
					break;
				}
				$_REQUEST["id"] = $surveyId;
			case 'edit':
				$this->_edit();
				break;
			case 'save':
				$this->_save();
				break;
			case 'list':
			default:
				$this->_list();
				break;
		}
	}

	protected function _list(){
		$this->search = new ProjectLibrary_Entity_Search(
			$this->getParameter( 'q', false, NULL ),
			$this->getParameter( 'o', false, 'survey_id DESC' ),
			$this->getParameter( 'p', true, 1 ),
			$this->getParameter( 'k', true, 10 )
		);
		$extraConditions = array();
		//$extraConditions[] = array( "columName"=>"role","value"=>$role,"isInteger"=>false);

		$result = ProjectLibrary_SDO_Survey::SearchSurvey( $this->search, $extraConditions );
		$exportToExcel = $this->getParameter( 'toExcel' );
		if ( $exportToExcel ){
		
			$users = $result->getResults();
			$data = array();
			foreach($users as $user){
				$data[] = get_object_vars( $user );
			}
			$this->exportToExcel( $data );

		}
		else {
			$this->tpl->assign( 'options', $result );
			$this->tpl->display(  "survey/list.php" );
		}
	}
	
	protected function _editResponse(){
		$id = $this->getParameter("surveyResponseId",true);

		$entity = ProjectLibrary_SDO_Survey::LoadSurveyResponse( $id );
		
		$this->tpl->assign( 'errors', new Validator_ErrorHandler() );
		
		if( empty($entity) ){
			$survey = new ProjectLibrary_Entity_Survey();
		}else{
			$survey = ProjectLibrary_SDO_Survey::LoadSurvey( $entity->getSurveyId() );
		}
		$this->tpl->assign( 'survey', $survey );
		
		$this->editResponse( $entity, $this->getCommand() );

	}
	
	public function editResponse( $entity, $cmd ){

		$this->tpl->assign( 'surveyResponse',     $entity );
		
		$surveyId = $this->getParameter('surveyId',true,0);
		$survey = ProjectLibrary_SDO_Survey::LoadSurvey( $surveyId );
		
		$surveyId = $this->tpl->survey->getSurveyId();
		$survey = empty( $surveyId ) ? $survey : $this->tpl->survey;
		
		$this->tpl->assign( 'survey', $survey );
		$this->tpl->assign( 'strSubtitles' , ( $cmd =='add' ? 'Agregar Respuesta a' : 'Editar Respuesta de' ) . ' la Encuesta "' . $survey->getQuestion() . '"' );
		$this->tpl->assign( 'strCmd'       , $this->getCommand() );

		$this->tpl->display( 'survey/editResponse.php' );
	}
	
	protected function _saveResponse(){
		$surveyId = $this->getParameter('surveyId',true,0);
		
		if ( !empty( $_REQUEST ) && isset( $_REQUEST['entity'] ) ){
	  		
			$entity = $_REQUEST['entity'];
  			
			$surveyResponse = new ProjectLibrary_Entity_SurveyResponse();

			$surveyResponse->setTotal( $entity['total'] );
			$surveyResponse->setSurveyId( $surveyId );
  			$surveyResponse->setSurveyResponseId( $entity['id'] );
  			$surveyResponse->setResponse( strip_tags($entity['response']) );
  			$surveyResponse->setOrderResponse( $entity['order_response'] );

			// Save type
			try{
				ProjectLibrary_SDO_Survey::SaverSurveyResponse( $surveyResponse );
				$this->tpl->assign( 'strSuccess', 'La respuesta fue guardada.' );
				$_REQUEST["id"] = $surveyId;
				$this->_edit();
			}
			catch( ProjectLibrary_SDO_Core_Validator_Exception $e){
				$this->tpl->assign( 'errors', new Validator_ErrorHandler( "Hay algunos errores", $e->getErrors() ) );
				$oldCmd = $this->getParameter( 'old_cmd', false, null );
				$this->edit( $user,  $oldCmd );
			}
		}
		else{
			throw new Exception('No hay suficiente informaci&oacute;n');
		}
	}
	
	protected function _deleteResponse(){
		$id = $this->getParameter();
			try{
				$user = ProjectLibrary_SDO_Survey::DeleteSurveyResponse($id );
				$this->tpl->assign( 'strSuccess', 'La respuesta "' . $user->getResponse() . '" ha sido eliminada' );
			}
			catch(ProjectLibrary_SDO_Core_Application_Exception $e) {
				$this->tpl->assign( 'strError', $e->getMessage() );
			}
		$surveyId = $this->getParameter('surveyId',true,0);
		$_REQUEST["id"] = $surveyId;
		$this->_edit();
		//$this->_list();
	}
	
	protected function _edit(){
		$id = $this->getParameter();
		
		$entity = ProjectLibrary_SDO_Survey::LoadSurvey( $id );
		$search = new ProjectLibrary_Entity_Search(
			$this->getParameter( 'q', false, '' ),
			$this->getParameter( 'o', false, 'order_response ASC' ),
			$this->getParameter( 'p', true, 1 ),
			$this->getParameter( 'k', true, -1 )
		);
		$extraConditions = array();

		$extraConditions[] = array( "columName"=>"survey_id","value"=>$entity->getSurveyId(),"isInteger"=>false);
		
		$result = ProjectLibrary_SDO_Survey::SearchSurveyResponse( $search, $extraConditions );
		
		$this->tpl->assign( 'resultResponses', $result );
		
		$this->tpl->assign( 'errors', new Validator_ErrorHandler() );
		
		$this->edit( $entity, $this->getCommand() );

	}

	public function edit( $entity, $cmd ){

		$this->tpl->assign( 'survey',     $entity );

		$this->tpl->assign( 'strSubtitles' , ( $cmd =='add' ? 'Agregar' : 'Editar' ) . ' Encuesta' );
		$this->tpl->assign( 'strCmd'       , $this->getCommand() );

		$this->tpl->display( 'survey/edit.php' );
	}

	protected function exportToExcel( $data ){

		$xls = new ExcelWriter( 'Events_' . date('Y-m-d'), 'Products' );
		$xls->writeHeadersAndDataFromArray( $data );


	}

	protected function _save(){
		
		if ( !empty( $_REQUEST ) && isset( $_REQUEST['entity'] ) ){
	  		
			$entity = $_REQUEST['entity'];
  			
			$survey = new ProjectLibrary_Entity_Survey();

  			$survey->setSurveyId( $entity['id'] );
  			$survey->setQuestion( strip_tags($entity['question']) );
  			$survey->setPosition( $entity['position'] );

  			$survey->setDate( date("Y-m-d H:i:s") );

			// Save type
			try{
				$actualPosition = $survey->getPosition();
				if( $actualPosition != 'hidden'){
					$result = ProjectLibrary_SDO_Survey::HideSurveyWithPosition( $actualPosition );
				}
				ProjectLibrary_SDO_Survey::SaverSurvey( $survey );

				$this->tpl->assign( 'strSuccess', 'La encuesta fue guardada.' );
				$_REQUEST['o'] ='';
				$this->_list();
			}
			catch( ProjectLibrary_SDO_Core_Validator_Exception $e){
				$this->tpl->assign( 'errors', new Validator_ErrorHandler( "Hay algunos errores", $e->getErrors() ) );
				$oldCmd = $this->getParameter( 'old_cmd', false, null );
				$this->edit( $user,  $oldCmd );
			}
		}
		else{
			throw new Exception('No hay suficiente informaci&oacute;n');
		}
	}

	protected function _delete(){
		$id = $this->getParameter();
			try{
				$user = ProjectLibrary_SDO_Survey::DeleteSurvey($id );
				$this->tpl->assign( 'strSuccess', 'La encuesta "' . $user->getQuestion() . '" ha sido eliminada' );
			}
			catch(ProjectLibrary_SDO_Core_Application_Exception $e) {
				$this->tpl->assign( 'strError', $e->getMessage() );
			}
		
		$this->_list();
	}

}
?>