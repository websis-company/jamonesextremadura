<?php 
$title_header = 'Env&iacute;o y Facturaci&oacute;n';
include_once "header.php";
$findUser = eCommerce_FrontEnd_Util_Session::Get('find_user');
if($findUser == 'true'){
	eCommerce_FrontEnd_Util_Session::Delete('find_user');
?>

<script type="text/javascript">
	$(document).ready(function(e){
		var div =  $("#alertUserFind").html();
		$.fancybox({
			padding	: 0,
			width	: '560px',
			height	: '204px',
			autoSize : false,
			closeBtn : false,
			helpers : { 'overlay' :{'closeClick': false,}},
			type : "iframe",
			scrolling: 'no',
			content: div,
		});	
	});
</script>
<?php

}//end if
$userProfile = eCommerce_FrontEnd_Util_UserSession::GetIdentity();

$userRole = $userProfile->getRole();
$userProfilePassword = $userProfile->getPassword();
$userProfileId       = $userProfile->getProfileId();
$userProfileEmail    = $userProfile->getEmail();
$userProfileName     = $userProfile->getFirstName();
$userProfileLastName = $userProfile->getLastName();
$userProfileAddress  = eCommerce_SDO_User::LoadUserAddress( $userProfileId );
$userPhone           = $userProfileAddress->getPhone();
$userCP              = $userProfileAddress->getZipCode();
$userStreet          = $userProfileAddress->getStreet();
$userExterior        = $userProfileAddress->getStreet3();
$userInterior        = $userProfileAddress->getStreet4();
$userColonia         = $userProfileAddress->getStreet2();
$userProfileConfirmado = $userProfile->getConfirmado();
$userEstado = $userProfileAddress->getState();
$userCiudad = $userProfileAddress->getCity();

if($userRole == eCommerce_SDO_User::ANONYMOUS_ROLE){
  $disablefields = '';  
  $cutString = ($_SESSION['Auth'] == 'true' || empty($userProfileName) || empty($userProfileLastName) || empty($userCP) || empty($userStreet))?false:true;
  $logOut = "";
}else{
  $disablefields = '';
  $cutString = ($_SESSION['Auth'] == 'true')?false:true;
  $logOut = "<strong><a href='".ABS_HTTP_URL."login.php?cmd=logout'>No soy yo. Cerrar sesión</a></strong>";
}
?>
<?php
$msgResponse  = $this->msgResponse;

$cart         = $this->cart;
$message      = $this->message;
$messageClass = $this->messageClass;
$isEmptyCart  = $this->isEmptyCart;
$billTo       = $cart->getOrderAddressByType( eCommerce_SDO_Cart::ADDRESS_BILL_TO );
$ship         = $this->ship;
$bill         = $this->bill;
$attr_ship    = 'errors'.eCommerce_SDO_Cart::ADDRESS_SHIP_TO;
$attr_bill    = 'errors'.eCommerce_SDO_Cart::ADDRESS_BILL_TO;
$errorsShip   = $this->$attr_ship;
$errorsBill   = $this->$attr_bill;
//$errorsBill =  $this->errors;
//$errorsShip 	= $this->errors;
//if($errors)$errorDescription = $errors->getDescription();
$requiere_fac    = $cart->getRequiereFactura();
$actualLanguage  = eCommerce_SDO_LanguageManager::GetActualLanguage();
$spanishLanguage = eCommerce_SDO_LanguageManager::GetValidLanguage('SP');
$spanishLanguage = $spanishLanguage["id"];
if ($actualLanguage == 'EN'){
	$lang = 0;
}
else{
	$lang = 1;
}
$edit_cart[0]    = "Edit Cart";
$edit_cart[1]    = "Editar Carrito";
$bill_address[0] = "Bill Address";
$bill_address[1] = "Direcci&oacute;n de Facturaci&oacute;n";
$street[0]       = "Street";
$street[1]       = "Calle";
$city[0]         = "City";
$city[1]         = "Ciudad";
$state[0]        = "State";
$state[1]        = "Estado";
$country[0]      = "Country";
$country[1]      = "Pa&iacute;s";
$zip[0]          = "Zip Code";
$zip[1]          = "C&oacute;digo Postal";
$continue[0]     = "Continue";
$continue[1]     = "Continuar";
$pur_1[0]        = "Billing address and Shipping address";
$pur_1[1]        = "Dirección de facturación y envío";

function ocultaCadena($string, $tipo){
  $arr = str_split($string);
  if($tipo == 'S'){
    $c = 0;
    foreach ($arr as $val) {
      if($c <= 2){
        $str[$c] = $val;
      }else{
        $str[$c] = '*';
      }
      $c++;
    }
    $cadena = implode('', $str);
  }elseif($tipo == 'N'){
    $length = count($arr);
    $corte  = $length - 4;
    $c = 1;
    foreach ($arr as $val) {
      if($c <= $corte){
        $str[$c] = '*';
      }else{
        $str[$c] = $val;
      }
      $c++;
    }
    $cadena = implode('', $str);
  }
  return $cadena;
}

?>
<section class="flujo-compra">
  <div class="container">
    <div class="row">
      <div id="sidebar" class="col-xs-3 col-lg-2">
        <?php include("accordion.php"); ?>
      </div>
      <div class="col-xs-9 col-lg-10">
      
        <!-- CARRITO-->
        <?php 
		
        if( !$isEmptyCart ){
			$costoEnvio = $_REQUEST['costoEnvio'];
          echo '<p class="cartTopInfo">' . eCommerce_SDO_Cart::GetHTML(true,false,true,$costoEnvio) . '</p>';
        }?>
        <article id="cartbottom">
          <div class="row">
          <!--ALERTA DE CONFIRMACION DE SOLICITUD-->
          <style>
			.alert{font-size:18px !important; font-weight:bold !important}
		  </style>
          <?php if(!empty($msgResponse)){?>
          <div class="col-xs-11">
				<?=$msgResponse?>
		  </div>
          <?php }?>
           <form method="POST" action="" name="formPayment" id="formPayment">
            <div class="col-xs-6">
              <div class="row">
               
                  <input type='hidden' name='nextStep' value='4' />
                  <input type='hidden' name='actualStep' value='5' />
                  <input type='hidden' name='cmd' value='saveStep' />
                  <input type="hidden" name="paymentMethod" id="paymentMethod" value="" />
                  <div class="col-xs-6">
                    <div class="box gray">
                      <h3 class="h3" style="color:#009fd0"><i class="spritecart identificacion"></i> Identificación</h3>
                      <p>Solicitamos únicamente la información esencial para la finalización de la compra.</p>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Correo:</label>
                        <?php if($cutString === true){?>
                        <label for="exampleInputEmail1"><strong><?=$userProfileEmail?></strong><?=$logOut?></label>
                        <?php }else{?>
                        <input type="email" class="form-control" placeholder="" name="email"  id="email" value="<?=$userProfileEmail?>" required="required" >
                        <?php }?>
                      </div>

                      <?php if($cutString === true){?> 
                      <div class="row">
                        <div class="col-xs-6" style="padding-right:5px; ">
                          <div class="form-group">
                            <div><label for="exampleInputEmail1">Nombre: </label></div>
                            <div><label><?php echo ocultaCadena($userProfileName,'S')?></label></div>
                            <input type="hidden" name="nombre"  id="nombre" value="<?=$userProfileName?>" >
                          </div>
                        </div>
                        <div class="col-xs-6" style="padding-left:5px; ">
                          <div class="form-group">
                            <div><label for="exampleInputEmail1">Apellido:</label></div>
                            <div><?php echo ocultaCadena($userProfileLastName,'S')?></div>
                            <input type="hidden" name="apellido"  id="apellido" value="<?=$userProfileLastName?>" >
                          </div>
                        </div>
                        <!--<div class="col-xs-6" style="padding-right:5px;">
                          <span class="text-danger">Este campo es obligatorio</span>
                        </div>-->
                        <div class="clearfix"></div>
                        <div class="col-xs-6" style="padding-left:15px; ">
                          <div class="form-group">
                            <div><label for="exampleInputEmail1">Teléfono/Móvil:</label></div>
                            <div><?php echo ocultaCadena($userPhone,'N')?></div>
                             <input type="hidden" name="ship[phone]"  id="phone" value="<?=$userPhone?>" >
                          </div>
                        </div>
                      </div>
                      <!--<a href="#" class="fancybox ">Editar</a>-->
                      <div onclick="<?=($userProfileConfirmado == 'No')?'showSetPassword();':'showFormLogin();'?>" class="btn btn-primary pull-right">Editar</div>

                      <?php }else{ ?>
                      <div class="row">
                        <div class="col-xs-6" style="padding-right:5px; ">
                          <div class="form-group">
                            <label for="exampleInputEmail1">Nombre:</label>
                            <input type="text" class="form-control" placeholder="" name="nombre"  id="nombre" value="<?=$userProfileName?>" required="required" <?=$disablefields?>>
                          </div>
                        </div>
                        <div class="col-xs-6" style="padding-left:5px; ">
                          <div class="form-group">
                            <label for="exampleInputEmail1">Apellido:</label>
                            <input type="text" class="form-control" placeholder="" name="apellido"  id="apellido" value="<?=$userProfileLastName?>" required="required" <?=$disablefields?>>
                          </div>
                        </div>
                        <div class="col-xs-6" style="padding-right:5px;">
                          <span class="text-danger">Este campo es obligatorio</span>
                        </div>
                        <div class="col-xs-6" style="padding-left:5px; ">
                          <div class="form-group">
                            <label for="exampleInputEmail1">Teléfono/Móvil:</label>
                            <input type="tel" pattern="^\d{3}-\d{2}-\d{2}$"  class="form-control" placeholder="" name="ship[phone]"  id="phone" value="<?=$userPhone?>" <?=$disablefields?> required="required" title="Formato XXX-XX-XX">
                          </div>
                        </div>
                      </div>
                      <?php } ?>
                    </div>
                  </div>

                  <div class="col-xs-6">
                    <div class="box gray">
                      <h3 class="h3" style="color:#F2097C;"><i class="spritecart datos"></i> Datos de envío</h3>
                   <?php if($cutString === true){?>
                      <div class="row">
                        <div class="col-xs-6">
                          <div class="form-group">
                            <label for="exampleInputEmail1">Código Postal:</label>
                             <div><label><?php echo ocultaCadena($userCP,'S')?></label></div> 
                              <input type="hidden" name="ship[zip_code]" id="zip_code" value="<?=$userCP?>">                           
                          </div> 
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Calle:</label>
                        <div><label><?php echo ocultaCadena($userStreet,'S')?></label></div> 
                         <input type="hidden" name="ship[street]"  id="street" value="<?=$userStreet?>">                       
                      </div>
                      <!--<p class="text-danger">Este valor es requerido</p>-->
                      <div class="row">
                        <div class="col-xs-6" style="padding-right:5px; ">
                          <div class="form-group">
                            <label for="exampleInputEmail1">No. Exterior:*</label>
                             <div><label><?php echo ocultaCadena($userExterior,'S')?></label></div>  
                              <input type="hidden" name="ship[street3]" id="exterior" value="<?=$userExterior?>" ></div>
                        </div>
                        <div class="col-xs-6" style="padding-left:5px; ">
                          <div class="form-group">
                            <label for="exampleInputEmail1">Número Interior:</label>
                            <div><label><?php echo ocultaCadena($userInterior,'S')?></label></div>   
                            <input type="hidden" name="ship[street4]" id="interior" value="<?=$userInterior?>" >                             
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Colonia:*</label>
                        <div><label><?php echo ocultaCadena($userColonia,'S')?></label></div>  
                        <input type="hidden" name="ship[street2]" id="colonia" value="<?=$userColonia?>">                             
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Estado:*</label>
                        <div><label><?php echo ocultaCadena($userEstado,'S')?></label></div>  
                        <input type="hidden" name="ship[state]" id="state" value="<?=$userEstado?>">                             
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Ciudad:*</label>
                        <div><label><?php echo ocultaCadena($userCiudad,'S')?></label></div>  
                        <input type="hidden" name="ship[city]" id="ciudad" value="<?=$userCiudad?>">                             
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nombre de la persona que va a recibir:*</label>
                        <div><label><?php echo ocultaCadena($recibe,'S')?></label></div> 
                        <input type="hidden" name="recibe"  id="recibe" value="<?=$recibe?>" >                         
                      </div>
                      <div onclick="<?=($userProfileConfirmado == 'No')?'showSetPassword();':'showFormLogin();'?>" class="btn btn-primary pull-right">Editar</div>
                   <?php }else{?>
                   	  <div class="row">
                        <div class="col-xs-6">
                          <div class="form-group">
                            <label for="exampleInputEmail1">Código Postal:</label>
                            <input type="text" class="form-control" placeholder="" name="ship[zip_code]"  id="zip_code" value="<?=$userCP?>" required="required" <?=$disablefields?>>
                          </div> 
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Calle:</label>
                        <input type="text" class="form-control" placeholder="" name="ship[street]"  id="street" value="<?=$userStreet?>" required="required" <?=$disablefields?>>
                      </div>
                     <!-- <p class="text-danger">Este valor es requerido</p>-->
                      <div class="row">
                        <div class="col-xs-6" style="padding-right:5px; ">
                          <div class="form-group">
                            <label for="exampleInputEmail1">No. Exterior:*</label>
                            <input type="text" class="form-control" placeholder="" name="ship[street3]"  id="exterior" value="<?=$userExterior?>" required="required" <?=$disablefields?>>
                          </div>
                        </div>
                        <div class="col-xs-6" style="padding-left:5px; ">
                          <div class="form-group">
                            <label for="exampleInputEmail1">Número Interior:</label>
                            <input type="text" class="form-control" placeholder="" name="ship[street4]"  id="interior" value="<?=$userInterior?>" <?=$disablefields?>>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Colonia:*</label>
                        <input type="text" class="form-control" placeholder="" name="ship[street2]"  id="colonia" value="<?=$userColonia?>" required="required" <?=$disablefields?>>
                      </div>
                       <div class="form-group">
                          <label for="exampleInputEmail1">Estado:</label>
                          <? $states = eCommerce_SDO_CountryManager::GetStatesOfCountry( 'MX', true, 'ship[state]', $ship->getState(),'shipstate','form-control', 'style="height:30px"', true, '' );
						echo ( !is_array($states)) ? $states : $states["div"];
						$errorsShip->getHtmlError("state"); ?>                          
                        </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Ciudad:</label>
                          <div id="shipCity"></div>
                          <!--<input type="text" class="form-control" placeholder="" name="ship[city]"  id="shipcity" value="<? //=$userCiudad?>" >-->
                        </div>                       
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nombre de la persona que va a recibir:*</label>
                        <input type="text" class="form-control" placeholder="" name="recibe"  id="recibe" value="<?=$recibe?>" required="required" <?=$disablefields?>>
                      </div>
                   <?php }?>
                    <?php /*if($cutString === false){?>
                      <!--<div class="checkbox2">
                        <div class="formcontrol active">
                          <label>¿Deseas factura? <span class="spritecheckboxs checkbox-status" style="background:none; width:60px; height:25px; vertical-align:top">Si<input type="radio" name="factura" id="facturasi" value="Si" style="margin-left:3px; vertical-align:bottom"/>No<input type="radio" name="factura" id="facturano" value="No" style="margin-left:3px; vertical-align:bottom" checked="checked"/></span></label>
                          
                        </div>
                      </div>
                      <br />                     
                      <div id="facturacion" style="display:none">
                        <h3 class="h3" style="color:#F2097C;"><i class="spritecart datos"></i> Datos de facturación</h3>
                        <div class="form-group">
                          <label for="exampleInputEmail1">Nombre o Raz&oacute;n Social:*</label>
                          <input type="text" class="form-control" placeholder="" name="bill[razon_social]"  id="razon_social" value="" >
                        </div>
                        <div class="form-group">
                          <label for="exampleInputEmail1">RFC:*</label>
                          <input type="text" class="form-control" placeholder="" name="bill[rfc]"  id="rfc" value="" >
                        </div>
                        <div class="form-group">
                          <label for="exampleInputEmail1">Calle:</label>
                          <input type="text" class="form-control" placeholder="" name="bill[street]"  id="billstreet" value="" required="required" >
                        </div>
                        <div class="form-group">
                          <label for="exampleInputEmail1">N&uacute;mero:</label>
                          <input type="text" class="form-control" placeholder="" name="bill[street3]"  id="billstreet3" value="" >
                        </div>
                        <div class="form-group">
                          <label for="exampleInputEmail1">Colonia:</label>
                          <input type="text" class="form-control" placeholder="" name="bill[street2]"  id="billstreet2" value="" >
                        </div>
                        <div class="form-group">
                          <label for="exampleInputEmail1">C&oacute;digo Postal:</label>
                          <input type="text" class="form-control" placeholder="" name="bill[zip_code]"  id="billzip_code" value="" >
                        </div>
                        <div class="form-group">
                          <label for="exampleInputEmail1">Estado:</label>
                          <? $states = eCommerce_SDO_CountryManager::GetStatesOfCountry( 'MX', true, 'bill[state]', $bill->getState(),'billstate','form-control', 'style="height:30px"', true, '' );
						echo ( !is_array($states)) ? $states : $states["div"];
						$errorsBill->getHtmlError("state"); ?> 
                         
                        </div>
                        <div class="form-group">
                          <label for="exampleInputEmail1">Ciudad:</label>
                          <div id="billCity"></div>
                          
                        </div>                        
                        <div class="form-group">
                          <label for="exampleInputEmail1">Tel&eacute;fono:</label>
                          <input type="text" class="form-control" placeholder="" name="bill[phone]"  id="billphone" value="" >
                        </div>
                      </div>
                      <label>Si requieres factura y no cuentas<br /> con los datos de facturación a la <br />mano, puedes seleccionar la <br />opción no y posteriormente<br /> puedes solicitar tu factura a <a href="mailto:tiendaenlinea@fiestaslaconcordia.com">tiendaenlinea@<br />fiestaslaconcordia.com</a></label> -->
	               	<?php } */?>
      <!--<div class="checkbox2">
      <div class="formcontrol active">
      <label>RECOGER EN TIENDA-Gratis-Hasta 3 días hábiles <span class="spritecheckboxs checkbox-status"></span></label>
      </div>
      <div class="formcontrol" style="border:0;">
      <label>SERVICIO TERRESTRE-ESTAFETA-$ 100.00-Hasta 8 días hábiles <span class="spritecheckboxs checkbox-status"></span></label>
      </div>
      </div>-->
      
     

	</div>
    </div>
        
    </div>

</div>

			<div class="col-xs-6">

    <div class="box gray">
    <h3 class="h3"><i class="spritecart pago"></i> Pago</h3>
    <div class="row">
    <div class="col-xs-4">
    <ul class="nav">
    <li id="deposito"><a  style="cursor:pointer">Depósito en Efectivo</a></li>
    <li id="transferencia"><a  style="cursor:pointer">Transferencia</a></li>
    <li id="oxxo" ><a style="cursor:pointer">Oxxo</a></li>
    <li id="paypal"><a  style="cursor:pointer">Paypal <i class="spritecart paypal pull-right" style="margin-top:-4px;"></i></a></li>
    <!--<li><a href="#">Mercado pago <i class="spritecart mercadopago pull-right" style="margin-top:-4px;"></i></a></li>-->
    </ul>
    </div>
    <div class="col-xs-8">
    <p class="text-right" style="color:#F2097C; display:block">Si requiere factura, tendrá la oportunidad de<br>
		solicitarla en el siguiente paso</p>
	<div class="box checkbox2" style="padding:10px 20px; display:none" id="depositoTxt">
    	<p><b>Depósito en Efectivo</b></p>
        <div class="row listado-marcas">        
        <div class="col-xs-12" align="center">
        	<img src="img/banbajio.png" style="width:50%">
        </div>
        </div>
        <ul class="listado-legales" style="font-size:13px">
        <li>BBVA Bancomer</li>
        <li>Cuenta: <span style="font-size:14px">0104341937</span></li>
        <li>Nombre: <span style="font-size:14px">Estamos de Fiesta SA de CV</span></li>
        </ul>        
    </div>
    <div class="box checkbox2" style="padding:10px 20px; display:none" id="transferenciaTxt">
    	<p><b>Transferencia Bancaria</b></p>
        <div class="row listado-marcas">
        <div class="col-xs-12">
        	<ul class="listado-legales" style="font-size:13px">
        <li>Banco: BBVA Bancomer</li>
<li>CLABE: 012650001043419370</li>
<li>Nombre: Estamos de Fiesta SA de CV</li>
<li>Email para confirmación: <a href="mailto:tiendaenlinea@fiestaslaconcordia.com">tiendaenlinea@fiestaslaconcordia.com</a></li>
       
        </ul>
        </div>
        
        </div>
        
        
    </div>
    <div class="box checkbox2" style="padding:10px 20px; display:none" id="oxxoTxt">
    	<p><b>Pago en Oxxo: Depósito bancario</b></p>
        <div class="row listado-marcas">        
        <div class="col-xs-12" align="center">
        	<img src="img/oxxo.png" style="width:50%">
        </div>
        </div>
        <ul class="listado-legales" style="font-size:13px">
        <li>Banco: BBVA Bancomer</li>
        <li>No. de tarjeta: 4152 3131 3916 3575</li>        
        </ul>
        
        
    </div>
    <div class="box checkbox2" style="padding:10px 20px; display:none" id="paypalTxt">
    	<p>El sistema te redireccionará automáticamente con Paypal para terminar tu compra.</p>
        <div class="row listado-marcas">
        <div class="col-xs-6">
        	<img src="img/pago-paypal.png" style="width:250px">
        </div>
        
        </div>
        
        
    </div>
    <!--<a class="btn btn-pink pull-right" id="comprar" style="display:none"><i class="glyphicon glyphicon-lock"></i> Comprar ahora</a>-->
    <input type="submit" class="btn btn-pink pull-right" id="comprar" style="display:none" value="Comprar ahora" name="comprar"/>
     
    </div>
    <div class="col-xs-12" style="display:none" id="terminostxt">
    <input type="checkbox" name="terminos" id="terminos" value="leido"/>&nbsp;He leído <a href="<?=ABS_HTTP_URL?>aviso-privacidad.php" target="_blank" style="font-weight:bold">Aviso de Privacidad</a> , <a href="<?=ABS_HTTP_URL?>politicas.php" target="_blank"  style="font-weight:bold">Política de envíos</a> y <a href="<?=ABS_HTTP_URL?>terminos.php" target="_blank"  style="font-weight:bold">Términos y Condiciones</a>
    </div>
    <div id="errorTerminos" class="col-xs-12"></div>
    </div>
    </div>
</div>
			</form>
</div>
</article>
</div>

</div>
</div>
</section>

<?php include("toolbar-right.php"); ?>
<?php include("footer.php"); ?>
<div id="alertUserFind" style="display: none;">
	<div style="background-color: #0186C3; height: 100%;">
		<div style="float: left; padding-top: 35px;">
			<img alt="access" src="<?php echo ABS_HTTP_URL?>img/layout/ICONO-CANDADO.png" width="125" height="125" style="width: 125px; height: 125px;"/>
		</div>
		<div style="float: left; padding: 25px; width:400px; color: #FFF;">
			<span style="font-size: 30px; font-weight: normal;">Hola !</span><br/><br/>
			<span style="font-size: 15px; font-weight: normal;  line-height: 1.5;">
				El correo que introdujo ya se encuentra registrado, se completará el formulario automáticamente con sus datos.
			</span>
			<div class="clearfix"></div>
			<div style="font-size: 15px; font-weight: normal; padding-top: 10px;">
				<button style="color: #000;" class="btn btn-default" onclick="closeFancy();">Continuar con la compra</button>
			</div>
		</div>
	</div>
</div>

<div id="formLogin" style="display:none">
  <div class="container">
    <div class="row">
      <div class="col-xs-4">
        <br>
        <p><i class="spritecart identificacion"></i><?php echo $userProfileEmail;?></p>
        <p>Ingresar contraseña</p>
        <hr>
        <p>
          <form id="formLogin" name="formLogin" method="post" action="">
          <input type="hidden" name="cmdLogin" value="loginForm" />
            <div class="pull-left">
              <label for="password">Contraseña :</label>
            </div>
            <div class="pull-right">
              <a style="cursor:pointer" onclick="showFormPassword()">Olvid&eacute; mi contrase&ntilde;a</a>
            </div>
            <div class="col-xs-12">
              <input type="password" id="passwordLogin" name="passwordLogin" class="col-xs-12 form-control" required="required">
            </div>
            <!--<div class="clearfix"></div>
            <div class="pull-right">
              <p><a style="cursor:pointer" onclick="showSetPassword()">No sabe o no recuerda cuál? Registrala ahora</a></p>
            </div>
            -->
            <div class="clearfix"></div>
            <hr>
            <div class="pull-right">
              <button id="btnLogin" type="submit" class="btn btn-primary" >Ingresar</button>
            </div>
          </form>
        </p>
      </div>  
    </div>
  </div>
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>-->
  <script type="text/javascript" src="<?=ABS_HTTP_URL?>js/validate/jquery.validate.js"></script>
  <script type="text/javascript">
  $(document).ready(function(){
      $("#formLogin").validate({
          debug: false,
          rules:{
              passwordLogin:{
                  required: true,
              },
          },
          messages:{
              passwordLogin:{
                  required: 'Contraseña es requerida',
              },
          },
        submitHandler: function(form){         
        },
      });
	   
	  $("#shipstate").change(function(){
			var estado = $("#shipstate").val();
			msgdataShip = $("#shipCity");
			if(estado != ''){
				$.ajax({
					type: "POST",
					data: "estado="+$("#shipstate").val()+"&tipo=ship",
					url: "<?=ABS_HTTP_URL?>ciudades.php?estado="+$("#shipstate").val()+"tipo=ship",
					beforeSend: function(){								
						msgdataShip.html("<img src='<?=ABS_HTTP_URL?>img/layout/loading.gif' alt='Cargando' title='Cargando' width='30' />");
					},
					success: function(result){					 
						 msgdataShip.html(result);						 
					}			
				});
				return false;
			}
		});
		$("#billstate").change(function(){
			var estado = $("#billstate").val();
			msgdataBill = $("#billCity");
			if(estado != ''){
				$.ajax({
					type: "POST",
					data: "estado="+$("#billstate").val()+"&tipo=bill",
					url: "<?=ABS_HTTP_URL?>ciudades.php?estado="+$("#billstate").val()+"tipo=bill",
					beforeSend: function(){								
						msgdataBill.html("<img src='<?=ABS_HTTP_URL?>img/layout/loading.gif' alt='Cargando' title='Cargando' width='30' />");
					},
					success: function(result){					 
						 msgdataBill.html(result);						 
					}			
				});
				return false;
			}
		});
  });
	
 function showSetPassword(){
    var div =  $("#formSetPassword").html();
    $.fancybox({
      padding : 0,
      width : '400px',
      height  : '350px',
      autoSize : false,
      closeBtn : true,
      helpers : { 'overlay' :{'closeClick': false,}},
      type : "iframe",
      scrolling: 'no',
      content: div,
    });     
  }

  function showFormLogin(){
    var div =  $("#formLogin").html();
    $.fancybox({
      padding : 0,
      width : '400px',
      height  : '314px',
      autoSize : false,
      closeBtn : true,
      helpers : { 'overlay' :{'closeClick': false,}},
      type : "iframe",
      scrolling: 'no',
      content: div,
    });     
  }
      function showFormPassword(){
    var div =  $("#formPassword").html();
    $.fancybox({
      padding : 0,
      width : '400px',
      height  : '314px',
      autoSize : false,
      closeBtn : true,
      helpers : { 'overlay' :{'closeClick': false,}},
      type : "iframe",
      scrolling: 'no',
      content: div,
    });     
  }
  </script>
</div>
<div id="formPassword" style="display:none">
  <div class="container">
    <div class="row">
      <div class="col-xs-4">
        <br>
        <p><i class="spritecart identificacion"></i><?php echo $userProfileEmail;?></p>
        <p>Ingresar email</p>
        <hr>
        <p>
          <form id="formLogin" name="formLogin" method="post" action="">
          <input type="hidden" name="cmdLogin" value="recoverPassword" />
          
            <div class="pull-left">
              <label for="password">Email :</label>
            </div>            
            <div class="col-xs-12">
              <input type="email" id="email" name="email" class="col-xs-12 form-control" required="required">
            </div>
            <div class="clearfix"></div>                        
            <hr>
            <div class="pull-right">
              <button id="btnLogin" type="submit" class="btn btn-primary" >Enviar</button>
            </div>
          </form>
        </p>
      </div>  
    </div>
  </div>
 
</div>
<div id="formSetPassword" style="display:none">
  <div class="container">
    <div class="row">
      <div class="col-xs-4">
        <br>
        <p><i class="spritecart identificacion"></i> Le enviaremos una clave de seguridad a su correo electrónico para confirmar su cuenta</p>
        <p>Registrar una nueva contraseña</p>
        <hr>
        <p>
          <form id="formLogin" name="formLogin" method="post" action="">
          <input type="hidden" name="cmdLogin" value="setPassword" />
          
            <div class="pull-left">
              <label for="password">Nueva contraseña :</label>
            </div>            
            <div class="col-xs-12">
              <input type="password" id="password" name="password" class="col-xs-12 form-control" required="required" pattern=".{6,10}" title="6 a 10 caracteres mínimo">
            </div>
            <div class="pull-left">
              <label for="password">Confirmar nueva contraseña :</label>
            </div>            
            <div class="col-xs-12">
              <input type="password" id="passwordconfirm" name="passwordconfirm" class="col-xs-12 form-control" required="required" pattern=".{6,10}" title="6 a 10 caracteres mínimo">
            </div>
            <div class="clearfix"></div>                        
            <hr>
            <div class="pull-right">
              <button id="btnLogin" type="submit" class="btn btn-primary" >Enviar</button>
            </div>
          </form>
        </p>
      </div>  
    </div>
  </div>
 
</div>
<script language="javascript" type="text/javascript">

$('#terminostxt').click(function() {
	var stat = $("#terminos").is(':checked');
         if (stat) {
           $("#comprar").css('display','block');
        }else{
			$("#comprar").css('display','none');	
		}
    });
$("#comprar").click(function(){
	var stat = $("#terminos").is(':checked');
	if (stat) {
    	$("#formPayment").submit();
    }else{
		$("#errorTerminos").html('<div class="alert alert-danger" role="alert">Debes leer y aceptar los términos y condiciones</div>');
		$("#formPayment").submit(function(){
			return false;	
		});
	}	
});
$("#formPayment").submit(function(){
	var stat = $("#terminos").is(':checked');	
	if (stat){	
		$("#formPayment").submit();
	}else{
		return false;
	}
	return false;
});
/*$("#comprar").click(function(event){
	event.preventDefault();
	var nombre = $("#nombre").val();
	var email = $("#email").val();
	$("#formPayment").submit();
});*/
function closeFancy(){
	$.fancybox.close();
}
$("#facturasi").click(function(){		
	$("#facturacion").show('slow');
	document.getElementById("razon_social").required = true;
	document.getElementById("rfc").required = true;
	document.getElementById("billstreet").required = true;
	document.getElementById("billstreet3").required = true;
	document.getElementById("billstreet2").required = true;
	document.getElementById("billzip_code").required = true;
	document.getElementById("billcity").required = true;
	document.getElementById("billstate").required = true;
	document.getElementById("billphone").required = true;
});
$("#facturano").click(function(){
	$("#facturacion").hide('slow');
	document.getElementById("razon_social").required = false;
	document.getElementById("rfc").required = false;
	document.getElementById("billstreet").required = false;
	document.getElementById("billstreet3").required = false;
	document.getElementById("billstreet2").required = false;
	document.getElementById("billzip_code").required = false;
	document.getElementById("billcity").required = false;
	document.getElementById("billstate").required = false;
	document.getElementById("billphone").required = false;
});
	$("#paypal").click(function(){ clearClasses(); hideAllPayments(); showPayment('p');});
	$("#deposito").click(function(){clearClasses(); hideAllPayments(); showPayment('d');});
	$("#transferencia").click(function(){clearClasses(); hideAllPayments(); showPayment('t');});
	$("#oxxo").click(function(){clearClasses(); hideAllPayments(); showPayment('o');});
		function clearClasses(){
			$("#deposito").removeClass('active');	
			$("#paypal").removeClass('active');	
			$("#transferencia").removeClass('active');
			$("#oxxo").removeClass('active');
		}
		function hideAllPayments(){
			$("#depositoTxt").css('display','none');	
			$("#paypalTxt").css('display','none');
			$("#transferenciaTxt").css('display','none');
			$("#oxxoTxt").css('display','none');
			$("#paymentMethod").val('');
		}
		function showPayment(payment){
			switch(payment){
				case 'p': $("#paypalTxt").show("slow");
						$("#paypal").addClass('active');
						$("#terminostxt").css('display','block');
						$("#comprar").css('display','block');
						$("#paymentMethod").val('paypal');
					break;
				case 'd': $("#depositoTxt").show("slow");
						$("#deposito").addClass('active');
						$("#terminostxt").css('display','block');
						$("#comprar").css('display','block');
						$("#paymentMethod").val('deposito');
					break;
				case 't': $("#transferenciaTxt").show("slow");
						$("#transferencia").addClass('active');
						$("#terminostxt").css('display','block');
						$("#comprar").css('display','block');
						$("#paymentMethod").val('transferencia');
					break;
				case 'o': $("#oxxoTxt").show("slow");
						$("#oxxo").addClass('active');
						$("#terminostxt").css('display','block');
						$("#comprar").css('display','block');
						$("#paymentMethod").val('oxxo');
					break;
				default:
					break;	
			}
		}
</script>