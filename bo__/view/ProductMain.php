<?php 
$products  = $this->result->getResults();
$category  = $this->category;

$brand     = $this->brand;
$totalxpag = count($products);
$q = $this->q;
$nom_catego          = $category->getName();
$nom_catego          = str_replace(' ','-', ucwords(strtolower($nom_catego)));
$parent_category_id  = $category->getParentCategoryId();
$baseFriendlyUrl     = ABS_HTTP_URL . 'producto/';
$baseListFriendlyUrl = ABS_HTTP_URL . $category->getCategoryId().'/productos/'.$nom_catego;
$productPathUrl      = $baseFriendlyUrl;
$pagingPathUrl       = $baseListFriendlyUrl;
$filtro = $_REQUEST['filtro'];

#-- Liga Ver todos
if(!empty($nom_catego)){
	$vertodos            = $baseListFriendlyUrl.'todos/';	
}

if(!empty($parent_category_id)){
	$parent_category = eCommerce_SDO_Core_Application_Category::LoadById($parent_category_id);
	$parent_category_name = $parent_category->getName();
	$parent_category_name = str_replace(' ','', ucwords(strtolower($parent_category_name)));
	$title_header = 'TecnoCompra | Productos | '.$parent_category_name." | ".$nom_catego;
}else{
	$title_header = 'TecnoCompra | Productos | '.$nom_catego;
}




$hiddenFields      = empty($this->hiddenFields) ? array() : $this->hiddenFields;
$hiddenFields['k'] = $_REQUEST['k'];
require_once 'header.php';
?>
<div class="container">
	<section id="main" class="categoria">
		<div class="row">
			<?php if(empty($filtro)){?>
				<div class="col-xs-3 col-pc-20 aside-filters">
					<?php
					include("accordion.php");
					?>
				</div>
				<?php }?>
				<div class="col-xs-<?=(empty($filtro)?'9  col-pc-80':'12  col-pc-100')?>">
					<h1 style="display:none"><?=$nom_catego;?></h1>
					<?php if(!empty($filtro)){?>
						<div class="row">
							<div class="col-md-12">
								<?php if($filtro == 'vendidos'){?>
									<img src="<?=ABS_HTTP_URL?>img/banner-mas-vendidos.jpg" alt="Productos Más Vendidos" class="img-thumbnail"/>
									<?php }elseif($filtro == 'ofertas'){?>
										<img src="<?=ABS_HTTP_URL?>img/banner-ofertas.jpg" alt="Productos Más Vendidos" class="img-thumbnail"/>
										<?php }elseif($filtro == 'recientes'){?>
											<img src="<?=ABS_HTTP_URL?>img/banner-recientes.jpg" alt="Productos Más Vendidos" class="img-thumbnail"/>
											<?php }?>
										</div>
									</div>
									<?php }?>

									<div class="toolbar">
										<div class="row">
											<div class="col-md-6 col-md-offset-6">
												<a id="filtersOpen" class="btn btn-lg pull-right">Filtrar</a>
												
											</div>
										</div>
									</div>
									<div  class="row" id="product-list-wrapper" style="padding-right:0; margin:10px 0 25px;">
										<?php if(count($products)>0){?>
											<?php $cont=1;
											$itemsCont = 0;
											foreach($products as $product){										            	
												$id				= $product->getProductId();
												$nombre 		= Util_String::subText($product->getName(),45);
												$descripcion	= utf8_encode(Util_String::subText(strip_tags($product->getDescriptionShort()),60,'chars'));

												if(empty($_REQUEST['catId'])){
													$categorias 	= eCommerce_SDO_Catalog::GetCategoriesByProduct($id);
													foreach($categorias as $categoria){
														$parent = $categoria->getParentCategoryId();		
														$category_id = $categoria->getCategoryId();	
													}
												}else{
													$category_id = $category->getCategoryId();	
												}
												
												$href			= $product->getFriendlyNameUrl($category_id );
												$src			= $product->getUrlImageId('medium',0);								
												$alt			= $nombre;
												$precio			= $product->getPrecio();

												$actualCurrency = eCommerce_SDO_CurrencyManager::GetActualCurrency();
												$precio	= eCommerce_SDO_CurrencyManager::NumberFormat($product->getPrecio()) . " " . $actualCurrency;
												$actualCurrency = eCommerce_SDO_CurrencyManager::GetActualCurrency();
												$versionesProducto = eCommerce_SDO_Catalog::GetAllProductVersiones($id);
												$totalVersiones = 0;				
												foreach($versionesProducto as $vP){
													$descuento[$itemsCont] = $vP->getDiscount();
													$precioFinalTmp = eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $vP->getPrice(), $vP->getCurrency() );								
													if($descuento[$itemsCont]>0){
														$precioFinal[$itemsCont] = $precioFinalTmp * ( 1 - ( $descuento[$itemsCont] / 100 ));	
													}else{
														$precioFinal[$itemsCont] = 0;	
													}								
													$precios[] = eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $vP->getPrice(), $vP->getCurrency() );
													$totalVersiones++;
												}							
												if(count($precios) > 1){
													$strPrice = 'desde';					
												}else{
													$strPrice = '';
												}
												$minimo = min($precios);
												$maximo = max($precios);
												unset($precios);
												if($totalVersiones <= 1){
													if($descuento[$itemsCont] > 0){
														$strPrice = "<strike>$".number_format(round($precioFinalTmp),0,'.',',')."</strike>";
														$minimo = $precioFinal[$itemsCont];
													}
												}					
												?>	
												<div class="col-sm-6 col-md-4 col-lg-3">
													<div class="item">
														<div class="thumbnail">
															<a href="<?=$href?>" title="<?=$nombre?>" class="img-thumbnail">
																<img alt="" src="<?=$src?>">
																<?php if($precioFinal[$itemsCont] > 0){?>
																	<div class="flag oferta">-<?=round($descuento[$itemsCont])?>%</div>
																	<?php }?>
																</a>
																<div class="caption">
																	<h4 style="word-wrap: break-word;"><a href="<?=$href?>" title="<?=$nombre?>"><?=$nombre?></a> </h4>
																	<p style="font-size:12px"><?=$descripcion?></p></div>
																	<div class="row">
																		<div class="col-xs-2">
																			<p class="precio old"><?=$strPrice?></p>
																		</div>
																		<div class="col-xs-10" style="text-align:right;">
																			<p class="precio">$<?= number_format(round($minimo),0,'.',',') . " " . $actualCurrency;?></p>
																		</div>
																	</div>
																	<a href="<?=$href?>" class="btn btn-primary btn-carrito btn-block" title="<?=$nombre?>">Ver más</a>
																</div>
															</div>
														</div>
														<?php 
														if($cont%4 == 0){ echo '<div style="clear:both"></div>';}
														$cont++;
													}
												}?>
											</div>
										</div>
									</div>
								</section>
							</div>
							<?php include("footer.php");?>
							<script>
								$(document).on("ready",function(){
									$("#filtersOpen").click(function(){
										$(".aside-filters").fadeIn("200").toggleClass("filtros-activos");
									});
									$("#filtersClose").click(function(){
										$(".aside-filters").toggleClass("filtros-activos").fadeOut("200");
									});

								});
							</script>