<?php
include_once ( "header.php" );
include('menu_customer.php');

function dataToInput( $var ){
	echo "".$var;
}


$member = $this->member;
$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
if ($actualLanguage == 'EN')
{
	$lang = 0;
}
else
{
	$lang = 1;
}

$title1[0] = "Your account has been created";
$title1[1] = "Su cuenta ha sido creada";

$title2[0] = "Your account has been create now you can access our site";
$title2[1] = "Su cuenta ha sido creada y ahora puede acceder a nuestro sitio";

?>

<div class="container" style="width: 100%; text-align: left;">
<div class="subtitle"><?php echo $title1[$lang]; ?></div>

<?php echo $title2[$lang]; ?>
</div>
<?php
include_once ( "footer.php" );
?>