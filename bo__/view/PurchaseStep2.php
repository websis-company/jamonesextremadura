<?php

$monto_total = $this->monto_total;
$header_title='3.Pago';
include 'header.php';
if( !$this->isEmptyCart ){?>
<div id="main">
	<div class="container">
		<?$this->class='class3';include $this->template('PurchaseMenuPasos.php')?>
		<div class="row">
			<div class="span12"><?=eCommerce_SDO_Cart::GetHTML(false, false, false, false, false)?></div>
		</div>
		<div class="row">
			<div class="span12">
            <h1 style="display:none">3.Pago</h1>
				<form method='POST' class="form-horizontal">
					<input type='hidden' name='nextStep' value='3' />
					<fieldset>
						<legend>Formas de pago</legend>
						<div class="accordion" id="tipo_pagos">
							<div class="accordion-group">
								<div class="accordion-heading">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#tipo_pagos" href="#deposito">
										<strong>Dep&oacute;sito Banc&aacute;rio</strong><span class="monto"><?=$monto_total?></span>
									</a>
								</div>
								<div id="deposito" class="accordion-body collapse">
									<div class="accordion-inner">
										<p>En esta opci&oacute;n el sistema te dar&aacute; un tal&oacute;n de pago con una referencia alfanum&eacute;rica para realizar el pago directo en ventanilla. 
										Con la referencia podremos detectar tu pago sin confirmarlo.</p>
										<p><strong>NOTA:</strong> Los dep&oacute;sitos con cheque demoran 24hrs. h&aacute;biles en ser acreditados.</p>
						                <p>* Recuerda es muy importante conservar tu comprobante de pago.</p>
						                <p>* Es necesario incluir la referencia que te da el sistema al momento de realizar tu pago.</p>
									</div>
								</div>
							</div>
							
							<div class="accordion-group">
								<div class="accordion-heading">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#tipo_pagos" href="#dineromail">
										<strong>DineroMail</strong><span class="monto"><?=$monto_total?></span>
									</a>
								</div>
								<div id="dineromail" class="accordion-body collapse">
									<div class="accordion-inner">
										<p>Realiza tu pago de forma segura en DineroMail.</p>
										<p>Monto a pagar: <?=$monto_total?></p>
									</div>
								</div>
							</div>
							<input type="hidden" name="tipo_pago" id="tipo_pago" value="" />
							<button class="btn" type="submit" style="float:right;margin:25px;">Confirmar compra</button>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
<!--
	jQuery(document).ready(
		function(){
			jQuery('input#tipo_pago').click(
				function(event){
					radio = jQuery(event.currentTarget);
					jQuery("div[id^='tabla_']").css('display','none');
					switch(radio.val()){
						case 'deposito':
							jQuery('div#tabla_deposito').css('display','block');
							break;
						case 'transferencia':
							jQuery('div#tabla_transferencia').css('display','block');
							break;
						case 'paypal':
							jQuery('div#tabla_paypal').css('display','block');
							break;
						case 'en_linea|':
							jQuery('div#tabla_en_linea0').css('display','block');
							break;
						<?$i=1;foreach ($array_plan as $plan){?>
						case 'en_linea|<?=$plan?>':
							jQuery('div#tabla_en_linea<?=$i?>').css('display','block');
							break;
						<?$i++;}?>
						
					}
				}
			);
		}
	);
//-->
</script>
<?}
include_once 'footer.php';?>