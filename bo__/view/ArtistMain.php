<?php



require_once("header.php");

$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();

$result	  = $this->result;

$artists = $result->getResults();



$totalPages = $result->getTotalPages();

$actualPage = $result->getPage();



$nextPage = ( $actualPage < $totalPages ) ? $actualPage+1 : $actualPage;



$lastPage = ( $actualPage > 1 ) ? $actualPage-1 : 1;





$querySearch = ( $result instanceof eCommerce_Entity_Search_Result_Artist ) ? $result->getKeywords() : '';

$querySearch = empty($querySearch) ? '' : $querySearch;





$htmlLinksGroups =	'';

	if( $totalPages > 1 ){

		

		$elementsForGroups = 5;

		

		if( ($actualPage + $elementsForGroups) >= $totalPages){

			$initialPage = ($totalPages - $elementsForGroups > 0) ? ($totalPages - $elementsForGroups) : 1;

		}else{

			$initialPage = (int)($actualPage / $elementsForGroups);

			

			$initialPage = ( $actualPage % $elementsForGroups == 0) ? $initialPage -1 : $initialPage;



			$initialPage = ($elementsForGroups * $initialPage) + 1;

		}

		

		$finalPage = $initialPage + $elementsForGroups;

		$finalPage = ($totalPages > $finalPage) ? $finalPage : $totalPages;

		

		$htmlLinksGroups .= "<a href=\"javascript:setParam('frmProduct','p','{$lastPage}');\" class='resultPages' >&lt;</a>&nbsp;&nbsp;".($actualLanguage !='EN' ? "Página" : "Page") ." ";

		for($i=$initialPage; $i < $finalPage; $i++){

			$class = ( $i == $actualPage ) ? 'actualPage' : 'resultPages';

			$htmlLinksGroups .= "&nbsp;<a href=\"javascript:setParam('frmProduct','p','{$i}');\" class='{$class}' >{$i}</a>&nbsp;&nbsp;";

		}

		

		$class = ( $totalPages == $actualPage ) ? 'actualPage' : 'resultPages';

		$finalPage = ( $totalPages == $finalPage) ? " <a href=\"javascript:setParam('frmProduct','p','{$totalPages}');\" class='{$class}' >{$totalPages}</a>" : 

		"... <a href=\"javascript:setParam('frmProduct','p','{$totalPages}');\" class='resultPages' >{$totalPages}</a>

		";

		$htmlLinksGroups .= $finalPage;

		$htmlLinksGroups .= "&nbsp;&nbsp;<a href=\"javascript:setParam('frmProduct','p','" . ($actualPage + 1) . "');\" class='resultPages' >&gt;</a>";

	}

	else{

		$htmlLinksGroups .= ($actualLanguage !='EN' ? "Página" : "Page") ." 1";

	}





?>



<p class="titleOfCategory" style="font-family:Arial, Helvetica, sans-serif"><?php if($actualLanguage == 'EN')
	{
		echo "ARTISTS";
	}
	else
	{
		echo "ARTISTAS";
	}

$x = count($artists);

if($x> 0){

?></p>

<p align="right" class="defaultText">&nbsp;<?php echo $htmlLinksGroups; ?>&nbsp;</p>



<hr style="border-width:thin;" size="1" color="#C0C0C0"/>

<div style="padding-left:50px;padding-top:7px;  width:30%">

  <p>

    <?php 

 $i = 0;

 

 foreach( $artists as $artist ){

 	echo eCommerce_SDO_Artist::writeHTMLInfoArtist( $artist, ($x > ++$i) );

 }

?>

    <?php

}else{
	if($actualLanguage == 'EN')
	{
		echo "<p class='titleOfCategory' style='font-family:Verdana, Arial, Helvetica, sans-serif;'>There are not results to display</p>";
	}
	else
	{
		echo "<p class='titleOfCategory' style='font-family:Verdana, Arial, Helvetica, sans-serif;'>No hay resultados que desplegar</p>";
	}
}

?>

  </p>

</div>







<hr style="border-width:thin;" size="1" color="#C0C0C0"/>

<p align="right" class="defaultText">&nbsp;<?php echo $htmlLinksGroups; ?>&nbsp;

<p align="right" class="defaultText">

<p align="right" class="defaultText">

<form action='' name='frmProduct' style="margin:0px;">

<input type='hidden' name='p' />

<input type='hidden' name='o' />

<input type='hidden' name='q' value="<?php echo empty($querySearch) ? '' : $querySearch; ?>" />

</form>



<script language="javascript"> 

	function setParam( formName, fieldName, value, submit ){   

		if ( submit == null ) {     submit = true;   }   

		var form = document.forms[formName];   

		form.elements[fieldName].value = value;   

		if ( submit ) {   	form.submit();   } 

	}

</script>



</p>

<?php

include_once("footer.php");

?>