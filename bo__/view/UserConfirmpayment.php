<?php
if( ! @include_once( "header.php" ) ) {
	header('location:../index.php');
	die();
}
$error = $this->error;
?>
<link href="<?=ABS_HTTP_URL?>css/form.css" rel="stylesheet" type="text/css" />
 <div align="justify" class="container"> 
   <h2 align="center" class="h2 text-center">Confirmación de Pago</h2>
	<hr class="hr" />
<div class="row">
<?php include "userInfoSidebar.php";?>
<div class="col-xs-9">
<?php if(!empty($error)){ echo $error; }?>
	 <div class="col-xs-6">
     
		<form action="" method="post" name="formpago" id="formpago">
        <input type="hidden" name="cmd" value="sendformpago" />
<div class="form-group">
	<label for="exampleInputEmail1">Selecciona tu operación realizada:</label>
    <select class="form-control" name="entity[operacion_realizada]" id="operacionrealizada" required="required">
    	<option value="">Selecciona una opción</option>
    	<option value="Depósito en Oxxo">Depósito en Oxxo</option>
        <option value="Depósito ventanilla Bancomer">Depósito ventanilla Bancomer</option>
        <option value="Transferencia Electrónica Bancomer">Transferencia Electrónica Bancomer </option>
    </select> 
    </div>
    <div class="form-group"> 
    <label for="exampleInputEmail1">Número de confirmación (Ticket o Boucher):</label>
    <input type="text" class="form-control" placeholder="" name="entity[numero_confirmacion]"  id="numero_confirmacion" value="<?=$numero_confirmacion?>" required="required" >  
    </div>
    <div class="form-group">
    <label for="exampleInputEmail1">Número de pedido:</label>
    <input type="text" class="form-control" placeholder="" name="entity[numero_pedido]"  id="numero_pedido" value="<?=$numero_pedido?>" required="required" > 
    </div>
    <div class="form-group">
     
<script src="//cdn.jsdelivr.net/webshim/1.14.5/polyfiller.js"></script>
<script>
webshims.setOptions('forms-ext', {types: 'date'});
webshims.polyfill('forms forms-ext');
$.webshims.formcfg = {
es: {
    dFormat: '-',
    dateSigns: '-',
    patterns: {
        d: "yy-mm-dd"
    }
}

};

</script>
    <label for="exampleInputEmail1">Fecha de operación:</label>
    <input type="date"  class="form-control" placeholder="" name="entity[fecha_operacion]"  id="fecha_operacion" value="<?=$fecha_operacion?>" required="required">                               
	</div>
    <div class="form-group">
    <label for="exampleInputEmail1">Monto:</label>
    <input type="number" min="1" step="any" class="form-control" placeholder="" name="entity[monto]"  id="monto" value="<?=$monto?>" required="required" placeholder="$">                           
	</div>
    <style>
    .btn-pink:hover{background-color:#ff4742; color:#FFFFFF}
    </style>
    <button type="submit" class="btn btn-pink pull-right btn-sm">
          <span class="glyphicon glyphicon-send"></span> Enviar 
        </button>
</form>
	</div>
</div>
</div>
</div>
<?php

include_once( "footer.php" );
?>
