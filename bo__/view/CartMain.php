<?php 
$title_header='Print Proyect | Carrito de Compras';
include('includes/head.php');?>

<?php include('includes/header.php');
//require_once("bo/menu_cart.php");
$spanishLanguage = eCommerce_SDO_LanguageManager::GetValidLanguage('SP');
$spanishLanguage = $spanishLanguage["id"];
$actualLanguage  = eCommerce_SDO_LanguageManager::GetActualLanguage();
$cart            = $this->cart;
$message         = $this->message;
$messageClass    = $this->messageClass;
$actualLanguage  = eCommerce_SDO_LanguageManager::GetActualLanguage();

?>

<div id="begin" class="c-layout-page page-inside">
	<section class="c-bg-blanco">
		<div class="container">
			<div class="col-xs-12">
				<div id="product-list-wrapper">
					<br />
					<h1 class="h2"><strong>Mi Compra</strong></h1>
					<div>
						<?php
						$items = $cart->getItems();
						if( count( $items ) > 0 ){
							if(!is_null($message)){
							 	echo '<p class="' . $messageClass . ' alert alert-success">' . $message . '</p>';
							}?>

							<?php echo eCommerce_SDO_Cart::GetHTML(true,true,true,null,false,false);?>
							<!-- <a href="<?=ABS_HTTP_URL?>cuadros-decorativos/" class="btn btn-primary" style="padding:10px 10px;">Seguir comprando</a>
							<input type="submit" class="btn btn-primary" value="COMPRAR" id="purchase" style="padding:10px 10px;"> -->
						<?php
						}else{
						?>
						<!-- Carrito vacío. Begin -->
						<section class="shopping-cart-empty">
							<span class="mensaje"><?php if($actualLanguage == 'EN'){ ?>Your cart is empty<?php }else{?>Su carrito de compras está vacio <?php } ?></span>
							<br/>
							<span class="button-wrapper"><a href="<?=ABS_HTTP_URL?>cuadros-decorativos/" class="button-lineal primary">Continuar comprando</a></span>
						</section>
						<!-- Carrito vacío. End -->
						<?php
						}//end if
						?>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<!-- CART -->


<?php
$GLOBALS["scripts"] = "
<script language='Javascript'>
	function updateQuantity( productId ){
		fieldQuantityName = 'quantity_' + productId;
		quantity = document.getElementById( fieldQuantityName ).value;
		if(quantity <=0){
			alert('La cantidad debe ser mayor o igual a 1');
			return false;
		}else{
			document.location.href = 'cart.php?cmd=update&productId=' + productId + '&' + fieldQuantityName + '='+quantity;	
		}
	}//end function
</script>";

include("includes/footer.php");
?>

<script language="javascript" type="text/javascript">
	$("#purchase").click(function(){	
		$("#formCart").submit();		
	});
</script>