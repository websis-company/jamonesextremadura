<?php

$result            = $this->result;
$Categorias_p_menu = $result->getResults();

$currentCategory   = $this->currentCategory;
if($currentCategory->getCategoryId() != 0){
    $img_banner = $currentCategory->getUrlBannerId('large',0);
    $nom_category = $currentCategory->getName();
    $des_category = $currentCategory->getDescription();
}


$id_parent_category = $this->id_parent_category;
$id_category_select = $this->id_category;

$hiddenFields = empty($this->hiddenFields) ? array() : $this->hiddenFields;
$title_header = "Print Proyect | Productos Promocionales";
include('includes/head.php');
include('includes/header.php');
?>

<!-- BEGIN: PAGE CONTAINER -->
<div class="c-layout-page page-inside">
    <div class="c-layout-breadcrumbs-1 c-bgimage-full   c-fonts-uppercase c-fonts-bold   c-bg-img-center" style="background-image: url(<?php echo $img_banner; ?>)">
        <div class="c-breadcrumbs-wrapper">
            <div class="container">
                <div class="c-page-title col-sm-12">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                    <h1 class="c-font-uppercase c-font-bold c-font-blanco c-font-20 c-font-slim c-opacity-09 c-center"><?php echo $nom_category; ?></h1><br>
                    <h4 class="c-font-blanco c-font-thin c-opacity-09 c-center"><?php echo $des_category; ?></h4>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
        </div>
    </div>

<?php /*?><?php include('menu-proceso.php'); ?><?php */?>

    <div class="container-fluid">
        <div class="c-layout-sidebar-menu c-theme ">
            <!-- BEGIN: LAYOUT/SIDEBARS/SHOP-SIDEBAR-MENU -->
            <?php include('sidebar-menu.php');?>
            <!-- END: LAYOUT/SIDEBARS/SHOP-SIDEBAR-MENU -->
        </div>
		<div class="c-layout-sidebar-content ">
		    <!-- BEGIN: PAGE CONTENT -->
		    <!-- BEGIN: CONTENT/SHOPS/SHOP-RESULT-FILTER-1 -->
		    <!-- END: CONTENT/SHOPS/SHOP-RESULT-FILTER-1 -->
		    <div class="margin-t-20"></div>
		    <!-- BEGIN: CONTENT/SHOPS/SHOP-2-7 -->
		    <div class="c-bs-grid-small-space">
		        <div class="row">
		        	<?php foreach ($Categorias_p_menu as $categoria_item) {
						$nombre     = $categoria_item->getName();
						$href       = $categoria_item->getFriendlyNameUrl();
						/*$img_random = eCommerce_SDO_Core_Application_Galeria::ImageRandom($categoria_item->getGalerycategoryId());
                        if(!is_null($img_random[0])){
                            $img = $img_random[0]->getUrlArrayImages('medium',0);
                        }else{
                            $img = "";
                        }*/
						$img         = $categoria_item->getUrlImageId('medium',0) != "" ? $categoria_item->getUrlImageId('medium',0) : "";
						$id_category = $categoria_item->getCategoryId();
						if($id_category == '4'){
							$rango = '$333.50 - $1,270.20 MXN';
						}elseif($id_category == '5'){
							$rango = '$1,015.00 - $2,001.00 MXN';
						}elseif($id_category == '6'){
							$rango = '$1,299.20 - $11,415.33 MXN';
						}
		        	?>
		            <div class="col-md-3 col-sm-6 margin-b-20">
		                <div class="c-content-product-2 c-bg-blanco c-border">
		                    <div class="c-content-overlay">
		                        <div class="c-overlay-wrapper">
		                            <div class="c-overlay-content">
		                                <a href="<?php echo $href;?>" class="btn btn-md c-btn-square c-btn-border-1x c-btn-bold c-btn-uppercase c-btn-grey-1" title="<?php echo $nombre; ?>">Ver</a> 
		                                <!-- <a href="http://vicomstudio.com/demos/printproyect/web-02/detalle.php" class="btn btn-md c-btn-square c-btn-border-1x c-btn-bold c-btn-uppercase c-btn-grey-1" title="compartir"> <i class="fa fa-facebook c-font-cuarto redes" title="compartir"></i></a> -->
		                            </div>
		                        </div>
		                        <div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 230px; background-image: url(<?php echo $img; ?>);"></div>
		                    </div>
		                    <div class="c-info col-md-12">
		                        <div class="col-md-12 col-xs-12">
		                        	<p class="c-title c-font-16 c-font-slim c-center"><?php echo $nombre; ?></p>
		                        </div>
		                        <!-- <div class="col-md-1 c-font-septimo hidden-xs">|</div>
		                        <div class="col-md-2 col-xs-6" align="center">
		                        	<a href="#"><i class="fa fa-facebook c-font-sexto redes c-font-right" title="compartir"></i></a>
		                        </div> -->
		                    </div>
		                    <div class="row c-center">
		                        <div class="col-md-12 col-xs-12">
		                        	<p class="c-price c-font-14 c-font-slim c-center">
		                        		<?php echo $rango;?>
		                            </p>
		                        </div>
		                    </div>
		                    <div class="btn-group btn-group-justified" role="group">
		                        <div class="btn-group c-border-left c-border-top" role="group">
		                            <a href="<?php echo $href; ?>" class="btn btn-sm c-btn-blanco c-btn-uppercase c-btn-square c-font-grey-3 c-font-blanco-hover c-bg-red-2-hover c-btn-product" title="<?php echo $nombre; ?>">Ver Más</a>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        	<?php
		        	}//end foreach ?>
		        </div>
		    </div>
		    <!-- END: CONTENT/SHOPS/SHOP-2-7 -->
		    <div class="pull-right">
		        <ul class="c-content-pagination c-square c-theme">
		            <?php //eCommerce_SDO_Core_Application_Form::foPrintPager($this->result,"Form","","","",4,$hiddenFields);?>
		        </ul>
		    </div>
		    <!-- END: PAGE CONTENT -->
		</div>
    </div>
</div>
<!-- END: PAGE CONTAINER -->

<?php
include("includes/footer.php");
?>
