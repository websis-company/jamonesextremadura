<?php 
$Noticias           = $this->Noticias;
$title_header       = 'TecnoCompra  | Noticias | '.$Noticias->getTitulo();
$header_description = '';
include("header.php");
$titulo             = $Noticias->getTitulo();
$descripcion        = Util_String::textByHyperlink(nl2br($Noticias->getDescripcion()));
$img_0              = $Noticias->getHTMLImagenPrincipal('560','420');
$href               = $Noticias->getFriendlyNameUrl();
setlocale(LC_TIME, 'es_ES.UTF-8');
$fecha              = $Noticias->getFecha();
$fecha_formato      = strftime( "%d de %B de %Y", strtotime($fecha));
$link = $Noticias->getLink();
$txtLink = $Noticias->getTexto();

?>
<section id="interior2">
	<div class="overlay">
		<div class="parallaxeos_animate  fadeIn horizontal_center vertical_center">
			<h2 class="h2"><span style="color: #ffffff;">Noticias</span></h2>
		</div>
	</div>
</section>
<section id="testimonios">
	<div style="padding-top:50px; padding-bottom:50px" class="container">
		<div style="padding:15px" align="center" class="col-xs-12 col-sm-12">
			<h2><?=$titulo?></h2></br>
			<p><span style="color:#F00803"><?=$fecha_formato?></span></p>
		</div>
		<div class="row">
			<div style="padding:35px" align="left" class="col-sm-6">
				<?=$img_0?>
			</div>
			<div style="padding:35px;" align="justify" class="col-sm-6 icon">
				<p><?=nl2br($descripcion)?> </p>
				<?php if(!empty($link)){?>
					<a href="<?=$link?>" target="_blank" style="color:#F00803"><?=(!empty($txtLink)?$txtLink:$link)?></a>
					<?php }?>
					<p>&nbsp;</p>
            <!--
<h4 style="color:#F00803">Fuente: </h4>
<p>ZonaGEEK.com</p>-->
</div>
</div>
</div>
</section>
<?php include("footer.php"); ?>
<script type="text/javascript" src="<?=ABS_HTTP_URL?>arquivos/home.js"></script>
