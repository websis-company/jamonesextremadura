<?php
$header_title='Accesa a tu Cuenta. ¿No Tienes Cuenta? Registrate.';
require_once( "bo/common.php" );

include_once( "header.php" );
include('menu_customer.php');

if(isset($_GET['username'])){
  $username = $_GET['username'];
}

$msg      = $this->msg;
$msgClass = $this->msgClass;
$msgResponse = !empty($_REQUEST['error'])?"<div class='alert alert-danger' role='alert'>El usuario no se encuentra registrado y/o el usuario / contrase&ntilde;a son incorrectos </div>":(!empty($msg)?"<div class='alert ".$msgClass."' role='alert'>".$msg."</div>":'');
?>

<style type="text/css">
  .error{color: #E03030; font-size: 15px; display: block;}
	input.error, input.error:focus, input.check.error{border: 2px solid #E03030;}	
	select.error, select.error:focus, select.check.error{border: 2px solid #E03030;}
</style>


<div class="home">
	<section id="teaser3" class="">
		<div class="container">
			<div class="row">
            <br />
        <div class="col-xs-12" align="center">
          <div>
            <h1 class="h4" style="font-weight: normal; color: #000000; font-size: 35px; ">
              Ingresa tu mail y contraseña para acceder
            </h1>
          </div>
          <br/>

          <style>.alert{font-size:18px !important; font-weight:bold !important}</style>

          <?php if(!empty($msgResponse)){?>
          <div class="col-xs-11">
						<?=$msgResponse?>
				  </div>
				  <?php }?>


					<form id="formUserLogin" name="formUserLogin" action="" method="post">
            <?php 
              if($this->message != 'Acceso Restringido'){
                echo  $this->message;
            }?>
            <div align="center">
              <input type="email" name="username" id="username" class="form-control"  value="<?php echo $username;?>" placeholder="su@correo.com" style="color: #777777; width: 60%; height: 55px; text-align: center;" required="required">
            </div>

            <div align="center">
              <input type="password" name="password" id="password" class="form-control" value="" placeholder="Password" style="color: #777777; width:60%; height: 55px; text-align: center;" required="required">
            </div>

            <input type='hidden' name='cmd' value='login' />
            <!--<input type="hidden" name="userType" id="userType" value="new" />-->
			<input type="hidden" name="userType" id="userType" value="FormularioUser" />			
            <div class="clearfix"></div>
						
            <div style="margin-top: 15px;">
							<button type="submit" class="btn btn-primary" style="width: 200px; height: 45px;">Enviar</button>
						</div>
					</form>
				</div>

        <div class="col-xs-12" align="center" style="color: #00A1D3;">
          <div class="col-xs-6">
            <a style="cursor:pointer;color: #000000;" onclick="showFormPassword()" >&iquest;Olvidaste tu contrase&ntilde;a?</a>
          </div>
          <div class="col-xs-6" style="display:block;">
            <a style="cursor:pointer;color: #000000;" onclick="showFormSetNewPassword()" >&iquest;A&uacute;n no tienes contrase&ntilde;a? Solicitala aqu&iacute;.</a>
          </div>
        </div>
      </div>
			
      <div class="clearfix"></div>
			
      <div class="row" align="center" style="margin-top: 25px;">
				<div style="width: 350px; color: #000000;" class="h4">
					Guardamos tu correo electr&oacute;nico de manera 100% segura para
					<br>
				</div>
				<div style="width: 350px;" align="left">
					<ul>
						<li>Identificar su perfil</li>
						<li>Notificar sobre los estados de su compra</li>
						<li>Guardar el historial de compra</li>
						<li>Facilitar el proceso de compras</li>
					</ul>					
				</div>
			</div>
		</div>
  </section>
 </div>

<div id="formPassword" style="display:none">
  <div class="container" style="width:100%; font-family:Oswald">
    <div class="row">
      <div class="col-xs-12">
        <br>
        <p><i class="spritecart identificacion"></i></p>
        <p>Ingresar tu email para recuperar tu contraseña</p>
        <hr>
        <p>
          <form id="formLogin" name="formLogin" method="post" action="">
          <input type="hidden" name="cmd" value="recoverPasswordByEmail" />
          
            <div class="pull-left">
              <label for="password">Email :</label>
            </div>            
            <div class="col-xs-12">
              <input type="email" id="email" name="email" class="col-xs-12 form-control" required="required">
            </div>
            <div class="clearfix"></div>                        
            <hr>
            <div class="pull-right">
              <button id="btnLogin" type="submit" class="btn btn-primary" >Enviar</button>
            </div>
          </form>
        </p>
      </div>  
    </div>
  </div>
</div>


<div id="formNewPassword" style="display:none">
  <div class="container" style="width:100%; font-family:Oswald">
    <div class="row">
      <div class="col-xs-12">
        <br>
        <p><i class="spritecart identificacion"></i></p>
        <p>Ingresar tu email para solicitar una contraseña</p>
        <hr>
        <p>
          <form id="formLogin" name="formLogin" method="post" action="">
            <input type="hidden" name="cmd" value="setNewPasswordByEmail" />
            <input type="hidden" name="type" value="generaPassword" />
            <div class="pull-left">
              <label for="password">Email :</label>
            </div>            
            <div class="col-xs-12">
              <input type="email" id="email" name="email" class="col-xs-12 form-control" required="required">
            </div>
            
            <div class="clearfix"></div>                        
            <hr>
            <div class="pull-right">
              <button id="btnLogin" type="submit" class="btn btn-primary" >Enviar</button>
            </div>
          </form>
        </p>
      </div>  
    </div>
  </div>
</div>


<?php
include_once( "footer.php" );
?>
<script type="text/javascript">
	$(document).ready(function(e){
		$("#formUserLogin").validate({
			debug: false,
			rules:{
				username:{required: true, email: true},
				password:{required: true, password: true},
			},
			messages:{
				username:{required: 'Este campo es obligatorio.', email:'Introduzca un correo electrónico válido, por favor.'},
				password:{required: 'Este campo es obligatorio.'},
			},
			submitHandler: function(form){
				document.getElementById("formUserLogin").submit();
			},
		});
	});
function showFormPassword(){
    var div =  $("#formPassword").html();
    $.fancybox({
      padding : 0,
      width : '400px',
      height  : '314px',
      autoSize : false,
      closeBtn : true,
      helpers : { 'overlay' :{'closeClick': false,}},
      type : "iframe",
      scrolling: 'no',
      content: div,
    });     
  }
 function showFormSetNewPassword(){
    var div =  $("#formNewPassword").html();
    $.fancybox({
      padding : 0,
      width : '400px',
      height  : '314px',
      autoSize : false,
      closeBtn : true,
      helpers : { 'overlay' :{'closeClick': false,}},
      type : "iframe",
      scrolling: 'no',
      content: div,
    });     
  }
</script>