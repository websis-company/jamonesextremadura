<?php include_once('includes/head.php'); ?>
<?php 
include_once('includes/header.php'); ?>

<?php 
//include_once("headerFlujo.php");
$noAut                = Application::getParameter('no_aut',false,NULL);
//$cotizacion         = true;
$paymentMethod        = $this->paymentMethod = $cotizacion ? 'CT' : $this->paymentMethod;
$html_cart            = $this->html_cart;
$responseCode         = $this->responseCode;
$responseMessage      = $this->responseMessage;
$amount               = $this->amount;
$statement_descriptor = $this->statement_descriptor;
//echo $responseCode;
//die();
if(isset($this->responseId)){
     $responseId   =  $this->responseId;
     $orderPayment = eCommerce_SDO_Core_Application_OrderPayment::LoadById($responseId);
}
//$paymentMethod = eCommerce_SDO_Order::GetPaymentMethodById( $this->paymentMethod );
$orderFull = $this->orderFull;
$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
if ($actualLanguage == 'EN')
{
    $lang = 0;
}
else
{
    $lang = 1;
}
$complete[0] = "ORDER COMPLETE";
$complete[1] = "Orden completada";
$completed[0] = "Your order has been completed";
$completed[1] = "Su cotizaci&oacute;n ha sido completada";
$payment_inf[0] = "Payment Information";
$payment_inf[1] = "Informaci&oacute;n de Pago";
$method[0] = "Payment Method";
$method[1] = "M&eacute;todo de Pago";
$bnk[0] = "Bank";
$bnk[1] = "Banco";
$accnt[0] = "Account Number";
$accnt[1] = "N&uacute;mero de cuenta";
$name[0] = "Name";
$name[1] = "Nombre";
$att[0] = "ATENTION";
$att[1] = "ATENCI&Oacute;N";
$no_pay[0] = "The payment method that you select doesn�t exists. Please contact us for complete the payment";
$no_pay[1] = "El m&eacute;todo de pago seleccionado no existe. Por Favor cont&aacute;ctenos para completar el pago";
$viewOrders[0] = "View my orders";
$viewOrders[1] = "Ver mis ordenes";
if( !$isEmptyCart ){
    $profile = eCommerce_SDO_Core_Application_Profile::LoadById($orderFull->getProfileId());
    $address = eCommerce_SDO_Core_Application_UserAddress::LoadById($orderFull->getProfileId());
    $order = eCommerce_SDO_Order::LoadById($orderFull->getOrderId());
    switch ($order->getTipoTarjeta()) {
        case 'oxxo':
            $medio_pago = 'Oxxo';
            break;
        case 'serfin_ticket':
            $medio_pago = 'Serfin';
            break;
        case 'banamex_ticket':
            $medio_pago = 'Banamex';
            break;
        case 'bancomer_ticket':
            $medio_pago = 'BBVA Bancomer';
            break;
        case '7eleven':
            $medio_pago = '7Eleven';
            break;
        case 'gestopago':
            $medio_pago = '7Eleven';
            break;
        case 'telecomm':
            $medio_pago = 'Telecomm';
            break;
    }
?>

<style type="text/css">
    #btnDescuento{
        display: none !important;
    }
</style>
<!--<link href="estilos.css" rel="stylesheet" type="text/css" />-->
<div id="main">
    <div id="begin" class="c-layout-page padding-t-150">
        <div class="container">
            <h1 class="h1 lemon">Tienda en línea</h1>
        </div>
    </div>
    <div class="container thank_you_page">
        <div class="row" style="margin-top: 25px;">
            <? if( $paymentMethod != 'Unknow Payment Method' || $cotizacion ){
                $html = "";
                switch( $this->paymentMethod ){
                    case 'en_linea':
                        $html .= '
                            <div class="row" style="font-family: Arial !important; font-size: 17px;">
                                <div class="col-sm-12 col-md-10 col-md-offset-1 thank-you-wrapper">
                                <div class="col-xs-12 col-md-7">
                                    <h2 class="h2" style="text-transform:uppercase; font-family: Arial; font-size: 35px;">Gracias por su compra</h2>
                                </div>
                                <div class="col-xs-12 col-md-5" style="padding: 10px;">
                                    <div style="text-transform:uppercase; text-align: right; font-weight: bold; font-size: 25px;">Confirmación de pedido</div>
                                    <div style="text-align: right;">Hola, '.$profile->getFirstName().' '.$profile->getLastName().'</div>
                                    <div style="text-align: right;">Pedido #: <span style="color: #6901A0;">'.$orderFull->getOrderId().'</span></div>
                                </div>
                                <div class="col-xs-12" style="border-radius: 15px; padding: 15px; border: 1px solid black; background-color: #FFF;">
                                    <div class="row">
                                        <div class="col-md-6">';
                                            if($responseCode == 'pending_contingency' || $responseCode == "pending_review_manual"){
                                                $html .= 'Tu orden se porceso con éxito, pero la transacción de pago está pendiente de ser confirmada esto puede ocurrir por filtros de seguridad.
                                                        <br/> <br/>Una vez que tu pago se haya procesado correctamente se te enviará la confirmación a tu correo electrónico.
                                                        <br/><br/>
                                                        <p>
                                                            <strong>Método de pago :</strong> En Línea <br/>
                                                            <strong>Estatus :</strong> Pendiente
                                                        </p>';
                                            }else{
                                                $html .= '
                                                        <p>Tu order y pago se procesaron con éxito, el detalle de tu compra se muestra a continuación </p>
                                                        <br/>
                                                        <p>En tu resumen verás el cargo de $ '.$amount.' MXN como '.$statement_descriptor.' </p>
                                                        <p>
                                                            <strong>Método de pago :</strong> En Línea <br/>
                                                            <strong>Estatus :</strong> Pagado
                                                        </p>';
                                            }
                                        $html .='
                                        </div>
                                        <div class="col-md-6">
                                            <p class="col-md-offset-2"><strong>Tu pedido será enviado a:</strong> <br><br>
                                            '.$address->getStreet().' '.$address->getStreet3().' Int. '.$address->getStreet4().'<br>'.$address->getStreet2().'<br>'.$address->getCity().', '.$address->getState().' '.$address->getZipCode().' <br>México<br></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-xs-12">
                                    <h1 class="h2" style="text-transform:uppercase; font-family: Arial; font-size: 35px;">Mis productos</h1>
                                </div>
                                <div class="clearfix"></div>
                                <div style="border: 1px solid black;">
                                '.eCommerce_SDO_Cart::GetHTML(false,false,true,false,false,true).'
                                </div>
                                <div class="clearfix"></div>
                                <br/>
                                <br/>
                                <div>
                                    <div class="pull-right" style="padding-left: 25px;">
                                        <a href="'.ABS_HTTP_URL.'" class="btn" style="background-image: none; background-color: #000; color: #FFF; font-family: Arial; font-size: 15px;">ACEPTAR</a>
                                    </div>
                                    <!--<div class="pull-right">
                                        <a title="Solicitar factura"  href="'.ABS_HTTP_URL.'solicitarFactura.php?orderId='.$orderFull->getOrderId().'&user_id='.$profile->getProfileId().'" class="ifancybox666 btn" style="background-image: none; background-color: #6901A0; color: #FFF; font-family: Arial; font-size: 15px;" id="btnFactura">Solicitar factura</a>
                                    </div>-->    
                                </div>
                                <br/>
                                <br/>
                            </div>
                        ';
                        break;
                    case 'P':
                    case 'paypal':
                        $paypal = new eCommerce_SDO_Core_Application_Paypal( $orderFull->getOrderId() );
                        ob_start(); 
                        ?>
                        <table width="100%" border="0" cellspacing="5" cellpadding="0">
                            <tr>
                                <td width="100%" align="center" class="negro16" style="padding-bottom:10px">
                                    <p>
                                        <span class="anaranjado14"><strong>Tu compra ha sido procesada exitosamente</strong></span>
                                    </p>
                                    <p class="gris14">
                                        En breve recibir&aacute;s un correo electr&oacute;nico con la confirmaci&oacute;n e informaci&oacute;n adicional importante.
                                    </p>
                                    <p>&nbsp;</p>
                                    <p class="negro20">Muchas Gracias por tu compra.</p>
                                    <p>&nbsp;</p>
                                    <?php  echo $paypal->getHtml();?>
                                </td>
                            </tr>
                        </table>
                        <?
                        $html=ob_get_contents();
                        ob_clean();
                            //<img border="0" src="ima/fo/paypal.jpg" width="167" height="250" align="left">
                        break;
                    case "deposito":
                    case 'D':
                        $html = '
                        <div class="row" style="font-family: Arial !important; font-size: 17px;">
                            <div class="col-sm-12 col-md-10 col-md-offset-1 thank-you-wrapper">
                                <div class="col-xs-12 col-md-7">
                                    <h2 class="h2" style="text-transform:uppercase; font-family: Arial; font-size: 35px;">Gracias por su compra</h2>
                                </div>
                                <div class="col-xs-12 col-md-5" style="padding: 10px;">
                                    <div style="text-transform:uppercase; text-align: right; font-weight: bold; font-size: 25px;">
                                        Confirmación de pedido
                                    </div>
                                    <div style="text-align: right;">Hola, '.$profile->getFirstName().' '.$profile->getLastName().'</div>
                                    <div style="text-align: right;">Pedido #: <span style="color: #6901A0;">'.$orderFull->getOrderId().'</span></div>
                                </div>
                                <div class="col-xs-12" style="border-radius: 15px; padding: 15px; border: 1px solid black; background-color: #FFF;">
                                    <div class="row">
                                        <div class="col-md-6">
                                            Tu orden se porceso con éxito. Una vez que tu pago se haya procesado se te enviará la confirmación a tu correo electrónico.
                                            <br/><br/>
                                            <p>
                                                <strong>Método de pago :</strong> Deposito en &eacute;fectivo <br/>
                                                <strong>Medio de pago :</strong> '.$medio_pago.'<br/>
                                                <strong>Estatus :</strong> Pendiente <br/>
                                            </p>
                                        </div>
                                        <div class="col-md-6">
                                            <p class="col-md-offset-2">
                                                <strong>Tu pedido será enviado a:</strong> <br><br>
                                                '.$address->getStreet().' '.$address->getStreet3().' Int. '.$address->getStreet4().'<br>'.$address->getStreet2().'<br>'.$address->getCity().', '.$address->getState().' '.$address->getZipCode().' <br>México<br><br>
                                                <a href="'.$orderPayment->getUrlpayment().'" target="_blank" class="btn c-btn-sexto c-btn-uppercase btn-lg c-btn-sbold c-btn-square c-font-middle col-xs-12"><img src="'.ABS_HTTP_URL.'images/layout/print.png" width="30" height="30"/> &nbsp; Imprimir comprobante de pago</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-xs-12">
                                    <h1 class="h2" style="text-transform:uppercase; font-family: Arial; font-size: 35px;">Mis productos</h1>
                                </div>
                                <div class="clearfix"></div>
                                <div style="border: 1px solid black;">
                                    '.eCommerce_SDO_Cart::GetHTML(false,false,true,false,false,true).'
                                </div>
                                <div class="clearfix"></div>
                                <br/>
                                <br/>
                                <div>
                                    <div class="pull-right" style="padding-left: 25px;">
                                        <a href="'.ABS_HTTP_URL.'" class="btn" style="background-image: none; background-color: #000; color: #FFF; font-family: Arial; font-size: 15px;">
                                            ACEPTAR
                                        </a>
                                    </div>
                                    <!--<div class="pull-right">
                                        <a title="Solicitar factura"  href="'.ABS_HTTP_URL.'solicitarFactura.php?orderId='.$orderFull->getOrderId().'&user_id='.$profile->getProfileId().'" class="ifancybox666 btn" style="background-image: none; background-color: #6901A0; color: #FFF; font-family: Arial; font-size: 15px;" id="btnFactura">Solicitar factura</a>
                                    </div>-->
                                </div>
                                <br/>
                                <br/>
                            </div>
                        </div>';
                        break;
                }
                echo $html;
            }else{
                echo "<br /><h3>" . $att[$lang] . "</h3><br />".$no_pay[$lang]."    ";
            }?>
        </div>
    </div>
    <div style="clear:both;"></div>
</div>
<!--<input id="enviado" name="enviado" val="" type="text"/>-->
<div id="tester"></div>
<? } 

/*$GLOBALS["scripts"]  .= "
    <script>
    add_event('orderPayed');</script>";*/

include("includes/footer.php");?>

<script type="text/javascript">
    /*$(document).ready(function(){
        // Envío de evento para google analytics
        ga('send', 'event', 'Checkout', 'Compra', 'Campaña PrintProyect','0');
    });*/

    

    var closedFromIframe = false;
    function customClose() {
        closedFromIframe = true;
        jQuery.fancybox.close();
    }

    $(".ifancybox666").fancybox({
         width : '400px',
         height : '720px',
         autoSize: false,
         autoScale : false,
         transitionIn : 'none',
         transitionOut : 'none',       
         type : 'iframe',
         scrolling: 'no',
         'padding' : 0,
         beforeClose : function () {
            if (closedFromIframe) {
                $("#btnFactura").attr('disabled',"true");
            }
         },
         afterClose : function() {
            closedFromIframe = false;
            return;
        }         
     });
</script>
<?php 
unset($_SESSION['Auth']);
unset($_SESSION['cupon_descuento']);
unset($_SESSION['valor_descuento']);
eCommerce_SDO_Cart::DestroyActualCart();
?>