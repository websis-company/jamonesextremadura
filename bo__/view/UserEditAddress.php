<?php
include_once ( "header.php" );
//include('menu_customer.php');

function dataToInput( $value ){
	return htmlentities( $value, ENT_QUOTES, 'UTF-8' );
}

$errors = $this->errors;
$address = $this->address;
//$address = new eCommerce_Entity_User_Address();

$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
	if ($actualLanguage == 'EN')
	{
		$lang = 0;
	}
	else
	{
		$lang = 1;
	}
	
	$edt_add[0] = "Edit Address";
	$edt_add[1] = "Editar Direcci&oacute;n";
	
	$title1[0] = "EAddress Information";
	$title1[1] = "Informaci&oacute;n de Direcci&oacute;n";
	
	$street[0] = "Street";
	$street[1] = "Calle";
	
	$street1[0] = "Colonia";
	$street1[1] = "Colonia";
	
	$city[0] = "City";
	$city[1] = "Ciudad";
	
	$estado[0] = "State";
	$estado[1] = "Estado";
	
	$pais[0] = "Country";
	$pais[1] = "Pa&iacute;s";
	
	$zip[0] = "Zip Code";
	$zip[1] = "C&oacute;digo Postal";
	
	$save[0] = "Save";
	$save[1] = "Guardar";
	
?>
<link href="<?=ABS_HTTP_URL?>css/form.css" rel="stylesheet" type="text/css" />
<div class="container">
<section id="main" class="categoria">
<div class="row">
			<div class="row">
				<div class="col-xs-3 col-pc-30">
                <?php include "userInfoSidebar.php";?>
                
				</div>
                
                <div class="col-xs-9 col-pc-70">
                <div  class="row" id="product-list-wrapper" style="padding-right:0; margin:10px 0 25px;">
<form name="frmUser" id="frmUser" method="post" action="user.php" onsubmit="">

  <div class="error">
		<?php echo $errors->getDescription(); ?>
  </div>
	
	<fieldset>
		<legend><?php echo $title1[$lang]; ?></legend>
		
		<dl class="form">
			<dt><?php echo $street[$lang]; ?>:*</dt>
			<dd>
				<input type="text" size="50" maxlength="100" class="form-control"
				       name="entity[street]" id="street"
				       value="<?php echo dataToInput( $address->getStreet()  ) ?>" >
			</dd>
			<?php $errors->getHtmlError("street"); ?>
            
             <dt><?php echo "Número Exterior" ?> : </dt>
			<dd>
				<input type="text" size="50" maxlength="100" class="form-control"
				       name="entity[street3]" id="street3"
				       value="<?php echo dataToInput( $address->getStreet3()  ) ?>" >
			</dd>
			<?php $errors->getHtmlError("street3"); ?>
           	<dt><?php echo "Número Interior" ?> : </dt>
			<dd>
				<input type="text" size="50" maxlength="100" class="form-control"
				       name="entity[street4]" id="street4"
				       value="<?php echo dataToInput( $address->getStreet4()  ) ?>" >
			</dd>
			<?php $errors->getHtmlError("street4"); ?>
			
			<dt><?php echo $street1[$lang]; ?>: </dt>
			<dd>
				<input type="text" size="50" maxlength="100" class="form-control"
				       name="entity[street2]" id="street2"
				       value="<?php echo dataToInput( $address->getStreet2()  ) ?>" >
			</dd>
			<?php $errors->getHtmlError("street2"); ?>
			
			<!-- <dt><?php echo $street[$lang]; ?> 3: </dt>
			<dd>
				<input type="text" size="50" maxlength="100" class="form-control"
				       name="entity[street3]" id="street3"
				       value="<?php echo dataToInput( $address->getStreet3()  ) ?>" >
			</dd>
			<?php $errors->getHtmlError("street3"); ?>
			
			<dt><?php echo $street[$lang]; ?> 4: </dt>
			<dd>
				<input type="text" size="50" maxlength="100" class="form-control"
				       name="entity[street4]" id="street4"
				       value="<?php echo dataToInput( $address->getStreet4()  ) ?>" >
			</dd>
			<?php $errors->getHtmlError("street4"); ?> -->
			
			<dt><?php echo $zip[$lang]; ?>: *</dt>
			<dd>
				<input type="text" size="5" maxlength="5" class="form-control"
				       name="entity[zip_code]" id="zip_code"
				       value="<?php echo dataToInput( $address->getZipCode()  ) ?>" >
			</dd>
			<?php $errors->getHtmlError("zip_code"); ?>
			
			
			
			<dt><?php echo $pais[$lang]; ?>: *</dt>
			<dd>
				<?php
				echo eCommerce_SDO_Core_Application_CountryManager::GetHTMLSelect( 'entity[country]', $address->getCountry(), 'country', 'form-control' , 'onChange="_refreshSTATE( this.value )"');
				?>
			</dd>
			<?php $errors->getHtmlError("country"); ?>
			
			<dt><?php echo $estado[$lang]; ?>: *</dt>
			<dd>
				<?php
				$states = eCommerce_SDO_CountryManager::GetStatesOfCountry( $address->getCountry(), true, 'entity[state]', $address->getState(),'state','form-control', 'style="height:35px"', true,'' );			
				echo ( !is_array($states)) ? $states : $states["div"];
				?>
			</dd>
			<?php $errors->getHtmlError("state"); ?>
            <dt><?php echo $city[$lang]; ?>: *</dt>
			<dd>
             <div id="shipCity">
				<input type="text" size="30" maxlength="30" class="form-control"
				       name="entity[city]" id="city"
				       value="<?php echo dataToInput( $address->getCity()  ) ?>" >
                       </div>
			</dd>
			<?php $errors->getHtmlError("city"); ?>
			
		</dl>
		
	</fieldset>
	<input type="hidden" name="entity[profile_id]" value="<?php echo $address->getProfileId() ?>">
	<input type="hidden" name="cmd" value="saveAddress" >
	
	<input type="submit" value="<?=$save[$lang]?>" class="btn btn-primary" style="line-height:20px; font-size:12px">
    <input  type="button" value="Cancelar" class="btn btn-primary" style="line-height:20px; font-size:12px" onclick="window.location.href='user_info.php'">
</form>
 </div>
                
                
                </div>
</div>
</div>		
</section>
</div>
  <script type="text/javascript">
  $(document).ready(function(){
	  $("#state").change(function(){
			var estado = $("#state").val();
			msgdataShip = $("#shipCity");
			if(estado != ''){
				$.ajax({
					type: "POST",
					data: "estado="+$("#state").val()+"&tipo=ship",
					url: "<?=ABS_HTTP_URL?>ciudades.php?estado="+$("#state").val()+"tipo=ship",
					beforeSend: function(){								
						msgdataShip.html("<img src='<?=ABS_HTTP_URL?>img/layout/loading.gif' alt='Cargando' title='Cargando' width='30' />");
					},
					success: function(result){					 
						 msgdataShip.html(result);						 
					}			
				});
				return false;
			}
		});
  });
		  </script>
<?php
include_once ( "footer.php" );
die;
?>