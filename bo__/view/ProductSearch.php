<?php
require_once("header_cart.php");

$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
$spanishLanguage = eCommerce_SDO_LanguageManager::GetValidLanguage('SP');
$spanishLanguage = $spanishLanguage["id"];


$result	  = $this->result;
$products = $result->getResults();

$catNum = $this->catNum;
$category = $this->category;
$artist = $this->artist;

$categoryName = ($category->getImageId() != 0) ? '<img src="file.php?id=' . $category->getImageId() . '">' : $category->getName();

$querySearch = ( $result instanceof eCommerce_Entity_Search_Result_Product ) ? $result->getKeywords() : '';
$querySearch = empty($querySearch) ? '' : $querySearch;
//-------------------------------------------------------------
//-------------------------------------------------------------
		
	$x = count($products);
	if($x> 0){
		$i = 0;
		foreach( $products as $product ){
			echo "<a href='?cmd=details&id=" . $product->getProductId() . "'>";
			debug($product->getProductId());
			echo "</a>";			
		}
		?>
		<p align="right" style="margin-right: 25px; margin-top: 8px"> 
			<?php eCommerce_SDO_Core_Application_Form::foPrintPager( $result,'','','',5,array('idCat'=> $category->getCategoryId() ),'' ); ?>
		</p>

		<form action='<?=ABS_PATH_HTTP?>productos/' name='frmProduct' style="margin:0px;">
		<input type='hidden' name='p' />
		<input type='hidden' name='o' />
		<input type='hidden' name='q' value="<?php echo empty($querySearch) ? '' : $querySearch; ?>" />
		<input type='hidden' name='idCat' value='<?php echo $category->getCategoryId(); ?>' />
		</form>
	
		<script language="javascript"> 
			function setParam( formName, fieldName, value, submit ){   
				if ( submit == null ) {     submit = true;   }   
				var form = document.forms[formName];   
				form.elements[fieldName].value = value;   
				if ( submit ) {   	form.submit();   } 
			}
		</script>
	<?php
	}else{
		echo ( ($actualLanguage == $spanishLanguage) ? "No hay resultados" : "There are not results to display");
	}
	?>
<?php
include_once("footer_cart.php");
?>