<?php
$products = $this->result->getResults();
$category = $this->category;
$brand = $this->brand;

$baseFriendlyUrl = ABS_HTTP_URL . 'producto/';
$baseListFriendlyUrl = ABS_HTTP_URL . 'productos/'; 

$productPathUrl = $baseFriendlyUrl;
$pagingPathUrl = $baseListFriendlyUrl;

$header_title = $this->trans('product').'s';
$header_description = '';
$header_keywords = '';
$categoria_id =$category->getCategoryId(); 
if(!empty($categoria_id)) {
	$header_title .= ' | ';
	$header_title_path = array();
	$productPathUrl .= $category->getCategoryId().'/';
	$pagingPathUrl .= $category->getCategoryId().'/';
	foreach($this->categoriesTree as $c) {
		$name = trim($c->getName());
		$header_title_path[] = $name;
		$friendlyName = Util_FriendlyURL::formatText($name);
		$productPathUrl .= $friendlyName . '/';
		$pagingPathUrl .= $friendlyName . '/';
	}
	$header_title .= implode(' - ', $header_title_path);
	$header_description .= '';
} else if(!empty($brand)) {
	$name = trim($brand->getName());
	$header_title .= ' | '.$this->trans('brand').'s '.$name;
	$friendlyName = Util_FriendlyURL::formatText($name);
	$productPathUrl .= $brand->getBrandId().'/'.strtolower($brand->getName()).'-'.$friendlyName.'/';
	$pagingPathUrl .= $brand->getBrandId().'/'.strtolower($brand->getName()).'-'.$friendlyName.'/';
}

$header_description .= '.';

require_once 'header.php';
?>
<script type="text/javascript" src="<?=ABS_HTTP_URL?>js/jquery-1.6.2.min.js"></script>


<script type="text/javascript">

	function showProductDetail(){
		hideCarrito();
		$("div#show_efect_detail").fadeIn("500");
	}
	function hideEfectDetail(){
		$("div#show_efect_detail").fadeOut("slow");
	}

	function addProduct(quantity,idProduct){
		if(quantity>1){
			cantidad=document.getElementById("producto"+idProduct).value; 
			if(cantidad<=0)
				cantidad=1
		}
		else
			cantidad=1
		//idProduct = document.cosa.productId.value;
		$("div#content_cart").slideDown("slow");
		var jqxhr = $.ajax({ url: "<?=ABS_HTTP_URL?>addcart.php?id_product="+idProduct+"&quantity="+cantidad,  
			async: false,
			beforeSend:function(){cargando();}
			})
		.success(function(data) { respuesta(data); })
	    .complete(function() { showCarrito(); });
	}

	function cargando(){
		var a = '<div id="content_cart"  style="width:30px;margin:30px 540px;position:absolute;	z-index:1000;background:#000000;border: 2px solid #FFFFFF;"><img src="<?=ABS_HTTP_URL?>ima/indicator_circle_ball.gif"></div>';
		$('div#show_add_cart_xajax').html(a);
	}

	function showCarrito(){
		$("div#content_cart").slideDown("slow");
	}

	function respuesta(resp){
	    $('div#show_add_cart_xajax').html(resp);
	}
	
	function hideCarrito(){
		$("div#content_cart").fadeOut("slow");
	}
	function goCarrito(){
		parent.document.location.href="<?=ABS_HTTP_URL?>cart.php";
	}


</script>
 <div id="show_add_cart_xajax" style="height:0px; position:absolute; margin-left:-200Px;">
<div id="content_cart" class="miniCarrito" style="width:30px"><img src='<?=ABS_HTTP_URL?>ima/indicator_circle_ball.gif'></div>
</div>
<div id="contenido" style="background-color:#FFF;">
<? include "buscador.php";?>

<div class="pie1">
		<? include "categorias.php";?>

   <div class="derecha" style="width:720px;">
			<img src="<?=ABS_HTTP_URL?>ima/home/int/empresa/empresa.png" alt="" width="66" height="13" />
			<p><img src="<?=ABS_HTTP_URL?>ima/int/shopping/shoping-tit.png" width="84" height="13" /></p>
			<p>&nbsp;</p>
					<?php if(count($products)>0)?>
					  <?php $cont=1;
			            	foreach($products as $product){			            	
			            		$id				= $product->getProductId();
								$nombre 		= $product->getName();
								$descripcion	= Util_String::subText($product->getDescriptionShort(),97 -strlen($nombre),'chars');
								$href			= $product->getFriendlyNameUrl();
								$src			= $product->getUrlImageId('medium',0);
								$alt			= $nombre;
								$precio			= $product->getPrice();					
			           ?>
                     <div class="item">
                     <a href="<?=$href?>"><img src="<?=$src?>" width="160" height="231" alt="<?=$alt?>" title="<?=$alt?>" border="0" /></a>
                      <p><?=$nombre?></p>
                      <p>$<?=$precio?></p>
                      <p><img src="<?=ABS_HTTP_URL?>ima/int/btn/nuevo.png" width="51" height="26" alt="<?=$alt?>" title="<?=$alt?>" /></p>
                     </div><!--/item -->
					<?php }?>
                   

                     <div class="paginador" align="right" style="float:right">
                      <?php eCommerce_SDO_Core_Application_Form::foPrintPager( $this->result, '', 'gris11', 'pag-actual', 'pag-actual', 5, array(), '', $pagingPathUrl."{p}/"  ); ?>
                    </div><!--/paginador -->
                                         
		</div><!--derecha -->
	</div>
</div>
<?php
require_once 'footer.php';
?>