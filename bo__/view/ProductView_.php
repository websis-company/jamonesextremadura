<?php
$product            = $this->product;
$precios            = $this->precios;
$currency           = $this->currency;
$tallas             = $this->tallas;
$colores            = $this->colores;
$Versiones          = $this->Versiones;
$VersionesAgrupadas = $this->VersionesAgrupadas;
$Categoria          = $this->Categoria;


//$maximo  = "$".eCommerce_SDO_Core_Application_CurrencyManager::numberFormat(max($precios));
//$minimo  = "$".eCommerce_SDO_Core_Application_CurrencyManager::numberFormat(min($precios));
//$brand    = $this->brand;
//$Tallas = $this->Tallas;
$baseFriendlyUrl     = ABS_HTTP_URL . 'producto/';
$baseListFriendlyUrl = ABS_HTTP_URL . 'productos/';
$skuProd             = trim($product->getSku());
$descrProd           = nl2br(trim($product->getDescriptionShort()));
$nameProd            = $product->getName();
$header_title        = !empty($nameProd) ? $nameProd : $skuProd;
$header_description  = Util_String::subText(trim($product->getDescriptionShort()), 200, 'chars', '');
$header_keywords     = '';

/*if(!empty($category)) {
	$header_title      .= ' | ';
	$header_title_path = array();
	$productPathUrl    .= $category->getCategoryId().'/';
	$pagingPathUrl     .= $category->getCategoryId().'/';
	foreach($this->categoriesTree as $c) {
		$name                = trim($c->getName());
		$header_title_path[] = $name;
		$friendlyName        = Util_FriendlyURL::formatText($name);
		$productPathUrl      .= $friendlyName . '/';
		$pagingPathUrl       .= $friendlyName . '/';
	}
	$header_title .= implode(' - ', $header_title_path);
} else if(!empty($brand)) {
	$name           = trim($brand->getName());
	$header_title   .= ' | '.$name;
	$friendlyName   = Util_FriendlyURL::formatText($name);
	$productPathUrl .= $brand->getBrandId().'/'.strtolower($brand->getName()).'-'.$friendlyName.'/';
	$pagingPathUrl  .= $brand->getBrandId().'/'.strtolower($brand->getName()).'-'.$friendlyName.'/';
}*/

include("header.php");
$descripcion_short   = nl2br($product->getDescriptionShort());
$descripcion_long    = nl2br($product->getDescriptionLong());
$precio              = $precios;
$src                 = $product->getUrlImageId('medium',0);
$src_0               = $product->getUrlImageId('large',0);
$src_small           = $product->getUrlImageId('small',0);
$imas                = $product->getArrayImages();
$produc_relacionados =  $product->getProductosRelacionados();
?>

<section class="int productos detalle">
	<div class="container">
		<div class="row">
			<div  class="col-xs-3 col-lg-2" id="sidebar">
				<?php include("accordion.php"); ?>
			</div>

			<div class="col-xs-9 col-lg-10">
				<div class="toolbar">
					<div class="row">
						<div class="col-xs-12">
							<ol class="breadcrumb">
								<li><a href="<?=ABS_HTTP_URL?>"><span class="glyphicon glyphicon-home"></span></a></li>							
								<?php
								if(!empty($Categoria)){
								?>
								<li><a href="<?=$Categoria->getFriendlyNameUrl();?>"><?php echo $Categoria->getName();?></a></li>
								<?php
								}//end if
								?>
								<li class="active"><?php echo $nameProd;?></li>
							</ol>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-7">
						<div style="background-image:url(<?=$src_0;?>);" class="img-thumbnail img-detalle"></div>
						<div class="row">
							<?php 
							$thumbnail = $product->getUrlImageId('small');
							foreach ($thumbnail as $res) {
							?>
								<div class="col-xs-4">
									<a class="thumbnail yellow"><span style="background-image:url(<?=$res;?>);" class="img-thumbnail"></span></a>
								</div>
							<?php
							}//end foreach
							?>
						</div>
					</div>

					<div class="col-xs-5">
						<div class="box rosita">
							<h1 class="h1"><?php echo $nameProd; ?></h1>
							<p class="sku">SKU <?php echo $skuProd;?></p>
							<p class="precio">$<?php echo number_format($precio,2,'.',',')." ".$currency;?></p>
							<div class="descripcion"> 
								<p>
									<?php echo $descripcion_short;?>
									<br><br>
									<?php echo $descripcion_long;?>
								</p>
							</div>

							<form class="form form-inline">
								<div class="form-group">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<?php
										if(!empty($tallas)){
										?>
										<tr>
											<td width="30%"><label>Tallas :</label></td>
											<td>
												<select id="talla" name="Talla" class="form-control" style="width: auto; margin-left:10px;">
													<option value="">Seleccione la Talla</option>
													<?php 
													foreach ($tallas as $talla_id => $nom_talla) {
													?>
													<option value="<?=$talla_id;?>"><?php echo $nom_talla;?></option>
													<?php
													}//end foreach
													?>
												</select>
											</td>
										</tr>
										<?php 
										}//end if
										?>
										<tr>
											<td colspan="2">&nbsp;</td>
										</tr>
										<tr>
											<td><label>Cantidad :</label></td>
											<td><input type="number" min="1" class="form-control" style="width:60px; margin-left:10px;" /></td>
										</tr>
									</table>
								</div>
							</form>
							<a href="#" class="btn btn-success btn-block">Agregar al carrito</a>
						</div>
						<div class="box rosita2">
							<img class="img-responsive" src="<?=ABS_HTTP_URL?>img/layout/share.png" width="271" height="20" alt="share" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="container"> <hr class="hr" /> </div>

<?php 
if(!empty($produc_relacionados)){
?>
<section class="relacionados">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<!--toolbar -->
				<div class="toolbar">
					<h2 class="h2 text-center">Tal vez necesites con tu compra</h2>
				</div>
				<!--toolbar -->
				<div class="row">
					<?php 
					$relacionados = explode(',', $produc_relacionados);
					foreach ($relacionados as $producto_id) {
						$product_rel            = eCommerce_SDO_Catalog::LoadProduct($producto_id);
						$thumbnail_rel          = $product_rel->getUrlImageId('small',0);
						$prod_nom_rel           = $product_rel->getName();
						$VersionesAgrupadas_rel = eCommerce_SDO_Catalog::GetProductVersiones($producto_id);
						$prod_pre_rel           = $VersionesAgrupadas_rel[0]['price'];
						$prod_currency_rel      = $VersionesAgrupadas_rel[0]['currency'];
					?>
					<div class="col-xs-2">
						<a class="thumbnail" href="#">
							<div style="background-image:url(<?php echo $thumbnail_rel;?>);" class="img-thumbnail"></div>
							<div class="caption">
								<h4 class="h4" style="height: 45px;"><?php echo $prod_nom_rel;?></h4>
								<p>$ <?php echo number_format($prod_pre_rel,2,'.',',')." ".$prod_currency_rel;?></p>
								<p class="btn btn-primary">Agregar al carrito</p>
							</div>
						</a>
					</div>
					<?php
					}//end foreach
					?>				
				</div>
			</div>
		</div>
	</div>
</section>
<?php 
}//end if
?>



<?php include('footer.php');?>