<?php 
//require_once("bo/menu_cart.php");
$header_title = 'Env&iacute;o y Facturaci&oacute;n';
include "header.php";

$cart = $this->cart;
$message = $this->message;
$messageClass = $this->messageClass;
$isEmptyCart = $this->isEmptyCart;


$billTo = $cart->getOrderAddressByType( eCommerce_SDO_Cart::ADDRESS_BILL_TO );
$errors = $this->errors;

$shipTo = $cart->getOrderAddressByType( eCommerce_SDO_Cart::ADDRESS_SHIP_TO );

$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
$spanishLanguage = eCommerce_SDO_LanguageManager::GetValidLanguage('SP');
$spanishLanguage = $spanishLanguage["id"];

if ($actualLanguage == 'EN'){
	$lang = 0;
}
else{
	$lang = 1;
}

$edit_cart[0] = "Edit Cart";
$edit_cart[1] = "Editar Carrito";

$bill_address[0] = "Bill Address";
$bill_address[1] = "Direcci&oacute;n de Facturaci&oacute;n";

$ship_address[0] = "Ship Address";
$ship_address[1] = "Direcci&oacute;n de Env&iacute;o";

$street[0] = "Street";
$street[1] = "Calle";

$ciudad[0] = "City";
$ciudad[1] = "Ciudad";

$estado[0] = "State";
$estado[1] = "Estado";

$pais[0] = "Country";
$pais[1] = "Pa&iacute;s";

$zip[0] = "Zip Code";
$zip[1] = "C&oacute;digo Postal";

$continue[0] = "Confirm Order";
$continue[1] = "Confirmar Orden";

$edit_bill[0] = "Edit Bill Adress";
$edit_bill[1] = "Editar Direcci&oacute;n de Facturaci&oacute;n";

$edit_ship[0] = "Edit ship Adress";
$edit_ship[1] = "Editar Direcci&oacute;n de Env&iacute;o";

$pur_3[0] = "Purchase 3 - Confirm Order";
$pur_3[1] = "Paso 3 - Confirmación de Órden";

$method[0] = "Payment Method";
$method[1] = "M&eacute;todo de Pago";

$comments[0] = "Comments";
$comments[1] = "Comentarios";

if( !$isEmptyCart ){

?>
 <div class="container" style="margin-top:30px">
	<div class="row" style="margin-left:0px">
    	<section id="content" >
    		 <article id="productos-relacionados" >
<!-- <div style="width:100%; text-align:left; padding-left:5px;padding-top:15px;" class="subtitle"><?php echo $pur_3[$lang]; ?></div>-->
<?php $class='class3';
include_once "menu_pasos.php";?>
<link href="<?=ABS_HTTP_URL?>css/form.css" rel="stylesheet" type="text/css" />

<div style="width:100%; padding-left:10px; padding-top:10px;">
<!-- CART -->
<?php

echo '<div class="success">' . $this->messageSuccess . '</div>';
echo '<div class="error">' . $this->messageErr . '</div>';
if($this->mensaje!=""){
	echo '<div class="error" style="font-size:24px;">' . $this->mensaje . '. Si lo desea intente el pago nuevamente</div>';
}

$html = eCommerce_SDO_Cart::GetHTML(false, false, true);
echo $html; 

?>
<!-- CART -->
<div class="edit-car" align="right">
&nbsp; &nbsp;<a href='cart.php' class="ACart"><?php echo $edit_cart[$lang]; ?></a>
</div>
</div>

<div style="padding-left: 15px; padding-top:25px">

<fieldset>
		<legend><?php echo $bill_address[$lang]; ?></legend>
		<dl class="form">
			<dt><?php echo $street[$lang]; ?></dt>
			<dd><?php echo $billTo->getStreet().", ".$billTo->getStreet2().", ".$billTo->getStreet3(); ?></dd>
			<!--<dt><?php echo $ciudad[$lang]; ?></dt>
			<dd><?php echo $billTo->getCity(); ?></dd>-->
			<dt><?php echo $estado[$lang]; ?></dt>
			<dd>
			<?php $state = eCommerce_SDO_CountryManager::GetStateById( $billTo->getState(), $billTo->getCountry() ); 
			echo $state["name"].",".$billTo->getState();
			?>
			</dd>
			<dt><?php echo $pais[$lang]; ?></dt>
			<dd><?php $country = eCommerce_SDO_CountryManager::GetCountryById($billTo->getCountry()); 
			echo $country["name"];
			?></dd>
			<dt><?php echo $zip[$lang]; ?></dt>
			<dd><?php echo $billTo->getZipCode(); ?></dd>
            <br />
			<dt style="width:20em"> &nbsp; &nbsp;<a href='purchase.php?nextStep=2' class="ACart"><?php echo $edit_ship[$lang] ?></a></dt>	
		</dl>
	</fieldset>
</div>	
	

<fieldset>
		<legend><?php echo $ship_address[$lang]; ?></legend>
		<dl class="form">
			<dt><?php echo $street[$lang]; ?></dt> <dd><?php echo $shipTo->getStreet().", ".$shipTo->getStreet2().", ".$shipTo->getStreet3(); ; ?></dd>
			<!--<dt><?php echo $ciudad[$lang]; ?></dt> <dd><?php echo $shipTo->getCity(); ?></dd>-->
			<dt><?php echo $estado[$lang]; ?></dt> <dd>
			<?php $state = eCommerce_SDO_CountryManager::GetStateById( $shipTo->getState(), $shipTo->getCountry() ); 
			echo $state["name"].",".$shipTo->getState();
			?></dd>
			<dt><?php echo $pais[$lang]; ?></dt> <dd>
			<?php $country = eCommerce_SDO_CountryManager::GetCountryById($shipTo->getCountry()); 
			echo $country["name"];
			?>
			</dd>
			<dt><?php echo $zip[$lang]; ?></dt> <dd><?php echo $shipTo->getZipCode(); ?></dd>
            <br />
			<dt style="width:20em"> &nbsp; &nbsp;<a href='purchase.php?nextStep=1' class="ACart"><?php echo $edit_bill[$lang]; ?></a></dt>
		</dl>
	</fieldset>
		
<br />
<div style="width: 400px;" >
<legend style="color: #000; float:left"><?php echo $method[$lang] ?>:</legend>
<form action='' method='POST' style="float:left" >

<br />
<?php
$paymentMethods = eCommerce_SDO_Order::GetArrPaymentMethods();

foreach( $paymentMethods as $key => $paymentMethod ){ 
	echo "<input type='radio' name='paymentMethod' value='{$key}' checked='checked'>{$paymentMethod}<br />";
}
?>
<br /><br />
<b><?php echo $comments[$lang] ?></b><br /><textarea name="comentarios" id="comentarios" cols='50' rows='3'><?=$this->comentarios?></textarea><br /><br />
<input type='hidden' name='nextStep' value='4' />
<input type='submit' name='boton' value='<?php echo $continue[$lang] ?>' >
</form>
</div>	
	



</article>
      <!--productos-relacionados --> 
      
    </section>
    <!--content --> 
    
  </div>
  <!-- Row --> 
  
</div>
<!-- /container -->
<?php
}
?>
<?php
include_once( "footer.php" );
?>