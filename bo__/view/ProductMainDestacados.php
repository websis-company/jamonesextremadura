<?php 
$products            = $this->result->getResults();
foreach ($products as $product) {
	$id				= $product->getProductId();
	$categorias 	= eCommerce_SDO_Catalog::GetCategoriesByProduct($id);
	foreach($categorias as $categoria){
		$parent = $categoria->getParentCategoryId();		
		$category_id = $categoria->getCategoryId();	
	}
	
	$nombre 		= $product->getName();
	$descripcion	= Util_String::subText($product->getDescriptionShort(),97 -strlen($nombre),'chars');
	$href			= $product->getFriendlyNameUrl($category_id);
	$src			= $product->getUrlImageId('medium',0);								
	$alt			= $nombre;
	$precio			= $product->getPrecio();
	
?>
	<div class="col-xs-2" align="center">
		<a class="thumbnail" href="<?=$href?>"><!--height: 170px;-->
			<div style="background-image:url('<?=$src;?>'); width:170px; background-position:10px center" class="img-thumbnail" align="center"></div>
			<div class="caption">
				<div style="height: 56px;">
					<h4 class="h4">
						<?=$nombre?>
					</h4>
				</div>
				<p><?php echo '$ '.number_format($precio,2,'.',',').' MXN';?></p>
				<p class="btn btn-primary" >Comprar</p>
			</div>
		</a>
	</div>
<?php
}
?>

