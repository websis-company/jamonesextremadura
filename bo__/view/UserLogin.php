<?php
$title_header='Accesa a tu Cuenta. ¿No Tienes Cuenta? Registrate.';
require_once( "bo/common.php" );

include('includes/head.php'); 
?>
<?php include('includes/header.php');
//include('menu_customer.php');
if(isset($_GET['username'])){
	$username = $_GET['username'];
}
?>

<style type="text/css">
	.error{color: #E03030; font-size: 15px; display: block;}
	input.error, input.error:focus, input.check.error{border: 2px solid #E03030;}	
	select.error, select.error:focus, select.check.error{border: 2px solid #E03030;}
</style>


<div class="c-layout-page page-inside">
	<section class="c-content-box c-size-md c-overflow-hide c-bg-blanco">
		<div class="container">
			<div class="row">
				<div class="col-xs-12" align="center">
					<div>
						<h1 class="h4" style="font-weight: normal; color: #000000; font-size: 35px;">Ingresa tu mail para continuar la compra</h1>
					</div>
					<br/>
					<form id="formUserLogin" name="formUserLogin" action="" method="post">
			            <?php 
			            if($this->message != 'Acceso Restringido'){
			                echo  $this->message;
			            }
			            ?>
						<div align="center">
							<input type="email" name="username" id="username" class="form-control email" value="<?php echo $username;?>" placeholder="su@correo.com" style="color: #777777; width: 60%; height: 55px; text-align: center;">
						</div>
						<input type='hidden' name='cmd' value='login' />
                        <input type="hidden" name="nopass" value="nopass" />
						<div class="clearfix"></div>
						<div style="margin-top: 15px;">
							<button type="submit" class="btn btn-primary" style="width: 200px; height: 45px;">Continuar</button>
						</div>
					</form>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="row" align="center" style="margin-top: 25px;">
				<div style="" class="h4">
					<p>
					Al ingresar tu correo electrónico, crearemos un perfil con tu historial para facilitar y asistirte en tus futuras compras, así como recibir notificaciones de los estados de envío de los productos adquiridos. <br><br>
					Todos los datos ingresados serán guardados de forma 100% segura.
					</p>
				</div>
				<!--<div style="width: 60%;" align="left">
					<ul>
						<li>Identificar su perfil</li>
						<li>Notificar sobre los estados de su compra</li>
						<li>Guardar el historial de compra</li>
						<li>Facilitar el proceso de compras</li>
					</ul>					
				</div>-->
			</div>
		</div>
  	</section>
  	<div class="c-content-box c-size-sm c-bg-sexto">
		<div class="container">
			<?php include('boletin-home.php'); ?>
		</div>
	</div>
  <!--/teaser3 -->
 </div>


<?php
$GLOBALS["scripts"] .= "
<script type=\"text/javascript\">
	$(document).ready(function(e){
		$(\"#formUserLogin\").validate({
			debug: false,
			rules:{
				username:{required: true, email: true},
			},
			messages:{
				username:{required: 'Este campo es obligatorio.', email:'Introduzca un correo electrónico válido, por favor.'},
			},
			submitHandler: function(form){
				document.getElementById(\"formUserLogin\").submit();
			},
		});
	});
</script>
";
?>
<?php include("includes/footer.php");?>

