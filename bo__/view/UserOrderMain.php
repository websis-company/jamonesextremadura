<?php
$products = $this->options->getResults();
$data = array();
foreach( $products as $product ){
	$data[] = get_object_vars( $product );
}
		

function renameStatus( $status ){
	return eCommerce_SDO_Order::GetStatusById( $status );
}

function renamePaymentMethod( $payment ){
	return eCommerce_SDO_Order::GetPaymentMethodById( $payment );
}

function renameProfileId( $profileId ){
	$profile = eCommerce_SDO_User::LoadUserProfile( $profileId );
	return "<a href='user.php?cmd=edit&id=". $profile->getProfileId(). "'>" . $profile->getEmail() . "</a>";
	
}

function renameOrder($orderId){
	$order = eCommerce_SDO_Order::LoadFullById( $orderId );
	$cantidad = "Orden No. ".$orderId." - $".number_format($order->getTotal(), 2, ',', ' ')." MXN";
	return $cantidad;
}

$listPanel = new GUI_ListPanel_ListPanel( "Order" );
$listPanel->setData( $data );
//VALIDAD IDIOMA
$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
if ($actualLanguage != 'EN')
{
$listPanel->addColumnNameOverride( "order_id", "&Oacute;rden" );
$listPanel->addColumnNameOverride( "payment_method", "M&eacute;todo de Pago" );
$listPanel->addColumnNameOverride( "status", "Estado" );
$listPanel->addColumnNameOverride( "creation_date", "Fecha de Creaci&oacute;n" );
$listPanel->addColumnNameOverride( "currency", "Moneda" );
}
$listPanel->addCallBack( "status","renameStatus" );
$listPanel->addCallBack( "payment_method","renamePaymentMethod" );
$listPanel->addCallBack( "profile_id","renameProfileId" );
$listPanel->addCallBack( "order_id","renameOrder" );

$hiddenColumns = array( "comments", "modification_date", "profile_id","language", "requiere_factura", "tipo_cargo", "folio","tipo_tarjeta","currency");

foreach( $hiddenColumns as $col ){
	$listPanel->addHiddenColumn( $col );
}


$listPanel->addHiddenField( "cmd", "listOrders" );


$listPanel = new GUI_ListPanel_Decorator_Sorter( $listPanel );
$listPanel->setOrderBy( $this->options->getOrderBy() );

$listPanel = new GUI_ListPanel_Decorator_RowActions( $listPanel );

// Modify
$actModify = new GUI_ListPanel_Action( '?cmd=viewOrder&id={$order_id}', ABS_HTTP_URL . 'img/consultar.png' );
if ($actualLanguage == 'EN')
{
	$actModify->setImageTitle( 'View Order' );
}
else
{
	$actModify->setImageTitle( 'Ver Órden' );
}
$listPanel->addAction( $actModify );


// Delete
//$actDelete = new GUI_ListPanel_Action( '?cmd=delete&id={$order_id}', 'backoffice/img/delete.png' );
//$actDelete->setImageTitle( 'Delete Order' );
//$actDelete->setOnClickEvent( 'return confirm("Are you sure you want to delete the order \"{$order_id}\"?");' );
//$listPanel->addAction( $actDelete );

// Add "Search" capabilities
$listPanel = new GUI_ListPanel_Decorator_Searcher( $listPanel );
$listPanel->setFieldLabel( '<img src="'.DIRECTORY_BO_RELATIVE.'backoffice/img/search2_small.png">' );
$listPanel->setButtonLabel( 'Buscar' );
$listPanel->setClearButtonLabel( " Limpiar " );
$listPanel->setSearchString( $this->options->getKeywords() );

// Add paging capabilities
$listPanel = new GUI_ListPanel_Decorator_Pager( $listPanel );
$listPanel->setCurrentPage( $this->options->getPage() );

$listPanel->setTotalPages( $this->options->getTotalPages() );
$listPanel->setOffset( $this->options->getResultsPerPage() );

include_once ( "header.php" );
//include('menu_customer.php');

$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
if ($actualLanguage == 'EN'){
	$lang = 0;
}
else{
	$lang = 1;
}

$title1[0] = "My Orders";
$title1[1] = "Mis &Oacute;rdenes";

$title2[0] = "Export To Excel";
$title2[1] = "Exportar a Excel";
?>
<style>
.listPanel_ColumnName{
	color:#E41F2A;
	background-color:#CCCCCC;
	font-size:14px;
	height:30px;
	font-family:inherit;		
	}
.listPanel_PagingRow a{
	text-decoration:none !important;
	color:#FFFFFF !important;
	font-weight:bold;
	font-size:14px;	
	font-family:inherit;	
	}
.listPanel_PageOffset_Selector{
	text-decoration:none !important;
	color:#000000 !important;
	font-weight:bold;
	font-size:12px;	
	font-family:inherit;
	}
.subtitle{
	text-decoration:none !important;
	color:#FFFFFF !important;
	font-weight:bold;
	font-size:16px;	
	font-family:inherit;
		}
.listPanel_RecordCell{
	text-decoration:none !important;
	color:#000000 !important;
	font-size:12px;	
	font-family:inherit;
	}
table{background-color:#FFFFFF}	
th{text-align:center}	
</style>
<div class="container">
<section id="main" class="categoria">
<div class="row">
			<div class="row">
				<div class="col-xs-3 col-pc-30">
                <?php include "userInfoSidebar.php";?>
                
				</div>
                
                <div class="col-xs-9 col-pc-70">
                <div  class="row" id="product-list-wrapper" style="padding-right:0; margin:10px 0 25px;">
<div class="success">
	<?php if ( isset( $this->strSuccess ) ) echo $this->strSuccess; ?>
</div>

<div class="error">
	<h2><?php if ( isset( $this->strError ) ) echo $this->strError;	?></h2>
</div>
	<?php
	$listPanel->display();
	?>	
   
<div style="margin-top:25px" align="right">
 <a href='user.php' class="btn btn-primary" style="line-height:20px; font-size:12px; padding:10px">Regresar</a>
 </div>
 </div>
                
                
                </div>
</div>
</div>		
</section>
</div>


<?php
include_once ( "footer.php" );
?>