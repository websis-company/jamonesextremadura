<?php
$product            = $this->product;
$Versiones          = $this->Versiones;
$tallas             = $this->tallas;
$colores            = $this->colores;
$productosRelacionados = $product->getProductosRelacionados();

$CategoriaView          = $this->Categoria;
$categoryId = $CategoriaView->getCategoryId();
$nom_catego = $CategoriaView->getName();
$product_id = $product->getProductId();

//$brand    = $this->brand;
//$Tallas = $this->Tallas;
$baseFriendlyUrl     = ABS_HTTP_URL . 'producto/';
$baseListFriendlyUrl = ABS_HTTP_URL . 'productos/';
$skuProd             = trim($product->getSku());
$descrProd           = utf8_encode(nl2br(trim($product->getDescriptionShort())));
$nameProd            = $product->getName();
$header_title        = !empty($nameProd) ? $nameProd : $skuProd;
$header_description  = Util_String::subText(trim($product->getDescriptionShort()), 200, 'chars', '');
$header_keywords     = '';

$title_header = 'TecnoCompra | Producto | '.$nom_catego.' | '.$nameProd;
include("header.php");
$descripcion_short   = utf8_encode(nl2br($product->getDescriptionShort()));
$descripcion_long    = nl2br($product->getDescriptionLong());
$especificaciones	 = nl2br($product->getEspecificaciones());
$video				 = $product->getVideo();
//$precio            = $precios;
//$precio              = $product->getPrecio();
$precio              = $product->getPrice();
$currency            = $product->getCurrency();
$src                 = $product->getUrlImageId('medium',0);
$src_0               = $product->getUrlImageId('large',0);
$src_small           = $product->getUrlImageId('small',0);
$imas                = $product->getArrayImages();
$produc_relacionados = $product->getProductosRelacionados();
foreach($Versiones as $Version){
	$imagenesVersion[] = $Version['imagen'];
	$arrayImagesVersion[] = $Version['array_images'];
	$VersionesId[] = $Version['version_id'];
}
if(empty($tallas)){
		$versionProduct = $Versiones[0]['version_id'];
		$precio = $Versiones[0]['price'];
	}else{
		$versionProduct = '';
		$precio = '';
}//end if																	
$tallasVersion = eCommerce_SDO_Core_Application_Version::GetTallasByProductId($product_id);
$coloresVersion = eCommerce_SDO_Core_Application_Version::GetColoresByProductId($product_id);

$versionesProducto = eCommerce_SDO_Catalog::GetAllProductVersiones($product_id);				
foreach($versionesProducto as $vP){
	$precios[] = eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $vP->getPrice(), $vP->getCurrency() );										
}
if(count($precios) > 1){
	$strPrice = 'desde';					
}else{
	$strPrice = '';
}
$minimo = min($precios);
$maximo = max($precios);
$color = "#ffffff;";

?>

<!--Microdatos-->
<div itemscope itemtype="http://schema.org/IndividualProduct" itemid="#product" style="display:none">
<span itemprop="name"><?=$nameProd?></span> 
<span itemprop="description"><?=$descripcion_long?></span>
<span itemprop="image"><?=$src?></span>
<span itemprop="sku"><?=$skuProd?></span>
<span itemprop="url"><?=$product->getFriendlyNameUrl($categoryId)?></span>

<div itemprop="Brand" itemscope itemtype="http://www.schema.org/Brand">
        <span itemprop="name">TecnoCompra</span>    
        <span itemprop="url"><?=ABS_HTTP_URL?></span>    
        <span itemprop="logo"><?=ABS_HTTP_URL?>img/layout/logo.png</span>    
    </div>
</div>
<!--Microdatos-->


<style>
#plugins { display:none; }
.error{ font-size: 14px;
    color: #F00803;
    text-align: center;
    width: 100%;
}

</style>

<!-- Marcado JSON-LD generado por el Asistente para el marcado de datos estructurados de Google. -->
<script type="application/ld+json">
{
  "@context" : "http://schema.org",
  "@type" : "Product",
  "name" : "<?=$nameProd?>",
  "image" : "<?=$src?>",
  "description" : "<?=$descripcion_long?>",
  "offers" : {
    "@type" : "Offer",
    "price" : "<?=$Versiones[0]['price'];?>"
  }
}
</script>

<section id="detalle">
<div class="container">

<ol class="breadcrumb">
  <li><a href="<?=ABS_HTTP_URL?>">Inicio </a></li>
  <?php
	if(!empty($CategoriaView)){
	?>
		<li><a href="<?=$CategoriaView->getFriendlyNameUrl();?>" title="<?php echo $CategoriaView->getName();?>"><?php echo $CategoriaView->getName();?></a></li>
	<?php
	}//end if
	?> 
  <li class="active"><?php echo $nameProd;?></li>
</ol>
<div class="row">
<div id="thumbnails" class="col-md-1 col-xs-2">
	<?php $cont=0; 
		if(count($tallasVersion) <= 0 && count($coloresVersion) <= 0){			
			$arrayImages = explode(',',$arrayImagesVersion[0]);					
			$versionId = $VersionesId[0];
			foreach($arrayImages as $imgVersion){				
				if(!empty($imgVersion)){
					$VersionF = eCommerce_SDO_Core_Application_Version::LoadById($versionId);				
					$img_0 = $VersionF->getUrlImagesId('medium',$cont);
			?>
                     <div class="box">
                        <a class="thumbnail" > <img src="<?=$img_0?>" width="126" height="126" alt="<?=$nameProd?>" id="thumbImage<?=$cont?>" class="thumbCSS"></a>
                    </div>
        <?php $cont++; }
				}
		}?>
		
		<? /*foreach($imagenesVersion as $imgVersion){
			$img_0 = $product->getUrlArrayFiles($imgVersion,'medium');		
		?>
    <div class="box">
        <a class="thumbnail" > <img src="<?=$img_0[0]?>" width="126" height="126" alt="<?=$nameProd?>" id="thumbImage<?=$cont?>" class="thumbCSS"></a>
    </div>
    <?php $cont++; 
	} */?>   
    <div class="box">
        <a class="thumbnail" > <img src="<?=$src?>" width="126" height="126" alt="<?=$nameProd?>" id="thumbImage<?=$cont?>" class="thumbCSS"></a>
    </div>
</div>

<div class="col-md-5 col-xs-10">
    <div class="box thumbnail" style="position:relative;">
    	<div class="flag oferta" style="display:none" id="discount"></div>
        <img class="img-responsive" src="<?=$src_0?>" width="567" height="567"  id="bigImage">
    </div>        
</div>
<div class="col-md-5 col-sm-12">
    <div class="box">
        <h1 class="h1"><?=$nameProd?></h1>
		<p class="sku" id="skuProduct">SKU <?=$skuProd?></p>
        <p id="descriptionProduct"><?=$descripcion_short?> 
        </p>
        <?php if(!empty($precio)){?>
			<p class="fixmargin"><strike class="precio old" id="originalPrice" style="display:none"></strike><?=$strPrice?>
            <strong class="precio mxn" id="precioProduct"><?=eCommerce_SDO_CurrencyManager::NumberFormat($minimo);?></strong>
            </p>
        <?php }else{ ?>
    		<p class="fixmargin"><strike class="precio old" id="originalPrice" style="display:none"></strike><strong class="precio" id="precioProduct"><!--$<? //=$currency?>--></strong></p>    	
        <?php }?>
		<div class="fixmargin">
        <div class="row">
        <div class="col-lg-4 col-md-5 col-sm-3 col-xs-5">
	        <!-- <img src="<?=ABS_HTTP_URL?>img/layout/control-cantidad.jpg" width="123" height="45" style="margin-right:20px;"> -->
	        <div id="qty-wrapper">
		        <div class="qty-control" id="res-qty">-</div>
		        <input id="cantidad" name="cantidad" type="number" value="1" min="1" class="form-control" style="background-color:<?=$color;?> color:black; font-weight: bold; font-size: 20px;" onchange="reloadPrecioCantidad(<?=$minimo;?>,this.value);" onkeyup="reloadPrecioCantidad(<?=$minimo;?>,this.value);"/>
		        <div class="qty-control" id="add-qty">+</div>
	        </div>
        </div>
        <div class="col-lg-8 col-md-7 col-sm-9 col-xs-7">
        <form id="addProduct" name="addProduct" class="form form-inline" method="" action="" >
			<input type="hidden" id="idProduct" value="<?=$product->getProductId();?>">								
			<input type="hidden" id="versionProduct" value="<?php echo $versionProduct;?>">								                                
			<input type="hidden" id="precio" value="<?php echo $minimo;?>">
    	   	<div class="form-group" >
		      <?php if(count($coloresVersion) > 0){?>
		      <div id="dataColor">  
			      <select class="form-control" style="height:45px;" id="color" name="color" onChange="reloadProduct(this.value);">
			        <option value="">Atributo</option>
			        <?php foreach($coloresVersion as $color){
						echo '<option value="'.$color['version_id'].'" rel="'.$color['color_id'].'">'.utf8_encode($color['nombre']).'</option>';
					}?>
			      </select>
			      <input type="hidden" name="noAtributos" id="noAtributos" value="false"  />
		      </div>
		      <?php }else{?>
		      <input type="hidden" name="noAtributos" id="noAtributos" value="true"  />
		      <?php }?>
		      <?php if(count($tallasVersion) > 0){?>
		      <div id="dataTalla">
			      <select class="form-control" style="height:45px;" id="talla" name="talla" onChange="reloadProduct(this.value);">
			        <option value="">Característica</option>
			        <?php foreach($tallasVersion as $talla){
						echo '<option value="'.$talla['version_id'].'" rel="'.$talla['talla_id'].'">'.utf8_encode($talla['nombre']).'</option>';
					}?>
			      </select>
			      <input type="hidden" name="noCaracteristica" id="noCaracteristica" value="false"  />
		      </div>
		      <?php }else{?>
		      	<input type="hidden" name="noCaracteristica" id="noCaracteristica" value="true"  />
		      <?php }?>
  			  <button style="margin-top:30px;" id="btnAgregarFancy" class="btn btn-primary btn-carrito btn-lg btn-block" type="submit" onclick="enviaDatos();" value="Agregar al Carrito">Agregar al Carrito</button>
  			</div>
        </form>
        </div>        
    </div>
    </div>
        </div>
        <div class="fixmargin">        
        </div>
        <div class="fixmargin fixpadding box-border" style="border-width:1px 0;">
        <div class="row">
	        <div class="col-md-12">
            <!--
	            <img src="<?=ABS_HTTP_URL?>img/layout/recomendar-btn.png" width="121" height="20"><img src="<?=ABS_HTTP_URL?>img/layout/twitter-btn.png" alt="" width="89" height="20"><img src="<?=ABS_HTTP_URL?>img/layout/pin-btn.png" alt="" width="40" height="20">
             -->
             <?php include 'redes_sociales.php';?>
	        </div>
	        <div class="col-md-12" style="display:none">
	            <a class="btn-deseos" href="#"><i class="glyphicon glyphicon-heart"></i> Lista de deseos</a>
	        </div>
        </div>
        </div>
        <div class="metodosdepago fixmargin">  
	        <img src="<?=ABS_HTTP_URL?>img/layout/ico/paypal-gris.png" width="75" height="21" alt="Paypal">
	        <img src="<?=ABS_HTTP_URL?>img/layout/ico/oxxo.png" width="68" height="31" alt="Oxxo">
	        <img src="<?=ABS_HTTP_URL?>img/layout/ico/seven-gris.png" width="30" height="31" alt="Seven">
        </div>
    </div>
</div>
</div>

</div>
</section>
<!--descripción-->
<section class="descripcion">
<div class="container">
<div class="row">
<div class="col-xs-9">
<ul id="producto-tabs" class="nav nav-tabs" role="tablist">
	<?php if(!empty($descripcion_long)){?>
	<li role="presentation" class="active"><a href="#detalles" aria-controls="detalles" role="tab" data-toggle="tab">Detalles</a></li>
    <?php }?>
    <?php if(!empty($especificaciones)){?>
	<li role="presentation"><a href="#especificaciones" aria-controls="especificaciones" role="tab" data-toggle="tab">Especificaciones</a></li>
    <?php }?>
    <?php if(!empty($video)){?>
	<li role="presentation"><a href="#aplicabilidad" aria-controls="aplicabilidad" role="tab" data-toggle="tab">Video</a></li>
    <?php }?>
</ul>					
<div class="tab-content">
						<?php if(!empty($descripcion_long)){?>
                        <div role="tabpanel" class="tab-pane active" id="detalles">
							<p style="font-size: 14px; line-height: 20px;" class="tvealignjustify"><?=$descripcion_long?></p>
						</div>
						<?php }?>
                        <?php if(!empty($especificaciones)){?>
						<div role="tabpanel" class="tab-pane" id="especificaciones">
							<p style="font-size: 14px; line-height: 20px;" class="tvealignjustify"><?=$especificaciones?></p>
						</div>
                        <?php }?>
						<?php if(!empty($video)){?>
						<div role="tabpanel" class="tab-pane" id="aplicabilidad">
							<p><iframe width="560" height="315" src="<?=$video?>" frameborder="0" allowfullscreen></iframe></p>
						</div>
                        <?php }?>
					</div>

 <!--<h3 class="h3 title-border">Descripción</h3>
 <p style="font-size: 14px; line-height: 20px;" class="tvealignjustify"><?=$descripcion_long?></font></span></p>-->
</div>
<div id="sidebar-right" class="col-xs-3">
<div class="box">
  <?php  include("carouselpublicidad.php"); ?>
</div>

</div>

</div>
</div>
</section>

<!--recomendados-->
<?php 
if(!empty($produc_relacionados)){
?>
<div class="container">
<section class="categoria" >
<div  class="row" id="product-list-wrapper">
 <h3 class="h3 title-border">Productos relacionados</h3>
 	<?php 
	$relacionados = explode(',', $produc_relacionados);	
	foreach ($relacionados as $producto_id) {
		$product_rel            = eCommerce_SDO_Catalog::LoadProduct($producto_id);		
		$thumbnail_rel          = $product_rel->getUrlImageId('medium',0);
		$prod_nom_rel           = $product_rel->getName();
		$VersionesAgrupadas_rel = eCommerce_SDO_Catalog::GetProductVersiones($producto_id);		
		$prod_pre_rel           = $VersionesAgrupadas_rel[0]['price'];
		$prod_pre_rel 			= eCommerce_SDO_CurrencyManager::ValueInActualCurrency( $prod_pre_rel, $product_rel->getCurrency() );
		$prod_currency_rel      = $VersionesAgrupadas_rel[0]['currency'];
		$href					= $product_rel->getFriendlyNameUrl($categoryId);
		$currency            	= $product_rel->getCurrency();
		$currency 				= eCommerce_SDO_CurrencyManager::GetActualCurrency();
		$discount				= round($VersionesAgrupadas_rel[0]['discount']);

		if($discount>0 && !empty($discount)){
				$precioOriginal = $prod_pre_rel;
				$prod_pre_rel = $prod_pre_rel * ( 1 - ( $discount/100 ));	
				
		}
	?>
	<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
          <div class="item">
          <div class="thumbnail">
                        <a href="<?=$href?>" title="" class="img-thumbnail">
                            <img width="230" height="230" alt="" src="<?=$thumbnail_rel?>" style="height:350px;">
                            <?php if(!empty($discount)){?>
                            <div class="flag oferta">-<?=$discount?>%</div>
                            <?php }?>
                        </a>
                        <div class="caption">
                            <h4 style="word-wrap: break-word;"><a href="<?=$href?>" title=""><?=$prod_nom_rel?></a> </h4>
                        </div>
                <div class="row">
                <?php if(!empty($discount)){?>
                    <div class="col-xs-6">
                            <p class="precio old"><strike>$<?=$precioOriginal?>MXN</strike></p>
                    </div>
                    <div class="col-xs-6" style="text-align:right;">
                            <p class="precio">$<?=$prod_pre_rel?><?=$currency?></p>
                    </div>
                <?php }else{?>
                 <div class="col-xs-12" style="text-align:right;">
                            <p class="precio">$<?=$prod_pre_rel?><?=$currency?></p>
                    </div>
                <?php }?>
                    
                </div>
                <a href="<?=$href?>" class="btn btn-primary btn-carrito btn-block">Ver más</a>
            </div>
          </div>
	</div>
    <?php }?>
	  
</div>
</section>
</div>
<?php }?>
<!--recomendados-->
<!--descripción-->

<?php include('footer.php');?>

<script type="text/javascript">
$(function(){
	var qty_in=$('#cantidad');
	//Plus one Quantity
	$('body').on('click', '#add-qty', function(){
		qty = parseInt(qty_in.val());
		if(qty < 25){
			qty++;
			qty_in.attr('value',qty);
			qty_in.change();
		}
	});
	//Subtract one Quantity
	$('body').on('click', '#res-qty', function(){
		qty = parseInt(qty_in.val());
		if(qty > 1){
			qty--;
			qty_in.attr('value',qty);
			qty_in.change();
		}
	});
});
	function reloadPrecioCantidad(precio,cantidad){		
		Number.prototype.format = function (n, x) {
			var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
			return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
		};
		
		if(cantidad.length==0 || cantidad <= 0){
			var precio = new Array();
			precio[0] = "0";
			precio[1] = "00";
		}else{
			var precioH         = $('#precio').val();						
			var precioCantidad = (parseFloat(precioH) * parseFloat(cantidad));
			precioCantidad     = precioCantidad.format(2);
			precio             = precioCantidad.split('.');	
		}//end if
		var html = "$"+precio[0]+'.'+precio[1];
		$("#precioProduct").html(html);
	}

	function reloadProduct(version_id){		
		var Url = '<?=ABS_HTTP_URL?>';
		var t_id = $("#talla").find('option:selected').attr('rel');
		var c_id = $("#color").find('option:selected').attr('rel');
		var p_id = $("#idProduct").val();
		var noAtributos = $("#noAtributos").val();
		var noCaracteristica = $("#noCaracteristica").val();
		
		if(version_id != ""){
			$.ajax({
				type: 'POST',
				url: Url+'searchProductAjax.php',
				data: {'version_id': version_id},
				beforeSend: function(){
					$('#searchProduct').addClass('loading');
				},
				success: function(data){
					$('#searchProduct').removeClass('loading');					
					if(data.estatus == true){
						$("#nameProduct").html(data.name);
						$("#skuProduct").html(data.sku);
						$("#descriptionProduct").html(data.descriptionProduct);
						$("#detailsProduct").html(data.detailsProduct);
											
						var descuento = parseInt(data.discount);						
						if(descuento > 0){
							$("#discount").html("-"+data.discount+"%");	
							$("#discount").css('display','block');					
							$("#precioProduct").val(data.precio);
							$("#originalPrice").css('display','block');
							$("#originalPrice").html(data.price);
						}else{
							$("#precioProduct").html(data.price);
							$("#precio").val(data.price);	
							$("#discount").css('display','none');
							$("#originalPrice").css('display','none');
						}
						//$("#especificcionProduct").html(data.especificacion);
						$("#versionProduct").val(data.version_id);
					}else{
						return false;
					}
				}
			});
			
			$.ajax({
				type: 'POST',
				url: Url+'updateVersionInfo.php',
				data: {'product_id' : p_id, 'version_id': version_id, 'talla_id' : t_id, 'color_id'  :c_id, 'noAtributos' : noAtributos, 'noCaracteristica':noCaracteristica},
				beforeSend: function(){					
					$('#searchProduct').addClass('loading');
				},
				success: function(data){										
						$('#searchProduct').removeClass('loading');																				
						var strSelected = '';
						if(!$("#noCaracteristica")){
							var strTalla = '<select class="form-control" style="height:45px;" id="talla" name="talla" onChange="reloadProduct(this.value);"><option value="">Caracteristica</option>';
							$.each(data.talla_id,function(index){
								strSelected = '';
								if(data.talla_id[index] == t_id){							
									strSelected = 'selected="selected"';	
								}
								strTalla = strTalla+'<option value="'+data.version_id+'" rel="'+data.talla_id[index]+'" '+strSelected+'>'+data.talla_nombre[index]+'</option>';						
							});					
							strTalla = strTalla+'</select>';
						}
					strSelected = '';
					
					if(!$("#noAtributos")){						
						var strColor = '<select class="form-control" style="height:45px;" id="color" name="color" onChange="reloadProduct(this.value);"><option value="">Atributo</option>';
						$.each(data.color_id,function(index){	
							strSelected = ''						
							if(data.color_id[index] == c_id){
								strSelected = 'selected="selected"';	
							}					
							strColor = strColor+'<option value="'+data.version_id+'" rel="'+data.color_id[index]+'" '+strSelected+'>'+data.color_nombre[index]+'</option>';						
						});										
						strColor = strColor+'</select>';	
					}
					if(data.estatus == true){
						if(!$("#noAtributos")){
							$('#dataColor').html(strColor);						
						}
						if(!$("#noCaracteristica")){
							$('#dataTalla').html(strTalla);
						}
						$("#versionProduct").val(data.version_id);
												
						$("#thumbnails").html(data.imagenes);
						$("#skuProduct").html("SKU "+data.sku);
						var descuento = parseInt(data.discount);
												
						if(descuento > 0){
							$("#discount").html("-"+data.discount+"%");	
							$("#discount").css('display','block');					
							$("#precioProduct").html("$"+data.precio+"MXN");
							$("#precio").val(data.precio);
							$("#originalPrice").css('display','block');
							$("#originalPrice").html("$"+data.price+"MXN");
						}else{
							$("#precio").val(data.price);
							$("#discount").css('display','none');
							$("#originalPrice").css('display','none');	
						}
					}else{
						return false;
					}
				}
			});
			
			
		}else{
			return false;
		}//end if
	}//end function
	function enviaDatos(){
		jQuery.validator.addMethod("integer", function(value, element) {
			return this.optional(element) || /^-?\d+$/.test(value);
		}, "A positive or negative non-decimal number please");
		$("#addProduct").validate({
			debug: false,
			rules:{
				talla:{required: true,},
				color:{required: true,},
				cantidad:{required: true, min: 1, integer: true},
			},
			messages:{
				color:{required: 'Seleccione atributo'},
				talla:{required: 'Selecciona caracteristica'},
				cantidad:{required: 'Cantidad es requerida',min: 'La cantidad debe ser mayor o igual a 1', integer:'Cantidad no acepta decimales'},
			},
			submitHandler: function(form){
				var $thisTalla = $("#talla");
				var $thisColor = $("#color");
				var productId  = $("#idProduct").val();
				var version_id = $("#versionProduct").val();
				var cantidad   = $("#cantidad").val();
				var talla		= $thisTalla.find('option:selected').attr('rel');
				var color		= $thisColor.find('option:selected').attr('rel');
				var Url        = '<?=ABS_HTTP_PATH?>';
				loadAjax(
					'show_add_cart_xajax',
					Url+'cart.php',
					'cmd=add&productId='+productId+'&quantity='+cantidad+'&versionId='+version_id+'&inAjax=1&talla='+talla+'&color='+color,
					'<img src="<?=ABS_HTTP_URL?>img/preload.gif" alt="loading..." style="padding-top:15px;"',true
				);
				$.ajax({
					type: 'POST',
					url: Url+'cart.php',
					data: 'cmd=getTotalItems',
					beforeSend: function(data){
						$("#total_item").html('...');
					},
					success: function(data){
						$("#total_item").html('');
						$("#total_item").html('('+data+')');
					}
				});
			},
		});
		return false;		
	}

function loadAjax(div,url,str_data,html_before,estilo){				
		$.ajax({
			async: true,
			type:		"POST",
			url: 		url,
			data:		str_data,
			dataType:	"html",			
			beforeSend: function (objeto){
				$('#btnAgregarFancy').attr("disabled", true);
				$("#btnAgregarFancy").html('Loading ...');
			},
			complete:	function (objeto, exito){},
			error:		function (objeto, quepaso, otroobj){
			},
			success:	function (datos){
				var Url        = '<?=ABS_HTTP_PATH?>';
				$.ajax({
					type: 'POST',
					url: Url+'cart.php',
					data: 'cmd=getTotalItemsCount',
					beforeSend: function(data){
						$("#total_item").html('...');
						$("#cantidadRight").html('...');
					},
					success: function(data){
						$("#total_item").html('');
						$("#total_item").html(data);
						$("#cantidadRight").html('');
						$("#cantidadRight").html(data);
					}
				});	
															
				$('#btnAgregarFancy').attr("disabled", false);
				$("#btnAgregarFancy").html('Agregar al carrito');
				/*$.fancybox({
					padding	: 8,
					helpers : { 'overlay' : {'closeClick': false}},
					type : "iframe",
					content: datos,
				});*/
				$("#cartright").show('slow').delay(5000).fadeOut();
			},
		});
		//INICIO
		$.ajax({
			async: true,
			type:		"POST",
			url: 		url,
			data:		"",
			dataType:	"html",			
			beforeSend: "",
			complete:	function (objeto, exito){},
			error:		function (objeto, quepaso, otroobj){
			},
			success:	function (datos){
				var Url        = '<?=ABS_HTTP_PATH?>';								
				$.ajax({
					type: 'POST',
					url: Url+'cart.php',
					data: 'cmd=getHTMLCart',
					beforeSend: function(data){
						$("#cartright").html('...');						
					},
					success: function(data){
						$("#cartright").html('');
						$("#cartright").html(data);						
					}
				});												
			},
		});		
		//FIN		
	}//end function 	
	$(function(){
	$("body").on('click','.thumbCSS',function(){
		var src = $(this).attr('src');		
		var id = $(this).attr('id');
		var idPadre = $("#bigImage");
		idPadre.attr('src',src);
		});	
	});
</script>