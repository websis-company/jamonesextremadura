<?php
$Products = $this->result->getResults();
$category = $this->category;
$brand    = $this->brand;
?>

<div class="row">
<?php 
foreach ($Products as $product) {
	$Descripcion = Util_String::subText($product->getDescriptionShort(),100);
	$img_0       = $product->getUrlImageId('medium',0);
	$href        = $product->getFriendlyNameUrl();
?>
	<div class="col-xs-4">
		<div class="thumbnail">
			<a href="<?=$href?>" class="img-thumbnail" style="background-image:url(<?php echo $img_0 ?>);"></a>
			<div class="caption">
				<div style="height: 20px;">
					<h5 class="h5"><?php echo ucwords($product->getName());?></h5>
				</div>
				<div class="row" style="text-align:center;">
					<div class="col-xs-12" style="padding-right:5px;">
						<p class="price"><?php echo $precio = number_format($product->getRealPrice(),2); ?><small>MXN</small></p>
					</div>
				</div>
				<a href="<?=$href;?>" class="btn btn-comprar btn-block" role="button">Comprar</a>
			</div>
		</div>
	</div>
<?php
}
?>
</div>


<div class="row pull-right">
	<?php 
	/*echo '<pre>';
	print_r($this->result);
	echo '</pre>';*/
	?>
	<?//=eCommerce_SDO_Core_Application_Form::foPrintPager($this->result,"Form","negro12","rojo14","rojo14",5,$hiddenFields);?>
</div>
















