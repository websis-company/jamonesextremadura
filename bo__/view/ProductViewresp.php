<?php
function printValue($value, $label = '', $type = '', $link = '') {
	$html = '';
	if(!empty($value)) {
		$html .= '<p style="margin:6px 0" class="">';
		if(!empty($value)) {
			$html .= '<span class="">'.$label.':</span>&nbsp;';
		}
		$html .= $link ? "<a href='$link' class='az1 hover'>" : "";
		switch($type) {
			case 'price':
				$html = '$ '.number_format($value, 0);
			default :
				$html .= trim($value);
		}
		$html .= $link ? "</a>" : "";
		
	}
	echo $html;
}

$product = $this->product;
$category = $this->category;
$brand = $this->brand;

$baseFriendlyUrl = ABS_HTTP_URL . 'producto/';
$baseListFriendlyUrl = ABS_HTTP_URL . 'productos/';

$skuProd = trim($product->getSku());
$descrProd = nl2br(trim($product->getDescriptionShort()));

$nameProd = $product->getName();

$header_title = !empty($nameProd) ? $nameProd : $skuProd;
$header_description = Util_String::subText(trim($product->getDescriptionShort()), 200, 'chars', '');
$header_keywords = '';

if(!empty($category)) {
	$header_title .= ' | ';
	$header_title_path = array();
	$productPathUrl .= $category->getCategoryId().'/';
	$pagingPathUrl .= $category->getCategoryId().'/';
	foreach($this->categoriesTree as $c) {
		$name = trim($c->getName());
		$header_title_path[] = $name;
		$friendlyName = Util_FriendlyURL::formatText($name);
		$productPathUrl .= $friendlyName . '/';
		$pagingPathUrl .= $friendlyName . '/';
	}
	$header_title .= implode(' - ', $header_title_path);
} else if(!empty($brand)) {
	$name = trim($brand->getName());
	$header_title .= ' | '.$name;
	$friendlyName = Util_FriendlyURL::formatText($name);
	$productPathUrl .= $brand->getBrandId().'/'.strtolower($brand->getName()).'-'.$friendlyName.'/';
	$pagingPathUrl .= $brand->getBrandId().'/'.strtolower($brand->getName()).'-'.$friendlyName.'/';
}

$inicio = 1;
include("header.php");


$descripcion_short = utf8_encode($product->getDescriptionShort());
$descripcion_long = utf8_encode($product->getDescriptionLong());
$precio = $product->getPrice();
$color = $product->getColors();
$talla = $product->getTalla();
$src	= $product->getUrlImageId('medium',0);
$srcJ	= $product->getUrlImageId('large',0);
$imas 	= $product->getArrayImages();


$productSmallArrSrc		= $product->getUrlArrayImages('small',NULL,true);
$productMediumArrSrc	= $product->getUrlArrayImages('medium',NULL,true);
$productLargeArrSrc		= $product->getUrlArrayImages('large',NULL,true);
$productJumboArrSrc		= $product->getUrlArrayImages('large',NULL,true);


$p_id = $product->getProductId();
$colores = eCommerce_SDO_Core_Application_Relacioncolor::GetByProductId($p_id);
$tallas	= eCommerce_SDO_Core_Application_Relaciontalla::GetByProductId($p_id);

$id	= $product->getProductId();

?>
<script src="http://code.jquery.com/jquery-1.6.1.min.js"></script>
<script type="text/javascript">
	jQuery(document).ready(
		function(){
			jQuery("img[src$='<?=ABS_HTTP_URL?>ima/int/btn/carrito.png']").click(
				function(){
					colorr 	= jQuery("#color").val();
					talla 	= jQuery("#talla").val();
					qty		= jQuery("#quantity").val();
					if(colorr == ''){						
						alert('Debes seleccionar un color');
						document.getElementById("color").focus();
						return false;
					}
					loadAjaxCart(	'cart_ajax',
								'<?=ABS_HTTP_PATH?>addcart.php',
								'inAjax=1&id_product=<?=$id?>&color='+colorr+'&talla='+talla+'&quantity='+qty,
								'<img src="<?=ABS_HTTP_PATH?>ima/load.gif" border="0" style="padding:38px 0 73px 124px" />');
					
				}
			);
		}
	);
	function hideCarrito(){
		$("div#cart_ajax").fadeOut("slow");
	}
</script>

<script type="text/javascript"> 

 function sendLink(id,url){
	document.getElementById(id).href=url;
   //alert(url); 
	 }

</script>

<!-- ZOOM_CAROUSEL -->		
<script type="text/javascript" src="<?=ABS_HTTP_URL?>zoom_carousel/jcarousel/jquery.jcarousel.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?=ABS_HTTP_URL?>zoom_carousel/jcarousel/skins/tango/skin.css" />

<script src="<?=ABS_HTTP_URL?>zoom_carousel/jqzoom/js/jquery.jqzoom-core.js" type="text/javascript"></script>
<link rel="stylesheet" href="<?=ABS_HTTP_URL?>zoom_carousel/jqzoom/css/jquery.jqzoom.css" type="text/css">

<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery('.jqzoom').jqzoom({
			zoomType: 'standard',
			lens:true,
			preloadImages: false,
			alwaysOn:false			
		});
		jQuery('#mycarousel').jcarousel();
	});
</script>
<!-- ZOOM_CAROUSEL -->


<!-- ZOOM -->

 <script type="text/javascript" src="<?=ABS_HTTP_URL?>zoom/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
 <link rel="stylesheet" type="text/css" href="<?=ABS_HTTP_URL?>zoom/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
 
<style>
<!--
	.zoomWindow{
		vertical-align : 'top';
	}
	.zoomWrapper{
		 
		margin-top : -1px; 
		margin-left : 16px;
		 
		*margin-top : -2px; 
		*margin-left : -3px;
	}
	
-->
</style>
 
 <script type="text/javascript">
 $(document).ready(function() {
		

		$(".popup").fancybox({
			'overlayShow'	: true,
			'transitionIn'	: 'elastic',
			'transitionOut'	: 'elastic'
		});

		$("#div_img").mouseover(function(){
			var current = $(".zoomWindow");
			current.css('left','290px');
			current.css('top','45px');
			//current.css('top','-60px');
			//curretnt.css('margin-left','400px');
			//current.css({'vertical-align' : 'top'});
			current.find('.zoomWrapper').css({'width' : '363px', 'vertical-align' : 'top'});
		});
		
	});
  </script>

<!-- ZOOM -->

<style>
.thumbs li{
	margin:5px;
	float:left;
	list-style:none;
	}
</style>

<div id="cart_ajax" style="position:absolute;background-color:#FFF;display:none; margin-left:286px; margin-top:-32px; *margin-top:120px; z-index:700; width:808px;"></div>




<div id="contenido" style="background-color:#FFF;">

<div class="pie1">
		<? include "categorias.php"; ?>


<div class="derecha-interior" style="width:720px;">
    		<h1 class="azulclaro24" style="margin-left:35px"><?=$nameProd?></h1>
            <div class="columna-fotoProducto" align="center">
			  <? $currentImgSmall 	= array_shift($productSmallArrSrc);
				 $currentImgMedium 	= array_shift($productMediumArrSrc);							
				 $currentImgJumbo 	= array_shift($productJumboArrSrc);?>
                 
                <div class="clearfix" id="content" style="height:374px;width:250px;vertical-align:top;" >							 
                	<div class="clearfix"  id="div_img" style="z-index:-1">
		              <p align="center"><a href="<?=$currentImgJumbo?>" title=" " class="jqzoom" rel='gal1'><img style="max-width:400px" height="440"  src="<?=$currentImgMedium?>" border="0" title="<?=$nameProd?>"></a>                       		   
                      </p>
                      <a href="<?=$currentImgJumbo?>" id="photo" class="popup" rel='testGaleria' title=" " style="position: absolute; top: 150px; margin-top: 500px; vertical-align: bottom;  margin-left: -35px;" >							
					            <img src="<?=ABS_HTTP_URL?>zoom/zoom-in.png"  alt="zoom" title="click para agrandar" border="0"  >
	                       </a>
                     </div>
                <div style="clear:both;"></div>
                <div id="wrap">
					<ul  style="width:50px !important; list-style:none; position:absolute; margin-top:-440px;  margin-left:-125px">
					    <li>
					    <a class="zoomThumbActive" href='javascript:void(0);' onclick="sendLink('photo','<?=$srcJ?>');"
						    rel="{gallery: 'gal1', smallimage: '<?=$src?>',largeimage: '<?=$srcJ?>'}" title=" " >						
								    <img src="<?=$src?>" height="85"   alt=" " title=" "  border="0"/></a></li>
                         <?  $cont_img = 0;
						 	$i=0; 
						 		foreach($imas as $image_s){
								$src_img	= $product->getUrlImagesId('medium',$cont_img);
								$src_imgG	= $product->getUrlImagesId('large',$cont_img);
								
								?>
						    <li>
							    <a href='javascript:void(0);' onclick="sendLink('photo','<?=$productJumboArrSrc[$i]?>');"
							rel="{gallery: 'gal1', smallimage: '<?=$productMediumArrSrc[$i]?>',largeimage: '<?=$productJumboArrSrc[$i]?>'}" title=" " >
							    <img src="<?=$src_img?>" height="85" style="max-width:80px"  alt=" " title=" " border="0" /></a></li>
							<? $i++; $cont_img++; }?>
                        </ul>
                       </div>
				</div>
            </div>
                                    
      <div class="columna-datosProducto" style="margin-top:11px; height:440px">
            	<h2 class="azulclaro18" style="margin-left:15px"><?=$descripcion_short?></h2>                
       	    <p style="height:200px; margin-left:15px"> <?=$descripcion_long?></p>
   	    <p class="precio" style="margin-left:15px">Precio: $<?=$precio?> </p>
        <p class="precio" style="margin-left:15px">Colores: <? //=$color?> 
        <select name="color" id="color" class="bold14" style="width:200px">
        <option value="">--Seleccionar color--</option>
        <? foreach($colores as $color) {
			$color_id = $color->getColorId();
			$color_data = eCommerce_SDO_Core_Application_Color::LoadById($color_id);
			$color_name = $color_data->getNombre();
			$color_data_id = $color_data->getColorId();					
			?>
        <option value="<?=$color_data_id?>"><?=$color_name?></option>
        <? }?>
        </select>
        
        </p>
        <p class="precio" style="margin-left:15px">Tallas: <? //=$talla?> 
        	 <select name="talla" id="talla" class="bold14"  style="width:200px; margin-left:15px"><option value="">--Seleccionar talla--</option>
				<? foreach($tallas as $talla) {
                    $talla_id = $talla->getTallaId();
                    $talla_data = eCommerce_SDO_Core_Application_Talla::LoadById($talla_id);
                    $talla_name = $talla_data->getNombre();
                    $talla_data_id = $talla_data->getTallaId();					
                    ?>
          				  <option value="<?=$talla_data_id?>"><?=$talla_name?></option>
            <? }?>
            </select>
        </p>
   	    <p class="precio" style="margin-left:15px">Cantidad: <input id="quantity" name="producto<?php echo $product->getProductId(); ?>" type="text" class="gris11" style="width:40px"/><img src="<?=ABS_HTTP_URL?>ima/int/btn/carrito.png" width="143" height="26" alt="Agregar a carrito" valign="bottom"  style="cursor:pointer; margin-left:10px; vertical-align:middle"/>       
        <!--<select name="color" id="color"><option value="">--Color--</option>
        <? foreach($colores as $color) {
			$color_id = $color->getColorId();
			$color_data = eCommerce_SDO_Core_Application_Color::LoadById($color_id);
			$color_name = $color_data->getNombre();
			$color_data_id = $color_data->getColorId();					
			?>
        <option value="<?=$color_data_id?>"><?=$color_name?></option>
        <? }?>
        </select>--></p>       
		<div class="redes" style="margin-top:25px; margin-left:20px" align="center"><? include "redes_news.php";?></div>
      </div>
      <div class="columna-fotoProducto" style="float: left; width: 320px; clear: both; display:none" align="center">
      <ul class="thumbs" style="padding:0">
      	<? $cont_img = 0; foreach($imas as $image_s){
			$src_img	= $product->getUrlImagesId('medium',$cont_img);
			$src_imgG	= $product->getUrlImagesId('large',$cont_img);
			?>
              <li><a href="<?=$src_imgG?>" rel="jquery-lightbox"><img height="100" style="max-width:80px"  src="<?=$src_img?>" border="0"></a></li>
        <? $cont_img++; }?>
		</ul>             
            </div>
        
                                                 
		</div><!--derecha -->
	</div>
</div>
<?
require_once("footer.php");
?>